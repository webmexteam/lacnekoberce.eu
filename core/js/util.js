function roundDec(num, dec){
	return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
}

function parseFloatNum(num){
	return parseFloat(String(num).replace(/\s/g, '').replace(/\,(\d{1,2})$/g, '.$1').replace(/\,/g, ''));
}

function fprice(num){
	if(_price_format == 1){
		return number_format(parseFloatNum(num), 2, '.', '');
		
	}else if(_price_format == 2){
		return number_format(parseFloatNum(num), 2, '.', ',');
		
	}else if(_price_format == 3){
		return number_format(parseFloatNum(num), 2, ',', ' ');
		
	}else if(_price_format == 4){
		return number_format(parseFloatNum(num), 0, '.', '');
		
	}else if(_price_format == 5){
		return number_format(parseFloatNum(num), 0, '.', ',');
	}
	
	return number_format(parseFloatNum(num), 0, '.', ' ');
}

function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number+'').replace(',', '').replace(' ', '');
    var n = !isFinite(+number) ? 0 : +number, 
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function price_vat(num, vat)
{
	price = parseFloatNum(num);
	
	if(vat && _vat_payer && _vat_mode == 'exclude'){
		price = price * (vat / 100 + 1);
	}
	
	if(_price_vat_round == 2){
		price = Math.round(price * 100) / 100;
		
	}else if(_price_vat_round == 1){
		price = Math.round(price * 10) / 10;
		
	}else if(_price_vat_round == 0){
		price = Math.round(price);
		
	}else{
		price = Math.round(price * 100) / 100;
	}
	
	return price;
}

function price_unvat(num, vat)
{
	price = parseFloatNum(num);
	
	if(vat && _vat_payer && _vat_mode == 'include'){
		price = price / (vat / 100 + 1);
	}
	
	return Math.round(price * 100) / 100;
}

function displayPrice(num)
{
	price = parseFloatNum(num);

	return fprice(price)+'&nbsp;'+_currency;
}

var Template = function(templates){
	this.templates = templates;

	return this;
}

Template.prototype = {
	
	apply: function(name, replace){
		var scope = this;
		var result = this.templates[name];
		
		if(typeof replace == 'object'){		
			// [if abc == 123] html1 [else] html2 [endif]
			var _exp_if = result.match(/\[if([^\[\]]*)\]([^\]\[]*)(\[else\])?([^\[\]]*)\[endif\]/gi);

			if(_exp_if){
				$.each(_exp_if, function(i, m){
					var _res = false, values = replace, m = m.replace(/(\[|\]|\(|\))/gi, "\\$1");
					var parts = result.match(/\[if([^\[\]]*)\]([^\]\[]*)(\[else\])?([^\[\]]*)\[endif\]/i);
					
					eval('if('+parts[1]+'){ _res = true; }else{ _res = false; }');
					
					if(_res){
						result = result.replace(RegExp(m, 'ig'), parts[2]);
					}else if(parts[4]){
						result = result.replace(RegExp(m, 'ig'), parts[4]);
					}else{
						result = result.replace(RegExp(m, 'ig'), '');
					}
				});
			}
		
			$.each(replace, function(i, v){
				result = result.replace(RegExp('\{'+i+'\}', 'ig'), v);
			});
		}
		
		return result;
	}
	
}

function lang(key, count){
	var s, argsPos = 1;

	if(typeof _lang[key] == 'object'){
		var str;
		
		$.each(_lang[key], function(c, s){
			str = s;
			
			var min = 0;
			var max = 999999;
			
			var minmatch = c.match(/^\d/);
			var maxmatch = c.match(/\d$/);
			
			if(minmatch && minmatch.length){
				min = parseInt(minmatch[0], 10);
			}
			
			if(maxmatch && maxmatch.length){
				max = parseInt(maxmatch[0], 10);
			}
			
			if(count >= min && count <= max){
				return false;
			}
		});
		
		return str;
	
	}else if(typeof _lang[key] != 'undefined'){
		s = _lang[key];
		
		var args = Array.prototype.slice.call(arguments, argsPos);
		
		$.each(args, function(i, arg){
			s = s.replace(/\%(s|d)/, arg);
		});
		
		return s;
	}
	
	return key;
}

function __(){
	return lang.apply(this, arguments);
}


var Event = {
	
	events: {},
	
	add: function(name, callback, scope, autoDestroy){
		
		if(typeof this.events[name] != 'object'){
			this.events[name] = [];
		}
		
		if(typeof callback != 'function'){
			return;
		}
	
		this.events[name].push({
			cb: callback,
			autoDestroy: autoDestroy || false,
			scope: scope || Event
		});
	},
	
	run: function(name){
		var args = Array.prototype.slice.call(arguments, 1);
		
		if(typeof this.events[name] == 'object'){
			$.each(this.events[name], $.proxy(function(i, obj){
				if(typeof obj != 'object'){
					return;
				}
				
				obj.cb.apply(obj.scope, args);
				
				if(obj.autoDestroy){
					delete this.events[name][i];
				}
			}, this));
		}
	}
	
}

var Base64 = {
 
	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
 
	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = Base64._utf8_encode(input);
 
		while (i < input.length) {
 
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
 
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
 
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
 
			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
 
		}
 
		return output;
	},
 
	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
		while (i < input.length) {
 
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
 
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
 
			output = output + String.fromCharCode(chr1);
 
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
 
		}
 
		output = Base64._utf8_decode(output);
 
		return output;
 
	},
 
	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	},
 
	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
 
}

var Format = {

	datetime: function(unix_timestamp, format){
		var d = new Date();
		d.setTime(unix_timestamp * 1000);
		
		if(! format){
			format = 'd. m. Y H:i';
		}
		
		return d.formatDate(format);
	},
	
	timeFromSeconds: function(seconds){
		var left = Math.ceil(seconds);
		var s = 0;
		var m = 0;
		var h = 0;
		var d = 0;
		
		if(left >= 86400){
			d = Math.floor(left / 86400);
			left -= d * 86400;
		}
		
		if(left >= 3600){
			h = Math.floor(left / 3600);
			left -= h * 3600;
		}
		
		if(left >= 60){
			m = Math.floor(left / 60);
			left -= m * 60;
		}
		
		s = left;
		
		return (d > 0 ? (d < 10 ? '0'+d : d)+'d ' : '')+
			((h > 0 || d > 0) ? (h < 10 ? '0'+h : h)+'h ' : '')+
			((m > 0 || h > 0) ? (m < 10 ? '0'+m : m)+'m ' : '')+
			((s > 0 || m > 0) ? (s < 10 ? '0'+s : s)+'s ' : '');
	},
	
	price: function(val, show_currency, as_float){
		if(typeof show_currency != 'boolean'){
			show_currency = true;
		}
		
		if(as_float){
			return parseFloat(val || 0).toFixed(2);
		}
		
		var str = '' + Math.round(parseFloat(val || 0) * 100) / 100;
		var parts = str.split('.');
		var rgx = /(\d+)(\d{3})/;
		while(rgx.test(parts[0])){
            parts[0] = parts[0].replace(rgx, '$1,$2');
        }
        
        if(typeof parts[1] == 'undefined'){
        	parts[1] = '0';
        }
        
        if(parts[1].length < 2){
        	parts[1] = parts[1]+'0';
        }
        
        return parts.join('.')+(show_currency ? ' CZK' : '');
	},
	
	parsePrice: function(val){
		return parseFloat(String(val).replace(/[^0-9\.]/g, ''));
	},
	
	fileSize: function(fsize){
		var size = fsize;
		var size_postfix = 'B';
		
		if(size >= 1024){
			size = Math.ceil(size / 1024);
			size_postfix = 'kB';
		}
		
		if(size >= 1024){
			size = Math.ceil(size / 1024);
			size_postfix = 'MB';
		}
		
		if(size >= 1024){
			size = Math.ceil(size / 1024);
			size_postfix = 'GB';
		}
		
		return size+' '+size_postfix;
	}
	
}



// formatDate :
// a PHP date like function, for formatting date strings
// authored by Svend Tofte <www.svendtofte.com>
// the code is in the public domain
//
// see http://www.svendtofte.com/javascript/javascript-date-string-formatting/
// and http://www.php.net/date
//
// thanks to 
//  - Daniel Berlin <mail@daniel-berlin.de>,
//    major overhaul and improvements
//  - Matt Bannon,
//    correcting some stupid bugs in my days-in-the-months list!
//  - levon ghazaryan. pointing out an error in z switch.
//  - Andy Pemberton. pointing out error in c switch 
//
// input : format string
// time : epoch time (seconds, and optional)
//
// if time is not passed, formatting is based on 
// the current "this" date object's set time.
//
// supported switches are
// a, A, B, c, d, D, F, g, G, h, H, i, I (uppercase i), j, l (lowecase L), 
// L, m, M, n, N, O, P, r, s, S, t, U, w, W, y, Y, z, Z
// 
// unsupported (as compared to date in PHP 5.1.3)
// T, e, o

Date.prototype.formatDate = function (input,time) {
    
    var daysLong =    ["Sunday", "Monday", "Tuesday", "Wednesday", 
                       "Thursday", "Friday", "Saturday"];
    var daysShort =   ["Sun", "Mon", "Tue", "Wed", 
                       "Thu", "Fri", "Sat"];
    var monthsShort = ["Jan", "Feb", "Mar", "Apr",
                       "May", "Jun", "Jul", "Aug", "Sep",
                       "Oct", "Nov", "Dec"];
    var monthsLong =  ["January", "February", "March", "April",
                       "May", "June", "July", "August", "September",
                       "October", "November", "December"];

    var switches = { // switches object
        
        a : function () {
            // Lowercase Ante meridiem and Post meridiem
            return date.getHours() > 11? "pm" : "am";
        },
        
        A : function () {
            // Uppercase Ante meridiem and Post meridiem
            return (this.a().toUpperCase ());
        },
    
        B : function (){
            // Swatch internet time. code simply grabbed from ppk,
            // since I was feeling lazy:
            // http://www.xs4all.nl/~ppk/js/beat.html
            var off = (date.getTimezoneOffset() + 60)*60;
            var theSeconds = (date.getHours() * 3600) + 
                             (date.getMinutes() * 60) + 
                              date.getSeconds() + off;
            var beat = Math.floor(theSeconds/86.4);
            if (beat > 1000) beat -= 1000;
            if (beat < 0) beat += 1000;
            if ((String(beat)).length == 1) beat = "00"+beat;
            if ((String(beat)).length == 2) beat = "0"+beat;
            return beat;
        },
        
        c : function () {
            // ISO 8601 date (e.g.: "2004-02-12T15:19:21+00:00"), as per
            // http://www.cl.cam.ac.uk/~mgk25/iso-time.html
            return (this.Y() + "-" + this.m() + "-" + this.d() + "T" + 
                    this.H() + ":" + this.i() + ":" + this.s() + this.P());
        },
        
        d : function () {
            // Day of the month, 2 digits with leading zeros
            var j = String(this.j());
            return (j.length == 1 ? "0"+j : j);
        },
        
        D : function () {
            // A textual representation of a day, three letters
            return daysShort[date.getDay()];
        },
        
        F : function () {
            // A full textual representation of a month
            return monthsLong[date.getMonth()];
        },
        
        g : function () {
           // 12-hour format of an hour without leading zeros, 1 through 12!
           if (date.getHours() == 0) {
               return 12;
           } else {
               return date.getHours()>12 ? date.getHours()-12 : date.getHours();
           }
       },
        
        G : function () {
            // 24-hour format of an hour without leading zeros
            return date.getHours();
        },
        
        h : function () {
            // 12-hour format of an hour with leading zeros
            var g = String(this.g());
            return (g.length == 1 ? "0"+g : g);
        },
        
        H : function () {
            // 24-hour format of an hour with leading zeros
            var G = String(this.G());
            return (G.length == 1 ? "0"+G : G);
        },
        
        i : function () {
            // Minutes with leading zeros
            var min = String (date.getMinutes ());
            return (min.length == 1 ? "0" + min : min);
        },
        
        I : function () {
            // Whether or not the date is in daylight saving time (DST)
            // note that this has no bearing in actual DST mechanics,
            // and is just a pure guess. buyer beware.
            var noDST = new Date ("January 1 " + this.Y() + " 00:00:00");
            return (noDST.getTimezoneOffset () == 
                    date.getTimezoneOffset () ? 0 : 1);
        },
        
        j : function () {
            // Day of the month without leading zeros
            return date.getDate();
        },
        
        l : function () {
            // A full textual representation of the day of the week
            return daysLong[date.getDay()];
        },
        
        L : function () {
            // leap year or not. 1 if leap year, 0 if not.
            // the logic should match iso's 8601 standard.
            // http://www.uic.edu/depts/accc/software/isodates/leapyear.html
            var Y = this.Y();
            if (         
                (Y % 4 == 0 && Y % 100 != 0) ||
                (Y % 4 == 0 && Y % 100 == 0 && Y % 400 == 0)
                ) {
                return 1;
            } else {
                return 0;
            }
        },
        
        m : function () {
            // Numeric representation of a month, with leading zeros
            var n = String(this.n());
            return (n.length == 1 ? "0"+n : n);
        },
        
        M : function () {
            // A short textual representation of a month, three letters
            return monthsShort[date.getMonth()];
        },
        
        n : function () {
            // Numeric representation of a month, without leading zeros
            return date.getMonth()+1;
        },
        
        N : function () {
            // ISO-8601 numeric representation of the day of the week
            var w = this.w();
            return (w == 0 ? 7 : w);
        },
        
        O : function () {
            // Difference to Greenwich time (GMT) in hours
            var os = Math.abs(date.getTimezoneOffset());
            var h = String(Math.floor(os/60));
            var m = String(os%60);
            h.length == 1? h = "0"+h:1;
            m.length == 1? m = "0"+m:1;
            return date.getTimezoneOffset() < 0 ? "+"+h+m : "-"+h+m;
        },
        
        P : function () {
            // Difference to GMT, with colon between hours and minutes
            var O = this.O();
            return (O.substr(0, 3) + ":" + O.substr(3, 2));
        },      
        
        r : function () {
            // RFC 822 formatted date
            var r; // result
            //  Thu         ,     21               Dec              2000
            r = this.D() + ", " + this.d() + " " + this.M() + " " + this.Y() +
            //    16          :    01          :    07               0200
            " " + this.H() + ":" + this.i() + ":" + this.s() + " " + this.O();
            return r;
        },

        s : function () {
            // Seconds, with leading zeros
            var sec = String (date.getSeconds ());
            return (sec.length == 1 ? "0" + sec : sec);
        },        
        
        S : function () {
            // English ordinal suffix for the day of the month, 2 characters
            switch (date.getDate ()) {
                case  1: return ("st"); 
                case  2: return ("nd"); 
                case  3: return ("rd");
                case 21: return ("st"); 
                case 22: return ("nd"); 
                case 23: return ("rd");
                case 31: return ("st");
                default: return ("th");
            }
        },
        
        t : function () {
            // thanks to Matt Bannon for some much needed code-fixes here!
            var daysinmonths = [null,31,28,31,30,31,30,31,31,30,31,30,31];
            if (this.L()==1 && this.n()==2) return 29; // ~leap day
            return daysinmonths[this.n()];
        },
        
        U : function () {
            // Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
            return Math.round(date.getTime()/1000);
        },

        w : function () {
            // Numeric representation of the day of the week
            return date.getDay();
        },
        
        W : function () {
            // Weeknumber, as per ISO specification:
            // http://www.cl.cam.ac.uk/~mgk25/iso-time.html
        
            var DoW = this.N ();
            var DoY = this.z ();

            // If the day is 3 days before New Year's Eve and is Thursday or earlier,
            // it's week 1 of next year.
            var daysToNY = 364 + this.L () - DoY;
            if (daysToNY <= 2 && DoW <= (3 - daysToNY)) {
                return 1;
            }

            // If the day is within 3 days after New Year's Eve and is Friday or later,
            // it belongs to the old year.
            if (DoY <= 2 && DoW >= 5) {
                return new Date (this.Y () - 1, 11, 31).formatDate ("W");
            }
            
            var nyDoW = new Date (this.Y (), 0, 1).getDay ();
            nyDoW = nyDoW != 0 ? nyDoW - 1 : 6;

            if (nyDoW <= 3) { // First day of the year is a Thursday or earlier
                return (1 + Math.floor ((DoY + nyDoW) / 7));
            } else {  // First day of the year is a Friday or later
                return (1 + Math.floor ((DoY - (7 - nyDoW)) / 7));
            }
        },
        
        y : function () {
            // A two-digit representation of a year
            var y = String(this.Y());
            return y.substring(y.length-2,y.length);
        },        
        
        Y : function () {
            // A full numeric representation of a year, 4 digits
    
            // we first check, if getFullYear is supported. if it
            // is, we just use that. ppks code is nice, but wont
            // work with dates outside 1900-2038, or something like that
            if (date.getFullYear) {
                var newDate = new Date("January 1 2001 00:00:00 +0000");
                var x = newDate .getFullYear();
                if (x == 2001) {              
                    // i trust the method now
                    return date.getFullYear();
                }
            }
            // else, do this:
            // codes thanks to ppk:
            // http://www.xs4all.nl/~ppk/js/introdate.html
            var x = date.getYear();
            var y = x % 100;
            y += (y < 38) ? 2000 : 1900;
            return y;
        },

        
        z : function () {
            // The day of the year, zero indexed! 0 through 366
            var s = "January 1 " + this.Y() + " 00:00:00 GMT" + this.O();
            var t = new Date(s);
            var diff = date.getTime() - t.getTime();
            return Math.floor(diff/1000/60/60/24);
        },

        Z : function () {
            // Timezone offset in seconds
            return (date.getTimezoneOffset () * -60);
        }        
    
    }

    function getSwitch(str) {
        if (switches[str] != undefined) {
            return switches[str]();
        } else {
            return str;
        }
    }

    var date;
    if (time) {
        var date = new Date (time);
    } else {
        var date = this;
    }

    var formatString = input.split("");
    var i = 0;
    while (i < formatString.length) {
        if (formatString[i] == "%") {
            // this is our way of allowing users to escape stuff
            formatString.splice(i,1);
        } else {
            formatString[i] = getSwitch(formatString[i]);
        }
        i++;
    }
    
    return formatString.join("");
}

Date.parseDate = function (str){
	var t = Date.parse(str);

	if(isNaN(t)){
		var d = str.split('-');
		
		return Date.parse(d[1]+'/'+d[2]+'/'+d[0]); // m/d/y
	}
	
	return t;
}

// Some (not all) predefined format strings from PHP 5.1.1, which 
// offer standard date representations.
// See: http://www.php.net/manual/en/ref.datetime.php#datetime.constants
//

// Atom      "2005-08-15T15:52:01+00:00"
Date.DATE_ATOM    = "Y-m-d%TH:i:sP";
// ISO-8601  "2005-08-15T15:52:01+0000"
Date.DATE_ISO8601 = "Y-m-d%TH:i:sO";
// RFC 2822  "Mon, 15 Aug 2005 15:52:01 +0000"
Date.DATE_RFC2822 = "D, d M Y H:i:s O";
// W3C       "2005-08-15 15:52:01+00:00"
Date.DATE_W3C     = "Y-m-d%TH:i:sP";