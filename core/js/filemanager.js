$(function(){
	
	var el = $('#filemanager');
	var files = [];
	var path = '';
	var selected_file = '';
	var root = _basepath+_fm_root;
	var uploaderInst;
	
	//config
	var dir = window.dir || '/';
	
	// set layout height
	var onResize = function(){
		var h = $(window).height() - 75;
		$('.files', el).height(h);
		$('.files .wrap', el).height(h - $('.files .path', el).outerHeight());
		$('.info', el).height(h - 20);
	}
	
	onResize();
	
	$(window).resize(function(){
		onResize();
	});
	
	
	var renderFiles = function(){
		var ul = $('.files .wrap > ul', el);
		
		ul.html('');
		
		$.each(files, function(i, file){
			var f = $('<li class="'+file.type+'"><a href="#'+file.path+'" class="delete">&nbsp;</a><a href="#'+file.path+'" class="fname">'+file.name+'</a></li>');
			
			if(file.cdate){
				f.find('.fname').before('<span class="date">'+file.cdate+'</span>');
			}
			
			if(file.size){
				f.find('.fname').before('<span class="size">'+formatSize(file.size)+'</span>');
			}
			
			ul.append(f);
		});
	}
	
	var renderInfo = function(file){
		var info = $('.info .wrap', el);
		
		info.html('');
		
		if(file.type == 'image'){
			info.append('<div class="preview"><img src="'+root+file.path+'" alt="" /></div>');
			
		}else if(file.ext){
			info.append('<div class="preview"><span class="ext">.'+file.ext+'</span></div>');
		}
		
		info.append('<strong class="name">'+file.name+'</strong>');
		
		if(file.size){
			info.append('<table><tr><td>'+_lang.filesize+':</td><td>'+formatSize(file.size)+'</td></tr></table>');
			
			if(file.cdate){
				info.find('table').append('<tr><td>'+_lang.uploaded+':</td><td>'+file.cdate+'</td></tr>');
			}
			
			if(file.width){
				info.find('table').append('<tr><td>'+_lang.dimensions+':</td><td>'+file.width+'x'+file.height+' px</td></tr>');
			}
		}
	}
	
	var renderPath = function(){
		var parts = path.replace(/^\/|\/$/, '').split('/');
		var p = $('.files .path', el);
		
		p.html('<a href="#/">~</a>');
		
		var _p = '/';
		$.each(parts, function(i, folder){	
			_p += folder+'/';
			p.append(' / <a href="#'+_p+'">'+folder+'</a>');
		});
	}
	
	var createDir = function(dir){
		$.post(_base+'admin/filemanager/createdir', { path: path, dir: dirify(dir) }, function(response){
			loadDir(path);
		});
	}
	
	var deleteFile = function(file){
		if(confirm(_lang.confirm_file_delete+"\n"+file)){
			$.post(_base+'admin/filemanager/deletefile', { file: file }, function(){
				loadDir(path);
			});
		}
	}
	
	var loadDir = function(dir){
		var p = $('.files .path', el);
		
		p.prepend('<span class="loader"></span>');
		
		$.post(_base+'admin/filemanager/listFiles', { dir: dir }, function(response){
			path = dir;
			files = response;
			
			if(uploaderInst){
				uploaderInst.setTargetDir(path);
			}
			
			if($('#upload', el).length && typeof $('#upload', el).uploadifySettings == 'function'){
				$('#upload', el).uploadifySettings('scriptData', {'path': path});
			}
			
			renderPath();
			renderFiles();
		}, 'json');
	}
	
	var showInfo = function(path){
		$.each(files, function(i, file){
			if(file.path == path){
				renderInfo(file);
				
				selected_file = path;
				
				$('button[name="select_file"]', el).css('opacity', 1).removeClass('btndisabled');
				
				return false;
			}
		});
	}
	
	var formatSize = function(bytes){
		if(bytes >= (1024 * 1024)){
			return roundDec(bytes / (1024 * 1024), 2)+' MB';
			
		}else if(bytes >= 1024){
			return roundDec(bytes / 1024, 2)+' kB';
		}
		
		return bytes+' B'
	}
	
	var urlParam = function(name){
		var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
		return results ? (results[1] || 0) : 0;
	}
	
	loadDir(dir);
	
	$('.files li.folder a.fname, .files .path a', el).live('click', function(){
		var p = $(this).attr('href').substring($(this).attr('href').lastIndexOf('#') + 1).replace(/\/$/, '');

		loadDir(p);
		
		return false;
	});
	
	$('.files li:not(.folder) a.fname', el).live('click', function(){
		var p = $(this).attr('href').substring($(this).attr('href').lastIndexOf('#') + 1).replace(/\/$/, '');
		
		showInfo(p);
		
		return false;
	});
	
	$('.files a.delete', el).live('click', function(){
		var p = $(this).attr('href').substring($(this).attr('href').lastIndexOf('#') + 1).replace(/\/$/, '');
		
		deleteFile(p);
		
		return false;
	});
	
	$('button[name="new_folder"]', el).bind('click', function(){
		var name = prompt(_lang.enter_new_folder_name, _lang.new_folder);
		
		if(name){
			createDir(name);
		}
		
		return false;
	});
	
	$('button[name="select_file"]', el).css('opacity', 0.2).addClass('btndisabled').bind('click', function(){
		
		if(! $(this).hasClass('btndisabled')){

			if(window.opener && window.opener._fm_callback){
				window.opener._fm_callback.call(window, root+selected_file);
			
			}else if(window.opener && window.opener.CKEDITOR){
				window.opener.CKEDITOR.tools.callFunction(urlParam('CKEditorFuncNum'), root+selected_file);
			}
			
			window.close();
		}
		
		return false;
	});
	
	var btn = $('button[name="upload"]', el);
	
	btn.bind('click', function(){
		if(! uploaderInst || ! uploaderInst.el){
			uploaderInst = new uploader({
				minTop: 15,
				StateChanged: function(u){
					if(u.state == plupload.STOPPED){
						loadDir(path);
					}
				}
			}); 
			
			uploaderInst.setTargetDir(path);
		}
		
		return false;
	});
	
	onResize();
});