$(function(){

	// Bootstrap tooltip
	if($('*[data-toggle="popover"]').length){
		$('*[data-toggle="popover"]').popover({
			trigger: 'hover',
			content: function() {
				if($(this).data('content-from-element')){
					el = $($(this).data('content-from-element'));
					return el.html();
				}else{
					return $(this).data('content');
				}
			},
			html: true
		}).click(function(){
			return false;
		});
	}

	var preventDataLoss;
	
	var sessionGuard = function(){
		var timeleft = 0;
		var alert = $('#sessionalert');
		var timer_int = false;
		var alert_visible = false;
		
		var timer = function(){
			timeleft --;
			
			if(timeleft <= 0){
				window.location.href = _base+'admin/default/logout';
				return;
			}
			
			alert.find('.time').html(formatTime(timeleft));
		}
		
		var hidealert = function(){
			alert.slideUp();
			timeleft = 99;
			clearInterval(timer_int);
			alert_visible = false;
		}
		
		var showalert = function(){
			timer();
			alert.slideDown();
			timer_int = setInterval(timer, 1000);
			alert_visible = true;
			
			alert.find('a.extend').unbind('click').bind('click', function(){
				$.post(_base+'admin/default/ajax_extendSession', { });
				hidealert();
				return false;
			});
		}
		
		var checkSession = function(){
			$.post(_base+'admin/default/ajax_checkSession', { session_disable_extend: 1 }, function(data){
				if(data && data.status == 2){
					timeleft = data.time_left;
					
					if(alert_visible === false){
						showalert();
					}
				}
			}, 'json');
		}
		
		setInterval(checkSession, 2 * 60 * 1000);
	}
	
	sessionGuard();
	
	htmlEditor();
	
	$('.toggletabs').bind('click', function(){
		$(this).parents('.form:first').toggleClass('notabs');
		return false;
	});
	
	$('ul.tabs li a').bind('click', function(){
		var tab = $(this).attr('href').substring($(this).attr('href').lastIndexOf('#') + 1);
		
		$(this).parent().parent().find('li.active').removeClass('active');
		$(this).parents('.tabsWrap:first').find('.tab').hide();
		$(this).parent().addClass('active');
		
		$('#'+tab).show();

		// Nastaveni formulare
		window.location.hash = 'jq-'+tab;
        var form_action = $(this).closest('form').attr('action');
        var arr = form_action.split('#');
        var original_form_action = arr[0];
        $(this).closest('form').attr('action', original_form_action+window.location.hash);
		
		return false;
	});
	
	/*$('#mainnav > ul:first li a').bind('mouseenter', function(){
		$(this).parent().parent().find('> li.active').removeClass('active');
		$(this).parent().addClass('active');
		
		var subnav = $(this).parent().attr('class').match(/subnav-\w+/);
		
		$('#mainnav ul.subnav:visible').stop(true, true).hide();
		$('#'+subnav).fadeIn();	
		
		return false;
	});*/
	
	if(typeof _active_tab != 'undefined'){
		var activeTab = $('#mainnav > ul > li.subnav-'+_active_tab).addClass('active');
		$('#subnav-'+_active_tab).show();
	}
	
	$('input.dirify2sef_url').bind('change', function(){
		var v = dirify($(this).val());
		var inp = $('input[name="sef_url"]');
		
		if(! inp.hasClass('locked')){
			inp.val(v);
		}
	}).trigger('change');
	
	$('input[name="sef_url"]').bind('change', function(){
		$(this).val(dirify($(this).val()));
	});

	if(typeof $.fancybox != 'undefined'){
		$('a.lightbox').fancybox({
			titleShow: false
		});
		
		$('#webmex_about_link').fancybox({
			titleShow: false
		});
	}
	
	$('input.date').each(function(){
		var el = $(this);
		var d = el.val() || new Date();
		
		el.DatePicker({
			format: 'Y-m-d',
			date: d,
			starts: 1,
			position: 'bottom',
			onBeforeShow: function(){
				el.DatePickerSetDate(d, true);
			},
			onChange: function(formated, dates){
				el.val(formated).DatePickerHide();
			},
			locale: _lang.datepicker
		});
	});
	
	$('fieldset.toggleset legend').bind('click', function(){
		$(this).parent().toggleClass('toggleset-expanded');
		return false;
	});
	
	$('table.datagrid a.delete').bind('click', function(){
		return confirm(_lang.confirm_delete);	
	});
	
	$('form.form').find('input[type="text"], input[type="password"], textarea').bind('change', function(){
		preventDataLoss = true;
	});
	
	$(window).bind('beforeunload', function(){
		if(preventDataLoss){
			return _lang.unsaved_data;
		}
	});
	
	$('form.form').bind('submit', function(){
		preventDataLoss = false;
		var emptyFields = $(this).find('.required input');
		emptyFields.push($(this).find(".required select"));
		var r = [];

		emptyFields.each(function(){
			if($(this).val() == ""){
				$(this).addClass('error');
				r.push($(this));
			}else{
				$(this).removeClass('error');
			}
		});

		return r.length == 0;
	});
	
	$('#open-filemanager').bind('click', function(){
		filemanager();
		return false;
	});
	
	if(typeof _is_premium != 'undefined' && ! _is_premium){
		var premiumDialog = function(){
			$.fancybox($('#webmex_premium_wrap').html(), {
	        	autoDimensions: false,
				width: 500,
				height: 'auto'
			});
		}
			
		$('.premium-dialog').bind('click', function(){
			premiumDialog();
			return false;
		});
		
		$('.premium').addClass('premium-js').each(function(){
			$(this).find('input, textarea, select').attr('readonly', true);
			$(this).find('input[type="checkbox"]').after('<span class="premiumicon">&nbsp;</span>');
			
		}).find('input, textarea, select').bind('click', function(){
			premiumDialog();
			
		}).val('');
		
		$('#mainnav span.premium').bind('click', function(){
			premiumDialog();
		});
	}
	
	$('a.notify-close').bind('click', function(){
		var m = this.href.match(/\#(\w+)/);
		
		$.get(_base+'admin/default/ajax_removeNotification/'+m[1], {}, function(response){
			if(response && response.status){
				$('#toplinks .notify-count').html(response.count);	
				
				if(! response.count){
					$('#toplinks .notify-count').hide();
				}
			}
		}, 'json');
		
		$(this).parent().parent().fadeOut('fast');
		
		return false;
	});
});

// editor
function htmlEditor(elements, toolbar){
	
	if(! elements){
		elements = $('textarea.editor');
	}

	var ckeditor_toolbar_full = [
		['Undo','Redo'],
		['Cut','Copy','Paste','PasteText','PasteFromWord'],
		['SelectAll','RemoveFormat'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		['Preview', '-', 'Source'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['Link','Unlink','Anchor'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		'/',
		['Styles','Format','Font'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks','-','About']
	];
	
	var ckeditor_toolbar_basic = [
		['Undo','Redo'],
		['Bold','Italic','Subscript','Superscript','NumberedList','BulletedList','-','Link','Unlink','-','Image','SpecialChar','PageBreak'],
		['RemoveFormat', 'Preview', '-', 'Source']
	];
	
	elements.each(function(){	
		if(toolbar){
			var tb = toolbar;
			
		}else{
			var tb = $(this).hasClass('full') ? ckeditor_toolbar_full : ckeditor_toolbar_basic;
		}
		
		CKEDITOR.replace($(this).attr('id'), {
			customConfig: false,
	        toolbar : tb,
	        resize_enabled: true,
	        resize_dir: 'vertical',
	        resize_maxHeight: 900,
	        resize_minHeight: 100,
	        height: $(this).height(),
	        uiColor : '#cccccc',
	        bodyClass: 'ckeditor',
	        filebrowserBrowseUrl: _filemanager_url,
	        baseHref: _url,
	        contentsCss: _tpl_front+'css/editor.css',
	        stylesSet: 'mystyles:'+_tpl_front+'css/styles.js'
	    });
	});
}


_progress = function(json){
	
}
_progress_finish = function(){
	
}

progress = function(title){
	this.init(title);
}

progress.prototype = {
	
	start_time: 0,
	percent: 0,

	init: function(title){
		this.el = $('<div class="progress" />');
		this.dialog = $('<div class="dialog" />');
		this.logel = $('<div class="log"><div></div></div>');
		this.bar = $('<div class="bar"><div></div><span>0%</span></div>');
		this.time = $('<div class="time">'+_lang.elapsed_time+': <span></span> <div class="esttime" style="display:none;">'+_lang.estimated_time+': <span></span></div></div>')
		this.ifr = $('<iframe src="'+_base+'admin/default/emptysrc" frameborder="0" style="width:0;height:0;border:none;visibility:hidden;" onload="return _progress_finish();"></iframe>');
		
		this.dialog.append('<h1>'+title+'</h1>');
		this.dialog.append(this.logel);
		this.dialog.append(this.bar);
		this.dialog.append(this.time);
		this.dialog.append(this.ifr);
		
		this.el.append(this.dialog);
		
		$('body').append(this.el);
		
		var d = new Date();
  		this.start_time = Math.round(d.getTime() / 1000);
	},
	
	setProgress: function(percent){
		if(percent > 100){
			percent = 100;
		}
		
		this.percent = percent;
		
		this.bar.find('div').width(percent+'%');
		this.bar.find('span').html(percent+'%');
	},
	
	log: function(str){
		var wrap = this.logel.find('div');
		
		wrap.append(str+"<br />");
		
		wrap.animate({
			scrollTop: wrap.scrollTop() + 5000
		}, 0);
	},
	
	request: function(url, params, progress_callback, callback){
		var http_request = false;
		var self = this;
		var timer;
		var iframe = this.ifr[0];
		
		if(iframe){
			var ch = url.match(/\?/) ? '&' : '?';
			iframe.contentWindow.location.href = url+ch+$.param(params);
			
			_progress = function(response){
				var json;
				
				if(response && (json = $.parseJSON(response))){
	        		progress_callback.call(self, json);
	        	}
			}
			
			_progress_finish = function(){
				callback.call(self);
		                
                clearInterval(timer);
                self.updateTime();
			}
			
			timer = setInterval(function(){
	  			self.updateTime();
	  		}, 1000);
		}
		
	},
	
	request_ajax: function(url, params, progress_callback, callback){
		var http_request = false;
		var self = this;
		var timer;
		
  		if(window.XMLHttpRequest){
 			http_request = new XMLHttpRequest();
     		
  		}else if(window.ActiveXObject){ // IE
 			try {
    			http_request = new ActiveXObject("Msxml2.XMLHTTP");
    			
 			}catch (e){
    			try {
       				http_request = new ActiveXObject("Microsoft.XMLHTTP");
        		}catch (e){}
     		}
  		}
  		
  		if(! http_request){
     		alert('Cannot create XMLHTTP instance');
     		return false;
  		}
  		
  		http_request.onreadystatechange = function(){
   
  		}
  		
  		timer = setInterval(function(){
  			self.updateTime();
  		}, 1000);
  		
  		http_request.open('POST', url, true);
  		http_request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  		http_request.send($.param(params));
	},
	
	updateTime: function(){
		var d = new Date();
		var t = Math.round(d.getTime() / 1000);
		var diff = t - this.start_time;
		
		this.time.find('span:first').html(formatTime(diff));
		
		if(diff > 5 && this.percent >= 1){
			var estt = Math.round((diff / this.percent) * 100) - diff;
			
			this.time.find('.esttime').show();
			this.time.find('.esttime span:first').html(formatTime(estt));
		}
		
		if(this.percent == 100){
			this.time.find('.esttime').html(_lang.finished);
		}
	}
}

uploader = function(opt){
	var config = $.extend({
		minTop: 60,
		minRight: 17,
		width: 550,
		minWidth: 160,
		FileUploaded: null,
		StateChanged: null
	}, opt);
	
	var inst, watermark = 0, wm_box, self = this;
	var el = $('<div />').addClass('webmex-plupload');
	var wrap = $('<div class="wrap" />').height(258);
	var tbar = $('<div class="tbar" />');
	var bbar = $('<div class="bbar" />');
	var min = $('<div class="minimalized" />').hide();
	
	var pos = { top: 0, left: 0 };
	var height = 0;
	
	this.el = el;
	this.inst = inst;
	
	this.setTargetDir = function(dir){
		inst.settings.multipart_params.path = dir.replace('/^\//', '');
	}
	
	var center = function(){
		el.css({
			top: ($(window).height() - 315) / 2,
			left: ($(window).width() - config.width) / 2
		});
		
		pos = el.offset();
	}
	
	var minimalize = function(){
		height = wrap.height();
		
		wrap.css({
			height: 0,
			overflow: 'hidden'
		});
		tbar.hide();
		bbar.hide();
		min.show();
		el.width(config.minWidth);
		
		pos = el.offset();
		
		el.animate({
			top: config.minTop,
			left: $(window).width() - config.minRight - config.minWidth
		}, 500);
	}
	
	var maximize = function(){
		center();
		
		el.animate({
			top: pos.top,
			left: pos.left
		}, 500, null, function(){
			wrap.css({
				height: height
			});
			tbar.show();
			bbar.show();
			min.hide();
			el.width(config.width);
		});
	}
	
	var close = function(){
		var confirmed = true;
		if(inst.state == plupload.STARTED){
			confirmed = confirm(_lang.confirm_upload_stop);
		}
		
		if(confirmed){
			inst.stop();
			
			if($.browser.msie){
				el.hide();
				
			}else{
				el.remove();
			}
			
			delete self.el;
		}
	}
	
	el.css({
		width: config.width,
		height: config.height
	});
	
	min.html('<div class="bar"><span></span></div> <em>'+_lang.select_files+'</em>');
	var pbarwrap = min.find('.bar').hide();
	var pbar = min.find('.bar span');
	var status = min.find('em');
	
	el.append(tbar);
	el.append(wrap);
	el.append(bbar);
	el.append(min);
	
	el.appendTo('body');
	
	center();
	
	el.find('.wrap').pluploadQueue({
		runtimes : (_upload_driver && _upload_driver != 'auto') ? _upload_driver : 'html5,gears,silverlight,flash,html4',
		url : _base+'admin/filemanager/plupload',
		chunk_size : '4mb',
		unique_names : false,
		multipart_params: { session_id: _session_id, watermark : watermark },
		flash_swf_url : _url+'core/vendor/plupload/js/plupload.flash.swf',
		silverlight_xap_url : _url+'core/vendor/plupload/js/plupload.silverlight.xap'
	});
	
	inst = el.find('.wrap').pluploadQueue();
	
	inst.bind('UploadProgress', function(u, f){
		wm_box.attr('disabled', true);
		
		pbarwrap.show();
		pbar.css({
			width: u.total.percent+'%'
		});
		
		status.html(u.total.percent+'%');
		
		if(u.total.percent >= 100){
			pbarwrap.hide();
			status.html(_lang.upload_finished);
			
			setTimeout(function(){
				close();
			}, 15000);
		}
	});
	
	if(config.FileUploaded){
		inst.bind('FileUploaded', config.FileUploaded);
	}
	
	if(config.StateChanged){
		inst.bind('StateChanged', config.StateChanged);
	}
	
	tbar.append('<a href="#" class="btn-min">'+_lang.minimalize+'</a><a href="#" class="btn-close">'+_lang.close+'</a>');
	bbar.append('<label><input type="checkbox" name="watermark" /> '+_lang.apply_watermark+'</label>');
	
	wm_box = bbar.find('input[name="watermark"]');
	wm_box.bind('click', function(){
		watermark = $(this).is(':checked') ? 1 : 0;
		
		inst.settings.multipart_params.watermark = watermark;
	});
	
	if(! _is_premium){
		wm_box.attr('disabled', true);
	}
	
	$('a.btn-min').bind('click', function(){
		minimalize();
		return false;
	});
	
	$('a.btn-close').bind('click', function(){
		close();
		return false;
	});
	
	$('div.minimalized').bind('click', function(){
		maximize();
		return false;
	});
}

flashMsg = function(opt){
	$(function(){
		var el = $('<div id="msg" class="'+opt.type+'"><p>'+opt.msg+'</p></div>');
		
		$('#container').append(el);
		
		var l = ($(window).width() - el.outerWidth()) / 2;
		
		el.css('left', l);
		
		setTimeout(function(){
			el.fadeOut('normal', function(){
				el.remove();
				delete el;
			});
		}, 2500);
	});
}

if(typeof filemanager == 'undefined'){
	filemanager = function(opt){
		var fm = window.open(_base+'admin/filemanager', 'filemanager', 'width=940,height=600'); 
		
		window._fm_window = fm;
		window._fm_options = opt;
		window._fm_callback = (opt && opt.callback) ? opt.callback : false;
	}
}

if(typeof productfinder == 'undefined'){
	productfinder = function(opt){
		var pf = window.open(_base+'admin/products/find', 'productfinder', 'width=940,height=500'); 
		
		window._pf_callback = (opt && opt.callback) ? opt.callback : false;
	}
}

files = function(el, data, callback, simple_layout){
	el = $(el);
	var elFiles = el.find('.fileswrap', el);
	var newid = 1;
	var uploaderInst;
	var btn = $('button[name="upload"]', el);
	
	btn.bind('click', function(){
		if(! uploaderInst || ! uploaderInst.el){
			uploaderInst = new uploader({
				FileUploaded: function(u, f, r){
					var fname = f.name;
					
					if(r){
						r = $.parseJSON(r.response);
						
						if(r && r.filename){
							fname = r.filename;
						}
					}
					
					$.each(data, function(i, file){
						if(file.filename == _fm_root+fname){
							delete data[i];
						}
					});
					
					data.push({
						id: 'new_'+newid++,
						filename: _fm_root+fname,
						description: '',
						position: getMaxPosition() + 1,
						is_image: fname.match(/\.(jpe?g|png|gif)$/i) ? 1 : 0
					});
					
					render();
				}
			}); 
			
			uploaderInst.setTargetDir(_quick_upload_dir);
		}
		
		return false;
	});
	
	var size_options = '';
	$.each(_image_sizes, function(i, s){
		size_options += '<option value="'+i+'">'+s+'</option>';
	});
	
	var render = function(){
		elFiles.html('');
		
		$.each(data, function(i, file){
			var f = $('<div class="file" />');
			
			f.append('<div class="filename"><input type="checkbox" name="files_delete[]" value="'+file.id+'" /> <a href="'+file.filename+'" target="_blank" class="'+(parseInt(file.is_image) == 1 ? 'lightbox' : '')+'">'+file.filename+'</a><input type="hidden" name="files_filename['+file.id+']" value="'+file.filename+'" /></div>');
			
			if(! simple_layout && parseInt(file.is_image) == 1){
				f.append('<select class="size" name="files_size['+file.id+']"><optgroup label="'+_lang.size+'">'+size_options+'</optgroup></select>');
				
				f.find('select[name^="files_size"]').val(typeof file.size == 'undefined' ? -1 : file.size);
				
			}else{
				f.append('<span class="size">&nbsp;</span>');
			}
			
			f.append('<input type="text" name="files_description['+file.id+']" value="'+file.description+'" class="input-text desc" />');
			f.append('<input type="text" name="files_position['+file.id+']" value="'+file.position+'" class="input-text position center" />');
			
			if(! simple_layout && parseInt(file.is_image) == 1){
				f.append('<select class="align" name="files_align['+file.id+']"><optgroup label="'+_lang.align+'">'+
				'<option value="2">'+_lang.left+'</option>'+
				'<option value="3">'+_lang.right+'</option>'+
				'<option value="1">'+_lang.top+'</option>'+
				'<option value="0">'+_lang.bottom+'</option>'+
				'</optgroup></select>');
				
				f.find('select[name^="files_align"]').val(typeof file.align == 'undefined' ? 2 : parseInt(file.align));
			}
			
			elFiles.append(f);
		});	
		
		if(! data.length){
			elFiles.append('<div class="nofiles">'+_lang.no_files+'</div>');
		}
		
		$('a.lightbox', el).fancybox({
			titleShow: false
		});
		
		if(typeof callback != 'undefined'){
			callback.call(this, data);
		}
	}
	
	var findFile = function(name, value){
		var ret = false;
		
		$.each(data, function(i, file){
			if(file[name] == value){
				ret = file;
				
				return false;
			}
		});
		
		return ret;
	}
	
	var setValue = function(id, name, value){
		var ret = false;
		
		$.each(data, function(i, file){
			if(id == file.id){
				file[name] = value;
				ret = true;
				
				return false;
			}
		});
		
		return ret;
	}
	
	var getMaxPosition = function(){
		var max = 0;
		
		$.each(data, function(i, file){
			var p = parseInt(file.position, 10);
			
			if(max < p){
				max = p;
			}
		});
		
		return max;
	}
	
	$('.filemanager', el).bind('click', function(){
		var pushFile = function(file){
			if(findFile('filename', file) !== false){
				//alert(_lang.file_already_attached);
				return;
			}

			file = String(file);

			data.push({
				id: 'new_'+(newid++),
				filename: file.substr(file.indexOf(_fm_root)).replace(/^\//, ''),
				description: '',
				position: getMaxPosition() + 1,
				is_image: file.match(/\.(jpe?g|png|gif)$/i) ? 1 : 0
			});
		}
		
		filemanager({
			callback: function(file_path){
			
				if($.isArray(file_path)){
					$.each(file_path, function(i, f){
						pushFile(f);
					});
					
				}else{
					pushFile(file_path);
				}
				
				render();
			}
		});
		
		return false;
	});
	
	render();
	
	elFiles.find('input').live('change', function(){
		var id = $(this).attr('name').match(/\[([^\]]*)\]$/);
		
		if(id){
			setValue(id[1], $(this).attr('name').match(/^files_([^\[]*)/)[1], $(this).val());
		}
	});
	
	elFiles.find('select').live('change', function(){
		var id = $(this).attr('name').match(/\[([^\]]*)\]$/);
		
		if(id){
			setValue(id[1], $(this).attr('name').match(/^files_([^\[]*)/)[1], $(this).val());
		}
	});
}

function formatTime(seconds){
	var left = Math.ceil(seconds);
	var s = 0;
	var m = 0;
	var h = 0;
	var d = 0;
	
	if(left >= 86400){
		d = Math.floor(left / 86400);
		left -= d * 86400;
	}
	
	if(left >= 3600){
		h = Math.floor(left / 3600);
		left -= h * 3600;
	}
	
	if(left >= 60){
		m = Math.floor(left / 60);
		left -= m * 60;
	}
	
	s = left;
	
	return (h < 10 ? '0'+h : h)+':'+
		(m < 10 ? '0'+m : m)+':'+
		(s < 10 ? '0'+s : s);
}


//Thanks to Movable Type team
function dirify(s, fileName){
	if(typeof fileName != 'boolean'){
		fileName = false;
	}
	
	var dirify_table = {
		"\u00C0": 'A',    // A`
		"\u00E0": 'a',    // a`
		"\u00C1": 'A',    // A'
		"\u00E1": 'a',    // a'
		"\u00C2": 'A',    // A^
		"\u00E2": 'a',    // a^
		"\u0102": 'A',    // latin capital letter a with breve
		"\u0103": 'a',    // latin small letter a with breve
		"\u00C6": 'AE',   // latin capital letter AE
		"\u00E6": 'ae',   // latin small letter ae
		"\u00C5": 'A',    // latin capital letter a with ring above
		"\u00E5": 'a',    // latin small letter a with ring above
		"\u0100": 'A',    // latin capital letter a with macron
		"\u0101": 'a',    // latin small letter a with macron
		"\u0104": 'A',    // latin capital letter a with ogonek
		"\u0105": 'a',    // latin small letter a with ogonek
		"\u00C4": 'A',    // A:
		"\u00E4": 'a',    // a:
		"\u00C3": 'A',    // A~
		"\u00E3": 'a',    // a~
		"\u00C8": 'E',    // E`
		"\u00E8": 'e',    // e`
		"\u00C9": 'E',    // E'
		"\u00E9": 'e',    // e'
		"\u00CA": 'E',    // E^
		"\u00EA": 'e',    // e^
		"\u00CB": 'E',    // E:
		"\u00EB": 'e',    // e:
		"\u0112": 'E',    // latin capital letter e with macron
		"\u0113": 'e',    // latin small letter e with macron
		"\u0118": 'E',    // latin capital letter e with ogonek
		"\u0119": 'e',    // latin small letter e with ogonek
		"\u011A": 'E',    // latin capital letter e with caron
		"\u011B": 'e',    // latin small letter e with caron
		"\u0114": 'E',    // latin capital letter e with breve
		"\u0115": 'e',    // latin small letter e with breve
		"\u0116": 'E',    // latin capital letter e with dot above
		"\u0117": 'e',    // latin small letter e with dot above
		"\u00CC": 'I',    // I`
		"\u00EC": 'i',    // i`
		"\u00CD": 'I',    // I'
		"\u00ED": 'i',    // i'
		"\u00CE": 'I',    // I^
		"\u00EE": 'i',    // i^
		"\u00CF": 'I',    // I:
		"\u00EF": 'i',    // i:
		"\u012A": 'I',    // latin capital letter i with macron
		"\u012B": 'i',    // latin small letter i with macron
		"\u0128": 'I',    // latin capital letter i with tilde
		"\u0129": 'i',    // latin small letter i with tilde
		"\u012C": 'I',    // latin capital letter i with breve
		"\u012D": 'i',    // latin small letter i with breve
		"\u012E": 'I',    // latin capital letter i with ogonek
		"\u012F": 'i',    // latin small letter i with ogonek
		"\u0130": 'I',    // latin capital letter with dot above
		"\u0131": 'i',    // latin small letter dotless i
		"\u0132": 'IJ',   // latin capital ligature ij
		"\u0133": 'ij',   // latin small ligature ij
		"\u0134": 'J',    // latin capital letter j with circumflex
		"\u0135": 'j',    // latin small letter j with circumflex
		"\u0136": 'K',    // latin capital letter k with cedilla
		"\u0137": 'k',    // latin small letter k with cedilla
		"\u0138": 'k',    // latin small letter kra
		"\u0141": 'L',    // latin capital letter l with stroke
		"\u0142": 'l',    // latin small letter l with stroke
		"\u013D": 'L',    // latin capital letter l with caron
		"\u013E": 'l',    // latin small letter l with caron
		"\u0139": 'L',    // latin capital letter l with acute
		"\u013A": 'l',    // latin small letter l with acute
		"\u013B": 'L',    // latin capital letter l with cedilla
		"\u013C": 'l',    // latin small letter l with cedilla
		"\u013F": 'l',    // latin capital letter l with middle dot
		"\u0140": 'l',    // latin small letter l with middle dot
		"\u00D2": 'O',    // O`
		"\u00F2": 'o',    // o`
		"\u00D3": 'O',    // O'
		"\u00F3": 'o',    // o'
		"\u00D4": 'O',    // O^
		"\u00F4": 'o',    // o^
		"\u00D6": 'O',    // O:
		"\u00F6": 'o',    // o:
		"\u00D5": 'O',    // O~
		"\u00F5": 'o',    // o~
		"\u00D8": 'O',    // O/
		"\u00F8": 'o',    // o/
		"\u014C": 'O',    // latin capital letter o with macron
		"\u014D": 'o',    // latin small letter o with macron
		"\u0150": 'O',    // latin capital letter o with double acute
		"\u0151": 'o',    // latin small letter o with double acute
		"\u014E": 'O',    // latin capital letter o with breve
		"\u014F": 'o',    // latin small letter o with breve
		"\u0152": 'OE',   // latin capital ligature oe
		"\u0153": 'oe',   // latin small ligature oe
		"\u0154": 'R',    // latin capital letter r with acute
		"\u0155": 'r',    // latin small letter r with acute
		"\u0158": 'R',    // latin capital letter r with caron
		"\u0159": 'r',    // latin small letter r with caron
		"\u0156": 'R',    // latin capital letter r with cedilla
		"\u0157": 'r',    // latin small letter r with cedilla
		"\u00D9": 'U',    // U`
		"\u00F9": 'u',    // u`
		"\u00DA": 'U',    // U'
		"\u00FA": 'u',    // u'
		"\u00DB": 'U',    // U^
		"\u00FB": 'u',    // u^
		"\u00DC": 'U',    // U:
		"\u00FC": 'u',    // u:
		"\u016A": 'U',    // latin capital letter u with macron
		"\u016B": 'u',    // latin small letter u with macron
		"\u016E": 'U',    // latin capital letter u with ring above
		"\u016F": 'u',    // latin small letter u with ring above
		"\u0170": 'U',    // latin capital letter u with double acute
		"\u0171": 'u',    // latin small letter u with double acute
		"\u016C": 'U',    // latin capital letter u with breve
		"\u016D": 'u',    // latin small letter u with breve
		"\u0168": 'U',    // latin capital letter u with tilde
		"\u0169": 'u',    // latin small letter u with tilde
		"\u0172": 'U',    // latin capital letter u with ogonek
		"\u0173": 'u',    // latin small letter u with ogonek
		"\u00C7": 'C',    // ,C
		"\u00E7": 'c',    // ,c
		"\u0106": 'C',    // latin capital letter c with acute
		"\u0107": 'c',    // latin small letter c with acute
		"\u010C": 'C',    // latin capital letter c with caron
		"\u010D": 'c',    // latin small letter c with caron
		"\u0108": 'C',    // latin capital letter c with circumflex
		"\u0109": 'c',    // latin small letter c with circumflex
		"\u010A": 'C',    // latin capital letter c with dot above
		"\u010B": 'c',    // latin small letter c with dot above
		"\u010E": 'D',    // latin capital letter d with caron
		"\u010F": 'd',    // latin small letter d with caron
		"\u0110": 'D',    // latin capital letter d with stroke
		"\u0111": 'd',    // latin small letter d with stroke
		"\u00D1": 'N',    // N~
		"\u00F1": 'n',    // n~
		"\u0143": 'N',    // latin capital letter n with acute
		"\u0144": 'n',    // latin small letter n with acute
		"\u0147": 'N',    // latin capital letter n with caron
		"\u0148": 'n',    // latin small letter n with caron
		"\u0145": 'N',    // latin capital letter n with cedilla
		"\u0146": 'n',    // latin small letter n with cedilla
		"\u0149": 'n',    // latin small letter n preceded by apostrophe
		"\u014A": 'N',    // latin capital letter eng
		"\u014B": 'n',    // latin small letter eng
		"\u00DF": 'ss',   // double-s
		"\u015A": 'S',    // latin capital letter s with acute
		"\u015B": 's',    // latin small letter s with acute
		"\u0160": 'S',    // latin capital letter s with caron
		"\u0161": 's',    // latin small letter s with caron
		"\u015E": 'S',    // latin capital letter s with cedilla
		"\u015F": 's',    // latin small letter s with cedilla
		"\u015C": 'S',    // latin capital letter s with circumflex
		"\u015D": 's',    // latin small letter s with circumflex
		"\u0218": 'S',    // latin capital letter s with comma below
		"\u0219": 's',    // latin small letter s with comma below
		"\u0164": 'T',    // latin capital letter t with caron
		"\u0165": 't',    // latin small letter t with caron
		"\u0162": 'T',    // latin capital letter t with cedilla
		"\u0163": 't',    // latin small letter t with cedilla
		"\u0166": 'T',    // latin capital letter t with stroke
		"\u0167": 't',    // latin small letter t with stroke
		"\u021A": 'T',    // latin capital letter t with comma below
		"\u021B": 't',    // latin small letter t with comma below
		"\u0192": 'f',    // latin small letter f with hook
		"\u011C": 'G',    // latin capital letter g with circumflex
		"\u011D": 'g',    // latin small letter g with circumflex
		"\u011E": 'G',    // latin capital letter g with breve
		"\u011F": 'g',    // latin small letter g with breve
		"\u0120": 'G',    // latin capital letter g with dot above
		"\u0121": 'g',    // latin small letter g with dot above
		"\u0122": 'G',    // latin capital letter g with cedilla
		"\u0123": 'g',    // latin small letter g with cedilla
		"\u0124": 'H',    // latin capital letter h with circumflex
		"\u0125": 'h',    // latin small letter h with circumflex
		"\u0126": 'H',    // latin capital letter h with stroke
		"\u0127": 'h',    // latin small letter h with stroke
		"\u0174": 'W',    // latin capital letter w with circumflex
		"\u0175": 'w',    // latin small letter w with circumflex
		"\u00DD": 'Y',    // latin capital letter y with acute
		"\u00FD": 'y',    // latin small letter y with acute
		"\u0178": 'Y',    // latin capital letter y with diaeresis
		"\u00FF": 'y',    // latin small letter y with diaeresis
		"\u0176": 'Y',    // latin capital letter y with circumflex
		"\u0177": 'y',    // latin small letter y with circumflex
		"\u017D": 'Z',    // latin capital letter z with caron
		"\u017E": 'z',    // latin small letter z with caron
		"\u017B": 'Z',    // latin capital letter z with dot above
		"\u017C": 'z',    // latin small letter z with dot above
		"\u0179": 'Z',    // latin capital letter z with acute
		"\u017A": 'z'     // latin small letter z with acute
	};
 s = s.replace(/<[^>]+>/g, '');
 for (var p in dirify_table)
     if (s.indexOf(p) != -1)
         s = s.replace(new RegExp(p, "g"), dirify_table[p]);
         
 if(! fileName) s = s.toLowerCase();
 
 s = s.replace(/&[^;\s]+;/g, '');
 s = s.replace(/[^-a-zA-Z0-9_ \.]/g, '');
 s = s.replace(/\s+/g, '-');
 s = s.replace(/[_|-]+$/, '');
 s = s.replace(/_+/g, '-');
 s = s.replace(/_-_/g, '-');
 if(fileName){
 	s = s.replace(/[^a-zA-Z0-9-\.]/ig,'-');
 }else{
 	s = s.replace(/[^a-zA-Z0-9-]/ig,'-');
 }
 s = s.replace(/-{2,}/ig,'-');
 s = s.replace(/^-+/ig,'');
 s = s.replace(/-+$/ig,'');
 return s;
}

var _dialog_zindex = 999;
var _activeDialogs = 0;

var Dialog = function(config){

	this.config = {
		scope: this,
		width: 600,
		height: 400,
		title: '',
		content: '',
		url: null,
		buttons: null,
		callback: null,
		scrollable: false,
		cls: null,
		closable: true,
		modal: true,
		draggable: false
	};
	
	$.extend(this.config, config);
	
	this.init();
	
	return this;
}

Dialog.prototype = {

	tpl: 	'<div class="xdialog">'+
				'<div class="wrap">'+
					'<h2><a href="#" class="btnClose">'+__('close')+'</a><span class="title">{title}</span></h2>'+
					'<form class="form content">'+
						'<div class="contentWrap">{content}</div>'+
						'<div class="dbuttons buttons"></div>'+
					'</form>'+
				'</div>'+
			'</div>',
			
	onResize: null,
	
	onScroll: null,
	
	onBodyClick: null,
	
	init: function(){
		
		this.onResize = $.proxy(function(){
			this.position();
		}, this);
		
		this.onScroll = $.proxy(function(){
			this.position();
		}, this);
		
		this.onBodyClick = $.proxy(function(e){
			var target = $(e.target);
			
			if(!target.hasClass('.dialog') && target.parents('.dialog').length == 0){
				this.blink();
				e.preventDefault();
				return false;
			}
		}, this);
	
		var attrs = this.config;
		
		this.el = $(this.buildTpl(attrs));
		
		if(typeof attrs.content == 'object'){
			this.el.find('.contentWrap').html(attrs.content);
		}
		
		if(! attrs.closable){
			this.el.find('h2 a.btnClose').hide();
		}
		
		if(attrs.scrollable){
			this.el.find('.contentWrap').height(attrs.height).addClass('scrollable');
		}
		
		window.currentDialog = this;
		
		if(this.config.buttons){
			$.each(this.config.buttons, $.proxy(function(i, button){
				if(button.simple){
					var btn = $('<span class="textbutton">'+__('or')+' <a href="#">'+button.text+'</a></span>');
				}else{
					var btn = $('<input type="submit" class="button" name="'+(button.name || '')+'" value="'+button.text+'" />');
				}
				
				if(typeof button.handler == 'function'){
					btn.bind('click', $.proxy(button.handler, this));
				}
				
				if(button.disabled){
					btn.addClass('button-disabled');
				}
				
				this.el.find('.buttons').append(btn);
			}, this));
			
		}else{
			this.el.find('.dbuttons').hide();
			this.el.find('.wrap').addClass('no-buttons');
		}
		
		this.el.find('.content, .mainIframe').css({
			width: this.config.width,
			height: this.config.height
		});
		
		this.el.css({
			'z-index': _dialog_zindex + 1
		});
		
		if(this.config.modal){
			this.overlay = $('<div class="pageOverlay'+(attrs.darkOverlay ? ' darkOverlay' : '')+'"></div>');
			
			this.overlay.css({
				'z-index': _dialog_zindex
			});
			
			$('body').append(this.overlay);
			
			$('body').bind('click', this.onBodyClick);
		}
		
		_dialog_zindex += 2;
		
		$('body').append(this.el);
		
		if(this.config.draggable){
			this.el.draggable({
				containment: 'document',
				handle: '.wrap > h2:first',
				distance: 20
			});
			this.el.addClass('dialogDraggable');
		}
		
		this.el.find('.btnClose').bind('click', $.proxy(function(){
			this.close();
			return false;
		}, this));
		
		this.position();
		
		if(! this.config.draggable){
			$(window).bind('resize', this.onResize);
			$(window).bind('scroll', this.onScroll);
		}
		
		_activeDialogs ++;
	},
	
	buildTpl: function(attrs){
		var s = this.tpl;
		
		if(this.config.url){
			this.config.content = '<iframe src="'+this.config.url+'" border="0" frameborde="0" class="mainIframe"></iframe>';
		}
		
		$.each(attrs, function(k, v){
			if(v && typeof v != 'object'){
				s = s.replace(RegExp('\{'+k+'\}', 'g'), v);
			}
		});
		
		return s;
	},
	
	setTitle: function(title){
		this.el.find('.title').html(title);
		
		return this;
	},
	
	close: function(buttonId){
		var r = true;
		
		if(typeof this.config.callback == 'function'){
			r = this.config.callback.call(this.config.scope, buttonId || 'close');
			
			if(typeof r != 'boolean'){
				r = true;
			}
		}
		
		if(r){
			$(window).unbind('resize', this.onResize);
			$(window).unbind('scroll', this.onScroll);
			$('body').unbind('click', this.onBodyClick);
			
			if(this.overlay){
				this.overlay.remove();
				delete this.overlay;
			}
			this.el.remove();
			delete this.el;
			
			_activeDialogs --;
		}
		return this;
	},
	
	blink: function(){
		var changeBg = function(el, i){
			if(i % 2 === 0){
				el.addClass('dialog-blink');
			}else{
				el.removeClass('dialog-blink');
			}
			
			if(i && i >= 1){
				return;
			}
			
			setTimeout(function(){
				changeBg(el, i+1);
			}, 50);
		}
		
		changeBg(this.el, 0);
	},
	
	position: function(){
		var window = this.windowSize();
		var width = this.el.width();
		var height = this.el.height();
		var left = width > window[0] ? 0 : (Math.round((window[0] - width) / 2) + window[2]);
		var top = height > window[1] ? 0 : (Math.round((window[1] - height) / 2) + window[3]);
		
		this.el.css({
			top: top,
			left: left
		});
	},
	
	windowSize: function(){
		return [ $(window).width(), $(window).height(), $(document).scrollLeft(), $(document).scrollTop() ];	
	}
	
}


var Alert = function(title, msg, cb){
	var d = new Dialog({
		title: title,
		content: '<div class="alert">'+msg+'</div>',
		callback: cb,
		width: 400,
		height: 70,
		buttons: [{
			text: __('ok'),
			handler: function(){
				d.close();
				return false;
			}
		}]
	});
}

var Confirm = function(title, msg, cb, buttons){
	var d = new Dialog({
		title: title,
		content: '<div class="confirm">'+msg+'</div>',
		callback: cb,
		width: 400,
		height: 70,
		buttons: buttons || [{
			text: __('yes'),
			handler: function(){
				d.close('yes');
				return false;
			}
		}, {
			text: __('no'),
			handler: function(){
				d.close('no');
				return false;
			}
		}]
	});
}

var TabDialog = function(config){
	var hd = '';
	var bd = $('<div />');
	
	config = $.extend({
		title: '',
		callback: function(){},
		buttons: null
	}, config);
	
	$.each(config.tabs, function(i, tab){
		hd += '<li class="tab"><a href="#'+i+'">'+tab.title+'</a></li>';
		bd.append($('<div class="tabContent tab-'+i+' '+(i === config.activeTab ? ' active' : '')+'" />').append(tab.html));
	});

	var dialog = new Dialog({
		title: config.title, 
		content: $('<div class="tabs-wrap"><ul class="tabs">'+hd+'</ul></div>').append(bd),
		buttons: config.buttons,
		callback: config.callback,
		width: config.width || 400,
		height: config.height || 250,
		scrollable: config.scrollable || false,
		cls: 'content-tabs'
	});
	
	var el = dialog.el;
	
	$('.tab a', el).bind('click', function(){
		var index = parseInt($(this).attr('href').substring(1));
		$('.tabContent', el).removeClass('active');
		$('.tab-'+index, el).addClass('active');
		
		$('.tab', el).removeClass('active');
		$(this).parent().addClass('active');
		
		if(! $('.tab-'+index, el).hasClass('binded')){
			$('.tab-'+index, el).addClass('binded');
		}
		
		return false;
	});
	
	$('.tab a[href="#'+config.activeTab+'"]').trigger('click');
	
	return dialog;
}

jQuery(document).ready(function($) {
	// Javascript to enable link to tab
	var hash = document.location.hash;
	var prefix = "jq-";
	if (hash) {
		$('ul.tabs li a[href='+hash.replace(prefix,"")+']').trigger('click');
	}
});
