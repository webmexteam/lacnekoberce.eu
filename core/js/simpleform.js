var SimpleForm = function(config){
	this.config = $.extend({
		action: '#',
		method: 'post',
		cls: ''
	}, config);
	
	this.el = null;
	
	this.next_id = 0;
	
	this.content = '';
	
	this.init();
	
	return this;
}

SimpleForm.prototype = {

	init: function(){
		
	},
	
	build: function(){
	
		$.extend(this.config, {
			content: this.content
		})
		
		this.el = $(this.template.apply('form', this.config));
		
		return this.el;
	},

	template: new Template({
		
		form: 	'<fieldset class="simpleForm">{content}</fieldset>',
				
		box: 	'<div id="box-{id}"[if values.cls] class="{cls}"[endif][if values.style] style="{style}"[endif]>{html}</div>',
		
		hidden: 	'<input type="hidden" name="{name}" value="{value}" />',
		
		textbox: 	'<div class="inputwrap" id="input-{id}">'+
						'[if values.label]<label>{label}</label>[endif]'+
							'<input type="text" name="{name}" value="{value}" maxlength="{maxlength}" autocomplete="{autocomplete}" />'+
					'</div>',
					
		textarea: 	'<div class="input" id="input-{id}" style="[if values.height]height:{height};[endif]">'+
						'[if values.label]<label>{label}</label>[endif]'+
							'<textarea name="{name}" rows="{rows}">{value}</textarea>'+
					'</div>',
					
		select: 	'<div class="input select" id="input-{id}">'+
						'[if values.label]<label>{label}</label>[endif]'+
						'<select name="{name}">'+
							'{list}'+
						'</select>'+
					'</div>',
					
		radiogroup:	'<div class="input clearfix" id="input-{id}">'+
						'[if values.label]<label>{label}</label>[endif]'+
						'<div class="radiogroup radiogroup-align-{radioAlign}">'+
							'{list}'+
						'</div>'+
					'</div>',
					
		checkbox_old: 	'<div class="input" id="input-{id}">'+
						'[if values.label]<label>{label}</label>[endif]'+
						'<div class="checkbox">'+
							'<label><input type="checkbox" name="{name}" value="1"[if values.checked] checked="checked"[endif] /> {boxLabel}</label>'+
						'</div>'+
					'</div>',
					
		checkbox: 	'<div class="checkbox-wrap"><div class="checkbox" id="input-{id}">'+
						'<label><input type="checkbox" name="{name}" value="1"[if values.checked] checked="checked"[endif] /> {boxLabel}</label>'+
					'</div></div>',
					
		column:	'<div class="column" id="column-{id}" style="width:{width};{style}">{html}</div>'
		
	}),
	
	getId: function(){
		this.next_id ++;
		return 'simpleForm'+this.next_id;
	},
	
	buildItem: function(config){
		switch(config.type){
		
			case 'textbox':
				var inp = this.textbox(config);
			break;
			
			case 'hidden':
				var inp = this.hidden(config);
			break;
			
			case 'textarea':
				var inp = this.textarea(config);
			break;
			
			case 'select':
			case 'combo':
				var inp = this.select(config);
			break;
			
			case 'box':
				var inp = this.box(config);
			break;
			
			case 'radiogroup':
				var inp = this.radiogroup(config);
			break;
			
			case 'checkbox':
				var inp = this.checkbox(config);
			break;
			
			case 'column':
				var inp = this.column(config);
			break;
			
			case 'hline':
				var inp = this.hline();
			break;
			
		}
		
		return inp;
	},
	
	add: function(config){
		
		this.content += this.buildItem(config);
		
		return this;
	},
	
	getValues: function(){
		var values = {};
		
		$(this.el).find('input, select, textarea').each(function(i, input){
			input = $(input);
			values[input.attr('name')] = input.val();
		});
		
		return values;
	},
	
	setValues: function(values){
		var el = $(this.el);
		
		$.each(values, function(name, val){
			var inp = $('[name="'+name+'"]', el);
			
			if(inp){
				inp.val(val);
				inp.trigger('change');
			}
		});
		
		return this;
	},
	
	hidden: function(config){
		config = $.extend({
			id: this.getId(),
			name: '',
			value: ''
		}, config);
		
		return this.template.apply('hidden', config);
	},
	
	textbox: function(config){
		config = $.extend({
			id: this.getId(),
			label: '',
			name: '',
			value: '',
			maxlength: 128,
			autocomplete: 'off' // on|off
		}, config);
		
		return this.template.apply('textbox', config);
	},
	
	textarea: function(config){
		config = $.extend({
			id: this.getId(),
			label: '',
			name: '',
			value: '',
			rows: 5,
			height: null
		}, config);
		
		return this.template.apply('textarea', config);
	},
	
	select: function(config){
		config = $.extend({
			id: this.getId(),
			label: '',
			name: '',
			value: '',
			options: []
		}, config);
		
		config.list = '';
		$.each(config.options, function(i, option){
			if(typeof option != 'object'){
				option = [option, option];
			}
			
			config.list += '<option value="'+option[0]+'"'+(config.value == option[0] ? ' selected="selected"' : '')+'>'+option[1]+'</option>';
		});
		
		return this.template.apply('select', config);
	},
	
	column: function(config){
		var scope = this;
		
		config = $.extend({
			id: this.getId(),
			width: 'auto',
			style: '',
			items: []
		}, config);
		
		config.html = '';
		$.each(config.items, function(i, item){
			config.html += scope.buildItem(item);
		});
		
		return this.template.apply('column', config);
	},
	
	box: function(config){
		config = $.extend({
			id: this.getId(),
			html: '',
			style: '',
		}, config);
		
		return this.template.apply('box', config);
	},
	
	hline: function(){
		return '<div class="hline"></div>';
	},
	
	checkbox: function(config){
		config = $.extend({
			id: this.getId(),
			label: '',
			name: '',
			checked: false,
			boxLabel: ''
		}, config);
		
		return this.template.apply('checkbox', config);
	},
	
	radiogroup: function(config){
		config = $.extend({
			id: this.getId(),
			label: '',
			name: '',
			radioAlign: 'left',
			options: []
		}, config);
		
		config.list = '';
		$.each(config.options, function(i, option){
			config.list += '<label><input type="radio" name="'+config.name+'" value="'+option[0]+'"'+(option[2] ? ' checked="checked"' : '')+' /> '+option[1]+'</label>';
		});
		
		return this.template.apply('radiogroup', config);
	}
	
}