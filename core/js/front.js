$(function(){
	$('a.lightbox').fancybox({
		titlePosition: 'over',
		padding: 0
	});
	
	// fontsize
	var setFontsize = function(size){
		$('body').removeClass('fontsize-small fontsize-big');
		
		if(size == 'small'){
			$('body').addClass('fontsize-small');
			
		}else if(size == 'big'){
			$('body').addClass('fontsize-big');	
		}
	}
	
	$('a.fontsize').bind('click', function(){
		var size = this.href.match(/#(\w+)$/)[1];
		setFontsize(size);
		$.cookie('fonsize', size, {expires: 7});
		
		return false;
	});
	
	var fontsize = $.cookie('fonsize');
	if(fontsize){
		setFontsize(fontsize);
	}

	// ajax basket recalculation
	if($('button[name="update_qty"]').length)
		$('button[name="update_qty"]').hide();


	var timer;
	$('.ajax-qty').bind('keyup change', function(){

		$(this).closest('tr').find('.totaltd .ajax').show();
		$(this).closest('tr').find('.totaltd .total').hide();

		clearInterval(timer);  //clear any interval on key up
		timer = setTimeout(function() { //then give it a second to see if the user is finished
			$.post(_base+'ajax/recalculate_basket', {
				data: $('div.basket form').serialize()
			}, function(response){
				if($('.webmex-basket-free-shipping-module').length){
					location.reload();
				}

				var data = $.parseJSON(response);

				$.each(data.products, function(){
					var tr = $('.basket .tablewrap table tr[data-productid="'+this.id+'"]');
					tr.find('.total').show().html(this.total);
					tr.find('.ajax').hide();
					tr.find('.ajax-qty').val(this.qty);
				});

				if(data.subtotal){
					$('.basket .tablewrap table tr.subtotal:nth-child(1) td.value').html(data.subtotal);
				}

				if(data.voucher){
					$('.basket .tablewrap table tr.subtotal:nth-child(2) td.value').html(data.voucher);
				}

				$('.basket .tablewrap table tr.total td.value').html(data.total);
			});
		}, 600);
	});
	
	// order
	if(typeof delivery != 'undefined'){
		var estPrice = function(str, t){
			var matches = String(str).match(/^([\+\-]?[\d\.\,]+)\%$/);
			
			if(matches){
				return t / 100 * parseFloatNum(matches[1]);
			}
			
			return str ? parseFloatNum(str) : 0;
		}
		
		var recalcPrice = function(delivery_payment){
			if(typeof _subtotal != 'undefined'){
				var total = parseFloatNum(_subtotal) + price_vat(parseFloatNum(delivery_payment), _vat_delivery);
				var total_excl_vat = parseFloat(_subtotal_excl_vat) + parseFloatNum(price_unvat(delivery_payment, _vat_delivery));
				console.log(total);

				if(typeof _voucher_value != 'undefined'){
					var discount = estPrice(_voucher_value, total);
					$('#discount').html('-'+displayPrice(discount));
					total -= discount;
					total_excl_vat -= discount;
					
					if(total < 0){
						total = 0;
					}
					if(total_excl_vat < 0) {
						total_excl_vat = 0;
					}
				}

				total = Math.round(total * Math.pow(10, _order_round_decimals)) / Math.pow(10, _order_round_decimals);
				total_excl_vat = Math.round(total_excl_vat * Math.pow(10, _order_round_decimals)) / Math.pow(10, _order_round_decimals);

				$('#total').html(displayPrice(total));
				$('#subtotal').html(displayPrice(total_excl_vat));

				if($('input[name="total_price"]').length) {
					$('input[name="total_price"]').val(displayPrice(total));
				}
			}
		}
		
		var enablePayments = function(delivery_id){
			$('.order .payment input[type=radio]').removeAttr('checked').attr('disabled', true).parent().css('opacity', 0.5).parent().find('.price').hide();
			
			if(delivery_id){
				$('#payment_help').hide();
			}
			
			$.each(delivery_payments, function(i, dp){
				var price = estPrice(delivery[delivery_id], _subtotal_excl_vat) + estPrice(payment[dp.payment_id], _subtotal_excl_vat) + estPrice(dp.price, _subtotal_excl_vat);
				
				if(dp.free_over && parseFloatNum(dp.free_over) <= _subtotal){
					price = 0; //  free delivery
				}
				
				if(dp.delivery_id == delivery_id){
					var el = $('#payment_'+dp.payment_id);
					
					el.removeAttr('disabled').data('price', price);
					el.parent().css('opacity', 1).parent().find('.price').html(price ? displayPrice(price_vat(price, _vat_delivery)) : _lang.free_delivery).show();
				}
			});
		}
		
		enablePayments();
		
		$('.order .delivery input[type=radio]').bind('change', function(){
			$('#deliverypayment').html('&mdash;');
			enablePayments(this.value);
			$('.nazev-dopravy').html($(this).parent().find('label strong').html());
			$('.cena-dopravy').html('0,00 Kč');
			recalcPrice(0);
		});
		
		$('.order .payment input[type=radio]').bind('change', function(){
			var price = $(this).data('price');
			
			$('#deliverypayment').html(displayPrice(price_vat(price, _vat_delivery)));
			recalcPrice(price);
			$('.nazev-platby').html($(this).parent().find('label strong').html());
			$('.cena-platby').html(displayPrice(price_vat(price, _vat_delivery)));
			if($('input[name="dp_price"]').length) {
				$('input[name="dp_price"]').val(displayPrice(price_vat(price, _vat_delivery)));
			}
		});
	}
	
	$('form.order, form.customer').bind('submit', function(){
		$(this).find('label.error').removeClass('error');

		var error = false;
		$(this).find('.required input:not(:disabled)').each(function(){
			if($(this).val() == ""){
				$(this).parent().find('label').addClass('error');
				error = true;
			}
		});
		if(error){
			alert(_lang.fill_in_required_fields);
			return false;
		}
		
		//var emptyfields = $(this).find('.required input[value=""]:not(:disabled)');
		
		/*if(emptyfields.length){
			emptyfields.parent().find('label').addClass('error');
			alert(_lang.fill_in_required_fields);
			return false;
		}*/
		
		var emailrx = new RegExp(/^((\w+\+*\-*)+\.?)+@((\w+\+*\-*)+\.?)*[\w-]+\.[a-z]{2,6}$/i);
		
		if($(this).find('input[name="email"]').length && ! emailrx.test($(this).find('input[name="email"]').val())){
			alert(_lang.enter_valid_email);
			return false;
		}
		
		if($(this).hasClass('order') && $(this).find('input[name="delivery"]').length && (! $(this).find('input[name="delivery"]:checked').length || ! $(this).find('input[name="payment"]:checked').length)){
			alert(_lang.select_delivery_payment);
			return false;
		}
		
		if($(this).hasClass('order') && $(this).find('input[name="terms"]:not(:checked)').length){
			alert(_lang.must_accept_terms);
			return false;
		}
		
	});
	
	$('#shipping_as_billing').bind('change', function(){
		if(this.checked){
			$('.jina-dodaci-wrap').hide().find('input, select').attr('disabled', true);
		}else{
			$('.jina-dodaci-wrap').show().find('input, select').removeAttr('disabled');
		}
		
	}).bind('click', function(){
		$(this).trigger('change');
		
	}).trigger('change');
	
	$('a.lost-password').bind('click', function(){
		$(this).parent().parent().find('.lost-password-form').fadeIn();
		return false;
	});
	
	$('.producers a.toggle-producers').bind('click', function(){
		var el = $(this).parent().find('.wrap');
		
		if(el.is(':visible')){
			el.slideUp('fast');
			
		}else{
			el.slideDown('fast');
		}
		
		return false;
	});
	
	$('.producers input[type="checkbox"]').bind('click', function(){
		var wrap = $(this).parent().parent().parent();
		var all = wrap.find('input[name=""]');
		
		if(this.name){
			all.removeAttr('checked');
			
		}else{
			$('input[type="checkbox"][name!=""]', wrap).removeAttr('checked');
		}
		
		if(! $('input[type="checkbox"]:checked', wrap).length){
			all.attr('checked', true);
		}
	});
	
	// attribute images
	var swapImage = function(file_id){
		var li = $('.fid-'+file_id);
		
		if(li.length){
			var wrap = li.parents('.files:first');
			var li_current = wrap.find('.picture:first');
			var li_clone = li.clone();
			var li_current_clone = li_current.clone();
			
			if(! li_current || li_current[0] == li[0]){
				return;
			}
			
			if(li_clone.hasClass('smallpic')){
				li_current_clone.addClass('smallpic');
				li_clone.removeClass('smallpic');
			}
			
			var size_match = li_clone.find('img').attr('src').match(/\/_(\d+x\d+)\//);
			
			if(size_match){
				li_current_clone.find('img').attr('src', li_current_clone.find('img').attr('src').replace(/\/_(\d+x\d+)\//, size_match[0]));
			}
			
			size_match = li_current.find('img').attr('src').match(/\/_(\d+x\d+)\//);
			
			if(size_match){
				li_clone.find('img').attr('src', li_clone.find('img').attr('src').replace(/\/_(\d+x\d+)\//, size_match[0]));
			}
			
			li_clone.hide();
			
			li.replaceWith(li_current_clone);
			li_current.replaceWith(li_clone);
			
			li_clone.fadeIn();
			
			$('a.lightbox').fancybox({
				titlePosition: 'over'
			});
		}
	}
	
	var showPrice = function(){
		var total = parseFloat(_product_price);
		var total_no_discount = parseFloat(_product_price_before_discount);
        
        if(_product_price_transport > 0) {
            total += _product_price_transport;
            $('.result .transport_fee').remove();
            $('.result').append('<span class="transport_fee">včetně transportní návinky + '+displayPrice(_product_price_transport)+'</span>');            
        }
		
		$('input[name^="attributes["]:checked, select[name="attributes[]"]').each(function(){
			var attr_id = this.value;
			
			if(product_attr_prices[attr_id]){
				total += product_attr_prices[attr_id];
				total_no_discount += product_attr_prices[attr_id];
			}
		});

		$('#product-price').html(displayPrice(price_vat(total, _product_vat)));
		$('#product-price-excl-vat').html(displayPrice(price_unvat(total, _product_vat)));
		
		if(_product_discount){
			$('#product-old-price').html(displayPrice(price_vat(total_no_discount, _product_vat)));
		}
	}
	
	$('input[name^="attributes["]').bind('click', function(){
		var attr_id = this.value;
		
		if(product_attr_files[attr_id]){
			swapImage(product_attr_files[attr_id]);
		}
		
		showPrice();
		
	}).filter(':checked').trigger('click');
	
	$('select[name="attributes[]"]').bind('change', function(){
		var attr_id = this.value;
		
		if(product_attr_files[attr_id]){
			swapImage(product_attr_files[attr_id]);
		}
		
		showPrice();
		
	}).trigger('change');
	
});

var product_attr_files = {};
var product_attr_prices = {};

// attribute images
function attrFiles(attr_id, file_id)
{
	product_attr_files[attr_id] = file_id;
}

// attribute prices
function attrPrices(attr_id, price)
{
	product_attr_prices[attr_id] = price;
}