<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Moneybookers extends Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->content = tpl('default_1columns.php');

		if (Core::$uri != 'moneybookers/check') {
			Moneybookers::setup();
		}
	}

	public function process_payment($hash)
	{
		if ($hash) {
			if ($parts = explode('_', $hash)) {
				if (($order = Core::$db->order[(int) $parts[0]]) && md5($order['id'] . $order['received'] . $order['email']) == $parts[1]) {

					if (!(int) $order['payment_realized']) {
						Moneybookers::pay($order);
					} else {
						redirect('moneybookers/fail');
					}
				}
			}
		}
	}

	public function callback()
	{
		Moneybookers::callback();
	}

	public function success($channel = null)
	{
		$order_id = 0;

		if (!isSet($_SESSION['moneybookers']) || empty($_SESSION['moneybookers']['sid'])) {
			redirect('moneybookers/error');
		}

		$order_id = $_SESSION['moneybookers']['order_id'];

		if (!$order_id || !($order = Core::$db->order[$order_id])) {
			redirect('moneybookers/error');
		}

		$_SESSION['order_id'] = $order['id'];

		$this->content->content = tpl('moneybookers/success.php', array(
			'order' => $order
				));
	}

	public function fail()
	{
		$order_id = 0;
		$order = $link = null;

		$order_id = $_SESSION['moneybookers']['order_id'];

		if (!$order_id || !($order = Core::$db->order[$order_id])) {
			redirect('moneybookers/error');
		}

		if ($order) {
			$link = url('moneybookers/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
			$_SESSION['order_id'] = $order['id'];
		}

		$this->content->content = tpl('moneybookers/fail.php', array(
			'order' => $order,
			'link' => $link
				));
	}

	public function error()
	{
		$order_id = 0;
		$order = $link = null;

		$order_id = $_SESSION['moneybookers']['order_id'];

		if ($order_id) {
			$order = Core::$db->order[$order_id];
		}

		if ($order) {
			$link = url('moneybookers/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
			$_SESSION['order_id'] = $order['id'];
		}

		$this->content->content = tpl('moneybookers/error.php', array(
			'order' => $order,
			'link' => $link
				));
	}

}