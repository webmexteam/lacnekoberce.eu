<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Files extends Controller
{

	public function __construct()
	{
		$this->tpl = null;
	}

	public function index()
	{
		$args = func_get_args();

		$path = join('/', $args);

		if (!empty($_GET['f'])) {
			Core::show_404();
			return;
		}

		if ($path) {
			$filepath = trim($path, ' /');
			$thumb = null;

			if (preg_match('#(/|\A)_(\d+x\d+)/#', $filepath, $m)) {
				$thumb = trim($m[1]);
				$filepath = trim(preg_replace('#(/|\A)_\d+x\d+/#', '/', $filepath), ' /');
			}

			if ($file = Core::$db->static_file()->where('localname', $filepath)->limit(1)->fetch()) {
				$local = DOCROOT . 'files/' . $file['localname'];
				$exists = false;

				if ($file['mimetype']) {
					header("Content-Type: " . $file['mimetype']);
				}

				if ($file['content'] !== null) {
					echo $file['content'];
					exit;
				} else if (file_exists($local)) {
					$fp = fopen($local, 'r');

					if ($fp) {
						fpassthru($fp);
					}

					exit;
				} else if ($file['url']) {
					$info = pathinfo($local);

					if (!is_dir($info['dirname'])) {
						FS::mkdir($info['dirname'], true);
					}

					$is_image = in_array(strtolower($info['extension']), array('jpg', 'jpeg', 'gif', 'png'));

					if (@copy($file['url'], $local)) {
						FS::writable($local);

						if ($is_image) {
							try {
								Image::thumbs($local);
							} catch (Exception $e) {

							}
						}

						redirect(Core::$url . 'files/' . $path . '?f=1');
					}
				}
			}
		}

		Core::show_404();
	}

}