<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Rss extends Controller
{

	public function __construct()
	{
		header('Content-Type: application/rss+xml; charset=UTF-8');
	}

	public function page($id)
	{
		$page = Core::$db->page[(int) $id];

		if ($page && (int) $page['rss']) {
			$this->tpl = tpl('rss.php', array(
				'page' => $page,
				'items' => Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)
					));

			exit;
		}

		Core::show_404();
	}

}