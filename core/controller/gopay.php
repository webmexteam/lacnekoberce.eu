<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Gopay extends Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->content = tpl('default_1columns.php');

		if (Core::$uri != 'gopay/check') {
			Gopay::setup();
		}
	}

	public function check_system()
	{
		$this->tpl = null;
		$errors = '';
		$url = 'https://testgw.gopay.cz/axis/EPaymentService?wsdl';

		if (!User::$logged_in) {
			die('You need to log-in.');
		}

		if (!class_exists('SoapClient', false)) {
			$errors .= '- SOAP module is required<br />';
		}

		if (!function_exists('mcrypt_module_open')) {
			$errors .= '- Mcrypt module is required<br />';
		}

		if (!function_exists('openssl_encrypt')) {
			$errors .= '- Open SSL module is required<br />';
		}

		if (!ini_get('allow_url_fopen')) {
			$errors .= '- allow_url_fopen must be "On"<br />';
		}

		if (!$errors) {
			try {
				if (class_exists('SoapClient', false)) {
					$go_client = new SoapClient($url, array());
				}
			} catch (Exception $e) {
				$errors .= $e->getMessage() . '<br />';
			}
		}

		if (!$errors) {
			echo 'OK. Your server is ready.';
		} else {
			echo 'Fix your server configuration:<br />' . $errors;
		}
	}

	public function process_payment($hash)
	{
		if ($hash) {
			if ($parts = explode('_', $hash)) {
				if (($order = Core::$db->order[(int) $parts[0]]) && md5($order['id'] . $order['received'] . $order['email']) == $parts[1]) {

					if (!(int) $order['payment_realized']) {
						$payment = $order->payment;

						if ($payment && $payment['driver']) {
							$driverclass = 'Payment_' . ucfirst($payment['driver']);
							if ($inst = new $driverclass($payment)) {
								$inst->process($order);
							}
						}
					}

					redirect('gopay/fail');
				}
			}
		}
	}

	public function callback()
	{
		if (!Gopay::callback()) {
			// ivalid input data
			redirect('gopay/error');
		}
	}

	public function notify()
	{
		$this->tpl = null;

		Gopay::notify();
	}

	public function success()
	{
		$order_id = 0;

		if (!empty($_SESSION['gopay_order_id'])) {
			$order_id = (int) $_SESSION['gopay_order_id'];
		} else if (!empty($_GET['order'])) {
			$order_id = (int) $_GET['order'];
		}

		if (!($order = Core::$db->order[$order_id])) {
			redirect('gopay/error');
		}

		$_SESSION['order_id'] = $order['id'];

		$this->content->content = tpl('gopay/success.php', array(
			'order' => $order
				));
	}

	public function waiting()
	{
		$order_id = 0;

		if (!empty($_SESSION['gopay_order_id'])) {
			$order_id = (int) $_SESSION['gopay_order_id'];
		} else if (!empty($_GET['order'])) {
			$order_id = (int) $_GET['order'];
		}

		if (!($order = Core::$db->order[$order_id])) {
			redirect('gopay/error');
		}

		$_SESSION['order_id'] = $order['id'];

		$this->content->content = tpl('gopay/waiting.php', array(
			'order' => $order
				));
	}

	public function fail()
	{
		$order_id = 0;
		$order = $link = null;

		if (!empty($_SESSION['gopay_order_id'])) {
			$order_id = (int) $_SESSION['gopay_order_id'];
		} else if (!empty($_GET['order'])) {
			$order_id = (int) $_GET['order'];
		}

		if ($order_id && !($order = Core::$db->order[$order_id])) {
			redirect('gopay/error');
		}

		if ($order) {
			$link = url('gopay/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
			$_SESSION['order_id'] = $order['id'];
		}

		$this->content->content = tpl('gopay/fail.php', array(
			'order' => $order,
			'link' => $link
				));
	}

	public function error($error_type = null)
	{
		$order_id = 0;
		$order = $link = null;

		if (!empty($_SESSION['gopay_order_id'])) {
			$order_id = (int) $_SESSION['gopay_order_id'];
		} else if (!empty($_GET['order'])) {
			$order_id = (int) $_GET['order'];
		}

		if ($order_id) {
			$order = Core::$db->order[$order_id];
		}

		if ($order) {
			$link = url('gopay/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
			$_SESSION['order_id'] = $order['id'];
		}

		$this->content->content = tpl('gopay/error.php', array(
			'order' => $order,
			'error_type' => $error_type,
			'link' => $link
				));
	}

}