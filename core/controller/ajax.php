<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 * @author Tomas Nikl <tomasnikl.cz@gmail.com>
 */
class Controller_Ajax extends Controller
{

	public function __construct() {
		$this->tpl = null;
	}

	public function recalculate_basket() {

		// Rozparsujeme $_POST data
		$post = array();
		parse_str($_POST['data'], $post);

		// Aktualizujeme polozky v kosiku
		$product_data = array();
		foreach($post['qty'] as $_id => $_qty) {
			if((int) $_qty <= 0){
				$_qty = 1;
			}
			$product_data[$_id] = $_qty;
		}

		Basket::update($product_data);

		$total = Basket::total();
		$data = array();

		foreach(Basket::products() as $product){

			ob_start();
				echo price_vat(array($product['product'], ($product['price'] * $product['qty'])));
				$product_total_price = ob_get_contents();
			ob_clean();

			$data['products'][$product['id']] = array(
				'id'       => $product['id'],
				'price'    => price_vat(array($product['product'], $product['price'])),
				'quantity' => $product['qty'],
				'total'    => $product_total_price,
				'qty'	   => $product_data[$product['id']]
			);
		}

		if((int) Core::config('vat_payer')){
			ob_start();
				echo price($total->subtotal);
				$subtotal = ob_get_contents();
			ob_clean();

			$data['subtotal'] = $subtotal;
		}

		if(Basket::voucher()){
			ob_start();
				echo '-'.price($total->discount);
				$voucher = ob_get_contents();
			ob_clean();

			$data['voucher'] = $voucher;
		}

		ob_start();
			echo price($total->total);
			$total_price = ob_get_contents();
		ob_clean();

		$data['total'] = $total_price;

		echo json_encode($data);
	}

    /**
     * Get product in basket by attributes
     */
	public function get_basket_product() {

        $prd = array();
        $product = null;
		$attr = $_POST['attr'];
		$pid = $_POST['product_id'];

        if(isset($attr)) {
            $product = Core::$db->basket_products()->where('basket_id', Basket::id())->where('product_id', $pid)->where('attributes', $attr)->fetch();

            $prd['id'] = $product['id'];
            $prd['basket_id'] = $product['basket_id'];
            $prd['attributes'] = $product['attributes'];
        }

        echo json_encode($prd);
    }

}