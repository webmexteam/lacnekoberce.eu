<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Robots extends Controller
{

	public function __construct()
	{

	}

	public function index()
	{
		$this->tpl = null;

		header('Content-Type: text/plain; charset=UTF-8');

		echo "User-agent: *\n";

		echo "Disallow: " . url('customer') . "/\n";
		echo "Disallow: " . url('admin') . "/\n";
		echo "Disallow: " . url('etc') . "/\n";

		if (PAGE_BASKET)
			echo "Disallow: " . url(PAGE_BASKET) . "\n";

		if (PAGE_ORDER)
			echo "Disallow: " . url(PAGE_ORDER) . "\n";

		if (PAGE_ORDER_FINISH)
			echo "Disallow: " . url(PAGE_ORDER_FINISH) . "\n";

		if (PAGE_SEARCH)
			echo "Disallow: " . url(PAGE_SEARCH) . "\n";

		if (PAGE_404)
			echo "Disallow: " . url(PAGE_404) . "\n";

		echo "Sitemap: " . url('sitemap.xml', null, true) . "\n";
	}

	public function sitemap()
	{
		$this->tpl = null;

		header('Content-Type: application/xml; charset=UTF-8');

		echo tpl('system/sitemap_xml.php');
	}

}