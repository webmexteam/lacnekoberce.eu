<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Script extends Controller
{

	protected $cache_offset = 86400;

	public function __construct()
	{
		parent::__construct();

		header('Content-Type: application/x-javascript; charset=UTF-8');
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $this->cache_offset) . ' GMT');
		header('Cache-control: cache');
		header('Pragma: cache');

		$this->tpl = null;
	}

	public function constants($app = 'admin')
	{
		foreach ((array) View::$script_constants[$app] as $k => $v) {
			$value = null;

			if (is_string($v)) {
				$value = "'" . addslashes($v) . "'";
			} else if (is_numeric($v) || is_bool($v)) {
				$value = $v;
			} else {
				$value = json_encode($v);
			}

			echo "_" . $k . " = " . $value . ";\n";
		}
	}

	public function lang($app = 'admin', $language = null)
	{
		echo "_lang = {};\n";

		if ($language && Core::$language != $language) {
			Core::$language = $language;
			Core::loadI18n();
		}

		foreach ((array) View::$script_language[$app] as $k => $v) {
			$value = null;

			if (is_string($v)) {
				$value = "'" . addslashes($v) . "'";
			} else {
				$value = json_encode($v);
			}

			echo "_lang['" . $k . "'] = " . $value . ";\n";
		}
	}

	private function minify($str)
	{
		$result = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $result);

		return str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $result);
	}

}