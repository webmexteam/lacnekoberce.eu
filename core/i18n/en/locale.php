<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Core::$locale = array(
	'language' => 'en',
	'date' => 'Y/m/d',
	'datetime' => 'Y/m/d g:i A',
	'currency' => 'EUR',
	'price_format' => 3,
	'vat' => 20,
	'stop_words' => array(
		'and', 'or', 'the', 'an', 'am', 'are', 'as', 'at', 'be', 'by', 'can', 'did', 'do', 'for', 'got', 'had', 'has', 'have', 'he',
		'her', 'him', 'if', 'in', 'is', 'it', 'not', 'of', 'she', 'so', 'to', 'was', 'were',
		'cm', 'mm', 'km', 'hz', 'ghz', 'mhz', 'kg'
	),
	'word_radix_fn' => 'radix_en'
);

function radix_en($str)
{
	return null;
}