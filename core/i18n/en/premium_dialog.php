<form action="http://www.webmex.cz/order" method="post" target="_blank">
	<h3>Premium features</h3>
	<p>You are using a limited version of this application, which is available for free and some features are not available. If you want to use all the premium features of this application, upgrade to premium version.</p>
	<p>Features:</p>
	<ul>
		<li>Attributes (color, size etc.)</li>
		<li>Customer login</li>
		<li>Price groups</li>
		<li>Invoices</li>
		<li>Data import - XML, CSV or XLS</li>
		<li>Discounts</li>
		<li>Vouchers</li>
	</ul>
	<p><a href="http://www.webmex.cz/#2-premium" target="_blank">More about premium &raquo;</a></p>

	<input type="hidden" name="version" value="<?php echo Core::version ?>" />
	<input type="hidden" name="instid" value="<?php echo Core::config('instid') ?>" />
	<input type="hidden" name="name" value="<?php echo Core::config('store_name') ?>" />
	<input type="hidden" name="host" value="<?php echo $_SERVER['HTTP_HOST'] ?>" />
	<input type="hidden" name="url" value="<?php echo Core::$url ?>" />

	<div class="buttons">
		<button class="button">Upgrade to premium &raquo;</button>
	</div>
</form>