<form action="http://www.webmex.cz/order" method="post" target="_blank">
	<h3>Premium funkce</h3>
	<p>Používáte omezenou verzi e-shopu, která je dostupná zdarma a některé funkce nejsou dostupné. Pokud chcete využít premiových funkcí tohoto systému, přejďěte na premium verzi.</p>
	<p>Premium verze e-shopu obsahuje tyto funkce:</p>
	<ul>
		<li>Atributy zboží (velikost, barva, a jiné) včetně vlastních kódů, cen a obrázků</li>
		<li>Přihlašování zákazníků - sledování objednávek, správa údajů</li>
		<li>Cenové hladiny pro registrované zákazníky</li>
		<li>Propojení s účetním programem Pohoda</li>
		<li>Faktury z objednávek</li>
		<li>Importy zboží např. od dodavatelů ve formátu XML, CSV nebo XLS</li>
		<li>Hromadné slevy a slevy s datem platnosti</li>
		<li>Slevové kupóny</li>
	</ul>
	<p><a href="http://www.webmex.cz/#2-premium" target="_blank">Více o premium funkcích &raquo;</a></p>

	<input type="hidden" name="version" value="<?php echo Core::version ?>" />
	<input type="hidden" name="instid" value="<?php echo Core::config('instid') ?>" />
	<input type="hidden" name="name" value="<?php echo Core::config('store_name') ?>" />
	<input type="hidden" name="host" value="<?php echo $_SERVER['HTTP_HOST'] ?>" />
	<input type="hidden" name="url" value="<?php echo Core::$url ?>" />

	<div class="buttons">
		<button class="button">Chci premium e-shop &raquo;</button>
	</div>
</form>