<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$db_extras = <<<END
INSERT INTO `page` VALUES(1,0,1,0,1,"vitejte","Vítejte","","",1,1,"","","",NULL,NULL,3,"list","",1,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(2,0,1,0,5,"kosik","Váš košík","","",0,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(3,0,1,0,5,"objednavka-krok-1","Objednávka krok 1","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(4,0,1,0,5,"objednavka-odeslana","Objednávka odeslána","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(5,0,1,0,1,"obchodni-podminky","Obchodní podmínky","","",0,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(6,0,1,0,2,"kategorie","Kategorie","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(7,0,1,0,5,"hledani","Výsledky hledání","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(8,0,1,0,5,"404","Stránka nenalezena","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(9,0,1,0,4,"mapa-stranek","Mapa stránek","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(30,0,1,0,5,"objednavka-krok-2","Objednávka krok 2","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `config` VALUES("page_index","1");
INSERT INTO `config` VALUES("page_basket","2");
INSERT INTO `config` VALUES("page_order_step1","3");
INSERT INTO `config` VALUES("page_order_step2","10");
INSERT INTO `config` VALUES("page_order_finish","4");
INSERT INTO `config` VALUES("page_terms","5");
INSERT INTO `config` VALUES("page_search","7");
INSERT INTO `config` VALUES("page_404","8");
INSERT INTO `config` VALUES("page_sitemap","9");

INSERT INTO `email_message` VALUES ("1","Vaše objednávka #{id}","<p>Dobrý den,<br>
potvrzujeme přijetí Vaší objednávky číslo #{id}.</p>","new_order", "order");

INSERT INTO `email_message` VALUES ("2","Váš zákaznický účet","<p>Dobrý den,<br>
byl Vám vytvořen zákaznický účet pro další nákupy:</p>","new_account", "account");

INSERT INTO `email_message` VALUES ("3","Reset hesla do zákaznického účtu","<p>Dobrý den,<br>
pokud chcete resetovat Vaše heslo, klikněte na následující odkaz:</p>","reset_password", "password_reset");

INSERT INTO `email_message` VALUES ("4","Vaše nové heslo","<p>Dobrý den,<br>
bylo Vám vygenerováno nové heslo pro přístup do zákaznického účtu:</p>","new_password", "account");

INSERT INTO `email_message` VALUES ("5","K objednávce  #{id} byl přidán komentář","<p>Dobrý den,<br>
k objednávce  #{id} byl přidán komentář administrátory obchodu:</p>","order_comment", "order_comment");

INSERT INTO `email_message` VALUES ("6","Nízká úroveň skladu {name}","<p>Dobrý den,<br>
následující zboží má nízkou úroveň skladových zásob:</p>
<p>{sku} - {name}, sklad: {stock} ks</p>","low_stock", "default");

INSERT INTO `email_message` VALUES ("7","Slevový kupón pro Váš další nákup","<p>Dobrý den,<br>
byl Vám vytvořen slevový kupón pro Váš další nákup v e-shopu {store_name}:</p>","voucher", "voucher");

INSERT INTO `email_message` VALUES ("8","Vaše objednávka se zpracovává","<p>Dobrý den,<br>
Vaše objednávka číslo {id} je nyní zpracována a bude odeslána v nejkratším možném termínu.</p>","order_status_in_process", "order");

INSERT INTO `email_message` VALUES ("9","Vaše objednávka je vyřízena","<p>Dobrý den,<br>
Vaše objednávka číslo {id} je vyřízena a odeslána na Vaši adresu.</p>","order_status_finished", "order");

INSERT INTO `email_message` VALUES ("10","Vaše objednávka byla stornována","<p>Dobrý den,<br>
Vaše objednávka číslo {id} je stornována.</p>","order_status_canceled", "order");

INSERT INTO `email_message` VALUES ("11","Vaše objednávka je připravena k odeslání","<p>Dobrý den,<br>
Vaše objednávka číslo {id} je připravena k odeslání.</p>","order_status_to_send", "order");

INSERT INTO `email_message` VALUES ("12","Vaše objednávka {id} je potvrzena","<p>Dobrý den,<br />
Vaše objednávka {id} byla potvrzena obchodníkem.</p>","order_confirmed", "order");

INSERT INTO `email_message` VALUES ("13","Vaše objednávka {id} je potvrzena","<p>Dobrý den,<br />
Vaše objednávka {id} byla potvrzena obchodníkem.</p>","order_status_confirmed", "order");

INSERT INTO `email_message` VALUES ("14","Vaše objednávka {id} byla upravena","<p>Dobrý den,<br />
Vaše objednávka {id} byla upravena.</p>","order_status_updated", "order");

INSERT INTO `email_message` VALUES ("15","Vaše objednávka {id} čeká na platbu","<p>Dobrý den,<br />
Vaše objednávka {id} čeká na uhrazení platby.</p>","order_status_waiting_for_payment", "order");

INSERT INTO `email_message` VALUES ("16","Vaše objednávka {id} byla zaplacena","<p>Dobrý den,<br />
Vaše objednávka {id} byla zaplacena.</p>","order_status_paid", "order");

INSERT INTO `email_message` VALUES ("17","Vaše objednávka {id} čeká na vyzvednutí","<p>Dobrý den,<br />
Vaše objednávka {id} čeká na vyzvednutí.</p>","order_status_waiting_for_pickup", "order");

INSERT INTO `email_message` VALUES ("18","Vaše objednávka {id} byla odeslána","<p>Dobrý den,<br />
Vaše objednávka {id} byla odeslána.</p>","order_status_sent", "order");

INSERT INTO `availability` VALUES(1,0,"Skladem");
INSERT INTO `country` VALUES(1,"Česká republika",0);
INSERT INTO `delivery` VALUES(1,"Česká pošta","cpost","0.0",0,null,null,null,null,1);
INSERT INTO `payment` VALUES(1,"Dobírka","","0.0",null,0,null,null,null,null,null,1);
INSERT INTO `delivery_payments` VALUES(1,1,"10%",3000);
INSERT INTO `block` VALUES(1,"Kategorie","",0,"left","pages",'{"type":"2","order_by":"position"}',null);
INSERT INTO `customer_group` VALUES ("1","Návštěvníci","1.0");
INSERT INTO `customer_group` VALUES ("2","Registrovaní","1.0");

INSERT INTO `vat` VALUES ("1","Základní","21");
INSERT INTO `vat` VALUES ("2","Snížená","15");

INSERT INTO `config` VALUES ('sharelinks','<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5253253243715f1c"></script>
<!-- AddThis Button END -->');
INSERT INTO `label` VALUES ("1","Akce","DA0400");
END;
