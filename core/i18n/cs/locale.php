<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Core::$locale = array(
	'language' => 'cs',
	'date' => 'j. n. Y',
	'datetime' => 'j. n. Y H:i',
	'currency' => 'Kč',
	'price_format' => 3,
	'vat' => 20,
	'stop_words' => array(
		'aby', 'ale', 'anebo', 'ani', 'aniž', 'ano', 'asi', 'avšak', 'až', 'bez', 'bude', 'budem', 'budeme', 'budeš', 'byl', 'byla',
		'byli', 'bylo', 'být', 'článek', 'článku', 'članky', 'com', 'což', 'další', 'dnes', 'do', 'email', 'jak', 'jaké', 'jako',
		'je', 'jeho', 'jej', 'její', 'jejich', 'jen', 'ještě', 'jenž', 'jiné', 'již', 'jsem', 'jseš', 'jsi', 'jsme', 'jsou', 'jste',
		'kam', 'kde', 'kdo', 'když', 'která', 'které', 'kteří', 'kterou', 'který', 'má', 'máte', 'mezi', 'mě', 'mít', 'mně', 'mnou', 'můj',
		'může', 'na', 'nad', 'nám', 'napište', 'nás', 'naši', 'nebo', 'neboť', 'nechť', 'nejsou', 'není', 'net', 'než', 'nic', 'nové',
		'nový', 'nýbrž', 'od', 'ode', 'org', 'pak', 'po', 'pod', 'podle', 'pokud', 'pouze', 'právě', 'před', 'přes', 'při', 'pro', 'proč',
		'proto', 'protože', 'první', 'ptá', 'se', 'sice', 'spol', 'strana', 'své', 'svůj', 'svých', 'svým', 'svými', 'tak', 'také',
		'takže', 'támhle', 'tato', 'tedy', 'téma', 'ten', 'tedy', 'tento', 'teto', 'ti', 'tím', 'tímto', 'tipy', 'to', 'tohle', 'toho',
		'tohoto', 'tom', 'tomto', 'tomuto', 'totiž', 'tudíž', 'tuto', 'tvůj', 'tyto', 'vám', 'vás', 'váš', 'vaše', 've', 'vedle', 'vy',
		'více', 'však', 'všechen', 'vždyť', 'zda', 'zde', 'zpět', 'že',
		'cm', 'mm', 'km', 'hz', 'ghz', 'mhz', 'kg'
	),
	'word_radix_fn' => 'radix_cs'
);

function radix_cs($str)
{
	$radix = null;

	if (preg_match('/(ovy|ova|ami|ych|ich)$/', $str)) {
		$radix = substr($str, 0, -3);
	} else if (preg_match('/(am|em|im|om|um|ym)$/', $str)) {
		$radix = substr($str, 0, -2);
	} else if (preg_match('/[aeiouy]$/', $str)) {
		$radix = substr($str, 0, -1);
	}

	return $radix ? $radix : $str;
}