<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$db_sample_extras = <<<END
INSERT INTO `feature` VALUES(1,	'Rozlišení',	0,	'Mpx',	'number',	1,	'18',	1);
INSERT INTO `feature` VALUES(2,	'Typ snímače',	0,	NULL,	'string',	1,	'CMOS',	1);
INSERT INTO `feature` VALUES(3,	'Automatické čištění snímače',	0,	NULL,	'yesno',	1,	'1',	1);
INSERT INTO `feature` VALUES(4,	'Poměr stran snímače',	0,	NULL,	'string',	1,	'3:2',	1);

INSERT INTO `featureset` VALUES(1,	'Digitální zrcadlovky');

INSERT INTO `page` VALUES(31,	0,	1,	0,	2,	'elektronika',	'Elektronika',	NULL,	NULL,	1,	0,	NULL,	NULL,	NULL,	1383330182,	1,	2,	'list',	NULL,	2,	0,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	0);
INSERT INTO `page` VALUES(32,	31,	1,	0,	0,	'digitalni-fotoaparaty',	'Digitální fotoaparáty',	NULL,	NULL,	1,	0,	NULL,	NULL,	NULL,	1383330179,	1,	2,	'list',	NULL,	2,	0,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	0);
INSERT INTO `page` VALUES(33,	32,	1,	0,	0,	'digitalni-zrcadlovky',	'Digitální zrcadlovky',	NULL,	NULL,	1,	0,	NULL,	NULL,	NULL,	1383330148,	1,	2,	'list',	NULL,	2,	0,	NULL,	NULL,	1,	0,	1,	NULL,	NULL,	NULL,	NULL,	0);

INSERT INTO `product_files` VALUES(1,	1,	'files/sample_images/book01.jpg',	'',	1,	-1,	1,	2);
INSERT INTO `product_files` VALUES(2,	1,	'files/sample_images/book02.jpg',	'',	2,	-1,	1,	2);
INSERT INTO `product_files` VALUES(3,	1,	'files/sample_images/book03.jpg',	'',	3,	-1,	1,	2);
INSERT INTO `product_files` VALUES(4,	1,	'files/sample_images/book04.jpg',	'',	4,	-1,	1,	2);
INSERT INTO `product_files` VALUES(5,	1,	'files/sample_images/book05.jpg',	'',	5,	-1,	1,	2);
INSERT INTO `product_files` VALUES(6,	1,	'files/sample_images/book06.jpg',	'',	6,	-1,	1,	2);
INSERT INTO `product_files` VALUES(7,	1,	'files/sample_images/book07.jpg',	'',	7,	-1,	1,	2);
INSERT INTO `product_files` VALUES(8,	1,	'files/sample_images/card01.jpg',	'',	8,	-1,	1,	2);
INSERT INTO `product_files` VALUES(9,	1,	'files/sample_images/card02.jpg',	'',	9,	-1,	1,	2);
INSERT INTO `product_files` VALUES(10,	1,	'files/sample_images/card03.jpg',	'',	10,	-1,	1,	2);
INSERT INTO `product_files` VALUES(30,	2,	'files/sample_images/moo06.jpg',	'',	10,	-1,	1,	2);
INSERT INTO `product_files` VALUES(29,	2,	'files/sample_images/moo05.jpg',	'',	9,	-1,	1,	2);
INSERT INTO `product_files` VALUES(28,	2,	'files/sample_images/card11.jpg',	'',	8,	-1,	1,	2);
INSERT INTO `product_files` VALUES(27,	2,	'files/sample_images/card10.jpg',	'',	7,	-1,	1,	2);
INSERT INTO `product_files` VALUES(26,	2,	'files/sample_images/card09.jpg',	'',	6,	-1,	1,	2);
INSERT INTO `product_files` VALUES(25,	2,	'files/sample_images/card08.jpg',	'',	5,	-1,	1,	2);
INSERT INTO `product_files` VALUES(24,	2,	'files/sample_images/card07.jpg',	'',	4,	-1,	1,	2);
INSERT INTO `product_files` VALUES(23,	2,	'files/sample_images/card06.jpg',	'',	3,	-1,	1,	2);
INSERT INTO `product_files` VALUES(22,	2,	'files/sample_images/card05.jpg',	'',	2,	-1,	1,	2);
INSERT INTO `product_files` VALUES(21,	2,	'files/sample_images/card04.jpg',	'',	1,	-1,	1,	2);
INSERT INTO `product_files` VALUES(31,	2,	'files/sample_images/moo07.jpg',	'',	11,	-1,	1,	2);
INSERT INTO `product_files` VALUES(32,	2,	'files/sample_images/moo08.jpg',	'',	12,	-1,	1,	2);
INSERT INTO `product_files` VALUES(33,	2,	'files/sample_images/moo09.jpg',	'',	13,	-1,	1,	2);
INSERT INTO `product_files` VALUES(34,	2,	'files/sample_images/moo10.jpg',	'',	14,	-1,	1,	2);
INSERT INTO `product_files` VALUES(35,	2,	'files/sample_images/moo11.jpg',	'',	15,	-1,	1,	2);
INSERT INTO `product_files` VALUES(36,	2,	'files/sample_images/moo12.jpg',	'',	16,	-1,	1,	2);
INSERT INTO `product_files` VALUES(37,	2,	'files/sample_images/moo13.jpg',	'',	17,	-1,	1,	2);

INSERT INTO `product_pages` VALUES(1,	1);
INSERT INTO `product_pages` VALUES(1,	33);
INSERT INTO `product_pages` VALUES(2,	1);
INSERT INTO `product_pages` VALUES(2,	33);

DROP TABLE IF EXISTS `features_1`;
CREATE TABLE `features_1` (`product_id` int(11) NOT NULL,`f1` float(12,2) DEFAULT NULL,`f2` varchar(255) DEFAULT NULL,`f3` int(1) NOT NULL DEFAULT '0',`f4` varchar(255) DEFAULT NULL,PRIMARY KEY (`product_id`),KEY `f1` (`f1`,`f2`(10),`f3`,`f4`(10))) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `features_1` VALUES(1,	18.00,	'CMOS',	1,	'3:2');
INSERT INTO `features_1` VALUES(2,	18.00,	'CMOS',	1,	'3:2');
END;
