<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Result implements Iterator, Countable
{

	public $odb, $table, $schema, $primary;
	public $where = array(), $select, $limit, $offset, $order = array();
	protected $sql_resource, $limit_start, $limit_left = -2, $current, $seek, $data_fetched = false;

	public function __construct($table, Odb $odb)
	{
		$this->table = $this->table_prefix() . $table;
		$this->odb = $odb;

		$this->schema = new Odb_Schema($this->table);

		$this->primary = $this->schema->getPrimaryKey();
	}

	public function __toString()
	{
		return (string) $this->buildQuery();
	}

	public function getReferencingColumn()
	{
		return $this->table . '_' . $this->primary;
	}

	public function table_prefix()
	{
		return '';
	}

	public function escape($value)
	{
		return $this->odb->escape((string) $value);
	}

	public function quote($value)
	{
		if ($value === null) {
			return 'NULL';
		} else if ($value === true) {
			return "'1'";
		} else if ($value === false) {
			return "'0'";
		} else if (is_object($value)) {
			return $this->quote((string) $value);
		} else if (is_array($value)) {
			return '(' . implode(', ', array_map(array($this, __FUNCTION__), $value)) . ')';
		} else if (is_int($value)) {
			return (int) $value;
		} else if (is_float($value)) {
			return sprintf('%F', $value);
		}

		return "'" . $this->escape($value) . "'";
	}

	public function quote_identifier($value)
	{
		if ($value === '*') {
			return $value;
		} else if (is_object($value)) {
			return $this->quote_identifier((string) $value);
		} else if (is_array($value)) {
			list ($value, $alias) = $value;

			return $this->quote_identifier($value) . ' AS ' . $this->quote_identifier($alias);
		}

		if (strpos($value, '"') !== false) {
			// Quote the column in FUNC("ident") identifiers
			return preg_replace('/"(.+?)"/e', '$this->quote_identifier("$1")', $value);
		} else if (strpos($value, '.') !== false) {
			$parts = explode('.', $value);

			if ($prefix = $this->table_prefix()) {
				// Get the offset of the table name, 2nd-to-last part
				// This works for databases that can have 3 identifiers (Postgre)
				$offset = count($parts) - 2;

				// Add the table prefix to the table name
				$parts[$offset] = $prefix . $parts[$offset];
			}

			// Quote each of the parts
			return implode('.', array_map(array($this, __FUNCTION__), $parts));
		} else if (preg_match('/\w+\((.*)\)|\w+\s+\w+/i', $value)) { // function
			return $value;
		} else {
			return '`' . trim($value, '`') . '`';
		}
	}

	public function quote_table($table)
	{
		if (is_string($table) && strpos($table, '.') === false) {
			$table = $table;
		}

		if ($this->odb->driver == 'sqlite') {
			return "'" . $table . "'";
		}

		return $this->quote_identifier($table);
	}

	public function insert($data, $toString = false)
	{
		$class = 'Model_' . ucfirst($this->table);

		if (class_exists($class, true) && method_exists($class, 'insert')) {
			$data = call_user_func(array($class, 'insert'), $data);
		}

		$sql = 'INSERT INTO ' . $this->quote_table($this->table) . ' (' . implode(', ', array_map(array($this, 'quote_identifier'), array_keys($data))) . ') VALUES (' . implode(', ', array_map(array($this, 'quote'), $data)) . ')';

		if ($toString) {
			return $sql;
		}

		if (!$this->odb->query($sql)) {
			return false;
		}

		return $this->odb->last_insert_id();
	}

	public function update($data, $toString = false, $single_row = null)
	{
		$values = array();

		$class = 'Model_' . ucfirst($this->table);

		if (class_exists($class, true) && method_exists($class, 'update')) {
			$data = call_user_func(array($class, 'update'), $data, $single_row);
		}

		foreach ($data as $key => $val) {
			$values[] = $this->quote_identifier($key) . ' = ' . $this->quote($val);
		}

		if (empty($values)) {
			return 0;
		}

		$sql = 'UPDATE ' . $this->quote_table($this->table) . ' SET ' . implode(', ', $values) . $this->buildWhere();

		if ($toString) {
			return $sql;
		}

		if (!$this->odb->query($sql)) {
			return false;
		}

		return true;
	}

	public function delete()
	{
		$sql = 'DELETE FROM ' . $this->quote_table($this->table) . $this->buildWhere();

		if (!$this->odb->query($sql)) {
			return false;
		}

		return $this->odb->affected_rows();
	}

	public function select($columns)
	{
		$this->select[] = $columns;

		return $this;
	}

	public function where($column, $value = null)
	{
		$args = func_num_args();

		if ($args == 2) {

			if (strpbrk($column, '?')) {
				return $this->where(preg_replace('/\?/', $this->quote($value), $column));
			}

			$condition = $this->quote_identifier($column);

			if (is_array($value)) {
				if (count($value)) {
					if (count($value) != 1) {
						$condition .= ' IN (' . implode(', ', array_map(array($this, 'quote'), $value)) . ')';
					} else {
						$condition .= ' = ' . $this->quote($value);
					}
				}
			} else if (is_null($value)) {
				$condition .= ' IS NULL';
			} else if ($value instanceof Odb_Result) {
				$select = $value->select;

				if ($_value = (string) $value) {
					if (!$select) {
						$value->select = array($value->primary);
					}

					$condition .= ' IN (' . $_value . ')';

					$value->select = $select;
				}
			} else {
				$condition .= ' = ' . $this->quote($value);
			}
		} else if (is_array($column)) {
			$this->where = array_merge($this->where, $column);
			return $this;
		} else {
			$condition = $column;
		}

		$this->where[] = $condition;

		return $this;
	}

	public function getWhere()
	{
		return $this->where;
	}

	public function limit($limit, $offset = null)
	{
		$this->limit = $limit;
		$this->offset = $offset;

		return $this;
	}

	public function order($columns)
	{
		$this->order[] = $columns;

		return $this;
	}

	public function group($functions, $having = '')
	{
		$sql = 'SELECT ' . $functions . ' FROM ' . $this->quote_table($this->table) . $this->buildWhere();

		if ($having) {
			$sql .= ' ' . $having;
		}

		return $this->odb->fetch_array($this->odb->query($sql));
	}

	public function count_all()
	{
		$sql = 'SELECT COUNT(*) FROM ' . $this->quote_table($this->table);

		$result = $this->odb->fetch_array($this->odb->query($sql));

		return (int) $result[0];
	}

	protected function buildWhere()
	{
		$query = '';

		if ($this->where) {
			$query .= ' WHERE (' . implode(') AND (', $this->where) . ')';
		}

		if ($this->order) {
			$query .= ' ORDER BY ' . implode(', ', $this->order);
		}

		if ($this->limit) {
			$query .= ' LIMIT ' . $this->limit;

			if ($this->offset) {
				$query .= ' OFFSET ' . $this->offset;
			}
		}

		return $query;
	}

	protected function buildQuery()
	{
		$query = 'SELECT';

		if ($this->select) {
			$query .= ' ' . implode(', ', (array) $this->select);
		} else {
			$query .= ' *';
		}

		$query .= ' FROM ' . $this->quote_table($this->table) . '';

		$query .= $this->buildWhere();

		return $query;
	}

	protected function fetchData()
	{
		$this->sql_resource = $this->odb->query($this->buildQuery());
		$this->data_fetched = true;
	}

	public function fetch()
	{
		return $this->current();
	}

	public function rewind()
	{
		if ($this->sql_resource && $this->count() > 0) {
			$this->odb->data_seek($this->sql_resource, $this->limit_start ? $this->limit_start : ($this->seek ? $this->seek : 0));
		}
	}

	public function current()
	{
		if (!$this->current) {
			return $this->next();
		}

		return $this->current;
	}

	public function key()
	{
		return $this->current[$this->schema->getPrimaryKey()];
	}

	public function next()
	{
		if (!$this->sql_resource) {
			if ($this->data_fetched) {
				return false;
			}

			$this->fetchData();
		}

		if ($this->limit_left > -1) {
			$this->limit_left--;
		}

		$row = $this->odb->fetch_assoc($this->sql_resource);

		if (!$row) {
			$this->current = false;
		} else {
			$this->current = new Odb_Row($row, $this);
		}

		return $this->current;
	}

	public function valid()
	{
		if ($this->limit_left == -1) {
			return false;
		}

		$var = $this->current() !== false;
		return $var;
	}

	public function count()
	{
		if (!$this->sql_resource) {
			if ($this->data_fetched) {
				return false;
			}

			$this->fetchData();
		}

		return $this->odb->num_rows($this->sql_resource);
	}

	public function seek($offset)
	{
		$this->seek = $offset;

		return $this;
	}

}