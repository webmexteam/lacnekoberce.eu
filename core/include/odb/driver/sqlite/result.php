<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Driver_Sqlite_Result implements Iterator, Countable
{

	public $result;
	protected $position = 0, $data;

	public function __construct($result)
	{
		$this->result = $result;
		$this->data = $result->fetchAll(PDO::FETCH_ASSOC);
	}

	public function current()
	{
		return isset($this->data[$this->position]) ? $this->data[$this->position] : false;
	}

	public function key()
	{
		return $this->position;
	}

	public function next()
	{
		++$this->position;
	}

	public function valid()
	{
		return isset($this->data[$this->position]);
	}

	public function rewind()
	{
		$this->position = 0;
	}

	public function count()
	{
		return count($this->data);
	}

	public function seek($position)
	{
		$this->position = (int) $position;
	}

	public function fetchArray()
	{
		$return = $this->current();

		if ($return === false) {
			return false;
		}

		$this->next();

		$arr = array();
		$i = 0;

		foreach ($return as $k => $v) {
			$arr[$k] = $arr[$i] = $v;

			$i++;
		}

		return $arr;
	}

	public function fetchAssoc()
	{
		$return = $this->current();

		if ($return === false) {
			return false;
		}

		$this->next();

		return (array) $return;
	}

	public function fetchObject()
	{
		$return = $this->current();

		if ($return === false) {
			return false;
		}

		$this->next();

		return (object) $return;
	}

}