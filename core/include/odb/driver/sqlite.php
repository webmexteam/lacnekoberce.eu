<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Driver_Sqlite extends Odb_Driver
{

	protected $pdo, $last_query;

	public function __construct($dsn, $user = null, $password = null)
	{
		$this->pdo = Core::$db_conn;

		if (!$this->pdo) {
			throw new Exception('Database connection error');
		}
	}

	public function query($query)
	{
		$this->last_query = $this->pdo->query($query);

		return new Odb_Driver_Sqlite_Result($this->last_query);
	}

	public function num_rows($resource)
	{
		return count($resource);
	}

	public function data_seek($resource, $rownum)
	{
		return $resource->seek($rownum);
	}

	public function fetch_assoc($resource)
	{
		return $resource->fetchAssoc();
	}

	public function fetch_array($resource)
	{
		return $resource->fetchArray();
	}

	public function fetch_object($resource)
	{
		return $resource->fetchObject();
	}

	public function escape($value)
	{
		return sqlite_escape_string($value);
	}

	public function last_insert_id()
	{
		return $this->pdo->lastInsertId();
	}

	public function affected_rows()
	{
		return $this->last_query ? $this->last_query->rowCount() : 0;
	}

}