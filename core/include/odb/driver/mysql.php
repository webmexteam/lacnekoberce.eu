<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Driver_Mysql extends Odb_Driver
{

	public function __construct($dsn, $user = null, $password = null)
	{
		preg_match('/host\=([^\;]+)/', $dsn, $host);
		preg_match('/dbname\=([^\;]+)/', $dsn, $dbname);

		$this->link = mysql_connect($host[1], $user, $password);

		if (!$this->link) {
			throw new Exception(mysql_error());
		}

		mysql_select_db($dbname[1]);

		mysql_query('SET NAMES UTF8');
	}

	public function query($query)
	{
		if (($result = mysql_query($query, $this->link)) === false) {
			throw new Odb_Exception(mysql_error());
		}

		return $result;
	}

	public function num_rows($resource)
	{
		return mysql_num_rows($resource);
	}

	public function data_seek($resource, $rownum)
	{
		return mysql_data_seek($resource, $rownum);
	}

	public function fetch_assoc($resource)
	{
		return mysql_fetch_assoc($resource);
	}

	public function fetch_array($resource)
	{
		return mysql_fetch_array($resource);
	}

	public function fetch_object($resource)
	{
		return mysql_fetch_object($resource);
	}

	public function escape($value)
	{
		return mysql_real_escape_string($value);
	}

	public function last_insert_id()
	{
		return mysql_insert_id($this->link);
	}

	public function affected_rows()
	{
		return mysql_affected_rows($this->link);
	}

}