<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Row implements ArrayAccess
{

	protected $data;
	public $result, $model, $table_name;

	public function __construct(array $data, Odb_Result $result)
	{
		$this->data = $data;
		$this->result = $result;
		$this->table_name = $result->table;

		$model = 'Model_' . ucfirst($this->result->table);

		if (!$this->model && class_exists($model, true)) {
			$this->model = new $model($this);
		}
	}

	public function __get($table)
	{
		$result = new Odb_Result_Single($table, $this->result->odb);

		$pk = $result->primary;

		$result->where($pk, $this[$table . '_' . $pk]);

		return $result->fetch();
	}

	public function __call($table, array $where)
	{
		$result = new Odb_Result($table, $this->result->odb);

		$pk = $this->result->primary;

		$result->where($this->result->getReferencingColumn(), $this[$pk]);

		if ($where) {
			$data = $where[0];

			if ($data instanceof Traversable) {
				$data = iterator_to_array($data);
			}

			if (is_array($data)) {
				return $result->insert($data, (isset($where[1]) && $where[1] == 1));
			}

			call_user_func_array(array($result, 'where'), $where);
		}

		return $result;
	}

	public function __isset($name)
	{
		return ($this->__get($name) !== null);
	}

	public function __toString()
	{
		return (string) $this[$this->result->primary];
	}

	public function update($data, $toString = false)
	{
		$pk = $this->result->primary;

		return $this->result->odb->__call($this->result->table, array($pk, $this[$pk]))->update($data, $toString, $this->data);
	}

	public function delete()
	{
		$pk = $this->result->primary;

		return $this->result->odb->__call($this->result->table, array($pk, $this[$pk]))->delete();
	}

	public function offsetExists($key)
	{
		return array_key_exists($key, $this->data);
	}

	public function offsetGet($key)
	{
		$value = $this->data[$key];

		if ($this->model) {
			$method = 'get' . ucfirst($key);

			if (method_exists($this->model, $method)) {
				$value = $this->model->$method();
			}
		}

		Event::run('Db_Row::offsetGet', $this, $key, $value);

		return $value;
	}

	public function offsetSet($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function offsetUnset($key)
	{
		unset($this->data[$key]);
	}

	public function as_array()
	{
		return (array) $this->data;
	}

	public function get($key)
	{
		return $this->data[$key];
	}

	public function needUpdate($data)
	{
		foreach ($data as $k => $v) {

			if (in_array($k, array('last_change', 'last_change_user'))) {
				continue;
			}

			$_v = $this->get($k);
			if ($_v !== $v || (($_v === null || $v === null) && $v != $v)) {
				return true;
			}
		}

		return false;
	}

}