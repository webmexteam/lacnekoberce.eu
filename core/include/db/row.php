<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Db_Row extends NotORM_Row
{

	public $model;

	public function __construct(array $row, NotORM_Result $result)
	{
		parent::__construct($row, $result);

		$model = 'Model_' . ucfirst($this->table_name);

		if (!$this->model && class_exists($model, true)) {
			$this->model = new $model($this);
		}
	}

	public function releaseMem()
	{
		unset($this->model);
		parent::releaseMem();
	}

	public function offsetGet($key)
	{
		$value = $this->get($key);

		if ($this->model) {
			$method = 'get' . ucfirst($key);

			if (method_exists($this->model, $method)) {
				$value = $this->model->$method();
			}
		}

		Event::run('Db_Row::offsetGet', $this, $key, $value);

		return $value;
	}

	public function get($key)
	{
		return parent::offsetGet($key);
	}

	public function needUpdate($data)
	{
		foreach ($data as $k => $v) {

			if (in_array($k, array('last_change', 'last_change_user'))) {
				continue;
			}

			$_v = $this->get($k);
			if ($_v !== $v || (($_v === null || $v === null) && $v != $v)) {
				return true;
			}
		}

		return false;
	}

}