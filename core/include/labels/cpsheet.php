<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
define('FPDF_FONTPATH', APPROOT . 'vendor/fpdf/font/');
require(APPROOT . 'vendor/fpdf/fpdf.php');
require(APPROOT . 'vendor/fpdf/fpdi/fpdi.php');

class WEBMEX_FPDF extends FPDI
{

	public $footer_text;
	public $date;

	public function Footer()
	{

	}

	public function AddPage($o = 'P', $f = 'A4')
	{
		parent::AddPage($o, $f);

		$this->setSourceFile(DOCROOT . 'core/vendor/cpost/podaci_arch_2.pdf');
		// import page 1
		$tplIdx = $this->importPage(1);

		$this->useTemplate($tplIdx);

		$this->SetY(47);
	}

}

class Labels_Cpsheet
{

	public $pdf;
	protected $lines = array();
	protected $fontsize = 10;

	public function __construct()
	{
		$this->pdf = new WEBMEX_FPDF();

		$this->pdf->AliasNbPages();

		$this->pdf->AddFont('Freesans', '', 'freesans.php');

		$this->pdf->AddPage('P', 'A4');
	}

	public function __set($key, $value)
	{
		$this->data[$key] = html_entity_decode($value);
	}

	public function add($sku, $name, $quantity, $price)
	{
		$this->data['items'][] = array($sku, html_entity_decode($name), $quantity, $price);
	}

	public function iconv($text)
	{
		return iconv('utf-8', 'windows-1250', $text);
	}

	public function cell($w, $h, $text, $border = 0, $ln = 0, $align = 'L', $fill = false)
	{
		$this->pdf->Cell($w, $h, $this->iconv($text), $border, $ln, $align, $fill);
	}

	public function multicell($w, $h, $text, $border = 0, $align = 'L')
	{
		$text = preg_replace('/\t/', '    ', $text);
		$x = $this->pdf->GetX();
		$y = $this->pdf->GetY();

		$this->pdf->MultiCell($w, $h, $this->iconv($text), $border, $align);

		$this->pdf->SetY($y);
		$this->pdf->SetX($w + $x);
	}

	public function moveX($change)
	{
		$this->pdf->SetX($this->pdf->GetX() + $change);
	}

	public function moveY($change)
	{
		$this->pdf->SetY($this->pdf->GetY() + $change);
	}

	public function hline($ln = 2)
	{
		$this->moveY($ln);
		$this->pdf->Line($this->pdf->GetX(), $this->pdf->GetY(), $this->pdf->GetX() + 189, $this->pdf->GetY());
		$this->moveY($ln);
	}

	public function save($name = null, $dest = 'I')
	{
		$this->render();
		$this->pdf->Output($name, $dest);
	}

	public function render()
	{
		$this->pdf->SetFont('Freesans', '', $this->fontsize);

		$i = 1;
		foreach ($this->lines as $line) {
			if ($i % 11 === 0) {
				$this->pdf->AddPage();
			}

			$this->moveX(70);
			$this->multicell(52, 4.7, $line['name'] . "\n" . $line['address'] . "\n" . $line['post']);

			$this->pdf->Ln(21);

			$i++;
		}
	}

	public function addOrder($order)
	{
		$prefix = '';

		if ($order['ship_street']) {
			$prefix = 'ship_';
		}

		$this->lines[] = array(
			'name' => $order[$prefix . 'first_name'] . ' ' . $order[$prefix . 'last_name'] . ($order[$prefix . 'company'] ? ', ' . $order[$prefix . 'company'] : ''),
			'post' => $order[$prefix . 'zip'] . ' ' . $order[$prefix . 'city'],
			'address' => $order[$prefix . 'street']
		);
	}

}