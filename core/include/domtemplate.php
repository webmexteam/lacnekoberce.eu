<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class DOMTemplate
{

	public static $doc;

	public function __construct($name)
	{
		if (!self::$doc) {
			self::$doc = new DOMDocument();
		}

		$this->el = self::$doc->createElement($name);
	}

	public function __toString()
	{
		return self::$doc->saveXML($this->el);
	}

	public function append($node)
	{
		if (func_num_args() > 1) {
			$args = func_get_args();
			foreach ($args as $arg) {
				$this->append($arg);
			}
		} else if (is_array($node)) {
			foreach ($node as $arg) {
				$this->append($arg);
			}
		} else if ($node instanceof DOMTemplate) {
			$this->el->appendChild($node->el);
		} else if (is_string($node) || (is_object($node) && !$node instanceof DOMNode)) {
			$fragment = self::$doc->createDocumentFragment();

			$str = $this->sanitizeString((string) $node);

			$fragment->appendXML($str);

			@$this->el->appendChild($fragment);
		} else if ($node instanceof DOMNode) {
			$this->el->appendChild($node);
		}

		return $this;
	}

	public function __call($method, $args)
	{
		$value = $this->sanitizeString(array_shift($args));

		if (!$method) {
			return $this;
		}

		$this->el->setAttribute($method, $value);

		return $this;
	}

	public function attr($name, $value)
	{
		return $this->__call($name, array($value));
	}

	public function sanitizeString($str)
	{
		$str = preg_replace_callback('/([^\s\>]+)?\@([\w\-]+)(\.\w{2,5})?/miu', array($this, 'replace_i18n'), $str);

		return xmlentities($str);
	}

	protected function replace_i18n($matches)
	{
		if ($matches[1] == '\\') {
			// dont replace
			return '@<!---->' . substr(ltrim($matches[0], '\\'), 1);
		} else if ($matches[1] && $matches[3]) {
			// an email, dont replace
			return $matches[0];
		}

		return $matches[1] . __($matches[2]);
	}

}

function createElement($name)
{
	$args = func_get_args();
	$el = new DOMTemplate($name);

	if (func_num_args() > 1) {
		$el->append(array_slice($args, 1));
	}

	return $el;
}

function doctype()
{
	return '<!doctype html>';
}

function a()
{
	$args = func_get_args();
	array_unshift($args, 'a');
	return call_user_func_array('createElement', $args);
}

function abbr()
{
	$args = func_get_args();
	array_unshift($args, 'abbr');
	return call_user_func_array('createElement', $args);
}

function acronym()
{
	$args = func_get_args();
	array_unshift($args, 'acronym');
	return call_user_func_array('createElement', $args);
}

function address()
{
	$args = func_get_args();
	array_unshift($args, 'address');
	return call_user_func_array('createElement', $args);
}

function applet()
{
	$args = func_get_args();
	array_unshift($args, 'applet');
	return call_user_func_array('createElement', $args);
}

function area()
{
	$args = func_get_args();
	array_unshift($args, 'area');
	return call_user_func_array('createElement', $args);
}

function article()
{
	$args = func_get_args();
	array_unshift($args, 'article');
	return call_user_func_array('createElement', $args);
}

function aside()
{
	$args = func_get_args();
	array_unshift($args, 'aside');
	return call_user_func_array('createElement', $args);
}

function audio()
{
	$args = func_get_args();
	array_unshift($args, 'audio');
	return call_user_func_array('createElement', $args);
}

function b()
{
	$args = func_get_args();
	array_unshift($args, 'b');
	return call_user_func_array('createElement', $args);
}

function base()
{
	$args = func_get_args();
	array_unshift($args, 'base');
	return call_user_func_array('createElement', $args);
}

function basefont()
{
	$args = func_get_args();
	array_unshift($args, 'basefont');
	return call_user_func_array('createElement', $args);
}

function bdo()
{
	$args = func_get_args();
	array_unshift($args, 'bdo');
	return call_user_func_array('createElement', $args);
}

function big()
{
	$args = func_get_args();
	array_unshift($args, 'big');
	return call_user_func_array('createElement', $args);
}

function blockquote()
{
	$args = func_get_args();
	array_unshift($args, 'blockquote');
	return call_user_func_array('createElement', $args);
}

function body()
{
	$args = func_get_args();
	array_unshift($args, 'body');
	return call_user_func_array('createElement', $args);
}

function br()
{
	$args = func_get_args();
	array_unshift($args, 'br');
	return call_user_func_array('createElement', $args);
}

function button()
{
	$args = func_get_args();
	array_unshift($args, 'button');
	return call_user_func_array('createElement', $args);
}

function canvas()
{
	$args = func_get_args();
	array_unshift($args, 'canvas');
	return call_user_func_array('createElement', $args);
}

function caption()
{
	$args = func_get_args();
	array_unshift($args, 'caption');
	return call_user_func_array('createElement', $args);
}

function center()
{
	$args = func_get_args();
	array_unshift($args, 'center');
	return call_user_func_array('createElement', $args);
}

function cite()
{
	$args = func_get_args();
	array_unshift($args, 'cite');
	return call_user_func_array('createElement', $args);
}

function code()
{
	$args = func_get_args();
	array_unshift($args, 'code');
	return call_user_func_array('createElement', $args);
}

function col()
{
	$args = func_get_args();
	array_unshift($args, 'col');
	return call_user_func_array('createElement', $args);
}

function colgroup()
{
	$args = func_get_args();
	array_unshift($args, 'colgroup');
	return call_user_func_array('createElement', $args);
}

function datalist()
{
	$args = func_get_args();
	array_unshift($args, 'datalist');
	return call_user_func_array('createElement', $args);
}

function dd()
{
	$args = func_get_args();
	array_unshift($args, 'dd');
	return call_user_func_array('createElement', $args);
}

function del()
{
	$args = func_get_args();
	array_unshift($args, 'del');
	return call_user_func_array('createElement', $args);
}

function details()
{
	$args = func_get_args();
	array_unshift($args, 'details');
	return call_user_func_array('createElement', $args);
}

function dfn()
{
	$args = func_get_args();
	array_unshift($args, 'dfn');
	return call_user_func_array('createElement', $args);
}

function div()
{
	$args = func_get_args();
	array_unshift($args, 'div');
	return call_user_func_array('createElement', $args);
}

function dt()
{
	$args = func_get_args();
	array_unshift($args, 'dt');
	return call_user_func_array('createElement', $args);
}

function em()
{
	$args = func_get_args();
	array_unshift($args, 'em');
	return call_user_func_array('createElement', $args);
}

function embed()
{
	$args = func_get_args();
	array_unshift($args, 'embed');
	return call_user_func_array('createElement', $args);
}

function fieldset()
{
	$args = func_get_args();
	array_unshift($args, 'fieldset');
	return call_user_func_array('createElement', $args);
}

function figcaption()
{
	$args = func_get_args();
	array_unshift($args, 'figcaption');
	return call_user_func_array('createElement', $args);
}

function figure()
{
	$args = func_get_args();
	array_unshift($args, 'figure');
	return call_user_func_array('createElement', $args);
}

function font()
{
	$args = func_get_args();
	array_unshift($args, 'font');
	return call_user_func_array('createElement', $args);
}

function footer()
{
	$args = func_get_args();
	array_unshift($args, 'footer');
	return call_user_func_array('createElement', $args);
}

function form()
{
	$args = func_get_args();
	array_unshift($args, 'form');
	return call_user_func_array('createElement', $args);
}

function frame()
{
	$args = func_get_args();
	array_unshift($args, 'frame');
	return call_user_func_array('createElement', $args);
}

function frameset()
{
	$args = func_get_args();
	array_unshift($args, 'frameset');
	return call_user_func_array('createElement', $args);
}

function h1()
{
	$args = func_get_args();
	array_unshift($args, 'h1');
	return call_user_func_array('createElement', $args);
}

function h2()
{
	$args = func_get_args();
	array_unshift($args, 'h2');
	return call_user_func_array('createElement', $args);
}

function h3()
{
	$args = func_get_args();
	array_unshift($args, 'h3');
	return call_user_func_array('createElement', $args);
}

function h4()
{
	$args = func_get_args();
	array_unshift($args, 'h4');
	return call_user_func_array('createElement', $args);
}

function h5()
{
	$args = func_get_args();
	array_unshift($args, 'h5');
	return call_user_func_array('createElement', $args);
}

function h6()
{
	$args = func_get_args();
	array_unshift($args, 'h6');
	return call_user_func_array('createElement', $args);
}

function head()
{
	$args = func_get_args();
	array_unshift($args, 'head');
	return call_user_func_array('createElement', $args);
}

function hgroup()
{
	$args = func_get_args();
	array_unshift($args, 'hgroup');
	return call_user_func_array('createElement', $args);
}

function hr()
{
	$args = func_get_args();
	array_unshift($args, 'hr');
	return call_user_func_array('createElement', $args);
}

function html()
{
	$args = func_get_args();
	array_unshift($args, 'html');
	return call_user_func_array('createElement', $args);
}

function i()
{
	$args = func_get_args();
	array_unshift($args, 'i');
	return call_user_func_array('createElement', $args);
}

function iframe()
{
	$args = func_get_args();
	array_unshift($args, 'iframe');
	return call_user_func_array('createElement', $args);
}

function img()
{
	$args = func_get_args();
	array_unshift($args, 'img');
	return call_user_func_array('createElement', $args);
}

function input()
{
	$args = func_get_args();
	array_unshift($args, 'input');
	return call_user_func_array('createElement', $args);
}

function ins()
{
	$args = func_get_args();
	array_unshift($args, 'ins');
	return call_user_func_array('createElement', $args);
}

function keygen()
{
	$args = func_get_args();
	array_unshift($args, 'keygen');
	return call_user_func_array('createElement', $args);
}

function kbd()
{
	$args = func_get_args();
	array_unshift($args, 'kbd');
	return call_user_func_array('createElement', $args);
}

function label()
{
	$args = func_get_args();
	array_unshift($args, 'label');
	return call_user_func_array('createElement', $args);
}

function legend()
{
	$args = func_get_args();
	array_unshift($args, 'legend');
	return call_user_func_array('createElement', $args);
}

function li()
{
	$args = func_get_args();
	array_unshift($args, 'li');
	return call_user_func_array('createElement', $args);
}

function map()
{
	$args = func_get_args();
	array_unshift($args, 'map');
	return call_user_func_array('createElement', $args);
}

function mark()
{
	$args = func_get_args();
	array_unshift($args, 'mark');
	return call_user_func_array('createElement', $args);
}

function meta()
{
	$args = func_get_args();
	array_unshift($args, 'meta');
	return call_user_func_array('createElement', $args);
}

function meter()
{
	$args = func_get_args();
	array_unshift($args, 'meter');
	return call_user_func_array('createElement', $args);
}

function nav()
{
	$args = func_get_args();
	array_unshift($args, 'nav');
	return call_user_func_array('createElement', $args);
}

function noframes()
{
	$args = func_get_args();
	array_unshift($args, 'noframes');
	return call_user_func_array('createElement', $args);
}

function noscript()
{
	$args = func_get_args();
	array_unshift($args, 'noscript');
	return call_user_func_array('createElement', $args);
}

function object()
{
	$args = func_get_args();
	array_unshift($args, 'object');
	return call_user_func_array('createElement', $args);
}

function ol()
{
	$args = func_get_args();
	array_unshift($args, 'ol');
	return call_user_func_array('createElement', $args);
}

function optgroup()
{
	$args = func_get_args();
	array_unshift($args, 'optgroup');
	return call_user_func_array('createElement', $args);
}

function option()
{
	$args = func_get_args();
	array_unshift($args, 'option');
	return call_user_func_array('createElement', $args);
}

function output()
{
	$args = func_get_args();
	array_unshift($args, 'output');
	return call_user_func_array('createElement', $args);
}

function p()
{
	$args = func_get_args();
	array_unshift($args, 'p');
	return call_user_func_array('createElement', $args);
}

function param()
{
	$args = func_get_args();
	array_unshift($args, 'param');
	return call_user_func_array('createElement', $args);
}

function pre()
{
	$args = func_get_args();
	array_unshift($args, 'pre');
	return call_user_func_array('createElement', $args);
}

function progress()
{
	$args = func_get_args();
	array_unshift($args, 'progress');
	return call_user_func_array('createElement', $args);
}

function q()
{
	$args = func_get_args();
	array_unshift($args, 'q');
	return call_user_func_array('createElement', $args);
}

function rp()
{
	$args = func_get_args();
	array_unshift($args, 'rp');
	return call_user_func_array('createElement', $args);
}

function rt()
{
	$args = func_get_args();
	array_unshift($args, 'rt');
	return call_user_func_array('createElement', $args);
}

function ruby()
{
	$args = func_get_args();
	array_unshift($args, 'ruby');
	return call_user_func_array('createElement', $args);
}

function s()
{
	$args = func_get_args();
	array_unshift($args, 's');
	return call_user_func_array('createElement', $args);
}

function samp()
{
	$args = func_get_args();
	array_unshift($args, 'samp');
	return call_user_func_array('createElement', $args);
}

function section()
{
	$args = func_get_args();
	array_unshift($args, 'section');
	return call_user_func_array('createElement', $args);
}

function select()
{
	$args = func_get_args();
	array_unshift($args, 'select');
	return call_user_func_array('createElement', $args);
}

function small()
{
	$args = func_get_args();
	array_unshift($args, 'small');
	return call_user_func_array('createElement', $args);
}

function source()
{
	$args = func_get_args();
	array_unshift($args, 'source');
	return call_user_func_array('createElement', $args);
}

function span()
{
	$args = func_get_args();
	array_unshift($args, 'span');
	return call_user_func_array('createElement', $args);
}

function strike()
{
	$args = func_get_args();
	array_unshift($args, 'strike');
	return call_user_func_array('createElement', $args);
}

function strong()
{
	$args = func_get_args();
	array_unshift($args, 'strong');
	return call_user_func_array('createElement', $args);
}

function style()
{
	$args = func_get_args();
	array_unshift($args, 'style');
	return call_user_func_array('createElement', $args);
}

function sub()
{
	$args = func_get_args();
	array_unshift($args, 'sub');
	return call_user_func_array('createElement', $args);
}

function summary()
{
	$args = func_get_args();
	array_unshift($args, 'summary');
	return call_user_func_array('createElement', $args);
}

function sup()
{
	$args = func_get_args();
	array_unshift($args, 'sup');
	return call_user_func_array('createElement', $args);
}

function table()
{
	$args = func_get_args();
	array_unshift($args, 'table');
	return call_user_func_array('createElement', $args);
}

function tbody()
{
	$args = func_get_args();
	array_unshift($args, 'tbody');
	return call_user_func_array('createElement', $args);
}

function td()
{
	$args = func_get_args();
	array_unshift($args, 'td');
	return call_user_func_array('createElement', $args);
}

function textarea()
{
	$args = func_get_args();
	array_unshift($args, 'textarea');
	return call_user_func_array('createElement', $args);
}

function tfoot()
{
	$args = func_get_args();
	array_unshift($args, 'tfoot');
	return call_user_func_array('createElement', $args);
}

function th()
{
	$args = func_get_args();
	array_unshift($args, 'th');
	return call_user_func_array('createElement', $args);
}

function thead()
{
	$args = func_get_args();
	array_unshift($args, 'thead');
	return call_user_func_array('createElement', $args);
}

function title()
{
	$args = func_get_args();
	array_unshift($args, 'title');
	return call_user_func_array('createElement', $args);
}

function tr()
{
	$args = func_get_args();
	array_unshift($args, 'tr');
	return call_user_func_array('createElement', $args);
}

function tt()
{
	$args = func_get_args();
	array_unshift($args, 'tt');
	return call_user_func_array('createElement', $args);
}

function u()
{
	$args = func_get_args();
	array_unshift($args, 'u');
	return call_user_func_array('createElement', $args);
}

function ul()
{
	$args = func_get_args();
	array_unshift($args, 'ul');
	return call_user_func_array('createElement', $args);
}

function video()
{
	$args = func_get_args();
	array_unshift($args, 'video');
	return call_user_func_array('createElement', $args);
}

function xmp()
{
	$args = func_get_args();
	array_unshift($args, 'xmp');
	return call_user_func_array('createElement', $args);
}

function htmlheader()
{
	$args = func_get_args();
	array_unshift($args, 'header');
	return call_user_func_array('createElement', $args);
}

function htmllink()
{
	$args = func_get_args();
	array_unshift($args, 'link');
	return call_user_func_array('createElement', $args);
}

function htmldl()
{
	$args = func_get_args();
	array_unshift($args, 'dl');
	return call_user_func_array('createElement', $args);
}

function script()
{
	$args = func_get_args();

	if (count($args) == 0) {
		// script tag must be closed by </script>
		$args = array('<!--//-->');
	}

	array_unshift($args, 'script');
	return call_user_func_array('createElement', $args);
}