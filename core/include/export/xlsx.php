<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Export_Xlsx
{

	public $dom;
	public $root, $table;
	public $head = false;
	public $push_id = true;

	public function __construct()
	{
		$this->dom = new DOMDocument('1.0', 'utf-8');
		$this->dom->formatOutput = true;

		$this->root = $this->dom->createElement('Workbook');
		$this->dom->appendChild($this->root);

		$this->root->setAttribute('xmlns', 'urn:schemas-microsoft-com:office:spreadsheet');
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:x', 'urn:schemas-microsoft-com:office:excel');
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:ss', 'urn:schemas-microsoft-com:office:spreadsheet');
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:html', 'http://www.w3.org/TR/REC-html40');

		$this->addWorksheet('Sheet 1');
	}

	public function addWorksheet($name)
	{
		$ws = $this->dom->createElement('Worksheet');
		$ws->setAttribute('ss:Name', $name);

		$this->table = $this->dom->createElement('Table');
		$ws->appendChild($this->table);

		$this->root->appendChild($ws);
	}

	public function addHeader($columns)
	{
		$row = $this->dom->createElement('Row');

		foreach ($columns as $col) {
			$cell = $this->dom->createElement('Cell');
			$data = $this->dom->createElement('Data', $col);

			$data->setAttribute('ss:Type', 'String');

			$cell->appendChild($data);
			$row->appendChild($cell);
		}

		$this->table->appendchild($row);

		$this->head = true;
	}

	public function addProduct($product, $columns = null)
	{
		if ($columns === null) {
			$columns = array_keys($product->as_array());

			$columns[] = 'attributes';
			$columns[] = 'discounts';
			$columns[] = 'files';
			$columns[] = 'pages';
			$columns[] = 'features';
		}

		if ($this->push_id && !in_array('id', $columns)) {
			array_unshift($columns, 'id');
		}

		if (in_array('attributes', $columns)) {
			array_unshift($columns, 'attribute_product_id');
		}

		if (!$this->head) {
			$head = $columns;

			if (array_search('attributes', $head) !== false) {
				unset($head[array_search('attributes', $head)]);
			}

			$row = $this->dom->createElement('Row');

			foreach ($head as $col) {
				$cell = $this->dom->createElement('Cell');
				$data = $this->dom->createElement('Data', $col);

				$data->setAttribute('ss:Type', 'String');

				$cell->appendChild($data);
				$row->appendChild($cell);
			}

			$this->table->appendchild($row);

			$this->head = true;
		}

		$row = $this->dom->createElement('Row');

		foreach ($columns as $col) {
			$value = '';

			if ($col == 'files') {
				$files = '';
				foreach ($product->product_files() as $file) {
					$files .= $file['filename'] . ',';
				}

				$value = trim($files, ',');
			} else if ($col == 'pages' || $col == 'categories') {
				$pages = '';
				foreach ($product->product_pages() as $page) {
					$pages .= join(' > ', $page->page->model->getFullPath()) . "\n";
				}

				$value = trim($pages);
			} else if ($col == 'attributes') {

			} else if ($product[$col] !== null) {
				$value = $product[$col];
			} else {
				$value = '';
			}


			$cell = $this->dom->createElement('Cell');

			$data = $this->dom->createElement('Data');
			$text = $this->dom->createTextNode($value);
			$data->appendChild($text);

			$data->setAttribute('ss:Type', (!is_numeric($value) ? 'String' : 'Number'));

			$cell->appendChild($data);
			$row->appendChild($cell);
		}

		$this->table->appendchild($row);

		if (in_array('attributes', $columns)) {
			foreach ($product->product_attributes() as $attr) {
				$row = $this->dom->createElement('Row');

				foreach ($columns as $i => $col) {
					$value = '';

					if ($col == 'files') {
						$files = '';
						foreach ($product->product_files() as $file) {
							$files .= $file['filename'] . ',';
						}

						$value = trim($files, ',');
					} else if ($col == 'attribute_product_id') {
						$value = $attr['product_id'];
					} else if ($col == 'name') {
						$value = $attr['name'] . ': ' . $attr['value'];
					} else if ($col == 'attributes') {

					} else if ($attr[$col]) {
						$value = $attr[$col];
					} else {
						$value = '';
					}

					$cell = $this->dom->createElement('Cell');

					$data = $this->dom->createElement('Data');
					$text = $this->dom->createTextNode($value);
					$data->appendChild($text);

					$data->setAttribute('ss:Type', (!is_numeric($value) ? 'String' : 'Number'));

					$cell->appendChild($data);
					$row->appendChild($cell);
				}

				$this->table->appendchild($row);
			}
		}
	}

	public function build()
	{
		return $this->dom->saveXML();
	}

}