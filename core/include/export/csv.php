<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Export_Csv
{

	public $head;
	public $content = '';

	public function __construct()
	{

	}

	public function addHeader($columns)
	{
		if (!in_array('id', $columns)) {
			array_unshift($columns, 'id');
		}

		if (in_array('attributes', $columns)) {
			array_unshift($columns, 'attribute_product_id');
		}

		$this->head = $this->createLine($columns);
	}

	public function addProduct($product, $columns = null)
	{
		if ($columns === null) {
			$columns = array_keys($product->as_array());

			$columns[] = 'attributes';
			$columns[] = 'files';
			$columns[] = 'pages';
		}

		if (!in_array('id', $columns)) {
			array_unshift($columns, 'id');
		}

		if (in_array('attributes', $columns)) {
			array_unshift($columns, 'attribute_product_id');
		}

		if (!$this->head) {
			$head = $columns;

			if (array_search('attributes', $head) !== false) {
				unset($head[array_search('attributes', $head)]);
			}

			$this->head = $this->createLine($head);
		}

		$data = array();
		foreach ($columns as $i => $col) {

			if ($col == 'files') {
				$files = '';
				foreach ($product->product_files() as $file) {
					$files .= $file['filename'] . ',';
				}

				$data[$i] = trim($files, ',');
			} else if ($col == 'pages' || $col == 'categories') {
				$pages = '';
				foreach ($product->product_pages() as $page) {
					if ($page->page['id'] && $page->page['menu'] != 3) {
						$pages .= join(' > ', $page->page->model->getFullPath()) . "\n";
					}
				}

				$data[$i] = trim($pages);
			} else if ($col == 'manufacturers') {
				$pages = '';
				foreach ($product->product_pages() as $page) {
					if ($page->page['id'] && $page->page['menu'] == 3) {
						$pages .= join(' > ', $page->page->model->getFullPath()) . "\n";
					}
				}

				$data[$i] = trim($pages);
			} else if ($col == 'attributes') {

			} else if ($col == 'availability') {
				$data[$i] = $product->availability['name'];
			} else if ($col == 'availability_days') {
				$data[$i] = $product->availability['days'];
			} else if ($product[$col] !== null) {
				$data[$i] = $product[$col];
			} else {
				$data[$i] = '';
			}
		}

		$this->content .= $this->createLine($data);

		if (in_array('attributes', $columns)) {
			foreach ($product->product_attributes() as $attr) {
				$data = array();

				foreach ($columns as $i => $col) {

					if ($col == 'files') {
						$files = '';
						foreach ($product->product_files() as $file) {
							$files .= $file['filename'] . ',';
						}

						$data[$i] = trim($files, ',');
					} else if ($col == 'attribute_product_id') {
						$data[$i] = $attr['product_id'];
					} else if ($col == 'name') {
						$data[$i] = $attr['name'] . ': ' . $attr['value'];
					} else if ($col == 'attributes') {

					} else if ($attr[$col]) {
						$data[$i] = $attr[$col];
					} else {
						$data[$i] = '';
					}
				}

				$this->content .= $this->createLine($data);
			}
		}
	}

	public function build()
	{
		return $this->head . $this->content;
	}

	public function createLine($data)
	{
		foreach ($data as $n => $v) {
			if ($v && !is_numeric($v)) {
				$data[$n] = '"' . preg_replace('/\"/', '""', $v) . '"';
			}
		}

		return iconv('utf-8', 'cp1250', join(';', $data)) . "\n";
	}

}