<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Voucher
{

	public static function generate($event, $customer, $order = null, $email_data = array())
	{
		$total = 0;
		$email = null;

		if (is_object($order) || is_array($order)) {
			$total = $order['total_incl_vat'];
		} else if (is_numeric($order)) {
			$total = (float) $order;
		}

		if (is_object($customer) || is_array($customer)) {
			$email = $customer['email'];
		} else if (is_string($customer)) {
			$email = $customer;
		}

		if (!$email) {
			return false;
		}

		if ((int) Core::config('enable_vouchers')) {
			foreach (Core::$db->voucher_generator()->where('status', 1)->where('event', $event)->order('id DESC') as $generator) {

				if ($generator->model->isValid()) {

					if ($event == 'order_submit' || $event == 'order_finish') {
						$group_id = (int) Core::config('customer_group_default');

						if ($customer['id']) {
							$group_id = (int) Core::config('customer_group_registered');

							if (($gid = (int) $customer['customer_group_id'])) {
								$group_id = $gid;
							}
						}

						if ($generator['customer_group_id'] && $group_id != $generator['customer_group_id']) {
							continue;
						}

						if ($generator['min_price'] && $generator['min_price'] > $total) {
							continue;
						}
					}

					$name = __('voucher_generator') . ' #' . $generator['id'] . ', ' . $email;

					if ($voucher = $generator->model->generate($total, $name)) {

						if (is_object($order)) {
							$email_data = array_merge($order->as_array(), $email_data);
						}

						$email_data['code'] = $voucher['code'];

						if (preg_match('/\%/', $voucher['value'])) {
							$email_data['value'] = $voucher['value'];
						} else {
							$email_data['value'] = (string) price($voucher['value']);
						}

						Email::event('voucher', $email, null, $email_data);

						return $voucher;
					}
				}
			}
		}

		return false;
	}

}