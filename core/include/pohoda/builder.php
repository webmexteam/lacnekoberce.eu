<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Pohoda_Builder
{

	public $schema = array(
		'dat' => 'http://www.stormware.cz/schema/data.xsd',
		'ord' => 'http://www.stormware.cz/schema/order.xsd',
		'typ' => 'http://www.stormware.cz/schema/type.xsd',
		'rsp' => 'http://www.stormware.cz/schema/response.xsd',
		'lst' => 'http://www.stormware.cz/schema/list.xsd',
		'adb' => 'http://www.stormware.cz/schema/addressbook.xsd',
	);

	public function __construct()
	{
		$this->dom = new DOMDocument('1.0', 'utf-8');
		$this->dom->formatOutput = false;
	}

	public function build()
	{
		return $this->dom->saveXML();
	}

}