<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Pohoda_Builder_Address
{

	const version = '1.6';

	protected $data;
	protected $datapack;
	protected $address;
	protected $address_header;

	public function __construct($datapack, $data)
	{
		$this->datapack = $datapack;
		$this->data = $data;

		$item = $this->datapack->addItem('ADDRESS' . $this->data['id']);

		$this->address = $this->datapack->dom->createElementNS($this->datapack->schema['adb'], 'adb:addressbook');
		$this->address->setAttribute('version', self::version);

		$item->appendChild($this->address);

		$this->header();
	}

	private function push($target, $name, $value = null)
	{
		if (preg_match('/^(\w+)\:/', $name, $m)) {
			$value = preg_replace('/\&/', '&amp;', $value);

			$el = $this->datapack->dom->createElementNS($this->datapack->schema[$m[1]], $name, $value);
		} else {
			$el = $this->datapack->dom->createElement($name, $value);
		}

		$target->appendChild($el);

		return $el;
	}

	private function header()
	{
		$this->address_header = $this->push($this->address, 'adb:addressbookHeader');

		$identity = $this->push($this->address_header, 'adb:identity');

		$extid = $this->push($identity, 'typ:extId');
		$this->push($extid, 'typ:ids', $this->data['id']);
		$this->push($extid, 'typ:exSystemName', 'WEBMEX');

		$addr = $this->push($identity, 'typ:address');

		if ($this->data['company']) {
			$this->push($addr, 'typ:company', $this->data['company']);
		} else {
			$this->push($addr, 'typ:company', $this->data['first_name'] . ' ' . $this->data['last_name']);
		}

		$this->push($addr, 'typ:name', $this->data['first_name'] . ' ' . $this->data['last_name']);
		$this->push($addr, 'typ:city', $this->data['city']);
		$this->push($addr, 'typ:street', $this->data['street']);
		$this->push($addr, 'typ:zip', $this->data['zip']);

		if ($this->data['company_id']) {
			$this->push($addr, 'typ:ico', $this->data['company_id']);
		}

		if ($this->data['company_vat']) {
			$this->push($addr, 'typ:dic', $this->data['company_vat']);
		}

		$this->push($this->address_header, 'adb:phone', $this->data['phone']);
		$this->push($this->address_header, 'adb:email', $this->data['email']);

		$duplicity = $this->push($this->address_header, 'adb:duplicityFields');
		$duplicity->setAttribute('actualize', 'true');

		$this->push($duplicity, 'adb:fieldICO', 'true');
		$this->push($duplicity, 'adb:fieldFirma', 'true');
	}

}