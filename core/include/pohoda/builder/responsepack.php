<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Pohoda_Builder_Responsepack extends Pohoda_Builder
{

	const version = '1.2';
	const item_version = '1.0';

	public $root;

	public function __construct($id, $state)
	{
		// $state = ok | error

		parent::__construct();

		$this->root = $this->dom->createElementNS($this->schema['rsp'], 'rsp:responsePack');

		$this->dom->appendChild($this->root);

		foreach ($this->schema as $code => $schema) {
			$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:' . $code, $schema);
		}

		$this->root->setAttribute('version', self::version);
		$this->root->setAttribute('id', $id);
		$this->root->setAttribute('state', $state);
	}

	public function addItem($id, $state)
	{
		$el = $this->dom->createElementNS($this->schema['rsp'], 'rsp:responsePackItem');
		$el->setAttribute('id', $id);
		$el->setAttribute('version', self::item_version);
		$el->setAttribute('state', $state);

		$this->root->appendChild($el);

		return $el;
	}

}