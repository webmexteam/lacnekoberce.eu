<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Servicepack extends Extendable
{

	public $info = array();

	public function install()
	{
		$installed = Registry::get('servicepacks_installed');

		if (!$installed) {
			$installed = array();
		}

		$name = get_class($this);

		if (!in_array($name, $installed)) {
			$installed[] = $name;

			Registry::set('servicepacks_installed', $installed);
		}

		$dir = DOCROOT . 'core/servicepacks/' . strtolower($name) . '.php';

		FS::writable($dir);
		FS::remove($dir);
	}

}