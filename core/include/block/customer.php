<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Block::$drivers[] = 'customer';

class Block_Customer extends Block
{

	private $sent = false;
	public $config = array(
	);

	public function __construct($block = null)
	{
		parent::__construct($block);
	}

	public function getContent()
	{
		$id = $this->block['id'];

		if (!Core::$is_premium) {
			return __('premium_info');
		}

		if (!(int) Core::config('enable_customer_login')) {
			return null;
		}

		return $this->block['text'] . tpl('blocks/customer.php', array('block' => $this));
	}

	public function getConfigForm()
	{
		return null;
	}

}