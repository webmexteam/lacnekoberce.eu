<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Block::$drivers[] = 'pages';

class Block_Pages extends Block
{

	public $config = array(
		'root' => 'menu_2',
		'order_by' => 'position',
		'view' => 'list'
	);

	public function __construct($block = null)
	{
		parent::__construct($block);

		if ($this->config['type']) {
			$this->config['root'] = 'menu_' . $this->config['type'];

			unset($this->config['type']);
		}
	}

	public function getContent()
	{
		$expanded = Core::$active_pages;

		if (Core::$current_page['id'] == PAGE_INDEX && ($pages = Registry::get('block_expanded_pages'))) {
			if ($ids = preg_split('/\s|\,|\;/', $pages)) {
				$expanded = array_merge($expanded, array_map('trim', $ids));
			}
		}

		return $this->block['text'] . tpl('blocks/pages.php', array('pages' => $this->getPages(), 'block' => $this, 'expanded' => $expanded));
	}

	public function getPages($parent_id = null)
	{
		$menu_type = 0;

		if (!$parent_id && is_numeric($this->config['root'])) {
			$parent_id = (int) $this->config['root'];
		} else if (!$parent_id) {
			$menu_type = (int) substr($this->config['root'], 5);
		}


		$pages = Core::$db->page()->where('status', 1)->order($this->config['order_by'] . ' ASC');

		if ($parent_id) {
			return $pages->where('parent_page', $parent_id);
		} else {
			return $pages->where('menu', $menu_type);
		}
	}

	public function getConfigForm()
	{
		$tree = $this->pageTree();

		return tpl('blocks/config_pages.php', array('block' => $this, 'page_tree' => $tree));
	}

	private function pageTree($parent_id = 0, $level = 1)
	{
		$out = '';
		$order = 'position ASC';

		if ($level == 1) {
			foreach (Core::$def['page_types'] as $type_id => $type) {
				$pages = Core::$db->page()->where('parent_page', $parent_id)->where('menu', $type_id)->order($order);

				if (count($pages)) {
					$out .= '<option value="menu_' . $type_id . '"' . ($this->config['root'] == 'menu_' . $type_id ? ' selected="selected"' : '') . '>' . __('menu') . ': ' . $type . '</option>';

					foreach ($pages as $page) {
						$out .= '<option value="' . $page['id'] . '"' . ($this->config['root'] == $page['id'] ? ' selected="selected"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

						$out .= $this->pageTree($page['id'], $level + 1);
					}
				}
			}
		} else {
			foreach (Core::$db->page()->where('parent_page', $parent_id)->order($order) as $page) {
				$out .= '<option value="' . $page['id'] . '"' . ($this->config['root'] == $page['id'] ? ' selected="selected"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

				$out .= $this->pageTree($page['id'], $level + 1);
			}
		}

		return $out;
	}

}