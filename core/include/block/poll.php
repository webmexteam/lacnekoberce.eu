<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */


Block::$drivers[] = 'poll';

class Block_Poll extends Block {
	
	public $config = array(
		'question' => '',
		'answers' => array()
	);
	
	public function __construct($block = null)
	{
		parent::__construct($block);
		
		if(isSet($_GET['poll']) && isSet($_GET['answer']) && $this->block && $_GET['poll']== $this->block['id']){
			if(! preg_match('/(alexa|googlebot|crawler|gigabot|msnbot|slurp|seznambot)/i', $_SERVER['HTTP_USER_AGENT'])){
				$this->vote((int) $_GET['answer']);
			}
		}
	}
	
	public function getContent()
	{
		$id = $this->block['id'];
		$disabled = false;
		
		if(isSet($_SESSION['poll_'.$id]) && $_SESSION['poll_'.$id] >= strtotime('-24 hours')){
			$disabled = true;
			
		}else if(isSet($_COOKIE['poll_'.$id]) && $_COOKIE['poll_'.$id] >= strtotime('-24 hours')){
			$disabled = true;
		}
		
		return $this->block['text'].tpl('blocks/poll.php', array('block' => $this, 'disabled' => $disabled));
	}
	
	public function getConfigForm()
	{
		return tpl('blocks/config_poll.php', array('block' => $this));
	}
	
	public function getTotalVotes()
	{
		$total = 0;
		
		foreach($this->config['answers'] as $i => $answer){
			$total += (int) $answer['votes'];
		}
		
		return $total;
	}
	
	public function vote($answer_id)
	{
		$id = $this->block['id'];
		
		if($this->config['answers'][$answer_id]){
			if(
				(! isSet($_SESSION['poll_'.$id]) || $_SESSION['poll_'.$id] < strtotime('-24 hours'))
				&& (! isSet($_COOKIE['poll_'.$id]) || $_COOKIE['poll_'.$id] < strtotime('-24 hours'))
			){
				$this->config['answers'][$answer_id]['votes'] ++;
				
				setcookie('poll_'.$id, time(), strtotime('+24 hours'));
				$_SESSION['poll_'.$id] = time();
				
				$this->saveConfig();
			}
		}
	}
}