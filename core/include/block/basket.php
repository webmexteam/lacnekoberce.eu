<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Block::$drivers[] = 'basket';

class Block_Basket extends Block
{

	public $config = array(
	);

	public function __construct($block = null)
	{
		parent::__construct($block);
	}

	public function getContent()
	{
		$id = $this->block['id'];

		$basket_products = Basket::products();
		$total = Basket::total();

		return $this->block['text'] . tpl('blocks/basket.php', array('block' => $this, 'basket_products' => $basket_products, 'total' => $total));
	}

	public function getConfigForm()
	{
		return null;
	}

}