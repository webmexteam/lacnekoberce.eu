<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Block::$drivers[] = 'products';

class Block_Products extends Block
{

	private $sent = false;
	public $config = array(
		'max' => 3,
		'orderby' => 'random', // random, latest, top_selling
		'promoted' => 0,
		'page_id' => 0
	);

	public function getContent()
	{
		$id = $this->block['id'];

		if ((int) $this->config['page_id']) {
			$page = Core::$db->page[(int) $this->config['page_id']];
			$products = Core::$db->product()->where('id', $page->product_pages()->select('product_id'))->where('status', 1)->limit($this->config['max']);
		} else {
			$products = Core::$db->product()->where('status', 1)->limit($this->config['max']);
		}

		if ((int) $this->config['promoted']) {
			$products->where('promote', 1);
		}

		if ($this->config['orderby'] == 'random') {
			$products->order(Core::$db_inst->_type == 'sqlite' ? 'RANDOM()' : 'RAND()');
		} else if ($this->config['orderby'] == 'latest') {
			$products->order('id DESC');
		} else if ($this->config['orderby'] == 'top_selling') {
			$products->order('sold_qty DESC');
		}

		return $this->block['text'] . tpl('blocks/products.php', array('block' => $this, 'products' => $products));
	}

	public function getConfigForm()
	{
		$pages = $this->_pageTree($this->config['page_id']);

		return tpl('blocks/config_products.php', array('block' => $this, 'pages' => $pages));
	}

	private function _pageTree($selected, $parent_id = 0, $level = 0)
	{
		$out = '';

		$pages = Core::$db->page()->where('parent_page', $parent_id)->order('position ASC, name ASC');

		if ($level == 0) {
			$pages->where('menu', 2);
			$out .= '<option value="0"' . ($selected == 0 ? ' selected="selected"' : '') . '>' . __('all') . '</option>';
		}

		if (count($pages)) {
			foreach ($pages as $page) {
				$out .= '<option value="' . $page['id'] . '"' . ($page['id'] == $selected ? ' selected="selected"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

				$out .= $this->_pageTree($selected, $page['id'], $level + 1);
			}
		}

		return $out;
	}

}