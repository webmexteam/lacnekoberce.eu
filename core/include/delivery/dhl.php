<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Delivery::$drivers[] = 'dhl';

class Delivery_Dhl extends Delivery
{

	public $name = 'DHL';

	public function getTrackLink($track_num = null)
	{
		if (!$track_num) {
			$track_num = $this->order['track_num'];
		}

		return 'http://www.dhl.cz/content/cz/cs/logistika/zakaznicka_domena/sledovan_zasilky_a_aplikace/sledovani_zasilky.html?brand=DOC&AWB=' . $track_num;
	}

}