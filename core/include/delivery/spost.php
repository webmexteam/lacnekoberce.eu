<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Delivery::$drivers[] = 'spost';

class Delivery_Spost extends Delivery
{

	public $name = 'Slovenská pošta';

	public function getTrackLink($track_num = null)
	{
		if (!$track_num) {
			$track_num = $this->order['track_num'];
		}

		return 'http://tandt.posta.sk/?q=' . $track_num;
	}

}