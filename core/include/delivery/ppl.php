<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Delivery::$drivers[] = 'ppl';

class Delivery_Ppl extends Delivery
{

	public $name = 'PPL';

	public function getTrackLink($track_num = null)
	{
		if (!$track_num) {
			$track_num = $this->order['track_num'];
		}

		return 'http://www.ppl.cz/main2.aspx?cls=Package&idSearch=' . $track_num;
	}

}