<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Delivery::$drivers[] = 'dpd';

class Delivery_Dpd extends Delivery
{

	public $name = 'DPD';

	public function getTrackLink($track_num = null)
	{
		if (!$track_num) {
			$track_num = $this->order['track_num'];
		}

		return 'https://extranet.dpd.de/cgi-bin/delistrack?typ=1&lang=cz&pknr=' . $track_num;
	}

}