<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class WebmexAPI_Model_Product extends WebmexAPI_Model
{

	public $table_name = 'product';
	public static $page_map = array();
	public static $pages = array(), $files = array();

	public function save($data, $unique = 'id', $return_item = true, $callback = null, $mode = 0, & $inserted = false)
	{
		$result = parent::save($data, $unique, $return_item, $callback, $mode, true, $inserted);

		if ($result) {
			if (isset($data['files'])) {
				$this->setFiles($result, $data['files']);
			}

			if (isset($data['categories'])) {
				$this->setPages($result, $data['categories'], false, array('menu' => 2, 'products' => 1));
			}

			if (isset($data['pages'])) {
				$this->setPages($result, (array) $data['pages']);
			}

			if (isset($data['manufacturers'])) {
				$this->setPages($result, $data['manufacturers'], false, array('menu' => 3, 'products' => 1));
			}

			if (isset($data['attributes']) && is_array($data['attributes'])) {
				$this->setAttributes($result, $data['attributes']);
			}

			if (isset($data['features'])) {
				$this->setFeatures($result, $data['features']);
			}

			if (isset($data['labels'])) {
				$this->setLabels($result, $data['labels']);
			}

			if (isset($data['related'])) {
				$this->setRelated($result, $data['related']);
			}
		}

		return $result;
	}

	public function insert($data, $return_item = false)
	{
		if (empty($data['name'])) {
			throw new Exception('Required param: "name"');
		}

		$data = $this->sanitize($data);

		if (empty($data['sef_url'])) {
			$data['sef_url'] = dirify($data['name'] . ' ' . $data['nameext']);
		}

		if (!isSet($data['status'])) {
			$data['status'] = 1;
		}

		if (!isSet($data['show_sku'])) {
			$data['show_sku'] = 1;
		}

		if (!isSet($data['vat_id'])) {
			$data['vat_id'] = (int) Core::config('vat_rate_default');
		}

		$product_id = parent::insert($data, $return_item);

		if ($product_id) {

			try {
				Search::indexProduct(array_merge(array(
							'id' => is_numeric($product_id) ? $product_id : $product_id['id'],
							'name' => '',
							'description_short' => '',
							'description' => ''
								), $data));
			} catch (PDOException $e) {

			}
		}

		return $product_id;
	}

	public function update($find_by, $data, $callback = null)
	{
		$data = $this->sanitize($data);

		$product = parent::update($find_by, $data, $callback);

		if ($product) {

			if ($product['featureset_id'] && $data['featureset_id'] && $product['featureset_id'] != $data['featureset_id']) {
				$tbl_name = 'features_' . (int) $product['featureset_id'];

				try {
					Core::$db->$tbl_name()->where('product_id', $product['id'])->limit(1)->delete();
				} catch (PDOException $e) {

				}
			}

			try {
				Search::indexProduct($product);
			} catch (PDOException $e) {

			}

			return $product;
		}

		return false;
	}

	private function sanitize($data)
	{
		if (isset($data['name'])) {
			$data['name'] = preg_replace('/\s{2,}/', ' ', trim($data['name']));
		}

		if (isset($data['nameext'])) {
			$data['nameext'] = preg_replace('/\s{2,}/', ' ', trim($data['nameext']));
		}

		if (User::$logged_in) {
			$data['last_change'] = time();
			$data['last_change_user'] = User::get('id');
		}

		if (!empty($data['sku'])) {
			$data['sku'] = trim($data['sku']);
		}

		if (!empty($data['ean13'])) {
			$data['ean13'] = preg_replace('/\s/', '', trim($data['ean13']));
		}

		if (isset($data['stock']) && $data['stock'] === '') {
			$data['stock'] = null;
		}

		if (!empty($data['availability'])) {
			$data['availability_id'] = $this->findAvailability($data['availability']);
		}

		if (!empty($data['availability_days'])) {
			$data['availability_id'] = $this->findAvailabilityByDays($data['availability_days']);
		}

		if (!empty($data['discount_date']) && !is_numeric($data['discount_date'])) {
			$data['discount_date'] = strtotime($data['discount_date']);
		}

		if (!empty($data['vat'])) {
			$data['vat_id'] = $this->findVatByRate($data['vat']);
		}

		return $data;
	}

	public function findAvailability($id_or_name)
	{
		if (is_numeric($id_or_name)) {
			$availability = Core::$db->availability[(int) $id_or_name];
		} else {
			$availability = Core::$db->availability()->where('name LIKE "' . $id_or_name . '"')->limit(1)->fetch();
		}

		return $availability;
	}

	public function findAvailabilityByDays($days)
	{
		return Core::$db->availability()->where('days', (int) $days)->limit(1)->fetch();
	}

	public function findVatByRate($vat_rate)
	{
		return Core::$db->vat()->where('rate', (float) $vat_rate)->limit(1)->fetch();
	}

	public function delete($find_by, $value = false)
	{
		if ($product = $this->find($find_by, $value)) {

			$product->product_attributes()->delete();
			$product->product_files()->delete();
			$product->product_pages()->delete();
			$product->product_discounts()->delete();
			$product->product_labels()->delete();
			$product->product_related()->delete();

			if ($product['featureset_id']) {
				$tbl_name = 'features_' . $product['featureset_id'];
				Core::$db->$tbl_name()->where('product_id', $product['id'])->delete();
			}

			$product->search_index()->delete();

			$product->delete();

			return $product;
		}

		return false;
	}

	public function setPages($product, $pages, $delete_pages = true, array $options = array())
	{
		if ($pages instanceof WebmexAPI_Bucket) {
			$options = array_merge($options, $pages->options);
		}

		$options = array_merge(array(
			'parent_page' => 0,
			'menu' => 2,
			'map_id' => null
				), $options);

		if (!$options['parent_page'] && $options['parent_page'] !== 0) {
			$options['parent_page'] = 0;
		} else if ($options['parent_page']) {
			$options['parent_page'] = (int) $options['parent_page'];
		}

		if ($options['map_id']) {
			self::$page_map = Registry::get('pagemap_' . $options['map_id']);
			$save_map = false;

			if (!self::$page_map) {
				self::$page_map = array();
			}
		}

		if (!is_object($product)) {
			$product = $this->find($product);
		}

		$old_pages = array();

		if (WebmexAPI::$bulk_mode && isset(self::$pages[$product['id']])) {
			$old_pages = self::$pages[$product['id']];
		} else {
			foreach ($product->product_pages() as $page) {
				$old_pages[] = $page['page_id'];
			}

			if (WebmexAPI::$bulk_mode) {
				self::$pages[$product['id']] = $old_pages;
			}
		}

		$allow_insert = (isBucket($pages) && !$pages->allow_insert) ? false : true;

		$current_pages = array();
		foreach ($pages as $page) {
			$page_id = null;

			if (is_object($page) || (is_array($page) && isset($page['id']))) {
				$page_id = $page['id'];
			} else if (is_numeric($page)) {
				$page_id = (int) $page;
			} else if (is_array($page) && is_string($page[0])) {
				$map_key = $options['parent_page'] . '_' . join('/', $page);

				if (isset(self::$page_map) && !empty(self::$page_map[$map_key])) {
					$page_id = self::$page_map[$map_key];
				} else if ($_page = Core::$api->page->createPath($page, $options['parent_page'], ($options['menu'] ? $options['menu'] : 0), $allow_insert)) {
					$page_id = $_page;

					self::$page_map[$map_key] = $page_id;
					$save_map = true;
				} else {
					continue;
				}
			}

			$current_pages[] = $page_id;

			if ($page_id && !in_array($page_id, $old_pages)) {
				try {
					Core::$db->product_pages(array(
						'product_id' => $product['id'],
						'page_id' => (int) $page_id
					));
				} catch (PDOException $e) {

				}
			}
		}

		if (WebmexAPI::$bulk_mode) {
			self::$pages[$product['id']] = $current_pages;
		}

		if ($current_pages && $delete_pages) {
			$product->product_pages()->where('page_id NOT IN (' . join(',', $current_pages) . ')')->delete();
		} else if ($delete_pages) {
			$product->product_pages()->delete();
		}

		if ($options['menu'] == 2 && $current_pages && (!$product['default_page'] || !in_array($product['default_page'], $current_pages))) {
			$product->update(array('default_page' => current($current_pages)));
		}

		if ($options['map_id'] && isset(self::$page_map) && $save_map) {
			Registry::set('pagemap_' . $options['map_id'], self::$page_map, true, true);
		}

		return $current_pages;
	}

	public function setFiles($product, array $files)
	{
		if (!is_object($product)) {
			$product = $this->find($product);
		}

		$old_files = array();

		if (WebmexAPI::$bulk_mode && isset(self::$files[$product['id']])) {
			$old_files = self::$files[$product['id']];
		} else {
			foreach ($product->product_files() as $file) {
				$old_files[] = $file['filename'];
			}

			if (WebmexAPI::$bulk_mode) {
				self::$files[$product['id']] = $old_files;
			}
		}

		$current_files = array();
		foreach ($files as $file) {
			if (!is_array($file)) {
				$file = array('filename' => $file);
			}

			if (!$file['filename']) {
				continue;
			}

			$file = Arr::overwrite(array(
						'filename' => '',
						'description' => '',
						'position' => 0,
						'is_image' => null,
						'size' => -1,
						'align' => 2,
						'directory' => '',
						'target' => '',
						'source' => '',
						'lazy' => false
							), $file);

			$already_exists = false;

			$source = $file['source'] ? $file['source'] : $file['filename'];
			$dir = $file['target'] ? array($file['directory'], $file['target']) : $file['directory'];

			$file['filename'] = Core::$api->copyFile($source, $dir, $file['is_image'], $already_exists, $file['lazy']);

			if (!$file['filename']) {
				continue;
			}

			if ($file['is_image'] === null) {
				$file['is_image'] = (bool) preg_match('/\.(jpe?g|gif|png)$/i', $file['filename']);
			}

			unset($file['directory'], $file['lazy'], $file['target'], $file['source']);

			if ($file['filename'] && in_array($file['filename'], $old_files)) {
				$current_files[] = $file['filename'];
				Core::$db->product_files()->where('product_id', $product['id'])->where('filename', $file['filename'])->update($file);
			} else if ($file['filename']) {
				$file['product_id'] = $product['id'];
				$file_id = Core::$db->product_files($file);
				$current_files[] = $file['filename'];

				$info = pathinfo(DOCROOT . $file['filename']);

				if ($file['is_image'] && !file_exists($info['dirname'] . '/_' . Core::$def['image_sizes'][2] . '/' . $info['basename'])) {
					try {
						Image::thumbs(DOCROOT . $file['filename']);
					} catch (Exception $e) {

					}
				}
			}
		}

		if (WebmexAPI::$bulk_mode) {
			self::$files[$product['id']] = $current_files;
		}

		if ($current_files) {
			$product->product_files()->where('filename NOT IN (' . join(', ', array_map(array(Core::$db_conn, 'quote'), $current_files)) . ')')->delete();
		} else {
			$product->product_files()->delete();
		}

		return $current_files;
	}

	public function setAttributes($product, array $attributes, $delete_attributes = true)
	{
		if (!is_object($product)) {
			$product = $this->find($product);
		}

		if (!$product) {
			return false;
		}

		$old_attributes = array();
		foreach ($product->product_attributes() as $attr) {
			$old_attributes[] = $attr['id'];
		}

		$current_attributes = array();
		foreach ($attributes as $attribute) {

			if (!empty($attribute['availability'])) {
				$attribute['availability_id'] = $this->findAvailability($attribute['availability']);
			}

			if (!empty($attribute['availability_days'])) {
				$attribute['availability_id'] = $this->findAvailabilityByDays($attribute['availability_days']);
			}

			$attr = Arr::overwrite(array(
						'id'              => null,
						'name'            => '',
						'value'           => '',
						'price'           => null,
						'weight'          => 0,
						'is_default'      => 0,
						'sku'             => null,
						'ean13'           => null,
						'stock'           => null,
						'availability_id' => null,
						'file_id'         => null,
						'filename'        => null
							), $attribute);

			$attr['name'] = trim($attr['name']);
			$attr['value'] = trim($attr['value']);

			if ($attr['name'] !== '' && $attr['value'] !== '') {
				if (array_key_exists('filename', $attr)) {
					if ($attr['filename'] && $file = Core::$db->product_files()->where('product_id', $product['id'])->where('filename', $attr['filename'])->fetch()) {
						$attr['file_id'] = $file['id'];
					}

					unset($attr['filename']);
				}

				if ($attr['stock'] === '') {
					$attr['stock'] = null;
				}

				$found_attr = null;

				if ($attr['id'] && in_array($attr['id'], $old_attributes)) {
					$found_attr = Core::$db->product_attributes[$attr['id']];
				} else if ($attr['sku']) {
					$found_attr = Core::$db->product_attributes()->where('product_id', $product['id'])->where('sku', $attr['sku'])->limit(1)->fetch();
				} else if ($attr['ean13']) {
					$found_attr = Core::$db->product_attributes()->where('product_id', $product['id'])->where('ean13', $attr['ean13'])->limit(1)->fetch();
				}

				if (!$found_attr) {
					$found_attr = Core::$db->product_attributes()->where('product_id', $product['id'])->where('name', $attr['name'])->where('value', $attr['value'])->limit(1)->fetch();
				}

				if ($found_attr) {
					$current_attributes[] = $found_attr['id'];
					unset($attr['id']);

					if ($found_attr->needUpdate($attr)) {
						if (isBucket($attribute)) {
							$attribute->updated = true;
						}

						$found_attr->update($attr);
					}
				} else {
					$attr['product_id'] = $product['id'];
					unset($attr['id']);
					$current_attributes[] = Core::$db->product_attributes($attr);

					if (isBucket($attribute)) {
						$attribute->inserted = true;
					}
				}
			}
		}

		if ($current_attributes && $delete_attributes) {
			$product->product_attributes()->where('id NOT IN (' . join(', ', $current_attributes) . ')')->delete();
		} else if ($delete_attributes) {
			$product->product_attributes()->delete();
		}

		return $current_attributes;
	}

	public function setFeatures($product, $features, $featureset_id = null)
	{
		if (!is_object($product)) {
			$product = $this->find($product);
		}

		if (!$featureset_id && isBucket($features)) {
			$featureset_id = $features->featureset_id;
		}

		if (!is_numeric($featureset_id)) {
			if ($fid = Registry::get('featureset_' . $featureset_id)) {
				$featureset_id = (int) $fid;
			} else {
				if ($fid = Core::$api->featureset->insert(array('name' => 'import ' . $featureset_id))) {
					Registry::set('featureset_' . $featureset_id, $fid);
					$featureset_id = (int) $fid;
				}
			}
		}

		if (!(int) $featureset_id) {
			return false;
		}

		$featureset_id = (int) $featureset_id;

		$tbl_name = 'features_' . $featureset_id;
		$features_row = null;

		if (isBucket($features)) {
			$features = $features->as_array();
		}

		if (is_array($features[0])) {
			$indexed_features = array();

			// array(name, value, type, unit, filter)

			foreach ($features as $i => $feature) {
				if ($f = Core::$db->feature()->where('featureset_id', $featureset_id)->where('name', trim($feature[0]))->limit(1)->fetch()) {
					$indexed_features['f' . $f['id']] = trim($feature[1]);
				} else {
					$fid = Core::$db->feature(array(
						'name' => trim($feature[0]),
						'featureset_id' => $featureset_id,
						'type' => !empty($feature[2]) ? $feature[2] : 'string',
						'unit' => !empty($feature[3]) ? $feature[3] : NULL,
						'filter' => (!empty($feature[4]) && $feature[4]) ? 1 : 0
							));

					$indexed_features['f' . $fid] = trim($feature[1]);
				}
			}

			$features = $indexed_features;
		} else if (key($features) > 0) {
			$indexed_features = array();

			foreach ($features as $fid => $value) {
				$indexed_features['f' . $fid] = trim($value);
			}

			$features = $indexed_features;
		}

		Core::$api->featureset->createTable($featureset_id);

		if (empty($indexed_features)) {
			return false;
		}

		if ((int) $featureset_id && ($features_row = Core::$db->$tbl_name()->where('product_id', $product['id'])->limit(1)->fetch())) {
			$features_row->result->primary = 'product_id';
		}

		if ((int) $product['featureset_id'] && (int) $product['featureset_id'] != (int) $featureset_id) {
			$old_tbl_name = 'features_' . (int) $product['featureset_id'];

			Core::$db->$old_tbl_name()->where('product_id', $product['id'])->delete();
		}

		if ($features_row) {
			$features_row->update($features);
		} else if ((int) $featureset_id && (int) $product['id']) {
			$features['product_id'] = (int) $product['id'];

			Core::$db->$tbl_name($features);
		}

		if ((int) $product['id']) {
			$this->update($product['id'], array('featureset_id' => $featureset_id));
		}
	}

	public function setLabels($product, array $labels)
	{
		if (!is_object($product)) {
			$product = $this->find($product);
		}

		$old_labels = array();
		foreach ($product->product_labels() as $l) {
			$old_labels[] = $l['label_id'];
		}

		$current_labels = array();
		foreach ($labels as $label_id) {
			if (is_numeric($label_id)) {
				$label = Core::$db->label[(int) $label_id];
			} else {
				$label = Core::$db->label()->where('name', $label_id)->fetch();
			}

			if ($label) {
				if (!in_array($label['id'], $old_labels)) {
					Core::$db->product_labels(array(
						'product_id' => $product['id'],
						'label_id' => $label['id']
					));
				}

				$current_labels[] = $label['id'];
			}
		}

		$delete = $product->product_labels();

		if ($current_labels) {
			$delete->where('label_id NOT IN (' . join(', ', $current_labels) . ')');
		}

		$delete->delete();
	}

	public function setRelated($product, array $related)
	{
		if (!is_object($product)) {
			$product = $this->find($product);
		}

		$old_related = array();
		foreach ($product->product_related() as $r) {
			$old_related[] = $r['related_id'];
		}

		$current_related = array();
		foreach ($related as $id) {
			if ((int) $id && !in_array($id, $old_related)) {
				Core::$db->product_related(array(
					'product_id' => $product['id'],
					'related_id' => $id
				));
			}

			$current_related[] = (int) $id;
		}

		$delete = $product->product_related();

		if ($current_related) {
			$delete->where('related_id NOT IN (' . join(', ', $current_related) . ')');
		}

		$delete->delete();
	}

	public function setQuantityDiscounts($product, array $discounts)
	{
		if (!is_object($product)) {
			$product = $this->find($product);
		}

		$old_discounts = array();
		foreach ($product->product_discounts() as $discount) {
			$old_discounts[$discount['quantity']] = $discount['id'];
		}

		$current_discounts = array();
		foreach ($discounts as $discount) {
			if (is_array($discount) && is_numeric(key($discount))) {
				$discount = array('quantity' => key($discount), 'value' => current($discount));
			}

			if (!$discount['quantity'] || !$discount['value']) {
				continue;
			}

			if (isset($old_discounts[$discount['quantity']])) {
				$current_discounts[] = $old_discounts[$discount['quantity']];

				Core::$db->product_discounts()->where('product_id', $product['id'])->where('id', $old_discounts[$discount['quantity']])->update($discount);
			} else {
				$discount['product_id'] = $product['id'];

				$current_discounts[] = Core::$db->product_discounts($discount);
			}
		}

		$delete = $product->product_discounts();

		if ($current_discounts) {
			$delete->where('id NOT IN (' . join(', ', $current_discounts) . ')');
		}

		$delete->delete();

		return $current_discounts;
	}

	public function updateURLs()
	{
		$offset = 0;
		$products = null;

		while (($products = Core::$db->product()->limit(500, $offset)) && count($products)) {
			foreach ($products as $product) {
				$product->update(array(
					'sef_url' => dirify($product['name'] . ' ' . $product['nameext'])
				));
			}

			$offset += 500;
		}

		return true;
	}

}