<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
if (!class_exists('Core', false)) {
	error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);

	ini_set('display_errors', true);

	$pathinfo = pathinfo(__FILE__);
	$dirname = substr($pathinfo['dirname'], 0, -4);

	define('SQC', true);
	define('WEBMEX', true);
	define('API', true);
	define('DOCROOT', $dirname . '/');
	define('APPDIR', 'core');
	define('APPROOT', DOCROOT . APPDIR . '/');

	set_include_path(DOCROOT);

	require_once(APPROOT . 'include/core.php');

	Core::setup(true);
}

class WebmexAPI
{

	protected $models = array();
	public static $inst = null;
	public static $bulk_mode = false;

	public function __construct()
	{
		if (self::$inst === null) {
			self::$inst = $this;
		}
	}

	public static function model($model)
	{
		return self::$inst->$model;
	}

	public function __get($key)
	{
		$key = strtolower($key);

		$classname = 'WebmexAPI_Model_' . ucfirst($key);

		if (class_exists($classname)) {
			if (!$this->models[$key]) {
				$this->models[$key] = new $classname;
			}

			return $this->models[$key];
		}

		return null;
	}

	public function bucket($data, array $options = array())
	{
		return new WebmexAPI_Bucket($data, $options);
	}

	public function copyFile($path, $_target = '', $is_image = null, & $alredy_exists = false, $lazy_static_file = false)
	{
		$target = $_target;

		if (is_array($_target)) {
			$target = $_target[0];
		}

		$target_dir = trim('files/' . trim($target, ' /'), ' /') . '/';
		$target_filename = null;
		$is_http = false;
		$status = false;
		$image_extensions = array('jpg', 'jpeg', 'gif', 'png');

		if (preg_match('/^(http|ftp)s?:\/\//', $path)) {
			$path = preg_replace('/\s/', '%20', $path);

			$is_http = true;
			$info = parse_url($path);
			$path_hash = md5($path);
			$path_hash_short = substr($path_hash, 0, 5) . substr($path_hash, -5);

			if (preg_match('/([^\/]+\.\w{3,5})$/i', $info['path'], $m)) {
				$target_filename = $path_hash_short . '_' . trim($m[1]);
			} else if (preg_match('/([^\/\?\&\=]+\.(jpe?g|gif|png|pdf|zip|doc|xls|xml|txt|html))/i', $path, $m)) {
				$target_filename = $path_hash_short . '_' . trim($m[1]);
			} else {
				$target_filename = $path_hash . '.jpg';
			}
		} else {
			$info = pathinfo($path);

			$target_filename = $info['basename'];
		}

		if (is_array($_target) && $_target[1]) {
			$target_filename = $_target[1];
		}

		$target_filename = preg_replace('/[\s\[\]\(\)\%]/', '_', urldecode($target_filename));

		$target_info = pathinfo($target_filename);
		$extension = strtolower($target_info['extension']);

		if (!$target && preg_match('/^files\/?/i', $info['dirname'])) {
			$target_dir = rtrim($info['dirname'], '/') . '/';
		} else if (!$target && preg_match('/^' . preg_replace('/\//', '\\/', DOCROOT . 'files/?') . '/i', $info['dirname'])) {
			$target_dir = rtrim(substr($info['dirname'], strlen(DOCROOT)), '/') . '/';
		}

		if (preg_match('/\*/', $target)) {
			$d = strtolower(substr(preg_replace('/[^\w]/', '', $target_info['basename']), 0, 2)) . '/';

			$target_dir = preg_replace('/\*/', $d, $target_dir);
		}

		$target_dir = preg_replace('/\/\//', '/', $target_dir);

		if (!is_dir(DOCROOT . $target_dir)) {
			FS::mkdir(DOCROOT . $target_dir, true);
		}

		if (file_exists(DOCROOT . $target_dir . $target_filename)) {
			$alredy_exists = true;

			FS::writable(DOCROOT . $target_dir . $target_filename);

			if (($is_image === true || ($is_image === null && in_array($extension, $image_extensions))) && !file_exists(DOCROOT . $target_dir . '_60x60/' . $target_filename)) {
				try {
					Image::thumbs(DOCROOT . $target_dir . $target_filename);
				} catch (Exception $e) {

				}
			}

			return $target_dir . $target_filename;
		}

		if ($is_http) {
			if ($lazy_static_file) {
				$status = true;

				try {
					Core::$db->static_file(array(
						'localname' => preg_replace('#^files/#', '', $target_dir . $target_filename),
						'url' => $path
					));
				} catch (PDOException $e) {
					$alredy_exists = true;
				}
			} else {
				if (function_exists('curl_init') && ($curl = curl_init($path))) {
					$target_fp = fopen(DOCROOT . $target_dir . $target_filename, 'w');

					curl_setopt($curl, CURLOPT_FILE, $target_fp);
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_TIMEOUT, 6);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_exec($curl);
					curl_close($curl);

					fclose($target_fp);

					$status = (file_exists(DOCROOT . $target_dir . $target_filename) && is_readable(DOCROOT . $target_dir . $target_filename));
				} else {
					$context = stream_context_create(array(
						'http' => array(
							'timeout' => 6
						)
							));

					if ($c = @file_get_contents($path, false, $context)) {
						$status = file_put_contents(DOCROOT . $target_dir . $target_filename, $c);
					}
				}
			}
		} else {
			$status = @remoteCopy($path, DOCROOT . $target_dir . $target_filename);
		}

		if (!$status) {
			return false;
		}

		if (!$lazy_static_file) {
			FS::writable(DOCROOT . $target_dir . $target_filename);

			if ($is_image === true || ($is_image === null && in_array($extension, $image_extensions))) {
				try {
					Image::thumbs(DOCROOT . $target_dir . $target_filename);
				} catch (Exception $e) {

				}
			}
		}

		return $target_dir . $target_filename;
	}

}