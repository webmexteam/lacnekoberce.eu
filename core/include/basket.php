<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Basket
{

	protected static $basket;

	public static function id($autocreate = true)
	{
		if (self::$basket) {
			return (int) self::$basket['id'];
		}

		$key = md5($_SERVER['HTTP_USER_AGENT']);

		if (!empty($_SESSION['basket_id'])) {
			self::$basket = Core::$db->basket[(int) $_SESSION['basket_id']];
		} else if (!$autocreate) {
			return null;
		}

		if (!self::$basket || self::$basket['key'] != $key) {
			self::$basket = self::create();

			$_SESSION['basket_id'] = self::$basket['id'];
		}

		return (int) self::$basket['id'];
	}

	private static function create()
	{
		if (mt_rand(1, 10) == 1) {
			foreach (Core::$db->basket()->where('last_update <= ' . strtotime('-3 days')) as $b) {
				$b->basket_products()->delete();
				$b->delete();
			}
		}

		$basket_id = Core::$db->basket(array(
			'last_update' => time(),
			'key' => md5($_SERVER['HTTP_USER_AGENT'])
				));

		return Core::$db->basket[$basket_id];
	}

	public static function destroy()
	{
		if ($basket_id = self::id(false)) {
			$basket = Core::$db->basket[$basket_id];

			if ($basket) {
				$basket->basket_products()->delete();
				$basket->delete();
			}

			unset($_SESSION['basket_id']);
		}
	}

	public static function add($product_id, $attributes = null, $qty = 1)
	{
		if ($attributes) {
			$attributes = join(';', $attributes);
		}

		$qty = abs($qty);
		$product = Core::$db->product[(int) $product_id];

		if (!$product || !$product['status']) {
			return false;
		}

		if ((int) Core::config('suspend_no_stock') && ($stock = self::isInStock($product_id, $attributes, $qty)) !== true) {
			return (int) $stock;
		}

		$r = Core::$db->basket_products()->where('basket_id', self::id())->where('product_id', $product_id)->where('attributes', $attributes)->fetch();

		if ($r) {
			Event::run('Basket::add', $product_id, $attributes, $qty);

			return self::update($r['id'], $r['qty'] + $qty);
		} else {
			Core::$db->basket_products(array(
				'basket_id' => self::id(),
				'product_id' => (int) $product_id,
				'attributes' => $attributes,
				'qty' => $qty
			));
		}

		Event::run('Basket::add', $product_id, $attributes, $qty);

		return true;
	}

	private static function isInStock($product_id, $attributes = null, $qty = 1)
	{
		$qty = abs($qty);
		$product = Core::$db->product[(int) $product_id];
		$stock = $product['stock'];

		if ($attributes && $attr_ids = explode(';', $attributes)) {
			$attr_stock = null;

			foreach ($attr_ids as $attr_id) {

				if (!is_numeric($attr_id)) {
					continue;
				}

				$attr = Core::$db->product_attributes[(int) $attr_id];

				if ($attr && $attr['product_id'] == $product_id) {
					if ($attr['stock'] !== null && $attr['stock'] !== '' && ($attr_stock === null || $attr['stock'] < $attr_stock)) {
						$attr_stock = (int) $attr['stock'];
					}
				}
			}

			Event::run('Basket::isInStock', $product, $attr_ids, $qty, $attr_stock);

			if ($attr_stock !== null) {
				$stock = $attr_stock;
			}
		}

		return $stock === null ? true : (($stock - $qty) >= 0 ? true : $stock);
	}

	public static function remove($row_id)
	{
		Core::$db->basket_products()->where('basket_id', self::id())->where('id', $row_id)->delete();
	}

	public static function update($row_id, $qty = null)
	{
		if (is_array($row_id)) {
			foreach ($row_id as $_row_id => $qty) {
				$result = ($r = self::update($_row_id, $qty)) === true ? $result : $r;
			}

			return $result;
		} else if ($qty == 0) {
			self::remove($row_id);
		} else if ($qty !== null) {
			$qty = abs($qty);
			$row = Core::$db->basket_products[(int) $row_id];

			if (!$row || $row['basket_id'] != self::id()) {
				return false;
			}

			if ((int) Core::config('suspend_no_stock') && ($stock = self::isInStock($row->product['id'], $row['attributes'], $qty)) !== true) {
				return (int) $stock;
			}

			$row->update(array(
				'qty' => $qty
			));
		}

		return true;
	}

	public static function products()
	{
		$basket_id = self::id();
		$products = array();

		if (!$basket_id) {
			return null;
		}

		foreach (self::$basket->basket_products() as $product) {
			if (!$product->product['id']) {
				continue;
			}

			$price = $product->product['price'];
			$sku = $product->product['sku'];
			$ean = $product->product['ean13'];
			$stock = $product->product['stock'];
			$weight = (float) $product->product['weight'];

			$attributes = '';

			Event::run('Basket::products.product_attributes', $product, $attributes, $price, $sku, $ean, $stock);

			$attribute_file_id = null;
			
			if ($attr_ids = explode(';', $product['attributes'])) {
				foreach ($attr_ids as $attr_id) {

					if (!is_numeric($attr_id)) {
						continue;
					}

					$attr = Core::$db->product_attributes[(int) $attr_id];

					if ($attr) {
						$attributes .= $attr['name'] . ': ' . $attr['value'] . ', ';

						if (preg_match('/^([\d\-\+\.\,]+)\%$/', trim($attr['price']), $matches)) {
							$price += parseFloat($matches[1]) / 100 * $product->product['price'];
						} else if (preg_match('/^([\d\-\+\.\,]+)$/', trim($attr['price']), $matches)) {
							$price += parseFloat($matches[1]);
						}

						if($attr['weight']) {
							$weight = (float) $attr['weight'];
						}

						if($attr['file_id']) {
							$attribute_file_id = (int) $attr['file_id'];
						}

						if ($attr['sku']) {
							if (strrpos($attr['sku'], '*') !== false) {
								$sku = preg_replace('/\*/', $sku, $attr['sku']);
							} else {
								$sku = $attr['sku'];
							}
						}

						if ($attr['ean13']) {
							if (strrpos($attr['ean13'], '*') !== false) {
								$ean = preg_replace('/\*/', $ean, $attr['ean13']);
							} else {
								$ean = $attr['ean13'];
							}
						}

						if ($attr['stock'] !== null) {
							$stock = $attr['stock'];
						}
					}
				}
			}

			if (count($discounts = $product->product->product_discounts()->order('quantity DESC'))) {
				foreach ($discounts as $discount) {
					if ($product['qty'] >= $discount['quantity']) {
						$price = $price - ((float) $discount['value'] / 100 * $price);
						break;
					}
				}
			}

			if ($product->product['recycling_fee']) {
				$price += (float) $product->product['recycling_fee'];
			}

			if ($product->product['copyright_fee']) {
				$price += (float) $product->product['copyright_fee'];
			}

			$products[] = array(
				'basket_product' => $product,
				'id' => $product['id'],
				'qty' => $product['qty'],
				'product' => $product->product,
				'price' => $price,
				'sku' => $sku,
				'ean13' => $ean,
				'stock' => $stock,
				'attributes' => trim($attributes, ', '),
				'attributes_ids' => $product['attributes'],
				'weight' => $weight,
				'attribute_file_id' => $attribute_file_id
			);
		}

		return $products;
	}

	public static function total()
	{
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		$discount = 0;

		$products = self::products();

		if (!$products || !count($products)) {
			return null;
		}

		foreach ($products as $product) {
			$subtotal += price_unvat($product['price'], $product['product']['vat'])->price * $product['qty'];
			$total += price_vat($product['price'], $product['product']['vat'])->price * $product['qty'];

			if ($product['weight']) {
				$weight += ($product['weight'] * $product['qty']);
			}
		}

		$tbd = $total;

		if ($voucher = self::voucher()) {
			$discount = estPrice($voucher['value'], $total);

			$total = $total - $discount;
		}

		if ($total < 0) {
			$total = 0;
		}

		$obj = new Basket_Total;
		$obj->subtotal = $subtotal;
		$obj->total = round($total, (int) Core::config('order_round_decimals'));
		$obj->_total = $total;
		$obj->_total_before_discount = $tbd;
		$obj->discount = $discount;
		$obj->weight = $weight;

		return $obj;
	}

	public static function count()
	{
		return (int) count(self::products());
	}

	public static function total_count()
	{
		$count = 0;
		foreach(self::products() as $product) {
			$count += (int) $product['qty'];
		}
		return $count;
	}

	public static function amount()
	{
		$amount = 0;
		$products = Core::$db->basket_products()->where('basket_id', self::id());

		foreach($products as $product) {
			$amount += $product['qty'];
		}

		return $amount;
	}

	public static function useVoucher($voucher)
	{
		if (Core::$is_premium && Core::config('enable_vouchers')) {
			self::id();

			self::$basket->update(array('voucher_id' => $voucher['id']));
			self::$basket['voucher_id'] = $voucher['id'];
		}
	}

	public static function removeVoucher()
	{
		self::id();

		if (self::$basket['voucher_id']) {
			self::$basket->update(array('voucher_id' => null));
			self::$basket['voucher_id'] = null;
		}
	}

	public static function voucher()
	{
		self::id();

		if (self::$basket['voucher_id'] && ($voucher = Core::$db->voucher[self::$basket['voucher_id']]) && $voucher->model->isValid()) {
			return $voucher;
		}

		return null;
	}

}

class Basket_Total
{

	public $subtotal, $total, $_total, $weight, $discount, $_total_before_disocunt;

	function __toString()
	{
		return (string) $this->subtotal;
	}

}