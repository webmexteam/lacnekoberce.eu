<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Registry
{

	public static $tmp = array();

	public static function set($name, $value, $compress = false, $shutdown_save = false)
	{
		if ($shutdown_save) {
			self::$tmp[$name] = array(
				'value' => $value,
				'compress' => $compress
			);

			return true;
		}

		if ($registry = Core::$db->registry()->where('name', $name)->fetch()) {
			return (bool) Core::$db->registry()->where('name', $name)->update(array('value' => self::encodeValue($value, $compress)));
		}

		return (bool) Core::$db->registry(array('name' => $name, 'value' => self::encodeValue($value, $compress)));
	}

	public static function get($name)
	{
		if (Core::$db_inst->tableFields('registry') && ($registry = Core::$db->registry()->where('name', $name)->fetch())) {
			return self::decodeValue($registry['value']);
		}

		return null;
	}

	protected static function encodeValue($value, $compress = false)
	{
		if (is_array($value) || is_object($value)) {
			$value = 'SERIALIZED:' . serialize($value);
		}

		if ($compress && function_exists('gzdeflate')) {
			$value = 'COMPRESSED:' . base64_encode(gzdeflate($value, 9));
		}

		return $value;
	}

	protected static function decodeValue($value)
	{
		if (substr($value, 0, 11) == 'COMPRESSED:') {
			$value = @gzinflate(base64_decode(substr($value, 11)));
		}

		if (substr($value, 0, 11) == 'SERIALIZED:') {
			return unserialize(substr($value, 11));
		}

		return $value;
	}

	public static function shutodown_handler()
	{
		foreach (self::$tmp as $name => $opt) {
			if ($registry = Core::$db->registry()->where('name', $name)->fetch()) {
				Core::$db->registry()->where('name', $name)->update(array('value' => self::encodeValue($opt['value'], $opt['compress'])));
			} else {
				Core::$db->registry(array('name' => $name, 'value' => self::encodeValue($opt['value'], $opt['compress'])));
			}
		}
	}

}

register_shutdown_function(array(Registry, 'shutodown_handler'));