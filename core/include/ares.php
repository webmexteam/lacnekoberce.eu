<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Ares
{

	public static $url = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=';

	public static function getInfo($ico)
	{
		$ico = intval($ico);

		$file = @file_get_contents(self::$url . $ico);

		if ($file)
			$xml = @simplexml_load_string($file);

		$result = array();

		if ($xml) {
			$ns = $xml->getDocNamespaces();
			$data = $xml->children($ns['are']);
			$el = $data->children($ns['D'])->VBAS;

			$co = strval($el->AA->CO);

			if (strval($el->ICO) == $ico) {
				$result['ico'] = strval($el->ICO);
				$result['dic'] = strval($el->DIC) ? strval($el->DIC) : null;
				$result['name'] = strval($el->OF);
				$result['street'] = strval($el->AA->NU) . ' ' . strval($el->AA->CD) . ($co ? '/' . $co : '');
				$result['city'] = strval($el->AA->N);
				$result['country'] = strval($el->AA->NS);
				$result['zip'] = strval($el->AA->PSC);
				$state = strval($el->PSU);
				$result['status'] = true;
			} else
				$result['status'] = -1; // no record found
		} else
			$result['status'] = -2; // connection failed

		return $result;
	}

}