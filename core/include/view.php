<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
require APPROOT . 'vendor/phpquery/phpQuery.php';
require APPROOT . 'include/domtemplate.php';

class View
{

	protected $path;
	protected $data = array();
	protected $template;
	protected $tmp_output = '';
	protected $current_slot = null;
	protected $current_slot_level = null;
	public $override = true;
	public $slots = array();
	public static $meta = array(
		'title' => '',
		'keywords' => '',
		'description' => ''
	);
	public static $global_data = array(
		'v' => Core::version
	);
	public static $css_files = array(
		'front' => array(),
		'admin' => array()
	);
	public static $js_files = array(
		'front' => array(),
		'admin' => array()
	);
	public static $script_constants = array(
		'front' => array(),
		'admin' => array()
	);
	public static $script_language = array(
		'front' => array(),
		'admin' => array()
	);

	public static function addCSS($app, $css_path)
	{
		self::$css_files[$app][] = $css_path;
	}

	public static function addJS($app, $js_path)
	{
		self::$js_files[$app][] = $js_path;
	}

	public static function addScriptConst($app, $name, $value = null)
	{
		if (is_array($name)) {
			foreach ($name as $n => $v) {
				self::addScriptConst($app, $n, $v);
			}
		} else {
			self::$script_constants[$app][$name] = $value;
		}
	}

	public static function addScriptLang($app, $key, $localization = null)
	{
		if (is_array($key)) {
			foreach ($key as $n => $v) {
				self::addScriptLang($app, $n, $v);
			}
		} else {
			self::$script_language[$app][$key] = $localization;
		}
	}

	public function __construct($path, $data = array(), $template = null)
	{
		$this->path = $path;
		$this->data = $data;
		$this->template = $template;
	}

	public function __toString()
	{
		return (string) $this->render();
	}

	public function __set($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function __get($key)
	{
		return $this->data[$key];
	}

	public function getData()
	{
		return $this->data;
	}

	public function render()
	{
		try {
			if (!$this->path) {
				throw new Exception('View path is not defined');
			}

			if (preg_match('/^system\//i', $this->path)) {
				$path = 'template/' . $this->path;
			} else if ($this->template) {
				$path = 'template/' . $this->template . '/' . $this->path;
			} else if (Core::$is_admin) {
				$path = 'admin/template/' . Core::$def['admin_template'] . '/' . $this->path;
			} else {
				$path = 'template/' . Core::config('template') . '/' . $this->path;
			}

			extract(array('meta' => self::$meta));
			extract(self::$global_data);
			extract($this->data);

			$view = $this;

			$status = false;
			$this->tmp_output = '';

			foreach ($this->findFiles($path) as $filepath) {
				$status = true;

				ob_start();
				$ob_level = ob_get_level();

				require $filepath;

				while (ob_get_level() > $ob_level) {
					ob_end_clean();
				}

				$out = ob_get_clean();

				if (!$this->override) {
					$this->tmp_output .= $out;
				} else {
					$this->tmp_output = $out;
				}
			}

			if (!$status) {
				throw new Exception('View "' . $this->path . '" not found');
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
		}

		return $this->tmp_output;
	}

	public function pq()
	{
		$this->tmp_output = phpQuery::newDocumentHTML($this->tmp_output);

		return $this->tmp_output;
	}

	public function find($selector)
	{
		return $this->pq()->find($selector);
	}

	public function slot_start($slot_name = null)
	{
		if (!$slot_name) {
			$slot_name = uniqid('slot');
		}

		$this->slots[$slot_name] = '';
		$this->current_slot = $slot_name;

		ob_start();
		$this->current_slot_level = ob_get_level();

		return $this->slots[$slot_name];
	}

	public function slot_stop()
	{
		if ($this->current_slot && isSet($this->slots[$this->current_slot])) {
			$this->current_slot = null;

			while (ob_get_level() > $this->current_slot_level) {
				ob_end_clean();
			}

			return ob_get_clean();
		}

		return null;
	}

	protected function findFiles($path)
	{
		$files = array();

		$pathinfo = pathinfo($path);
		$override_path = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '.override.' . $pathinfo['extension'];

		foreach (Core::$autoload_paths as $dir) {

			if (file_exists(DOCROOT . $dir . $path)) {
				$files[] = DOCROOT . $dir . $path;
			}

			if (file_exists(DOCROOT . $dir . $override_path)) {
				$files = array(); // overrides all

				$files[] = DOCROOT . $dir . $override_path;
			}
		}

		return $files;
	}

}