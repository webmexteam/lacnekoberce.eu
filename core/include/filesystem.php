<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class FS
{

	private static $user_uid;
	public static $wrapper;

	public static function mkdir($path, $chmod = 0666)
	{
		if ($chmod === true) {
			$chmod = 0777;
		}
    
    $dirname = dirname($path);
    if (!is_dir($dirname)) {
        self::mkdir($dirname, $chmod);
    }

		if (!is_dir($path)) {
			$old = @umask(0);
			mkdir(self::wrapPath($path), $chmod, true);
			@umask($old);
		}

		self::chmod($path, $chmod);
		self::chown($path, self::getUID());
	}

	public static function writable($path)
	{
		self::chmod($path, 0777);
		self::chown($path, self::getUID());
	}

	public static function chmod($path_orig, $chmod)
	{
		$path = self::wrapPath($path_orig);

		if (preg_match('/^ftp\:\/\//', $path)) {
			$info = parse_url($path);
			$fileinfo = pathinfo($info['path']);

			$ftp = self::connectFtp($info['host'], $info['user'], $info['pass'], $info['port'], $fileinfo['dirname']);

			if (!$ftp) {
				return @chmod($path, $chmod);
			}

			if (!($res = @ftp_chmod($ftp, $chmod, $fileinfo['basename']))) {
				$res = @ftp_chmod($ftp, $chmod, $info['path']);
			}

			if (!$res) {
				@chmod($path_orig, $chmod);
			}

			return $res;
		}

		return @chmod($path_orig, $chmod);
	}

	public static function chown($path, $uid = null)
	{
		/* if(self::$wrapper || preg_match('/^ftp\:\/\//', $path)){
		  return true;
		  } */

		if ($uid === null) {
			$uid = self::getUID();
		}

		return @chown($path, $uid);
	}

	public static function rmdir($path)
	{
		if (!is_dir($path)) {
			return true;
		}

		$files = scandir($path);

		foreach ($files as $file) {
			if ($file == '.' || $file == '..')
				continue;

			if (is_dir($path . '/' . $file)) {
				self::rmdir($path . '/' . $file);
			} else {
				@unlink(self::wrapPath($path) . '/' . $file);
			}
		}

		return rmdir(self::wrapPath($path));
	}

	public static function remove($path)
	{
		if (is_dir($path)) {
			return self::rmdir($path);
		} else if (file_exists($path)) {
			return @unlink(self::wrapPath($path));
		}

		return null;
	}

	public static function rename($old_path, $new_path)
	{
		return @rename(self::wrapPath($old_path), self::wrapPath($new_path));
	}

	public static function copy($src, $dest)
	{
		if (is_dir($src)) {
			return self::copydir($src, $dest);
		}

		if (!file_exists($src)) {
			return null;
		}

		if (($pos = strrpos($dest, '/')) !== false) {
			$dir = substr($dest, 0, $pos);

			self::mkdir($dir, true);
		}

		return copy($src, self::wrapPath($dest));
	}

	public static function copydir($src, $dest)
	{
		$files = scandir($src);

		foreach ($files as $file) {
			if ($file == '.' || $file == '..') {
				continue;
			}

			if (is_dir($src . '/' . $file)) {
				self::copydir($src . '/' . $file, $dest . '/' . $file);
			} else {
				self::copy($src . '/' . $file, $dest . '/' . $file);
			}
		}
	}

	public static function touch($file)
	{
		touch($file, time());
	}

	public static function cleanCache()
	{
		if ($f = glob(DOCROOT . 'etc/tmp/*')) {
			foreach ($f as $file) {
				self::writable($file);
				self::remove($file);
			}
		}

		if ($f = glob(DOCROOT . 'etc/feed/*')) {
			foreach ($f as $file) {
				self::writable($file);
				self::remove($file);
			}
		}

		Core::$db_inst->table_fields = array();

		Core::$db->config()->where('name', 'last_cache_clean')->update(array('value' => time()));
	}

	private static function getUID()
	{
		if (self::$user_uid) {
			return self::$user_uid;
		}

		if (is_null(self::$user_uid) || !self::$user_uid) {
			$stat = stat(DOCROOT . 'files');

			self::$user_uid = $stat['uid'];

			return $stat['uid'];
		}
	}

	private static function connectFtp($server, $login, $password, $port = 21, $dir = null)
	{
		$conn = @ftp_connect($server, $port ? $port : 21);

		if ($conn && @ftp_login($conn, $login, $password)) {
			if ($dir) {
				@ftp_chdir($conn, $dir);
			}

			return $conn;
		}

		return false;
	}

	private static function wrapPath($path)
	{
		if (self::$wrapper) {
			$path = substr($path, strlen(DOCROOT));
			return self::$wrapper . ltrim($path, '/');
		}

		return $path;
	}

}