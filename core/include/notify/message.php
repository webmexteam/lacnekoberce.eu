<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Notify_Message extends Extendable
{

	public $uid, $msg, $url, $datetime, $type;

	public function __construct($msg, $datetime = null, $type = 'info', $url = null)
	{
		$this->uid = uniqid('msg');
		$this->msg = $msg;
		$this->url = $url;
		$this->datetime = $datetime ? $datetime : time();
		$this->type = $type;
	}

	public function __toString()
	{
		return (string) $this->msg;
	}

	public function remove()
	{
		Notify::remove($this->uid);
	}

}