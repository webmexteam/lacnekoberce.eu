<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Export
{

	public $writer;

	public function __construct($driver)
	{
		if ($driver == 'xml') {
			$this->writer = new Export_Xml();
		} else if ($driver == 'csv') {
			$this->writer = new Export_Csv();
		} else if ($driver == 'xlsx') {
			$this->writer = new Export_Xlsx();
		} else if ($driver == 'ods') {
			$this->writer = new Export_Ods();
		}
	}

	public function addHeader($columns = null)
	{
		if (method_exists($this->writer, 'addHeader')) {
			$this->writer->addHeader($columns);
		}
	}

	public function addProduct($product, $columns = null)
	{
		$this->writer->addProduct($product, $columns);
	}

	public function save()
	{
		return $this->writer->build();
	}

}