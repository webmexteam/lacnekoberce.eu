<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb
{

	protected $db;
	public $driver;

	public function __construct($driver, array $connection)
	{
		$this->driver = strtolower($driver);

		$driver_class = 'Odb_Driver_' . ucfirst($this->driver);

		$this->db = new $driver_class($connection['dsn'], $connection['user'], $connection['password']);

		if (!$this->db) {
			throw new Exception('Database connection error');
		}
	}

	public function __get($table)
	{
		return new Odb_Result_Single($table, $this);
	}

	public function __call($table, array $where)
	{
		$result = new Odb_Result($table, $this);

		if ($where) {
			$data = $where[0];

			if ($data instanceof Traversable) {
				$data = iterator_to_array($data);
			}

			if (is_array($data)) {
				return $result->insert($data, (isset($where[1]) && $where[1] == 1));
			}

			call_user_func_array(array($result, 'where'), $where);
		}

		return $result;
	}

	public function query($query)
	{
		$st = microtime(true);

		$resource = $this->db->query($query);

		Db::logQuery($query, microtime(true) - $st);

		return $resource;
	}

	public function num_rows($resource)
	{
		return $this->db->num_rows($resource);
	}

	public function data_seek($resource, $rownum)
	{
		return $this->db->data_seek($resource, $rownum);
	}

	public function fetch_assoc($resource)
	{
		return $this->db->fetch_assoc($resource);
	}

	public function fetch_array($resource)
	{
		return $this->db->fetch_array($resource);
	}

	public function fetch_object($resource)
	{
		return $this->db->fetch_object($resource);
	}

	public function escape($value)
	{
		return $this->db->escape($value);
	}

	public function last_insert_id()
	{
		return $this->db->last_insert_id();
	}

	public function affected_rows()
	{
		return $this->db->affected_rows();
	}

	public function dumpLog()
	{
		var_dump($this->log);
	}

}