<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Payment
{

	public $name;
	public $payment;
	public $payment_info;
	public $config = array();
	public static $drivers = array();

	public function __construct($payment = null)
	{
		$this->payment = $payment;
		$this->payment_info = $payment['info'];
		$this->config = array_merge($this->config, (array) json_decode($payment['config'], true));
	}

	public function getConfigForm()
	{
		return null;
	}

	public function process($order)
	{
		return null;
	}

	public function getPaymentLink($order)
	{
		return null;
	}

	public function getPaymentInfo($order)
	{
		return $this->payment_info;
	}

}