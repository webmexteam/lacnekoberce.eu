<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Customer extends Extendable
{

	public static $logged_in = false;
	public static $data;

	public static function setup()
	{
		if (empty($_SESSION['webmex_customer']) && !empty($_COOKIE['customer_remember'])) {
			$parts = explode(';', $_COOKIE['customer_remember']);

			if (count($parts) == 2 && is_numeric($parts[0])) {
				$customer = Core::$db->customer[(int) $parts[0]];

				if ($customer && (int) $customer['active'] && sha1(Core::config('instid') . $customer['id'] . $customer['password']) == $parts[1]) {
					$_SESSION['webmex_customer'] = array(
						't' => time(),
						'customer' => $customer->as_array()
					);

					$customer->update(array(
						'last_login' => time()
					));
				}
			}
		}

		if (!empty($_SESSION['webmex_customer']) && is_array($_SESSION['webmex_customer'])) {
			if ($_SESSION['webmex_customer']['t'] >= strtotime('-30 minutes')) {
				self::$data = $_SESSION['webmex_customer'];

				$_SESSION['webmex_customer']['t'] = time();

				self::$logged_in = true;
			} else {
				$_SESSION['flash_login_error'] = 'msg_timeout_logout';
				unset($_SESSION['webmex_customer']);
			}
		} else {
			unset($_SESSION['webmex_customer']);
		}

		Extendable::callMixins();
	}

	public static function login($email, $password, $remember = false)
	{

		$password = trim($password);

		$customer = Core::$db->customer('email', $email)->where('active', 1)->fetch();

		if (!$customer) {
			return 'user_not_found';
		}

		if ($customer['password'] == sha1(sha1($password))) {
			$_SESSION['webmex_customer'] = array(
				't' => time(),
				'customer' => $customer->as_array()
			);

			$customer->update(array(
				'last_login' => time()
			));

			if ($remember) {
				$v = $customer['id'] . ';' . sha1(Core::config('instid') . $customer['id'] . $customer['password']);

				setcookie('customer_remember', $v, strtotime('+3 months'), Core::$base);
			}

			return true;
		} else {
			return 'invalid_password';
		}
	}

	public static function logout()
	{
		unset($_SESSION['webmex_customer']);
		setcookie('customer_remember', '', time() - 3600, Core::$base);

		Extendable::callMixins();
	}

	public static function get($name = null)
	{
		$value = $name ? self::$data['customer'][$name] : self::$data['customer'];

		if (!$value) {
			$value = Extendable::callMixins();

			if ($value == Extendable::METHOD_NOT_FOUND) {
				$value = null;
			}
		}

		return $value;
	}

}