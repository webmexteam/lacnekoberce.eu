<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Block
{

	public $block;
	public $config = array();
	public static $drivers = array();

	public function __construct($block = null)
	{
		$this->block = $block;
		$this->config = array_merge($this->config, (array) json_decode($block['config'], true));
	}

	public function getTitle()
	{
		return $this->block['title'];
	}

	public function getContent()
	{
		return $this->block['text'];
	}

	public function getConfigForm()
	{
		return null;
	}

	public function saveConfig()
	{
		if ($this->block) {
			$this->block->update(array('config' => json_encode($this->config)));
		}
	}

}