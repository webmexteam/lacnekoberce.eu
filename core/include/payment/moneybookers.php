<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Payment::$drivers[] = 'moneybookers';

class Payment_Moneybookers extends Payment
{

	public $name = 'MoneyBookers';
	public $config = array();

	public function __construct($payment = null)
	{
		parent::__construct($payment);
	}

	public function getConfigForm()
	{
		return null;
	}

	public function process($order)
	{
		Moneybookers::setup();

		if (Moneybookers::pay($order) !== true) {
			redirect('moneybookers/error');
		}
	}

	public function getPaymentLink($order)
	{
		return url('moneybookers/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
	}

	public function getPaymentInfo($order)
	{
		if ((int) Core::config('confirm_orders') && !(int) $order['confirmed']) {
			return (string) __('wait_for_order_confirmation');
		}

		return $this->payment_info ? $this->payment_info : (string) __('order_payment_link', $this->getPaymentLink($order));
	}

}