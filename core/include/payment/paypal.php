<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Payment::$drivers[] = 'paypal';

class Payment_Paypal extends Payment
{

	public $name = 'Paypal.com';
	public $config = array();

	public function __construct($payment = null)
	{
		parent::__construct($payment);
	}

	public function getConfigForm()
	{
		return null;
	}

	public function process($order)
	{
		Paypal::setup();

		if (Paypal::pay($order) !== true) {
			redirect('paypal/error');
		}
	}

	public function getPaymentLink($order)
	{
		return url('paypal/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
	}

	public function getPaymentInfo($order)
	{
		if ((int) Core::config('confirm_orders') && !(int) $order['confirmed']) {
			return (string) __('wait_for_order_confirmation');
		}

		return $this->payment_info ? $this->payment_info : (string) __('order_payment_link', $this->getPaymentLink($order));
	}

}