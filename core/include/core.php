<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Core

{

	const version = '2.0.4';

	public static $servicepacks = array();
	public static $uri;
	public static $orig_uri;
	public static $request_uri;
	public static $orig_url;
	public static $segments = array();
	public static $base = null;
	public static $domain = null;
	public static $url = null;
	public static $is_admin = false;
	public static $is_premium = false;
	public static $unlimited_license = null;
	public static $is_trial = false;
	public static $use_htaccess = false;
	public static $current_page;
	public static $current_product;
	public static $active_pages = array();
	public static $active_tab;
	public static $silent_mode = false;
	public static $db;
	public static $db_inst;
	public static $db_conn;
	public static $api;
	public static $language = 'cs';
	public static $lang = array();
	public static $locale = array();
	public static $config;
	public static $def = array();
	public static $benchmark = array();
	public static $remove_flash = true;
	public static $magic_quotes_gpc = false;
	public static $fix_path = false;
	public static $autoload_not_found = array();
	public static $autoload_paths = array('core/', 'etc/core/');
	public static $buffer_level = 0;
	public static $log = array();
	public static $profiler = false;
	public static $debug = false;
	public static $debug_report = true;
	public static $modules = array();
	public static $controller = 'default';
	public static $default_uri = 'default/index';
	public static $route = array(
		// match product (P) or page (A) - find last occurence of id
		'([^/]*)-(p|a)([0-9]{1,})((\z|/)((?!-(p|a)[0-9]{1,}(\z|/)).)*)$' => 'default/$2/$3/$1/$4$5',
		'sitemap.xml' => 'robots/sitemap',
		'robots.txt' => 'robots/index',
		'files/(.*)' => 'files/index/$1',
	);
	public static $error_codes = array(
		E_ERROR => 'Fatal Error',
		E_PARSE => 'Parse Error',
	);
	public static $error = false;

	public static function setup($silent_mode = false)
	{
		self::$silent_mode = $silent_mode;

		self::$benchmark['start_time'] = microtime(true);

		ob_start(array(__CLASS__, 'output_buffer'));

		self::$buffer_level = ob_get_level();

		if (!self::$silent_mode) {
			set_error_handler(array('Core', 'error_handler'), E_ALL & ~E_STRICT & ~E_NOTICE);
			set_exception_handler(array('Core', 'error_handler'));
		}

		register_shutdown_function(array('Core', 'shutdown_handler'));

		spl_autoload_register(array('Core', 'autoLoad'), true);

		if (@get_magic_quotes_runtime()) {
			@set_magic_quotes_runtime(0);
		}

		// magic_quotes_gpc is enabled
		if (@get_magic_quotes_gpc()) {
			self::$magic_quotes_gpc = true;
		}

		if (isSet($_SERVER['FCGI_ROLE']) && (bool) ini_get('cgi.force_redirect')) {
			self::$fix_path = true;
		}

		if (isSet($_POST['session_id'])) {
			session_id($_POST['session_id']);
		}

		if (!self::$silent_mode) {
			session_start();
		}

		if (isset($_SESSION['webmex_flash_remove']) && is_array($_SESSION['webmex_flash_remove'])) {
			foreach ($_SESSION['webmex_flash_remove'] as $name) {
				unset($_SESSION[$name]);
			}
		}

		$_SESSION['webmex_flash_remove'] = array();

		self::load();

		self::loadI18N();

		if (!self::$silent_mode) {
			header('Content-Type: text/html; charset=UTF-8');
			header('X-UA-Compatible: IE=edge,chrome=1');

			self::dispatch();
		}
	}

	private static function setEnv()
	{
		self::$base = substr($_SERVER['SCRIPT_NAME'], 0, -9);
		$port = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 443 : 80;

		if (self::$def['strip_request_uri']) {
			self::$base = preg_replace('#' . rtrim(self::$def['strip_request_uri'], '/') . '#i', '', self::$base);
		}

		self::$domain = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . ($_SERVER['SERVER_PORT'] != $port ? ':' . $_SERVER['SERVER_PORT'] : '');
		self::$url = self::$domain . self::$base;

		if (self::$db) {
			if (self::config('vat_payer')) {
				if (self::config('vat_mode') == 'exclude') {
					define('PRICE_NAME', 'price_excl_vat');
				} else {
					define('PRICE_NAME', 'price_incl_vat');
				}
			} else {
				define('PRICE_NAME', 'price');
			}

			$vat_delivery = 0;
			$vat_default = 0;

			$vat_rates = array();
			foreach (self::$db->vat() as $vat_rate) {
				$vat_rates[$vat_rate['id']] = $vat_rate;
			}

			if ((int) self::config('vat_payer')) {
				if ($vat = $vat_rates[(int) self::config('vat_delivery')]) {
					$vat_delivery = (float) $vat['rate'];
				}
			}

			if ((int) self::config('vat_rate_default')) {
				if ($vat = $vat_rates[(int) self::config('vat_rate_default')]) {
					$vat_default = (float) $vat['rate'];
				}
			}

			define('VAT_DELIVERY', $vat_delivery);
			define('VAT_DEFAULT', $vat_default);

			define('PAGE_INDEX', (int) self::config('page_index'));
			define('PAGE_BASKET', (int) self::config('page_basket'));
			define('PAGE_ORDER', (int) self::config('page_order_step1'));
			define('PAGE_ORDER2', (int) self::config('page_order_step2'));
			define('PAGE_ORDER_FINISH', (int) self::config('page_order_finish'));
			define('PAGE_SEARCH', (int) self::config('page_search'));
			define('PAGE_404', (int) self::config('page_404'));
			define('PAGE_SITEMAP', (int) self::config('page_sitemap'));

			self::$is_premium = self::config('activated') == 1;
			self::$is_trial = false;

			self::$language = self::config('language');

			View::$meta = array(
				'title' => '',
				'keywords' => self::config('keywords'),
				'description' => self::config('description')
			);

			self::trial();
			self::unlimited();

			$modules = array();

			if (self::$db) {
				Notify::setup();

				foreach (self::$db->module() as $module) {
					$modules[$module['name']] = $module;
				}
			}

			foreach (self::$modules as $module_name => $module) {
				if (self::$db && ($mod = $modules[$module_name])) {
					$module->installed = true;
					$module->status = (int) $mod['status'];
					$module->registry = $mod;

					if ((int) $module->status) {
						self::$autoload_paths[] = 'etc/modules/' . $module_name . '/';

						if (method_exists($module, 'setup')) {
							$module->setup();
						}
					}
				}
			}

			// Nastaveni vodoznaku
			$watermark_font_colors = array();
			foreach (explode(',', Core::config('watermark_font_color')) as $color) {
				$watermark_font_colors[] = (int) $color;
			}
			self::$def['image_watermark'] = array(
				'apply_on_original' => (bool) Core::config('watermark_apply_on_original'),
				'min_size' => (int) Core::config('watermark_min_size'),
				'type' => Core::config('watermark_type'), // image / text
				'filepath' => Core::config('watermark_filepath'),
				'text' => Core::config('watermark_text'),
				'alignment' => Core::config('watermark_alignment'), // tl, tr, bl, br, c
				'font_color' => $watermark_font_colors,
				'font_size' => (int) Core::config('watermark_font_size'),
				'opacity' => (int) Core::config('watermark_opacity'),
				'png24' => (bool) Core::config('watermark_png24'),
				'text_shadow' => (bool) Core::config('watermark_text_shadow'),
			);
		}
	}

	private static function setScriptConstants()
	{
		if (!self::$db) {
			return false;
		}

		$sizes = array();
		foreach (Core::$def['image_sizes'] as $size => $name) {
			$sizes[$size] = (string) $name;
		}

		$vat_rates = array();
		foreach (Core::$db->vat() as $rate) {
			$vat_rates[$rate['id']] = (float) $rate['rate'];
		}

		$global = array(
			'docroot' => DOCROOT,
			'appdir' => APPDIR,
			'base' => url(''),
			'tpl_front' => View::$global_data['base'] . View::$global_data['tplbase_front'],
			'url' => View::$global_data['base'],
			'session_id' => session_id(),
			'image_sizes' => $sizes,
			'vat_payer' => (int) Core::config('vat_payer'),
			'order_round_decimals' => (int) Core::config('order_round_decimals'),
			'vat_delivery' => $vat_rates[(float) Core::config('vat_delivery')],
			'vat_mode' => Core::config('vat_mode'),
			'is_premium' => (Core::$is_premium ? 1 : 0),
			'price_format' => (int) Core::config('price_format'),
			'price_vat_round' => (int) Core::config('price_vat_round'),
			'currency' => Core::config('currency'),
			'vat_rates' => $vat_rates,
		);

		View::addScriptConst('admin', $global);
		View::addScriptConst('front', $global);

		View::addScriptConst('admin', array(
			'fm_root' => Core::$def['filemenager_root'],
			'quick_upload_dir' => Core::$def['quick_upload_dir'],
			'upload_driver' => Core::config('upload_driver'),
			'filemanager_url' => url('admin/filemanager', array('reset' => 1, 'ck' => 1), true),
		));

		return true;
	}

	private static function filterInput()
	{
		foreach ($_GET as $k => $v) {
			if (!is_array($v)) {
				$_GET[$k] = self::xss_clean($v);
			}
		}

		foreach ($_COOKIE as $k => $v) {
			$_COOKIE[$k] = self::xss_clean($v);
		}

		foreach ($_POST as $k => $v) {
			if (!is_array($v)) {
				$_POST[$k] = self::xss_clean($v);
			} else {
				self::filterInputArray($_POST[$k]);
			}
		}
	}

	private static function filterInputArray(& $arr)
	{
		foreach ($arr as $k => $v) {
			if (!is_array($v)) {
				$arr[$k] = self::xss_clean($v);
			} else {
				self::filterInputArray($arr[$k]);
			}
		}
	}

	private static function setViewData()
	{
		if ($style_file = Core::findFile('template/' . self::config('template') . '/css/style/' . self::config('template_style') . '.less.css')) {
			$tplbase = substr(dirname($style_file), strlen(DOCROOT), -9);
		} else {
			$tplbase = 'core/template/' . self::config('template') . '/';
		}

		View::$global_data['tplbase_front'] = $tplbase;
		View::$global_data['tplbase'] = $tplbase;

		if (self::$is_admin) {
			View::$global_data['tplbase'] = 'core/admin/template/' . Core::$def['admin_template'] . '/';
		}

		View::$global_data['base'] = self::$url;
		View::$global_data['site'] = substr(self::$url, 0, strlen(self::$base) * -1);

		if (self::$db) {
			View::$global_data['_options'] = (array) Registry::get('template_options');

			if (self::config('ajaxbasket')) {
				View::addJS('front', 'core/js/ajaxbasket.js');
			}
		}
	}

	private static function sort_modules_priority($a, $b)
	{
		$ap = $a->getLoadPriority();
		$bp = $b->getLoadPriority();

		return $ap > $bp ? 1 : -1;
	}

	private static function sort_modules_dependency($modules, $before = array(), $dependecies)
	{
		foreach ($modules as $module_name => $module) {
			$dependency = property_exists($module, 'dependencies') ? $a->dependencies : null;

			if ($dependency) {

				foreach ($dependency as $mod => $version) {
					sort_modules_dependency();
				}
			} else {
				$before[] = $module_name;
			}
		}
	}

	private static function sort_modules_dependencies($data, $mod, & $out)
	{
		if (!empty($data[$mod])) {
			foreach ($data[$mod] as $m) {
				self::sort_modules_dependencies($data, $m, $out);
			}
		}

		if (!in_array($mod, $out)) {
			$out[] = $mod;
		}
	}

	private static function load()
	{
		if (!function_exists('json_encode') || !function_exists('json_decode')) {
			require_once(APPROOT . 'vendor/json/json.php');
		}

		require_once(APPROOT . 'include/filesystem.php');
		require_once(APPROOT . 'include/helpers.php');
		require_once(APPROOT . 'include/def.php');

		if (file_exists(DOCROOT . 'etc/def.php')) {
			require_once(DOCROOT . 'etc/def.php');
		}

		require_once(APPROOT . 'vendor/notorm/NotORM.php');

		$modules = array();
		$dependencies = array();

		foreach (Core::glob('etc/modules') as $module) {
			if (file_exists($module . '/module.php')) {
				$module_name = substr($module, strrpos($module, '/') + 1);
				$module_class_name = 'Module_' . ucfirst($module_name);

				require_once($module . '/module.php');

				if (class_exists($module_class_name, false)) {
					$modules[$module_name] = new $module_class_name;
					$dependencies[$module_name] = property_exists($modules[$module_name], 'dependencies') ? array_keys($modules[$module_name]->dependencies) : array();

					if(method_exists($module_class_name, 'load')) {
						$module_class_name::load();
					}
				}
			}
		}

		uasort($modules, array('self', 'sort_modules_priority'));

		$sorted_dependencies = array();

		foreach ($modules as $mod => $dep) {
			self::sort_modules_dependencies($dependencies, $mod, $sorted_dependencies);
		}

		foreach ($sorted_dependencies as $module_name) {
			if (isset($modules[$module_name])) {
				$module = $modules[$module_name];

				$module->module_name = $module_name;

				self::$modules[$module_name] = $module;
			}
		}

		date_default_timezone_set(self::$def['timezone']);

		self::$db_inst = new Db();

		if (self::$db_conn = self::$db_inst->connect()) {
			if (self::$db_inst->tableFields('config')) {

				if (self::$def['db_library'] == 'odb') {
					$db_type = 'mysql';

					if (preg_match('/sqlite\:/', Core::$def['db']['dsn'])) {
						$db_type = 'sqlite';
					}

					self::$db = new Odb($db_type, Core::$def['db']);
				} else {
					self::$db = new NotORM(self::$db_conn);
					self::$db->row_class = 'Db_Row';
				}

				if (Core::$def['db']['table_prefix']) {
					self::$db->table_prefix = Core::$def['db']['table_prefix'];
				}

				if (self::config() === false) {
					self::$db = null;
				} else {
					self::$api = new WebmexAPI();
				}
			}
		}

		if (!empty(self::$def['fs_wrapper'])) {
			FS::$wrapper = self::$def['fs_wrapper'];
		}

		self::setEnv();

		foreach (Core::glob('include/block') as $file) {
			if (substr($file, -4) == '.php') {
				$block_name = substr($file, strrpos($file, '/') + 1, -4);

				self::autoload('Block_' . ucfirst($block_name));
			}
		}

		foreach (Core::glob('include/delivery') as $file) {
			if (substr($file, -4) == '.php') {
				$delivery_name = substr($file, strrpos($file, '/') + 1, -4);

				self::autoload('Delivery_' . ucfirst($delivery_name));
			}
		}

		foreach (Core::glob('include/payment') as $file) {
			if (substr($file, -4) == '.php') {
				$payment_name = substr($file, strrpos($file, '/') + 1, -4);

				self::autoload('Payment_' . ucfirst($payment_name));
			}
		}
	}

	public static function loadI18N($load_custom_tranlsation = true)
	{
		foreach (self::$autoload_paths as $path) {
			$file = DOCROOT . $path . 'i18n/' . self::$language . '/lang.php';
			$locale = DOCROOT . $path . 'i18n/' . self::$language . '/locale.php';

			if (file_exists($file)) {
				include($file);
			}

			if (file_exists($locale)) {
				require_once($locale);
			}
		}

		if ($load_custom_tranlsation && self::$db && ($custom = Registry::get('i18n_' . self::$language))) {

			foreach ($custom as $k => $v) {
				if (preg_match('/\./', $k)) {
					$parts = explode('.', $k);
					$custom[$parts[0]][$parts[1]] = $v;
					unset($custom[$k]);
				}
			}

			self::$lang = Arr::overwrite(self::$lang, $custom);
		}

		View::$script_language['admin'] = (array) self::$lang['_script_admin'];
		View::$script_language['front'] = (array) self::$lang['_script_front'];
	}

	public static function config($name = null, $value = false)
	{
		if (!self::$config) {
			if (!self::$db) {
				return null;
			}

			try {
				$config = self::$db->config();
				$config->fetch();
			} catch (PDOException $e) {
				return false;
			}

			self::$config = array();

			foreach ($config as $cfg) {
				self::$config[$cfg['name']] = $cfg['value'];
			}
		}

		if ($name === null) {
			return self::$config;
		}

		if ($value !== false) {
			// set
			if (!isset(self::$config[$name])) {
				Core::$db->config(array(
					'name' => $name,
					'value' => $value
				));
			}

			return true;
		}

		// get
		return self::$config[$name];
	}

	public static function schema()
	{
		self::$db_conn->query(file_get_contents(APPROOT . 'schema.sql'));
	}

	public static function dispatch()
	{
		if (self::$fix_path || isSet($_GET['uri'])) {
			self::$uri = trim($_GET['uri'], ' /');
		} else {
			self::$uri = trim($_SERVER['PATH_INFO'], ' /');
		}

		if (preg_match('/^htaccess\/?/i', self::$uri)) {
			self::$use_htaccess = true;
			self::$uri = substr(self::$uri, 9);
		}

		self::$orig_uri = self::$uri;
		self::$request_uri = self::$def['strip_request_uri'] ? preg_replace('#' . rtrim(self::$def['strip_request_uri'], '/') . '#i', '', $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI'];

		self::$orig_url = self::$uri . ($_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : '');

		if (!self::$db && !preg_match('/^(admin\/install|style)/', self::$uri)) {
			redirect('admin/install');
		}

		if (!preg_match('/^(admin\/install|style)/', self::$uri)) {
			if (!is_dir(DOCROOT . 'etc/tmp')) {
				die('Incomplete installation: directory /etc/tmp is missing. Please create this directory ang try again.');
			}

			if (!is_writable(DOCROOT . 'etc/tmp')) {
				die('Incomplete installation: directory /etc/tmp is not writable. Please set permissions to 0777 and try again.');
			}
		}

		if (!self::$uri) {
			self::$uri = self::$default_uri;
		}

		foreach (self::$route as $preg => $redirect) {
			if (preg_match('#' . $preg . '#i', self::$uri, $matches)) {

				foreach ($matches as $i => $m) {
					$redirect = preg_replace('#\$' . $i . '#', $m, $redirect);
				}

				self::$uri = $redirect;
			}
		}

		if (preg_match('/^admin(\/|\z)/i', self::$uri)) {
			self::$is_admin = true;
			self::$uri = substr(self::$uri, 6);
		}

		if (!self::$is_admin) {
			self::$language = self::config('language_front');
			self::loadI18N();
		}

		self::filterInput();
		self::unlimited();

		self::setViewData();
		self::setScriptConstants();

		self::$segments = explode('/', self::$uri);

		$controller = self::$controller;
		$method = 'index';
		$arguments = null;

		$modules = array();

		if (self::$db) {
			Notify::setup();

			foreach (self::$db->module() as $module) {
				$modules[$module['name']] = $module;
			}
		}

		foreach (self::$modules as $module_name => $module) {
			if (self::$db && ($mod = $modules[$module_name])) {
				$module->installed = true;
				$module->status = (int) $mod['status'];
				$module->registry = $mod;

				if ((int) $module->status) {
					if (method_exists($module, 'setEnv')) {
						$module::setEnv();
					}
				}
			}
		}

		if (!empty(self::$segments[0])) {
			$controller = self::$segments[0];
		} else {
			self::checkRedirect();
		}

		if (!empty(self::$segments[1]) && !is_numeric(self::$segments[1])) {
			$method = self::$segments[1];
		}

		if (!self::$is_admin && (int) self::config('maintenance') && !in_array($controller, array('style', 'script', 'gopay', 'robots'))) {
			$controller = 'default';
			$method = 'maintenance';
		}

		$arguments = array_slice(self::$segments, is_numeric(self::$segments[1]) ? 1 : 2);

		$class_name = 'Controller_' . ucfirst($controller);

		if (!self::autoload($class_name)) {
			self::show_404();
		}

		try {
			$class = new ReflectionClass($class_name);
		} catch (ReflectionException $e) {
			self::show_404();
		}

		$controller = $class->newInstance();

		self::$controller = $controller;
		View::$global_data['controller'] = $controller;

		try {
			$_method = $class->getMethod($method);

			if ($method[0] === '_') {
				self::show_404();
			}

			if ($_method->isProtected() or $_method->isPrivate()) {
				throw new ReflectionException('protected controller method');
			}
		} catch (ReflectionException $e) {
			if (call_user_func_array(array($controller, $method), $arguments) != Extendable::METHOD_NOT_FOUND) {
				$_method = null;
			} else {
				// Use __call instead
				$_method = $class->getMethod('__call');

				$arguments = array($method, $arguments);
			}
		}

		if ($_method) {
			$_method->invokeArgs(self::$controller, $arguments);
		}

		if (!self::$is_admin && self::$is_premium) {
			if (!self::validatelkey() && !self::$is_trial) {
				self::$is_premium = false;
				self::show_invalidlicense();
			}
		}
	}

	public static function error_handler($exception = NULL, $message = NULL, $file = NULL, $line = NULL)
	{
		if (error_reporting() == 0) {
			return;
		}

		if (!function_exists('tpl')) {
			die($message);
		}

		try {
			$PHP_ERROR = (func_num_args() === 5);

			$view = tpl('system/error' . (!self::$debug ? '_disabled' : '') . '.php');

			header('HTTP/1.1 500 Internal Server Error');

			try {
				$bt = debug_backtrace();
				foreach ($bt as $i => &$t) {
					unset($t['args']);
				}

				$bt = str_replace(DOCROOT, 'DOCROOT/', print_r($bt, true));
			} catch (Exception $e) {
				$bt = '';
			}

			// hide DB password in backtrace
			$view->backtrace = preg_replace('/(\[password\] =\> ).*/', '$1*****', $bt);

			$included_files = array();

			foreach (get_included_files() as $included_file) {
				$included_files[] = str_replace(DOCROOT, 'DOCROOT/', $included_file);
			}

			$view->included_files = $included_files;

			if ($PHP_ERROR) {
				$view->code = $exception;
				$view->message = $message;
				$view->file = str_replace(DOCROOT, 'DOCROOT/', $file);
				$view->line = $line;
			} else {

				$file = $exception->getFile();

				$view->code = $exception->getCode();
				$view->file = str_replace(DOCROOT, 'DOCROOT/', $file);
				$view->line = $exception->getLine();

				if (get_class($exception) == 'PDOException' || get_class($exception) == 'Odb_Exception') {
					$trace = $exception->getTrace();
					$view->message = 'Database error: ' . $exception->getMessage();

					$query = null;
					foreach ($trace as $row) {
						if ($row['function'] == 'query') {
							$query = $row['args'][0];
							break;
						}
					}

					$view->query = $query;
				} else {
					$view->message = (!empty(self::$error_codes[$view->code]) ? self::$error_codes[$view->code] : 'Exception') . ': ' . $exception->getMessage();
				}
			}

			if ($file && file_exists($file)) {
				$code = file($file);

				if ($code) {
					$code_lines = array();

					for ($i = max(0, $view->line - 6); $i < $view->line + 5; $i++) {
						if (isSet($code[$i])) {
							$code_lines[$i + 1] = $code[$i];
						}
					}

					$view->code_lines = $code_lines;
				}
			}

			self::log($view->code . ': ' . $view->message . '; ' . $view->file . ' [' . $view->line . ']', 'error');

			self::writeLog();

			Event::run('Core::error');

			while (ob_get_level() > self::$buffer_level) {
				ob_end_clean();
			}

			if (!self::$debug && self::$debug_report) {

				$k = md5(self::config('instid'));

				$errordata = serialize(array(
					'host' => $_SERVER['HTTP_HOST'],
					'version' => self::version . ' [' . join('; ', self::$servicepacks) . ']',
					'modules' => join('; ', array_keys(self::$modules)),
					'msg' => $view->code . ': ' . $view->message,
					'file' => $view->file . '[' . $view->line . ']',
					'query' => $view->query
						));

				$errordata = base64_encode($errordata ^ str_repeat($k, ceil(strlen($errordata) / strlen($k))));

				$chunks = str_split($errordata, 256);

				$reportdata = array(
					'instid' => substr(self::config('instid'), 5),
					'error' => $chunks
				);

				$view->report = 'http://www.webmex.cz/reporterror?' . http_build_query($reportdata);
			}

			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				echo 'Error: ' . $view->message . '; ' . $view->file . ' [' . $view->line . ']';
			} else {
				ob_end_clean();
				echo $view;
			}

			self::$error = true;

			exit(1);
		} catch (Exception $e) {
			die('Fatal Error: ' . $e->getMessage() . ' File: ' . $e->getFile() . ' Line: ' . $e->getLine());
		}
	}

	public static function shutdown_handler()
	{
		$output = '';

		if (self::$controller && method_exists(self::$controller, 'render')) {
			$output = (string) self::$controller->render();

			if (Core::$def['enable_short_links']) {
				$output = preg_replace_callback('/\{(\d+)\}/miu', array('self', 'replace_links_cb'), $output);
			}
		}

		if (self::$remove_flash) {
			if (!isSet($_SESSION['webmex_flash_remove'])) {
				$_SESSION['webmex_flash_remove'] = array();
			}

			foreach ($_SESSION as $name => $v) {
				if (substr($name, 0, 6) == 'flash_' && !in_array($name, $_SESSION['webmex_flash_remove'])) {
					$_SESSION['webmex_flash_remove'][] = $name;
				}
			}
		}

		self::$benchmark['stop_time'] = microtime(true);
		self::$benchmark['exec_time'] = round(self::$benchmark['stop_time'] - self::$benchmark['start_time'], 4);
		self::$benchmark['memory_usage'] = round(memory_get_usage() / 1024 / 1024, 2);

		self::writeLog();

		if (!self::$silent_mode && (($error = error_get_last()) AND !empty(self::$error_codes[$error['type']]))) {
			ob_get_level() and ob_clean();

			Core::error_handler(new ErrorException($error['message'], $error['type'], 0, $error['file'], $error['line']));

			exit(1);
		}


		if (!self::$error) {
			echo $output;
		}
	}

	private static function replace_links_cb($matches)
	{
		return url((int) $matches[1]);
	}

	public static function show_404()
	{
		self::checkRedirect();

		if (method_exists(self::$controller, '_show_404')) {
			self::$controller->_show_404();
		} else {

			header('HTTP/1.1 404 File Not Found');

			$page = null;

			if (PAGE_404) {
				$page = Core::$db->page[PAGE_404];
			}

			echo tpl('system/404.php', array('page' => $page));
		}

		exit;
	}

	public static function checkRedirect()
	{
		if ($redirect = Core::$db->redirect()->where('url = "' . Core::$db_conn->quote(trim(self::$orig_uri, '/')) . '" OR url ="' . trim(self::$request_uri, '/') . '"')->limit(1)->fetch()) {
			if ($redirect['object_type'] == 'product') {
				$obj = Core::$db->product[(int) $redirect['object_id']];
			} else if ($redirect['object_type'] == 'page') {
				$obj = Core::$db->page[(int) $redirect['object_id']];
			}

			if ($obj) {
				redirect($obj, '301');
			}
		}
	}

	public static function show_invalidlicense()
	{
		self::$controller->tpl = null;

		$d = base64_encode(json_encode(array(
					's' => array(
						'h' => $_SERVER['HTTP_HOST'],
						's' => $_SERVER['SERVER_SOFTWARE'],
						'i' => $_SERVER['SERVER_ADDR'],
						'r' => $_SERVER['REMOTE_ADDR'],
						'a' => $_SERVER['SERVER_ADMIN']
					),
					'l' => self::config('license_key'),
					'id' => self::config('instid')
				)));

		echo 'Your license is invalid <img src="http://www.webmex.cz/license/invalid?d=' . $d . '" alt="" width="1" height="1" />';
		exit;
	}

	public static function decodelKey()
	{
		$lkey = self::config('license_key');

		if (!$lkey) {
			return null;
		}

		$k = md5(self::config('instid'));
		$data = @base64_decode($lkey);
		$data = @unserialize($data ^ str_repeat($k, ceil(strlen($data) / strlen($k))));

		if ($data && is_array($data) && $data[3] == self::config('instid') && !empty($data[6])) {
			return $data;
		}

		return false;
	}

	public static function validatelkey()
	{
//		if (!self::config('license_key') && file_exists(DOCROOT . 'etc/license.php')) {
//			return true;
//		}

		$data = self::decodelKey();

		if (!is_array($data) || empty($data[4])) {
			return null;
		}

		$host = preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);

		if ($host == 'localhost' || preg_match('/\.docasna\.cz$/', $host)) {
			return true;
		}

		foreach (explode(';', $data[4]) as $domain) {
			if ($domain == $host || (substr($domain, 0, 1) == '*' && preg_match('/^(?:([^\.]+)\.)?' . str_replace('.', '\\.', substr($domain, 2)) . '$/', $host))) {
				return true;
			}
		}

		return false;
	}

	public static function checklkey($instid = null, $lkey = null)
	{
		if (!$lkey) {
			$lkey = self::config('license_key');
		}

		if (!$instid) {
			$instid = self::config('instid');
		}

		if (self::$db) {
			self::$db->config()->where('name', 'last_lcheck')->update(array(
				'value' => time()
			));
		}

		if (!$lkey) {
			return null;
		}

		if ($_SERVER['HTTP_HOST'] == 'localhost' || preg_match('/\.docasna\.cz$/', $_SERVER['HTTP_HOST'])) {
			return true;
		}

		$ret = @file_get_contents('http://www.webmex.cz/license/check?k=' . $lkey . '&id=' . $instid . '&h=' . $_SERVER['HTTP_HOST'] . '&u=' . urlencode(self::$url));

		if ($ret && strlen($ret) <= 20) {
			if ($ret == 'OK') {
				return true;
			} else {
				$_SESSION['invalid_license'] = $ret;
			}
		} else {
			// validation failure
			return true;
		}

		return false;
	}

	/** @todo Odstranit */
	public static function trial()
	{

	}

	/** @todo Odstranit */
	public static function unlimited($check = false)
	{
		return false;
	}

	public static function xss_clean($string)
	{
		if (trim($string) == '')
			return $string;

		if (self::$magic_quotes_gpc === true) {
			$string = stripslashes($string);
		}

		$string = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $string);
		// fix &entitiy\n;

		$string = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $string);
		$string = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $string);
		$string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');

		if (!self::$is_admin) {
			// remove any attribute starting with "on" or xmlns
			$string = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*>#iu', '$1>', $string);
			// remove javascript: and vbscript: protocol
			$string = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $string);
			$string = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $string);
			$string = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $string);
			//<span style="width: expression(alert('Ping!'));"></span>
			// only works in ie...
			$string = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*>#i', '$1>', $string);
			$string = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*>#i', '$1>', $string);
			$string = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*>#iu', '$1>', $string);
			//remove namespaced elements (we do not need them...)
			$string = preg_replace('#</*\w+:\w[^>]*>#i', '', $string);
			//remove really unwanted tags

			do {
				$oldstring = $string;
				$string = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*>#i', '', $string);
			} while ($oldstring != $string);
		}

		return $string;
	}

	public static function glob($dir, $only_dir = false, $recursive = false)
	{
		$dir = trim((string) $dir, '/');
		$result = array();
		$match = '';

		if (!preg_match('/\*/', $dir)) {
			$match = '/*';
		}

		if ($glob = @glob(DOCROOT . $dir . $match, $only_dir ? GLOB_ONLYDIR : 0)) {
			if ($recursive) {
				foreach ($glob as $file) {
					if (is_dir($file)) {
						$result = array_merge($result, self::glob(substr($file, strlen(DOCROOT . $dir) - 1), $only_dir, $recursive));
					} else {
						$result[] = $file;
					}
				}
			} else {
				$result = array_merge($result, $glob);
			}
		}

		if ($glob = @glob(DOCROOT . 'etc/' . $dir . $match, $only_dir ? GLOB_ONLYDIR : 0)) {
			if ($recursive) {
				foreach ($glob as $file) {
					if (is_dir($file)) {
						$result = array_merge($result, self::glob(substr($file, strlen(DOCROOT . $dir) - 1), $only_dir, $recursive));
					} else {
						$result[] = $file;
					}
				}
			} else {
				$result = array_merge($result, $glob);
			}
		}

		foreach (self::$autoload_paths as $load_dir) {
			if ($glob = @glob($load_dir . $dir . $match, $only_dir ? GLOB_ONLYDIR : 0)) {
				if ($recursive) {
					foreach ($glob as $file) {
						if (is_dir($file)) {
							$result = array_merge($result, self::glob(substr($file, strlen(DOCROOT . $dir) - 1), $only_dir, $recursive));
						} else {
							$result[] = $file;
						}
					}
				} else {
					$result = array_merge($result, $glob);
				}
			}
		}

		return $result;
	}

	public static function findFile($file)
	{
		$file = ltrim($file, '/');

		foreach (self::$autoload_paths as $dir) {
			if (file_exists(DOCROOT . $dir . $file)) {
				return DOCROOT . $dir . $file;
			}
		}

		return false;
	}

	public static function autoLoad($class)
	{

		if (in_array($class, self::$autoload_not_found)) {
			return false;
		}

		if (class_exists($class, false)) {
			return true;
		}

		$parts = preg_split('/_(?=[A-Z])/', $class);

		if (!in_array($parts[0], array('Controller')) || count($parts) == 1) {
			array_unshift($parts, 'include');
		} else if (self::$is_admin && count($parts) > 1) {
			array_unshift($parts, 'admin');
		}

		$path = implode('/', array_map('strtolower', $parts)) . '.php';

		$status = false;

		foreach (self::$autoload_paths as $dir) {
			if (file_exists(DOCROOT . $dir . $path)) {
				try {
					require DOCROOT . $dir . $path;
					$status = true;
				} catch (Exception $e) {

				}
			}
		}

		if ($status) {
			return true;
		}

		self::$autoload_not_found[] = $class;

		return false;
	}

	public static function module($module_name)
	{
		if (self::$modules[$module_name]) {
			return self::$modules[$module_name];
		}

		return null;
	}

	public static function output_buffer($output)
	{
		echo $output;

		return $output;
	}

	public static function log($msg, $type = 'debug')
	{
		self::$log[] = array('time' => date('H:i:s'), 'type' => $type, 'msg' => $msg);
	}

	public static function writeLog()
	{
		$out = '';

		foreach (self::$log as $rec) {
			$out .= $rec['time'] . "\t(" . $rec['type'] . ")\t\t" . $rec['msg'] . "\n";
		}

		if (is_writable(DOCROOT . 'etc/log/') && !empty($out)) {
			@file_put_contents(DOCROOT . 'etc/log/' . date('Y-m-d') . '.log', $out, FILE_APPEND);
			self::$log = array();
		}
	}

	public static function addRoute($match, $target)
	{
		self::$route[$match] = $target;
	}

}
