<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Product extends Model
{

	public static $customer_group;

	public function getURL()
	{
		$prefix = '';

		if ((int) Core::config('url_full_path') && $this->row['default_page'] && ($page = Core::$db->page[(int) $this->row['default_page']])) {
			$prefix = $page->model->getURL(true);
		}

		return ($prefix ? rtrim($prefix, '/') . '/' : '') . $this->row['sef_url'] . '-p' . $this->row['id'];
	}

	public function getFees()
	{
		$fees = 0;

		if ($fee = (float) $this->row->get('recycling_fee')) {
			$fees += $fee;
		}

		if ($fee = (float) $this->row->get('copyright_fee')) {
			$fees += $fee;
		}

		return $fees;
	}

	public function getVat()
	{
		if ((int) Core::config('vat_payer')) {
			$vat = $this->row->vat;

			if ($vat['id']) {
				return (float) $vat['rate'];
			}
		}

		return null;
	}

	public function getBestAvailability()
	{
		$avs = array();

		if ($this->row->availability && $this->row->availability['days'] !== null) {
			$avs[$this->row->availability['days']] = $this->row->availability;
		}

		if (!isset($avs[0]) && ($attrs = $this->row->product_attributes()->order('name ASC, id ASC'))) {
			foreach ($attrs as $attr) {
				if ($attr->availability && $attr->availability['days'] !== null) {
					$avs[$attr->availability['days']] = $attr->availability;
				}

				if (isset($avs[0])) {
					break;
				}
			}
		}

		uksort($avs, array($this, 'sortAvailabilities'));

		return $avs ? current($avs) : null;
	}

	private function sortAvailabilities($a, $b)
	{
		if ($a < 0) {
			return 1;
		}

		return $a > $b;
	}

	public function getPricebeforediscount()
	{
		return $this->row->get('price');
	}

	public function getPrice()
	{
		$price = $this->row->get('price');

		if ($price === null && !Core::$is_admin && ($cost = $this->row->get('cost')) && ($margin = $this->row->get('margin'))) {
			$price = round($cost * (1 + ($margin / 100)), 2);
		} else if ($price === null) {
			return null;
		}

		if (!Core::$is_admin && !(int) $this->row->get('ignore_discounts')) {
			$total_discount = 0;

			if (Core::$is_premium && !self::$customer_group) {
				$group_id = Core::config('customer_group_default');

				if (Customer::$logged_in) {
					$group_id = Core::config('customer_group_registered');

					if ((int) Customer::get('customer_group_id')) {
						$group_id = Customer::get('customer_group_id');
					}
				}

				self::$customer_group = Core::$db->customer_group[(int) $group_id];
			}

			if (Core::$is_premium && self::$customer_group) {
				$price = $price * (float) self::$customer_group['coefficient'];
			}


			if (Core::$is_premium && ($pages = Core::$db->page()->where('id', Core::$db->product_pages()->where('product_id', $this->row)->select('page_id'))->where('status', 1)->where('discount IS NOT NULL'))) {
				foreach ($pages as $page) {
					if (!$page['discount_date'] || ((int) $page['discount_date'] + 86399) >= time()) {
						$total_discount += (float) $page['discount'];
					}
				}
			}

			if (Core::$is_premium && ($discount = $this->row->get('discount')) && (!$this->row->get('discount_date') || ((int) $this->row->get('discount_date') + 86399) >= time())) {
				$total_discount += (float) $discount;
			}

			if ($price && Customer::$logged_in && Customer::get('discount')) {
				$total_discount += (float) Customer::get('discount');
			}

			if ($total_discount) {
				$price = $price * (1 - ($total_discount / 100));
			}
		}

		return $price;
	}

	public function getDiscount()
	{
		if (!Core::$is_premium) {
			return null;
		}

		$discount = $this->row->get('discount');

		if (!Core::$is_admin && $this->row->get('discount_date') && ((int) $this->row->get('discount_date') + 86399) < time()) {
			return null;
		}

		if ((int) $this->row->get('ignore_discounts')) {
			return null;
		}

		return $discount;
	}

	public function setStock($stock_change, $attributes = null)
	{
		$type = 'product';

		$stock = $new_stock = null;
		$product_id = $this->row->get('id');

		$stock = $this->row->get('stock');

		if ($stock !== null && $stock !== '') {
			$new_stock = $stock + $stock_change;
		}

		if ($attributes && ($attr_ids = explode(';', $attributes))) {
			$attrs = array();
			$attribute = null;
			$attr_stock = null;

			foreach ($attr_ids as $attr_id) {

				if (!is_numeric($attr_id)) {
					continue;
				}

				$attr = Core::$db->product_attributes[(int) $attr_id];

				if ($attr && $attr['product_id'] = $product_id) {

					if ($attr['stock'] !== null && $attr['stock'] !== '') {
						$attrs[] = $attr;

						if ($attr_stock === null || $attr['stock'] < $attr_stock) {
							$attr_stock = (int) $attr['stock'];
							$attribute = $attr;
						}
					}
				}
			}

			Event::run('Model_Product::setStock.attributes', $product_id, $attr_ids, $attrs, $attribute, $attr_stock);

			if ($attribute) {
				$type = 'attribute';
				$stock = $attr_stock;

				$new_stock = $stock + $stock_change;
			}
		}

		$availability = (int) Core::config('stock_out_availability');
		$product_stock = $this->row->get('stock');

		if ($type == 'product') {
			$update_data = array('stock' => $new_stock);

			if ($new_stock <= 0 && $availability) {
				$update_data['availability_id'] = (int) $availability;
			}

			if ($new_stock <= 0 && (int) Core::config('stock_out_set_status')) {
				$update_data['status'] = 0;
			}

			$this->row->update($update_data);

			$email_data = array(
				'sku' => $this->row->get('sku'),
				'name' => $this->row->get('name'),
				'price' => $this->row->get('price'),
				'stock' => $new_stock
			);
		} else if (isSet($attrs)) {
			foreach ($attrs as $attr) {
				$_new_stock = $attr['stock'] + $stock_change;

				$update_data = array('stock' => $_new_stock);

				if ($_new_stock <= 0 && $availability && $attr['availability_id']) {
					$update_data['availability_id'] = (int) $availability;
				}

				$attr->update($update_data);
			}

			if ($availability && $this->row->availability['id'] && $this->row->availability['days'] >= 0) {
				$min_av_id = 0;
				$min_av_days = 99;
				$avs = array();
				$max_stock = $product_stock !== null ? $product_stock : 0;
				$av_unknown_id = 0;

				foreach ($this->row->product_attributes() as $attr) {
					if ($attr['stock'] !== null && $attr['stock'] !== '' && (int) $attr['stock'] > $max_stock) {
						$max_stock = (int) $attr['stock'];
					}

					if ($attr->availability['id']) {
						$avs[] = $attr->availability['days'];

						if ($attr->availability['days'] < 0) {
							$av_unknown_id = $attr->availability['id'];
						}
					}

					if ($attr->availability['id'] && $attr->availability['days'] >= 0 && $min_av_days > $attr->availability['days']) {
						$min_av_id = $attr->availability['id'];
						$min_av_days = $attr->availability['days'];
					}
				}

				Event::run('Model_Product::setStock.attributes_set_availability', $this->row, $min_av_id, $min_av_days, $av_unknown_id);

				if (count($avs) && max($avs) < 0 && $av_unknown_id) {
					$this->row->update(array('availability_id' => $av_unknown_id));
				} else if ($min_av_id && $this->row->availability['days'] != $min_av_days) {
					$this->row->update(array('availability_id' => $min_av_id));
				}

				if ($max_stock !== null && $max_stock <= 0 && (int) Core::config('stock_out_set_status')) {
					$this->row->update(array('status' => 0));
				}
			}

			if ($attribute) {
				$sku = $attribute['sku'];

				if (strrpos($sku, '*') !== false) {
					$sku = preg_replace('/\*/', $this->row->get('sku'), $sku);
				}

				$email_data = array(
					'sku' => $sku ? $sku : $this->row->get('sku'),
					'name' => $this->row->get('name') . ' [' . $attribute['name'] . ': ' . $attribute['value'] . ']',
					'price' => estPrice($attribute['price'], $this->row->get('price')),
					'stock' => $new_stock
				);
			}
		}

		if ($stock !== null && $new_stock !== null && $email_data) {
			if (Core::config('stock_notify') !== '' && $new_stock < (int) Core::config('stock_notify')) {
				Email::event('low_stock', Core::config('email_notify'), null, $email_data);
			}
		}
	}

	public static function update($data, $row = null)
	{
		if ($row && isSet($data['sef_url']) && !empty($row['sef_url']) && $data['sef_url'] != $row['sef_url']) {
			$url = $row['sef_url'] . '-p' . $row['id'];

			Core::$db->redirect()->where('url', $data['sef_url'] . '-p' . $row['id'])->delete();

			if ($redirect = Core::$db->redirect()->where('url', $url)->limit(1)->fetch()) {
				$redirect->delete();
			}

			Core::$db->redirect(array(
				'url' => $url,
				'object_type' => 'product',
				'object_id' => $row['id']
			));
		}

		if ($row && isset($data['margin'])) {
			if ($row['cost'] && !isset($data['price'])) {
				$data['price'] = $row['cost'] * ($data['margin'] / 100 + 1);
			} else if ($row['price'] && !isset($data['cost'])) {
				$data['cost'] = $row['cost'] / ($data['margin'] / 100 + 1);
			}
		}

		return $data;
	}

	public static function getDataHash($data, $fields = array())
	{
		$row_data = $product = $product_id = null;

		if (is_numeric($data)) {
			if ($product = Core::$db->product[(int) $data]) {
				$row_data = $product->as_array();
			}
		} else if (is_object($data) && method_exists($data, 'as_array')) {
			$row_data = $data->as_array();
		} else if (is_object($data) && $data instanceof WebmexAPI_Bucket) {
			$row_data = $data->data->as_array();
		} else {
			$row_data = $data;
		}

		if ($row_data && $row_data['id']) {
			$product_id = $row_data['id'];
		}

		if ($fields && is_array($fields)) {
			$_row_data = array();

			foreach ($fields as $field) {
				if (isset($row_data[$field])) {
					$_row_data[$field] = $row_data[$field];
				}
			}

			$row_data = prepare_data('product', $_row_data, false);
		}

		unset($row_data['id'], $row_data['import_unique'], $row_data['import_uid'], $row_data['import_schema'], $row_data['last_change'], $row_data['last_change_user']);

		$hash = md5(join(';', array_values($row_data)));

		if (in_array('categories', $fields) || in_array('pages', $fields) || in_array('manufacturers', $fields)) {

			if ($product_id) {
				$pages = Core::$db->product_pages()->select('GROUP_CONCAT(page_id) as pages')->where('product_id', $product_id)->fetch();

				if ($pages && $pages['pages'] !== null) {
					$hash .= '_' . $pages['pages'];
				}
			} else if (!empty($data['categories'])) {
				$pages = array();

				foreach ($data['categories'] as $path) {
					$pages[] = Core::$api->page->findByPath($path);
				}

				if ($pages) {
					$hash .= '_' . join(',', $pages);
				}
			}
		}

		if (in_array('files', $fields)) {
			if ($product_id) {
				$files = Core::$db->product_files()->select('GROUP_CONCAT(filename) as files')->where('product_id', $product_id)->fetch();

				if ($files && $files['files'] !== null) {
					$hash .= '_' . $files['files'];
				}
			} else if (!empty($data['files'])) {
				if ($files && $files['files'] !== null) {
					$hash .= '_' . join(',', $files['files']);
				}
			}
		}

		if (in_array('attributes', $fields)) {
			if ($product_id) {
				$attrs = Core::$db->product_attributes()->select('GROUP_CONCAT(name, value) as attrs')->where('product_id', $product_id)->fetch();

				if ($attrs && $attrs['attrs'] !== null) {
					$hash .= '_' . $attrs['attrs'];
				}
			} else if (!empty($data['attributes']) && is_array($data['attributes'])) {
				$attrs = array();
				foreach ($data['attributes'] as $attr) {
					$attrs[] = @$attr['name'] . @$attr['value'];
				}

				if ($attrs) {
					$hash .= '_' . join(',', $attrs);
				}
			}
		}

		echo $hash;

		return md5($hash);
	}

}