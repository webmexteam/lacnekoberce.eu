<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Page extends Model
{

	public function getURL($incl_parents = null)
	{
		if ($this->row['id'] == PAGE_INDEX) {
			return null;
		}

		if ($incl_parents === null) {
			$incl_parents = (int) Core::config('url_full_path') ? true : false;
		}

		$prefix = '';

		if ($incl_parents && $this->row['parent_page'] && ($parent = Core::$db->page[$this->row['parent_page']])) {
			$prefix = $parent->model->getURL(true) . '/' . $prefix;
		}

		return ($prefix ? rtrim($prefix, '/') . '/' : '') . $this->row['sef_url'] . '-a' . $this->row['id'];
	}

	public function getFullPath()
	{
		$path = array(
			$this->row['id'] => $this->row['name']
		);

		$parent_id = $this->row['parent_page'];

		while ($parent_id && ($parent = Core::$db->page[(int) $parent_id])) {
			$path[$parent['id']] = $parent['name'];
			$parent_id = $parent['parent_page'];
		}

		return array_reverse($path, true);
	}

	public static function update($data, $row = null)
	{
		if ($row && isSet($data['sef_url']) && !empty($row['sef_url']) && $data['sef_url'] != $row['sef_url']) {
			$url = $row['sef_url'] . '-a' . $row['id'];

			Core::$db->redirect()->where('url', $data['sef_url'] . '-a' . $row['id'])->delete();

			if ($redirect = Core::$db->redirect()->where('url', $url)->limit(1)->fetch()) {
				$redirect->delete();
			}

			Core::$db->redirect(array(
				'url' => $url,
				'object_type' => 'page',
				'object_id' => $row['id']
			));
		}

		return $data;
	}

}