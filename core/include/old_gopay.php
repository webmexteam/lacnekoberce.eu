<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
require_once(APPROOT . 'vendor/gopay/gopay_helper.php');

class GopayHelper extends WEBMEX_GopayHelper
{

	function fullIntegrationURL()
	{
		return Gopay::$gp_url_full;
	}

	function ws()
	{
		return Gopay::$gp_url_wsdl;
	}

}

class Gopay
{

	public static $goId;
	public static $secret;
	public static $test;
	// soap | pear
	protected static $soap_driver;
	public static $order_model = 'order';
	public static $gp_url_wsdl = 'https://gate.gopay.cz/axis/EPaymentService?wsdl';
	public static $gp_url_full = 'https://gate.gopay.cz/zaplatit-plna-integrace';
	public static $url_callback = 'gopay/callback';
	public static $url_success = 'gopay/success';
	public static $url_success_supercash = 'gopay/success/supercash';
	public static $url_success_mojeplatba = 'gopay/success/mojeplatba';
	public static $url_success_eplatby = 'gopay/success/eplatby';
	public static $url_success_mpenize = 'gopay/success/mpenize';
	public static $url_success_bank = 'gopay/success/bank';
	public static $url_fail = 'gopay/fail';
	public static $gpClass;

	public static function setup()
	{
		if (!self::$gpClass || !self::$goId) {
			if (class_exists('SoapClient', false)) {
				self::$soap_driver = 'soap';
			} else if (class_exists('SOAP_WSDL', false)) {
				self::$soap_driver = 'pear';
			} else {
				throw new Exception('No SOAP library available');
			}

			require_once(DOCROOT . 'etc/gopay.php');
			require_once(APPROOT . 'vendor/gopay/gopay_' . self::$soap_driver . '.php');

			self::$goId = $gopay['goId'];
			self::$secret = $gopay['secret'];
			self::$test = (bool) $gopay['test'];

			if (self::$test) {
				self::$gp_url_wsdl = 'https://testgw.gopay.cz/axis/EPaymentService?wsdl';
				self::$gp_url_full = 'https://testgw.gopay.cz/zaplatit-plna-integrace';
			}

			if (empty(self::$goId) || empty(self::$secret)) {
				throw new Exception('Wrong Gopay configuration');
			}

			self::$gpClass = 'Gopay' . ucfirst(self::$soap_driver);
		}
	}

	public static function pay($order, $channels = null, $default_channel = null, $send_customer_data = true)
	{
		if (!$order || !$order['id']) {
			return false;
		}

		$desc = $order['first_name'] . ' ' . $order['last_name'] . ', ' . fdate($order['received'], true);

		$method = 'createEshopPayment';

		$post_data = array(
			self::$goId + 0,
			$desc,
			round((float) $order['total_incl_vat'] * 100, 0),
			$order['id'],
			url(self::$url_callback, null, true),
			url(self::$url_callback, null, true),
			self::$secret,
			$channels
		);

		if ($send_customer_data) {
			$method = 'createCustomerEshopPayment';

			$country = 'CZE';

			if (preg_match('/slovensk|slovakia/iu', $order['country'])) {
				$country = 'SLO';
			} else if (preg_match('/polsk|poland/iu', $order['country'])) {
				$country = 'PLN';
			}

			$post_data = array_merge($post_data, array(
				$order['first_name'],
				$order['last_name'],
				$order['city'],
				$order['street'],
				preg_replace('/\s/', '', $order['zip']),
				$country,
				$order['email'],
				preg_replace('/\s/', '', $order['phone'])
					));
		}

		$paymentSessionId = call_user_func_array(array(self::$gpClass, $method), $post_data);

		if ($paymentSessionId > 0) {
			$order->update(array(
				'payment_session' => preg_replace('/\.[0]+$/', '', $paymentSessionId),
				'payment_description' => $desc
			));

			$encryptedSignature = GopayHelper::encrypt(
							GopayHelper::hash(GopayHelper::concatPaymentSession(
											self::$goId + 0, $paymentSessionId, self::$secret
									)), self::$secret
			);

			redirect(GopayHelper::fullIntegrationURL() . "?sessionInfo.eshopGoId=" . self::$goId . "&sessionInfo.paymentSessionId=" . $paymentSessionId . "&sessionInfo.encryptedSignature=" . $encryptedSignature . ($default_channel ? "&paymentChannel=" . strtoupper($default_channel) : ''));

			return true;
		} else {
			// error
			return $paymentSessionId;
		}

		return false;
	}

	public static function callback($data = null)
	{
		if (!($result = self::validateCallback($data))) {
			return false;
		}

		$status = $result[0];
		$order = $result[1];

		$_SESSION['gopay_order_id'] = $order['id'];

		session_write_close();

		if ($status > 0) {
			$order->update(array(
				'payment_realized' => time()
			));

			Notify::payment('Gopay.cz', $order);

			redirect(url(self::$url_success, array('order' => $order['id']), true));
		} else if ($status == -3) {
			redirect(url(self::$url_success_supercash, null, true));
		} else if ($status == -4) {
			redirect(url(self::$url_success_mojeplatba, null, true));
		} else if ($status == -5) {
			redirect(url(self::$url_success_eplatby, null, true));
		} else if ($status == -6) {
			redirect(url(self::$url_success_mpenize, null, true));
		} else if ($status == -7) {
			redirect(url(self::$url_success_bank, null, true));
		} else {
			// payment canceled or fraud
			redirect(url(self::$url_fail, null, true));
		}
	}

	public static function notify($data = null)
	{
		if (!($result = self::validateCallback($data))) {
			return false;
		}

		$status = $result[0];
		$order = $result[1];

		if ($status == 1) {
			$order->update(array(
				'payment_realized' => time()
			));

			Notify::payment('Gopay.cz', $order);

			return true;
		}

		return false;
	}

	private static function validateCallback($data = null)
	{
		if ($data === null) {
			$data = $_GET;
		}

		if (empty($data)) {
			return false;
		}

		$order_model = self::$order_model;

		$paymentSessionId = $data['paymentSessionId'];
		$goId = $data['eshopGoId'];
		$variableSymbol = $data['variableSymbol'];
		$encryptedSignature = $data['encryptedSignature'];

		if (empty($paymentSessionId) || empty($goId) || empty($variableSymbol) || empty($encryptedSignature)) {
			return false;
		}

		if (!($order = Core::$db->$order_model()->where('payment_session', $paymentSessionId)->fetch()) || $order['payment_realized']) {
			return false;
		}

		$is_valid = GopayHelper::checkPaymentIdentity(
						$goId + 0, $paymentSessionId, $variableSymbol, $encryptedSignature, self::$goId, $order['id'], self::$secret
		);

		if (!$is_valid) {
			// fraud
			return array(-1, $order);
		}

		$result = call_user_func_array(array(self::$gpClass, 'isEshopPaymentDone'), array(
			$paymentSessionId + 0,
			self::$goId + 0,
			$order['id'],
			round((float) $order['total_incl_vat'] * 100, 0),
			$order['payment_description'],
			self::$secret
				));

		return array($result, $order);
	}

}