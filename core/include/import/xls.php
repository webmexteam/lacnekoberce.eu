<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Import_Xls
{

	protected $file;
	protected $xls;
	protected $offset = 1;
	protected $colcount = 0;
	public $error = false;

	public function __construct($file)
	{
		require_once(APPROOT . 'vendor/phpexcel/excel_reader2.php');

		$this->file = $file;

		$this->xls = new Spreadsheet_Excel_Reader($this->file);

		$this->colcount = $this->xls->colcount();
	}

	public function reset()
	{
		$this->offset = 1;
	}

	public function findAllColumns()
	{
		$this->reset();

		$cols = array();

		for ($i = 1; $i <= $this->xls->colcount(); $i++) {
			$cols[] = $this->xls->val(1, $i);
		}

		return $cols;
	}

	public function getColumnIndex($col)
	{
		$index = -1;

		foreach ($this->findAllColumns() as $i => $c) {
			$c = trim($c);

			if ($c == $col) {
				$index = $i;
			}
		}

		$this->reset();

		return $index;
	}

	public function countElement($el = null)
	{
		$this->reset();

		return $this->xls->rowcount() - 1;
	}

	public function getElement()
	{
		$data = array();

		for ($i = 1; $i <= $this->colcount; $i++) {
			$data[] = $this->xls->val($this->offset, $i);
		}

		$this->offset++;

		if (count($data) > 1 && join('', $data)) {
			return $data;
		}

		return false;
	}

	public function analyze()
	{
		if ($this->error) {
			return false;
		}

		// common tag names
		$common_cols = array(
			'name' => array('NAME', 'PRODUCT NAME', 'NAZEV', 'PRODUCT'),
			'ean13' => array('EAN', 'EAN13'),
			'sku' => array('SKU', 'CODE'),
			'price' => array('PRICE', 'PRICE_VAT', 'CENA'),
			'description' => array('DESCRIPTION', 'POPIS'),
			'files' => array('IMGURL', 'IMAGE', 'OBRAZEK'),
			'categories' => array('CATEGORYTEXT', 'CATEGORY'),
		);

		$common_uniques = array('sku', 'ean13', 'name');

		if ($elements = $this->findAllColumns()) {

			$col_matches = array();

			if (!empty($_SESSION['dataimport']['elements']) && is_array($_SESSION['dataimport']['elements'])) {
				foreach ($_SESSION['dataimport']['elements'] as $param => $el) {
					$col_matches[$param] = $el;
				}
			} else {
				foreach ($elements as $i => $col) {
					foreach ($common_cols as $tag => $common) {
						if (in_array(strtoupper(dirify($col)), $common)) {
							$col_matches[$tag] = $i;
							break;
						}
					}
				}
			}

			$unique = null;

			if (!empty($_SESSION['dataimport']['options']['product_unique'])) {
				$unique = $_SESSION['dataimport']['options']['product_unique'];
			} else {
				foreach ($common_uniques as $tag) {
					if ($col_matches[$tag]) {
						$unique = $tag;
						break;
					}
				}

				if (in_array('id', $elements) && in_array('price', $elements) && in_array('name', $elements)) {
					$unique = 'id';
				}
			}

			$sample = null;

			$this->reset();

			$this->getElement();

			while ($product = $this->getElement()) {
				if (!$sample && mt_rand(1, 5) == 5) {
					$sample = $product;
					break;
				}
			}

			return array(
				'filesize' => round(filesize($this->file) / 1024 / 1024, 2),
				'elements' => $elements,
				'suggest' => $col_matches,
				'product_element' => null,
				'product_count' => $this->countElement(),
				'unique' => $unique,
				'sample' => trim($this->cols2Text($elements, $sample))
			);
		}

		return false;
	}

	private function cols2Text($cols, $data)
	{
		$str = '';

		if (!is_array($data)) {
			return '';
		}

		foreach ($data as $i => $v) {
			$str .= "<" . $cols[$i] . "> = " . utf8_encode($v) . "\n";
		}

		return htmlspecialchars($str);
	}

}