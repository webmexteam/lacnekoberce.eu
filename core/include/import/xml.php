<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Import_Xml extends XMLReader
{

	protected $file;
	public $error = false;

	public function __construct($file)
	{
		$this->file = $file;

		if (!$this->open($this->file)) {
			$this->error = true;
		}
	}

	public function reset()
	{
		$this->close();
		$this->open($this->file);
	}

	public function findAllElements()
	{
		$elements = array();
		$path = array();

		while (@$this->read()) {
			if ($this->nodeType == XMLReader::ELEMENT && !$this->isEmptyElement) {
				$path[] = $this->name;

				if (!in_array($path, $elements)) {
					$elements[] = $path;
				}
			} else if ($this->nodeType == XMLReader::END_ELEMENT) {
				array_pop($path);
			}
		}

		return $elements;
	}

	public function getElement($name)
	{
		while ($this->read()) {
			if ($this->nodeType == XMLReader::ELEMENT && $this->name == $name) {
				return new SimpleXMLElement($this->readOuterXML(), LIBXML_NOCDATA);
			}
		}

		return false;
	}

	public function countElement($name)
	{
		$count = 0;

		while ($this->read()) {
			if ($this->nodeType == XMLReader::ELEMENT && $this->name == $name) {
				$count++;
			}
		}

		return $count;
	}

	public function analyze()
	{
		if ($this->error) {
			return false;
		}

		// common tag names
		$common_tags = array(
			'name' => array('NAME', 'PRODUCTNAME', 'NAZEV', 'PRODUCT'),
			'ean13' => array('EAN', 'EAN13'),
			'sku' => array('SKU', 'CODE'),
			'price' => array('PRICE', 'PRICE_VAT', 'CENA'),
			'description' => array('DESCRIPTION', 'POPIS'),
			'files' => array('IMGURL', 'OBRAZEK'),
			'categories' => array('CATEGORYTEXT'),
		);

		$common_uniques = array('sku', 'ean13', 'name');

		$tag_matches = array();

		if ($elements = $this->findAllElements()) {
			$parents = array();

			foreach ($elements as $element) {

				foreach (array_slice($element, 1, -1) as $i => $parent) {
					if (!isset($parents[$parent])) {
						$parents[$parent] = 0;
					}

					$parents[$parent] += (1 / ($i + 1));
				}
			}

			arsort($parents);

			if (!empty($_SESSION['dataimport']['options']['product_element'])) {
				$product_element = $_SESSION['dataimport']['options']['product_element'];
			} else {
				$product_element = key($parents);
			}

			$variant_element = null;

			if (!empty($_SESSION['dataimport']['options']['variant_element'])) {
				$variant_element = $_SESSION['dataimport']['options']['variant_element'];
			}

			if (!empty($_SESSION['dataimport']['elements']) && is_array($_SESSION['dataimport']['elements'])) {
				foreach ($_SESSION['dataimport']['elements'] as $param => $el) {
					$tag_matches[$param] = $el;
				}
			} else {
				foreach ($elements as $element) {
					if (isSet($element[count($element) - 2]) && $element[count($element) - 2] == $product_element) {
						$el_name = $element[count($element) - 1];

						if (($pos = array_search($product_element, $element)) !== false) {
							$el_path = join('/', array_slice($element, $pos + 1));
						} else {
							$el_path = $el_name;
						}

						foreach ($common_tags as $tag => $common) {
							if (in_array(strtoupper($el_name), $common)) {
								$tag_matches[$tag] = $el_path;
								break;
							}

							if (!$variant_element && ($el_name == 'product_id' || $el_name == 'attribute_product_id')) {
								$variant_element = $el_name;
							}
						}
					}
				}
			}

			$unique = null;

			if (!empty($_SESSION['dataimport']['options']['product_unique'])) {
				$unique = $_SESSION['dataimport']['options']['product_unique'];
			} else {
				foreach ($common_uniques as $tag) {
					if ($tag_matches[$tag]) {
						$unique = $tag;
						break;
					}
				}

				if (in_array(array('datapack'), $elements) && in_array(array('datapack', 'product', 'id'), $elements)) {
					$unique = 'id';
				}
			}

			$this->reset();

			$count = $this->countElement($product_element);

			$this->reset();

			$sample = null;

			while ($product = $this->getElement($product_element)) {
				if (!$sample && mt_rand(1, 5) == 5) {
					$sample = $product;
					break;
				}
			}

			return array(
				'filesize' => round(filesize($this->file) / 1024 / 1024, 2),
				'elements' => $elements,
				'suggest' => $tag_matches,
				'product_element' => $product_element,
				'product_count' => $count,
				'unique' => $unique,
				'variant_element' => $variant_element,
				'sample' => trim(htmlspecialchars($this->node2Text($sample)))
			);
		}

		return false;
	}

	private function node2Text($node, $level = 0)
	{
		$str = '';

		if (!$node instanceof Traversable) {
			return (string) $node;
		}

		foreach ($node as $n => $v) {
			if ($node instanceof Traversable && count($v) > 1) {
				$str .= str_repeat("\t", $level) . "<" . $n . "> \n" . $this->node2Text($v, $level + 1);
			} else {
				$str .= str_repeat("\t", $level) . "<" . $n . "> = " . $v . "\n";
			}
		}
		return $str;
	}

}