<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Import_Ods
{

	protected $file;
	public $error = false;
	public $ods;
	public $current_row = 0;

	public function __construct($file)
	{
		$this->file = $file;

		require_once(APPROOT . 'vendor/odsphp/ods.php');

		if (!$this->open($this->file)) {
			$this->error = true;
		}
	}

	public function open($file)
	{
		$this->ods = parseOds($this->file);

		if (!$this->ods || empty($this->ods->sheets)) {
			return false;
		}

		return true;
	}

	public function reset()
	{

	}

	public function findAllColumns()
	{
		$first_line = $this->ods->sheets[0]['rows'][0];

		$cols = array();

		if ($first_line) {
			foreach ($first_line as $i => $col) {
				if (trim($col['value'])) {
					$cols[$i] = trim($col['value']);
				}
			}
		}

		return $cols;
	}

	public function getColumnIndex($col)
	{
		$index = -1;

		foreach ($this->findAllColumns() as $i => $c) {
			$c = trim($c);

			if ($c == $col) {
				$index = $i;
			}
		}

		return $index;
	}

	public function countElement($el = null)
	{
		return count($this->ods->sheets[0]['rows']);
	}

	public function getElement()
	{
		$data = @$this->ods->sheets[0]['rows'][$this->current_row];

		$this->current_row++;

		if ($data && count($data) > 1) {

			foreach ($data as $i => $v) {
				$data[$i] = $v['value'];
			}

			return $data;
		}

		return false;
	}

	public function analyze()
	{
		if ($this->error) {
			return false;
		}

		// common tag names
		$common_cols = array(
			'name' => array('NAME', 'PRODUCT NAME', 'NAZEV', 'PRODUCT'),
			'ean13' => array('EAN', 'EAN13'),
			'sku' => array('SKU', 'CODE'),
			'price' => array('PRICE', 'PRICE_VAT', 'CENA'),
			'description' => array('DESCRIPTION', 'POPIS'),
			'files' => array('IMGURL', 'IMAGE', 'OBRAZEK'),
			'categories' => array('CATEGORYTEXT', 'CATEGORY'),
		);

		$common_uniques = array('name', 'sku', 'ean13');

		if ($elements = $this->findAllColumns()) {

			$col_matches = array();
			$variant_element = null;

			if (!empty($_SESSION['dataimport']['elements']) && is_array($_SESSION['dataimport']['elements'])) {
				foreach ($_SESSION['dataimport']['elements'] as $param => $el) {
					$col_matches[$param] = $el;
				}
			} else {
				foreach ($elements as $i => $col) {
					foreach ($common_cols as $tag => $common) {
						if (in_array(strtoupper(dirify($col)), $common)) {
							$col_matches[$tag] = $i;
							break;
						}
					}
				}
			}

			$unique = null;

			if (!empty($_SESSION['dataimport']['options']['product_unique'])) {
				$unique = $_SESSION['dataimport']['options']['product_unique'];
				$variant_element = $_SESSION['dataimport']['options']['variant_element'];
			} else {
				foreach ($common_uniques as $tag) {
					if ($col_matches[$tag]) {
						$unique = $tag;
						break;
					}
				}

				if (in_array('id', $elements) && in_array('price', $elements) && in_array('name', $elements)) {
					$unique = 'id';
				}
			}

			$sample = null;

			$this->reset();

			$this->getElement();

			while ($product = $this->getElement()) {
				if (!$sample) {
					$sample = $product;
					break;
				}
			}

			$this->reset();

			return array(
				'filesize' => round(filesize($this->file) / 1024 / 1024, 2),
				'elements' => $elements,
				'suggest' => $col_matches,
				'product_element' => null,
				'variant_element' => $variant_element,
				'product_count' => $this->countElement(),
				'unique' => $unique,
				'sample' => trim($this->cols2Text($elements, $sample))
			);
		}

		return false;
	}

	private function cols2Text($cols, $data)
	{
		$str = '';

		if (!is_array($data)) {
			return '';
		}

		foreach ($data as $i => $v) {
			$str .= "<" . $cols[$i] . "> = " . utf8_encode($v) . "\n";
		}

		return htmlspecialchars($str);
	}

}