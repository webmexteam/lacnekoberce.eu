<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Image
{

	protected $filename;
	protected $image;
	protected $actions = array();
	protected $tmp_image;
	protected $use_imagick = false;
	protected $imagick;

	public static function thumbs($file, $watermark = false)
	{
		if (file_exists($file)) {
			$real_path = str_replace('\\', '/', realpath($file));
			$pathinfo = pathinfo($real_path);
			$filename = $pathinfo['basename'];

			$sizes = Core::$def['image_sizes'];

			$img = new Image($file);

			unset($sizes[0], $sizes[-1]);

			usort($sizes, array('Image', 'sort_sizes'));

			foreach ($sizes as $_id => $size) {
				if (preg_match('/^(\d+)x(\d+)$/i', $size, $matches)) {
					$new_width = (int) $matches[1];
					$new_height = (int) $matches[2];

					$dir = $pathinfo['dirname'] . '/_' . $size;

					$img->resize($new_width, $new_height, $dir . '/' . $filename, ($watermark && ($new_width >= Core::$def['image_watermark']['min_size'] || $new_height >= Core::$def['image_watermark']['min_size'])));

					if (!is_writable($pathinfo['dirname'])) {
						FS::writable($pathinfo['dirname']);
					}

					if (!is_dir($dir)) {
						FS::mkdir($dir, true);
					}
				}
			}

			$img->process(null, true);
		}
	}

	public static function sort_sizes($a, $b)
	{
		$aparts = explode('x', $a);
		$bparts = explode('x', $b);

		return ($aparts[0] * $aparts[1]) < ($bparts[0] * $bparts[1]);
	}

	public function __construct($filename)
	{
		$this->filename = $filename;

		if ((Core::config('image_driver') == 'auto' || Core::config('image_driver') == 'imagick') && class_exists('Imagick')) {
			$this->use_imagick = true;

			$types = array(
				'JPEG' => IMAGETYPE_JPEG,
				'GIF' => IMAGETYPE_GIF,
				'PNG' => IMAGETYPE_PNG
			);

			try {
				$this->imagick = new Imagick();
			} catch (Exception $e) {
				throw new Exception('Image ' . $filename . ' not readable (using iMagick)');
			}

			$this->imagick->pingImage($this->filename);

			$image_info = $this->imagick->identifyImage();

			preg_match('/^(\w+)/', $image_info['format'], $m);

			$format = strtoupper($m[1]);

			$this->image = array(
				'file' => str_replace('\\', '/', realpath($filename)),
				'width' => $image_info['geometry']['width'],
				'height' => $image_info['geometry']['height'],
				'type' => $types[$format]
			);
		} else {
			$er = error_reporting(0);
			$image_info = getimagesize($this->filename);
			error_reporting($er);

			if (!is_array($image_info) OR count($image_info) < 3) {
				throw new Exception('Image ' . $filename . ' not readable (using GD)');
			}

			$this->image = array(
				'file' => str_replace('\\', '/', realpath($filename)),
				'width' => $image_info[0],
				'height' => $image_info[1],
				'type' => $image_info[2]
			);
		}

		return $this;
	}

	public function __get($key)
	{
		if (isSet($this->image[$key])) {
			return $this->image[$key];
		} else {
			throw new Exception('Undefined property ' . $key . '', $image);
		}
	}

	public function reset()
	{
		$this->action = array();

		return $this;
	}

	public function getImg()
	{
		$this->process(null);
		return $this->tmp_image;
	}

	public function resize($width, $height, $target = null, $watermark = false)
	{
		$this->actions[] = array(
			'action' => 'resize',
			'target' => $target,
			'watermark' => $watermark,
			'params' => array('width' => $width, 'height' => $height)
		);

		return $this;
	}

	public function crop($width, $height, $top = 0, $left = 0)
	{
		$this->actions[] = array(
			'action' => 'crop',
			'params' => array('width' => $width, 'height' => $height, 'top' => $top, 'left' => $left)
		);

		return $this;
	}

	public function rotate($degrees)
	{
		$this->actions[] = array(
			'action' => 'rotate',
			'params' => array('degrees' => $degrees)
		);

		return $this;
	}

	public function flip($dir)
	{
		$this->actions[] = array(
			'action' => 'flip',
			'params' => array('direction' => (strtolower($dir) == 'vertical' ? 'vertical' : 'horizontal'))
		);

		return $this;
	}

	public function watermark($config)
	{
		if (!Core::$is_premium) {
			return $this;
		}

		$this->actions[] = array(
			'action' => 'watermark',
			'params' => array(
				'type' => isSet($config['type']) ? $config['type'] : 'text',
				'filepath' => isSet($config['filepath']) ? $config['filepath'] : 'files/watermark.png',
				'text' => isSet($config['text']) ? $config['text'] : 'Copyright ' . date('Y'),
				'alignment' => isSet($config['alignment']) ? $config['alignment'] : 'tr', // tl, tr, bl, br, c
				'font_color' => isSet($config['font_color']) ? $config['font_color'] : array(255, 255, 255),
				'font_size' => isSet($config['font_size']) ? $config['font_size'] : 16,
				'opacity' => isSet($config['opacity']) ? $config['opacity'] : 50,
				'png24' => isSet($config['png24']) ? $config['png24'] : false,
				'text_shadow' => isSet($config['text_shadow']) ? $config['text_shadow'] : true,
			)
		);

		return $this;
	}

	public function save($newfilename = null)
	{
		$this->process($newfilename);

		return $this;
	}

	public function process($file, $dont_save = false)
	{
		if ($this->use_imagick) {
			return $this->process_imagick($file, $dont_save);
		}

		if ($file === null) {
			$file = $this->filename;
		}

		switch ($this->image['type']) {
			case IMAGETYPE_JPEG:
				$create = 'imagecreatefromjpeg';
				break;
			case IMAGETYPE_GIF:
				$create = 'imagecreatefromgif';
				break;
			case IMAGETYPE_PNG:
				$create = 'imagecreatefrompng';
				break;
		}

		$new_ext = strtolower(substr(strrchr($file, '.'), 1));

		switch ($new_ext) {
			case 'jpg':
			case 'jpeg':
				$save = 'imagejpeg';
				break;
			case 'gif':
				$save = 'imagegif';
				break;
			case 'png':
				$save = 'imagepng';
				break;
		}

		if (empty($create) || !function_exists($create)) {
			throw new Exception('File type ' . $this->image['type'] . ' not allowed');
		}

		if (empty($save) || !function_exists($save)) {
			throw new Exception('File type ' . $new_ext . ' not allowed');
		}

		$jpeg_compress = Core::config('jpeg_quality') ? Core::config('jpeg_quality') : 100;

		$this->tmp_image = @$create($this->image['file']);

		if (!$this->tmp_image) {
			$e = error_get_last();

			throw new Exception('Image error: ' . $e['message']);
		}

		imagealphablending($this->tmp_image, true);
		imagesavealpha($this->tmp_image, true);

		foreach ($this->actions as $action) {
			$this->tmp_image = @$create($this->image['file']);
			$func = 'exec_' . $action['action'];
			$params = $action['params'];
			$target = @$action['target'];
			$use_watermark = @$action['watermark'];

			if (!$this->$func($params)) {
				return false;
			}

			if ($use_watermark && $action['action'] != 'watermark') {

				$tmp = imagecreatetruecolor(imagesx($this->tmp_image), imagesy($this->tmp_image));

				imagecopy($tmp, $this->tmp_image, 0, 0, 0, 0, imagesx($this->tmp_image), imagesy($this->tmp_image));

				$params = array_merge(array(
					'type' => 'text',
					'filepath' => 'files/watermark.png',
					'text' => 'Copyright ' . date('Y'),
					'alignment' => 'tr', // tl, tr, bl, br, c
					'font_color' => array(255, 255, 255),
					'font_size' => 16,
					'opacity' => 50,
					'png24' => false,
					'text_shadow' => true,
						), Core::$def['image_watermark']);

				$this->exec_watermark($params, $tmp);

				if ($target) {
					$status = $save($tmp, $target, $save == 'imagejpeg' ? $jpeg_compress : 9);
				}

				unset($tmp);
			} else if ($target) {
				$status = $save($this->tmp_image, $target, $save == 'imagejpeg' ? $jpeg_compress : 9);
			}
		}

		if (!$dont_save) {
			$status = $save($this->tmp_image, $file, $save == 'imagejpeg' ? $jpeg_compress : 9);
		}

		return $status;
	}

	public function process_imagick($file, $dont_save = false)
	{
		if ($file === null) {
			$file = $this->filename;
		}

		$status = false;

		$this->imagick->readImage($this->image['file']);
		$this->imagick->setCompressionQuality(100);

		foreach ($this->actions as $action) {
			$params = $action['params'];
			$target = @$action['target'];
			$use_watermark = @$action['watermark'];

			if ($action['action'] == 'resize') {
				if (!$this->imagick->scaleImage($params['width'], $params['height'], true)) {
					return false;
				}
			} else if ($action['action'] == 'crop') {
				if (!$this->imagick->cropImage($params['width'], $params['height'], $params['left'], $params['top'])) {
					return false;
				}
			} else if ($action['action'] == 'rotate') {
				if (!$this->imagick->rotateImage(new ImagickPixel(), $params['degrees'])) {
					return false;
				}
			} else if ($action['action'] == 'flip' && $params['direction'] === 'horizontal') {
				if (!$this->imagick->flopImage()) {
					return false;
				}
			} else if ($action['action'] == 'flip' && $params['direction'] === 'vertical') {
				if (!$this->imagick->flipImage()) {
					return false;
				}
			} else if ($action['action'] == 'watermark') {
				$this->imagick_watermark($this->imagick, $params);
			}

			if ($use_watermark && $action['action'] != 'watermark') {

				$tmp = $this->imagick->getImage();

				$params = array_merge(array(
					'type' => 'text',
					'filepath' => 'files/watermark.png',
					'text' => 'Copyright ' . date('Y'),
					'alignment' => 'tr', // tl, tr, bl, br, c
					'font_color' => array(255, 255, 255),
					'font_size' => 16,
					'opacity' => 50,
					'png24' => false,
					'text_shadow' => true,
						), Core::$def['image_watermark']);

				$this->imagick_watermark($tmp, $params);

				if ($target) {
					$tmp->writeImage($target);
				}

				unset($tmp);
			} else if ($target) {
				$this->imagick->writeImage($target);
			}
		}

		if (!$dont_save && $this->imagick) {
			$this->imagick->writeImage($file);
		}

		return $status;
	}

	private function imagick_watermark(& $img, $params)
	{
		$size = $img->getImageGeometry();

		if ($params['type'] == 'text') {

			$params['font_size'] *= 1.5;

			$y_fix = $params['font_size'];

			$draw = new ImagickDraw();

			$draw->setFont(APPROOT . 'vendor/fonts/sans.ttf');
			$draw->setFontSize($params['font_size']);

			$color = '#' . join(array_map('dechex', $params['font_color']));

			$metrics = $img->queryFontMetrics($draw, $params['text']);

			switch ($params['alignment']) {
				case 'tl':
					$x = 5;
					$y = 5;
					break;
				case 'tr':
					$x = $size['width'] - $metrics['textWidth'] - 5;
					$y = 5;
					break;
				case 'bl':
					$x = 5;
					$y = $size['height'] - $metrics['textHeight'] - 5;
					break;
				case 'br':
					$x = $size['width'] - $metrics['textWidth'] - 5;
					$y = $size['height'] - $metrics['textHeight'] - 5;
					break;
				default:
					$x = ($size['width'] - $metrics['textWidth'] - 5) / 2;
					$y = ($size['height'] - $metrics['textHeight'] - 5) / 2;
					break;
			}

			$y += $y_fix;

			if ($params['text_shadow']) {
				$draw->setFillColor('#000000');
				$draw->setFillOpacity(0.2);

				$img->annotateImage($draw, $x + 1, $y + 1, 0, $params['text']);
			}

			$draw->setFillColor($color);

			if ($params['opacity']) {
				$draw->setFillOpacity($params['opacity'] / 100);
			}

			$img->annotateImage($draw, $x, $y, 0, $params['text']);
		} else {
			$watermark = new Imagick(DOCROOT . $params['filepath']);

			$watermark_size = $watermark->getImageGeometry();

			switch ($params['alignment']) {
				case 'tl':
					$x = 5;
					$y = 5;
					break;
				case 'tr':
					$x = $size['width'] - $watermark_size['width'] - 5;
					$y = 5;
					break;
				case 'bl':
					$x = 5;
					$y = $size['height'] - $watermark_size['height'] - 5;
					break;
				case 'br':
					$x = $size['width'] - $watermark_size['width'] - 5;
					$y = $size['height'] - $watermark_size['height'] - 5;
					break;
				default:
					$x = ($size['width'] - $watermark_size['width'] - 5) / 2;
					$y = ($size['height'] - $watermark_size['height'] - 5) / 2;
					break;
			}

			$img->compositeImage($watermark, $watermark->getImageCompose(), $x, $y);
		}

		return $img;
	}

	private function exec_resize($params)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		$master = (($width / $params['width']) > ($height / $params['height'])) ? 'width' : 'height';

		if ($master == 'width') {
			$params['height'] = round($height * $params['width'] / $width);
		}

		if ($master == 'height') {
			$params['width'] = round($width * $params['height'] / $height);
		}

		$img = imagecreatetruecolor($params['width'], $params['height']);

		imagealphablending($img, false);
		imagesavealpha($img, true);

		$transparent = imagecolorallocatealpha($img, 255, 255, 255, 127);

		imagefilledrectangle($img, 0, 0, $params['width'], $params['height'], $transparent);

		if ($status = imagecopyresampled($img, $this->tmp_image, 0, 0, 0, 0, $params['width'], $params['height'], $width, $height)) {
			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}

	private function exec_crop($params)
	{
		$this->sanitize_geometry($params);

		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		$img = imagecreatetruecolor($params['width'], $params['height']);

		if ($status = imagecopyresampled($img, $this->tmp_image, 0, 0, $params['left'], $params['top'], $width, $height, $width, $height)) {
			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}

	private function exec_rotate($params)
	{
		$img = $this->tmp_image;

		$transparent = imagecolorallocatealpha($img, 255, 255, 255, 127);

		$img = imagerotate($img, 360 - $params['degrees'], $transparent, -1);

		imagecolortransparent($img, $transparent);

		if ($status = imagecopymerge($this->tmp_image, $img, 0, 0, 0, 0, imagesx($this->tmp_image), imagesy($this->tmp_image), 100)) {
			imagealphablending($img, true);
			imagesavealpha($img, true);

			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}

	private function exec_flip($params)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		$img = imagecreatetruecolor($width, $height);

		if ($params['direction'] === 'horizontal') {
			for ($x = 0; $x < $width; $x++) {
				$status = imagecopy($img, $this->tmp_image, $x, 0, $width - $x - 1, 0, 1, $height);
			}
		} elseif ($params['direction'] === 'vertical') {
			for ($y = 0; $y < $height; $y++) {
				$status = imagecopy($img, $this->tmp_image, 0, $y, 0, $height - $y - 1, $width, 1);
			}
		} else {
			return true;
		}

		if ($status === true) {
			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}

	private function exec_watermark($params, & $tmp = null)
	{
		if ($tmp === null) {
			$tmp = $this->tmp_image;
		}

		$width = imagesx($tmp);
		$height = imagesy($tmp);

		if ($params['type'] == 'text') {
			$color = imagecolorresolvealpha($tmp, $params['font_color'][0], $params['font_color'][1], $params['font_color'][2], 127 - ($params['opacity'] * 1.27));
			$shadow_color = imagecolorresolvealpha($tmp, 0, 0, 0, 80);

			$font = APPROOT . 'vendor/fonts/sans.ttf';

			$dim = imagettfbbox($params['font_size'], 0, $font, $params['text']);

			$el_width = $dim[4];
			$el_height = $dim[1];
			$y_fix = $params['font_size'];
		} else {

			$wm = new Image(DOCROOT . $params['filepath']);
			$img = $wm->getImg();

			$el_width = $wm->width;
			$el_height = $wm->height;
			$y_fix = 0;
		}

		switch ($params['alignment']) {
			case 'tl':
				$x = 5;
				$y = 5 + $y_fix;
				break;
			case 'tr':
				$x = $width - $el_width - 5;
				$y = 5 + $y_fix;
				break;
			case 'bl':
				$x = 5;
				$y = $height - $el_height - 5;
				break;
			case 'br':
				$x = $width - $el_width - 5;
				$y = $height - $el_height - 5;
				break;
			default:
				$x = ($width - $el_width - 5) / 2;
				$y = ($height - $el_height - 5) / 2;
				break;
		}

		imagealphablending($tmp, true);

		if ($params['type'] == 'text') {
			if ($params['text_shadow']) {
				imagettftext($tmp, $params['font_size'], 0, $x + 1, $y + 1, $shadow_color, $font, $params['text']);
			}

			return imagettftext($tmp, $params['font_size'], 0, $x, $y, $color, $font, $params['text']);
		} else {

			if ($params['png24']) {
				// For PNG-24
				return imagecopy($tmp, $img, $x, $y, 0, 0, $wm->width, $wm->height);
			} else {
				return imagecopymerge($tmp, $img, $x, $y, 0, 0, $wm->width, $wm->height, $params['opacity']);
			}
		}
	}

	private function sanitize_geometry(&$geometry)
	{
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		$reporting = error_reporting(0);

		$geometry['width'] = min($geometry['width'], $width);
		$geometry['height'] = min($geometry['height'], $height);

		if ($geometry['top'] === 'center') {
			$geometry['top'] = floor(($height / 2) - ($geometry['height'] / 2));
		} elseif ($geometry['top'] === 'top') {
			$geometry['top'] = 0;
		} elseif ($geometry['top'] === 'bottom') {
			$geometry['top'] = $height - $geometry['height'];
		}

		if ($geometry['left'] === 'center') {
			$geometry['left'] = floor(($width / 2) - ($geometry['width'] / 2));
		} elseif ($geometry['left'] === 'left') {
			$geometry['left'] = 0;
		} elseif ($geometry['left'] === 'right') {
			$geometry['left'] = $width - $geometry['height'];
		}

		error_reporting($reporting);
	}

}