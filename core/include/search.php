<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Search
{

	protected static $tmp_words = array();

	public static function index($ajax_progress = false)
	{
		$offset = 0;
		$limit = 100;
		$i = 0;

		if ($ajax_progress && isSet($_SESSION['index_offset'])) {
			$offset = $_SESSION['index_offset'];
		}

		$total = Core::$db->product()->select('COUNT(id) as total')->fetch();
		$total = (int) $total['total'];

		while (($_products = Core::$db->product()->limit($limit, $offset)) && count($_products)) {
			foreach ($_products as $product) {
				if ($ajax_progress) {
					$progress = round(100 / $total * ($i + 1));
					echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) $progress, 'log' => trim(__('indexing_product') . ': ' . htmlspecialchars($product['name'])))) . '\');</script>';
					flush();
				}

				self::indexProduct($product);

				$i++;
			}

			$offset += $limit;

			$_SESSION['index_offset'] = $offset;
		}

		unset($_SESSION['index_offset']);

		if ($ajax_progress) {
			echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) $progress, 'log' => trim(__('reindex_finished', $total)))) . '\');</script>';
			flush();
		}
	}

	public static function indexProduct($product)
	{
		$sql = '';

		$name_words = self::getWords($product['name'] . ' ' . $product['nameext']);
		$description_short_words = self::getWords($product['description_short']);
		$description_words = self::getWords($product['description']);
		$page_words = array();

		if (is_object($product)) {
			foreach ($product->product_pages() as $page) {
				$page_words = array_merge($page_words, self::getWords($page->page['name']));
			}
		}

		$word_weight = self::calcWeight(array(
					array(10, $name_words),
					array(7, $description_short_words),
					array(4, $description_words),
					array(4, $page_words),
				));

		$word_ids = array();
		if (is_object($product)) {
			foreach ($product->search_index() as $index) {
				$word_ids[$index['search_word_id']] = $index['weight'];
			}
		}

		$new_word_ids = array();

		foreach ($word_weight as $word => $weight) {
			$insert = false;

			if (preg_match('/^__\d+$/', $word)) {
				$word = substr($word, 2);
			}

			$word = trim($word);

			if (!$word) {
				continue;
			}

			$word_id = null;

			if (isSet(self::$tmp_words[$word])) {
				$word_id = self::$tmp_words[$word];
			} else {
				if ($search_word = Core::$db->search_word()->where('word', $word)->limit(1)->fetch()) {
					$word_id = $search_word['id'];
					self::$tmp_words[$word] = $word_id;
				} else {
					try {
						$word_id = Core::$db->search_word(array('word' => $word));
					} catch (PDOException $e) {
						continue;
					}

					self::$tmp_words[$word] = $word_id;
					$insert = true;
				}
			}

			$new_word_ids[] = $word_id;

			if (!isSet($word_ids[$word_id])) {
				Core::$db->search_index(array(
					'product_id' => $product['id'],
					'search_word_id' => $word_id,
					'weight' => $weight
				));
			} else if ($word_ids[$word_id] != $weight) {
				Core::$db->search_index()->where('product_id', $product['id'])->where('search_word_id', $word_id)->update(array('weight' => $weight));
			}
		}

		foreach ($word_ids as $wid => $w) {
			if (!in_array($wid, $new_word_ids)) {
				Core::$db->search_index()->where('product_id', $product['id'])->where('search_word_id', $wid)->delete();
			}
		}
	}

	private function calcWeight(array $maps)
	{
		$word_weight = array();

		foreach ($maps as $map) {
			$max_weight = $map[0];
			$words = $map[1];
			$coef = 1 * (count($words) > 15 ? 1.2 : 1);

			foreach ($words as $i => $word) {
				$weight = $max_weight * $coef;

				if (preg_match('/\d/', $word) && !is_numeric($word)) {
					// maybe specific name
					$weight += 1;
				}

				if (is_numeric($word)) {
					$word = '__' . $word;
					$weight -= 2;
				}

				$weight = round($weight, 1);

				if (isSet($word_weight[$word])) {
					$word_weight[$word] = max(($word_weight[$word] < $weight ? $weight : $word_weight[$word]) + 1, $max_weight);
				} else {
					$word_weight[$word] = $weight;
				}

				$coef = max(0.1, $coef - ($i / 50));
			}
		}

		arsort($word_weight);

		return array_splice($word_weight, 0, 25);
	}

	private function getWords($str)
	{
		$words = array();
		$parts = preg_split('/[\s\,\.\-\/]+/', strip_tags($str));

		foreach ($parts as $part) {
			if ($part = self::removeSpecialChars($part)) {
				$len = strlen(utf8_decode($part));

				if ($len >= 2 && !in_array($part, Core::$locale['stop_words'])) {
					$words[] = $len >= 4 ? Search::findRadix($part) : $part;
				}
			}
		}

		return $words;
	}

	private function removeSpecialChars($str)
	{
		return dirify(trim(preg_replace('/[\!\?\#\$\^\*\(\)\[\]\|]/', '', $str), ' :;.,/'));
	}

	public static function findRadix($str)
	{
		$fn = Core::$locale['word_radix_fn'];

		if (function_exists($fn)) {
			return $fn($str);
		}

		return $str;
	}

	public static function findProducts($query, $order_by = 'default', $order_dir = 'ASC')
	{
		$words = self::getWords($query);
		$wcount = count(array_unique($words));

		$sql = '';

		foreach ($words as $word) {
			$sql .= 'word = ' . Core::$db_conn->quote($word) . ' OR ';
		}

		$sql = preg_replace('/\sOR\s$/', '', $sql);

		$word_ids = array();

		if ($sql) {
			foreach (Core::$db->search_word()->select('id')->where($sql) as $word) {
				$word_ids[] = $word['id'];
			}
		}

		if (empty($word_ids) || count($word_ids) <= 3) {
			$sql = '';

			foreach ($words as $word) {
				$sql .= 'word LIKE ' . Core::$db_conn->quote($word.'%').' OR ';
			}

			$sql = preg_replace('/\sOR\s$/', '', $sql);

			if ($sql) {
				foreach (Core::$db->search_word()->select('id')->where($sql) as $word) {
					$word_ids[] = $word['id'];
				}
			}
		}

		$product_ids = array();
		$matches = array();

		if ($word_ids) {
			foreach (Core::$db->search_index()->where('search_word_id', $word_ids) as $product_id) {
				$product_ids[$product_id['product_id']] += $product_id['weight'];
				$matches[$product_id['product_id']]++;
			}
		}

		$strict_product_ids = $product_ids;

		foreach ($matches as $pid => $count) {
			if ($count < $wcount) {
				unset($strict_product_ids[$pid]);
			}
		}

		// strict search
		$product_ids = $strict_product_ids;

		// non-strict search
		/*
		  if(count($strict_product_ids) >= 2){
		  $product_ids = $strict_product_ids;
		  } */

		arsort($product_ids);

		$product_ids = array_keys($product_ids);

		if (Core::$db_inst->_type == 'sqlite') {
			Core::$db_conn->sqliteCreateFunction('field', array('Search', 'sqlite_field'));

			if (count($product_ids) > 115) {
				$product_ids = array_slice($product_ids, 0, 115);
			}
		}

		$where = '';
		$products = Core::$db->product();

		if (!Core::$is_admin) {
			$products->where('status', 1);
		}

		if (preg_match('/^[\w\-\/\.]+$/', $query)) {
			$where .= '(sku LIKE "' . $query . '" OR productno LIKE ' . Core::$db_conn->quote($query) . ') OR ';
		}

		if ($wcount == 1) {
			if (is_numeric($words[0]) && substr($words[0], 0, 1) != '0' && strlen($words[0]) < 6) {
				// id
				$product_ids[] = (int) $words[0];
			} else if (is_numeric($words[0]) && strlen($words[0]) >= 10) {
				// ean
				$where .= 'ean13 = "' . $words[0] . '" OR ';
			}
		}

		if ($product_ids) {
			$where .= 'id IN (' . join(', ', $product_ids) . ')';
		}

		if (!$where && $query) {
			$where .= '(sku LIKE "' . Core::$db_conn->quote($query) . '" OR productno LIKE ' . Core::$db_conn->quote($query) . ') OR ';
		}

		$where = preg_replace('/\sOR\s$/', '', $where);

		if ($where) {
			$products->where($where);
		} else {
			return false;
		}

		if ($order_by == 'default' && $product_ids) {
			$products->order('field(id, ' . join(', ', $product_ids) . ') ' . $order_dir);
		} else if ($order_by != 'default') {
			$products->order($order_by . ' ' . $order_dir);
		}

		return $products;
	}

	public static function sqlite_field($field)
	{
		$values = func_get_args();
		$values = array_slice($values, 1);

		return array_search($field, $values);
	}

}
