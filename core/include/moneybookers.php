<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Moneybookers
{

	public static $endpoint = 'https://www.moneybookers.com/app/payment.pl';
	public static $currency;
	public static $language;
	public static $pay_to_email;
	public static $countryCode;

	public static function setup()
	{
		if (!self::$pay_to_email) {
			if (!function_exists('curl_init')) {
				throw new Exception('CURL extension is required');
			}

			require_once(DOCROOT . 'etc/moneybookers.php');

			self::$pay_to_email = $moneybookers['pay_to_email'];
			self::$currency = $moneybookers['currency_code'];
			self::$language = $moneybookers['language_code'];
			self::$countryCode = $moneybookers['country_code'];

			if (empty(self::$pay_to_email)) {
				throw new Exception('Wrong MoneyBookers configuration');
			}
		}
	}

	private static function request($nvp_str)
	{
		$postdata = http_build_query(array(
					'pay_to_email' => self::$pay_to_email,
					'language' => self::$language,
					'currency' => self::$currency
				)) . '&' . $nvp_str;

		$ch = curl_init(self::$endpoint);
		curl_setopt_array($ch, array(
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_SSL_VERIFYHOST => FALSE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_POST => TRUE,
			CURLOPT_HEADER => TRUE
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

		$response = curl_exec($ch);

		if ($err = curl_errno($ch)) {
			echo $err;
		} else {
			curl_close($ch);
		}

		return $response;
	}

	public static function pay($order)
	{
		$sid = null;

		$nvp_str = http_build_query(array(
			'prepare_only' => 1,
			'amount' => round((float) $order['total_incl_vat'], 2),
			'transaction_id' => self::getUID($order),
			'status_url' => url('moneybookers/callback', null, true),
			'return_url' => url('moneybookers/success', null, true),
			'cancel_url' => url('moneybookers/fail', null, true),
			'pay_from_email' => $order['email'],
			'firstname' => $order['first_name'],
			'lastname' => $order['last_name'],
			'address' => $order['street'],
			'postal_code' => $order['zip'],
			'city' => $order['city'],
			'country' => self::$countryCode,
			'detail1_description' => __('order') . ':',
			'detail1_text' => $order['id']
				));

		if ($response = self::request($nvp_str)) {
			if (preg_match('/Set-Cookie: SESSION_ID=([\d\w]+);/miu', $response, $m) && $m[1]) {
				$sid = $m[1];

				$_SESSION['moneybookers'] = array(
					'order_id' => $order['id'],
					'sid' => $sid
				);

				redirect(self::$endpoint . '?sid=' . $sid);
			}
		}

		return false;
	}

	public static function callback()
	{
		if (!preg_match('/Moneybookers Merchant Payment Agent/i', $_SERVER['HTTP_USER_AGENT'])) {
			return false;
		}

		if (empty($_POST) || empty($_POST['transaction_id'])) {
			return false;
		}

		if (empty($_POST['transaction_id']) || !($parts = explode('X', $_POST['transaction_id'], 1)) || !($order = Core::$db->order[(int) $parts[0]])) {
			return false;
		}

		if ($_POST['status'] == 2) {
			if ($_POST['transaction_id'] != self::getUID($order)) {
				// invalid transaction_id
				return -10;
			}

			if ($_POST['pay_to_email'] != self::$pay_to_email) {
				// invalid pay_to_email
				return -11;
			}

			if ($_POST['mb_amount'] != round((float) $order['total_incl_vat'], 2)) {
				// invalid amount
				return -12;
			}

			if ($_POST['mb_currency'] != self::$currency) {
				// invalid currency
				return -13;
			}

			$order->update(array(
				'payment_session' => $_POST['mb_transaction_id'],
				'payment_realized' => time()
			));

			Notify::payment('MoneyBookers.com', $order);

			return true;
		} else {
			return (int) $_POST['status'];
		}
	}

	private static function getUID($order)
	{
		return $order['id'] . 'X' . strtoupper(substr(md5($order['id'] . $order['received'] . self::$pay_to_email), 2, 8));
	}

}