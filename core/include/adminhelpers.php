<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
function flashMsg($msg, $type = 'ok')
{
	$_SESSION['flash_msg'] = array(
		'type' => $type,
		'msg' => is_object($msg) ? $msg->__toString() : $msg
	);
}

function forminput($type, $name, $value = '', $opt = array())
{
	$value = htmlspecialchars($value);

	if ($type == 'text') {
		$el = '<input type="text" name="' . $name . '" value="' . $value . '" id="inp-' . $name . '" class="input-text' . (isSet($opt['cls']) ? ' ' . $opt['cls'] : '') . '" />';
	} else if ($type == 'file') {
		$el = '<input type="file" name="' . $name . '" value="' . $value . '" id="inp-' . $name . '" class="input-file' . (isSet($opt['cls']) ? ' ' . $opt['cls'] : '') . '" />';
	} else if ($type == 'password') {
		$el = '<input type="password" name="' . $name . '" value="' . $value . '" id="inp-' . $name . '" class="input-text' . (isSet($opt['cls']) ? ' ' . $opt['cls'] : '') . '" autocomplete="' . (isSet($opt['autocomplete']) ? $opt['autocomplete'] : 'off') . '" />';
	} else if ($type == 'textarea') {
		$el = '<textarea name="' . $name . '" id="inp-' . $name . '" class="input-textarea' . (isSet($opt['cls']) ? ' ' . $opt['cls'] : '') . '" rows="' . (isSet($opt['rows']) ? $opt['rows'] : '5') . '">' . $value . '</textarea>';
	} else if ($type == 'checkbox') {
		$el = '<input type="checkbox" name="' . $name . '" value="1" id="inp-' . $name . '" class="input-checkbox' . (isSet($opt['cls']) ? ' ' . $opt['cls'] : '') . '"' . ($value ? ' checked="checked"' : '') . ' />';
	} else if ($type == 'select') {

		if (is_array($opt['options'])) {
			$options = '';

			foreach ($opt['options'] as $v => $n) {
				$options .= '<option value="' . $v . '"' . (($v == $value || (is_array($value) && in_array($v, $value))) ? ' selected="selected"' : '') . '>' . $n . '</option>';
			}
		} else {
			$options = $opt['options'];
		}

		$el = '<select name="' . $name . '"' . (!empty($opt['multiple']) ? ' multiple="multiple" size="' . $opt['multiple'] . '"' : '') . ' id="inp-' . $name . '" class="input-select' . (isSet($opt['cls']) ? ' ' . $opt['cls'] : '') . '">' . $options . '</select>';

		if (!empty($opt['multiple'])) {
			$el .= '<span class="inputhelp">' . __('multiple_select_help') . '</span>';
		}
	}

	return '<div class="inputwrap clearfix'
			. (!empty($opt['required']) ? ' required' : '')
			. (!empty($opt['premium']) ? ' premium' : '')
			. '"><label for="inp-' . $name . '">' . (!empty($opt['label']) ? $opt['label'] : __($name)) . ':</label>' . $el . '</div>';
}

function gridpagination($count, $page = null)
{
	$limit = pgLimit();
	$params = (array) $_GET;

	if ($page === null) {
		$page = Core::$controller->pagination_page;
	}

	$out = '<ul class="pagination"><li>' . __('pagination_records', $count) . '</li>';

	if ($page > 1) {
		$out .= '<li class="split"></li><li><a href="' . url(true, array_merge($params, array('page' => $page - 1))) . '">&laquo; ' . __('pagination_prev') . '</a></li>';
	} else if ($page * $limit < $count) {
		$out .= '<li class="split"></li>';
	}

	if ($page * $limit < $count) {
		$out .= '<li><a href="' . url(true, array_merge($params, array('page' => $page + 1))) . '">' . __('pagination_next') . ' &raquo;</a></li>';
	}

	$out .= '<li class="split"></li>';
	$out .= '<li>' . __('pages') . ': </li>';

	$total = ceil($count / $limit);

	for ($i = 1; $i <= $total; $i++) {
		if (abs($i - $page) > 5 && $i != $total && $i != 1) {
			if ($i == 2) {
				$out .= '<li>...</li>';
			}
			if ($i == $total - 1) {
				$out .= '<li>...</li>';
			}
			continue;
		}

		$out .= '<li' . ($page == $i ? ' class="active"' : '') . '><a href="' . url(true, array_merge($params, array('page' => $i))) . '">' . $i . '</a></li>';
	}

	$out .= '</ul>';

	return $out;
}

function validate($rules)
{
	$errors = array();

	foreach ($rules as $k => $v) {
		if (is_numeric($k)) {
			if (empty($_POST[$v])) {
				$errors[$v] = 'required';
			} else if ($v == 'email' && !valid_email($_POST[$v])) {
				$errors[$v] = 'invalid';
			}
		}
	}

	return empty($errors) ? true : $errors;
}

function gatekeeper($permission = null)
{
	$ok = false;
	$parent = null;

	if (User::$logged_in) {
		if ($permission) {
			if (!is_numeric($permission)) {
				$pid = array_search($permission, Core::$def['permissions']);

				if (substr($pid, -2) != '00') {
					$parent = (int) substr($pid, 0, 2) * 100;
				}
			}

			if (User::has($permission) || ($parent && User::has($parent))) {
				$ok = true;
			}
		} else {
			$ok = true;
		}
	}

	if (!$ok) {
		flashMsg(__('msg_access_denied'), 'error');
		redirect('admin');
	}
}

function sortDir(& $order_by, & $order_dir)
{
	$cname = 'webmex_admin_sort_' . md5(Core::$orig_uri);

	if (!empty($_COOKIE[$cname]) && ($parts = explode('|', $_COOKIE[$cname])) && preg_match('/^asc|desc$/i', $parts[1])) {
		$order_by = $parts[0];
		$order_dir = $parts[1];
	}

	if (!empty($_GET['sort'])) {
		$order_by = $_GET['sort'];
	}

	if (!empty($_GET['dir'])) {
		$order_dir = $_GET['dir'];
	}

	setcookie($cname, $order_by . '|' . $order_dir, strtotime('+7 days'), url('admin'));
}