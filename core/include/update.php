<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Update
{

	protected static $base = 'http://www.webmex.cz/';
	protected static $log = '';
	protected static $ajax = false;
	protected static $progress = false;
	public static $wrapper;
	public static $version;

	public static function getVersions($old_version, $new_version, $precision = null)
	{
		$stages = array('beta', 'rc1', 'rc2', 'rc3', 'rc4');

		if (!$old_version || !$new_version) {
			return false;
		}

		if (version_compare($new_version, $old_version) < 1) {
			return false;
		}

		$old_parts = preg_split('/\.|\-|\_/', $old_version);
		$new_parts = preg_split('/\.|\-|\_/', $new_version);

		$v1 = $v2 = '';
		$stages_count = count($stages);

		// update to 2.0.0 hack
		$versions = array();
		if($new_version == '2.0.0') {
			for ($i=0; $i < 99; $i++) { 
				$versions[] = '1.0.'.$i;
			}
			return $versions;
		}

		if ($precision === null) {
			$precision = max(count($old_parts), count($new_parts));

			if (!preg_match('/(' . join('|', $stages) . ')$/i', $new_version) && !preg_match('/(' . join('|', $stages) . ')$/i', $old_version)) {
				$precision += 1;
			}
		}

		for ($i = 0; $i < $precision; $i++) {
			$pos1 = $old_parts[$i];
			$pos2 = $new_parts[$i];

			if ($pos1 !== null && !is_numeric($pos1)) {
				$pos1 = (int) array_search($pos1, $stages);
			} else {
				$pos1 = (int) $pos1 + $stages_count;
			}

			$v1 .= ($pos1 < 10 ? '0' : '') . $pos1;

			if ($pos2 !== null && !is_numeric($pos2)) {
				$pos2 = (int) array_search($pos2, $stages);
			} else {
				$pos2 = (int) $pos2 + $stages_count;
			}

			$v2 .= ($pos2 < 10 ? '0' : '') . $pos2;

			
		}

		$v1 = (int) $v1;
		$v2 = (int) $v2;

		$versions = array();
		for ($i = ($v1 + 1); $i <= $v2; $i++) {
			$v = $v2 - ($v2 - $i);

			$version = array();
			foreach (self::str_rsplit($v, -2) as $p) {
				$p = (int) $p - $stages_count;
				$version[] = $p < 0 ? $stages[($p + $stages_count)] : $p;
			}

			$l = current(array_slice($version, -1));

			$_version = join('.', array_slice($version, 0, $precision - 1)) . (!is_numeric($l) ? '-' . $l : '');
			if (version_compare($_version, $old_version) == 1 && !in_array($_version, $versions)) {
				$versions[] = $_version;
			}
		}

		return $versions;
	}

	public static function check($beta = null, $install = null)
	{
		$modules = array();
		unset($_SESSION['webmex_update']);

		if ($beta === null) {
			$beta = Registry::get('update_beta') ? 1 : 0;
		}

		foreach (Core::$modules as $module_name => $module) {
			if (is_object($module) && $module->auto_update) {
				$v = $module->getInfo('version');

				$modules[$module_name] = $v;
			}
		}

		$data = array(
			'b' => $beta ? 1 : 0,
			'v' => Core::version,
			'i' => Core::config('instid'),
			'm' => $modules,
			'l' => Core::$language,
			'install' => $install
		);

		if ($json = @file_get_contents(self::$base . 'update/check_v3?' . http_build_query($data))) {

			if ($update = json_decode($json, true)) {

				if (is_array($update) && (bool) $update['status']) {

					if ($update['items'] && $update['items']['system'] && version_compare($update['items']['system']['version'], Core::version) < 1) {
						return -2;
					}

					$sps_installed = Registry::get('servicepacks_installed');

					if (!$sps_installed) {
						$sps_installed = array();
					}

					$sps_installed = array_merge($sps_installed, Core::$servicepacks);

					foreach ($update['items'] as $item_name => $data) {
						$is_service_pack = (bool) preg_match('/^SP_[\d\-\.]+$/', $item_name);

						if ($is_service_pack && ($spname = strtoupper(preg_replace('/[\-\.]/', '', $item_name))) && in_array($spname, $sps_installed)) {
							unset($update['items'][$item_name]);
						}
					}

					if ($update['items']) {
						$_SESSION['webmex_update'] = $update;

						$has_update = false;

						foreach ($update['items'] as $item_name => $opts) {
							if (empty($opts['install']) || !$opts['install']) {
								$has_update = true;
							}
						}

						$notification = Notify::find('type', 'update');

						if (!$has_update && $notification) {
							$notification->remove();
						} else if ($has_update && !$notification) {
							Notify::push(__('update_available'), 'update', url('admin/update'));
						}
					}

					return $update;
				}
			}
		} else {
			return -1;
		}

		if ($json === false) {
			return null;
		}

		return false;
	}

	public static function run($update_items)
	{
		self::$ajax = true;

		if (empty($update_items)) {
			return;
		}

		$update = array();
		foreach ($update_items as $item_name => $item_version) {
			$is_service_pack = (bool) preg_match('/^SP_[\d\-\.]+$/', $item_name);

			if ($item_name == 'system') {
				$update['system'] = array(Core::version, $item_version);
			} else if ($is_service_pack) {
				$update[$item_name] = array(1, 1);
			} else if ($module = Core::module($item_name)) {
				$v = $module->getInfo('version');

				$update[$item_name] = array($v, $item_version);
			} else if (!$module) {
				$update[$item_name] = array(0, $item_version);
			}
		}

		$data = array(
			'i' => Core::config('instid'),
			'u' => $update
		);

		try {
			self::log(__('update_start'));

			register_shutdown_function(array('Update', 'shutdown_handler'));

			if ($notification = Notify::find('type', 'update')) {
				$notification->remove();
			}

			$_SESSION['update_maintenance'] = Core::config('maintenance');

			if ($json = file_get_contents(self::$base . 'update/update_v3?' . http_build_query($data))) {
				$update_data = json_decode($json, true);
			} else {
				self::log(__('update_error', 'Remote repository server is not responding.'));
				return self::$log;
			}

			if (!isSet($update_data) || !is_array($update_data) || !is_array($update_data['items'])) {
				self::log(__('update_error', 'Invalid update data. Please try again later.'));
				return self::$log;
			}

			if ($update_data['items'] && $update_data['items']['system'] && version_compare($update_data['items']['system']['version'], Core::version) < 1) {
				self::log(__('update_error', 'Invalid Core version. Please try again later.'));
				return self::$log;
			}

			Core::$db->config()->where('name', 'maintenance')->update(array('value' => 1));

			foreach ($update_data['items'] as $item_name => $item_data) {
				$is_service_pack = (bool) preg_match('/^SP_[\d\-\.]+$/', $item_name);

				if (empty($item_data['files'])) {
					continue;
				}

				if ($item_name == 'system') {
					self::log(__('update_system', $item_data['version']));
				} else if ($is_service_pack) {
					self::log(__('update_service_pack', $item_name));
				} else {
					self::log(__('update_module', $item_name, $item_data['version']));
				}

				self::log(__('update_copy_files', count($item_data['files'])));

				self::copyFiles($item_name, $item_data['version'], $item_data['files']);
			}

			self::log(__('update_clear_cache'));
			self::clearCache();

			self::log(__('update_finished'));
		} catch (Exception $e) {
			self::log('ERROR: ' . $e->getMessage());
		}

		return self::$log;
	}

	protected static function copyFiles($module_name, $version, $files)
	{
		FS::$wrapper = self::$wrapper;
		$is_service_pack = (bool) preg_match('/^SP_[\d\-\.]+$/', $module_name);

		$total = count($files);

		$perm = fileperms(DOCROOT . 'index.php') & 511;
		$perm_dir = fileperms(DOCROOT . 'core') & 511;

		foreach ($files as $i => $file) {
			$hash = null;

			if (is_array($file)) {
				$hash = $file[1];
				$file = ltrim($file[0], '/');
			}

			if ($module_name == 'system') {
				$dir = 'repository/' . $version . '/';
				$target = '';
			} else if ($is_service_pack) {
				$spname = strtoupper(preg_replace('/[\-\.]/i', '', $module_name));

				$dir = 'repository/servicepacks/' . substr($module_name, 3) . '/';
				$target = '';
			} else {
				$dir = 'repository/modules/' . $module_name . '/' . $version . '/';
				$target = 'etc/modules/' . $module_name . '/';
			}

			$url = self::$base . $dir . $file;
      
  		if (!is_dir(DOCROOT . $target)) {
  			FS::mkdir(DOCROOT . $target, $perm_dir);
  		}      

			if ($content = @file_get_contents($url)) {
				$fileinfo = pathinfo($file);

				if ($hash && md5($content) != $hash) {
					// try again
					sleep(1);
					if ($content = @file_get_contents($url)) {
						if ($hash && md5($content) != $hash) {
							self::log("\t" . 'ERROR: Repository file "' . $file . '" is corrupted (invalid checksum)');
							continue;
						}
					}
				}

				if (!is_dir(DOCROOT . $target . $fileinfo['dirname'])) {
					FS::mkdir(DOCROOT . $target . $fileinfo['dirname'], $perm_dir);
				}

				if (file_put_contents((self::$wrapper ? self::$wrapper . $target : DOCROOT . $target) . $file, $content, 0, stream_context_create(array('ftp' => array('overwrite' => true))))) {
					self::log(__('update_copy_file', $file));
				} else {
					self::log(__('update_copy_file_error', $file));
				}
			} else {
				self::log(__('update_copy_file_error', $file));
			}

			self::$progress = round(100 / $total * ($i + 1));
		}
	}

	protected static function clearCache()
	{
		FS::cleanCache();
	}

	protected static function log($msg)
	{
		$msg = $msg . "\n";

		if (self::$ajax) {
			echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) self::$progress, 'log' => trim($msg))) . '\');</script>';
			flush();
		}

		self::$log .= date('H:i:s') . "\t" . $msg;
	}

	protected static function writeLog()
	{
		if (!is_dir(DOCROOT . 'etc/log')) {
			FS::mkdir(DOCROOT . 'etc/log', true);
		}

		@file_put_contents(DOCROOT . 'etc/log/update_' . date('Y-m-d') . '.txt', self::$log . "\n\n", FILE_APPEND);
	}

	public static function shutdown_handler()
	{
		self::writeLog();

		Core::$db->config()->where('name', 'maintenance')->update(array('value' => (isSet($_SESSION['update_maintenance']) ? (int) $_SESSION['update_maintenance'] : 0)));
	}

	public static function str_rsplit($str, $sz)
	{
		// splits a string "starting" at the end, so any left over (small chunk) is at the beginning of the array.
		if (!$sz) {
			return false;
		}
		if ($sz > 0) {
			return str_split($str, $sz);
		} // normal split

		$l = strlen($str);
		$sz = min(-$sz, $l);
		$mod = $l % $sz;

		if (!$mod) {
			return str_split($str, $sz);
		} // even/max-length split
		// split
		return array_merge(array(substr($str, 0, $mod)), str_split(substr($str, $mod), $sz));
	}

}