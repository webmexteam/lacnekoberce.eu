<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class User
{

	const has_or = -1;

	public static $logged_in = false;
	public static $data;
	protected static $permission;

	public static function setup()
	{
		if (!empty($_SESSION['webmex_user']) && is_array($_SESSION['webmex_user'])) {
			if ($_SESSION['webmex_user']['t'] >= strtotime('-45 minutes')) {
				self::$data = $_SESSION['webmex_user'];

				if (empty($_POST['session_disable_extend'])) {
					$_SESSION['webmex_user']['t'] = time();
				}

				self::$logged_in = true;
			} else {
				$_SESSION['flash_login_error'] = 'msg_timeout_logout';
				unset($_SESSION['webmex_user']);
			}
		} else {
			unset($_SESSION['webmex_user']);
		}
	}

	public static function login($username, $password)
	{
		$password = trim($password);

		$user = Core::$db->user('username = ?', $username)->fetch();

		if (!$user) {
			return 'user_not_found';
		}

		if ((int) $user['enable_login'] && $user['password'] == sha1(sha1($password))) {
			foreach ((array) $_SESSION as $k => $v) {
				unset($_SESSION[$k]);
			}

			$_SESSION['webmex_user'] = array(
				't' => time(),
				'login_time' => time(),
				'user' => $user->as_array()
			);

			$user->update(array(
				'last_login' => time()
			));

			return true;
		} else {
			return 'invalid_password';
		}
	}

	public static function logout()
	{
		unset($_SESSION['webmex_user']);
	}

	public static function get($name)
	{
		return self::$data['user'][$name];
	}

	public static function has($permission)
	{
		if (func_num_args() > 1) {
			$or = false;
			$args = func_get_args();
			foreach ($args as $arg) {
				if ($arg == self::has_or) {
					$or = true;
				} else if ($or && self::has($arg)) {
					return true;
				} else if (!$or && !self::has($arg)) {
					return false;
				}
			}

			return !$or;
		}

		if (self::$permission === null) {
			self::$permission = array();

			foreach (Core::$db->user_permissions()->where('user_id', (int) self::get('id')) as $perm) {
				self::$permission[$perm['permission_id']] = Core::$def['permissions'][$perm['permission_id']];
			}
		}

		if ((int) self::get('id') == 1 || isSet(self::$permission[10000])) { // admin (id 1) has all permissions
			return true;
		}

		if (is_numeric($permission) && isSet(self::$permission[$permission])) {
			return true;
		} else if (in_array($permission, self::$permission)) {
			return true;
		}

		return false;
	}

}