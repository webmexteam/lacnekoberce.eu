<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

class Updatescript
{

	protected $current;
	protected $latest;

	public function exec($forcefrom = null)
	{
		$this->current = Core::config('version');
		if (!is_null($forcefrom)) $this->current = $forcefrom;
		$this->latest = Core::version;

		$versions = Update::getVersions($this->current, $this->latest);

		if (!$versions) {
			return false;
		}

		foreach ($versions as $version) {
			if (is_array($version)) {
				$func = 'version_' . join('_', $version);

				if (!method_exists($this, $func) && array_pop($version) == 0) {
					$func = 'version_' . join('_', $version);
				}

			} else {
				$func = 'version_' . preg_replace('/[\.\-]/', '_', $version);
			}

			if (method_exists($this, $func)) {
				// TODO: log version update
				self::log('Upgrade databaze - ' . $func);
				call_user_func(array($this, $func));
			}

		}


		FS::cleanCache();

		Core::$db->config()->where('name', 'version')->update(array('value' => $this->latest));
		Core::$db->config()->where('name', 'dbversion')->update(array('value' => $this->latest));

	}

	private static $log = "";


	protected static function log($msg)
	{
		$msg = $msg . "\n";

		self::$log .= date('H:i:s') . "\t" . $msg;
	}

	protected static function writeLog()
	{
		if (!empty(self::$log)) {
			if (!is_dir(DOCROOT . 'etc/log')) {
				FS::mkdir(DOCROOT . 'etc/log', true);
			}
			@file_put_contents(DOCROOT . 'etc/log/updatedb_' . date('Y-m-d') . '.txt', self::$log . "\n\n", FILE_APPEND);
			self::$log = "";
		}

	}

	public static function shutdown_handler()
	{
		self::writeLog();
		FS::cleanCache();
		Core::$db->config()->where('name', 'maintenance')->update(array('value' => (isSet($_SESSION['update_maintenance']) ? (int)$_SESSION['update_maintenance'] : 0)));
	}

	public function exec_individually($force = null)
	{
		register_shutdown_function(array('Updatescript', 'shutdown_handler'));

		$_SESSION['update_maintenance'] = Core::config('maintenance');

		Core::$db->config()->where('name', 'maintenance')->update(array('value' => 1));

		return $this->exec($force);

	}

	private function version_1_0_21()
	{

		if (Core::$language == "cs") {
			if (!Core::$db->email_message[13]) {
				$sql = 'INSERT INTO `email_message` VALUES ("13","Vaše objednávka {id} je potvrzena","<p>Dobrý den,<br />
						Vaše objednávka {id} byla potvrzena obchodníkem.</p>","order_status_confirmed", "order");';
				Core::$db_conn->exec($sql);
			}

			if (!Core::$db->email_message[14]) {
				$sql = 'INSERT INTO `email_message` VALUES ("14","Vaše objednávka {id} byla upravena","<p>Dobrý den,<br />
						Vaše objednávka {id} byla upravena.</p>","order_status_updated", "order");';
				Core::$db_conn->exec($sql);
			}

			if (!Core::$db->email_message[15]) {
				$sql = 'INSERT INTO `email_message` VALUES ("15","Vaše objednávka {id} čeká na platbu","<p>Dobrý den,<br />
						Vaše objednávka {id} čeká na uhrazení platby.</p>","order_status_waiting_for_payment", "order");';
				Core::$db_conn->exec($sql);
			}

			if (!Core::$db->email_message[16]) {
				$sql = 'INSERT INTO `email_message` VALUES ("16","Vaše objednávka {id} byla zaplacena","<p>Dobrý den,<br />
						Vaše objednávka {id} byla zaplacena.</p>","order_status_paid", "order");';
				Core::$db_conn->exec($sql);
			}

			if (!Core::$db->email_message[17]) {
				$sql = 'INSERT INTO `email_message` VALUES ("17","Vaše objednávka {id} čeká na vyzvednutí","<p>Dobrý den,<br />
						Vaše objednávka {id} čeká na vyzvednutí.</p>","order_status_waiting_for_pickup", "order");';
				Core::$db_conn->exec($sql);
			}

			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("18","Vaše objednávka {id} byla odeslána","<p>Dobrý den,<br />
						Vaše objednávka {id} byla odeslána.</p>","order_status_sent", "order");';
				Core::$db_conn->exec($sql);
			}
		}

		if (Core::$language == "sk") {
			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("13","Vaša objednávka {id} je potvrdená","<p>Dobrý deň,<br />
						Vaša objednávka {id} bola potvrzena obchodníkem.</p>","order_status_confirmed", "order");';
				Core::$db_conn->exec($sql);
			}

			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("14","Vaša objednávka {id} bola upravena","<p>Dobrý deň,<br />
						Vaša objednávka {id} bola upravena.</p>","order_status_updated", "order");';
				Core::$db_conn->exec($sql);
			}


			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("15","Vaša objednávka {id} čaká na platbu","<p>Dobrý deň,<br />
						Vaša objednávka {id} čaká na platbu.</p>","order_status_waiting_for_payment", "order");';
				Core::$db_conn->exec($sql);
			}


			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("16","Vaša objednávka {id} bola zaplatena","<p>Dobrý deň,<br />
						Vaša objednávka {id} bola zaplatena.</p>","order_status_paid", "order");';
				Core::$db_conn->exec($sql);
			}


			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("17","Vaša objednávka {id} čaká na vyzdvihnutie","<p>Dobrý deň,<br />
						Vaša objednávka {id} čaká na vyzdvihnutie.</p>","order_status_waiting_for_pickup", "order");';
				Core::$db_conn->exec($sql);
			}


			if (!Core::$db->email_message[18]) {
				$sql = 'INSERT INTO `email_message` VALUES ("18","Vaša objednávka {id} bola odoslaná","<p>Dobrý deň,<br />
						Vaša objednávka {id} bola odoslaná.</p>","order_status_sent", "order");';
				Core::$db_conn->exec($sql);
			}


		}

	}

	private function version_1_0_20()
	{
		if (Core::config('jpeg_quality') === null) {
			Core::$db->config(array(
				'name' => 'jpeg_quality',
				'value' => 100
			));
		} else {
			Core::$db->config()->where('name', 'jpeg_quality')->update(array('value' => 100));
		}
	}


	private function version_1_0_14()
	{
		if (Core::config('default_order') === null) {
			Core::$db->config(array(
				'name' => 'default_order',
				'value' => 'position ASC'
			));
		}
	}

	private function version_1_0_13()
	{
		if (Core::config('dbversion') === null) {
			Core::$db->config(array(
				'name' => 'dbversion',
				'value' => '1.0.0'
			));
		}
	}

	private function version_1_0_11()
	{
		Core::$db_inst->schema->createTable('rotator', array(
			'id' => array(
				'type' => 'integer',
				'primary' => true,
				'autoincrement' => true
			),
			'name' => array(
				'type' => 'varchar',
				'not_null' => true,
				'default' => ''
			),
			'width' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => '0'
			),
			'height' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => '0'
			),
			'interval' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => '0'
			),
			'last_change' => array(
				'type' => 'integer',
			),
			'last_change_user' => array(
				'type' => 'integer',
			)
		));

		Core::$db_inst->schema->addColumns('payment', array(
			'ord' => array(
				'type' => 'integer'
			)
		));
		Core::$db_inst->schema->addColumns('delivery', array(
			'ord' => array(
				'type' => 'integer'
			)
		));

		$i = 10;
		foreach (Core::$db->payment() as $p) {
			$p->update(array('ord' => $i));
			$i += 10;
		}

		foreach (Core::$db->delivery() as $p) {
			$p->update(array('ord' => $i));
			$i += 20;
		}
	}

	private function version_1_0_30()
	{
		Core::$db_inst->schema->addColumns('product_attributes', array(
			'weight' => array(
				'type' => 'float',
				'not_null' => true
			)
		));
	}

	private function version_1_0_32()
	{
		if (Core::config('watermark_apply_on_original') === null) {
			Core::$db->config(array(
				'name' => 'watermark_apply_on_original',
				'value' => 0
			));
		}

		if (Core::config('watermark_min_size') === null) {
			Core::$db->config(array(
				'name' => 'watermark_min_size',
				'value' => 250
			));
		}

		if (Core::config('watermark_type') === null) {
			Core::$db->config(array(
				'name' => 'watermark_type',
				'value' => 'text'
			));
		}

		if (Core::config('watermark_filepath') === null) {
			Core::$db->config(array(
				'name' => 'watermark_filepath',
				'value' => 'files/watermark.png'
			));
		}

		if (Core::config('watermark_text') === null) {
			Core::$db->config(array(
				'name' => 'watermark_text',
				'value' => ''
			));
		}

		if (Core::config('watermark_alignment') === null) {
			Core::$db->config(array(
				'name' => 'watermark_alignment',
				'value' => 'c'
			));
		}

		if (Core::config('watermark_font_color') === null) {
			Core::$db->config(array(
				'name' => 'watermark_font_color',
				'value' => '255, 255, 255'
			));
		}

		if (Core::config('watermark_font_size') === null) {
			Core::$db->config(array(
				'name' => 'watermark_font_size',
				'value' => 30
			));
		}

		if (Core::config('watermark_opacity') === null) {
			Core::$db->config(array(
				'name' => 'watermark_opacity',
				'value' => 50
			));
		}

		if (Core::config('watermark_png24') === null) {
			Core::$db->config(array(
				'name' => 'watermark_png24',
				'value' => 0
			));
		}

		if (Core::config('watermark_text_shadow') === null) {
			Core::$db->config(array(
				'name' => 'watermark_text_shadow',
				'value' => 1
			));
		}
	}

	private function version_1_0_37()
	{
		if (Core::config('hide_no_stock_products') === null) {
			Core::$db->config(array(
				'name' => 'hide_no_stock_products',
				'value' => '0'
			));
		}

		Core::$db_inst->schema->addColumns('product', array(
			'hide_product_on_zero_stock' => array(
				'type' => 'integer',
				'length' => 1,
				'not_null' => true,
				'default' => 0
			)
		));
	}

	private function version_1_0_38()
	{
		Core::$db_inst->schema->changeColumnType('customer_group', 'coefficient', 'FLOAT( 12, 6 )');
	}

	private function version_1_0_41()
	{

		$js = '<script type="text/javascript">
			var _hrq = _hrq || [];
			_hrq.push(["setKey", "%s"]);
			_hrq.push(["setOrderId", "%s"]);

			%s

			_hrq.push(["trackOrder"]);

			(function() {
				var ho = document.createElement("script"); ho.type = "text/javascript"; ho.async = true;
				ho.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".heureka.cz/direct/js/cache/1-roi-async.js";
				var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ho, s);
			})();
		</script>';

		Core::$db->config(array(
			"name" => "conversion_rate_script",
			"value" => $js,
		));
	}

	private function version_1_0_40()
	{
		Core::$db->config(array(
			'name' => 'account_num',
			'value' => ''
		));
	}

	private function version_2_0_1() {
		Core::$db_inst->schema->addColumns('order', array(
			'send_email_to_customer' => array(
				'type' => 'integer',
				'default' => 0,
				'not_null' => true,
			)
		));
	}

	private function version_2_0_2() {
		Core::$db_inst->schema->addColumns('product', array(
			'unit' => array(
				'type' => 'varchar',
				'length' => 10
			)
		));

		if (Core::config('default_unit') === null) {
			Core::$db->config(array(
				'name' => 'default_unit',
				'value' => 'ks'
			));
		}
	}

	private function version_2_0_3() {
		Core::$db_inst->schema->addColumns('availability', array(
			'hex_color' => array(
				'type' => 'varchar'
			)
		));
	}

	private function version_2_0_4() {
		if (Core::config('page_order_step1') === null) {
			Core::$db->config(array(
				'name' => 'page_order_step1',
				'value' => ''
			));
		}
		if (Core::config('page_order_step2') === null) {
			Core::$db->config(array(
				'name' => 'page_order_step2',
				'value' => ''
			));
		}
	}



	/*
		private function version_1_4_1()
		{
			Core::$db_inst->schema->addColumns('order', array(
				'payment_status' => array(
					'type' => 'varchar'
				)
			));
		}

		private function version_1_3_10()
		{
			if(Core::config('language_front') === null){
				Core::$db->config(array(
					'name' => 'language_front',
					'value' => Core::config('language')
				));
			}
		}

		private function version_1_3_6()
		{
			if(Core::config('zbozi_conversions') === null){
				Core::$db->config(array(
					'name' => 'zbozi_conversions',
					'value' => ''
				));
			}
		}

		private function version_1_3_5_beta()
		{
			Core::$db_inst->schema->addColumns('product', array(
				'import_unique' => array(
					'type' => 'varchar',
					'length' => 48,
					'index' => true
				),
				'import_schema' => array(
					'type' => 'varchar',
					'length' => 48,
					'index' => true
				),
				'import_uid' => array(
					'type' => 'varchar',
					'length' => 13,
					'index' => true
				)
			));

			FS::cleanCache();

			if($f = glob(DOCROOT.'etc/xml_import/*.php')){

				foreach($f as $file){
					$info = pathinfo($file);

					$schema_hash = sha1($info['filename']);

					$schema_id = 'di_'.substr($schema_hash, 0, 6).'_'.substr($schema_hash, -6);

					$index = Registry::get($schema_id);

					$uid = uniqid();

					if($index && is_array($index)){
						foreach($index as $unique => $product_id){
							Core::$db->product()->where('id', (int) $product_id)->where('import_schema IS NULL')->update(array(
								'import_unique' => $unique,
								'import_schema' => $info['filename'],
								'import_uid' => $uid
							));
						}
					}
				}
			}
		}

		private function version_1_3_0()
		{
			Core::$db_inst->schema->addColumns('product', array(
				'ignore_discounts' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				)
			));

			if(Core::config('url_rewrite') === null){
				Core::$db->config(array(
					'name' => 'url_rewrite',
					'value' => 0
				));
			}

		}

		private function version_1_3_0_rc1()
		{
			Core::$db_inst->schema->addColumns('payment', array(
				'info' => array(
					'type' => 'text',
				)
			));

			Core::$db_inst->schema->addColumns('invoice', array(
				'type' => array(
					'type' => 'varchar',
					'length' => 24,
					'not_null' => true,
					'default' => 'invoice'
				),
				'shipping_address' => array(
					'type' => 'text'
				),
				'customer_name' => array(
					'type' => 'varchar'
				),
				'company_id' => array(
					'type' => 'varchar',
					'length' => 24
				),
				'company_vat' => array(
					'type' => 'varchar',
					'length' => 24
				),
				'address' => array(
					'type' => 'text'
				)
			));

			Core::$db_inst->schema->createTable('product_attribute_template', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'name' => array(
					'type' => 'varchar',
					'not_null' => true,
					'default' => ''
				)
			));

			Core::$db_inst->schema->createTable('product_attribute_template_items', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'product_attribute_template_id' => array(
					'type' => 'integer',
					'index' => true
				),
				'name' => array(
					'type' => 'varchar',
					'not_null' => true,
					'default' => ''
				),
				'value' => array(
					'type' => 'varchar'
				),
				'price' => array(
					'type' => 'varchar',
					'length' => 24
				)
			));

			if(Core::config('jpeg_quality') === null){
				Core::$db->config(array(
					'name' => 'jpeg_quality',
					'value' => 90
				));
			}

			if(Core::config('ajaxbasket') === null){
				Core::$db->config(array(
					'name' => 'ajaxbasket',
					'value' => 0
				));
			}

			FS::cleanCache();

			foreach(Core::$db->invoice() as $invoice){
				$lines = preg_split('/\r?\n/', $invoice['customer']);
				preg_match('/IČ:\s+([\d\s]{1,})/', $invoice['customer'], $ic_m);
				preg_match('/DIČ:\s+([\w\d\s]{1,})/', $invoice['customer'], $dic_m);

				$name = trim($lines[0]);
				$address = join("\n", array_slice($lines, 1, -2));
				$ic = trim($ic_m[1]);
				$dic = trim($dic_m[1]);

				$invoice->update(array(
					'customer_name' => $name,
					'company_id' => $ic,
					'company_vat' => $dic,
					'address' => $address
				));
			}
		}

		private function version_1_3_0_beta()
		{
			if(Core::config('stock_out_set_status') === null){
				Core::$db->config(array(
					'name' => 'stock_out_set_status',
					'value' => 0
				));
			}

			if(Core::config('upload_unique_names') === null){
				Core::$db->config(array(
					'name' => 'upload_unique_names',
					'value' => 1
				));
			}

			Core::$db_inst->schema->addColumns('email_message', array(
				'template' => array(
					'type' => 'varchar',
				)
			));

			FS::cleanCache();

			Core::$db->email_message()->where('`key`', 'new_order')->update(array('template' => 'order'));
			Core::$db->email_message()->where('`key`', 'new_account')->update(array('template' => 'account'));
			Core::$db->email_message()->where('`key`', 'reset_password')->update(array('template' => 'password_reset'));
			Core::$db->email_message()->where('`key`', 'new_password')->update(array('template' => 'account'));
			Core::$db->email_message()->where('`key`', 'order_comment')->update(array('template' => 'order_comment'));
			Core::$db->email_message()->where('`key`', 'low_stock')->update(array('template' => 'default'));
			Core::$db->email_message()->where('`key`', 'voucher')->update(array('template' => 'voucher'));
			Core::$db->email_message()->where('`key`', 'order_status_in_process')->update(array('template' => 'order'));
			Core::$db->email_message()->where('`key`', 'order_status_finished')->update(array('template' => 'order'));
			Core::$db->email_message()->where('`key`', 'order_status_canceled')->update(array('template' => 'order'));
			Core::$db->email_message()->where('`key`', 'order_status_to_send')->update(array('template' => 'order'));
			Core::$db->email_message()->where('`key`', 'order_confirmed')->update(array('template' => 'order'));

		}

		private function version_1_2_20()
		{
			if(Core::config('xml_feed_availability_id') === null){
				Core::$db->config(array(
					'name' => 'xml_feed_availability_id',
					'value' => 0
				));
			}
		}

		private function version_1_2_16()
		{
			Core::$db_inst->schema->addColumns('voucher', array(
				'min_price' => array(
					'type' => 'float',
				)
			));
		}

		private function version_1_2_14()
		{
			if(Core::config('phone_required') === null){
				Core::$db->config(array(
					'name' => 'phone_required',
					'value' => 1
				));
			}
		}

		private function version_1_2_12()
		{
			if(Core::config('phone_required') === null){
				Core::$db->config(array(
					'name' => 'phone_required',
					'value' => 1
				));
			}
		}

		private function version_1_2_11()
		{
			foreach(Core::$db->product()->where('price IS NULL') as $product){
				if($product['cost'] && $product['margin']){
					$product->update(array(
						'price' => round((float) $product['cost'] * (((float) $product['margin'] / 100) + 1), 2)
					));
				}
			}
		}

		private function version_1_2_10()
		{
			Core::$db_inst->schema->createTable('label', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'name' => array(
					'type' => 'varchar',
				),
				'color' => array(
					'type' => 'varchar',
				)
			));

			Core::$db_inst->schema->createTable('product_labels', array(
				'product_id' => array(
					'type' => 'integer',
					'primary' => true
				),
				'label_id' => array(
					'type' => 'integer',
					'primary' => true
				),
			));

			Core::$db_inst->schema->createTable('product_related', array(
				'product_id' => array(
					'type' => 'integer',
					'primary' => true
				),
				'related_id' => array(
					'type' => 'integer',
					'primary' => true
				),
			));

			Core::$db->label(array('name' => 'Akce', 'color' => 'DA0400'));
		}

		private function version_1_2_9()
		{
			if(Core::config('xml_feeds_ttl') === null){
				Core::$db->config(array(
					'name' => 'xml_feeds_ttl',
					'value' => 60
				));
			}

			Core::$db->config()->where('name', 'xml_feeds')->update(array('value' => 'dynamic'));
		}

		private function version_1_2_8()
		{
			if(Core::config('xml_feeds') === null){
				Core::$db->config(array(
					'name' => 'xml_feeds',
					'value' => 'static'
				));
			}

			if(Core::config('price_vat_round') === null){
				Core::$db->config(array(
					'name' => 'price_vat_round',
					'value' => -1
				));
			}

			Core::$db_inst->schema->addColumns('order', array(
				'currency' => array(
					'type' => 'varchar',
					'length' => 24
				)
			));
		}

		private function version_1_2_7()
		{
			if(Core::config('price_vat_round') === null){
				Core::$db->config(array(
					'name' => 'price_vat_round',
					'value' => -1
				));
			}

			Core::$db_inst->schema->addColumns('order', array(
				'currency' => array(
					'type' => 'varchar',
					'length' => 24
				)
			));
		}

		private function version_1_2_6()
		{
			if(Core::config('meta_robots') === null){
				Core::$db->config(array(
					'name' => 'meta_robots',
					'value' => 'index,follow'
				));
			}

			if(Core::config('meta_author') === null){
				Core::$db->config(array(
					'name' => 'meta_author',
					'value' => ''
				));
			}
		}

		private function version_1_2_3()
		{
			if(! Core::$db->email_message()->where('`key`', 'order_status_in_process')->fetch()){
				Core::$db->email_message(array(
					'key' => 'order_status_in_process',
					'subject' => 'Vaše objednávka se zpracovává',
					'text' => 'Dobrý den,
	Vaše objednávka číslo {id} je nyní zpracována a bude odeslána v nejkratším možném termínu.

	Kontaktní údaje:
	E-mail: {email}
	Tel.: {phone}

	Objednané zboží:
	{items}

	Děkujeme za Váš nákup.
	'
				));
			}

			if(! Core::$db->email_message()->where('`key`', 'order_status_finished')->fetch()){
				Core::$db->email_message(array(
					'key' => 'order_status_finished',
					'subject' => 'Vaše objednávka je vyřízena',
					'text' => 'Dobrý den,
	Vaše objednávka číslo {id} je vyřízena a odeslána na Vaši adresu.

	Kontaktní údaje:
	E-mail: {email}
	Tel.: {phone}

	Objednané zboží:
	{items}

	Děkujeme za Váš nákup.
	'
				));
			}

			if(! Core::$db->email_message()->where('`key`', 'order_status_canceled')->fetch()){
				Core::$db->email_message(array(
					'key' => 'order_status_canceled',
					'subject' => 'Vaše objednávka byla stornována',
					'text' => 'Dobrý den,
	Vaše objednávka číslo {id} je stornována.

	Kontaktní údaje:
	E-mail: {email}
	Tel.: {phone}

	Objednané zboží:
	{items}

	Děkujeme za Váš nákup.
	'
				));
			}

			if(! Core::$db->email_message()->where('`key`', 'order_status_to_send')->fetch()){
				Core::$db->email_message(array(
					'key' => 'order_status_to_send',
					'subject' => 'Vaše objednávka je připravena k odeslání',
					'text' => 'Dobrý den,
	Vaše objednávka číslo {id} je připravena k odeslání.

	Kontaktní údaje:
	E-mail: {email}
	Tel.: {phone}

	Objednané zboží:
	{items}

	Děkujeme za Váš nákup.
	'
				));
			}

			if(! Core::$db->email_message()->where('`key`', 'order_confirmed')->fetch()){
				Core::$db->email_message(array(
					'key' => 'order_confirmed',
					'subject' => 'Vaše objednávka {id} je potvrzena',
					'text' => 'Dobrý den,
	Vaše objednávka {id} byla potvrzena obchodníkem.

	{payment_info}

	Děkujeme za Váš nákup.
	'
				));
			}
		}

		private function version_1_2_2()
		{
			if(Core::config('image_driver') === null){
				Core::$db->config(array(
					'name' => 'image_driver',
					'value' => 'gd'
				));
			}
		}

		private function version_1_2_1()
		{
			Core::$db_inst->schema->createTable('static_file', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'localname' => array(
					'type' => 'varchar',
					'not_null' => true,
					'unique' => true
				),
				'url' => array(
					'type' => 'varchar',
				),
				'mimetype' => array(
					'type' => 'varchar',
				),
				'content' => array(
					'type' => 'blob',
				)
			));
		}

		private function version_1_2_0_rc2()
		{
			$this->version_1_2_0();
		}

		private function version_1_2_0()
		{
			FS::cleanCache();

			if(Core::config('version') === null){
				Core::$db->config(array(
					'name' => 'version',
					'value' => '1.2.0'
				));
			}

			Core::$db_inst->schema->addColumns('order', array(
				'confirmed' => array(
					'type' => 'integer',
				)
			));

			Core::$db_inst->schema->addColumns('product', array(
				'nameext' => array(
					'type' => 'varchar',
				),
				'productno' => array(
					'type' => 'varchar',
				)
			));

			Core::config('send_order_status', 0);
			Core::config('sitemap_products', 0);
			Core::config('upload_driver', 'auto');
			Core::config('confirm_orders', 0);
			Core::config('url_full_path', 0);

			$modules = Core::$db->module();
			count($modules);

			Core::$db_inst->schema->dropTable('module');

			Core::$db_inst->schema->createTable('module', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'name' => array(
					'type' => 'varchar',
					'unique' => true
				),
				'version' => array(
					'type' => 'varchar'
				),
				'status' => array(
					'type' => 'integer',
					'length' => 2,
					'not_null' => true,
					'default' => 0
				),
				'storage' => array(
					'type' => 'text'
				)
			));

			foreach($modules as $module){
				Core::$db->module($module->as_array());
			}
		}

		private function version_59()
		{

		}

		private function version_58()
		{
			Core::$db_inst->schema->createTable('registry', array(
				'name' => array(
					'type' => 'varchar',
					'length' => 32,
					'primary' => true
				),
				'value' => array(
					'type' => 'text'
				)
			));

			Core::$db_inst->schema->addColumns('voucher_generator', array(
				'type' => array(
					'type' => 'integer',
					'default' => 1,
					'not_null' => true
				),
				'event' => array(
					'type' => 'varchar',
				)
			));
		}

		private function version_57()
		{
			if(Core::config('last_cache_clean') === null){
				Core::$db->config(array(
					'name' => 'last_cache_clean',
					'value' => '0'
				));
			}
		}

		private function version_56()
		{
			Core::$db_inst->schema->createTable('module', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'name' => array(
					'type' => 'varchar',
					'unique' => true
				),
				'version' => array(
					'type' => 'integer'
				),
				'status' => array(
					'type' => 'integer',
					'length' => 2,
					'not_null' => true,
					'default' => 0
				),
				'storage' => array(
					'type' => 'text'
				)
			));
		}

		private function version_55()
		{
			if(Core::config('attributes_prices_change') === null){
				Core::$db->config(array(
					'name' => 'attributes_prices_change',
					'value' => '0'
				));
			}

			if(Core::config('allow_order_payment_change') === null){
				Core::$db->config(array(
					'name' => 'allow_order_payment_change',
					'value' => '0'
				));
			}

			Core::$db_inst->schema->addColumns('order_products', array(
				'type' => array(
					'type' => 'integer',
					'default' => 0,
					'not_null' => true
				)
			));
		}

		private function version_54()
		{
			Core::$db_inst->schema->createTable('voucher_generator', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'value' => array(
					'type' => 'varchar',
					'not_null' => true,
				),
				'name' => array(
					'type' => 'varchar'
				),
				'max_quantity' => array(
					'type' => 'integer'
				),
				'customer_group_id' => array(
					'type' => 'integer'
				),
				'min_price' => array(
					'type' => 'float'
				),
				'valid_from' => array(
					'type' => 'integer'
				),
				'valid_to' => array(
					'type' => 'integer'
				),
				'status' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 1
				),
				'last_change' => array(
					'type' => 'integer'
				),
				'last_change_user' => array(
					'type' => 'integer'
				),
				'used' => array(
					'type' => 'integer',
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('voucher', array(
				'generator_id' => array(
					'type' => 'integer',
				)
			));

			if(! Core::$db->email_message()->where('`key`', 'voucher')->fetch()){
				Core::$db->email_message(array(
					'subject' => 'Slevový kupón pro Váš další nákup',
					'text' => "Dobrý den,
	byl Vám vytvořen slevový kupón pro Váš další nákup v e-shopu {store_name}:

	Kód slevového kupónu: {code}
	Hodnota slevy: {value}",
					'key' => 'voucher'
				));
			}
		}

		private function version_53()
		{
			Core::$db_inst->schema->addColumns('basket', array(
				'voucher_id' => array(
					'type' => 'integer',
				)
			));

			Core::$db_inst->schema->addColumns('order', array(
				'voucher_id' => array(
					'type' => 'integer',
				)
			));

			Core::$db_inst->schema->addColumns('invoice', array(
				'voucher_id' => array(
					'type' => 'integer',
				)
			));

			Core::$db_inst->schema->addColumns('product', array(
				'cost' => array(
					'type' => 'float',
				),
				'margin' => array(
					'type' => 'varchar',
				)
			));

			if(Core::config('enable_vouchers') === null){
				Core::$db->config(array(
					'name' => 'enable_vouchers',
					'value' => '0'
				));
			}

			Core::$db_inst->schema->createTable('voucher', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'code' => array(
					'type' => 'varchar',
					'not_null' => true,
				),
				'value' => array(
					'type' => 'varchar',
					'not_null' => true,
				),
				'name' => array(
					'type' => 'varchar'
				),
				'max_quantity' => array(
					'type' => 'integer'
				),
				'quantity_per_customer' => array(
					'type' => 'integer'
				),
				'valid_from' => array(
					'type' => 'integer'
				),
				'valid_to' => array(
					'type' => 'integer'
				),
				'status' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 1
				),
				'last_change' => array(
					'type' => 'integer'
				),
				'last_change_user' => array(
					'type' => 'integer'
				),
				'used' => array(
					'type' => 'integer',
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->createTable('voucher_orders', array(
				'voucher_id' => array(
					'type' => 'integer',
					'primary' => true,
					'not_null' => true
				),
				'order_id' => array(
					'type' => 'integer',
					'primary' => true,
					'not_null' => true
				),
				'date' => array(
					'type' => 'integer',
					'not_null' => true
				)
			));
		}

		private function version_52()
		{
			Core::$db_inst->schema->addColumns('order', array(
				'vat_payer' => array(
					'type' => 'integer',
					'length' => 2,
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('invoice', array(
				'vat_payer' => array(
					'type' => 'integer',
					'length' => 2,
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('product', array(
				'vat_id' => array(
					'type' => 'integer'
				),
				'weight' => array(
					'type' => 'float'
				)
			));
		}

		private function version_51()
		{

		}

		private function version_50()
		{
			Core::$db_inst->schema->createTable('vat', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'name' => array(
					'type' => 'varchar'
				),
				'rate' => array(
					'type' => 'float'
				)
			));

			if(Core::config('vat_rate_default') === null){
				Core::$db->config(array(
					'name' => 'vat_rate_default',
					'value' => '1'
				));
			}

			if(Core::config('vat_payer') === null){
				Core::$db->config(array(
					'name' => 'vat_payer',
					'value' => (float) Core::config('vat') ? 1 : 0
				));
			}

			if(Core::config('vat_delivery') === null){
				Core::$db->config(array(
					'name' => 'vat_delivery',
					'value' => '1'
				));
			}

			if(Core::config('invoice_price_format') === null){
				Core::$db->config(array(
					'name' => 'invoice_price_format',
					'value' => '1'
				));
			}

			if(Core::config('order_round_decimals') === null){
				Core::$db->config(array(
					'name' => 'order_round_decimals',
					'value' => '2'
				));
			}

			if(Core::config('customer_confirmation') === null){
				Core::$db->config(array(
					'name' => 'customer_confirmation',
					'value' => '0'
				));
			}

			Core::$db_inst->schema->addColumns('order', array(
				'vat_payer' => array(
					'type' => 'integer',
					'length' => 2,
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('invoice', array(
				'vat_payer' => array(
					'type' => 'integer',
					'length' => 2,
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('product', array(
				'vat_id' => array(
					'type' => 'integer'
				),
				'weight' => array(
					'type' => 'float'
				)
			));

			Core::$db_inst->schema->addColumns('delivery', array(
				'weight_min' => array(
					'type' => 'float'
				),
				'weight_max' => array(
					'type' => 'float'
				),
				'price_min' => array(
					'type' => 'float'
				),
				'price_max' => array(
					'type' => 'float'
				)
			));

			Core::$db_inst->schema->addColumns('payment', array(
				'weight_min' => array(
					'type' => 'float'
				),
				'weight_max' => array(
					'type' => 'float'
				),
				'price_min' => array(
					'type' => 'float'
				),
				'price_max' => array(
					'type' => 'float'
				)
			));

			Core::$db_inst->schema->addColumns('customer', array(
				'active' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				)
			));

			/////////

			Core::$db->vat(array('name' => 'Základní', 'rate' => 20));
			Core::$db->vat(array('name' => 'Snížená', 'rate' => 10));


			$res = Core::$db_conn->exec('UPDATE `product` SET `vat_id` = "1"');
			unset($res);

			$res = Core::$db_conn->exec('UPDATE `invoice` SET `vat_payer` = "'.((float) Core::config('vat') ? 1 : 0).'"');
			unset($res);

			$res = Core::$db_conn->exec('UPDATE `order` SET `vat_payer` = "'.((float) Core::config('vat') ? 1 : 0).'"');
			unset($res);
		}

		private function version_49()
		{

		}

		private function version_48()
		{
			if(Core::config('heureka_conversions') === null){
				Core::$db->config(array(
					'name' => 'heureka_conversions',
					'value' => ''
				));
			}
		}

		private function version_47()
		{
			Core::$db_inst->schema->addColumns('product', array(
				'default_page' => array(
					'type' => 'integer'
				)
			));

			if(Core::config('invoice_symbol') === null){
				Core::$db->config(array(
					'name' => 'invoice_symbol',
					'value' => 'order_id'
				));
			}

			if(Core::config('hide_prices_to_visitors') === null){
				Core::$db->config(array(
					'name' => 'hide_prices_to_visitors',
					'value' => 0
				));
			}
		}

		private function version_46()
		{
			if(Core::config('last_lcheck') === null){
				Core::$db->config(array(
					'name' => 'last_lcheck',
					'value' => 0
				));
			}
		}

		private function version_45()
		{
			foreach(Core::$db->order() as $order){
				$order->update(array('total_incl_vat' => $order->model->getTotal_incl_vat(true)));
			}
		}

		private function version_44()
		{
			Core::$db_inst->schema->addColumns('order', array(
				'total_incl_vat' => array(
					'type' => 'float'
				)
			));

			foreach(Core::$db->order() as $order){
				$order->update(array('total_incl_vat' => $order['total_incl_vat']));
			}
		}

		private function version_43()
		{
			Core::$db_inst->schema->addColumns('page', array(
				'featureset_id' => array(
					'type' => 'integer'
				)
			));

			Core::$db_inst->schema->addColumns('product', array(
				'featureset_id' => array(
					'type' => 'integer'
				)
			));

			Core::$db_inst->schema->addColumns('feature', array(
				'type' => array(
					'type' => 'varchar',
					'not_null' => true,
					'default' => 'string'
				),
				'featureset_id' => array(
					'type' => 'integer',
					'index' => true
				),
				'value' => array(
					'type' => 'text'
				),
				'filter' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				),
			));

			Core::$db_inst->schema->createTable('featureset', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'name' => array(
					'type' => 'varchar'
				)
			));

			$features = Core::$db->product_features();

			if(count($features) && $featureset_id = Core::$db->featureset(array('name' => 'Default'))){
				if($featureset_id){
					Core::$db->feature()->update(array(
						'type' => 'string',
						'featureset_id' => $featureset_id,
						'value' => '',
						'filter' => 0
					));

					require_once(APPROOT.'admin/controller/features.php');

					$controller = new Controller_Features();

					$controller->_createTable(Core::$db->featureset[$featureset_id]);

					$tbl_name = 'features_'.$featureset_id;

					foreach(Core::$db->product() as $product){

						if(($features = $product->product_features()) && count($features)){
							$data = array(
								'product_id' => $product['id']
							);

							foreach($features as $feature){
								$data['f'.$feature['feature_id']] = $feature['value'];
							}

							Core::$db->$tbl_name($data);

							$product->update(array('featureset_id' => $featureset_id));
						}
					}

				}else{
					throw new Exception('Create default featureset failed');
				}
			}

			foreach(Core::$db->page() as $page){
				if($page['parent_page'] && ! $this->pageHasTopLevelParent($page['parent_page'])){
					$page->product_pages()->delete();
					$page->page_files()->delete();
					$page->delete();
				}
			}

		}

		private function version_42()
		{
			if(Core::config('stock_out_availability') === null){
				Core::$db->config(array(
					'name' => 'stock_out_availability',
					'value' => 0
				));
			}

			Core::$db_inst->schema->addColumns('product_attributes', array(
				'file_id' => array(
					'type' => 'integer'
				)
			));

			Core::$db_inst->schema->addColumns('block', array(
				'page_id' => array(
					'type' => 'integer'
				)
			));
		}

		private function version_41()
		{

		}

		private function version_40()
		{

		}

		private function version_39()
		{

		}

		private function version_38()
		{
			Core::$db_inst->schema->addColumns('product_attributes', array(
				'availability_id' => array(
					'type' => 'integer'
				)
			));
		}

		private function version_37()
		{
			$less = "@banner-input: #ffffff;

	 .logo ";

			foreach(Core::glob('theme/default_agatha_*.less.css') as $theme){
				if($content = file_get_contents($theme)){
					$content = preg_replace('/\/\* .logo \*\//miu', $less, $content);

					file_put_contents($theme, $content);
				}
			}

			foreach(Core::glob('theme/default_default_*.less.css') as $theme){
				if($content = file_get_contents($theme)){
					$content = preg_replace('/\/\* .logo \*\//miu', $less, $content);

					file_put_contents($theme, $content);
				}
			}
		}


		private function version_36()
		{
			Core::$db_inst->schema->createTable('search_word', array(
				'id' => array(
					'type' => 'integer',
					'primary' => true,
					'autoincrement' => true
				),
				'word' => array(
					'type' => 'varchar',
					'not_null' => true,
					'unique' => true
				)
			));

			Core::$db_inst->schema->createTable('search_index', array(
				'product_id' => array(
					'type' => 'integer',
					'primary' => true
				),
				'search_word_id' => array(
					'type' => 'integer',
					'primary' => true
				),
				'weight' => array(
					'type' => 'float',
					'not_null' => true
				)
			));
		}

		private function version_35()
		{
			Core::$db_inst->schema->addColumns('page', array(
				'seo_title' => array(
					'type' => 'varchar'
				)
			));
		}

		private function version_34()
		{
			Core::$db_inst->schema->addColumns('invoice', array(
				'note' => array(
					'type' => 'text'
				),
				'customer_id' => array(
					'type' => 'integer'
				)
			));
		}

		private function version_33()
		{
			Core::$db_inst->schema->addColumns('invoice', array(
				'last_change' => array(
					'type' => 'integer'
				),
				'last_change_user' => array(
					'type' => 'integer'
				)
			));

			Core::$db_inst->schema->addColumns('invoice_items', array(
				'vat' => array(
					'type' => 'float'
				)
			));
		}

		private function version_32()
		{

		}

		private function version_31()
		{
			if(Core::config('page_sitemap') === null){
				Core::$db->config(array(
					'name' => 'page_sitemap',
					'value' => 0
				));
			}

			Core::$db_inst->schema->addColumns('delivery', array(
				'customer_group_id' => array(
					'type' => 'integer',
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('payment', array(
				'customer_group_id' => array(
					'type' => 'integer',
					'not_null' => true,
					'default' => 0
				)
			));

			Core::$db_inst->schema->addColumns('page', array(
				'sync_id' => array(
					'type' => 'integer'
				)
			));
		}

		private function version_30()
		{
			Core::$db_inst->schema->addColumns('product', array(
				'lock_data' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				),
				'lock_price' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				),
				'lock_stock' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				)
			));

			if(Core::config('html_header') === null){
				Core::$db->config(array(
					'name' => 'html_header',
					'value' => ''
				));
			}
		}

		private function version_29()
		{

		}

		private function version_28()
		{
			Core::$db_inst->schema->addColumns('payment', array(
				'config' => array(
					'type' => 'text'
				)
			));

			Core::$db_inst->schema->addColumns('order', array(
				'payment_session' => array(
					'type' => 'varchar'
				),
				'payment_description' => array(
					'type' => 'varchar'
				)
			));

			if(Core::$db_inst->_type == 'mysql'){
				Core::$db_conn->exec('ALTER TABLE `order` CHANGE `payment_realized` `payment_realized` INT(10) NULL');
			}

			Core::$db_inst->schema->addIndex('page', array('discount'));
		}

		private function version_27()
		{
			Core::$db_inst->schema->addColumns('product', array(
				'sold_qty' => array(
					'type' => 'integer'
				)
			));
		}

		private function version_26()
		{

		}

		private function version_25()
		{
			Core::$db_inst->schema->addColumns('page', array(
				'inherit_products' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				),
				'enable_filter' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 1
				),
				'discount' => array(
					'type' => 'float'
				),
				'discount_date' => array(
					'type' => 'integer'
				)
			));
		}

		private function version_24()
		{

		}

		private function version_23()
		{
			if(Core::$db_inst->_type == 'mysql'){
				Core::$db_conn->exec('ALTER TABLE `config` CHANGE  `value` `value` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL');
			}

			if(Core::config('sharelinks') === null){
				Core::$db->config(array(
					'name' => 'sharelinks',
					'value' => '<script type="text/javascript">var addthis_config = {ui_language: "cs"}</script>
		<div class="addthis_toolbox addthis_default_style">
		<a href="http://www.addthis.com/bookmark.php?v=250&username=xa-4cd670dd7cef33d2" class="addthis_button_compact">Sdílet</a>
		<span class="addthis_separator">|</span>
		<a class="addthis_button_preferred_1"></a>
		<a class="addthis_button_preferred_2"></a>
		<a class="addthis_button_preferred_3"></a>
		<a class="addthis_button_preferred_4"></a>
		</div>
		<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4cd670dd7cef33d2"></script>
		<!-- AddThis Button END -->'
				));
			}
		}

		private function version_22()
		{
			Core::$db_inst->schema->addColumns('product_attributes', array(
				'sku' => array(
					'type' => 'varchar'
				),
				'ean13' => array(
					'type' => 'varchar'
				),
				'stock' => array(
					'type' => 'integer'
				)
			));

			Core::$db_inst->schema->addColumns('product', array(
				'show_sku' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 1
				),
				'show_ean13' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				),
				'list_attributes' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				)
			));
		}

		private function version_21()
		{
			if(Core::config('sync_key') === null){
				Core::$db->config(array(
					'name' => 'sync_key',
					'value' => random(mt_rand(10,14))
				));
			}

			if(Core::config('pohoda') === null){
				Core::$db->config(array(
					'name' => 'pohoda',
					'value' => ''
				));
			}

			Core::$db_inst->schema->addColumns('order', array(
				'sync_time' => array(
					'type' => 'integer'
				)
			));

			Core::$db_inst->schema->addColumns('page', array(
				'subpages_position' => array(
					'type' => 'integer',
					'not_null' => true,
					'default' => 1
				)
			));
		}

		private function version_20()
		{

		}

		private function version_19()
		{

		}

		private function version_18()
		{

		}

		private function version_17()
		{

		}

		private function version_16()
		{
			Core::$db_inst->schema->addColumns('product', array(
				'paid_listing' => array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'default' => 0
				)
			));
		}

		private function pageHasTopLevelParent($parent_id)
		{
			if($page = Core::$db->page[$parent_id]){

				if($page['parent_page']){
					return $this->pageHasTopLevelParent($page['parent_page']);

				}else{
					return true;
				}

			}else{
				return false;
			}
		}
	*/
}
