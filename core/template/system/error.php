<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>System Error - <?php echo $message?></title>
		
		<meta name="robots" content="noindex,nofollow" />
		
		<meta http-equiv="pragma" content="no-cache" />
		
		<style type="text/css">
			body {
				font-family: Arial, Tahoma;
				font-size: 12px;
				background: #f4f4f4;
			}
			a {
				color: #000;
			}
			#container {
				margin: 10px;
				background: #fff;
				border: 1px solid #ccc;
				padding: 2px;
			}
			#container h2 {
				font-size: 130%;
				margin: 0;
				padding: 10px;
				background: #cb0000;
				color: #fff;
			}
			#container h3 {
				font-size: 120%;
				margin: 15px 0;
			}
			#container h4 {
				font-size: 100%;
				margin: 15px 0 0 0;
				padding: 5px;
				background: #f0f0f0;
				border: 1px solid #ccc;
				border-width: 1px;
			}
			#container h5 {
				font-size: 100%;
				margin: 15px 0 5px 0;
			}
			.errorCode {
				float: right;
				font-weight: normal;
			}
			.wrap {
				padding: 10px;
			}
			.scrollable {
				border: 1px solid #ccc;
				border-width: 0 1px 1px 1px;
				background: #f8f8f8;
				padding: 5px;
				height: 300px;
				overflow: auto;
			}
			.code {
				background: #fff;
				padding: 0;
				height: auto;
			}
			.code .line {
				display: block;
				clear: left;
				font-family: monospace, Courier;
				white-space: nowrap;
			}
			.code .line span {
				display: block;
				padding: 2px 6px;
				white-space: pre;
			}
			.code .linenums .line span {
				padding-left: 0;
			}
			.code .selected {
				background-color: #ffd5d5;
			}
			.code .line .lnum {
				background: #f4f4f4;
				width: 70px;
				text-align: right;
				float: left;
				margin-right: 6px;
				color: #444;
			}
			.collapsible .scrollable {
				display: none;
			}
			.commercio-profiler {
				background-color: #fff;
			}
		</style>
		
		<script type="text/javascript">
			//<![CDATA[
			function toggle(id){
				var el = document.getElementById(id);
				
				if(el){
					el.style.display = (el.style.display == 'block') ? 'none' : 'block';
				}
				
				return false;
			}
			//]]>
		</script>
		
	</head>
	<body>
		
		<div id="container">
			<h2><span class="errorCode">Error code: <?php echo $code?></span><?php echo $message?></h2>
			
			<div class="wrap">
				<pre><?php echo $file?> [line <?php echo $line?>]</pre>
				
				<?php if(! empty($code_lines)): ?>
					<h4>Source Code</h4>
					<div class="scrollable code linenums">
						<?php foreach($code_lines as $i => $c): ?>
						<div class="line<?php echo ($i == $line ? ' selected' : '')?>">
							<span class="lnum"><?php echo $i?></span>
							<span><?php echo htmlspecialchars($c)?></span>
						</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				
				<?php if(! empty($query)): ?>
					<h4>Database Query</h4>
					<div class="scrollable code">
						<div class="line">
							<span><?php echo htmlspecialchars($query)?></span>
						</div>
					</div>
				<?php endif; ?>
				
				<div class="collapsible">
					<h4><a href="#" onclick="return toggle('backtrace');">Back Trace</a></h4>
					<div class="scrollable" id="backtrace">
						<pre><?php echo $backtrace?></pre>
					</div>
				</div>
				
				<div class="collapsible">
					<h4><a href="#" onclick="return toggle('included_files');">Included Files (<?php echo count($included_files)?>)</a></h4>
					<div class="scrollable code" id="included_files">
						<?php foreach($included_files as $inc_file): ?>
							<div class="line">
								<span><?php echo $inc_file?></span>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				
				<div class="collapsible">
					<h4><a href="#" onclick="return toggle('environment');">Profiler</a></h4>
					<div class="scrollable" id="environment">
						<?php echo tpl('system/profiler.php')?>
					</div>
				</div>
			</div>
		</div>
		
	</body>
</html>