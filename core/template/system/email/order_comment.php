<?php defined('WEBMEX') or die('No direct access.'); ?>

<div><?php echo $content?></div>

<table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin-top:25px">
	<tr>
		<td colspan="2">
			<h1 style="font-size:16px;font-weight:normal;margin:0 0 10px 0"><?php echo __('comments')?></h1>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" style="padding:0 0 15px 0">
			
			<table align="right" cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
				
				<?php $comment = $comments->fetch(); if(! $comment['is_private']): ?>
				<tr>
					<td style="padding:5px 5px 15px 5px;">
						<span style="display:block;margin-bottom:5px;color:#777;font-size:10px"><?php echo fdate($comment['time'], true)?></span>
						
						<p style="margin:0"><b><?php echo nl2br($comment['comment'])?></b></p>
					</td>
				</tr>
				<?php endif; ?>
			
				<?php if(count($comments) > 1): ?>
				<tr>
					<td style="background:#f0f0f0;padding:5px"><?php echo __('order_comment_history')?></td>
				</tr>
				
				<tr>
					<td style="padding:5px;font-size:10px">
						<?php $i = 0; foreach($comments as $comment): if(! $comment['is_private'] && $i > 0): ?>
						<div style="border-left:3px solid #ddd;margin: 5px 0;padding-left:10px">
							<span style="display:block;margin-bottom:5px;color:#777;font-size:10px"><?php echo fdate($comment['time'], true)?></span>
						
							<p style="margin:0"><?php echo nl2br($comment['comment'])?></p>
						</div>
						<?php endif; $i ++; endforeach; ?>
					</td>
				</tr>

				<?php endif; ?>
			</table>
			
		</td>
	</tr>
	
</table>