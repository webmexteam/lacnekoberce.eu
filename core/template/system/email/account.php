<?php defined('WEBMEX') or die('No direct access.'); ?>

<div><?php echo $content?></div>

<table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin-top:25px">
	<tr>
		<td colspan="2">
			<h1 style="font-size:16px;font-weight:normal;margin:0 0 10px 0"><?php echo __('your_customer_account')?></h1>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" style="padding:0 0 15px 0">
			
			<table align="right" cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
				<tr>
					<td style="background:#f0f0f0;padding:5px" colspan="2"><?php echo __('login_credentials')?></td>
				</tr>
				<tr>
					<td style="padding:5px;">
						<?php echo __('email')?>: <b><?php echo $email?></b><br>
						<?php echo __('password')?>: <b><?php echo $password?></b>
					</td>
				</tr>
				<tr>
					<td style="padding:5px;">
						<?php echo __('customer_account_url')?>: <a href="<?php echo url('customer', null, true)?>"><?php echo url('customer', null, true)?></a>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	
</table>