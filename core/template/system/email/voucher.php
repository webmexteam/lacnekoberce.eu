<?php defined('WEBMEX') or die('No direct access.'); ?>

<div><?php echo $content?></div>

<table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin-top:25px">
	<tr>
		<td colspan="2">
			<h1 style="font-size:16px;font-weight:normal;margin:0 0 10px 0"><?php echo __('discount_voucher')?></h1>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" style="padding:0 0 15px 0">
			
			<table align="right" cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
				<tr>
					<td style="padding:5px;">
						<?php echo __('voucher_code')?>: <b><?php echo $code?></b><br>
						<?php echo __('voucher_value')?>: <b><?php echo $value?></b>
						
						<?php if($valid_to): ?>
						<br><?php echo __('valid_to')?>: <?php echo fdate($valid_to)?>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td style="padding:5px;font-size:10px;color:#777">
						<?php echo __('email_voucher_info')?>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	
</table>