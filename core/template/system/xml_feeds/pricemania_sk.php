<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<products>
	<?php 
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');
	
	while(
		($_products = Core::$db->product()->where('status', 1)->limit(100, $offset)) && 
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) && 
		count($_products)
	):

	foreach($_products as $product): ?>
	<product> 
		<id><?php echo $product['id']?></id> 
		<name><![CDATA[<?php echo strip_tags(trim($product['name'].' '.$product['nameext']))?>]]></name>  
		<description><![CDATA[<?php echo strip_tags($product['description_short'])?>]]></description> 
		<price><?php echo price_vat($product['price'] + $product['fees'], $product['vat'])->price?></price>  
		<category><![CDATA[<?php echo Core::$controller->_getCategoryPath($product->product_pages()->fetch(), ' > ')?>]]></category> 
		<manufacturer><![CDATA[<?php echo Core::$controller->_getManufacturer($product)?>]]></manufacturer> 
		<url><?php echo url($product, null, true)?></url>  
		<picture><?php echo (($img = imgsrc($product, 3)) ? preg_replace('/\s/', '%20', $site.$img) : '')?></picture>
		<availability><?php echo $product->availability['name']?></availability> 
		
		<shipping></shipping> 
		
		<?php if($product['ean13']): ?>
		<ean><?php echo $product['ean13']?></ean>
		<?php endif; ?>
	</product>
	
	<?php endforeach; Core::$controller->_flush(); $offset += 100; endwhile; ?>
	
</products>
<?php Core::$controller->_flush(); ?>