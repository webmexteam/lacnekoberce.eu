<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<shop>
	<?php 
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');
	
	while(
		($_products = Core::$db->product()->where('status', 1)->limit(100, $offset)) && 
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) && 
		count($_products)
	):

	foreach($_products as $product): ?>
	<item>
		<internal_id><?php echo $product['id']?></internal_id>
		<name><![CDATA[<?php echo strip_tags(trim($product['name'].' '.$product['nameext']))?>]]></name>
		<description><![CDATA[<?php echo strip_tags($product['description_short'])?>]]></description>
		<manufacturer><![CDATA[<?php echo Core::$controller->_getManufacturer($product)?>]]></manufacturer>
		<price_eur><?php echo price_vat($product['price'] + $product['fees'], $product['vat'])->price?></price_eur>
		<vat><?php echo (float) $product['vat']?></vat>
		<product_url><?php echo url($product, null, true)?></product_url>
		<product_img><?php echo (($img = imgsrc($product, 3)) ? preg_replace('/\s/', '%20', $site.$img) : '')?></product_img>
		<category><![CDATA[<?php echo Core::$controller->_getCategoryPath($product->product_pages()->fetch(), ' > ')?>]]></category>

		<?php if(($days = $product->availability['days']) !== null): ?>
		<available><?php echo ($days == 0 ? 1 : ($days == 1 ? 2 : ($days < 0 ? 0 : $days)))?></available>
		<?php endif; ?>
		
		<highlight><?php echo (int) $product['paid_listing']?></highlight>
	</item>
	
	<?php endforeach; Core::$controller->_flush(); $offset += 100; endwhile; ?>
	
</shop>
<?php Core::$controller->_flush(); ?>