<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<SHOP>
	<?php 
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');
	
	while(
		($_products = Core::$db->product()->where('status', 1)->limit(100, $offset)) && 
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) && 
		count($_products)
	):

	foreach($_products as $product): ?>
	<SHOPITEM>
		<ITEMID><?php echo $product['id']?></ITEMID>
		<PRODUCT><![CDATA[<?php echo strip_tags(trim($product['name'].' '.$product['nameext']))?>]]></PRODUCT>
		<DESCRIPTION><![CDATA[<?php echo strip_tags($product['description_short'])?>]]></DESCRIPTION>
		<URL><?php echo url($product, null, true)?></URL>
		<TOLLFREE><?php echo ($product['paid_listing'] == 1 ? '0' : '1')?></TOLLFREE>
		
		<?php if(($days = $product->availability['days']) !== null): ?>
		<AVAILABILITY><?php echo ($days < 0 ? 'N' : ($days == 0 ? '0' : ($days * 24)))?></AVAILABILITY>
		<?php endif; ?>
		
		<?php if(($stock = $product['stock']) !== null): ?>
		<STOCK_COUNT><?php echo $stock?></STOCK_COUNT>
		<?php endif; ?>
		
		<?php if((int) $product['promote']): ?>
		<SALE>1</SALE>
		<?php endif; ?>
		
		<?php if($img = imgsrc($product, 3)): ?>
		<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
		<?php endif; ?>
		
		<PRICE><?php echo price_unvat($product['price'], $product['vat'])->price?></PRICE>
		<VAT><?php echo (float) $product['vat']?></VAT>
		
		<?php if($fees = $product['fees']): ?>
		<DUES><?php echo price_vat($fees, $product['vat'])->price?></DUES>
		<?php endif; ?>
		
		<?php foreach($product->product_pages() as $page): ?>
		<CATEGORYTEXT><![CDATA[<?php echo Core::$controller->_getCategoryPath($page, ' / ')?>]]></CATEGORYTEXT> 
		<?php endforeach; ?>
		
		<?php if($product['ean13']): ?>
		<EAN><?php echo $product['ean13']?></EAN>
		<?php endif; ?>
		
	</SHOPITEM>
	<?php endforeach; Core::$controller->_flush(); $offset += 100; endwhile; ?>
</SHOP>

<?php Core::$controller->_flush(); ?>