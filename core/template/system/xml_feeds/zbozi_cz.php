<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<SHOP>
	<?php
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');

	while(
		($_products = Core::$db->product()->where('status', 1)->limit(100, $offset)) &&
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) &&
		count($_products)
	):

	$row = Core::$db->availability[Core::config("stock_out_availability")];
	$outOfStockDeliveryDays = $row["days"];

	foreach($_products as $product):
	$attributes = $product->product_attributes()->order('name ASC, id ASC');

	?>
	<SHOPITEM>
		<?php if(count($attributes)): ?>
		<PRODUCTNAME><![CDATA[<?php echo strip_tags($product['name'])?>]]></PRODUCTNAME>

		<?php elseif($product['nameext']): ?>
		<PRODUCTNAME><![CDATA[<?php echo strip_tags($product['name'])?>]]></PRODUCTNAME>
		<PRODUCTNAMEEXT><![CDATA[<?php echo strip_tags($product['nameext'])?>]]></PRODUCTNAMEEXT>

		<?php else: ?>
		<PRODUCT><![CDATA[<?php echo strip_tags($product['name'])?>]]></PRODUCT>
		<?php endif; ?>

		<DESCRIPTION><![CDATA[<?php echo strip_tags($product['description_short'])?>]]></DESCRIPTION>
		<URL><?php echo url($product, null, true)?></URL>
		<UNFEATURED><?php echo ($product['paid_listing'] == 1 ? '0' : '1')?></UNFEATURED>

		<?php if (-1 < $product->availability["days"]): ?>

			<?php if ($product["stock"]): ?>

				<DELIVERY_DATE><?php echo $product->availability["days"]?></DELIVERY_DATE>

			<?php else: ?>

				<?php if (-1 < (int) Core::config("stock_out_availability")): ?>
					<DELIVERY_DATE><?php echo $outOfStockDeliveryDays ?></DELIVERY_DATE>

				<?php else: ?>
					<DEVLIVERY_DATE>-1</DELIVERY_DATE>

				<?php endif; ?>

			<?php endif; ?>

		<?php else: ?>

			<DELIVERY_DATE>-1</DELIVERY_DATE>

		<?php endif; ?>

		<?php if($img = imgsrc($product, 3)): ?>
		<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
		<?php endif; ?>

		<PRICE><?php echo price_unvat($product['price'], $product['vat'])->price?></PRICE>
		<VAT><?php echo (float) $product['vat']?></VAT>

		<?php if($fees = $product['fees']): ?>
		<DUES><?php echo price_vat($fees, $product['vat'])->price?></DUES>
		<?php endif; ?>

		<?php foreach($product->product_pages() as $page): ?>
		<CATEGORYTEXT><![CDATA[<?php echo Core::$controller->_getCategoryPath($page, ' | ')?>]]></CATEGORYTEXT>
		<?php endforeach; ?>

		<?php if($manufacturer = Core::$controller->_getManufacturer($product)): ?>
		<MANUFACTURER><![CDATA[<?php echo $manufacturer?>]]></MANUFACTURER>
		<?php endif; ?>

		<?php if($product['ean13']): ?>
		<EAN><?php echo $product['ean13']?></EAN>
		<?php endif; ?>

		<?php if(count($attributes)): foreach($attributes as $attribute): ?>
		<VARIANT>
			<PRODUCTNAMEEXT><![CDATA[<?php echo $attribute['name']?>: <?php echo $attribute['value']?>]]></PRODUCTNAMEEXT>
	    	<PRICE><?php echo price_unvat($product['price'] + estPrice($attribute['price'], $product['price']), $product['vat'])->price?></PRICE>

	    	<?php if($attribute['ean13']): ?>
			<EAN><?php echo $attribute['ean13']?></EAN>
			<?php endif; ?>

	    	<?php if($attribute['file_id'] && ($attrimg = imgsrc(Core::$db->product_files[$attribute['file_id']], 3))): ?>
	    	<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$attrimg)?></IMGURL>
	    	<?php elseif($img): ?>
	    	<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
	    	<?php endif; ?>

	    </VARIANT>
		<?php endforeach; endif; ?>
	</SHOPITEM>
	<?php endforeach; Core::$controller->_flush(); $offset += 100; endwhile; ?>
</SHOP>

<?php Core::$controller->_flush(); ?>
