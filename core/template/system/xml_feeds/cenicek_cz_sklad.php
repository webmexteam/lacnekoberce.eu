<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

Core::$controller->cache = false;
 
echo '<?xml version="1.0" encoding="utf-8"?>'?>

<SKLADONLINE>
	<?php if($product = Core::$db->product[(int) $_GET['itemid']]): ?>
	<ITEMID><?php echo $product['id']?></ITEMID>
	<STATUS>OK</STATUS>
	<STOCK_COUNT><?php echo ($product['stock'] !== null ? $product['stock'] : 1)?></STOCK_COUNT>
	<INFO><?php echo $product->availability['name']?></INFO>
	
	<?php else: ?>
	<ITEMID><?php echo $_GET['itemid']?></ITEMID>
	<STATUS>ERROR</STATUS>
	<STOCK_COUNT>0</STOCK_COUNT>
	<INFO>Zboží s tímto kódem v obchodě neexistuje.</INFO>
	<?php endif; ?>
</SKLADONLINE>

<?php Core::$controller->_flush(); ?>