<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

echo '<?xml version="1.0" encoding="utf-8"?>'?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<?php foreach(Core::$db->page()->where('status', 1) as $page): 

	if(in_array($page['id'], array(PAGE_BASKET, PAGE_ORDER, PAGE_ORDER_FINISH, PAGE_SEARCH, PAGE_404))){
		continue;
	}
	
?>
<url> 
<loc><?php echo url($page, null, true)?></loc> 
<lastmod><?php echo date('c', $page['last_change'] ? $page['last_change'] : time())?></lastmod> 
<changefreq>weekly</changefreq> 
<priority><?php echo ($page['menu'] ? '1.0' : '0.5')?></priority> 
</url> 
<?php endforeach; ?>

<?php if((int) Core::config('sitemap_products')): foreach(Core::$db->product()->where('status', 1) as $product): ?>
<url> 
<loc><?php echo url($product, null, true)?></loc> 
<lastmod><?php echo date('c', $product['last_change'] ? $product['last_change'] : time())?></lastmod> 
<changefreq>weekly</changefreq> 
<priority>0.5</priority> 
</url> 
<?php endforeach; endif; ?>

</urlset>