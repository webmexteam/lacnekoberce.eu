<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo __('error_404')?></title>
		
		<meta name="robots" content="noindex,nofollow" />
		
		<meta http-equiv="pragma" content="no-cache" />
		
		<style type="text/css">
			body {
				font-family: Arial, Tahoma;
				font-size: 12px;
				background: #f4f4f4;
				color: #555;
			}
			#container {
				width: 700px;
				margin: 10px auto;
				background: #fff;
				border: 1px solid #ccc;
				padding: 0;
			}
			#container .header {
				display: block;
				background: #ddd;
				padding: 30px;
				font-size: 350%;
				font-family: Georgia, sans;
				color: #a0a0a0;
			}
			#container h1 {
				font-size: 150%;
				margin: 0;
				padding: 5px;
				padding: 20px 30px;
			}
			#container p {
				margin: 15px 30px;
			}
			#container .goback {
				background: #efefef;
				border: 1px solid #ccc;
				border-width: 1px 0;
				padding: 10px 30px;
				margin-bottom: -1px;
			}
			#container .goback a {
				color: #555;
				font-weight: bold;
			}
			#container .goback a:hover {
				text-decoration: none;
			}
		</style>
		
	</head>
	<body>
		
		<div id="container">
			<span class="header">404</span>
			
			<h1><?php echo ($page ? $page['name'] : __('error_404'))?></h1>
			
			<div class="goback">
				<a href="<?php echo url('')?>">&lsaquo; <?php echo __('back_to_homepage')?></a>
			</div>
			
			<?php echo ($page ? $page['description'] : '')?>
		</div>
		
	</body>
</html>