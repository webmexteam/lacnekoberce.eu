<style type="text/css">
.commercio-profiler {
	padding: 10px;
}
.commercio-profiler h5 {
	font-size: 100%;
	margin: 15px 0 5px 0;
}
.commercio-profiler table {
	border-collapse: collapse;
	margin: 0;
	width: 100%;
}
.commercio-profiler table td {
	border: 1px solid #ddd;
	padding: 4px;
	background-color: #fff;
	font-family: monospace, Courier;
}
.commercio-profiler table tr > td:first-child {
	width: 20%;
	background-color: #f7f7f7;
	text-align: right;
}
</style>

<div class="commercio-profiler">
	<h5>STATS</h5>
	<table>
		<tr>
			<td>Memory Usage</td>
			<td><?php echo round(memory_get_usage() / 1024 / 1024, 2)?> MB</td>
		</tr>
		<tr>
			<td>Execution Time</td>
			<td><?php echo round(microtime(true) - Core::$benchmark['start_time'], 4)?> s</td>
		</tr>
	</table>
	
	<?php if(function_exists('get_defined_constants')): ?>
		<h5>CONSTANTS</h5>
		<table>
			<?php $c = get_defined_constants(true); foreach($c['user'] as $name => $value): ?>
			<tr>
				<td><?php echo $name?></td>
				<td><?php var_dump($value); ?></td>
			</tr>
			<?php endforeach; ?>
		</table>
	<?php endif; ?>
	
	<?php if(! empty($_GET)): ?>
		<h5>$_GET</h5>
		<table>
			<?php foreach($_GET as $name => $value): ?>
			<tr>
				<td><?php echo $name?></td>
				<td><?php var_dump($value); ?></td>
			</tr>
			<?php endforeach; ?>
		</table>
	<?php endif; ?>
	
	<?php if(! empty($_POST)): ?>
		<h5>$_POST</h5>
		<table>
			<?php foreach($_POST as $name => $value): ?>
			<tr>
				<td><?php echo $name?></td>
				<td><?php var_dump($value); ?></td>
			</tr>
			<?php endforeach; ?>
		</table>
	<?php endif; ?>
	
	<h5>$_COOKIE</h5>
	<table>
		<?php foreach($_COOKIE as $name => $value): ?>
		<tr>
			<td><?php echo $name?></td>
			<td><?php var_dump($value); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
	
	<h5>$_SESSION</h5>
	<table>
		<?php foreach($_SESSION as $name => $value): ?>
		<tr>
			<td><?php echo $name?></td>
			<td><?php var_dump($value); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
	
	<?php if(Core::$db): ?>
	<h5>DATABASE QUERIES</h5>
	<table>
		<?php $total = 0; foreach(Db::$queries as $i => $query): ?>
		<tr>
			<td>#<?php echo ($i + 1)?></td>
			<td style="<?php echo ($query['time'] >= 0.01 ? 'background-color:#f9f1d0;' : '')?>">(<?php echo $query['time']?> s) <?php echo $query['sql']?></td>
		</tr>
		<?php $total += (float) $query['time']; endforeach; ?>
		
		<tr>
			<td>Queries</td>
			<td><?php echo count(Db::$queries)?></td>
		</tr>
		<tr>
			<td>Total exec. time</td>
			<td><?php echo $total?> s</td>
		</tr>
		<tr>
			<td>Driver</td>
			<td><?php echo Core::$db_inst->_type?> (<?php echo Core::$def['db_library']?>)</td>
		</tr>
	</table>
	<?php endif; ?>
</div>