<?php if($analytics = Core::config('analytics')): ?>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<?php echo $analytics?>']);
	  _gaq.push(['_trackPageview']);
	  _gaq.push(['_addTrans',
	    '<?php echo $order['id']?>',
	    '<?php echo Core::config('store_name')?>',
	    '<?php echo $order['total_price']?>',
	    '', // tax
	    '<?php echo $order['delivery_payment']?>',
	    '<?php echo $order['city']?>',
	    '', // state or province
	    '<?php echo $order['country']?>'
	  ]);
		
	  <?php foreach($order->order_products() as $product): ?>
	  _gaq.push(['_addItem',
	    '<?php echo $order['id']?>',
	    '<?php echo ($product['sku'] ? $product['sku'] : $product['id'])?>',
	    '<?php echo $product['name']?>',
	    '', // category or variation
	    '<?php echo $product['price']?>',
	    '<?php echo $product['quantity']?>'
	  ]);
	  <?php endforeach; ?>
	  
	  _gaq.push(['_trackTrans']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
<?php endif; ?>

<?php

# Heureka conversion script
echo '<!-- Heureka conversion script -->';
if($heureka_conversions = Core::config("heureka_conversions") && Core::config("conversion_rate_script"))
	echo heureka_conversion_rate_script($order);
	
?>

<?php if($zbozi_conversions = Core::config('zbozi_conversions')): ?>
<iframe src="<?php echo $zbozi_conversions?>&uniqueId=<?php echo $order['id']?>&price=<?php echo $order['total_incl_vat']?>" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="position:absolute; top:-3000px; left:-3000px; width:1px; height:1px; overflow:hidden;"></iframe>
<?php endif; ?>

<?php if($sklik_conversions = Core::config('sklik_conversions')): ?>
<!-- Měřicí kód Sklik.cz -->
<iframe width="119" height="22" frameborder="0" scrolling="no" src="<?php echo $sklik_conversions?><?php echo $order['total_incl_vat']?>"></iframe>
<?php endif; ?>

<?php 
	if($heureka = Core::config('heureka_overeno')){
		$products = array(
			'produkt' => array()
		);
		
		foreach($order->order_products() as $product){
			if($product['product_id']){
				$products['produkt'][] = $product['name'];
			}
		}

		@file_get_contents('http://www.heureka.cz/direct/dotaznik/objednavka.php?id='.$heureka.'&email='.$order['email'].'&objednano='.date('Y-m-d').'&'.http_build_query($products));	
	}
?>
