<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul class="subpages subpages-<?php echo $position?>">
	<?php foreach($pages as $page): ?>
	<li>
		&raquo; <a href="<?php echo url($page)?>" class="title"><?php echo $page['name']?></a><br />
		
		<?php if($page['description_short']): ?>
		<div class="shortdesc">
			<?php echo $page['description_short']?>
		</div>
		<?php endif; ?>
	</li>
	<?php endforeach; ?>
</ul>