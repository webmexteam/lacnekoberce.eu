<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

echo tpl('layout_head.php'); ?>

	<div id="drop-shadow">
		<div id="container">
			<?php echo tpl('header.php'); ?>

			<div class="wrapper">
				<div id="main" class="clearfix">
					<div id="content"><?php echo $content ?></div>
				</div><!--! end of #main-->
			</div>

			<?php echo tpl('footer.php'); ?>
		</div>
	</div>

<?php echo tpl('layout_foot.php'); ?>