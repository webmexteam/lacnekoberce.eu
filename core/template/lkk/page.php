<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */


$nobread = array(5,14,20,2,23,24,4);
?>

<?php echo $page_file_content ?>

<?php echo (!in_array($page['id'], $nobread)) ? breadcrumb() : ''?>

	<!-- top files -->
<?php echo listFiles($page, 1)?>

	<!-- right files -->
<?php echo listFiles($page, 3)?>

	<!-- left files -->
<?php echo listFiles($page, 2)?>

<?php if($page['id'] == PAGE_INDEX) {
	require_once 'homepage.php';
}?>

<?php if(!in_array($page['id'], $nobread)):?>
<h1><?php echo ($page['id'] != PAGE_INDEX) ? (trim($page['seo_title']) ? $page['seo_title'] : $page['name']) : ""?></h1>
<?php endif?>

<?php if($page['description_short']): ?>
	<div class="shortdesc">
		<?php echo $page['description_short']?>
	</div>
<?php endif; ?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 2): ?>
	<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
		'pages' => $subpages,
		'position' => 'top'
	))?>
<?php endif; ?>

<?php echo $page['description']?>

	<!-- non-image files -->
<?php echo listFiles($page)?>

	<!-- bottom files -->
<?php echo listFiles($page, 0)?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 1): ?>
	<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
		'pages' => $subpages,
		'position' => 'bottom'
	))?>
<?php endif; ?>

<?php echo $page_action_top?>

<?php if($page['id'] != PAGE_INDEX && (count($products) || ! empty($_GET['f']))): ?>
	<?php echo tpl('products/'.$product_tpl, array('products' => $products, 'page' => $page, 'producers' => $producers, 'features' => $features))?>

	<?php if(count($products)): ?>
		<?php echo pagination($products_count)?>
	<?php elseif(! empty($_GET['p']) || ! empty($_GET['f'])): ?>
		<p class="no-products-found"><?php echo __('no_products_found')?></p>
	<?php endif; ?>
<?php endif; ?>

<?php echo $page_action?>