<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

if(count($pages)): ?>
	<option disabled="disabled" selected="selected"><?php echo __('select_option')?></option>
	<?php foreach($pages as $page): ?>
	<option value="<?php echo url($page)?>" <?php echo (Core::$current_page['id'] == $page['id'] ? ' selected="selected"' : '')?>><?php echo $page['name']?></option>
	<?php endforeach; ?>
<?php endif; ?>