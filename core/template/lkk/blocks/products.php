<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<div class="block-products">
	<?php foreach($products as $product): ?>
		<div class="product">
			<h3><a href="<?php echo url($product)?>"><?php echo $product['name'].' '.$product['nameext']?></a></h3>
			
			<?php if(($img = imgsrc($product)) !== null): ?>
				<div class="picture">
					<a href="<?php echo url($product)?>"><img src="<?php echo $img?>" alt="" /></a>
				</div>
			<?php endif; ?>
			
			<?php if(SHOW_PRICES && $product['price'] !== null): ?>
			<div class="price">
				<?php echo __('price')?>: <strong><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></strong>
			</div>
			<?php endif; ?>
			
		</div>
	<?php endforeach; ?>
</div>