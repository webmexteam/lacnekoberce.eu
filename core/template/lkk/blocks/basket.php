<div class="block-basket">
	<?php if($basket_products === null || ! count($basket_products)): ?>
	<p class="empty-basket"><?php echo __('basket_is_empty')?></p>
	<?php else: ?>
	<ul class="items">
		<?php foreach($basket_products as $product): ?>
		<li class="item itemid-<?php echo $product['product']['id']?>">
			<a href="<?php echo url($product['product'])?>"><?php echo $product['product']['name']?></a><br />
			
			<?php if($product['attributes']): ?>
			<?php echo $product['attributes']?><br />
			<?php endif; ?>
			
			<?php echo __('price')?>: <strong><?php echo price_vat(array($product['product'], $product['price']))?></strong><br />
			<?php echo __('quantity')?>: <strong class="qty"><?php echo $product['qty']?></strong>
		</li>
		<?php endforeach; ?>
	</ul>
	
	<div class="total">
		<?php echo __('total')?>: <strong><?php echo price($total->total)?></strong>
	</div>
	
	<div class="buttons">
		<a href="<?php echo url(PAGE_BASKET)?>"><?php echo __('checkout')?> &rsaquo;</a>
	</div>
	<?php endif; ?>
</div>