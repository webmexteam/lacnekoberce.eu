<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 if(Core::$is_premium && (int) Core::config('enable_customer_login')): ?>
	<?php echo tpl('customer_login.php')?>
<?php endif; ?>

<form action="<?php echo url(PAGE_ORDER)?>" method="post" class="form order clearfix">

	<?php if(isSet($order_errors)): ?>
	<div class="errors">
		<ul>
		<?php foreach($order_errors as $error): ?>
			<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
	
	<div class="clearfix">
		<fieldset class="colleft">
			<legend><?php echo __('billing_address')?></legend>
			
			<div class="clearfix">
				<div class="input inline required">
					<label for="first_name"><?php echo __('first_name')?>:</label>
					<input type="text" name="first_name" id="first_name" value="<?php echo $order['first_name']?>" class="text" />
				</div>
				
				<div class="input inline inline-right required">
					<label for="last_name"><?php echo __('last_name')?>:</label>
					<input type="text" name="last_name" id="last_name" value="<?php echo $order['last_name']?>" class="text" />
				</div>
			</div>
			
			<div class="input">
				<label for="company"><?php echo __('company')?>:</label>
				<input type="text" name="company" id="company" value="<?php echo $order['company']?>" class="text" />
			</div>
			
			<div class="clearfix">
				<div class="input inline">
					<label for="company_id"><?php echo __('company_id')?>:</label>
					<input type="text" name="company_id" id="company_id" value="<?php echo $order['company_id']?>" class="text" />
				</div>
				
				<div class="input inline inline-right">
					<label for="company_vat"><?php echo __('company_vat')?>:</label>
					<input type="text" name="company_vat" id="company_vat" value="<?php echo $order['company_vat']?>" class="text" />
				</div>
			</div>
			
			<div class="help"><?php echo __('company_help')?></div>
			
			<div class="input required">
				<label for="street"><?php echo __('street')?>:</label>
				<input type="text" name="street" id="street" value="<?php echo $order['street']?>" class="text" />
			</div>
			
			<div class="clearfix">
				<div class="input inline required">
					<label for="city"><?php echo __('city')?>:</label>
					<input type="text" name="city" id="city" value="<?php echo $order['city']?>" class="text" />
				</div>
				
				<div class="input inline inline-right required">
					<label for="zip"><?php echo __('zip')?>:</label>
					<input type="text" name="zip" id="zip" value="<?php echo $order['zip']?>" class="text short" />
				</div>
				
				<div class="input inline required">
					<label for="country"><?php echo __('country')?>:</label>
					<select name="country" id="country">
					<?php foreach(Core::$db->country() as $country): ?>
						<option value="<?php echo $country['name']?>"<?php echo (($order && $order['country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>
		
		</fieldset>
		
		<fieldset class="colright">
			<legend><?php echo __('shipping_address')?></legend>
			
			<div class="input">
				<input type="checkbox" name="shipping_as_billing" id="shipping_as_billing" value="1" class="checkbox"<?php echo ($order['ship_street'] ? '' : 'checked="checked"')?> /> 
				<label for="shipping_as_billing"><?php echo __('shipping_as_billing')?></label> 
			</div>
			
			<div id="shipping_address">
				<div class="clearfix">
					<div class="input inline required">
						<label for="ship_first_name"><?php echo __('first_name')?>:</label>
						<input type="text" name="ship_first_name" id="ship_first_name" value="<?php echo $order['ship_first_name']?>" class="text" />
					</div>
					
					<div class="input inline inline-right required">
						<label for="ship_last_name"><?php echo __('last_name')?>:</label>
						<input type="text" name="ship_last_name" id="ship_last_name" value="<?php echo $order['ship_last_name']?>" class="text" />
					</div>
				</div>
				
				<div class="input">
					<label for="ship_company"><?php echo __('company')?>:</label>
					<input type="text" name="ship_company" id="ship_company" value="<?php echo $order['ship_company']?>" class="text" />
				</div>
				
				<div class="input required">
					<label for="ship_street"><?php echo __('street')?>:</label>
					<input type="text" name="ship_street" id="ship_street" value="<?php echo $order['ship_street']?>" class="text" />
				</div>
				
				<div class="clearfix">
					<div class="input inline required">
						<label for="ship_city"><?php echo __('city')?>:</label>
						<input type="text" name="ship_city" id="ship_city" value="<?php echo $order['ship_city']?>" class="text" />
					</div>
					
					<div class="input inline inline-right required">
						<label for="ship_zip"><?php echo __('zip')?>:</label>
						<input type="text" name="ship_zip" id="ship_zip" value="<?php echo $order['ship_zip']?>" class="text short" />
					</div>
					
					<div class="input inline required">
						<label for="ship_country"><?php echo __('country')?>:</label>
						<select name="ship_country" id="ship_country">
						<?php foreach(Core::$db->country() as $country): ?>
							<option value="<?php echo $country['name']?>"<?php echo (($order && $order['ship_country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
						<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	
	<div class="clearfix">
		<fieldset class="colleft">
			<legend><?php echo __('contact_info')?></legend>
			
			<div class="clearfix">
				<div class="input inline required">
					<label for="email"><?php echo __('email')?>:</label>
					<input type="text" name="email" id="email" value="<?php echo $order['email']?>" class="text"<?php echo (Customer::$logged_in ? ' disabled="disabled"' : '')?> />
				</div>
				
				<div class="input inline inline-right<?php echo ($phone_required ? ' required' : '')?>">
					<label for="phone"><?php echo __('phone')?>:</label>
					<input type="text" name="phone" id="phone" value="<?php echo $order['phone']?>" class="text" />
				</div>
			</div>
			
			<?php if(! Customer::$logged_in): ?>
			
			<?php if(Core::$is_premium && (int) Core::config('enable_customer_login')): ?>
			<div class="input">
				<input type="checkbox" name="create_account" id="create_account" value="1" class="checkbox" checked="checked" /> 
				<label for="create_account"><?php echo __('create_account')?></label> 
			</div>
			<?php endif; ?>
			
			<div class="input">
				<input type="checkbox" name="newsletter" id="newsletter" value="1" class="checkbox" checked="checked" /> 
				<label for="newsletter""><?php echo __('want_to_receive_newsletter')?></label> 
			</div>
			
			<?php if(Core::$is_premium && (int) Core::config('enable_customer_login')): ?>
			<div class="help"><?php echo __('create_account_help')?></div>
			<?php endif; ?>
			
			<?php endif; ?>
		</fieldset>
		
		<fieldset class="colright">
			<legend><?php echo __('order_note')?></legend>
			
			<div class="input">
				<label for="comment"><?php echo __('comment')?>:</label>
				<textarea name="comment" id="comment" cols="90" rows="4"></textarea>
			</div>
		</fieldset>
	</div>
	
	<div class="clearfix">
		<fieldset class="colleft delivery">
			<legend><?php echo __('delivery')?></legend>
			
			<script>
				var delivery = {};
			</script>
			
			<?php if($deliveries): foreach($deliveries as $delivery): ?>
			<script>
				delivery[<?php echo $delivery['id']?>] = '<?php echo $delivery['price']?>';
			</script>
			
			<div class="input">
				<input type="radio" name="delivery" id="delivery_<?php echo $delivery['id']?>" value="<?php echo $delivery['id']?>" class="checkbox" /> 
				<label for="delivery_<?php echo $delivery['id']?>"><strong><?php echo $delivery['name']?></strong></label> 
			</div>
			<?php endforeach; endif; ?>
		</fieldset>
		
		<fieldset class="colright payment">
			<legend><?php echo __('payment')?></legend>
			
			<script>
				var payment = {};
			</script>
			
			<div class="help" id="payment_help"><?php echo __('payment_help')?></div>
			
			<?php if($payments): foreach($payments as $payment): ?>
			<script>
				payment[<?php echo $payment['id']?>] = '<?php echo $payment['price']?>';
			</script>
			
			<div class="input">
				<input type="radio" name="payment" id="payment_<?php echo $payment['id']?>" value="<?php echo $payment['id']?>" class="checkbox" /> 
				<label for="payment_<?php echo $payment['id']?>"><strong><?php echo $payment['name']?></strong> <span class="price">120 Kc</span></label> 
			</div>
			<?php endforeach; endif; ?>
		</fieldset>
		
		<script>
			var delivery_payments = [];
			<?php foreach(Core::$db->delivery_payments() as $dp): 
				$dp_data = $dp->as_array();
			?>
			delivery_payments.push(<?php echo json_encode($dp_data)?>);
			<?php endforeach; ?>
		</script>
	</div>
	
	<div class="basket">
		<div class="tablewrap">
			<table>
				<thead>
					<tr>
						<td class="picture">&nbsp;</td>
						<td class="name"><?php echo __('name')?></td>
						<td class="price price_qty"><?php echo __((int) Core::config('vat_payer') ? 'price_incl_vat' : 'price')?></td>
						<td class="total"><?php echo __('total')?></td>
					</tr>
				</thead>
				<tbody>
					<?php if($basket_products === null || ! count($basket_products)): ?>
					<tr>
						<td colspan="6" class="basketempty"><?php echo __('basket_is_empty')?></td>
					</tr>
					<?php endif; ?>
					
					<?php foreach($basket_products as $product): ?>
					<tr>
						<td class="picture">
							<?php if(isset($product['attribute_file_id'])): ?>
								<?php if(($img = imgsrcvariant($product['attribute_file_id'], $product['product'], 1)) !== null): ?>
									<img src="<?php echo $img?>" alt="" />
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							<?php else: ?>
								<?php if(($img = imgsrc($product['product'], 1)) !== null): ?>
									<img src="<?php echo $img?>" alt="" />
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							<?php endif; ?>
						</td>
						<td class="name">
							<?php echo $product['product']['name'].' '.$product['product']['nameext']?>
							
							<?php if($product['attributes']): ?>
							<br /><span class="attr"><?php echo $product['attributes']?></span>
							<?php endif; ?>
						</td>
						<td class="price price_qty"><?php echo $product['qty']?> &times; <?php echo price_vat(array($product['product'], $product['price']))?></td>
						<td class="total"><?php echo price_vat(array($product['product'], $product['price'] * $product['qty']))?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				
				<?php if($basket_products !== null && count($basket_products)):
					$total = Basket::total();
				?>
				<tfoot>
					<?php if((int) Core::config('vat_payer')): ?>
					<tr class="subtotal">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __('total_excl_vat')?>:</td>
						<td class="value"><span id="subtotal"><?php echo price($total->subtotal)?></span></td>
					</tr>
					<?php endif; ?>
					<tr class="deliverypayment">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __('delivery_payment')?>:</td>
						<td class="value"><span id="deliverypayment">&mdash;</span></td>
					</tr>
					
					<?php if($voucher = Basket::voucher()): ?>
					<tr class="voucher">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
						<td class="value"><span id="discount"><?php echo '-'.price($total->discount)?></span></td>
					</tr>
					<?php endif; ?>
					
					<tr class="total">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
						<td class="value"><span id="total"><?php echo price($total->total)?></span></td>
					</tr>
				</tfoot>
				<?php endif; ?>
			</table>
		</div>
	</div>
	
	<div class="buttons">
		<input type="checkbox" name="terms" id="terms" class="checkbox" /> <label for="terms"><?php echo __('i_accept_terms')?></label>
		<button type="submit" name="submit_order" class="button"><?php echo __('submit_order')?> &raquo;</button>
	</div>
	
</form>

<script>
	var _subtotal_excl_vat = <?php echo $total->subtotal?>;
	var _subtotal = <?php echo $total->_total_before_discount?>;
	
	<?php if($voucher): ?>
	var _voucher_value = '<?php echo $voucher['value']?>';
	<?php endif; ?>
</script>