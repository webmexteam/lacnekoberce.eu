<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="ordersummary order clearfix">

	<div class="steps">
		<div class="step step1 complete">
			<div class="num">1</div>
			<div class="text">Nákupní košík</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2 complete">
			<div class="num">2</div>
			<div class="text">Doprava a platba</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2 complete">
			<div class="num">3</div>
			<div class="text">Dodací údaje</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2 complete">
			<div class="num">4</div>
			<div class="text">Souhrn objednávky</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">
			<fieldset>
				<legend><?php echo __('your_order')?> #<?php echo $order['id']?></legend>
				<table>
					<tr>
						<th><?php echo __('order_id')?>:</th>
						<td>#<?php echo $order['id']?></td>
					</tr>
					<tr>
						<th><?php echo __('received')?>:</th>
						<td><?php echo fdate($order['received'], true)?></td>
					</tr>
					<?php if($order['status'] > 1): ?>
						<tr>
							<th><?php echo __('status')?>:</th>
							<td><?php echo __('status_'.Core::$def['order_status'][$order['status']])?></td>
						</tr>
					<?php endif; ?>
					<?php if($order['track_num']): ?>
						<tr>
							<th><?php echo __('track_num')?>:</th>
							<td><?php echo $order['track_num']?></td>
						</tr>
					<?php endif; ?>
				</table>
			</fieldset>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<fieldset>
				<legend><?php echo __('billing_address')?></legend>
				<table>
					<tr>
						<th><?php echo __('first_name')?>:</th>
						<td><?php echo $order['first_name']?></td>
					</tr>
					<tr>
						<th><?php echo __('last_name')?>:</th>
						<td><?php echo $order['last_name']?></td>
					</tr>
					<tr>
						<th><?php echo __('company')?>:</th>
						<td><?php echo $order['company']?></td>
					</tr>
					<tr>
						<th><?php echo __('company_id')?>:</th>
						<td><?php echo $order['company_id']?></td>
					</tr>
					<tr>
						<th><?php echo __('company_vat')?>:</th>
						<td><?php echo $order['company_vat']?></td>
					</tr>
					<tr>
						<th><?php echo __('street')?>:</th>
						<td><?php echo $order['street']?></td>
					</tr>
					<tr>
						<th><?php echo __('city')?>:</th>
						<td><?php echo $order['city']?></td>
					</tr>
					<tr>
						<th><?php echo __('zip')?>:</th>
						<td><?php echo $order['zip']?></td>
					</tr>
					<tr>
						<th><?php echo __('country')?>:</th>
						<td><?php echo $order['country']?></td>
					</tr>
					<tr>
						<th><?php echo __('email')?>:</th>
						<td><?php echo $order['email']?></td>
					</tr>
					<tr>
						<th><?php echo __('phone')?>:</th>
						<td><?php echo $order['phone']?></td>
					</tr>
				</table>
			</fieldset>
		</div>

		<div class="span6">
			<fieldset>
				<legend><?php echo __('shipping_address')?></legend>
				<?php if($order['ship_street']): ?>
					<table>
						<tr>
							<th><?php echo __('first_name')?>:</th>
							<td><?php echo $order['ship_first_name']?></td>
						</tr>
						<tr>
							<th><?php echo __('last_name')?>:</th>
							<td><?php echo $order['ship_last_name']?></td>
						</tr>
						<tr>
							<th><?php echo __('company')?>:</th>
							<td><?php echo $order['ship_company']?></td>
						</tr>
						<tr>
							<th><?php echo __('street')?>:</th>
							<td><?php echo $order['ship_street']?></td>
						</tr>
						<tr>
							<th><?php echo __('city')?>:</th>
							<td><?php echo $order['ship_city']?></td>
						</tr>
						<tr>
							<th><?php echo __('zip')?>:</th>
							<td><?php echo $order['ship_zip']?></td>
						</tr>
						<tr>
							<th><?php echo __('country')?>:</th>
							<td><?php echo $order['ship_country']?></td>
						</tr>
					</table>
				<?php else: ?>
					<p><?php echo __('shipping_as_billing')?></p>
				<?php endif; ?>
			</fieldset>

			<?php if($order['comment']):?>
				<fieldset>
					<legend><?php echo __('comment')?></legend>
					<?php echo nl2br($order['comment'])?>
				</fieldset>
			<?php endif; ?>

			<fieldset>
				<legend><?php echo __('delivery_payment')?></legend>
				<table>
					<tr>
						<th><?php echo __('delivery')?>:</th>
						<td><?php echo $order->delivery['name']?></td>
					</tr>
					<tr>
						<th><?php echo __('payment')?>:</th>
						<td><?php echo $order->payment['name']?></td>
					</tr>
					<?php if(Core::config('account_num') && strlen(Core::config('account_num')) > 0): ?>
						<tr>
							<th><?php echo __('account_num')?>:</th>
							<td><?php echo Core::config('account_num')?></td>
						</tr>
					<?php endif; ?>

					<?php if((int) $order['payment_realized']): ?>
						<tr>
							<th><?php echo __('payment_realized')?>:</th>
							<td><?php echo fdate($order['payment_realized'], true)?></td>
						</tr>
					<?php endif; ?>

					<?php if((int) $order['payment_realized'] && $order['payment_session']): ?>
						<tr>
							<th><?php echo __('payment_session')?>:</th>
							<td><?php echo $order['payment_session']?></td>
						</tr>
					<?php endif; ?>

					<?php if((int) Core::config('allow_order_payment_change') && ! (int) $order['payment_realized'] && $order['status'] == 1): ?>
						<tr>
							<th>&nbsp;</th>
							<td><a href="<?php echo url(PAGE_ORDER, array('payment' => 1, 'id' => $order['id'], 'hash' => $order->model->hash()), true)?>"><?php echo __('select_different_payment')?></a></td>
						</tr>
					<?php endif; ?>
				</table>
			</fieldset>
		</div>
	</div>

	<div class="wrap basket clearfix">
		<h3><?php echo __('products')?></h3>

		<div class="tablewrap">
			<table>
				<tbody>
					<?php
					$products = $order->order_products();

					if(Core::$db_inst->_type == 'sqlite'){
						$products->order('rowid ASC');
					}

					foreach($products as $product): ?>
					<tr>
						<td class="name">
							<?php echo $product['name'].' '.$product['nameext']?>
						</td>
						<td class="price price_qty"><?php echo $product['quantity']?> &times; <?php echo price_vat($product['price'], $product['vat'], $order['currency'])?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>

				<tfoot>
					<?php if((int) Core::config('vat_payer')): ?>
					<tr class="subtotal">
						<td class="label"><?php echo __('total_excl_vat')?>:</td>
						<td class="value"><span id="subtotal"><?php echo price($order['total_price'] + price_unvat($order['delivery_payment'], VAT_DELIVERY)->price, $order['currency'])?></span></td>
					</tr>
					<?php endif; ?>

					<?php if($order['voucher_id'] && ($voucher = $order->voucher) && $voucher['id']): ?>
					<tr class="voucher">
						<td class="label"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
						<td class="value"><span id="discount"><?php echo '-'.price(estPrice($voucher['value'], $order->model->getTotal_incl_vat(true)), $order['currency'])?></span></td>
					</tr>
					<?php endif; ?>

					<tr class="total">
						<td class="label"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
						<td class="value"><span id="total"><?php echo price($order['total_incl_vat'], $order['currency'])?></span></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

</div>
