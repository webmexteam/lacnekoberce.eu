﻿<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$tploptions = Registry::get('template_options');
?>


<div id="pdetail">
	<div class="row-fluid">
		<div class="span3">
			<div class="images">
				<!-- top files --><?php echo listFiles($product, 1)?>
				<!-- right files --><?php echo listFiles($product, 3, 3)?>
				<!-- left files --><?php echo listFiles($product, 2, 'product')?>

				<?php if(($labels = $product->product_labels()) && count($labels)): ?>
					<div class="labels">
						<?php foreach($labels as $label): if($label->label['name']): ?>
							<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
							<span class="reset"></span>
						<?php endif; endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="span9">
			<div class="row-fluid">
				<div class="<?php if($related_products): ?>span9<?php else:?>span12<?php endif?>">
					<div class="mainproduct">
						<h1 class="name"><?php echo trim($product['name'].' '.$product['nameext'])?></h1>

						<div class="description">
							<?php if($product['description_short']): ?>
								<div class="shortdesc">
									<?php echo $product['description_short']?>
								</div>
							<?php endif; ?>

						</div>

						<?php if(count($features)): ?>
							<div class="features">
								<table>
									<tbody>
									<?php foreach($features as $feature): ?>
										<tr>
											<td class="featurename"><?php echo $feature['name']?>:</td>
											<td><?php echo $feature['value'].' '.$feature['unit']?></td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						<?php endif; ?>


						<?php if(SHOW_PRICES && $product['price'] !== null && $product['vat']): ?>
							<div class="prices">
								<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
									<div class="availability">
										Dostupnost:
										<strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>>
											<?php echo $availability['name']?>
											<?php $unit = (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?>
											<?php echo ($product['stock'] > 0) ? '> '. $product['stock'] .' '.$unit: '' ?>
										</strong>
									</div>
								<?php endif ?>

								<?php if(SHOW_PRICES && ((float) $product['price_old'] || $product['discount'])): ?>
									<div class="old-price" id="product-old-price"><?php echo ((float) $product['price_old'] ? price($product['price_old']) : price_vat(array($product, $product['pricebeforediscount'])))?></div>
								<?php endif; ?>
								<div id="product-price"><?php echo price($product['price'] + $product['recycling_fee'])?></div>
								<div class="include-vat">včetně DPH</div>
								<div class="priceexclvat"><span id="product-price-excl-vat"><?php echo price_unvat(array($product, $product['price']+$product['recycling_fee']))?></span> bez DPH</div>
							</div>
						<?php elseif(SHOW_PRICES && $product['price'] !== null): ?>
							<div class="prices">
								<?php if(SHOW_PRICES && ((float) $product['price_old'] || $product['discount'])): ?>
									<div class="old-price" id="product-old-price"><?php echo ((float) $product['price_old'] ? price($product['price_old']) : price_vat(array($product, $product['pricebeforediscount'])))?></div>
								<?php endif; ?>
								<div id="product-price"><?php echo price_unvat(array($product, $product['price']+$product['recycling_fee']))?></div>
							</div>
						<?php endif; ?>

						<form action="<?php echo url(PAGE_BASKET)?>" method="post" class="basket clearfix">
							<?php if(Core::$is_premium): $i = 0;
								$attr_total_prices = ((int) Core::config('attributes_prices_change') || count($attributes) > 1) ? 0 : 1;
								?>

								<script>
									_product_discount = <?php echo (($product['discount'] && ! $product['price_old']) ? (float) $product['discount'] : 0)?>;
									_product_price_before_discount = <?php echo (float) $product['pricebeforediscount']?>;
									_product_price = <?php echo (float) $product['price']?>;
									_product_vat = <?php echo (float) $product['vat']?>;
								</script>

								<?php if(count($attributes)):?>
									<div class="attributes">
									<?php foreach($attributes as $attr_name => $options): ?>
										<fieldset class="attribute">
											<label><?php echo $attr_name?>:</label>

											<?php if(in_array($attr_name, $attributes_with_stock)): ?>
												<div class="product-variants">
													<?php foreach($options as $opt): ?>
														<label class="product-variant">
										<span class="variant-availability">
											<?php if($opt['availability']): ?>
												<strong class="availability-<?php echo $opt['availability']['days']?>days"><?php echo $opt['availability']['name']?></strong>
											<?php endif; ?>

											<?php echo (($display_stock && $opt['stock'] > 0) ? ' ('.$opt['stock'].' '.__('pcs').')' : '')?></span>
															<input type="radio" name="attributes[<?php echo $i?>]" value="<?php echo $opt['id']?>"<?php echo (($opt['default'] && $opt['enable']) ? ' checked="checked"' : '').($opt['enable'] ? '' : ' disabled="disabled"')?> /> <?php echo $opt['value']?>


															<?php if($opt['price']): ?>
																<?php if($attr_total_prices): ?>
																	<small>(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)</small>

																<?php else: ?>
																	<small>(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)</small>
																<?php endif; ?>
															<?php endif; ?>
														</label>

														<script>
															attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
														</script>

													<?php if($opt['file_id']): ?>
														<script>
															attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
														</script>
													<?php endif; ?>

													<?php endforeach; ?>
												</div>
											<?php else: ?>

												<select name="attributes[]">
													<?php foreach($options as $opt): ?>
														<option value="<?php echo $opt['id']?>"<?php echo ($opt['default'] ? ' selected="selected"' : '')?>>
															<?php echo $opt['value']?>

															<?php if($opt['price']): ?>
																<?php if($attr_total_prices): ?>
																	(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)

																<?php else: ?>
																	(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)
																<?php endif; ?>
															<?php endif; ?>
														</option>

														<script>
															attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
														</script>

													<?php if($opt['file_id']): ?>
														<script>
															attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
														</script>
													<?php endif; ?>

													<?php endforeach; ?>
												</select>
											<?php endif; ?>

										</fieldset>
										<?php $i ++; endforeach;?>
								</div><?php endif; ?>
							<?php endif; ?>

							<div class="reset"></div>


							<?php if($enable_buy && SHOW_PRICES): ?>
								<div class="buy">
									<fieldset class="buy">
										<input type="hidden" name="product_id" value="<?php echo $product['id']?>" />
										<input type="text" name="qty" value="1" size="2" class="qty"> ks
										<button type="submit" name="buy" class="btn btn-6">
								<span>
									<span>Do košíku</span>
								</span>
										</button>
									</fieldset>
								</div>
							<?php endif; ?>      
						</form>
													<?php echo $product['description']?>
			                  		</div>
				</div>

				<?php if($related_products): ?>
					<div class="span1"></div>
					<div class="span2">
						<div class="related">
							<h3>Podobné zboží</h3>
							<div class="clearfix">
								<?php foreach($related_products as $i => $related): ?>
									<div class="related">
										<a href="<?php echo url($related)?>" class="related-title">
											<img src="<?php echo imgsrc($related)?>" alt="" />
											<strong class="name"><?php echo limit_words($related['name'], 5)?></strong>
											<strong class="price"><?php echo price_vat($related)?></strong>
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>