<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$tploptions = Registry::get('template_options');
?>

<div class="steps">
	<div class="step step1 complete">
		<div class="num">1</div>
		<div class="text">Nákupní košík</div>
		<div class="line"><div class="left"></div><div class="right"></div></div>
	</div>
	<div class="step step2 complete">
		<div class="num">2</div>
		<div class="text">Doprava a platba</div>
		<div class="line"><div class="left"></div><div class="right"></div></div>
	</div>
	<div class="step step2 current">
		<div class="num">3</div>
		<div class="text">Dodací údaje</div>
		<div class="line"><div class="left"></div><div class="right"></div></div>
	</div>
	<div class="step step2">
		<div class="num">4</div>
		<div class="text">Souhrn objednávky</div>
		<div class="line"><div class="left"></div><div class="right"></div></div>
	</div>
</div>

<form action="<?php echo url(PAGE_ORDER2)?>" method="post" class="order inputs form clearfix">
	<div class="row-fluid">
		<div class="span8">
			<?php if(isSet($order_errors)): ?>
				<div class="errors">
					<ul>
						<?php foreach($order_errors as $error): ?>
							<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

			<fieldset class="">
				<legend><?php echo __('billing_address')?></legend>

				<div class="row-fluid">
					<div class="span6">
						<div class="input  required">
							<label for="first_name"><?php echo __('first_name')?>:</label>
							<input type="text" name="first_name" id="first_name" value="<?php echo $order['first_name']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input  -right required">
							<label for="last_name"><?php echo __('last_name')?>:</label>
							<input type="text" name="last_name" id="last_name" value="<?php echo $order['last_name']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="street"><?php echo __('street')?>:</label>
							<input type="text" name="street" id="street" value="<?php echo $order['street']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input  required">
							<label for="city"><?php echo __('city')?>:</label>
							<input type="text" name="city" id="city" value="<?php echo $order['city']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input  -right required">
							<label for="zip"><?php echo __('zip')?>:</label>
							<input type="text" name="zip" id="zip" value="<?php echo $order['zip']?>" class="text short" />
						</div>
					</div>
					<div class="span6">
						<div class="input  required">
							<label for="country"><?php echo __('country')?>:</label>
							<select name="country" id="country">
								<?php foreach(Core::$db->country() as $country): ?>
									<option value="<?php echo $country['name']?>"<?php echo (($order && $order['country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input  required">
							<label for="email"><?php echo __('email')?>:</label>
							<input type="text" name="email" id="email" value="<?php echo $order['email']?>" class="text"<?php echo (Customer::$logged_in ? ' disabled="disabled"' : '')?> />
						</div>
					</div>
					<div class="span6">
						<div class="input  -right<?php echo ($phone_required ? ' required' : '')?>">
							<label for="phone"><?php echo __('phone')?>:</label>
							<input type="text" name="phone" id="phone" value="<?php echo $order['phone']?>" class="text" />
						</div>
					</div>
				</div>

				<label class="buyoncompany">
					<input type="checkbox"> Zboží nakupuji na firmu (IČ)
				</label>

				<div class="buyoncompany-wrap">
					<div class="row-fluid">
						<div class="span6">
							<div class="input">
								<label for="company"><?php echo __('company')?>:</label>
								<input type="text" name="company" id="company" value="<?php echo $order['company']?>" class="text" />
							</div>
						</div>
						<div class="span3">
							<div class="input ">
								<label for="company_id"><?php echo __('company_id')?>:</label>
								<input type="text" name="company_id" id="company_id" value="<?php echo $order['company_id']?>" class="text" />
							</div>
						</div>
						<div class="span3">
							<div class="input  -right">
								<label for="company_vat"><?php echo __('company_vat')?>:</label>
								<input type="text" name="company_vat" id="company_vat" value="<?php echo $order['company_vat']?>" class="text" />
							</div>
						</div>
					</div>
				</div>
			</fieldset>

			<label class="jina-dodaci" for="shipping_as_billing">
				<input type="checkbox" name="shipping_as_billing" id="shipping_as_billing" value="1" class="checkbox"<?php echo ($order['ship_street'] ? '' : 'checked="checked"')?> />
				Dodací adresa je shodná s fakturační
			</label>

			<fieldset class="jina-dodaci-wrap">
				<legend><?php echo __('shipping_address')?></legend>

				<div class="row-fluid">
					<div class="span6">
						<div class="input  required">
							<label for="ship_first_name"><?php echo __('first_name')?>:</label>
							<input type="text" name="ship_first_name" id="ship_first_name" value="<?php echo $order['ship_first_name']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input  -right required">
							<label for="ship_last_name"><?php echo __('last_name')?>:</label>
							<input type="text" name="ship_last_name" id="ship_last_name" value="<?php echo $order['ship_last_name']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="ship_street"><?php echo __('street')?>:</label>
							<input type="text" name="ship_street" id="ship_street" value="<?php echo $order['ship_street']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input  required">
							<label for="ship_city"><?php echo __('city')?>:</label>
							<input type="text" name="ship_city" id="ship_city" value="<?php echo $order['ship_city']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input  -right required">
							<label for="ship_zip"><?php echo __('zip')?>:</label>
							<input type="text" name="ship_zip" id="ship_zip" value="<?php echo $order['ship_zip']?>" class="text short" />
						</div>
					</div>
					<div class="span6">
						<div class="input  required">
							<label for="ship_country"><?php echo __('country')?>:</label>
							<select name="ship_country" id="ship_country">
								<?php foreach(Core::$db->country() as $country): ?>
									<option value="<?php echo $country['name']?>"<?php echo (($order && $order['ship_country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<div class="input">
							<label for="ship_company"><?php echo __('company')?>:</label>
							<input type="text" name="ship_company" id="ship_company" value="<?php echo $order['ship_company']?>" class="text" />
						</div>
					</div>
				</div>
			</fieldset>

			<fieldset class="">
				<legend><?php echo __('order_note')?></legend>

				<div class="row-fluid">
					<div class="span12">
						<div class="input">
							<label for="comment"><?php echo __('comment')?>:</label>
							<textarea name="comment" id="comment" cols="90" rows="4"></textarea>
						</div>
					</div>
				</div>

				<?php if(! Customer::$logged_in): ?>
					<div class="row-fluid">
						<div class="span6">
							<?php if(Core::$is_premium && (int) Core::config('enable_customer_login')): ?>
								<div class="input">

									<label for="create_account">
										<input type="checkbox" name="create_account" id="create_account" value="1" class="checkbox" checked="checked" />
										<?php echo __('create_account')?>
									</label>
								</div>
							<?php endif; ?>
						</div>
						<div class="span6">
							<div class="input">
								<label for="newsletter"">
									<input type="checkbox" name="newsletter" id="newsletter" value="1" class="checkbox" checked="checked" />
									<?php echo __('want_to_receive_newsletter')?>
								</label>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
		<div class="span4 ordersidebar">
			<div class="title help">Pomoc s objednávkou</div>
			<div id="help-with-order">
				<?php if(Core::config('email_notify')):?><div class="email"><?php echo Core::config('email_notify')?></div><?php endif?>
			</div>
			<div class="basket">
				<div class="title">Nákupní košík</div>
				<div class="tablewrap last">
					<table>
						<tbody>
						<?php if($basket_products === null || ! count($basket_products)): ?>
							<tr>
								<td colspan="3" class="basketempty"><?php echo __('basket_is_empty')?></td>
							</tr>
						<?php endif; ?>

						<?php foreach($basket_products as $product): ?>
							<tr>
								<td class="name">
									<?php echo $product['product']['name'].' '.$product['product']['nameext']?>

									<?php if($product['attributes']): ?>
										<br /><span class="attr"><?php echo $product['attributes']?></span>
									<?php endif; ?>
								</td>
								<td class="price price_qty"><?php echo $product['qty']?>&times;</td>
								<td class="total"><?php echo str_replace(' ', '&nbsp;', price_vat(array($product['product'], $product['price'] * $product['qty'])))?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>

					<hr>

					<table class="dp">
						<tr>
							<td><strong><?php echo __('delivery_payment')?>:</strong></td>
							<td><strong id="deliverypayment"><?php echo $dp_price?></strong></td>
						</tr>
						<tr>
							<td><?php echo __('delivery')?></td>
							<td id="delivery-value"><?php echo $delivery['name']?></td>
						</tr>
						<tr>
							<td><?php echo __('payment')?></td>
							<td id="payment-value"><?php echo $payment['name']?></td>
						</tr>
					</table>
				</div>

				<div class="tablewrap final">
					<table>
						<?php if($basket_products !== null && count($basket_products)):
							$total = Basket::total();
							?>
							<tfoot>

							<?php if($voucher = Basket::voucher()): ?>
								<tr class="voucher">
									<td class="label" colspan="2"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
									<td class="value"><span id="discount"><?php echo '-'.price($total->discount)?></span></td>
								</tr>
							<?php endif; ?>

							<tr class="total">
								<td class="label" colspan="2"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
								<td class="value"><span id="total"><?php echo $total_price?></span></td>
							</tr>

							<?php if((int) Core::config('vat_payer')): ?>
								<tr class="subtotal">
									<td class="label" colspan="2"><?php echo __('total_excl_vat')?>:</td>
									<td class="value"><span id="subtotal"><?php echo price($total->subtotal)?></span></td>
								</tr>
							<?php endif; ?>

							</tfoot>
						<?php endif; ?>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="buttons clearfix">
		<div class="row-fluid">
			<div class="span6">
				<a href="<?php echo url(PAGE_ORDER)?>" class="button text btn btn-7"><span>Zpět na výběr dopravy a platby</span></a>
			</div>
			<div class="span6">
				<label for="terms" class="terms"><input type="checkbox" name="terms" id="terms" class="checkbox" /> <?php echo __('i_accept_terms')?></label>
				<button type="submit" name="submit_order" class="button text checkout btn btn-4"><?php echo __('submit_order')?> &rsaquo;</button>
				<div class="reset"></div>
			</div>
		</div>
	</div>
</form>

<script>
	$(document).ready(function(){
		$('.buyoncompany-wrap').hide();
		$('.buyoncompany input').change(function(){
			$('.buyoncompany-wrap').toggle();
		});
	});
</script>