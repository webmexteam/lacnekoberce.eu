<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul>
	<?php foreach($pages as $page): ?>
		<li><a href="<?php echo url($page)?>"><span><?php echo $page['name']?></span></a></li>
	<?php endforeach; ?>
	<?php if(Customer::$logged_in): ?>
		<li><a href="<?php echo url('customer')?>" class="ico-user"><span>Můj účet</span></a></li>
		<li><a href="<?php echo url('customer/logout')?>" class="ico-lock"><span>Odhlásit se</span></a></li>
	<?php else: ?>
		<li><a href="<?php echo url('customer/login')?>" class="ico-user"><span>Přihlášení</span></a></li>
		<li><a href="<?php echo url('customer/register')?>" class="ico-lock"><span>Registrace</span></a></li>
	<?php endif; ?>
</ul>