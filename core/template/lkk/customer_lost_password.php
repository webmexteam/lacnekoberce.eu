<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<form action="<?php echo url(true)?>" method="post" class="form customerpassword clearfix">
	<div class="customer-login clearfix">
		<fieldset>
			<legend><?php echo __('lost_password')?></legend>
			
			<?php if(isSet($customer_login_error)): ?>
			<div class="error">
				<p><?php echo $customer_login_error?></p>
			</div>
			<?php endif; ?>
			
			<?php if(isSet($customer_login_msg)): ?>
			<div class="msg">
				<p><?php echo $customer_login_msg?></p>
			</div>
			<?php endif; ?>
			
			<div class="input">
				<label for="customer_email_reset"><?php echo __('email')?>:</label>
				<input type="text" name="customer_email_reset" id="customer_email_reset" value="" class="text" />
			</div>
					
			<div class="buttons">
				<button name="reset_password" class="button"><?php echo __('reset_password')?></button>
			</div>
					
			<p><?php echo __('lost_password_help')?></p>
		</fieldset>
	</div>
</form>