<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tpls = Registry::get('template_options'); ?>

	<footer>
		<div id="footer">
			<div id="footer-wrap">
				<div class="wrapper">
					<div class="row-fluid">
						<div class="span3 border same-height">
							<h4><?php echo $tpls['lkk']['footer_menu_1_title']?></h4>
							<?php echo menu(101)?>
						</div>
						<div class="span3 border same-height">
							<h4><?php echo $tpls['lkk']['footer_menu_2_title']?></h4>
							<?php echo menu(102)?>
						</div>
						<div class="span3 border same-height">
							<h4><?php echo $tpls['lkk']['footer_menu_3_title']?></h4>
							<?php echo menu(103)?>
						</div>
						<div class="span3 same-height">
							<h4><?php echo $tpls['lkk']['footer_menu_4_title']?></h4>
							<?php echo menu(104)?>
						</div>
					</div>

					<a id="fb-link" href="<?php echo $tpls['lkk']['facebook_link']?>" target="_blank" title="Facebook">Facebook</a>
				</div>
			</div>
		</div>

		<div id="subfooter">
			<div class="wrapper">
				<p>Produced by <a href="<?php echo $tpls['lkk']['mr_yes_link']?>" target="_blank" class="yes"></a> studio: Copyright © 2013 LKK, Pohání systém <a href="http://webmex.cz" target="_blank" title="Webmex.cz">Webmex.cz</a></p>
			</div>
		</div>
	</footer>
<?php echo (Core::$profiler ? tpl('system/profiler.php') : '')?>