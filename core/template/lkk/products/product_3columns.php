<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');
$tabs_array = array($tploptions['tech']['hp_tab_1'],$tploptions['tech']['hp_tab_2'],$tploptions['tech']['hp_tab_3'],$tploptions['tech']['hp_tab_4'],$tploptions['tech']['hp_tab_5'],$tploptions['tech']['hp_tab_6'],$tploptions['tech']['hp_tab_7']);
if(!in_array($page['id'], $tabs_array)){
	echo tpl('filter.php', array('page' => $page, 'producers' => $producers, 'features' => $features));
}
?>

	<?php $i = 0; foreach($products as $product): ?>
	<?php $pname = trim($product['name'].' '.$product['nameext']); ?>

	<?php if($i % 3 == 0 && $i != 0 && $i != count($products)):?></div></div><div class="products"><div class="row-fluid line"><?php endif?>
	<?php if($i == 0):?><div class="products"><div class="row-fluid line"><?php endif?>

		<div class="span4">
			<div class="product">
				<div class="row-fluid">
					<div class="span4">
						<?php if(($img = imgsrc($product, null, 2, $alt)) !== null): ?>
							<a href="<?php echo url($product)?>" title="<?php echo $pname?>" class="image">
								<img src="<?php echo $img?>" alt="<?php echo $pname?>"/>
							</a>
						<?php endif; ?>
					</div>
					<div class="span8">
						<h2 class="title-jq"><a href="<?php echo url($product)?>" title="<?php echo $pname?>"><?php echo $pname?></a></h2>
						<div class="desc"><?php echo $product['description_short']?></div>

						<div class="row-fluid">
							<div class="span6">
								<?php if(SHOW_PRICES && $product['price'] !== null): ?>
								<div class="price">
									<?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?>
								</div>
								<?php endif?>
							</div>
							<div class="span6">
								<a href="<?php echo url($product)?>" class="buy" title="<?php echo $pname?>">Koupit</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php if($i+1 == count($products)):?></div></div><?php endif?>
	<?php $i ++; endforeach; ?>
