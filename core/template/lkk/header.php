<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

?><header>
	<div id="topnav">
		<div class="wrapper">
			<?php echo menu(4)?>
		</div>
	</div>

	<div class="wrapper">
		<div id="header">
			<div id="logo">
				<a href="<?php echo url(null)?>" title="<?php echo Core::config('store_name') ?>">Nejaky text eshopu<span></span></a>
			</div>

			<div class="nav">
				<?php echo menu(1)?>
			</div>

			<?php if($page = Core::$db->page[PAGE_BASKET]): ?>
				<div id="basket" class="basket">
					<a href="<?php echo url(PAGE_BASKET)?>" title="<?php echo $page['name']?>">
						<span class="title"><?php echo $page['name']?></span>
						<?php if($count = Basket::count()): ?>
							<?php $total = Basket::total(); ?>
							<span class="summary"><strong><?php echo $count?> ks zboží: <?php echo price($total->total)?></strong></span>
						<?php else: ?>
							<span class="summary"><strong>Košík je prázdný</strong></span>
						<?php endif; ?>
					</a>
				</div>
			<?php endif?>
			<div class="reset"></div>
		</div>
	</div>
</header>