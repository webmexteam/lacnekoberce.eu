<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="block" id="block-id<?php echo $block->block['id']?>">
	<?php if($block->getTitle() != "nezobrazovat"):?>
		<div class="title">
			<span><?php echo $block->getTitle()?></span>
		</div>
	<?php endif?>
	
	<div class="content">
		<?php echo $block->getContent()?>
	</div>
</div>