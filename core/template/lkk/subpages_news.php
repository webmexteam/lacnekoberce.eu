<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul class="subpages subpages-<?php echo $position?>">
	<?php foreach($pages as $page): ?>
	<li class="photo">
		<?php if(($img = imgsrc($page, null, 1)) !== null): ?>
			<img src="<?php echo $img?>" alt="" class="picture" />
		<?php endif; ?>
		
		<a href="<?php echo url($page)?>" class="title"><?php echo $page['name']?></a><br />
		
		<span class="date"><?php echo ($page['pubdate'] ? fdate($page['pubdate']) : '')?></span>
		
		<div class="shortdesc">
			<?php echo $page['description_short']?>
		</div>
	</li>
	<?php endforeach; ?>
</ul>