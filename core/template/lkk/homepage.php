<?php $tpls = Registry::get('template_options'); ?>


<div id="carousel-top">{carousel <?php echo $tpls['lkk']['hp_carousel_id']?>}</div>



<?php $menu_pages = Core::$db->page()->where('menu', 100)->where('status', 1)->order('position ASC'); ?>
<?php $x=1;foreach($menu_pages as $menu_page):?>
	<?php 
		if(Core::config('hide_no_stock_products')) {
			$menu_products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id', $menu_page->product_pages()->select('product_id')->where('page_id', $menu_page['id']));
		}else{
			$menu_products = Core::$db->product()->where('id', $menu_page->product_pages()->select('product_id')->where('page_id', $menu_page['id']));
		}
		$menu_products->where('status', 1)->limit(5); 
	?>
		<?php if(count($menu_products)):?>
			<div class="promo-products" data-id="<?php echo $x?>"<?php if($x==1):?> style="display:block"<?php endif?>>
				<div class="row-fluid">
					<div class="span1"></div>
					<?php $i = 1;foreach($menu_products as $product): ?>
						<?php if($i >= 6) continue;?>
						<div class="span2 item">
							<a href="<?php echo url($product)?>" title="<?php echo trim($product['name'].' '.$product['nameext'])?>">
								<span class="image">
									<?php if(($img = imgsrc($product, 3, 2, $alt)) !== null): ?>
										<img src="<?php echo $img?>" alt="<?php echo trim($product['name'].' '.$product['nameext'])?>"/>
									<?php endif?>
								</span>
								<span><?php echo trim($product['name'])?></span>
							</a>
						</div>
					<?php $i++;endforeach?>
					<div class="span1"></div>
				</div>
			</div>
		<?php endif?>
<?php $x++;endforeach; ?>

<div id="doporucujeme">
	<?php echo menu(100)?>
</div>

<div id="three-boxes">
	<div class="row-fluid">
		<div class="span4">
			<div class="item">
				<h3><?php echo $tpls['lkk']['hp_box_1_title']?></h3>
				<p><?php echo $tpls['lkk']['hp_box_1_body']?></p>
				<img src="<?php echo $tpls['lkk']['hp_box_1_image']?>" alt="<?php echo $tpls['lkk']['hp_box_1_title']?>"/>
			</div>
		</div>
		<div class="span4">
			<div class="item">
				<h3><?php echo $tpls['lkk']['hp_box_2_title']?></h3>
				<p><?php echo $tpls['lkk']['hp_box_2_body']?></p>
				<img src="<?php echo $tpls['lkk']['hp_box_2_image']?>" alt="<?php echo $tpls['lkk']['hp_box_2_title']?>"/>
			</div>
		</div>
		<div class="span4">
			<div class="item">
				<h3><?php echo $tpls['lkk']['hp_box_3_title']?></h3>
				<p><?php echo $tpls['lkk']['hp_box_3_body']?></p>
				<img src="<?php echo $tpls['lkk']['hp_box_3_image']?>" alt="<?php echo $tpls['lkk']['hp_box_3_title']?>"/>
			</div>
		</div>
	</div>
</div>