jQuery(document).ready(function($) {

	/**
	 * Zmena obrzku obsahu boxu #top-box na uvodni strane
	 */
	$('#top-box .box-2.item:not(:first-child)').hide();
	$('#top-box .box-2.item:first-child').addClass('active');
	function changeTopBox() {
		var current = 1;
		var count = $('#top-box .box-2.item').length;
		var next = 2;
		$('#top-box .box-2.item').each(function(i) {
			if($(this).hasClass('active')) {
				current = i+1;
			}
		});

		$('#top-box .box-2.item.active').removeClass('active').fadeOut(function(){
			if(current == count) {
				next = 1;
			}else{
				next = current + 1;
			}
			$('#top-box .box-2.item:nth-child('+next+')').addClass('active').fadeIn();
		})
	}
	setInterval(changeTopBox, 5000);


	// Presun h1 a description an strance dolu
	if($('.page-data-here').length && $('.page-data-from').length) {
		$('.page-data-here').html($('.page-data-from').html());
		$('.page-data-from').remove();
	}

	// Schovani vice obrazku
	if($('#productdetail .files').length) {
		$('#productdetail .files li.smallpic').each(function(i){
			if(i > 2) {
				$(this).hide();
			}
			if(i == 2) {
				$(this).after('<li class="more"><a href="#">Více fotografií</a></li>')
			}
		});

		$('#productdetail .files li.more a').click(function(){
			$(this).hide();
			$('#productdetail .files li.smallpic').fadeIn();
			return false;
		});
	}

	// Dotaz na produkt
	if($('a[href="#tab-webmex-product-question"]').length) {
		$('#sharebox .write a').click(function(){
			$('a[href="#tab-webmex-product-question"]').trigger('click');
			$('html, body').animate({
				scrollTop: $("#webmex-product-question").offset().top - 100
			}, 1000);
			return false;
		});
	}else{
		$('a[href="#tab-webmex-product-question"]').remove();
	}

	var h = 0;
	$('.filter-content .feature').each(function(i){
		if($(this).height() > h) {
			h = $(this).height();
		}
		if(i+1 == $('.filter-content .feature').length) {
			$('.filter-content .feature').css('height', h);
		}
	});

	// spany na filtru vlastnosti
	$('.filter-content input:checked').each(function(){
		$(this).parent().find('span').addClass('active');
	});

	$('.filter-content input').change(function(){
		$(this).closest('.feature').find('.click').removeClass('active');
		$(this).parent().find('span').addClass('active');
	})


});

// Srovnani produktu v rade
$(window).load(function(){
	var hw = 0;
	var hh3 = 0;
	$('.products:not(".cols-1") .line').each(function(){
		var l = $(this);
		$(this).find('.product').each(function(i){
			if($(this).height() > hw) {
				hw = $(this).height();
			}
			if($(this).find('h3 a').height() > hh3) {
				hh3 = $(this).find('h3 a').height();
			}
			if(i+1 == l.find('.product').length) {
				l.find('.product').css('height', hw);
				l.find('.product h3 a').css('height', hh3);
			}
		})
	});
})