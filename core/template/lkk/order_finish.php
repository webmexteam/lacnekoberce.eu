<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('order_summary.php', array('order' => $order))?>
 
<?php echo tpl('system/analytics_order.php', array('order' => $order))?>