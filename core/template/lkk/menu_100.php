<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<ul>
	<?php $i=1;foreach($pages as $page): ?>
		<li>
			<a <?php if($i==1):?>class="active" <?php endif?>data-id="<?php echo $i?>" href="<?php echo url($page)?>"<?php echo ($page['seo_title'] ? ' title="'.$page['seo_title'].'"' : '')?>><?php echo $page['name']?></a>
		</li>
	<?php $i++;endforeach; ?>
</ul>