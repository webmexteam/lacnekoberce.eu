<?php if($page['enable_filter']): ?>

<?php 
$_order_options = array('name', 'price', 'default');
$_producers = isSet($_GET['p']) ? (array) $_GET['p'] : array();
$_features = isSet($_GET['f']) ? (array) $_GET['f'] : array();
?>

	<form action="<?php echo url(true)?>" method="get">
		<?php if($features): ?>
			<div class="filter-wrap">
				<div class="title">Vyberte si obojek podle</div>
				<div class="filter-content">
					<div class="producers clearfix">
						<div class="wrap"<?php echo ((! empty($_producers) || ! empty($_features)) ? ' style="display:block;"' : '')?>>
							<div class="features">
								<?php $i=1;foreach($features as $feature_id => $feature): ?>
									<div class="<?php echo dirify($feature['name']) ?> feature<?php if($i==count($features)):?> last<?php endif?><?php if($i==1):?> first<?php endif?>">
										<div class="head"><?php echo $feature['name']?></div>

										<?php foreach($feature['options'] as $opt_text => $opt): ?>
											<label class="cls-<?php echo dirify($opt_text) ?>">
												<span class="radio-replace realtive">
													<span class="click"></span>
													<input type="radio" name="f[<?php echo $feature_id?>]" value="<?php echo $opt['value']?>"<?php echo ((isSet($_features[$feature_id]) && $_features[$feature_id] == $opt['value']) ? ' checked="checked"' : '')?>>
												</span>
												<?php echo $opt_text?> <small>(<?php echo $opt['count']?>)</small>
											</label>
										<?php endforeach; ?>


										<?php /*<select name="f[<?php echo $feature_id?>]" id="filter-<?php echo $feature_id?>">
											<option value=""><?php echo __('all')?></option>
											<?php foreach($feature['options'] as $opt_text => $opt): ?>
												<option value="<?php echo $opt['value']?>"<?php echo ((isSet($_features[$feature_id]) && $_features[$feature_id] == $opt['value']) ? ' selected="selected"' : '')?>><?php echo $opt_text?> (<?php echo $opt['count']?>)</option>
											<?php endforeach; ?>
										</select>*/?>
									</div>
								<?php $i++;endforeach;?>
								<div class="reset"></div>
							</div>
							<div class="btns">
								<button type="submit" class="btn btn-6"><?php echo __('filter_products')?></button>
								<div class="reset"></div>
								<?php if($features): ?>
									<a href="<?php echo url($page)?>" class="button"><?php echo __('filter_reset')?></a>
								<?php endif; ?>
							</div>
						</div>
						<div class="reset"></div>
					</div>
					<div class="reset"></div>
				</div>
			</div>
		<?php endif; ?>

		<div class="hidefilter">
			<?php if(isSet($_GET['uri']) && ! Core::$use_htaccess): ?>
				<input type="hidden" name="uri" value="<?php echo $_GET['uri']?>" />
			<?php endif; ?>

			<?php if(isSet($_GET['q'])): ?>
				<input type="hidden" name="q" value="<?php echo $_GET['q']?>" />
			<?php endif; ?>

			<input type="hidden" name="dir-<?php echo strtolower($_order_dir)?>" value="1" />
			<input type="hidden" name="view-<?php echo strtolower($_view)?>" value="1" />

			<?php echo __('order_by')?>:
			<select name="order">
				<?php foreach($_order_options as $opt): ?>
					<option value="<?php echo $opt?>"<?php echo ($opt == $_order ? ' selected="selected"' : '')?>><?php echo __($opt)?></option>
				<?php endforeach; ?>
			</select>

			<input type="submit" name="dir-asc" value="" class="dir dir-asc<?php echo ($_order_dir == 'ASC' ? ' active' : '')?>" />
			<input type="submit" name="dir-desc" value="" class="dir dir-desc<?php echo ($_order_dir == 'DESC' ? ' active' : '')?>" />
		</div>
	</form>

<div class="tabs sorting">
	<ul>
		<li class="info">Řadit:</li>
		<li><a id="sort-default" class="active" href="#">Výchozí</a></li>
		<li><a id="sort-nejlevnejsi" href="">Od nejlevnějšího</a></li>
		<li><a id="sort-nejdrazsi" href="">Od nejdražšího</a></li>
		<li><a id="sort-nazev-ses" href="">Podle názvu A-Z</a></li>
		<li><a id="sort-nazev-vzes" href="">Podle názvu Z-A</a></li>
	</ul>
</div>

<script>
	jQuery(document).ready(function($) {
		var price = $('select[name="order"] option[value="price"]');
		var price_sel = price.attr('selected');

		var name = $('select[name="order"] option[value="name"]');
		var name_sel = name.attr('selected');

		if(typeof price_sel !== 'undefined' && price_sel !== false) {
			$('.sorting a').removeClass('active');
			if($('input.dir.dir-desc').hasClass('active')){
				$('#sort-nejdrazsi').addClass('active');
			}else{
				$('#sort-nejlevnejsi').addClass('active');
			}
		}else if(typeof name_sel !== 'undefined' && name_sel !== false) {
			$('.sorting a').removeClass('active');
			if($('input.dir.dir-desc').hasClass('active')){
				$('#sort-nazev-vzes').addClass('active');
			}else{
				$('#sort-nazev-ses').addClass('active');
			}
		}else{
			$('.sorting a').removeClass('active');
			$('#sort-default').addClass('active');
		}

		$('.sorting a').click(function(){

			if($(this).attr('id') == "sort-default"){
				$('select[name="order"] option').removeAttr('selected');
				$('select[name="order"] option[value="default"]').attr('selected', 'selected');
				$('input.dir.dir-desc').trigger('click');
				return false;
			}

			if($(this).attr('id') == "sort-nazev-ses"){
				$('select[name="order"] option').removeAttr('selected');
				$('select[name="order"] option[value="name"]').attr('selected', 'selected');
				$('input.dir.dir-asc').trigger('click');
				return false;
			}

			if($(this).attr('id') == "sort-nazev-vzes"){
				$('select[name="order"] option').removeAttr('selected');
				$('select[name="order"] option[value="name"]').attr('selected', 'selected');
				$('input.dir.dir-desc').trigger('click');
				return false;
			}

			if($(this).attr('id') == "sort-nejlevnejsi"){
				$('select[name="order"] option').removeAttr('selected');
				$('select[name="order"] option[value="price"]').attr('selected', 'selected');
				$('input.dir.dir-asc').trigger('click');
				return false;
			}

			if($(this).attr('id') == "sort-nejdrazsi"){
				$('select[name="order"] option').removeAttr('selected');
				$('select[name="order"] option[value="price"]').attr('selected', 'selected');
				$('input.dir.dir-desc').trigger('click');
				return false;
			}

			return false;
		});

		function getParameter(paramName) {
			var searchString = window.location.search.substring(1),
				i, val, params = searchString.split("&");

			for (i=0;i<params.length;i++) {
				val = params[i].split("=");
				if (val[0] == paramName) {
					return unescape(val[1]);
				}
			}
			return null;
		}

		if(getParameter('order') || getParameter('page')) {
			$('html, body').animate({
				scrollTop: $("#slide-here-in-sorting").offset().top - 100
			}, 1000);
		}
	});
</script>

<?php endif; ?>