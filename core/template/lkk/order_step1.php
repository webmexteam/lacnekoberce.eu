<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');
?>

<form action="<?php echo url(PAGE_ORDER)?>" method="post" class="form order clearfix">

	<div class="steps">
		<div class="step step1 complete">
			<div class="num">1</div>
			<div class="text">Nákupní košík</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2 current">
			<div class="num">2</div>
			<div class="text">Doprava a platba</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2">
			<div class="num">3</div>
			<div class="text">Dodací údaje</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2">
			<div class="num">4</div>
			<div class="text">Souhrn objednávky</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span8">
			<?php if(isSet($order_errors)): ?>
				<div class="errors">
					<ul>
						<?php foreach($order_errors as $error): ?>
							<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

			<fieldset class=" delivery">
				<legend><?php echo __('delivery')?></legend>

				<script>
					var delivery = {};
				</script>

				<?php if($deliveries): foreach($deliveries as $delivery): ?>
					<script>
						delivery[<?php echo $delivery['id']?>] = '<?php echo $delivery['price']?>';
					</script>

					<div class="input">
						<label for="delivery_<?php echo $delivery['id']?>">
							<input type="radio" name="delivery" id="delivery_<?php echo $delivery['id']?>" value="<?php echo $delivery['id']?>" class="checkbox" />
							<strong><?php echo $delivery['name']?></strong>
						</label>
					</div>
				<?php endforeach; endif; ?>
			</fieldset>

			<fieldset class=" payment">
				<legend><?php echo __('payment')?></legend>

				<script>
					var payment = {};
				</script>

				<div class="help" id="payment_help"><?php echo __('payment_help')?></div>

				<?php if($payments): foreach($payments as $payment): ?>
					<script>
						payment[<?php echo $payment['id']?>] = '<?php echo $payment['price']?>';
					</script>

					<div class="input">
						<label for="payment_<?php echo $payment['id']?>">
							<span class="row-fluid">
								<span class="span9">
									<input type="radio" name="payment" id="payment_<?php echo $payment['id']?>" value="<?php echo $payment['id']?>" class="checkbox" />
									<strong><?php echo $payment['name']?></strong>
								</span>
								<span class="span3">
									<span class="price">120 Kc</span>
								</span>
							</span>

						</label>

					</div>
				<?php endforeach; endif; ?>
			</fieldset>

			<script>
				var delivery_payments = [];
				<?php foreach(Core::$db->delivery_payments() as $dp):
					$dp_data = $dp->as_array();
				?>
				delivery_payments.push(<?php echo json_encode($dp_data)?>);
				<?php endforeach; ?>
			</script>
		</div>
		<div class="span4 ordersidebar">
			<div class="title help">Pomoc s objednávkou</div>
			<div id="help-with-order">
				<?php if(Core::config('email_notify')):?><div class="email"><?php echo Core::config('email_notify')?></div><?php endif?>
			</div>
			<div class="basket">
				<div class="title">Nákupní košík</div>
				<div class="tablewrap last">
					<table>
						<tbody>
						<?php if($basket_products === null || ! count($basket_products)): ?>
							<tr>
								<td colspan="3" class="basketempty"><?php echo __('basket_is_empty')?></td>
							</tr>
						<?php endif; ?>

						<?php foreach($basket_products as $product): ?>
							<tr>
								<td class="name">
									<?php echo $product['product']['name'].' '.$product['product']['nameext']?>

									<?php if($product['attributes']): ?>
										<br /><span class="attr"><?php echo $product['attributes']?></span>
									<?php endif; ?>
								</td>
								<td class="price price_qty"><?php echo $product['qty']?>&times;</td>
								<td class="total"><?php echo str_replace(' ', '&nbsp;', price_vat(array($product['product'], $product['price'] * $product['qty'])))?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>

					<hr>

					<table class="dp">
						<tr>
							<td><strong><?php echo __('delivery_payment')?>:</strong></td>
							<td><strong id="deliverypayment">&mdash;</strong></td>
						</tr>
						<tr>
							<td><?php echo __('delivery')?></td>
							<td id="delivery-value">&mdash;</td>
						</tr>
						<tr>
							<td><?php echo __('payment')?></td>
							<td id="payment-value">&mdash;</td>
						</tr>
					</table>
				</div>

				<div class="tablewrap final">
					<table>
						<?php if($basket_products !== null && count($basket_products)):
							$total = Basket::total();
							?>
							<tfoot>

							<?php if($voucher = Basket::voucher()): ?>
								<tr class="voucher">
									<td class="label" colspan="2"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
									<td class="value"><span id="discount"><?php echo '-'.price($total->discount)?></span></td>
								</tr>
							<?php endif; ?>

							<tr class="total">
								<td class="label" colspan="2"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
								<td class="value"><span id="total"><?php echo str_replace(' ', '&nbsp;', price($total->total))?></span></td>
							</tr>

							<?php if((int) Core::config('vat_payer')): ?>
								<tr class="subtotal">
									<td class="label" colspan="2"><?php echo __('total_excl_vat')?>:</td>
									<td class="value"><span id="subtotal"><?php echo price($total->subtotal)?></span></td>
								</tr>
							<?php endif; ?>

							</tfoot>
						<?php endif; ?>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="buttons clearfix">
		<a href="<?php echo url(PAGE_BASKET)?>" class="button text btn btn-7"><span>Zpět do košíku</span></a>
		<button type="submit" name="submit_order" class="button text checkout btn btn-4">Pokračovat &rsaquo;</button>
		<div class="reset"></div>
	</div>

	<input type="hidden" name="dp_price">
	<input type="hidden" name="total_price">
</form>

<script>
	var _subtotal_excl_vat = <?php echo $total->subtotal?>;
	var _subtotal = <?php echo $total->_total_before_discount?>;
	
	<?php if($voucher): ?>
	var _voucher_value = '<?php echo $voucher['value']?>';
	<?php endif; ?>

	$(document).ready(function(){
		$('.delivery input[type="radio"]').click(function(){
			$('.delivery .input.active').removeClass('active');
			$(this).closest('.input').addClass('active');
			$('#delivery-value').html($(this).parent().text());
		});

		$('.payment input[type="radio"]').click(function(){
			$('.payment .input.active').removeClass('active');
			$(this).closest('.input').addClass('active');
			$('#payment-value').html($(this).parent().text());
		});
	});
</script>