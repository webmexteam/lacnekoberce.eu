<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<ul>
	<?php foreach($pages as $page): ?>
	<li class="<?php echo (in_array($page['id'], Core::$active_pages) ? ' active' : '')?><?php if(count($subpages = Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)->order('position ASC'))): ?> has-submenu<?php endif?>">
		<a href="<?php echo url($page)?>"<?php echo ($page['seo_title'] ? ' title="'.$page['seo_title'].'"' : '')?>><span><?php echo $page['name']?></span></a>
	</li>
	<?php endforeach; ?>
</ul>