<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>

<div class="row-fluid">
	<div class="span12">
		<?php echo $content?>
	</div>
</div>