<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>

<div class="row-fluid">
	<div class="span3 sidebar" id="sidebar">
		<?php echo blocks('left')?>
		<?php echo blocks('right')?>
	</div>
	<div class="span9 main">
		<?php echo $content?>
	</div>
</div>