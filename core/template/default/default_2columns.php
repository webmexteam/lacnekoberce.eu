<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<div class="column columns2" id="col2">
	<div class="contentwrap">
		<div class="main">
			<?php echo $content?>
		</div>
	</div>
</div><!--! end of #col2-->

<div class="column" id="col1">
	<?php echo blocks('left')?>
</div><!--! end of #col1-->