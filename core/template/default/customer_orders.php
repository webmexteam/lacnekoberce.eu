<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php foreach($orders as $order): 
if($order['delivery_id']){
	$delivery_driver = 'Delivery_'.ucfirst($order->delivery['driver']);
}

$delivery = null;
if($order['track_num'] && isSet($delivery_driver) && class_exists($delivery_driver)){
	$delivery = new $delivery_driver($order);		
}
?>
<div class="cust-order">
	<div class="head">
		<span><?php echo fdate($order['received'], true)?></span>
		<a href="<?php echo url('customer/order/'.$order['id'])?>"><?php echo $order['id']?></a>
	</div>
	
	<span class="status"><?php echo __('status_'.Core::$def['order_status'][$order['status']])?></span>
	
	<p><?php echo __('total')?>: <strong><?php echo price($order['total_incl_vat'], $order['currency'])?></strong></p>
	<p><?php echo __('recipient')?>: <?php echo ($order['ship_street'] ? $order['ship_first_name'].' '.$order['ship_last_name'] : $order['first_name'].' '.$order['last_name'])?></p>
	
	<div class="bbar">
	<?php if($order['track_num'] && $delivery): ?>
		<a href="<?php echo $delivery->getTrackLink()?>" target="_blank" class="tracklink"><?php echo __('track_package')?></a> | 
	<?php endif; ?>
	
	<?php if($invoice = Core::$db->invoice()->where('order_id', $order['id'])->fetch()): ?>
		<a href="<?php echo url('customer/invoice/'.$invoice['id'])?>" class="invoice"><?php echo __('invoice')?></a> |
	<?php endif; ?>
	
	<a href="<?php echo url('customer/order/'.$order['id'])?>"><?php echo __('detail')?></a>
	</div>
</div>
<?php endforeach; ?>

<?php echo pagination($total_count, null, $limit)?>