<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="pages">
	<?php if($block->config['view'] == 'list'): ?>
		<?php echo tpl('blocks/pages_ul.php', array('pages' => $pages, 'block' => $block, 'expanded' => $expanded))?>
	<?php else: ?>
	<div class="pages-select">
		<select name="page" onchange="window.location.href=this.value">
			<?php echo tpl('blocks/pages_select.php', array('pages' => $pages, 'block' => $block, 'expanded' => $expanded))?>
		</select>
	</div>
	<?php endif; ?>
</div>