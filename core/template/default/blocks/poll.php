<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="poll">
	<h4><?php echo $block->config['question']?></h4>
	
	<ul>
		<?php 
		$total = $block->getTotalVotes();
		
		foreach($block->config['answers'] as $i => $answer): 
			$votes = (int) $answer['votes'];
			
			if(! $answer['text']){
				continue;
			}	
		
			$percent = $votes > 0 ? round(100 / $total * (int) $answer['votes']) : 0;
		?>
		<li>
			<?php if($disabled): ?>
			<strong><?php echo $answer['text']?></strong> (<?php echo $percent?>%)<br />
			<?php else: ?>
			<a href="<?php echo url(true, array('poll' => $block->block['id'], 'answer' => $i))?>" rel="nofollow"><?php echo $answer['text']?></a> (<?php echo $percent?>%)<br />
			<?php endif; ?>
			
			<div class="bar"><div style="width:<?php echo $percent?>%"></div></div>
		</li>
		<?php endforeach; ?>
	</ul>
	
	<span class="total"><?php echo __('total_votes')?>: <?php echo $total?></span>
</div>