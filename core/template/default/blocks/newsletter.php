<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><form action="<?php echo url('newsletter')?>" method="post" class="block-newsletter">
	<fieldset>
		<label for="newsletter_email"><?php echo __('enter_your_email')?>:</label>
		<input type="text" name="newsletter_email" id="newsletter_email" value="@" class="text" />
	</fieldset>
	
	<button type="submit" name="signup" class="button"><?php echo __('newsletter_signup')?></button>
</form>