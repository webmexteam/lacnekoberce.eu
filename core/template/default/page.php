<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('toplinks.php', array('page' => $page))?>
 
<?php echo breadcrumb()?>

<!-- top files -->
<?php echo listFiles($page, 1)?>

<!-- right files -->
<?php echo listFiles($page, 3)?>

<!-- left files -->
<?php echo listFiles($page, 2)?>

<h1><?php echo (trim($page['seo_title']) ? $page['seo_title'] : $page['name'])?></h1>

<?php if($page['description_short']): ?>
<div class="shortdesc">
	<?php echo $page['description_short']?>
</div>
<?php endif; ?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 2): ?>
	<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
		'pages' => $subpages,
		'position' => 'top'
	))?>
<?php endif; ?>

<?php echo $page['description'].$page_file_content?>

<!-- non-image files -->
<?php echo listFiles($page)?>

<!-- bottom files -->
<?php echo listFiles($page, 0)?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 1): ?>
	<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
		'pages' => $subpages,
		'position' => 'bottom'
	))?>
<?php endif; ?>

<?php echo $page_action_top?>

<?php if(count($products) || ! empty($_GET['f'])): ?>
	<?php echo tpl('products/'.$product_tpl, array('products' => $products, 'page' => $page, 'producers' => $producers, 'features' => $features))?>
	
	<?php if(count($products)): ?>
	<?php echo pagination($products_count)?>
	<?php elseif(! empty($_GET['p']) || ! empty($_GET['f'])): ?>
	<p class="no-products-found"><?php echo __('no_products_found')?></p>
	<?php endif; ?>
<?php endif; ?>

<?php echo $page_action?>

<?php echo tpl('bottomlinks.php')?>