<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php if(isSet($register)): ?>
<h1><?php echo __('register_customer')?></h1>
<?php endif; ?>

<form action="<?php echo url(true)?>" method="post" class="form customer clearfix">

	<?php if(isSet($customer_errors)): ?>
	<div class="errors">
		<ul>
		<?php foreach($customer_errors as $error): ?>
			<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
	
	<div class="clearfix">
		<fieldset class="colleft">
			<legend><?php echo __('billing_address')?></legend>
			
			<div class="clearfix">
				<div class="input inline required">
					<label for="first_name"><?php echo __('first_name')?>:</label>
					<input type="text" name="first_name" id="first_name" value="<?php echo $customer['first_name']?>" class="text" />
				</div>
				
				<div class="input inline inline-right required">
					<label for="last_name"><?php echo __('last_name')?>:</label>
					<input type="text" name="last_name" id="last_name" value="<?php echo $customer['last_name']?>" class="text" />
				</div>
			</div>
			
			<div class="input">
				<label for="company"><?php echo __('company')?>:</label>
				<input type="text" name="company" id="company" value="<?php echo $customer['company']?>" class="text" />
			</div>
			
			<div class="clearfix">
				<div class="input inline">
					<label for="company_id"><?php echo __('company_id')?>:</label>
					<input type="text" name="company_id" id="company_id" value="<?php echo $customer['company_id']?>" class="text" />
				</div>
				
				<div class="input inline inline-right">
					<label for="company_vat"><?php echo __('company_vat')?>:</label>
					<input type="text" name="company_vat" id="company_vat" value="<?php echo $customer['company_vat']?>" class="text" />
				</div>
			</div>
			
			<div class="help"><?php echo __('company_help')?></div>
			
			<div class="input required">
				<label for="street"><?php echo __('street')?>:</label>
				<input type="text" name="street" id="street" value="<?php echo $customer['street']?>" class="text" />
			</div>
			
			<div class="clearfix">
				<div class="input inline required">
					<label for="city"><?php echo __('city')?>:</label>
					<input type="text" name="city" id="city" value="<?php echo $customer['city']?>" class="text" />
				</div>
				
				<div class="input inline inline-right required">
					<label for="zip"><?php echo __('zip')?>:</label>
					<input type="text" name="zip" id="zip" value="<?php echo $customer['zip']?>" class="text short" />
				</div>
				
				<div class="input inline required">
					<label for="country"><?php echo __('country')?>:</label>
					<select name="country" id="country">
					<?php foreach(Core::$db->country() as $country): ?>
						<option value="<?php echo $country['name']?>"<?php echo (($customer && $customer['country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>
		
		</fieldset>
		
		<fieldset class="colright">
			<legend><?php echo __('shipping_address')?></legend>
			
			<div class="input">
				<input type="checkbox" name="shipping_as_billing" id="shipping_as_billing" value="1" class="checkbox"<?php echo ($customer['ship_street'] ? '' : 'checked="checked"')?> /> 
				<label for="shipping_as_billing"><?php echo __('shipping_as_billing')?></label> 
			</div>
			
			<div id="shipping_address">
				<div class="clearfix">
					<div class="input inline required">
						<label for="ship_first_name"><?php echo __('first_name')?>:</label>
						<input type="text" name="ship_first_name" id="ship_first_name" value="<?php echo $customer['ship_first_name']?>" class="text" />
					</div>
					
					<div class="input inline inline-right required">
						<label for="ship_last_name"><?php echo __('last_name')?>:</label>
						<input type="text" name="ship_last_name" id="ship_last_name" value="<?php echo $customer['ship_last_name']?>" class="text" />
					</div>
				</div>
				
				<div class="input">
					<label for="ship_company"><?php echo __('company')?>:</label>
					<input type="text" name="ship_company" id="ship_company" value="<?php echo $customer['ship_company']?>" class="text" />
				</div>
				
				<div class="input required">
					<label for="ship_street"><?php echo __('street')?>:</label>
					<input type="text" name="ship_street" id="ship_street" value="<?php echo $customer['ship_street']?>" class="text" />
				</div>
				
				<div class="clearfix">
					<div class="input inline required">
						<label for="ship_city"><?php echo __('city')?>:</label>
						<input type="text" name="ship_city" id="ship_city" value="<?php echo $customer['ship_city']?>" class="text" />
					</div>
					
					<div class="input inline inline-right required">
						<label for="ship_zip"><?php echo __('zip')?>:</label>
						<input type="text" name="ship_zip" id="ship_zip" value="<?php echo $customer['ship_zip']?>" class="text short" />
					</div>
					
					<div class="input inline required">
						<label for="ship_country"><?php echo __('country')?>:</label>
						<select name="ship_country" id="ship_country">
						<?php foreach(Core::$db->country() as $country): ?>
							<option value="<?php echo $country['name']?>"<?php echo (($customer && $customer['ship_country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
						<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
	
	<div class="clearfix">
		<fieldset class="colleft">
			<legend><?php echo __('contact_info')?></legend>
			
			<div class="clearfix">
				<div class="input inline required">
					<label for="email"><?php echo __('email')?>:</label>
					<input type="text" name="email" id="email" value="<?php echo $customer['email']?>" class="text"<?php echo (isSet($register) ? '' : ' disabled="disabled"')?> />
				</div>
				
				<div class="input inline inline-right required">
					<label for="phone"><?php echo __('phone')?>:</label>
					<input type="text" name="phone" id="phone" value="<?php echo $customer['phone']?>" class="text" />
				</div>
			</div>
			
			<div class="input">
				<input type="checkbox" name="newsletter" id="newsletter" value="1" class="checkbox"<?php echo ($has_newsletter ? ' checked="checked"' : '')?> /> 
				<label for="newsletter"><?php echo __('want_to_receive_newsletter')?></label> 
			</div>
		</fieldset>
		
		<?php if(! isSet($register) || ! (int) Core::config('customer_confirmation')): ?>
		<fieldset class="colright">
			<legend><?php echo __('password')?></legend>
			
			<?php if(! isSet($register)): ?> 
			<div class="clearfix">
				<div class="input inline">
					<label for="old_password"><?php echo __('current_password')?>:</label>
					<input type="password" name="old_password" id="old_password" value="" class="text" autocomplete="off" />
				</div>
			</div>
			
			<div class="clearfix">
				<div class="input inline">
					<label for="password1"><?php echo __('new_password')?>:</label>
					<input type="password" name="password1" id="password1" value="" class="text" autocomplete="off" />
				</div>
				
				<div class="input inline inline-right">
					<label for="password2"><?php echo __('new_password_again')?>:</label>
					<input type="password" name="password2" id="password2" value="" class="text" autocomplete="off" />
				</div>
			</div>
			
			<?php else: ?>
			<div class="clearfix">
				<div class="input inline required">
					<label for="password1"><?php echo __('password')?>:</label>
					<input type="password" name="password1" id="password1" value="" class="text" autocomplete="off" />
				</div>
				
				<div class="input inline inline-right required">
					<label for="password2"><?php echo __('password_again')?>:</label>
					<input type="password" name="password2" id="password2" value="" class="text" autocomplete="off" />
				</div>
			</div>
			<?php endif; ?>
		
		</fieldset>
		<?php endif; ?>
	</div>
	
	<div class="buttons">
		<button type="submit" name="save" class="button"><?php echo __(isSet($register) ? 'register' : 'save')?> &raquo;</button>
	</div>
</form>