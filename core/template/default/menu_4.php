<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul>
	<?php foreach($pages as $page): ?>
	<li><a href="<?php echo url($page)?>"><?php echo $page['name']?></a></li>
	<?php endforeach; ?>
	<?php if(Customer::$logged_in): ?>
		<li><a href="<?php echo url('customer')?>" class="ico-user">Můj účet</a></li>
		<li><a href="<?php echo url('customer/logout')?>" class="ico-lock">Odhlásit se</a></li>
	<?php else: ?>
		<li><a href="<?php echo url('customer/login')?>" class="ico-user">Přihlášení</a></li>
		<li><a href="<?php echo url('customer/register')?>" class="ico-lock">Registrace</a></li>
	<?php endif; ?>
</ul>