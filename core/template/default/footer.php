<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div id="footer" class="clearfix">
	<div class="wrap">
		<?php if(! Core::$is_premium || ! (int) Core::config('hide_webmex_link')): ?>
		<span class="powered">Powered by <a href="http://www.webmex.cz/" target="_blank">WEBMEX</a></span>
		<?php endif; ?>
		
		<?php if($footer = Core::config('footer')): ?>
			<?php echo preg_replace('/\{year\}/', date('Y'), $footer)?>
			
		<?php else: ?>
			Copyright &copy; <?php echo date('Y').' '.Core::config('store_name')?>
		<?php endif; ?>
	</div>
</div>
<?php echo (Core::$profiler ? tpl('system/profiler.php') : '')?>