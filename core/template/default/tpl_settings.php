<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
echo forminput('select', 'opt[default][show_text_logo]', $options['default']['show_text_logo'], array('label' => __('show_text_logo'), 'options' => array(
	'0' => __('no'),
	'1' => __('yes')
)))
.forminput('select', 'opt[default][show_font_size]', $options['default']['show_font_size'], array('label' => __('show_font_size'), 'options' => array(
	'0' => __('no'),
	'1' => __('yes')
)));