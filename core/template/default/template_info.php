<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$template = array(
	'name' => 'Default',
	'version' => '1.2',
	'author' => 'Webmex',
	'www' => 'http://www.webmex.cz'
);