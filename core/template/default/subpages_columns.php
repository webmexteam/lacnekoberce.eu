<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$per_column = ceil(count($pages) / 3);
?>
<div class="subpages clearfix">
	<ul class="subpages-column subpages-column-first subpages-<?php echo $position?>">
		<?php $i = 1; foreach($pages as $page): ?>
		<li>
			<a href="<?php echo url($page)?>" class="title"><?php echo $page['name']?></a><br />
		</li>
		
		<?php if($i % $per_column === 0): ?>
		</ul><ul class="subpages-column subpages-<?php echo $position?>">
		<?php endif; ?>
		
		<?php $i ++; endforeach; ?>
	</ul>
</div>