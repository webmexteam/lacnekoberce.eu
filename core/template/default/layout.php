<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('layout_head.php'); ?>

	<div id="drop-shadow">
		<div id="container">
			<?php echo tpl('header.php'); ?>

			<div id="main" class="clearfix">

				<?php echo $content?>

			</div><!--! end of #main-->

			<?php echo tpl('footer.php'); ?>
		</div>
	</div>

<?php echo tpl('layout_foot.php'); ?>