<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="columns1">
	<div class="column columns1" id="col2">
		<div class="contentwrap">
			<div class="main">
				<?php echo $content?>
			</div>
		</div>
	</div><!--! end of #col2-->
</div>