<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<form action="<?php echo url(PAGE_ORDER, array('payment' => 1, 'id' => $order['id'], 'hash' => $order->model->hash()))?>" method="post" class="form order clearfix">

	<?php if(isSet($order_errors)): ?>
	<div class="errors">
		<ul>
		<?php foreach($order_errors as $error): ?>
			<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
	
	<h3><?php echo __('order').' #'.$order['id'].' ('.$order['first_name'].' '.$order['last_name'].')'?></h3>
	
	<div class="clearfix">
		<fieldset class="colleft delivery">
			<legend><?php echo __('delivery')?></legend>
			
			<script>
				var delivery = {};
			</script>
			
			<?php if($deliveries): foreach($deliveries as $delivery): ?>
			<script>
				delivery[<?php echo $delivery['id']?>] = '<?php echo $delivery['price']?>';
			</script>
			
			<div class="input">
				<input type="radio" name="delivery" id="delivery_<?php echo $delivery['id']?>" value="<?php echo $delivery['id']?>" class="checkbox" /> 
				<label for="delivery_<?php echo $delivery['id']?>"><strong><?php echo $delivery['name']?></strong></label> 
			</div>
			<?php endforeach; endif; ?>
		</fieldset>
		
		<fieldset class="colright payment">
			<legend><?php echo __('payment')?></legend>
			
			<script>
				var payment = {};
			</script>
			
			<div class="help" id="payment_help"><?php echo __('payment_help')?></div>
			
			<?php if($payments): foreach($payments as $payment): ?>
			<script>
				payment[<?php echo $payment['id']?>] = '<?php echo $payment['price']?>';
			</script>
			
			<div class="input">
				<input type="radio" name="payment" id="payment_<?php echo $payment['id']?>" value="<?php echo $payment['id']?>" class="checkbox" /> 
				<label for="payment_<?php echo $payment['id']?>"><strong><?php echo $payment['name']?></strong> <span class="price">120 Kc</span></label> 
			</div>
			<?php endforeach; endif; ?>
		</fieldset>
		
		<script>
			var delivery_payments = [];
			<?php foreach(Core::$db->delivery_payments() as $dp): 
				$dp_data = $dp->as_array();
			?>
			delivery_payments.push(<?php echo json_encode($dp_data)?>);
			<?php endforeach; ?>
		</script>
	</div>
	
	<div class="basket">
		<div class="tablewrap">
			<table>
				<tfoot>
					<tr class="order_total">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __('order')?>:</td>
						<td class="value"><span id="order_total"><?php echo price($order_total, $order['currency'])?></span></td>
					</tr>
					<tr class="deliverypayment">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __('delivery_payment')?>:</td>
						<td class="value"><span id="deliverypayment">&mdash;</span></td>
					</tr>
					
					<?php if($voucher): ?>
					<tr class="voucher">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
						<td class="value"><span id="discount"><?php echo '-'.price($discount, $order['currency'])?></span></td>
					</tr>
					<?php endif; ?>
					
					<tr class="total">
						<td>&nbsp;</td>
						<td class="label" colspan="2"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
						<td class="value"><span id="total"><?php echo price($total, $order['currency'])?></span></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	
	<div class="buttons">
		<button type="submit" name="submit_order" class="button"><?php echo __('next')?> &raquo;</button>
	</div>
	
</form>

<script>
	var _subtotal_excl_vat = <?php echo $subtotal?>;
	var _subtotal = <?php echo $order_total?>;
	_currency = '<?php echo $order['currency']?>';
	
	<?php if($voucher): ?>
	var _voucher_value = '<?php echo $voucher['value']?>';
	<?php endif; ?>
</script>