<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div id="toplinks">	
	<?php if(isSet($page) && $page['rss'] && count(Core::$db->page()->where('parent_page', $page['id'])->where('status', 1))): ?>
	<a href="<?php echo url('rss/page/'.$page['id'], null, true)?>" class="rss"><?php echo __('rss')?></a>
	<?php endif; ?>
	
	<?php if(! isSet($_options['default']['show_font_size']) || $_options['default']['show_font_size']): ?>
	<a href="#small" class="fontsize fssmall" title="<?php echo __('fontsize_small')?>">A</a>
	<a href="#normal" class="fontsize fsnormal" title="<?php echo __('fontsize_normal')?>">A</a>
	<a href="#big" class="fontsize fsbig" title="<?php echo __('fontsize_big')?>">A</a>
	<?php else: ?>
	&nbsp;
	<?php endif; ?>
</div>