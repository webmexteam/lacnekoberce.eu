<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><header>
	<div id="topnav">
		<div class="wrapper">
			<?php echo menu(4)?>
		</div>
	</div>



	<div id="topnav">
		<div class="wrapper">
			<?php echo menu(4)?>
		</div>
	</div>

	<div id="topnav" class="clearfix">
		<div class="wrap">
		<?php if($page = Core::$db->page[PAGE_BASKET]): ?>
			<?php if($count = Basket::count()): ?>
			<div class="checkout">
				<a href="<?php echo url(PAGE_BASKET)?>"><?php echo __('checkout')?> &rsaquo;</a>
			</div>
			<?php endif; ?>
		
			<div class="cart">
				<a href="<?php echo url(PAGE_BASKET)?>">
					<span class="t"><?php echo $page['name']?></span>
					
					<?php if($count = Basket::count()): ?>
					<small class="cart-items"><?php echo __('products_in_basket')?>: <strong><?php echo $count?></strong></small>
					<?php else: ?>
					<small class="cart-items"><?php echo __('basket_is_empty')?></small>
					<?php endif; ?>
				</a>
			</div>
		<?php endif; ?>
		
		<?php echo menu(4)?>
		</div>
	</div><!--! end of #topnav-->
	
	<div id="banner">
		<div class="wrap">
			<div class="logo">
				<a href="<?php echo url(null)?>" class="logo"><?php echo ((! isSet($_options['default']['show_text_logo']) || $_options['default']['show_text_logo']) ? Core::config('store_name') : '')?><span class="logo-img"></span></a>
			</div>
			
			<div class="search">
				<form action="<?php echo url(PAGE_SEARCH)?>" method="get">
					<?php if(Core::$fix_path && ($search_page = Core::$db->page[PAGE_SEARCH])): ?>
					<input type="hidden" name="uri" value="<?php echo $search_page['sef_url'].'-a'.$search_page['id']?>" />
					<?php endif; ?>
					
					<fieldset>
						<label for="search-q"><?php echo __('search_label')?>:</label>
						<input type="text" name="q" id="search-q" value="" />
						<button type="submit" class="button"><?php echo __('search')?></button>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!--! end of #banner-->
	
	<div id="mainnav">
		<div class="wrap">
			<?php echo menu(1)?>
		</div>
	</div><!--! end of #mainnav-->
	
</header>