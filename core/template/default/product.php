<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('toplinks.php')?>
 
<?php echo breadcrumb($product)?>

<div class="productdetail<?php echo ((int) $product['promote'] ? ' productdetail-promote' : '')?>">
	<div class="clearfix">
		
		<!-- top files -->
		<?php echo listFiles($product, 1)?>
		
		<!-- right files -->
		<?php echo listFiles($product, 3, 3)?>
		
		<!-- left files -->
		<?php echo listFiles($product, 2, 'product')?>
		
		<div class="producthead clearfix">
			<h1 class="name"><?php echo trim($product['name'].' '.$product['nameext'])?></h1>
			
			<?php if($product['show_sku'] && $product['sku']): ?>
				<span class="sku"><?php echo __('sku')?>: <?php echo $product['sku']?></span>
			<?php endif; ?>
			
			<?php if($product['show_ean13'] && $product['ean13']): ?>
				<span class="ean"><?php echo __('ean')?>: <?php echo $product['ean13']?></span>
			<?php endif; ?>
			
			<?php if(($labels = $product->product_labels()) && count($labels)): ?>
			<div class="labels">
				<?php foreach($labels as $label): if($label->label['name']): ?>
				<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
				<?php endif; endforeach; ?>
			</div>
			<?php endif; ?>
			
			<ul class="pages">
				<?php foreach($product->product_pages() as $page): ?>
					<li><?php echo ($page->page['menu'] == 3 ? '<span class="manufacturer">'.__('manufacturer').':</span>' : '').pageFullPath($page->page, true)?></li> 
				<?php endforeach; ?>
			</ul>
			
			<?php if($product['description_short']): ?>
			<div class="shortdesc">
				<?php echo $product['description_short']?>
			</div>
			<?php endif; ?>
			
			<div class="productinfo">
				<ul>
					<?php if(SHOW_PRICES && $product['price'] !== null && $product['vat']): ?>
						<li class="price"><span><?php echo __('price_incl_vat')?>:</span><strong id="product-price"><?php echo price_vat(array($product, $product['price']+$product['recycling_fee']))?></strong></li>
						<li class="priceexclvat"><span><?php echo __('price_excl_vat')?>:</span><em id="product-price-excl-vat"><?php echo price_unvat(array($product, $product['price']+$product['recycling_fee']))?></em></li>
						
					<?php elseif(SHOW_PRICES && $product['price'] !== null): ?>
						<li class="price"><span><?php echo __('price')?>:</span><strong id="product-price"><?php echo price($product['price'] + $product['recycling_fee'])?></strong></li>
					<?php endif; ?>
					
					<?php if($product['discount']): ?>
						<li class="discount"><span><?php echo __('discount')?>:</span><strong><?php echo (float) $product['discount']?> %</strong></li>
					<?php endif; ?>
					
					<?php if(SHOW_PRICES && ((float) $product['price_old'] || $product['discount'])): ?>
						<li class="oldprice"><span><?php echo __('old_price')?>:</span><del id="product-old-price"><?php echo ((float) $product['price_old'] ? price($product['price_old']) : price_vat(array($product, $product['pricebeforediscount'])))?></del></li>
					<?php endif; ?>
					
					<?php if(SHOW_PRICES && $product['recycling_fee']): ?>
						<li class="recycling_fee"><span><?php echo __('recycling_fee')?> <small>(<?php echo __('recycling_fee_included')?>)</small>:</span><?php echo price_vat(array($product, $product['recycling_fee']))?></li>
					<?php endif; ?>
					
					<?php if(SHOW_PRICES && $product['copyright_fee']): ?>
						<li class="copyright_fee"><span><?php echo __('copyright_fee')?>:</span><?php echo price_vat(array($product, $product['copyright_fee']))?></li>
					<?php endif; ?>
					
					<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
					<li>
						<span><?php echo __('availability')?>:</span>
						<strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $availability['name']?></strong>
						
						<?php if($display_stock && ! $attributes_have_stock && (int) $product['stock'] > 0): ?>
							(<?php echo $product['stock']?> <?php echo __('pcs')?>)
						<?php endif; ?>
					</li>
					<?php endif; ?>
					
					<?php if($product['guarantee']): ?>
						<li class="guarantee"><span><?php echo __('guarantee')?>:</span><?php echo $product['guarantee'].(is_numeric($product['guarantee']) ? ' '.__('months_short') : '')?></li>
					<?php endif; ?>
				</ul>
				
				<form action="<?php echo url(PAGE_BASKET)?>" method="post" class="basket clearfix">
					<?php if($enable_buy && SHOW_PRICES): ?>
					<fieldset class="buy">
						<input type="hidden" name="product_id" value="<?php echo $product['id']?>" />
						
						<button type="submit" name="buy" class="button buy"><?php echo __('add_to_basket')?></button>
						<?php echo __('quantity')?>: <input type="text" name="qty" value="1" size="2" /> <?php echo (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?>
					</fieldset>
					<?php endif; ?>
					
					<?php if(Core::$is_premium): $i = 0;
					$attr_total_prices = ((int) Core::config('attributes_prices_change') || count($attributes) > 1) ? 0 : 1;
					?>
					
					<script>
						_product_discount = <?php echo (($product['discount'] && ! $product['price_old']) ? (float) $product['discount'] : 0)?>;
						_product_price_before_discount = <?php echo (float) $product['pricebeforediscount']?>;
						_product_price = <?php echo (float) $product['price']?>;
						_product_vat = <?php echo (float) $product['vat']?>;
					</script>
					
					<?php foreach($attributes as $attr_name => $options): ?>
					
					<fieldset class="attribute">
						<label><?php echo $attr_name?>:</label>
						
						<?php if(in_array($attr_name, $attributes_with_stock)): ?>
						<div class="product-variants">
							<?php foreach($options as $opt): ?>
							<label class="product-variant">
								<span class="variant-availability">
									<?php if($opt['availability']): ?>
									<strong class="availability-<?php echo $opt['availability']['days']?>days"><?php echo $opt['availability']['name']?></strong>
									<?php endif; ?>
									
									<?php echo (($display_stock && $opt['stock'] > 0) ? ' ('.$opt['stock'].' '.__('pcs').')' : '')?></span>
									<input type="radio" name="attributes[<?php echo $i?>]" value="<?php echo $opt['id']?>"<?php echo (($opt['default'] && $opt['enable']) ? ' checked="checked"' : '').($opt['enable'] ? '' : ' disabled="disabled"')?> /> <?php echo $opt['value']?>
								
								
								<?php if($opt['price']): ?>
									<?php if($attr_total_prices): ?>
										<small>(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)</small>
										
									<?php else: ?>
										<small>(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)</small>
									<?php endif; ?>
								<?php endif; ?>
							</label>
							
							<script>
								attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
							</script>
							
							<?php if($opt['file_id']): ?>
							<script>
								attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
							</script>
							<?php endif; ?>
							
							<?php endforeach; ?>
						</div>
						<?php else: ?>

						<select name="attributes[]">
							<?php foreach($options as $opt): ?>
							<option value="<?php echo $opt['id']?>"<?php echo ($opt['default'] ? ' selected="selected"' : '')?>>
								<?php echo $opt['value']?>
								
								<?php if($opt['price']): ?>
									<?php if($attr_total_prices): ?>
										(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)
										
									<?php else: ?>
										(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)
									<?php endif; ?>
								<?php endif; ?>
							</option>
							
							<script>
								attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
							</script>
							
							<?php if($opt['file_id']): ?>
							<script>
								attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
							</script>
							<?php endif; ?>
							
							<?php endforeach; ?>
						</select>
						<?php endif; ?>
						
					</fieldset>
					<?php $i ++; endforeach; 
					endif; ?>
				</form>
			</div>
		</div>
	</div>
	
	<div class="sharelinks">
	<?php echo Core::config('sharelinks'); ?>
	</div>
	
	<div class="description">
		<h2><?php echo trim($product['name'].' '.$product['nameext'])?></h2>
		
		<?php echo $product['description']?>
	</div>
	
	<?php if(count($features)): ?>
	<div class="features">
		<table>
			<tbody>
			<?php foreach($features as $feature): ?>
				<tr>
					<td class="featurename"><?php echo $feature['name']?>:</td>
					<td><?php echo $feature['value'].' '.$feature['unit']?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php endif; ?>
	
	<?php if(Core::$is_premium && $attributes && $product['list_attributes']): ?>
	<div class="attributes-list">
			<h4 class="pane-title"><?php echo __('available_options')?>:</h4>
			
			<?php foreach($attributes as $attr_name => $options): ?>
			<table class="grid attrs">
				<caption><?php echo $attr_name?></caption>
				<tbody>
					<?php foreach($options as $option): ?>
					<tr>
						<td class="attr-value">
							<?php echo $option['value']?>
							
							<?php if($attributes_show_sku && $option['sku']): ?>
							<span class="sku"><?php echo __('sku').': '.$option['sku']?></span>
							<?php endif; ?>
							
							<?php if($attributes_show_ean13 && $option['ean13']): ?>
							<span class="ean13"><?php echo __('ean').': '.$option['ean13']?></span>
							<?php endif; ?>
						</td>
						<td class="attr-price"><?php echo ($option['price'] ? ($option['price'] > 0 ? '+' : '').price_vat(array($product, $option['price'])) : '')?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php endforeach; ?>
	</div>
	<?php endif; ?>
	
	<?php if($related_products): ?>
	<div class="related-products">
		<h4 class="pane-title"><?php echo __('related_products')?>:</h4>
		
		<div class="clearfix">
			<?php foreach($related_products as $i => $related): ?>
			<div class="related<?php echo (($i + 1) % 3 === 0 ? ' related-last' : '')?>">
				<a href="<?php echo url($related)?>" class="related-title"><strong><?php echo limit_words($related['name'], 5)?></strong></a>
				
				<div class="picture">
					<a href="<?php echo url($related)?>"><img src="<?php echo imgsrc($related)?>" alt="" /></a>
				</div>
				
				<div class="prices">
					<div><strong><?php echo price_vat($related)?></strong><?php echo __('price')?>:</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if(Core::$is_premium && $discounts): ?>
	<div class="quantity-discounts">
		<h4 class="pane-title"><?php echo __('quantity_discounts')?>:</h4>
		<table class="grid">
			<thead>
				<tr>
					<td class="range"><?php echo __('quantity')?></td>
					<td class="discount"><?php echo __('discount')?></td>
					<td class="price"><?php echo __('price')?></td>
				</tr>
			</thead>
			<tbody>
			<?php foreach($discounts as $discount): ?>
				<tr>
					<td class="range"><?php echo $discount['range']?></td>
					<td class="discount"><?php echo (float) $discount['discount']?> %</td>
					<td class="price"><?php echo price_vat(array($product, $discount['price']))?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php endif; ?>
	
	<!-- non-image files -->
	<?php echo listFiles($product)?>
	
	<!-- bottom files -->
	<?php echo listFiles($product, 0)?>
</div>

<?php echo tpl('bottomlinks.php')?>