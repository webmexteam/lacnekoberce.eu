<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="page-customer">

	<div class="header">
		<div class="row-fluid">
			<div class="span9">
				<h2 class="customer-name"><?php echo Customer::get('first_name').' '.Customer::get('last_name')?></h2>
				<i class="email"><?php echo Customer::get('email')?></i>
			</div>
			<div class="span3">
				<a href="<?php echo url('customer/logout')?>" class="btn btn-danger pull-right"><?php echo __('logout')?></a>
			</div>
		</div>

		<br>

		<ul class="nav nav-tabs">
			<li<?php echo ($active_tab == 'orders' ? ' class="active"' : '')?>><a href="<?php echo url('customer')?>"><?php echo __('orders')?></a></li>
			<li<?php echo ($active_tab == 'settings' ? ' class="active"' : '')?>><a href="<?php echo url('customer/settings')?>"><?php echo __('settings')?></a></li>
		</ul>
	</div>

	<?php echo $content?>
</div>