<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>

<div class="row-fluid main-cols-2">
	<div class="span3">
		<div class="sidebar sidebar-left">
			<?php echo blocks('left')?>
		</div>
	</div>
	<div class="span9">
		<div class="main">
			<?php echo $content?>
		</div>
	</div>
</div>