<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('toplinks.php')?>

<?php echo breadcrumb($product)?>

<div class="productdetail<?php echo ((int) $product['promote'] ? ' productdetail-promote' : '')?>">

	<div class="page-header">
		<h1><?php echo trim($product['name'])?><?php echo ($product['nameext']) ? ' <small>'.trim($product['nameext']).'</small>' : ''?></h1>
	</div>

	<?php if(($labels = $product->product_labels()) && count($labels)): ?>
		<div class="labels">
			<?php foreach($labels as $label): if($label->label['name']): ?>
				<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
			<?php endif; endforeach; ?>
		</div>
	<?php endif; ?>
	<div class="reset"></div>

	<div class="row-fluid product-info">
		<div class="span5">
			<?php /* ToDo: to nesmyslne zarovnani dat uplne pryc. Zbytecna komplikace, kterou nikdo nepouziva */?>
			<!-- top files -->
			<?php echo listFiles($product, 1)?>
			<!-- right files -->
			<?php echo listFiles($product, 3, 3)?>
			<!-- left files -->
			<?php echo listFiles($product, 2, 'product')?>

			<?php if(count($features)): ?>
				<h4><?php echo __('features')?></h4>
				<div class="features">
					<table class="table">
						<tbody>
						<?php foreach($features as $feature): ?>
							<tr>
								<td class="featurename"><?php echo $feature['name']?>:</td>
								<td><?php echo $feature['value'].' '.$feature['unit']?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			<?php endif; ?>
		</div>
		<div class="span7">
			<h2 class="product-title"><?php echo trim($product['name'])?><?php echo ($product['nameext']) ? ' <small>'.trim($product['nameext']).'</small>' : ''?></h2>

			<div class="sharelinks">
				<?php echo ($_SERVER['HTTP_HOST'] != "localhost") ? Core::config('sharelinks') : ''; ?>
			</div>

			<?php if($product['description_short']): ?>
				<div class="shortdesc muted">
					<?php echo $product['description_short']?>
				</div>
			<?php endif; ?>

			<?php if($product['description']): ?>
				<div class="description">
					<?php echo $product['description']?>
				</div>
			<?php endif; ?>

			<table class="table">
				<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
					<!-- Dostupnost -->
					<tr>
						<th><?php echo __('availability')?>:</th>
						<td>
							<strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $availability['name']?></strong>
							<?php if($display_stock && ! $attributes_have_stock && (int) $product['stock'] > 0): ?>
								(<?php echo $product['stock'];?> <?php echo ($product['unit']) ? $product['unit'] : Core::$config['default_unit']?>)
							<?php endif; ?>
						</td>
					</tr>
				<?php endif; ?>

				<?php if(SHOW_PRICES && $product['price'] !== null && $product['vat']): ?>
					<!-- Cena s DPH -->
					<tr>
						<th><?php echo __('price_incl_vat')?>:</th>
						<td><strong id="product-price"><?php echo price_vat(array($product, $product['price']+$product['recycling_fee']))?></strong></td>
					</tr>
					<!-- Cena bez DPH -->
					<tr>
						<th><?php echo __('price_excl_vat')?>:</th>
						<td><em id="product-price-excl-vat"><?php echo price_unvat(array($product, $product['price']+$product['recycling_fee']))?></em></td>
					</tr>
				<?php elseif(SHOW_PRICES && $product['price'] !== null): ?>
					<!-- Cena (neplatci DPH) -->
					<tr>
						<th><?php echo __('price')?>:</th>
						<td><strong id="product-price"><?php echo price($product['price'] + $product['recycling_fee'])?></strong></td>
					</tr>
				<?php endif; ?>

				<?php if($product['discount']): ?>
					<!-- Sleva -->
					<tr>
						<th><?php echo __('discount')?>:</th>
						<td><?php echo (float) $product['discount']?> %</td>
					</tr>
				<?php endif; ?>

				<?php if(SHOW_PRICES && ((float) $product['price_old'] || $product['discount'])): ?>
					<!-- Puvodni cena -->
					<tr>
						<th><?php echo __('old_price')?>:</th>
						<td><strike id="product-old-price"><?php echo ((float) $product['price_old'] ? price($product['price_old']) : price_vat(array($product, $product['pricebeforediscount'])))?></strike></td>
					</tr>
				<?php endif; ?>

				<?php if(SHOW_PRICES && $product['recycling_fee']): ?>
					<!-- Recyklacni poplatek -->
					<tr>
						<th><?php echo __('recycling_fee')?> <small>(<?php echo __('recycling_fee_included')?>)</small>:</th>
						<td><?php echo price_vat(array($product, $product['recycling_fee']))?></td>
					</tr>
				<?php endif; ?>

				<?php if(SHOW_PRICES && $product['copyright_fee']): ?>
					<!-- Autorsky poplatek -->
					<tr>
						<th><?php echo __('copyright_fee')?>:</th>
						<td><?php echo price_vat(array($product, $product['copyright_fee']))?></td>
					</tr>
				<?php endif; ?>

				<?php if($product['guarantee']): ?>
					<!-- Zaruka -->
					<tr>
						<th><?php echo __('guarantee')?>:</th>
						<td><?php echo $product['guarantee'].(is_numeric($product['guarantee']) ? ' '.__('months_short') : '')?></td>
					</tr>
				<?php endif; ?>

				<?php if($product['show_sku'] && $product['sku']): ?>
					<!-- SKU -->
					<tr>
						<th><?php echo __('sku')?>:</th>
						<td><?php echo $product['sku']?></td>
					</tr>
				<?php endif; ?>

				<?php if($product['show_ean13'] && $product['ean13']): ?>
					<!-- EAN -->
					<tr>
						<th><?php echo __('ean')?>:</th>
						<td><?php echo $product['ean13']?></td>
					</tr>
				<?php endif; ?>
				<tr>
					<th><?php echo __('manufacturer')?>:</th>
					<td>
						<ul class="unstyled">
							<?php foreach($manufacturers as $page): ?>
								<li><?php echo pageFullPath($page->page, true)?></li>
							<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<tr>
					<th><?php echo __('category')?>:</th>
					<td>
						<ul class="unstyled">
							<?php foreach($categories as $page): ?>
								<li><?php echo pageFullPath($page->page, true)?></li>
							<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<!-- Formular -->
	<form action="<?php echo url(PAGE_BASKET)?>" method="post" class="basket clearfix">
		<?php if(Core::$is_premium): $i = 0;
			$attr_total_prices = ((int) Core::config('attributes_prices_change') || count($attributes) > 1) ? 0 : 1;
			?>

			<script>
				_product_discount = <?php echo (($product['discount'] && ! $product['price_old']) ? (float) $product['discount'] : 0)?>;
				_product_price_before_discount = <?php echo (float) $product['pricebeforediscount']?>;
				_product_price = <?php echo (float) $product['price']?>;
				_product_vat = <?php echo (float) $product['vat']?>;
			</script>

			<div class="well">
			<?php foreach($attributes as $attr_name => $options): ?>

				<div class="attribute">
					<h4><?php echo $attr_name?>:</h4>
					<table class="table table-striped table-hover variants-table">

					<?php if(in_array($attr_name, $attributes_with_stock)): ?>
						<?php foreach($options as $opt): ?>
							<tr>
								<td>
									<label class="product-variant">
										<span class="row-fluid">
											<span class="span6">
												<input type="radio" name="attributes[<?php echo $i?>]" value="<?php echo $opt['id']?>"<?php echo (($opt['default'] && $opt['enable']) ? ' checked="checked"' : '').($opt['enable'] ? '' : ' disabled="disabled"')?> />
												<?php echo $opt['value']?>
												<span class="variant-availability">
													<?php if($opt['availability']): ?>
														<strong class="availability-<?php echo $opt['availability']['days']?>days"><?php echo $opt['availability']['name']?></strong>
													<?php endif; ?>
													<?php echo (($display_stock && $opt['stock'] > 0) ? ' ('.$opt['stock'].' '.__('pcs').')' : '')?>
												</span>
											</span>
											<span class="span6">
												<span class="pull-right">
													<?php if($opt['price']): ?>
														<?php if($attr_total_prices): ?>
															<small>(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)</small>

														<?php else: ?>
															<small>(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)</small>
														<?php endif; ?>
													<?php endif; ?>
												</span>
											</span>
										</span>
									</label>

									<script>
										attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
										<?php if($opt['file_id']): ?>
											attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
										<?php endif; ?>
									</script>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>

						<select name="attributes[]">
							<?php foreach($options as $opt): ?>
								<option value="<?php echo $opt['id']?>"<?php echo ($opt['default'] ? ' selected="selected"' : '')?>>
									<?php echo $opt['value']?>

									<?php if($opt['price']): ?>
										<?php if($attr_total_prices): ?>
											(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)

										<?php else: ?>
											(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)
										<?php endif; ?>
									<?php endif; ?>
								</option>

								<script>
									attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
								</script>

							<?php if($opt['file_id']): ?>
								<script>
									attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
								</script>
							<?php endif; ?>

							<?php endforeach; ?>
						</select>
					<?php endif; ?>
					</table>
				</div>
				<?php $i ++; endforeach; ?>
				</div>
		<?php endif; ?>

		<?php if($enable_buy && SHOW_PRICES): ?>
			<div class="well">
				<div>
					<input type="number" name="qty" value="1" size="2" class="span4" autocomplete="off" /> <?php echo (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?>
				</div>
				<div>
					<button type="submit" name="buy" class="btn btn-success btn-large"><?php echo __('add_to_basket')?></button>
				</div>
			</div>
		<?php endif; ?>

		<input type="hidden" name="product_id" value="<?php echo $product['id']?>" />
	</form>

	<?php if(Core::$is_premium && $attributes && $product['list_attributes']): ?>
	<div class="attributes-list">
			<h4 class="pane-title"><?php echo __('available_options')?>:</h4>

			<?php foreach($attributes as $attr_name => $options): ?>
			<table class="grid attrs">
				<caption><?php echo $attr_name?></caption>
				<tbody>
					<?php foreach($options as $option): ?>
					<tr>
						<td class="attr-value">
							<?php echo $option['value']?>

							<?php if($attributes_show_sku && $option['sku']): ?>
							<span class="sku"><?php echo __('sku').': '.$option['sku']?></span>
							<?php endif; ?>

							<?php if($attributes_show_ean13 && $option['ean13']): ?>
							<span class="ean13"><?php echo __('ean').': '.$option['ean13']?></span>
							<?php endif; ?>
						</td>
						<td class="attr-price"><?php echo ($option['price'] ? ($option['price'] > 0 ? '+' : '').price_vat(array($product, $option['price'])) : '')?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if($related_products): ?>
	<div class="related-products">
		<h4><?php echo __('related_products')?>:</h4>

		<div class="products products-3cols">
			<?php $x = 1; foreach($related_products as $related): ?>
				<?php if($x == 1):?><div class="row-fluid"><?php endif?>
				<div class="span4 product<?php echo ((int) $related['promote'] ? ' product-promote' : '').' i'.($x % 3)?>">

					<div class="thumbnail">
						<div class="caption">
							<h3 class="name">
								<a href="<?php echo url($related)?>" title="<?php echo trim($related['name'].' '.$related['nameext'])?>"><?php echo trim($related['name'].' '.$related['nameext'])?></a>
							</h3>
							<?php if(($img = imgsrc($related, null, 5, $alt)) !== null): ?>
								<a href="<?php echo url($related)?>" title="<?php echo trim($related['name'].' '.$related['nameext'])?>" class="image">
									<?php if($related['discount']): ?>
										<span class="badge discount badge-important">-<?php echo ((float) $related['discount'])?> %</span>
									<?php endif; ?>

									<?php if(($labels = $related->product_labels()) && count($labels)): ?>
										<div class="labels">
											<?php foreach($labels as $label): if($label->label['name']): ?>
												<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
												<div class="reset"></div>
											<?php endif; endforeach; ?>
										</div>
									<?php endif; ?>
									<img src="<?php echo $img?>" alt="<?php echo $alt?>" data-function="verticalImageCenter">
								</a>
							<?php endif; ?>

							<div class="description muted">
								<?php echo $related['description_short']?>
							</div>

							<br>

							<div class="row-fluid bottom-row">
								<div class="span7">
									<div class="price">
										<?php if(SHOW_PRICES && $related['price'] !== null): ?>
											<strong><?php echo price_vat(array($related, $related['price'] + $related['recycling_fee']))?></strong>
										<?php else: ?>
											&nbsp;
										<?php endif; ?>
									</div>
									<?php if((($availability = $related->availability) && $availability['name']) || ($availability = $related['bestAvailability'])): ?>
										<div class="availability">
											<strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $availability['name']?></strong>
										</div>
									<?php endif; ?>
								</div>
								<div class="span5">
									<?php /*if(SHOW_PRICES && $related['price'] !== null && (! (int) Core::config('suspend_no_stock') || ! (notEmpty($product['stock']) && $product['stock'] == 0)) ): ?>
								<a href="<?php echo url(PAGE_BASKET, array('buy' => $related['id']))?>" class="btn btn-primary buy"><i class="icon-shopping-cart"></i></a>
							<?php endif;*/ ?>

									<a href="<?php echo url($related)?>" class="btn btn-success pull-right"><?php echo __('detail')?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php if($x % 3 == 0 && $x != count($related_products) && $x != 1):?></div><div class="row-fluid"><?php endif?>
				<?php if($x == count($related_products)):?></div><?php endif?>
				<?php $x++; endforeach; ?>
		</div>
	</div>
	<?php endif; ?>

	<?php if(Core::$is_premium && $discounts): ?>
	<div class="quantity-discounts">
		<h4 class="pane-title"><?php echo __('quantity_discounts')?>:</h4>
		<table class="grid">
			<thead>
				<tr>
					<td class="range"><?php echo __('quantity')?></td>
					<td class="discount"><?php echo __('discount')?></td>
					<td class="price"><?php echo __('price')?></td>
				</tr>
			</thead>
			<tbody>
			<?php foreach($discounts as $discount): ?>
				<tr>
					<td class="range"><?php echo $discount['range']?></td>
					<td class="discount"><?php echo (float) $discount['discount']?> %</td>
					<td class="price"><?php echo price_vat(array($product, $discount['price']))?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php endif; ?>

	<!-- non-image files -->
	<?php echo listFiles($product)?>

	<!-- bottom files -->
	<?php echo listFiles($product, 0)?>
</div>

<?php echo tpl('bottomlinks.php')?>