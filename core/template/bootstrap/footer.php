<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
	<div id="footer">
		<div class="container">
			<div class="row-fluid">
				<div class="span3">
					<h5><?php echo $_options['bootstrap']['footer_menu_1_title']?></h5>
					<?php echo menu(6)?>
				</div>
				<div class="span3">
					<h5><?php echo $_options['bootstrap']['footer_menu_2_title']?></h5>
					<?php echo menu(7)?>
				</div>
				<div class="span3">
					<h5><?php echo $_options['bootstrap']['footer_menu_3_title']?></h5>
					<?php echo menu(8)?>
				</div>
				<div class="span3">
					<h5><?php echo $_options['bootstrap']['footer_menu_4_title']?></h5>
					<?php echo menu(9)?>
				</div>
			</div>
			<div class="subfooter">
				<?php if(! Core::$is_premium || ! (int) Core::config('hide_webmex_link')): ?>
					<span class="powered">Powered by <a href="http://www.webmex.cz/" target="_blank">WEBMEX</a></span>
				<?php endif; ?>

				<?php if($footer = Core::config('footer')): ?>
					<?php echo preg_replace('/\{year\}/', date('Y'), $footer)?>

				<?php else: ?>
					Copyright &copy; <?php echo date('Y').' '.Core::config('store_name')?>
				<?php endif; ?>
			</div>
		</div>
</div>
<?php echo (Core::$profiler ? tpl('system/profiler.php') : '')?>