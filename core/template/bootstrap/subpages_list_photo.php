<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul class="nav nav-tabs nav-stacked subpages subpages-<?php echo $position?>">
	<?php foreach($pages as $page): ?>
		<li>
			<a href="<?php echo url($page)?>">
				<?php if(($img = imgsrc($page, null, 1)) !== null): ?>
					<span class="row-fluid">
						<span class="span2">
							<img src="<?php echo $img?>" alt="" class="picture" />
						</span>
						<span class="span10">
							<?php echo $page['name']?>
							<?php if($page['description_short']): ?>
								<span class="shortdesc muted">
									<?php echo $page['description_short']?>
								</span>
							<?php endif; ?>
						</span>
					</span>
				<?php else: ?>
					<?php echo $page['name']?>
					<?php if($page['description_short']): ?>
						<span class="shortdesc muted">
							<?php echo $page['description_short']?>
						</span>
					<?php endif; ?>
				<?php endif?>
			</a>

		</li>
	<?php endforeach; ?>
</ul>