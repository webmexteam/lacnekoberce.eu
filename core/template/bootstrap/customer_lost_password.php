<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<div class="page-header">
	<h1><?php echo __('lost_password')?></h1>
</div>

<form action="<?php echo url(true)?>" method="post" class="form customerpassword clearfix">
	<div class="customer-login clearfix">
		<fieldset>
			<?php if(isSet($customer_login_error)): ?>
				<div class="alert alert-block alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p><?php echo $customer_login_error?></p>
				</div>
			<?php endif; ?>
			
			<?php if(isSet($customer_login_msg)): ?>
				<div class="alert alert-block alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<p><?php echo $customer_login_msg?></p>
				</div>
			<?php endif; ?>

			<p><?php echo __('lost_password_help')?></p>

			<div class="row-fluid">
				<div class="span6">
					<div class="placeholder-input">
						<label for="customer_email_reset"><?php echo __('email')?>:</label>
						<div class="input-prepend">
							<span class="add-on">@</span>
							<input type="text" class="span12" name="customer_email_reset" id="customer_email_reset" value="" class="text">
						</div>
					</div>
				</div>
				<div class="span6">
					<label>&nbsp;</label>
					<button name="reset_password" class="btn btn-success"><?php echo __('reset_password')?></button>
				</div>
			</div>
		</fieldset>
	</div>
</form>