<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="block" id="block-id<?php echo $block->block['id']?>">
	<div class="title page-header">
		<h3><?php echo $block->getTitle()?></h3>
	</div>
	
	<div class="content">
		<?php echo $block->getContent()?>
	</div>
</div>