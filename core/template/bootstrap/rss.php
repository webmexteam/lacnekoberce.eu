<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><rss version="2.0"> 
	<channel> 
		<title><![CDATA[<?php echo strip_tags(Core::config('store_name'))?> - <?php echo $page['name']?>]]></title> 
		<link><?php echo Core::$url?></link> 
		<description><![CDATA[<?php echo strip_tags(Core::config('description'))?>]]></description> 
		<language><?php echo Core::$language?></language> 
		<generator>Webmex <?php echo Core::version?></generator>
	  
	  	<?php foreach($items as $item): ?>
	  	
		<item> 
			<title><![CDATA[<?php echo $item['name']?>]]></title> 
			<link><?php echo url($item, array('rss' => 1), true)?></link> 
			<description><![CDATA[<?php echo strip_tags($item['description_short'])?>]]></description> 
			<pubDate><?php echo gmdate('r', $item['pubdate'] ? $item['pubdate'] : time())?></pubDate> 
		</item>
		<?php endforeach; ?>

	</channel> 
</rss>