<?php if($page['enable_filter']): ?>

<?php 
$_order_options = array('name', 'price', 'default');
$_producers = isSet($_GET['p']) ? (array) $_GET['p'] : array();
$_features = isSet($_GET['f']) ? (array) $_GET['f'] : array();
?>
<form action="<?php echo url(true)?>" method="get">

	<div class="filter well">



		<?php if(isSet($_GET['uri']) && ! Core::$use_htaccess): ?>
		<input type="hidden" name="uri" value="<?php echo $_GET['uri']?>" />
		<?php endif; ?>
		
		<?php if(isSet($_GET['q'])): ?>
		<input type="hidden" name="q" value="<?php echo $_GET['q']?>" />
		<?php endif; ?>
		
		<input type="hidden" name="dir-<?php echo strtolower($_order_dir)?>" value="1" />
		<input type="hidden" name="view-<?php echo strtolower($_view)?>" value="1" />

		<div class="pull-right">

		</div>

		<div class="reset"></div>

		<div class="row-fluid">
			<div class="span9">
				<select name="order" class="pull-left">
					<?php foreach($_order_options as $opt): ?>
						<option value="<?php echo $opt?>"<?php echo ($opt == $_order ? ' selected="selected"' : '')?>><?php echo __($opt)?></option>
					<?php endforeach; ?>
				</select>
				<button type="submit" name="dir-asc" class="pull-left btn<?php echo ($_order_dir == 'ASC' ? ' active' : '')?>" style="margin-left:5px;"><i class="icon-long-arrow-up"></i></button>
				<button type="submit" name="dir-desc" class="pull-left btn<?php echo ($_order_dir == 'DESC' ? ' active' : '')?>" style="margin-left:5px;"><i class="icon-long-arrow-down"></i></button>
			</div>
			<div class="span3">
				<ul class="nav nav-pills pull-right">
					<li<?php echo ($_view == 'list' ? ' class="active"' : '')?>><button type="submit" name="view-list" class="btn<?php echo ($_view == 'list' ? ' active' : '')?>"><i class="icon-list-ul"></i></button>&nbsp;</li>
					<li<?php echo ($_view == 'pictures' ? ' class="active"' : '')?>><button type="submit" name="view-pictures" class="btn<?php echo ($_view == 'pictures' ? ' active' : '')?>"><i class="icon-picture"></i></button></li>
				</ul>
			</div>
		</div>

		<div class="reset"></div>

		<?php if($producers || $features): ?>
			<div class="producers clearfix">
				<a href="#" class="toggle-producers"><?php echo __($features ? 'filter_by_producers_or_features' : 'filter_by_producers')?></a>

				<div class="wrap"<?php echo ((! empty($_producers) || ! empty($_features)) ? ' style="display:block;"' : '')?>>
					<div class="producer">
						<label for="producer_all">
							<input type="checkbox" id="producer_all" <?php echo (empty($_producers) ? ' checked="checked"' : '')?> />
							<?php echo __('all')?>
						</label>
					</div>

					<div class="clearfix">
						<?php $i=1;foreach($producers as $producer): ?>
							<?php if($i == 1):?><div class="row-fluid"><?php endif?>
							<div class="span4">
								<label for="producer_<?php echo $producer['id']?>">
									<input type="checkbox" name="p[]" id="producer_<?php echo $producer['id']?>" value="<?php echo $producer['id']?>"<?php echo (in_array($producer['id'], $_producers) ? ' checked="checked"' : '')?> />
									<?php echo $producer['name']?>
								</label>
							</div>
							<?php if($i % 3 == 0 && $i != count($producers) && $i != 1):?></div><div class="row-fluid"><?php endif?>
							<?php if($i == count($producers)):?></div><?php endif?>
						<?php $i++;endforeach; ?>
					</div>

					<?php if($features): ?>
						<div class="features clearfix">
							<?php $i = 1;foreach($features as $feature_id => $feature): ?>
								<?php if($i == 1):?><div class="row-fluid"><?php endif?>
								<div class="span4">
									<label for="filter-<?php echo $feature_id?>"><?php echo $feature['name']?>:</label>
									<select name="f[<?php echo $feature_id?>]" id="filter-<?php echo $feature_id?>">
										<option value=""><?php echo __('all')?></option>
										<?php foreach($feature['options'] as $opt_text => $opt): ?>
											<option value="<?php echo $opt['value']?>"<?php echo ((isSet($_features[$feature_id]) && $_features[$feature_id] == $opt['value']) ? ' selected="selected"' : '')?>><?php echo $opt_text?> (<?php echo $opt['count']?>)</option>
										<?php endforeach; ?>
									</select>
								</div>
								<?php if($i % 3 == 0 && $i != count($features) && $i != 1):?></div><div class="row-fluid"><?php endif?>
								<?php if($i == count($features)):?></div><?php endif?>
							<?php $i++;endforeach;?>
						</div>
					<?php endif; ?>

					<br />
					<button type="submit" class="btn btn-success pull-right" style="margin-left:5px;"><?php echo __('filter_products')?></button>

					<?php if($features): ?>
						<a href="<?php echo url($page)?>" class="btn btn-inverse pull-right"><?php echo __('filter_reset')?></a>
					<?php endif; ?>

					<div class="reset"></div>
				</div>
			</div>
		<?php endif; ?>
	</div>

</form>

<?php endif; ?>