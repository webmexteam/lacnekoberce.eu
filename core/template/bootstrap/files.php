<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul class="files files-<?php echo ($align === null ? 'n' : $align)?>">
	<?php 
	$_autosize = $autosize;
	
	foreach($files as $i => $file): 
		$cls = '';
		
		if($file['is_image']):
			
			if($autosize == 'product' && $_autosize == $autosize){
				$_autosize = 5;
				
			}else if($autosize == 'product'){
				$_autosize = 1;
				if($file['size'] == -1){
					$cls = 'smallpic';
				}
			}
	?>
		<li class="picture fid-<?php echo $file['id']?> <?php echo $cls?>">
			<div class="thumbnail">
				<?php if($file['size'] != 0): ?>
					<a href="<?php echo imgsrc($file, 8)?>" class="lightbox" rel="pictures">
						<img src="<?php echo imgsrc($file, null, $_autosize)?>" data-function="verticalImageCenter" alt="<?php echo $file['description']?>" />
					</a>

				<?php else: ?>
					<img src="<?php echo imgsrc($file, null, $_autosize)?>" data-function="verticalImageCenter" alt="<?php echo $file['description']?>" />
				<?php endif; ?>

				<?php if($file['description']): ?>
					<br /><span class="desc"><?php echo $file['description']?></span>
				<?php endif; ?>
			</div>
		</li>
		
		<?php else: ?>
		<li class="file ext-<?php echo substr($file['filename'], strrpos($file['filename'], '.') + 1)?>">
			<a href="<?php echo $base.$file['filename']?>" title="<?php echo $file['description']?>"><?php echo $file['filename']?></a>
		</li>
		<?php endif; ?>
	
	<?php endforeach; ?>
</ul>