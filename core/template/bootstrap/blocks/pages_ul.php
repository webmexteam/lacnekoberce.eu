<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

if(count($pages)): ?>
<ul class="nav nav-tabs nav-stacked">
	<?php foreach($pages as $page): ?>
	<li<?php echo (Core::$current_page['id'] == $page['id'] ? ' class="active"' : '')?>>
	<a href="<?php echo url($page)?>">
		<?php echo $page['name']?>
		<i class="icon-chevron-right"></i>
	</a>
	<?php if(in_array($page['id'], (array) $expanded)): ?>
	<?php echo tpl('blocks/pages_ul.php', array('pages' => $block->getPages($page['id']), 'block' => $block, 'expanded' => $expanded))?>
	<?php endif; ?>
	
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>