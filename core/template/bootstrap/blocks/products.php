<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<div class="products">
	<?php foreach($products as $product): ?>
		<?php $pname = $product['name'].' '.$product['nameext'];?>
		<div class="thumbnail">
			<?php if(($img = imgsrc($product, 4)) !== null): ?>
				<a href="<?php echo url($product)?>" title="<?php echo $pname?>"><img src="<?php echo $img?>" alt="<?php echo $pname?>" /></a>
			<?php endif; ?>
			<div class="caption">
				<h3><a href="<?php echo url($product)?>" title="<?php echo $pname?>"><?php echo $pname?></a></h3>
				<?php if($product['description_short']):?>
				<?php echo $product['description_short']?>
				<?php endif?>
				<?php if(SHOW_PRICES && $product['price'] !== null): ?>
					<div class="row-fluid price">
						<div class="span6">
							<strong><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></strong>
						</div>
						<div class="span6">
							<a href="<?php echo url($product)?>" title="<?php echo $pname?>" class="btn pull-right btn-success"><?php echo __('detail')?> &raquo;</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach; ?>
</div>