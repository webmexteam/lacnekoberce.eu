<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><form action="<?php echo url('newsletter')?>" method="post" class="block-newsletter">
	<fieldset>
		<div class="placeholder-input">
			<label for="newsletter_email"><?php echo __('enter_your_email')?>:</label>
			<div class="input-prepend">
				<span class="add-on">@</span>
				<input type="text" class="span12" name="newsletter_email" id="newsletter_email" value="@" class="text" />
			</div>
		</div>
	</fieldset>
	
	<button type="submit" name="signup" class="btn btn-success"><?php echo __('newsletter_signup')?></button>
</form>