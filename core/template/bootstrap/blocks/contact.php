<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="contact">
	<form action="#" method="post">
		<?php if($sent): ?>
			<div class="alert alert-block alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo __('message_sent')?>
			</div>
		<?php endif; ?>

		<?php if($error): ?>
			<div class="alert alert-block alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $error?>
			</div>
		<?php endif; ?>

		<input type="hidden" name="block_id" value="<?php echo $block->block['id']?>" />
		<input type="hidden" name="bcc_<?php echo $block->block['id']?>" value="0" />

		<fieldset>

			<div class="placeholder-input">
				<label for="bc-contact"><?php echo __('your_contact')?>:</label>
				<div class="input-prepend">
					<span class="add-on">@</span>
					<input type="text" class="span12" name="contact-contact" id="bc-contact" value="<?php echo @$values['contact-contact']?>">
				</div>
			</div>

			<div class="placeholder-input">
				<label for="bc-text"><?php echo __('your_message')?>:</label>
				<div class="input-prepend">
					<textarea name="contact-text" class="span12" id="bc-text" cols="40" rows="5"><?php echo @$values['contact-text']?></textarea>
				</div>
			</div>


			<?php if($block->config['captcha'] && $captcha = Captcha::get('contact'.$block->block['id'])): ?>
				<div class="bc-captcha" id="bc-captcha-<?php echo $block->block['id']?>">
					<label for="bc-captcha"><?php echo __('captcha_verification_code')?>:</label>

					<div class="bc-captchapic">
						<img src="<?php echo $captcha?>" alt="" id="bc-captcha-img" class="bc-code" />
					</div>

					<div class="bc-captchainput">
						<input type="text" name="captcha" id="bc-captcha" value="" />
						<a href="#" onclick="(el=document.getElementById('bc-captcha-img')).src=el.src.replace(/\/[\d\.]+$/, '')+'/'+Math.random();return false;" class="bc-reload"><?php echo __('captcha_reload')?></a>
					</div>
				</div>
				<script>
					(function(){
						var captcha = document.getElementById('bc-captcha-<?php echo $block->block['id']?>');

						<?php if(empty($values['contact-contact'])): ?>
						captcha.style.display = 'none';
						<?php endif; ?>

						$(function(){
							var wrap = $(captcha).parent();

							wrap.find('input, textarea').bind('focus', function(){
								$(captcha).slideDown();
							});

							wrap.parents('form:first').bind('submit', function(){
								var input = $(this).find('input[name="captcha"]');

								if(! input.val()){
									$(captcha).show();
									input.focus();
									return false;
								}

								$(this).attr('action', '<?php echo url(true, array('js' => 1))?>');
							});
						});
					})();

					$(function(){ $('input[name="bcc_<?php echo $block->block['id']?>"]').val('<?php echo $block->block['id']?>'); });
				</script>
			<?php endif; ?>

			<input type="text" name="contact-email" value="" class="bc-email" />

			<button type="submit" name="send" class="btn btn-success"><?php echo __('send_message')?></button>
		</fieldset>
	</form>
</div>