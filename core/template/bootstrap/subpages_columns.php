<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<ul class="thumbnails gallery subpages subpages-<?php echo $position?>">
	<?php $i = 1; foreach($pages as $page): ?>
	<li class="span4">
		<div class="thumbnail">
			<a href="<?php echo url($page)?>" class="title">
				<?php echo $page['name']?>

				<?php if($page['description_short']): ?>
					<span class="shortdesc muted">
					<?php echo $page['description_short']?>
				</span>
				<?php endif; ?>
			</a>
		</div>
	</li>

	<?php if($i % 3 === 0): ?>
	</ul><ul class="thumbnails gallery subpages subpages-<?php echo $position?>">
	<?php endif; ?>

	<?php $i ++; endforeach; ?>
</ul>
