<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('products/product_template.php', array('products' => $products, 'cols' => 4, 'page' => $page, 'producers' => $producers, 'features' => $features))?>