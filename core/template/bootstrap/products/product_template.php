<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
echo tpl('filter.php', array('page' => $page, 'producers' => $producers, 'features' => $features));

switch($cols) {
	case 2: $cols = 2; $span = 6; break;
	case 3: $cols = 3; $span = 4; break;
	case 4: $cols = 4; $span = 3; break;
	default: $cols = 3; $span = 4; break;
}
?>

<div class="products products-<?php echo $cols?>cols">
	<?php $i = 1; foreach($products as $product): ?>
		<?php if($i == 1):?><div class="row-fluid"><?php endif?>
		<div class="span<?php echo $span?> product<?php echo ((int) $product['promote'] ? ' product-promote' : '').' i'.($i % $cols)?>">

			<div class="thumbnail">
				<div class="caption">
					<h3 class="name">
						<a href="<?php echo url($product)?>" title="<?php echo trim($product['name'].' '.$product['nameext'])?>"><?php echo trim($product['name'].' '.$product['nameext'])?></a>
					</h3>
					<?php if(($img = imgsrc($product, null, 5, $alt)) !== null): ?>
						<a href="<?php echo url($product)?>" title="<?php echo trim($product['name'].' '.$product['nameext'])?>" class="image">
							<?php if($product['discount']): ?>
								<span class="badge discount badge-important">-<?php echo ((float) $product['discount'])?> %</span>
							<?php endif; ?>

							<?php if(($labels = $product->product_labels()) && count($labels)): ?>
								<div class="labels">
									<?php foreach($labels as $label): if($label->label['name']): ?>
										<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
										<div class="reset"></div>
									<?php endif; endforeach; ?>
								</div>
							<?php endif; ?>
							<img src="<?php echo $img?>" alt="<?php echo $alt?>" data-function="verticalImageCenter">
						</a>
					<?php endif; ?>

					<div class="description muted">
						<?php echo $product['description_short']?>
					</div>

					<br>

					<div class="row-fluid bottom-row">
						<div class="span7">
							<div class="price">
								<?php if(SHOW_PRICES && $product['price'] !== null): ?>
									<strong><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></strong>
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							</div>
							<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
								<div class="availability">
									<strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $availability['name']?></strong>
								</div>
							<?php endif; ?>
						</div>
						<div class="span5">
							<?php /*if(SHOW_PRICES && $product['price'] !== null && (! (int) Core::config('suspend_no_stock') || ! (notEmpty($product['stock']) && $product['stock'] == 0)) ): ?>
								<a href="<?php echo url(PAGE_BASKET, array('buy' => $product['id']))?>" class="btn btn-primary buy"><i class="icon-shopping-cart"></i></a>
							<?php endif;*/ ?>

							<a href="<?php echo url($product)?>" class="btn btn-success pull-right"><?php echo __('detail')?></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php if($i % $cols == 0 && $i != count($products) && $i != 1):?></div><div class="row-fluid"><?php endif?>
		<?php if($i == count($products)):?></div><?php endif?>
		<?php $i ++; endforeach; ?>
</div>