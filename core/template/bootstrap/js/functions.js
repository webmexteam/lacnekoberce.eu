$(document).ready(function(){
	$('.buyoncompany-wrap').hide();
	$('.buyoncompany input').change(function(){
		$('.buyoncompany-wrap').toggle();
	});


	// Dodaci adresa u registrace
	$('#shipping_as_billing').bind('change', function(){
		if(this.checked){
			$('#shipping_address').hide().find('input, select').attr('disabled', true);
		}else{
			$('#shipping_address').show().find('input, select').removeAttr('disabled');
		}
	}).bind('click', function(){
		$(this).trigger('change');
	}).trigger('change');


	$('*[data-function]').each(function(){
		var f = $(this).data('function');

		if (typeof window[f] === 'function'){
			window[f]($(this));
		}
	});
});

// Srovnani obrazku na stred podle rodicovskeho elementu
function verticalImageCenter(t) {
	t.load(function(){
		var parentHeight = t.parent().height();
		var imageHeight = t.height();

		if(parentHeight > imageHeight) {
			t.css('margin-top', (parentHeight-imageHeight) / 2);
		}
	});
}