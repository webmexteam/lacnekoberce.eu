<?php defined('WEBMEX') or die('No direct access.');
/**
 * Webmex - http://www.webmex.cz.
 */
?>

<form action="<?php echo url(PAGE_SEARCH)?>" method="get" class="navbar-form pull-right">
	<?php if(Core::$fix_path && ($search_page = Core::$db->page[PAGE_SEARCH])): ?>
		<input type="hidden" name="uri" value="<?php echo $search_page['sef_url'].'-a'.$search_page['id']?>" />
	<?php endif; ?>

	<input type="text" name="q" class="span2" placeholder="<?php echo __('search_label')?>">
	<button type="submit" class="btn btn-default"><?php echo __('search')?></button>
</form>