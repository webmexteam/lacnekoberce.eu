<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php foreach($orders as $order): 
if($order['delivery_id']){
	$delivery_driver = 'Delivery_'.ucfirst($order->delivery['driver']);
}

$delivery = null;
if($order['track_num'] && isSet($delivery_driver) && class_exists($delivery_driver)){
	$delivery = new $delivery_driver($order);		
}
?>
<div class="customer-order">
	<div class="thumbnail">
		<div class="caption">
			<table class="table table-striped">
				<col width="40%">
				<col width="60%">
				<tr>
					<th><?php echo __('order_id')?>:</th>
					<td><?php echo $order['id']?></td>
				</tr>
				<tr>
					<th><?php echo __('received')?>:</th>
					<td><?php echo fdate($order['received'], true)?></td>
				</tr>
				<tr>
					<th><?php echo __('status')?>:</th>
					<td><?php echo __('status_'.Core::$def['order_status'][$order['status']])?></td>
				</tr>
				<tr>
					<th><?php echo __('total')?>:</th>
					<td><?php echo price($order['total_incl_vat'], $order['currency'])?></td>
				</tr>
				<tr>
					<th><?php echo __('recipient')?>:</th>
					<td><?php echo ($order['ship_street'] ? $order['ship_first_name'].' '.$order['ship_last_name'] : $order['first_name'].' '.$order['last_name'])?></td>
				</tr>
			</table>

			<a href="<?php echo url('customer/order/'.$order['id'])?>" class="btn btn-success pull-right"><?php echo __('detail')?></a>

			<?php if($invoice = Core::$db->invoice()->where('order_id', $order['id'])->fetch()): ?>
				<a href="<?php echo url('customer/invoice/'.$invoice['id'])?>" class="btn btn-info pull-right" style="margin-right:10px;"><?php echo __('invoice')?></a>
			<?php endif; ?>

			<?php if($order['track_num'] && $delivery): ?>
				<a href="<?php echo $delivery->getTrackLink()?>" target="_blank" class="btn btn-inverse pull-right" style="margin-right:10px;"><?php echo __('track_package')?></a>
			<?php endif; ?>

			<div class="reset"></div>
		</div>
	</div>
</div>
<?php endforeach; ?>

<?php echo pagination($total_count, null, $limit)?>