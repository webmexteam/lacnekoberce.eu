<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('layout_head.php'); ?>

<?php echo tpl('header.php'); ?>

	<div class="container" id="container">
		<div id="main" class="clearfix">
			<?php echo $content?>
		</div><!--! end of #main-->
	</div>

	<?php echo tpl('footer.php'); ?>

<?php echo tpl('layout_foot.php'); ?>