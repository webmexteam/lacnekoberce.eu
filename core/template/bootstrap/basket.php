<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="basket">
	<form action="<?php echo url(PAGE_BASKET)?>" method="post">
	
		<?php if(isSet($basket_error)): ?>
		<div class="error">
			<p><?php echo $basket_error?></p>
		</div>
		<?php endif; ?>
		
		<div class="tablewrap">
			<table>
				<thead>
					<tr>
						<td class="picture">&nbsp;</td>
						<td class="name"><?php echo __('name')?></td>
						<td class="price"><?php echo __((int) Core::config('vat_payer') ? 'price_incl_vat' : 'price')?></td>
						<td class="quantity"><?php echo __('quantity')?></td>
						<td class="total"><?php echo __('total')?></td>
						<td class="remove">&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<?php if($basket_products === null || ! count($basket_products)): ?>
					<tr>
						<td colspan="6" class="basketempty"><?php echo __('basket_is_empty')?></td>
					</tr>
					<?php endif; ?>
					
					<?php foreach($basket_products as $product): ?>
					<tr data-productid="<?php echo $product['id'] ?>">
						<td class="picture">
							<?php if(isset($product['attribute_file_id'])): ?>
								<?php if(($img = imgsrcvariant($product['attribute_file_id'], $product['product'], 1)) !== null): ?>
									<img src="<?php echo $img?>" alt="" />
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							<?php else: ?>
								<?php if(($img = imgsrc($product['product'], 1)) !== null): ?>
									<img src="<?php echo $img?>" alt="" />
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							<?php endif; ?>
						</td>
						<td class="name">
							<a href="<?php echo url($product['product'])?>"><?php echo $product['product']['name'].' '.$product['product']['nameext']?></a>
							
							<?php if($product['attributes']): ?>
							<br /><span class="attr"><?php echo $product['attributes']?></span>
							<?php endif; ?>
						</td>
						<td class="price"><?php echo price_vat(array($product['product'], $product['price']))?></td>
						<td class="quantity">
							<input type="text" name="qty[<?php echo $product['id']?>]" class="ajax-qty" value="<?php echo $product['qty']?>" size="2" /> <?php echo (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?>
						</td>
						<?php ?>
						<td class="total"><?php echo price_vat(array($product['product'], ($product['price'] * $product['qty'])))?></td>
						<td class="remove"><a href="<?php echo url(true, array('delete' => $product['id']))?>"><?php echo __('remove')?></a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				
				<?php if($basket_products !== null && count($basket_products)):
					$total = Basket::total();
				?>
				<tfoot>
					<?php if((int) Core::config('vat_payer')): ?>
					<tr class="subtotal">
						<td>&nbsp;</td>
						<td class="label" colspan="3"><?php echo __('total_excl_vat')?>:</td>
						<td class="value"><?php echo price($total->subtotal)?></td>
						<td class="remove">&nbsp;</td>
					</tr>
					<?php endif; ?>
					
					<?php if($voucher = Basket::voucher()): ?>
					<tr class="voucher">
						<td>&nbsp;</td>
						<td class="label" colspan="3"><?php echo __('voucher')?> (<?php echo $voucher['code']?>, <a href="<?php echo url(true, array('removevoucher' => 1))?>" title="<?php echo __('remove')?>">&times;</a>):</td>
						<td class="value"><?php echo '-'.price($total->discount)?></td>
						<td class="remove">&nbsp;</td>
					</tr>
					<?php endif; ?>
					
					<tr class="total">
						<td>&nbsp;</td>
						<td class="label" colspan="3"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
						<td class="value"><?php echo price($total->total)?></td>
						<td class="remove">&nbsp;</td>
					</tr>
				</tfoot>
				<?php endif; ?>
			</table>
		</div>

		<?php if($basket_products !== null && count($basket_products)): ?>
		
		<?php if(Core::$is_premium && Core::config('enable_vouchers') && ! Basket::voucher()): ?>
		<div class="voucher">
			<h4><?php echo __('discount_voucher')?></h4>
			<p><?php echo __('voucher_info')?></p>
			<fieldset>
				<input type="text" name="voucher" value="" />
				<button type="submit" name="use_voucher" class="button"><?php echo __('use_voucher')?></button>
			</fieldset>
		</div>
		<?php endif; ?>
		
		<div class="buttons clearfix">
			<button type="submit" name="update_qty" class="button update"><?php echo __('update_qty')?></button>
			<button type="submit" name="checkout" class="button checkout"><?php echo __('checkout')?> &rsaquo;</button>
		</div>
		<?php endif; ?>
	</form>
</div>