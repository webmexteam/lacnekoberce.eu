<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php if(isSet($register)): ?>
	<div class="page-header">
		<h1><?php echo __('register_customer')?></h1>
	</div>
<?php endif; ?>

<form action="<?php echo url(true)?>" method="post" class="form customer clearfix">

	<?php if(isSet($customer_errors)): ?>
		<div class="alert alert-block alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<ul>
				<?php foreach($customer_errors as $error): ?>
					<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

	<fieldset>
		<legend><?php echo __('billing_address')?></legend>

		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="first_name"><?php echo __('first_name')?>:</label>
					<input type="text" name="first_name" id="first_name" value="<?php echo $customer['first_name']?>" class="span12">
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="last_name"><?php echo __('last_name')?>:</label>
					<input type="text" name="last_name" id="last_name" value="<?php echo $customer['last_name']?>" class="span12">
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="street"><?php echo __('street')?>:</label>
					<input type="text" name="street" id="street" value="<?php echo $customer['street']?>" class="span12">
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="city"><?php echo __('city')?>:</label>
					<input type="text" name="city" id="city" value="<?php echo $customer['city']?>" class="span12">
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="zip"><?php echo __('zip')?>:</label>
					<input type="text" name="zip" id="zip" value="<?php echo $customer['zip']?>" class="span6">
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="country"><?php echo __('country')?>:</label>
					<select name="country" id="country" class="span12">
						<?php foreach(Core::$db->country() as $country): ?>
							<option value="<?php echo $country['name']?>"<?php echo (($customer && $customer['country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="email"><?php echo __('email')?>:</label>
					<input type="text" name="email" id="email" value="<?php echo $customer['email']?>" class="span12"<?php echo (isSet($register) ? '' : ' disabled="disabled"')?>>
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="phone"><?php echo __('phone')?>:</label>
					<input type="text" name="phone" id="phone" value="<?php echo $customer['phone']?>" class="span12">
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span12">
				<label class="buyoncompany checkbox">
					<input type="checkbox"> Zboží nakupuji na firmu (IČ)
				</label>
			</div>
		</div>

		<div class="buyoncompany-wrap">
			<div class="row-fluid">
				<div class="span6">
					<label class="control-label" for="company"><?php echo __('company')?>:</label>
					<input type="text" name="company" id="company" value="<?php echo $customer['company']?>" class="span12">
				</div>
				<div class="span3">
					<label class="control-label" for="company_id"><?php echo __('company_id')?>:</label>
					<input type="text" name="company_id" id="company_id" value="<?php echo $customer['company_id']?>" class="span12">
				</div>

				<div class="span3">
					<label class="control-label" for="company_vat"><?php echo __('company_vat')?>:</label>
					<input type="text" name="company_vat" id="company_vat" value="<?php echo $customer['company_vat']?>" class="span12">
				</div>
			</div>
			<div class="well"><?php echo __('company_help')?></div>
		</div>
	</fieldset>

	<hr>

	<div class="row-fluid">
		<div class="span12">
			<label class="control-label" for="shipping_as_billing">
				<input type="checkbox" name="shipping_as_billing" id="shipping_as_billing" value="1" class="checkbox"<?php echo ($customer['ship_street'] ? '' : 'checked="checked"')?> />
				<?php echo __('shipping_as_billing')?>
			</label>
		</div>
	</div>

	<fieldset id="shipping_address">
		<legend><?php echo __('shipping_address')?></legend>


		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="ship_first_name"><?php echo __('first_name')?>:</label>
					<input type="text" name="ship_first_name" id="ship_first_name" value="<?php echo $customer['ship_first_name']?>" class="span12">
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="ship_last_name"><?php echo __('last_name')?>:</label>
					<input type="text" name="ship_last_name" id="ship_last_name" value="<?php echo $customer['ship_last_name']?>" class="span12">
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="ship_city"><?php echo __('city')?>:</label>
					<input type="text" name="ship_city" id="ship_city" value="<?php echo $customer['ship_city']?>" class="span12">
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="ship_street"><?php echo __('street')?>:</label>
					<input type="text" name="ship_street" id="ship_street" value="<?php echo $customer['ship_street']?>" class="span12">
				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="ship_zip"><?php echo __('zip')?>:</label>
					<input type="text" name="ship_zip" id="ship_zip" value="<?php echo $customer['ship_zip']?>" class="span6">
				</div>
			</div>
			<div class="span6">
				<div class="placeholder-input control-group required">
					<label class="control-label" for="ship_country"><?php echo __('country')?>:</label>
					<select name="ship_country" id="ship_country" class="span12">
						<?php foreach(Core::$db->country() as $country): ?>
							<option value="<?php echo $country['name']?>"<?php echo (($customer && $customer['ship_country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
	</fieldset>

	<hr>

	<fieldset>
		<div class="row-fluid">
			<div class="span12">
				<label class="control-label" for="newsletter">
					<input type="checkbox" name="newsletter" id="newsletter" value="1" class="checkbox"<?php echo ($has_newsletter ? ' checked="checked"' : '')?> />
					<?php echo __('want_to_receive_newsletter')?>
				</label>
			</div>
		</div>

		<hr>

		<?php if(! isSet($register) || ! (int) Core::config('customer_confirmation')): ?>
			<fieldset>
				<legend><?php echo __('password')?></legend>

				<?php if(! isSet($register)): ?>

					<div class="row-fluid">
						<div class="span4">
							<div class="placeholder-input control-group">
								<label class="control-label" for="old_password"><?php echo __('current_password')?>:</label>
								<input type="password" name="old_password" id="old_password" value="" class="span12" autocomplete="off">
							</div>
						</div>
						<div class="span4">
							<div class="placeholder-input control-group">
								<label class="control-label" for="password1"><?php echo __('new_password')?>:</label>
								<input type="password" name="password1" id="password1" value="" class="span12" autocomplete="off">
							</div>
						</div>
						<div class="span4">
							<div class="placeholder-input control-group">
								<label class="control-label" for="password2"><?php echo __('new_password_again')?>:</label>
								<input type="password" name="password2" id="password2" value="" class="span12" autocomplete="off">
							</div>
						</div>
					</div>

				<?php else: ?>
					<div class="row-fluid">
						<div class="span6">
							<div class="placeholder-input control-group">
								<label class="control-label" for="password1"><?php echo __('password')?>:</label>
								<input type="password" name="password1" id="password1" value="" class="span12" autocomplete="off">
							</div>
						</div>
						<div class="span6">
							<div class="placeholder-input control-group">
								<label class="control-label" for="password2"><?php echo __('password_again')?>:</label>
								<input type="password" name="password2" id="password2" value="" class="span12" autocomplete="off">
							</div>
						</div>
					</div>
				<?php endif; ?>
			</fieldset>
		<?php endif; ?>
	</fieldset>

	<div class="row-fluid">
		<div class="span12">
			<button type="submit" name="save" class="btn btn-success btn-large pull-right"><?php echo __(isSet($register) ? 'register' : 'save')?></button>
		</div>
	</div>
</form>