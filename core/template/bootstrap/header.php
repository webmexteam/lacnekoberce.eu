<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>


<header>
	<div id="header">
		<div class="navbar-fixed-top">
			<div id="topnav" class="navbar navbar-inverse">
				<div class="navbar-inner">
					<div class="container">
						<div class="nav-collapse collapse pull-right">
							<?php echo menu(4)?>
							<?php echo search_form()?>
						</div>
					</div>
				</div>
			</div>

			<div id="mainnav" class="navbar">
				<div class="navbar-inner">
					<div class="container">
						<a class="brand" href="<?php echo url(null)?>" title="<?php echo Core::config('store_name')?>">
							<?php echo ((! isSet($_options['bootstrap']['show_text_logo']) || $_options['bootstrap']['show_text_logo']) ? Core::config('store_name') : '')?>
							<span class="logo-img"></span>
						</a>

						<div class="nav-collapse collapse">
							<?php echo menu(1)?>
						</div>

						<div id="basket" class="pull-right">
							<?php if($page = Core::$db->page[PAGE_BASKET]): ?>
								<a href="<?php echo url(PAGE_BASKET)?>" title="<?php echo $page['name']?>">
									<span class="row-fluid">
										<span class="span2">
											<i class="icon-shopping-cart"></i>
										</span>
										<span class="span10">
											<?php $total_count = Basket::total_count(); ?>
											<?php if($count = Basket::count()): ?>
												<span class="qty"><?php echo __('products_in_basket')?>: <strong><?php echo $count?></strong> (<strong><?php echo $total_count?> ks</strong>)</span>
												<span class="price">V hodnotě: <strong><?php echo price(Basket::total()->total)?></strong></span>
											<?php else: ?>
												<span class="empty"><?php echo __('basket_is_empty')?></span>
											<?php endif; ?>
										</span>
									</span>
								</a>
							<?php endif; ?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>




<?php /*


<header>
	<div id="topnav">
		<div class="wrapper">
			<?php echo menu(4)?>
		</div>
	</div>



	<div id="topnav">
		<div class="wrapper">
			<?php echo menu(4)?>
		</div>
	</div>

	<div id="topnav" class="clearfix">
		<div class="wrap">
		<?php if($page = Core::$db->page[PAGE_BASKET]): ?>
			<?php if($count = Basket::count()): ?>
			<div class="checkout">
				<a href="<?php echo url(PAGE_BASKET)?>"><?php echo __('checkout')?> &rsaquo;</a>
			</div>
			<?php endif; ?>

			<div class="cart">
				<a href="<?php echo url(PAGE_BASKET)?>">
					<span class="t"><?php echo $page['name']?></span>

					<?php if($count = Basket::count()): ?>
					<small class="cart-items"><?php echo __('products_in_basket')?>: <strong><?php echo $count?></strong></small>
					<?php else: ?>
					<small class="cart-items"><?php echo __('basket_is_empty')?></small>
					<?php endif; ?>
				</a>
			</div>
		<?php endif; ?>

		<?php echo menu(4)?>
		</div>
	</div><!--! end of #topnav-->

	<div id="banner">
		<div class="wrap">
			<div class="logo">
				<a href="<?php echo url(null)?>" class="logo"><?php echo ((! isSet($_options['default']['show_text_logo']) || $_options['default']['show_text_logo']) ? Core::config('store_name') : '')?><span class="logo-img"></span></a>
			</div>

			<div class="search">
				<form action="<?php echo url(PAGE_SEARCH)?>" method="get">
					<?php if(Core::$fix_path && ($search_page = Core::$db->page[PAGE_SEARCH])): ?>
					<input type="hidden" name="uri" value="<?php echo $search_page['sef_url'].'-a'.$search_page['id']?>" />
					<?php endif; ?>

					<fieldset>
						<label for="search-q"><?php echo __('search_label')?>:</label>
						<input type="text" name="q" id="search-q" value="" />
						<button type="submit" class="button"><?php echo __('search')?></button>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!--! end of #banner-->

	<div id="mainnav">
		<div class="wrap">
			<?php echo menu(1)?>
		</div>
	</div><!--! end of #mainnav-->

</header>*/?>