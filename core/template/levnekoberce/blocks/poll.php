<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="poll">
	<p class="question"><?php echo $block->config['question']?></p>
	
	<ul>
		<?php 
		$total = $block->getTotalVotes();
		
		foreach($block->config['answers'] as $i => $answer): 
			$votes = (int) $answer['votes'];
			
			if(! $answer['text']){
				continue;
			}	
		
			$percent = $votes > 0 ? round(100 / $total * (int) $answer['votes']) : 0;
		?>
		<li>
			<?php if($disabled): ?>
			<?php echo $answer['text']?> <small>(<?php echo $percent?>%)</small>
			<?php else: ?>
			<a href="<?php echo url(true, array('poll' => $block->block['id'], 'answer' => $i))?>" rel="nofollow"><?php echo $answer['text']?></a> <small>(<?php echo $percent?>%)</small>
			<?php endif; ?>
			<div class="progress">
				<div class="bar" style="width:<?php echo $percent?>%;"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
	
	<span class="total"><?php echo __('total_votes')?>: <strong><?php echo $total?></strong></span>
</div>