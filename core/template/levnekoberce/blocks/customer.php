<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="customer">
	<?php if(Customer::$logged_in): ?>
		<div class="customerinfo">
			<small class="block"><?php echo __('logged_in_as')?>:</small>
			<strong><?php echo Customer::get('first_name').' '.Customer::get('last_name')?></strong>
			<strong><?php echo Customer::get('email')?></strong>

			<div class="row-fluid actions">
				<div class="span6">
					<a href="<?php echo url('customer')?>" class="btn btn-success"><i class="icon-user"></i> <?php echo __('my_account')?></a>
				</div>
				<div class="span6">
					<a href="<?php echo url('customer/logout')?>" class="btn btn-danger pull-right"><i class="icon-unlock-alt"></i> <?php echo __('logout')?></a>
				</div>
			</div>
		</div>
	<?php else: ?>
		<form action="<?php echo url('customer/login')?>" method="post">
			<fieldset>
				<input type="hidden" name="block_id" value="<?php echo $block->block['id']?>">

				<div class="placeholder-input">
					<label for="customer-email"><?php echo __('email')?>:</label>
					<div class="input-prepend">
						<span class="add-on">@</span>
						<input type="text" class="span12" name="customer_email" id="customer-email" value="">
					</div>
				</div>

				<div class="placeholder-input">
					<label for="customer-pass"><?php echo __('password')?>:</label>
					<div class="input-prepend">
					<span class="add-on">
						<i class="icon-lock"></i>
					</span>
						<input type="password" class="span12" name="customer_password" id="customer-pass" value="">
					</div>
				</div>

				<label for="customer-remember" class="checkbox">
					<input type="checkbox" name="customer_remember" value="1" class="checkbox" id="customer-remember">
					<?php echo __('login_remember')?>
				</label>

				<button type="submit" name="login" class="btn btn-success"><?php echo __('login')?></button>
			</fieldset>

			<br>

			<div class="row-fluid">
				<div class="span12">
					<a href="<?php echo url('customer/register')?>" class="btn btn-info btn-small pull-left"><?php echo __('register')?></a>
					<a href="<?php echo url('customer/lost_password')?>" class="btn btn-info btn-small pull-right"><?php echo __('lost_password')?></a>
				</div>
			</div>
		</form>
	<?php endif; ?>
</div>