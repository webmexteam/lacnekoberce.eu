<div class="basket">

	<table class="table table-bordered table-striped table-hover">
		<tbody>
		<?php if($basket_products === null || ! count($basket_products)): ?>
			<tr>
				<td><?php echo __('basket_is_empty')?></td>
			</tr>
		<?php else: ?>
			<?php foreach($basket_products as $product): ?>
				<tr>
					<td>
						<a href="<?php echo url($product['product'])?>"><?php echo $product['qty']?>x <?php echo $product['product']['name']?></a>
						<div class="smaller">
							<?php if($product['attributes']): ?>
								<div class="attributes">
									(<?php echo $product['attributes']?>)
								</div>
							<?php endif; ?>
							<div class="price"><?php echo price_vat(array($product['product'], $product['price']))?></div>
						</div>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td class="warning">
						<?php echo __('total')?>: <strong><?php echo price($total->total)?></strong>
					</td>
				</tr>
			</tfoot>
		<?php endif?>
	</table>

	<?php if($basket_products !== null || count($basket_products)): ?>
		<div class="row-fluid">
			<div class="span12">
				<a href="<?php echo url(PAGE_BASKET)?>" class="btn btn-success"><?php echo __('checkout')?> &rsaquo;</a>
			</div>
		</div>
	<?php endif?>
</div>