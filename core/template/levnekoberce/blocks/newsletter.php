<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><form action="<?php echo url('newsletter')?>" method="post" class="block-newsletter">

	<input type="text" class="text" name="newsletter_email" id="newsletter_email" value="Váš email" class="text" />
	
	<button type="submit" name="signup" class="btn btn-success"><?php echo __('newsletter_signup')?></button>
</form>