<?php if (Core::$is_premium): $i = 0;
	$attr_total_prices = ((int)Core::config('attributes_prices_change') || count($attributes) > 1) ? 0 : 1;
	?>

	<script>
		_product_discount = <?php echo (($product['discount'] && ! $product['price_old']) ? (float) $product['discount'] : 0)?>;
		_product_price_before_discount = <?php echo (float) $product['pricebeforediscount']?>;
		_product_price = <?php echo (float) $product['price']?>;
		_product_vat = <?php echo (float) $product['vat']?>;
        _product_price_transport = <?php echo (float) $product['price_transport']?>;
	</script>

	<?php if (count($attributes)): ?>
	<div class="attributes-box"><?php endif ?>
		<?php foreach ($attributes as $attr_name => $options): ?>

			<div class="attribute">
				<h4><?php echo $attr_name ?>:</h4>
				<table class="table table-striped table-hover variants-table">

					<?php if (in_array($attr_name, $attributes_with_stock)): ?>
						<?php foreach ($options as $opt): ?>
							<tr>
								<td>
									<label class="product-variant">
										<span class="row-fluid">
											<span class="span6">
												<input type="radio" name="attributes[<?php echo $i ?>]"
													   value="<?php echo $opt['id'] ?>"<?php echo (($opt['default'] && $opt['enable']) ? ' checked="checked"' : '') . ($opt['enable'] ? '' : ' disabled="disabled"') ?> />
												<?php echo $opt['value'] ?>
												<span class="variant-availability">
													<?php if ($opt['availability']): ?>
														<strong
															class="availability-<?php echo $opt['availability']['days'] ?>days"><?php echo $opt['availability']['name'] ?></strong>
													<?php endif; ?>
													<?php echo(($display_stock && $opt['stock'] > 0) ? ' (' . $opt['stock'] . ' ' . __('pcs') . ')' : '') ?>
												</span>
											</span>
											<span class="span6">
												<span class="pull-right">
													<?php if ($opt['price']): ?>
														<?php if ($attr_total_prices): ?>
															<small>
																(<?php echo price_vat(array($product, $opt['price'] + $product['price'])) ?>
																)
															</small>

														<?php else: ?>
															<small>
																(<?php echo ($opt['price'] > 0 ? '+' : '') . price_vat(array($product, $opt['price'])) ?>
																)
															</small>
														<?php endif; ?>
													<?php endif; ?>
												</span>
											</span>
										</span>
									</label>

									<script>
										attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
										<?php if($opt['file_id']): ?>
										attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
										<?php endif; ?>
									</script>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>

						<select name="attributes[]">
							<?php foreach ($options as $opt): ?>
								<option
									value="<?php echo $opt['id'] ?>"<?php echo($opt['default'] ? ' selected="selected"' : '') ?>>
									<?php echo $opt['value'] ?>

									<?php if ($opt['price']): ?>
										<?php if ($attr_total_prices): ?>
											<?php //echo '('.price_vat(array($product, $opt['price'] + $product['price'])).')' ?>

										<?php else: ?>
											<?php //echo '('.($opt['price'] > 0 ? '+' : '') . price_vat(array($product, $opt['price'])).')' ?>
										<?php endif; ?>
									<?php endif; ?>
								</option>

								<script>
									attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
								</script>

							<?php if ($opt['file_id']): ?>
								<script>
									attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
								</script>
							<?php endif; ?>

							<?php endforeach; ?>
						</select>
					<?php endif; ?>
				</table>
			</div>
			<?php $i++; endforeach; ?>
		<?php if (count($attributes)): ?></div><?php endif ?>
<?php endif; ?>


<?php if (Core::$is_premium && $attributes && $product['list_attributes']): ?>
	<div class="attributes-list">
		<h4 class="pane-title"><?php echo __('available_options') ?>:</h4>

		<?php foreach ($attributes as $attr_name => $options): ?>
			<table class="grid attrs">
				<caption><?php echo $attr_name ?></caption>
				<tbody>
				<?php foreach ($options as $option): ?>
					<tr>
						<td class="attr-value">
							<?php echo $option['value'] ?>

							<?php if ($attributes_show_sku && $option['sku']): ?>
								<span class="sku"><?php echo __('sku') . ': ' . $option['sku'] ?></span>
							<?php endif; ?>

							<?php if ($attributes_show_ean13 && $option['ean13']): ?>
								<span class="ean13"><?php echo __('ean') . ': ' . $option['ean13'] ?></span>
							<?php endif; ?>
						</td>
						<td class="attr-price"><?php echo($option['price'] ? ($option['price'] > 0 ? '+' : '') . price_vat(array($product, $option['price'])) : '') ?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<script>
$(document).ready(function(){

    var lock_pieces = <?php echo $product['lock_pieces']?>;

    if(lock_pieces) {
        $('select[name="attributes[]"]').change(function() {
            edButton();
        });

        edButton();

        function edButton() {
            var attr = '';
            $('select[name="attributes[]"] option:selected').each(function() {
              attr += $(this).val() + ";";
            });
            attr = attr.slice(0,-1)

            var product_id = <?php echo $product['id']?>;

            $.post(_base+'ajax/get_basket_product', {
                product_id: product_id,
                attr: attr
            }, function(response){
                var data = $.parseJSON(response);
                console.log(data);
                if(data != undefined && data.id > 0) {
                    $('.result button[name="buy"]').prop('disabled', true);
                } else {
                    $('.result button[name="buy"]').prop('disabled', false);
                }
            });
        }
    }

});
</script>