<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

?>
<div class="basket">
	<div class="steps">
		<div class="step step1 current">
			<div class="num">1</div>
			<div class="text">Nákupní košík</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2">
			<div class="num">2</div>
			<div class="text">Doprava a platba</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2">
			<div class="num">3</div>
			<div class="text">Dodací údaje</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
		<div class="step step2">
			<div class="num">4</div>
			<div class="text">Souhrn objednávky</div>
			<div class="line"><div class="left"></div><div class="right"></div></div>
		</div>
	</div>

	<form action="<?php echo url(PAGE_BASKET)?>" method="post">

		<?php if(isSet($basket_error)): ?>
		<div class="error">
			<p><?php echo $basket_error?></p>
		</div>
		<?php endif; ?>

		<div class="tablewrap">
			<table>
				<thead>
					<tr>
						<td class="picture">&nbsp;</td>
						<td class="name"><?php echo __('name')?></td>
						<td class="availability"><?php echo __('availability')?></td>
						<td class="price"><?php echo __((int) Core::config('vat_payer') ? 'price_incl_vat' : 'price')?></td>
						<td class="quantity"><?php echo __('quantity')?></td>
						<td class="total"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?></td>
						<td class="remove">&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<?php if($basket_products === null || ! count($basket_products)): ?>
					<tr>
						<td colspan="6" class="basketempty"><?php echo __('basket_is_empty')?></td>
					</tr>
					<?php endif; ?>

					<?php $i=1; foreach($basket_products as $product): ?>
					<tr data-productid="<?php echo $product['id'] ?>" class="<?php echo ($i%2 == 0) ? 'even' : 'odd'?>">
						<td class="picture">
							<span>
								<?php if(isset($product['attribute_file_id'])): ?>
									<?php if(($img = imgsrcvariant($product['attribute_file_id'], $product['product'], 2)) !== null): ?>
										<a href="<?php echo imgsrc(imgsrcvariant($product['attribute_file_id'], $product['product'], 2))?>" class="lightbox">
											<img src="<?php echo $img?>" alt="" />
										</a>
									<?php else: ?>
										&nbsp;
									<?php endif; ?>
								<?php else: ?>
									<?php if(($img = imgsrc($product['product'], 2)) !== null): ?>
										<a href="<?php echo imgsrc($product['product'], 5)?>" class="lightbox">
											<img src="<?php echo $img?>" alt="" />
										</a>
									<?php else: ?>
										&nbsp;
									<?php endif; ?>
								<?php endif; ?>
							</span>
						</td>
						<td class="name">
							<a href="<?php echo url($product['product'])?>"><?php echo $product['product']['name'].' '.$product['product']['nameext']?></a>

							<?php
								$availabilities = array();
								$db_availability = $db_availability_product = Core::$db->availability[(int) $product['product']['availability_id']];
								$availabilities[(int) $product['product']['availability_id']] = (int) $db_availability['days'];
							?>

							<?php if($product['attributes']): ?>
								<?php
								foreach (explode(';', $product['attributes_ids']) as $attr_id):
									$attribute = Core::$db->product_attributes[(int) $attr_id];
									if($attribute['availability_id'] != NULL) {
										$db_availability = Core::$db->availability[(int) $attribute['availability_id']];
										$availabilities[(int) $attribute['availability_id']] = (int) $db_availability['days'];
									}else{
										$db_availability = $db_availability_product;
									}
									?>
									<div class="attributes">
										<div class="row-fluid">
											<div class="span4 name"><strong><?php echo $attribute['name']?>:</strong></div>
											<div class="span5 value"><?php echo $attribute['value']?></div>
											<div class="span3 av days-<?php echo $db_availability['days']?>" style="color:#<?php echo $db_availability['hex_color']?>">(<?php echo $db_availability['name']?>)</div>
										</div>
									</div>

								<?php endforeach?>
							<?php endif; ?>
						</td>
						<td class="availability">
							<?php
								$maxAvailabilityId = 0;
								$maxAvailabilityDays = null;
								foreach ($availabilities as $id => $days) {
									if(max($availabilities) == $days) {
										$maxAvailabilityId = $id;
										$maxAvailabilityDays = $days;
									}
								}
								if($maxAvailabilityId > 0) {
									$availability = Core::$db->availability[(int) $maxAvailabilityId];
									$dostupnost_name = $availability['name'];
									$hex_color = $availability['hex_color'];
									if($maxAvailabilityDays == 0) {
										$dostupnost_cls = 'skladem';
									}else{
										$dostupnost_cls = 'days dostupnost-'.$maxAvailabilityDays;
									}
								}else{
									$dostupnost_name = 'Neznámá';
									$dostupnost_cls = 'neznama';
									$hex_color = 'cc0000';
								}
							?>

							<strong class="<?php echo $dostupnost_cls?>" style="color:#<?php echo $hex_color?>"><?php echo $dostupnost_name?></strong>
						</td>
						<td class="price"><?php echo price_vat(array($product['product'], $product['price']))?></td>
						<td class="quantity">
							<div class="qtywrap">
								<input type="text" <?php echo $product['product']['lock_pieces'] ? 'disabled' : '' ?> autocomplete="off" name="qty[<?php echo $product['id']?>]" class="ajax-qty" value="<?php echo $product['qty']?>" size="2" />
                                <a class="<?php echo $product['product']['lock_pieces'] ? 'disabled' : '' ?> qtychange plus" href="#">+</a>
								<a class="<?php echo $product['product']['lock_pieces'] ? 'disabled' : '' ?> qtychange minus" href="#">-</a>
							</div>
							<div class="qtytext"><?php echo (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?></div>
						</td>
						<?php ?>
						<td class="totaltd">
							<span class="total"><?php echo price_vat(array($product['product'], ($product['price'] * $product['qty']) + $product['product']['price_transport']))?></span>
							<span class="ajax"></span>
						</td>
						<td class="remove">
							<a href="<?php echo url(true, array('delete' => $product['id']))?>">
								<?php echo __('remove')?>
								<span></span>
							</a>
						</td>
					</tr>
					<?php $i++;endforeach; ?>
				</tbody>

				<?php if($basket_products !== null && count($basket_products)):
					$total = Basket::total();
				?>
				<tfoot>
					<?php if((int) Core::config('vat_payer')): ?>
					<tr class="subtotal">
						<td colspan="4">&nbsp;</td>
						<td class="label"><?php echo __('total_excl_vat')?>:</td>
						<td class="value"><?php echo price($total->subtotal)?></td>
						<td class="remove">&nbsp;</td>
					</tr>
					<?php endif; ?>

					<?php if($voucher = Basket::voucher()): ?>
					<tr class="subtotal">
						<td colspan="4">&nbsp;</td>
						<td class="label"><?php echo __('voucher')?> (<?php echo $voucher['code']?>, <a href="<?php echo url(true, array('removevoucher' => 1))?>" title="<?php echo __('remove')?>">&times;</a>):</td>
						<td class="value"><?php echo '-'.price($total->discount)?></td>
						<td class="remove">&nbsp;</td>
					</tr>
					<?php endif; ?>

					<tr class="total">
						<td colspan="4">&nbsp;</td>
						<td class="label"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
						<td class="value"><?php echo price($total->total)?></td>
						<td class="remove">&nbsp;</td>
					</tr>
				</tfoot>
				<?php endif; ?>
			</table>
		</div>

		<?php if($basket_products !== null && count($basket_products)): ?>

		<?php if(Core::$is_premium && Core::config('enable_vouchers') && ! Basket::voucher()): ?>
		<div class="voucher">
			<h4><?php echo __('discount_voucher')?></h4>
			<p><?php echo __('voucher_info')?></p>
			<fieldset>
				<input type="text" name="voucher" value="" />
				<button type="submit" name="use_voucher" class="button"><?php echo __('use_voucher')?></button>
			</fieldset>
		</div>
		<?php endif; ?>

		<div class="buttons clearfix">
			<button type="submit" name="update_qty" class="button update"><?php echo __('update_qty')?></button>
			<button type="submit" name="checkout" class="button text checkout btn btn-4">Objednat zboží &rsaquo;</button>
			<div class="reset"></div>
		</div>
		<?php endif; ?>
	</form>
</div>

<script>
	jQuery(document).ready(function($) {
		$('.qtychange').click(function(){
			var inp = $(this).parent().find('input');
			var val = parseInt(inp.val());

			if($(this).hasClass('plus')) {
				inp.val(val+1);
			}else if($(this).hasClass('minus') && val >= 2) {
				inp.val(val-1);
			}else{
				inp.val(1);
			}
			inp.trigger('change');
			return false;
		});
	});
</script>