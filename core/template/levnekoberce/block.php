<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="block" id="block-id<?php echo $block->block['id']?>">
	<?php if($block->getTitle() != "nezobrazovat"):?>
        <h3><?php echo $block->getTitle()?></h3>
    <?php endif?>
	
	<div class="content">
		<?php echo $block->getContent()?>
	</div>
</div>