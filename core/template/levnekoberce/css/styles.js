CKEDITOR.stylesSet.add('mystyles',[
	{name:_lang.style_big,element:'big'},
	{name:_lang.style_small,element:'small'},
	{name:_lang.style_code,element:'code'},
	{name:_lang.style_deleted,element:'del'},
	{name:_lang.style_inserted,element:'ins'},
	{name:_lang.style_cite,element:'cite'},
	{name:_lang.style_image_left,element:'img',attributes:{style:'padding: 5px; margin-right: 5px',align:'left'}},
	{name:_lang.style_image_right,element:'img',attributes:{style:'padding: 5px; margin-left: 5px',align:'right'}},
	{name:_lang.style_table_border,element:'table',attributes:{'class': 'bordertable'}}
]);

