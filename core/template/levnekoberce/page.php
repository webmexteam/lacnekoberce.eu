<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

 echo tpl('toplinks.php', array('page' => $page))?>

<?php if(!in_array($page['id'], array(PAGE_BASKET, PAGE_ORDER, PAGE_ORDER_STEP1, PAGE_ORDER_STEP2, PAGE_FINISH))){
	echo breadcrumb();
} ?>

<!-- top files -->
<?php echo listFiles($page, 1)?>

<!-- right files -->
<?php echo listFiles($page, 3)?>

<!-- left files -->
<?php echo listFiles($page, 2)?>

<?php if(!in_array($page['id'], array(PAGE_INDEX, PAGE_BASKET, PAGE_ORDER, PAGE_ORDER_STEP1, PAGE_ORDER_STEP2, PAGE_FINISH))){
	echo '<h1>'.(trim($page['seo_title']) ? $page['seo_title'] : $page['name']).'</h1>';
} ?>

<div class="reset clearfix"></div>

<?php if((int) $page['id'] == PAGE_INDEX): ?>
	<div class="promo-boxes">
		<div class="row-fluid">
			<div class="span4">
				<div class="item item-1">
					<strong><?php echo $tploptions['levnekoberce']['hp_box_1_title']?></strong>
					<span><?php echo $tploptions['levnekoberce']['hp_box_1_body']?></span>
				</div>
			</div>
			<div class="span4">
				<div class="item item-2">
					<strong><?php echo $tploptions['levnekoberce']['hp_box_2_title']?></strong>
					<span><?php echo $tploptions['levnekoberce']['hp_box_2_body']?></span>
				</div>
			</div>
			<div class="span4">
				<div class="item item-3">
					<strong><?php echo $tploptions['levnekoberce']['hp_box_3_title']?></strong>
					<span><?php echo $tploptions['levnekoberce']['hp_box_3_body']?></span>
				</div>
			</div>
		</div>
	</div>
<?php endif?>


<?php if($page['description_short']): ?>
<div class="">
	<?php echo $page['description_short']?>
</div>
<?php endif; ?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 2): ?>
	<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
		'pages' => $subpages,
		'position' => 'top'
	))?>
<?php endif; ?>

<?php echo $page['description'].$page_file_content?>

<!-- non-image files -->
<?php echo listFiles($page)?>

<!-- bottom files -->
<?php echo listFiles($page, 0)?>

<div class="reset clearfix"></div>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 1): ?>
	<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
		'pages' => $subpages,
		'position' => 'bottom'
	))?>
<?php endif; ?>
<div class="reset clearfix"></div>
<?php echo $page_action_top?>
<div class="reset clearfix"></div>
<?php if(count($products) || ! empty($_GET['f'])): ?>
	<?php echo tpl('products/'.$product_tpl, array('products' => $products, 'page' => $page, 'producers' => $producers, 'features' => $features))?>

	<?php if(count($products)): ?>
	<?php echo pagination($products_count)?>
	<?php elseif(! empty($_GET['p']) || ! empty($_GET['f'])): ?>
	<p class="no-products-found"><?php echo __('no_products_found')?></p>
	<?php endif; ?>
<?php endif; ?>

<?php echo $page_action?>

<?php echo tpl('bottomlinks.php')?>

<script>
	$(window).load(function(){
		var h = 0;
		$('.files.files-0 li').each(function(){
			if($(this).outerHeight() > h) {
				h = $(this).outerHeight();
			}
		});
		$('.files.files-0 li').css('height', h);
	});
</script>