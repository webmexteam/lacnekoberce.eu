<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
?>
<div class="sitemap clearfix">

	<?php $pages = Core::$db->page()->where('status', 1)->where('parent_page', 0)->where('menu', 1)->order('position ASC'); ?>
	
	<?php if(count($pages)): ?>
		<h2><?php echo __('pages')?></h2>
		
		<div class="clearfix">
			<div class="col">
			
			<?php $i = 0; foreach($pages as $page): ?>
		
			<?php if($i == ceil(count($pages) / 2)): ?>
			</div>
			<div class="col">
			<?php endif; ?>
			
				<h3><a href="<?php echo url($page)?>"><?php echo $page['name']?></a></h3>
				
				<?php echo tpl('sitemap_list.php', array('page' => $page))?>
			<?php $i ++; endforeach; ?>
			
			</div>
		</div>
	<?php endif; ?>

	<?php $pages = Core::$db->page()->where('status', 1)->where('parent_page', 0)->where('menu', 2)->order('position ASC'); ?>
	
	<?php if(count($pages)): ?>
		<h2><?php echo __('categories')?></h2>
		
		<div class="clearfix">
			<div class="col">
			
			<?php $i = 0; foreach($pages as $page): ?>
		
			<?php if($i == ceil(count($pages) / 2)): ?>
			</div>
			<div class="col">
			<?php endif; ?>
			
				<h3><a href="<?php echo url($page)?>"><?php echo $page['name']?></a></h3>
				
				<?php echo tpl('sitemap_list.php', array('page' => $page))?>
			<?php $i ++; endforeach; ?>
			
			</div>
		</div>
	<?php endif; ?>
	
	<?php $pages = Core::$db->page()->where('status', 1)->where('parent_page', 0)->where('menu', 3)->order('position ASC'); ?>
	
	<?php if(count($pages)): ?>
		<h2><?php echo __('producers')?></h2>
		
		<div class="clearfix">
			<div class="col">
			
			<?php $i = 0; foreach($pages as $page): ?>
		
			<?php if($i == ceil(count($pages) / 2)): ?>
			</div>
			<div class="col">
			<?php endif; ?>
			
				<h3><a href="<?php echo url($page)?>"><?php echo $page['name']?></a></h3>
				
				<?php echo tpl('sitemap_list.php', array('page' => $page))?>
			<?php $i ++; endforeach; ?>
			
			</div>
		</div>
	<?php endif; ?>

</div>