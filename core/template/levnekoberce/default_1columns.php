<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>

<div class="row-fluid main-cols-1">
	<div class="span12">
		<div class="one-col">
			<?php echo $content?>
		</div>
	</div>
</div>