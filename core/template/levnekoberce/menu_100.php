<?php defined('WEBMEX') or die('No direct access.');
/**
 * Webmex - http://www.webmex.cz.
 */
?>
<?php if(count($pages)):?>
	<ul class="<?php echo (isset($dropdown)) ? 'dropdown-menu' : 'nav'?>">
		<?php foreach($pages as $page): ?>
			<?php
			$subpages = Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)->order('position ASC');
			?>
			<li class="<?php echo (Core::$current_page == $page['id']) ? 'active ' : '';?>level-<?php echo $level?><?php if(count($subpages)): ?> dropdown<?php endif?>">
				<a href="<?php echo url($page)?>" title="<?php echo ($page['seo_title']) ? $page['seo_title'] : $page['name']?>"<?php echo (count($subpages)) ? ' class="dropdown-toggle" data-toggle="dropdown"' : ''?>>
					<?php echo $page['name']?>
					<?php if(count($subpages)):?><b class="caret"></b><?php endif?>
				</a>
				<?php if(count($subpages)): ?>
					<?php echo tpl('menu_4.php', array('pages' => $subpages, 'level' => $level + 1, 'dropdown' => true))?>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif?>