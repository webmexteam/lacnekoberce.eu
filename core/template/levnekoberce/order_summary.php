<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="order-summary">

	<h4><i class="icon-shopping-cart"></i> <?php echo __('your_order')?></h4>
	<table class="table table-striped">
		<col width="40%">
		<col width="60%">
		<tr>
			<th><?php echo __('order_id')?>:</th>
			<td>#<?php echo $order['id']?></td>
		</tr>
		<tr>
			<th><?php echo __('received')?>:</th>
			<td><?php echo fdate($order['received'], true)?></td>
		</tr>
		<?php if($order['status'] > 1): ?>
			<tr>
				<th><?php echo __('status')?>:</th>
				<td><?php echo __('status_'.Core::$def['order_status'][$order['status']])?></td>
			</tr>
		<?php endif; ?>
		<?php if($order['track_num']): ?>
			<tr>
				<th><?php echo __('track_num')?>:</th>
				<td><?php echo $order['track_num']?></td>
			</tr>
		<?php endif; ?>
	</table>

	<br><h4><i class="icon-credit-card"></i> <?php echo __('billing_address')?></h4>
	<table class="table table-striped">
		<col width="40%">
		<col width="60%">
		<tr>
			<th><?php echo __('first_name')?>:</th>
			<td><?php echo $order['first_name']?></td>
		</tr>
		<tr>
			<th><?php echo __('last_name')?>:</th>
			<td><?php echo $order['last_name']?></td>
		</tr>
		<tr>
			<th><?php echo __('company')?>:</th>
			<td><?php echo $order['company']?></td>
		</tr>
		<tr>
			<th><?php echo __('company_id')?>:</th>
			<td><?php echo $order['company_id']?></td>
		</tr>
		<tr>
			<th><?php echo __('company_vat')?>:</th>
			<td><?php echo $order['company_vat']?></td>
		</tr>
		<tr>
			<th><?php echo __('street')?>:</th>
			<td><?php echo $order['street']?></td>
		</tr>
		<tr>
			<th><?php echo __('city')?>:</th>
			<td><?php echo $order['city']?></td>
		</tr>
		<tr>
			<th><?php echo __('zip')?>:</th>
			<td><?php echo $order['zip']?></td>
		</tr>
		<tr>
			<th><?php echo __('country')?>:</th>
			<td><?php echo $order['country']?></td>
		</tr>
	</table>

	<br><h4><i class="icon-truck"></i> <?php echo __('shipping_address')?></h4>
	<?php if($order['ship_street']): ?>
		<table class="table table-striped">
			<col width="40%">
			<col width="60%">
			<tr>
				<th><?php echo __('first_name')?>:</th>
				<td><?php echo $order['ship_first_name']?></td>
			</tr>
			<tr>
				<th><?php echo __('last_name')?>:</th>
				<td><?php echo $order['ship_last_name']?></td>
			</tr>
			<tr>
				<th><?php echo __('company')?>:</th>
				<td><?php echo $order['ship_company']?></td>
			</tr>
			</tr>
			<tr>
				<th><?php echo __('street')?>:</th>
				<td><?php echo $order['ship_street']?></td>
			</tr>
			<tr>
				<th><?php echo __('city')?>:</th>
				<td><?php echo $order['ship_city']?></td>
			</tr>
			<tr>
				<th><?php echo __('zip')?>:</th>
				<td><?php echo $order['ship_zip']?></td>
			</tr>
			<tr>
				<th><?php echo __('country')?>:</th>
				<td><?php echo $order['ship_country']?></td>
			</tr>
		</table>
	<?php else: ?>
		<table class="table table-striped">
			<tr><td><?php echo __('shipping_as_billing')?></td></tr>
		</table>
	<?php endif; ?>

		<br><h4><i class="icon-phone"></i> <?php echo __('contact_info')?></h4>
		<table class="table table-striped">
			<col width="40%">
			<col width="60%">
			<tr>
				<th><?php echo __('email')?>:</th>
				<td><?php echo $order['email']?></td>
			</tr>
			<tr>
				<th><?php echo __('phone')?>:</th>
				<td><?php echo $order['phone']?></td>
			</tr>
			<tr>
				<th><?php echo __('comment')?>:</th>
				<td><?php echo nl2br($order['comment'])?></td>
			</tr>
		</table>

		<br><h4><i class="icon-archive"></i> <?php echo __('delivery_payment')?></h4>
		<table class="table table-striped">
			<col width="40%">
			<col width="60%">
			<tr>
				<th><?php echo __('delivery')?>:</th>
				<td><?php echo $order->delivery['name']?></td>
			</tr>
			<tr>
				<th><?php echo __('payment')?>:</th>
				<td><?php echo $order->payment['name']?></td>
			</tr>
			<?php if(Core::config('account_num') && strlen(Core::config('account_num')) > 0): ?>
				<tr>
					<th><?php echo __('account_num')?>:</th>
					<td><?php echo Core::config('account_num')?></td>
				</tr>
			<?php endif; ?>
			<?php if((int) $order['payment_realized']): ?>
				<tr>
					<th><?php echo __('payment_realized')?></th>
					<td><?php echo fdate($order['payment_realized'], true)?></td>
				</tr>
			<?php endif; ?>
			<?php if((int) $order['payment_realized'] && $order['payment_session']): ?>
				<tr>
					<th><?php echo __('payment_session')?></th>
					<td><?php echo $order['payment_session']?></td>
				</tr>
			<?php endif; ?>
			<?php if((int) Core::config('allow_order_payment_change') && ! (int) $order['payment_realized'] && $order['status'] == 1): ?>
				<tr>
					<th></th>
					<td><a href="<?php echo url(PAGE_ORDER, array('payment' => 1, 'id' => $order['id'], 'hash' => $order->model->hash()), true)?>"><?php echo __('select_different_payment')?></a></td>
				</tr>
			<?php endif; ?>
		</table>

		<br><h4><i class="icon-folder-open"></i> <?php echo __('products')?></h4>
		<table class="table table-striped">
			<col width="60%">
			<col width="40%">
			<tbody>
				<?php
					$products = $order->order_products();

					if(Core::$db_inst->_type == 'sqlite'){
						$products->order('rowid ASC');
					}

					foreach($products as $product): ?>
					<tr>
						<td><?php echo $product['name'].' '.$product['nameext']?></td>
						<td><?php echo $product['quantity']?> &times; <?php echo price_vat($product['price'] + $product['price_transport'], $product['vat'], $order['currency'])?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<?php if((int) Core::config('vat_payer')): ?>
					<tr>
						<th><?php echo __('total_excl_vat')?>:</th>
						<td><?php echo price($order['total_price'] /*+ price_unvat($order['delivery_payment'], VAT_DELIVERY)->price*/, $order['currency'])?></td>
					</tr>
				<?php endif; ?>

				<?php if($order['voucher_id'] && ($voucher = $order->voucher) && $voucher['id']): ?>
					<tr>
						<th><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</th>
						<td><?php echo '-'.price(estPrice($voucher['value'], $order->model->getTotal_incl_vat(true)), $order['currency'])?></td>
					</tr>
				<?php endif; ?>

				<tr>
					<th><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</th>
					<td><?php echo price($order['total_incl_vat'], $order['currency'])?></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
