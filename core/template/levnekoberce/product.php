<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

echo tpl('toplinks.php') ?>

<?php echo breadcrumb($product) ?>

	<div class="productdetail<?php echo((int)$product['promote'] ? ' productdetail-promote' : '') ?>">


		<h1><?php echo trim($product['name']) ?><?php echo ($product['nameext']) ? ' <small>' . trim($product['nameext']) . '</small>' : '' ?></h1>


		<?php if (($labels = $product->product_labels()) && count($labels)): ?>
			<div class="labels">
				<?php foreach ($labels as $label): if ($label->label['name']): ?>
					<span class="label"
						  style="background-color:#<?php echo trim($label->label['color']) ?>"><?php echo $label->label['name'] ?></span>
				<?php endif; endforeach; ?>
			</div>
		<?php endif; ?>
		<div class="reset"></div>

        <?php
            $productInBasket = null;
            if($product['lock_pieces']) {
                $basketId = Basket::id();
                $productInBasket = Core::$db->basket_products()->where('basket_id', $basketId)->where('product_id', $product['id'])->fetch();
            }
        ?>

		<!-- Formular -->
		<form action="<?php echo url(PAGE_BASKET) ?>" method="post" class="basket clearfix">
		<div class="row-fluid product-info">
			<div class="detLeft">
				<?php /* ToDo: to nesmyslne zarovnani dat uplne pryc. Zbytecna komplikace, kterou nikdo nepouziva */ ?>
				<!-- top files -->
				<?php echo listFiles($product, 1) ?>
				<!-- right files -->
				<?php echo listFiles($product, 3, 3) ?>
				<!-- left files -->
				<?php echo listFiles($product, 2, 'product') ?>

				<?php if (count($features)): ?>
					<h4><?php echo __('features') ?></h4>
					<div class="features">
						<table class="table">
							<tbody>
							<?php foreach ($features as $feature): ?>
								<tr>
									<td class="featurename"><?php echo $feature['name'] ?>:</td>
									<td><?php echo $feature['value'] . ' ' . $feature['unit'] ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				<?php endif; ?>
			</div>
				<input type="hidden" name="product_id" value="<?php echo $product['id'] ?>"/>

				<div class="detRight">

					<?php /*
			<div class="sharelinks">
				<?php echo ($_SERVER['HTTP_HOST'] != "localhost") ? Core::config('sharelinks') : ''; ?>
			</div>
			*/ ?>

					<?php if ($product['description_short']): ?>

						<?php echo $product['description_short'] ?>

					<?php endif; ?>


					<table>
						<?php if ((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
							<!-- Dostupnost -->
							<tr>
								<th><?php echo __('availability') ?>:</th>
								<td>
									<strong
										class="availability"<?php echo ($availability['hex_color']) ? ' style="color:#' . $availability['hex_color'] . '"' : "" ?>><?php echo $availability['name'] ?></strong>
									<?php if ($display_stock && !$attributes_have_stock && (int)$product['stock'] > 0): ?>
										(<?php echo $product['stock']; ?><?php echo ($product['unit']) ? $product['unit'] : Core::$config['default_unit'] ?>)
									<?php endif; ?>
								</td>
							</tr>
						<?php endif; ?>

						<?php if ($product['discount']): ?>
							<!-- Sleva -->
							<tr>
								<th><?php echo __('discount') ?>:</th>
								<td><?php echo (float)$product['discount'] ?> %</td>
							</tr>
						<?php endif; ?>

						<?php if (SHOW_PRICES && ((float)$product['price_old'] || $product['discount'])): ?>
							<!-- Puvodni cena -->
							<tr>
								<th><?php echo __('old_price') ?>:</th>
								<td>
									<strike id="product-old-price">
										<?php
										$units = '';
										if ($product['area_units_allowed'])
											$units = '/m<sup>2</sup>';

										echo((float)$product['price_old']
											? price($product['price_old'])
											: price_vat(array($product, $product['pricebeforediscount'])));
										echo $units;
										?>
									</strike>
								</td>
							</tr>
						<?php endif; ?>

					</table>


					<div class="priceBox">
						<div>
							<strong>
								<?php
								$units = '';
								if ($product['area_units_allowed'])
									$units = '/m<sup>2</sup>';

                                if($product['price_from']) {
                                    echo "<small>od</small> ";
                                }
								echo price_vat(array($product, $product['price'] + $product['recycling_fee']));
								echo $units;
								?>
							</strong>
							<em><?php echo price_unvat(array($product, $product['price'] + $product['recycling_fee'])) ?>
								bez DPH</em>
						</div>

						<?php /*<a href="#">Mám zadat velikost plochy<br/>přesně nebo s okrajem?</a>*/ ?>
					</div>

					<div class="priceCalculator">
						<?php include __DIR__ . '/attributes.php'; ?>
						<div id="append-calculator"></div>
						<div class="result">
							<div>
								<strong
									id="product-price"><?php echo price($product['price'] + $product['recycling_fee'] + floatval($product['price_transport'])) ?></strong>
								<em><span
										id="product-price-excl-vat"><?php echo price_unvat(array($product, $product['price'] + $product['recycling_fee'] + floatval($product['price_transport']))) ?></span>
									bez DPH</em>
							</div>
							<button type="submit" name="buy" <?php echo isset($productInBasket['id']) ? 'disabled' : '' ?>
									class="btn btn-buy"><?php echo __('add_to_basket') ?></button>
							<span class="cross">x</span>
							<input type="text" name="qty" value="1" size="2" class="span2 qty" autocomplete="off" <?php echo $product['lock_pieces'] ? 'disabled' : '' ?>>
							<br clear="all"/>
						</div>
					</div>
				</div>
		</div>

		<?php if ($product['description']): ?>
			<div class="description">
				<?php echo $product['description'] ?>
			</div>
		<?php endif; ?>
		</form>

		<?php if ($related_products): ?>
			<div class="related-products">
				<h4><?php echo __('related_products') ?>:</h4>

				<ul class="products" data-cols="<?php echo $cols ?>">
					<?php $i = 1;
					foreach ($related_products as $related): ?>
						<li class="cols-<?php echo $i ?> product-id-<?php echo $related['id'] ?>">
							<h3 class="name"><a href="<?php echo url($related) ?>"
												title="<?php echo trim($related['name'] . ' ' . $related['nameext']) ?>"><?php echo trim($related['name'] . ' ' . $related['nameext']) ?></a>
							</h3>
							<?php if (($img = imgsrc($related, null, 5, $alt)) !== null): ?>
								<a href="<?php echo url($related) ?>"
								   title="<?php echo trim($related['name'] . ' ' . $related['nameext']) ?>"
								   class="image">
									<?php if ($related['discount']): ?>
										<span
											class="badge discount badge-important">-<?php echo((float)$related['discount']) ?>
											%</span>
									<?php endif; ?>

									<?php if (($labels = $related->product_labels()) && count($labels)): ?>
										<span class="labels">
		                            <?php foreach ($labels as $label): if ($label->label['name']): ?>
										<span class="label"
											  style="background-color:#<?php echo trim($label->label['color']) ?>"><?php echo $label->label['name'] ?></span>
									<?php endif; endforeach; ?>
		                        </span>
									<?php endif; ?>
									<!-- <img src="<?php echo $img ?>" alt="<?php echo $alt ?>" data-function="verticalImageCenter"> -->
									<img src="<?php echo $img ?>" alt="<?php echo $alt ?>">
								</a>
							<?php endif; ?>

							<span class="price">
						<?php if (SHOW_PRICES && $related['price'] !== null): ?>
							<strong
								class="with-vat"><?php echo price_vat(array($related, $related['price'] + $related['recycling_fee'])) ?></strong>
							<span class="unvat"><span
									class="curr"><?php echo price_vat(array($related, $related['price'] + $related['recycling_fee'])) ?></span> bez DPH</span>
						<?php else: ?>
							&nbsp;
						<?php endif; ?>
		            </span>

							<a href="<?php echo url($related) ?>" class="btn btn-success">Detail produktu</a>

		            <span class="description muted">
		                <?php echo $related['description_short'] ?>
		            </span>
							<hr>
						</li>
						<?php $i++; endforeach; ?>
				</ul>
			</div>
			<script>
				// $(window).ready(function(){
				//     var cols = parseInt($('ul.products').data('cols'));
				//     var default_height = 590;
				//     var height = default_height;
				//     var iteration = 1;

				//     $('ul.products>li').each(function(i){
				//         i = i+1;
				//         $(this).attr('data-iteration', iteration);
				//         if($(this).height() > height) {
				//             height = $(this).height();
				//         }
				//         console.log(cols);
				//         if(i % cols == 0) {
				//             $('ul.products>li[data-iteration="'+iteration+'"]').css('height', height);
				//             height = default_height;
				//             iteration++;
				//         }
				//     });
				// });
			</script>
		<?php endif; ?>

		<?php if (Core::$is_premium && $discounts): ?>
			<div class="quantity-discounts">
				<h4 class="pane-title"><?php echo __('quantity_discounts') ?>:</h4>
				<table class="grid">
					<thead>
					<tr>
						<td class="range"><?php echo __('quantity') ?></td>
						<td class="discount"><?php echo __('discount') ?></td>
						<td class="price"><?php echo __('price') ?></td>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($discounts as $discount): ?>
						<tr>
							<td class="range"><?php echo $discount['range'] ?></td>
							<td class="discount"><?php echo (float)$discount['discount'] ?> %</td>
							<td class="price"><?php echo price_vat(array($product, $discount['price'])) ?></td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		<?php endif; ?>

		<!-- non-image files -->
		<?php echo listFiles($product) ?>

		<!-- bottom files -->
		<?php echo listFiles($product, 0) ?>

		<!-- promo boxes -->
		<div class="promo-boxes" style="margin: 20px 0 0 0;">
			<div class="row-fluid">
				<div class="span4">
					<div class="item item-1">
						<strong><?php echo $tploptions['levnekoberce']['hp_box_1_title'] ?></strong>
						<span><?php echo $tploptions['levnekoberce']['hp_box_1_body'] ?></span>
					</div>
				</div>
				<div class="span4">
					<div class="item item-2">
						<strong><?php echo $tploptions['levnekoberce']['hp_box_2_title'] ?></strong>
						<span><?php echo $tploptions['levnekoberce']['hp_box_2_body'] ?></span>
					</div>
				</div>
				<div class="span4">
					<div class="item item-3">
						<strong><?php echo $tploptions['levnekoberce']['hp_box_3_title'] ?></strong>
						<span><?php echo $tploptions['levnekoberce']['hp_box_3_body'] ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php echo tpl('bottomlinks.php') ?>