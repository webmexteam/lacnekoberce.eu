<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

echo tpl('filter.php', array('page' => $page, 'producers' => $producers, 'features' => $features));
?>
<div class="products products-1cols">
<?php foreach($products as $product): ?>
	<div class="thumbnail<?php echo ((int) $product['promote'] ? ' product-promote' : '')?>">

		<div class="caption">

			<div class="row-fluid">
				<div class="span4">
					<?php if(($img = imgsrc($product, null, 4, $alt)) !== null): ?>
						<div class="picture">
							<a href="<?php echo url($product)?>"><img src="<?php echo $img?>" alt="<?php echo $alt?>" /></a>
						</div>
					<?php elseif($product['discount']): ?>
						<div class="discount-wrap"><span class="discount"><?php echo __('discount')?> <?php echo ((float) $product['discount'])?> %</span></div>
					<?php endif; ?>
				</div>
				<div class="span8">
					<h3 class="name<?php if($product['discount']): ?> with-discount<?php endif?>">
						<a href="<?php echo url($product)?>" title="<?php echo trim($product['name'].' '.$product['nameext'])?>"><?php echo trim($product['name'].' '.$product['nameext'])?></a>
					</h3>

					<?php if($product['discount']): ?>
						<span class="badge discount badge-important">-<?php echo ((float) $product['discount'])?> %</span>
					<?php endif; ?>

					<?php if(($labels = $product->product_labels()) && count($labels)): ?>
						<div class="labels">
							<?php foreach($labels as $label): if($label->label['name']): ?>
								<div class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></div>
							<?php endif; endforeach; ?>
						</div>
					<?php endif; ?>

					<div class="description muted">
						<?php echo $product['description_short']?>
					</div>

					<div class="row-fluid">
						<div class="span6">
							<?php if(SHOW_PRICES && $product['price'] !== null): ?>
								<div class="price">
									<strong><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></strong>
								</div>
							<?php endif; ?>

							<table class="table">
								<?php if(SHOW_PRICES && ((float) $product['price_old'] || $product['discount'])): ?>
									<tr>
										<th><?php echo __('old_price')?>:</th>
										<td><strike><?php echo ((float) $product['price_old'] ? price($product['price_old']) : price_vat(array($product, $product['pricebeforediscount'])))?></strike></td>
									</tr>
								<?php endif; ?>
								<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
									<tr>
										<th><?php echo __('availability')?></th>
										<td><strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $availability['name']?></strong></td>
									</tr>
								<?php endif; ?>
							</table>
						</div>
						<div class="span6">
							<?php /*if(SHOW_PRICES && $product['price'] !== null && (! (int) Core::config('suspend_no_stock') || ! (notEmpty($product['stock']) && $product['stock'] == 0)) ): ?>
								<a href="<?php echo url(PAGE_BASKET, array('buy' => $product['id']))?>" class="button buy"><?php echo __('add_to_basket')?></a>
							<?php endif;*/ ?>
							<a href="<?php echo url($product)?>" class="pull-right btn btn-success btn-large"><?php echo __('detail')?> &raquo;</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
</div>