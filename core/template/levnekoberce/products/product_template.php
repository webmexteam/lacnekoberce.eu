<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
echo tpl('filter.php', array('page' => $page, 'producers' => $producers, 'features' => $features));


?>

<ul class="products" data-cols="<?php echo $cols?>">
	<?php  $i = 1; foreach($products as $product): ?>
    	<li class="cols-<?php echo $i?> product-id-<?php echo $product['id']?>">
			<h3 class="name"><a href="<?php echo url($product)?>" title="<?php echo trim($product['name'].' '.$product['nameext'])?>"><?php echo trim($product['name'].' '.$product['nameext'])?></a></h3>
			<?php if(($img = imgsrc($product, null, 5, $alt)) !== null): ?>
                <a href="<?php echo url($product)?>" title="<?php echo trim($product['name'].' '.$product['nameext'])?>" class="image">
                    <?php if($product['discount']): ?>
                        <span class="badge discount badge-important">-<?php echo ((float) $product['discount'])?> %</span>
                    <?php endif; ?>

                    <?php if(($labels = $product->product_labels()) && count($labels)): ?>
                        <span class="labels">
                            <?php foreach($labels as $label): if($label->label['name']): ?>
                                <span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
                            <?php endif; endforeach; ?>
                        </span>
                    <?php endif; ?>
                    <!-- <img src="<?php echo $img?>" alt="<?php echo $alt?>" data-function="verticalImageCenter"> -->
                    <img src="<?php echo $img?>" alt="<?php echo $alt?>" data-function="verticalImageCenter" >
                </a>
            <?php endif; ?>

            <span class="price">
				<?php if(SHOW_PRICES && $product['price'] !== null): ?>
                    <strong class="with-vat"><?php echo $product['price_from'] ? '<small>od</small> ' : '' ?><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></strong>
                    <span class="unvat"><span class="curr"><?php echo price_unvat(array($product, $product['price']+$product['recycling_fee']))?></span> bez DPH</span>
                <?php else: ?>
                    &nbsp;
                <?php endif; ?>
            </span>

            <a href="<?php echo url($product)?>" class="btn btn-success">Detail produktu</a>

            <span class="description muted">
                <?php echo $product['description_short']?>
            </span>
            <hr>
    	</li>
	<?php $i ++; endforeach; ?>
</ul>

<script>
    // $(window).ready(function(){
    //     var cols = parseInt($('ul.products').data('cols'));
    //     var default_height = 450;
    //     var height = default_height;
    //     var iteration = 1;

    //     $('ul.products>li').each(function(i){
    //         i = i+1;
    //         $(this).attr('data-iteration', iteration);
    //         if($(this).height() > height) {
    //             height = $(this).height();
    //         }
    //         console.log(cols);
    //         if(i % cols == 0) {
    //             $('ul.products>li[data-iteration="'+iteration+'"]').css('height', height);
    //             height = default_height;
    //             iteration++;
    //         }
    //     });
    // });
</script>