<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul class="subpages subpages-<?php echo $position?>">
	<?php foreach($pages as $page): ?>
	<li>
		<a href="<?php echo url($page)?>">
			<?php echo $page['name']?>
			<?php if($page['description_short']): ?>
				<span class="shortdesc muted">
					<?php echo $page['description_short']?>
				</span>
			<?php endif; ?>
		</a>

	</li>
	<?php endforeach; ?>
</ul>