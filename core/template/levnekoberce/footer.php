<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
	<footer id="footer">
		<div class="footer">
			<div class="row-fluid">
				<div class="span3">
					<h5><?php echo $_options['levnekoberce']['footer_menu_1_title']?></h5>
					<?php echo menu(6)?>
				</div>
				<div class="span3">
					<h5><?php echo $_options['levnekoberce']['footer_menu_2_title']?></h5>
					<?php echo menu(7)?>
				</div>
				<div class="span3 kontakt">
					<h5><?php echo $_options['levnekoberce']['footer_menu_3_title']?></h5>
					<?php echo $_options['levnekoberce']['footer_box_3_body']?>
				</div>
				<div class="span3">
					<h5><?php echo $_options['levnekoberce']['footer_menu_4_title']?></h5>
					<img src="<?php echo $_options['levnekoberce']['footer_box_4_image']?>" alt="<?php echo $_options['levnekoberce']['footer_menu_4_title']?>" />
				</div>
			</div>
		</div>
        <div class="subfooter">
        	<p><?php echo preg_replace('/\{year\}/', date('Y'), $footer)?> Copyright &copy; <?php echo date('Y').' '.Core::config('store_name')?></p>
			<p class="powered"<a href="http://www.webmex.cz/" target="_blank">Webmex.cz</a></p>
			</div>
	</footer>
<?php echo (Core::$profiler ? tpl('system/profiler.php') : '')?>