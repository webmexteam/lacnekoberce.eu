<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

?>


<header id="header">
    <div class="userBox">
        <a class="login" href="<?php echo url('customer/login')?>"><?php echo __('login')?></a>
        <a class="reg" href="<?php echo url('customer/register')?>"><?php echo __('register')?></a>
    </div>
    <div class="header">
        <a class="brand" href="<?php echo url(null)?>" title="<?php echo Core::config('store_name')?>">
            <?php echo Core::config('store_name')?>
            <span>&nbsp;</span>
        </a>
        
        <div class="center">
            <p class="phone"><?php echo $_options['levnekoberce']['header_contact']?></p>
            <?php echo search_form()?>
        </div>
        
         <div class="basket">
            <?php if($page = Core::$db->page[PAGE_BASKET]): ?>
                <a href="<?php echo url(PAGE_BASKET)?>" title="<?php echo $page['name']?>">
                	<span class="h5">Nákupní košík</span>
					<?php $total_count = Basket::total_count(); ?>
                    <?php if($count = Basket::count()): ?>
                        <span class="qty"><span>počet:</span> <strong><?php echo $total_count?> ks</strong></span>
                        <span class="price"><span>cena:</span> <strong><?php echo price(Basket::total()->total)?></strong></span>
                    <?php else: ?>
                        <span class="qty"><span>počet:</span> <strong><?php echo $count?></strong></span>
                        <span class="price"><span>cena:</span> <strong>0 Kč</strong></span>
                    <?php endif; ?>  
                </a>
            <?php endif; ?>
        </div>
            
        <div class="navbar">       
	        <?php echo menu(1)?>     
        </div>
	</div>
</header>