<?php defined('WEBMEX') or die('No direct access.');
/**
 * Webmex - http://www.webmex.cz.
 */
?>
<?php if(count($pages)):?>
	<ul class="unstyled">
		<?php foreach($pages as $page): ?>
			<li>
				<a href="<?php echo url($page)?>" title="<?php echo ($page['seo_title']) ? $page['seo_title'] : $page['name']?>">
					<?php echo $page['name']?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif?>