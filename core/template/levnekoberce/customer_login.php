<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="page-customer">
	<?php if(Customer::$logged_in): ?>
		<div class="header">
			<div class="row-fluid">
				<div class="span9">
					<h2 class="customer-name"><?php echo Customer::get('first_name').' '.Customer::get('last_name')?></h2>
					<i class="email"><?php echo Customer::get('email')?></i>
				</div>
				<div class="span3">
					<a href="<?php echo url('customer/logout')?>" class="btn btn-danger pull-right"><?php echo __('logout')?></a>
				</div>
			</div>

			<br>

			<ul class="nav nav-tabs">
				<li><a href="<?php echo url('customer')?>"><?php echo __('orders')?></a></li>
				<li><a href="<?php echo url('customer/settings')?>"><?php echo __('settings')?></a></li>
			</ul>
		</div>

	<?php else: ?>
		<div class="page-header">
			<h1><?php echo __('customer_login')?></h1>
		</div>

		<form action="<?php echo url(true)?>" method="post" class="form customerlogin clearfix">
			<div class="customer-login clearfix">
				<fieldset>
					<?php if(isSet($customer_login_error)): ?>
						<div class="alert alert-block alert-error">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p><?php echo $customer_login_error?></p>
						</div>
					<?php endif; ?>

					<?php if(isSet($customer_login_msg)): ?>
						<div class="alert alert-block alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p><?php echo $customer_login_msg?></p>
						</div>
					<?php endif; ?>

					<?php if(isSet($_SESSION['customer_confirm'])): ?>
						<div class="alert alert-block alert-error">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<p><?php echo __('customer_confirmation_required')?></p>
						</div>
						<?php unset($_SESSION['customer_confirm']); endif; ?>

					<div class="row-fluid">
						<div class="span6">
							<div class="placeholder-input">
								<label for="customer_email"><?php echo __('email')?>:</label>
								<div class="input-prepend">
									<span class="add-on">@</span>
									<input type="text" class="span12" name="customer_email" id="customer_email" value="" class="text">
								</div>
							</div>
						</div>
						<div class="span6">
							<div class="placeholder-input">
								<label for="customer_password"><?php echo __('password')?>:</label>
								<div class="input-prepend">
									<span class="add-on"><i class="icon-lock"></i></span>
									<input type="password" class="span12" name="customer_password" id="customer_password" value="" class="text">
								</div>
							</div>
						</div>
					</div>

					<div class="row-fluid">
						<div class="span6">
							<a href="#lost-password" role="button" data-toggle="modal" class="btn btn-danger"><?php echo __('lost_password')?></a>
						</div>
						<div class="span6">
							<button type="submit" name="login" class="btn btn-success pull-right"><?php echo __('login')?></button>
						</div>
					</div>


					<div id="lost-password" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="myModalLabel"><?php echo __('lost_password')?></h3>
						</div>
						<div class="modal-body">
							<p><?php echo __('lost_password_help')?></p>
							<div class="row-fluid">
								<div class="span6">
									<div class="placeholder-input">
										<label for="customer_email_reset"><?php echo __('email')?>:</label>
										<div class="input-prepend">
											<span class="add-on">@</span>
											<input type="text" class="span12" name="customer_email_reset" id="customer_email_reset" value="" class="text" />
										</div>
									</div>
								</div>
								<div class="span6">
									<label>&nbsp;</label>
									<button type="submit" name="reset_password" class="btn btn-success"><?php echo __('reset_password')?></button>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Zavřít</button>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	<?php endif; ?>
</div>