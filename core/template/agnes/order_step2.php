<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$this->override = true;
$tploptions = Registry::get('template_options');
?>



<form action="<?php echo url(PAGE_ORDER2)?>" method="post" class="order inputs form clearfix">

	<div class="reset"></div>
	<div class="steps row-fluid">
		<div class="span3 step step-1">
			<div class="num">1.</div>
			<div class="text">Košík</div>
			<div class="reset"></div>
		</div>
		<div class="span3 step step-2">
			<div class="num">2.</div>
			<div class="text">Doprava a platba</div>
			<div class="reset"></div>
		</div>
		<div class="span3 step step-3 active">
			<div class="num">3.</div>
			<div class="text">Dodací údaje</div>
			<div class="reset"></div>
		</div>
		<div class="span3 step step-4">
			<div class="num">4.</div>
			<div class="text">Souhrn</div>
			<div class="reset"></div>
		</div>
		<div class="reset"></div>
	</div>
	<div class="reset"></div>

	<div class="row-fluid">
		<div class="span8">
			<?php if(isSet($order_errors)): ?>
				<div class="errors">
					<ul>
						<?php foreach($order_errors as $error): ?>
							<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

			<fieldset class="colleft">
				<legend><?php echo __('billing_address')?></legend>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="first_name"><?php echo __('first_name')?>:</label>
							<input type="text" name="first_name" id="first_name" value="<?php echo $order['first_name']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input required">
							<label for="last_name"><?php echo __('last_name')?>:</label>
							<input type="text" name="last_name" id="last_name" value="<?php echo $order['last_name']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="street"><?php echo __('street')?>:</label>
							<input type="text" name="street" id="street" value="<?php echo $order['street']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input required">
							<label for="city"><?php echo __('city')?>:</label>
							<input type="text" name="city" id="city" value="<?php echo $order['city']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="zip"><?php echo __('zip')?>:</label>
							<input type="text" name="zip" id="zip" value="<?php echo $order['zip']?>" class="text short" />
						</div>
					</div>
					<div class="span6">
						<div class="input required">
							<label for="country"><?php echo __('country')?>:</label>
							<select name="country" id="country">
								<?php foreach(Core::$db->country() as $country): ?>
									<option value="<?php echo $country['name']?>"<?php echo (($order && $order['country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="email"><?php echo __('email')?>:</label>
							<input type="text" name="email" id="email" value="<?php echo $order['email']?>" class="text"<?php echo (Customer::$logged_in ? ' disabled="disabled"' : '')?> />
						</div>
					</div>
					<div class="span6">
						<div class="input<?php echo ($phone_required ? ' required' : '')?>">
							<label for="phone"><?php echo __('phone')?>:</label>
							<input type="text" name="phone" id="phone" value="<?php echo $order['phone']?>" class="text" />
						</div>
					</div>
				</div>

				<label class="buyoncompany">
					<input type="checkbox"> Zboží nakupuji na firmu (IČ)
				</label>

				<div class="buyoncompany-wrap">
					<div class="row-fluid">
						<div class="span6">
							<div class="input">
								<label for="company"><?php echo __('company')?>:</label>
								<input type="text" name="company" id="company" value="<?php echo $order['company']?>" class="text" />
							</div>
						</div>
						<div class="span3">
							<div class="input">
								<label for="company_id"><?php echo __('company_id')?>:</label>
								<input type="text" name="company_id" id="company_id" value="<?php echo $order['company_id']?>" class="text" />
							</div>
						</div>
						<div class="span3">
							<div class="input">
								<label for="company_vat"><?php echo __('company_vat')?>:</label>
								<input type="text" name="company_vat" id="company_vat" value="<?php echo $order['company_vat']?>" class="text" />
							</div>
						</div>
					</div>
				</div>
			</fieldset>

			<label class="jina-dodaci" for="shipping_as_billing">
				<input type="checkbox" name="shipping_as_billing" id="shipping_as_billing" value="1" class="checkbox"<?php echo ($order['ship_street'] ? '' : 'checked="checked"')?> />
				Dodací adresa je shodná s fakturační
			</label>

			<br><br>

			<fieldset class="jina-dodaci-wrap colleft">
				<legend><?php echo __('shipping_address')?></legend>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="ship_first_name"><?php echo __('first_name')?>:</label>
							<input type="text" name="ship_first_name" id="ship_first_name" value="<?php echo $order['ship_first_name']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input required">
							<label for="ship_last_name"><?php echo __('last_name')?>:</label>
							<input type="text" name="ship_last_name" id="ship_last_name" value="<?php echo $order['ship_last_name']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="ship_street"><?php echo __('street')?>:</label>
							<input type="text" name="ship_street" id="ship_street" value="<?php echo $order['ship_street']?>" class="text" />
						</div>
					</div>
					<div class="span6">
						<div class="input required">
							<label for="ship_city"><?php echo __('city')?>:</label>
							<input type="text" name="ship_city" id="ship_city" value="<?php echo $order['ship_city']?>" class="text" />
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						<div class="input required">
							<label for="ship_zip"><?php echo __('zip')?>:</label>
							<input type="text" name="ship_zip" id="ship_zip" value="<?php echo $order['ship_zip']?>" class="text short" />
						</div>
					</div>
					<div class="span6">
						<div class="input required">
							<label for="ship_country"><?php echo __('country')?>:</label>
							<select name="ship_country" id="ship_country">
								<?php foreach(Core::$db->country() as $country): ?>
									<option value="<?php echo $country['name']?>"<?php echo (($order && $order['ship_country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<div class="input">
							<label for="ship_company"><?php echo __('company')?>:</label>
							<input type="text" name="ship_company" id="ship_company" value="<?php echo $order['ship_company']?>" class="text" />
						</div>
					</div>
				</div>
			</fieldset>

			<fieldset class="colleft">
				<legend><?php echo __('order_note')?></legend>

				<div class="row-fluid">
					<div class="span12">
						<div class="input">
							<label for="comment"><?php echo __('comment')?>:</label>
							<textarea name="comment" id="comment" cols="90" rows="4"></textarea>
						</div>
					</div>
				</div>

				<?php if(! Customer::$logged_in): ?>
					<div class="row-fluid">
						<div class="span6">
							<?php if(Core::$is_premium && (int) Core::config('enable_customer_login')): ?>
								<div class="input">

									<label for="create_account">
										<input type="checkbox" name="create_account" id="create_account" value="1" class="checkbox" checked="checked" />
										<?php echo __('create_account')?>
									</label>
								</div>
							<?php endif; ?>
						</div>
						<div class="span6">
							<div class="input">
								<label for="newsletter"">
									<input type="checkbox" name="newsletter" id="newsletter" value="1" class="checkbox" checked="checked" />
									<?php echo __('want_to_receive_newsletter')?>
								</label>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</fieldset>
		</div>
		<div class="span4 ordersidebar">
            <div class="basket orderbasket">
				<div class="tablewrap last">
					<div class="products w100">
						<table>
							<tbody>
								<?php if($basket_products === null || ! count($basket_products)): ?>
								<tr>
									<td colspan="2" class="basketempty"><?php echo __('basket_is_empty')?></td>
								</tr>
								<?php endif; ?>
								
								<?php foreach($basket_products as $product): ?>
								<tr>
									<td class="name">
										<?php echo $product['product']['name'] ?>
									</td>
									<td class="total"><?php echo price_vat(array($product['product'], $product['price'] * $product['qty']))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>

					<div class="delivery">
						<table>
							<tr class="label">
								<td colspan="2">Doprava:</td>
							</tr>
							<tr class="value">
								<td class="name nazev-dopravy"><?php echo $delivery['name']?></td>
								<td class="price cena-dopravy"></td>
							</tr>
						</table>
					</div>

					<div class="payment">
						<table>
							<tr class="label">
								<td colspan="2">Platba:</td>
							</tr>
							<tr class="value">
								<td class="name nazev-platby"><?php echo $payment['name']?></td>
								<td class="price cena-platby"></td>
							</tr>
						</table>
					</div>

					<div class="payment">
						<table>
							<tr class="label">
								<td colspan="2">Doprava a platba:</td>
							</tr>
							<tr class="value">
								<td class="name nazev-platby">Celkem:</td>
								<td class="price cena-platby"><?php echo $dp_price?></td>
							</tr>
						</table>
					</div>

					<hr class="dorovnani">

					<div class="total-price">
						<table>
							<?php if($basket_products !== null && count($basket_products)):
								$total = Basket::total();
							?>
							<?php if($voucher = Basket::voucher()): ?>
								<tr class="voucher">
									<td class="label" colspan="2"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
									<td class="value"><span id="discount"><?php echo '-'.price($total->discount)?></span></td>
								</tr>
							<?php endif; ?>
							<tr>
								<td rowspan="2" class="label">
									Celková cena:
								</td>
								<td class="value td-big">
									<span id="total"><?php echo price($total->total)?></span>
								</td>
							</tr>
							<?php if((int) Core::config('vat_payer')): ?>
								<tr>
									<td class="value td-small">
										<span id="subtotal"><?php echo price($total->subtotal)?> bez DPH</span>
									</td>
								</tr>
							<?php endif; ?>
							
							<?php endif; ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="buttons clearfix">

		<div class="newbasket step-2">
			<div class="buttons orderbuttons">
				<button type="submit" name="submit_order" class="button pokracovat" style="padding-left:40px;">Pokračovat</button>
				<a href="<?php echo url(PAGE_ORDER)?>" class="button back step-2" style="margin-right:-30px;margin-left:30px;"><span>Zpět na výběr dopravy a platby</span></a>
				<div class="podminky">
					<input type="checkbox" name="terms" id="terms" class="checkbox" /> <label for="terms"><?php echo __('i_accept_terms')?></label>
				</div>
			</div>
		</div>
	</div>
</form>

<script>
	$(document).ready(function(){
		$('.buyoncompany-wrap').hide();
		$('.buyoncompany input').change(function(){
			$('.buyoncompany-wrap').toggle();
		});
	});
</script>