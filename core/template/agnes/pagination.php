<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<div class="pagination">
	<ul>	
		<?php for($i = 1; $i <= $total_pages; $i ++){
			if(abs($i - $current_page) > 3 && $i != $total_pages && $i != 1){
				if($i == 2){
					echo '<li class="space"><span>…</span></li>';
				}
				if($i == $total_pages - 1){
					echo '<li class="space"><span>…</span></li>';
				}
				continue;
			}
			
			if($current_page == $i){
				echo '<li class="active"><span>'.$i.'</span></li>';
			}else{
				echo '<li'.($current_page == $i ? ' class="active"' : '').'><a href="'.url(true, array_merge($params, array('page' => $i))).'">'.$i.'</a></li>';	
			}
		} ?>
	</ul> 
</div>