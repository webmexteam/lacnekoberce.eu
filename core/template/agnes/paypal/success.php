<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<div class="paypal">
	<div class="paypal-ok">
		<h1><?php echo __('payment_success')?></h1>
		
		<p><?php echo __('paypal_success')?></p>
	</div>
	
	<a href="<?php echo url(PAGE_ORDER_FINISH, array('finish' => $order['received']), true)?>" class="button"><?php echo __('continue_to_order')?></a>
</div>