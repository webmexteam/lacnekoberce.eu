<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php if(Customer::$logged_in): ?>
<div class="order-customer">
	<h4><?php echo Customer::get('first_name').' '.Customer::get('last_name')?></h4>
	
	<div class="buttons">
		<a href="<?php echo url('customer')?>"><?php echo __('my_account')?></a> |
		<a href="<?php echo url('customer/logout')?>" class="logout"><?php echo __('logout')?></a>
	</div>
</div>

<?php else: ?>
<form action="<?php echo url(true)?>" method="post" class="form customerlogin clearfix">
	<div class="customer-login clearfix">
		<fieldset>
			<legend><?php echo __('customer_login')?></legend>
			
			<?php if(isSet($customer_login_error)): ?>
			<div class="error">
				<p><?php echo $customer_login_error?></p>
			</div>
			<?php endif; ?>
			
			<?php if(isSet($customer_login_msg)): ?>
			<div class="msg">
				<p><?php echo $customer_login_msg?></p>
			</div>
			<?php endif; ?>
			
			<?php if(isSet($_SESSION['customer_confirm'])): ?>
			<div class="msg">
				<p><?php echo __('customer_confirmation_required')?></p>
			</div>
			<?php unset($_SESSION['customer_confirm']); endif; ?>

			<div class="login clearfix">
				<div class="clearfix">
					<div class="input inline">
						<label for="customer_email"><?php echo __('email')?>:</label>
						<input type="text" name="customer_email" id="customer_email" value="" class="text" />
					</div>
					
					<div class="input inline inline-right">
						<label for="customer_password"><?php echo __('password')?>:</label>
						<input type="password" name="customer_password" id="customer_password" value="" class="text" />
					</div>
				</div>
				
				<div class="buttons">
					<button type="submit" name="login" class="loginbtn button"><?php echo __('login')?></button>
				
					<a href="#" class="lost-password"><?php echo __('lost_password')?></a>
				</div>
				
				<div class="lost-password-form">
					<div class="input">
						<label for="customer_email_reset"><?php echo __('email')?>:</label>
						<input type="text" name="customer_email_reset" id="customer_email_reset" value="" class="text" />
					</div>
					
					<div class="buttons">
						<button type="submit" name="reset_password" class="button"><?php echo __('reset_password')?></button>
					</div>
					
					<p><?php echo __('lost_password_help')?></p>
				</div>
			</div>
		</fieldset>
	</div>
</form>
<?php endif; ?>