<?php defined('WEBMEX') or die('No direct access.');



/**

 * Webmex - http://www.webmex.cz.

 */



?><!doctype html>

<html lang="<?php echo Core::$language?>" class="no-js">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	

	<title><?php echo vsprintf(Core::config('title'), $meta['title'] ? $meta['title'] : '')?></title>

	<meta name="description" content="<?php echo $meta['description']?>">

	<meta name="keywords" content="<?php echo $meta['keywords']?>">

	<meta name="generator" content="Webmex <?php echo Core::version?>; (c) 2010-2013 Webmex.cz">

	

	<?php if(Core::config('meta_robots')): ?>

	<meta name="robots" content="<?php echo Core::config('meta_robots')?>">

	<?php endif; ?>

	

	<?php if(Core::config('meta_author')): ?>

	<meta name="author" content="<?php echo Core::config('meta_author')?>">

	<?php endif; ?>

	

	<link rel="shortcut icon" href="<?php echo $base?>favicon.ico">

	<link rel="stylesheet" href="<?php echo url('style', array('v' => (! isSet($_GET['preview']) ? getStyleId() : time()).$v ), true)?>">

	<link rel="stylesheet" media="print" href="<?php echo url('style/less/print.less.css', array('v' => (! isSet($_GET['preview']) ? getStyleId() : time()).$v ), true)?>">

	<link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.css?v=<?php echo $v?>">

	

	<script src="<?php echo $base.APPDIR?>/js/modernizr-1.5.min.js"></script>

	<script src="<?php echo $base.APPDIR?>/js/jquery-1.4.2.min.js"></script>



	<script src="<?php echo $base?>core/template/agnes/js/jquery.selectbox/js/jquery.selectbox-0.2.min.js"></script>

	

	<!--[if (gte IE 6)&(lte IE 8)]>

	<script type="text/javascript" src="<?php echo $base.APPDIR?>/js/selectivizr.js"></script>

	<![endif]-->

	

	<script src="<?php echo url('script/constants/front', array('v' => $v, 't' => time()), true)?>"></script>

	<script src="<?php echo url('script/lang/front/'.Core::$language, array('v' => $v), true)?>"></script>

	

	<script src="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.mousewheel-3.0.4.pack.js?v=<?php echo $v?>"></script>

  	<script src="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.js?v=<?php echo $v?>"></script>

	<script src="<?php echo $base.APPDIR?>/js/plugins.js?v=<?php echo $v?>"></script>

	<script src="<?php echo $base.APPDIR?>/js/util.js?v=<?php echo $v?>"></script>

	<script src="<?php echo $base.APPDIR?>/js/front.js?v=<?php echo $v?>"></script>

	

	<?php foreach(View::$js_files['front'] as $js): ?>

	<script src="<?php echo $base.$js?>"></script>

	<?php endforeach; ?>

	

	<?php if($canonical): ?>

	<link rel="canonical" href="<?php echo $canonical?>" />

	<?php endif; ?>

	

	<?php echo Core::config('html_header')?>

	

	<?php echo tpl('system/analytics.php')?>

	

</head>



<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->

<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->

<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->