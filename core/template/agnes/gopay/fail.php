<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<div class="gopay">
	<div class="gopay-fail">
		<h1><?php echo __('payment_fail')?></h1>
		<p><?php echo __('gopay_fail')?></p>
		
		<?php if($order && $link && ! (int) $order['payment_realized']): ?>
		<p><?php echo __('gopay_fail_retry')?><br />
			<a href="<?php echo $link?>"><?php echo $link?></a>
		</p>
		<?php endif; ?>
	</div>
	
	<?php if($order): ?>
		<?php if((int) Core::config('allow_order_payment_change')): ?>
			<a href="<?php echo url(PAGE_ORDER, array('payment' => 1, 'id' => $order['id'], 'hash' => $order->model->hash()), true)?>" class="button"><?php echo __('select_different_payment')?></a>
		<?php endif; ?>
		
	<a href="<?php echo url(PAGE_ORDER_FINISH, array('finish' => $order['received']), true)?>" class="button"><?php echo __('continue_to_order')?></a>
	<?php endif; ?>
</div>