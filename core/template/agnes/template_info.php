<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$template = array(
	'name' => 'Agnes',
	'version' => '1.0',
	'author' => 'Webmex',
	'www' => 'http://www.webmex.cz'
);