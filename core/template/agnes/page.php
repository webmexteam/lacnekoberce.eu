<?php defined('WEBMEX') or die('No direct access.');



/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

if(Core::$current_page['id'] == $tploptions['agnes']['news_page_id']):
	echo tpl('news/page.php', array('page' => $page, 'subpages' => $subpages));
else:
	echo tpl('toplinks.php', array('page' => $page));
	if(!in_array(Core::$current_page['id'], array(Core::$config['page_basket'],Core::$config['page_order_step1'],Core::$config['page_order_step2'],Core::$config['page_order_finish']))){
		echo breadcrumb();
	}

	if($page['description_short'] && Core::$current_page['id'] == Core::$config['page_index']):?>
		<div class="shortdesc">
			<?php echo $page['description_short']?>
		</div>
	<?php endif;

	// Novinky a promo boxy
	if (Core::$current_page['id'] == Core::$config['page_index']): ?>
		<div id="flags">
			<div class="item">
				<img src="<?php echo url(null)?>core/template/agnes/images/label-1.png" width="30" alt="" />
				<label><span><?php echo $tploptions['agnes']['box_1_title']?></span><?php echo $tploptions['agnes']['box_1_text']?></label>
			</div>
			<div class="item">
				<img src="<?php echo url(null)?>core/template/agnes/images/label-1.png" width="30" alt="" />
				<label><span><?php echo $tploptions['agnes']['box_2_title']?></span><?php echo $tploptions['agnes']['box_2_text']?></label>
			</div>
			<div class="item">
				<img src="<?php echo url(null)?>core/template/agnes/images/label-1.png" width="30" alt="" />
				<label><span><?php echo $tploptions['agnes']['box_3_title']?></span><?php echo $tploptions['agnes']['box_3_text']?></label>
			</div>
		</div>
	<?php else:?>
		<!-- top files -->
		<?php echo listFiles($page, 1)?>

		<!-- right files -->
		<?php echo listFiles($page, 3)?>

		<!-- left files -->
		<?php echo listFiles($page, 2)?>

		<?php if(!in_array(Core::$current_page['id'], array(Core::$config['page_basket'],Core::$config['page_order_step1'],Core::$config['page_order_step2'],Core::$config['page_order_finish']))):?>
		<h1><?php echo (trim($page['seo_title']) ? $page['seo_title'] : $page['name'])?></h1>
		<?php endif?>

		<?php if($page['description_short']): ?>
			<div class="shortdesc">
				<?php echo $page['description_short']?>
			</div>
		<?php endif; ?>

		<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 2): ?>
			<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
				'pages' => $subpages,
				'position' => 'top'
			))?>
		<?php endif; ?>

		<?php if($page['description']): ?>
			<div class="longDesc">
				<?php echo $page['description'].$page_file_content?>
			</div>
		<?php endif; ?>
	<?php endif ?>

	<!-- non-image files -->
	<?php echo listFiles($page)?>

	<!-- bottom files -->
	<?php echo listFiles($page, 0)?>

	<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 1): ?>
		<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
			'pages' => $subpages,
			'position' => 'bottom'
		))?>
	<?php endif; ?>

	<?php echo $page_action_top?>

	<?php if(count($products) || ! empty($_GET['f'])): ?>
		<?php echo tpl('products/'.$product_tpl, array('products_count'=> $products_count, 'products' => $products, 'page' => $page, 'producers' => $producers, 'features' => $features))?>
	<?php endif; ?>


	<?php if(Core::$current_page['id'] == Core::$config['page_index']): ?>
	<div class="reset"></div>
	<!-- top files -->
	<?php echo listFiles($page, 1)?>

	<!-- right files -->
	<?php echo listFiles($page, 3)?>

	<!-- left files -->
	<?php echo listFiles($page, 2)?>

	<?php if(Core::$current_page['id'] != Core::$config['page_index'] && !in_array(Core::$current_page['id'], array(Core::$config['page_basket'],Core::$config['page_order'],Core::$config['page_order_finish']))):?>
	<h1><?php echo (trim($page['seo_title']) ? $page['seo_title'] : $page['name'])?></h1>
	<?php endif?>

	<?php if($page['description_short'] && Core::$current_page['id'] != Core::$config['page_index']): ?>
		<div class="shortdesc">
			<?php echo $page['description_short']?>
		</div>
	<?php endif; ?>

	<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 2): ?>
		<?php echo tpl('subpages_'.$page['subpages'].'.php', array(
			'pages' => $subpages,
			'position' => 'top'
		))?>
	<?php endif; ?>

	<?php if($page['description']): ?>
		<div class="longDesc">
			<?php echo $page['description'].$page_file_content?>
		</div>
	<?php endif; ?>
	<div class="reset"></div>
	<?php if(isset($tploptions['agnes']['news_page_id']) && (int) $tploptions['agnes']['news_page_id'] > 0 && $news_page = Core::$db->page[(int) $tploptions['agnes']['news_page_id']]): ?>
		<div class="news">
			<h3 class="headline">Články a aktuality <a href="<?php echo url($news_page)?>">Zobrazit všechny aktuality</a></h3>
			<div class="wrap">
				<?php
					$_news = Core::$db->page()->where('parent_page', $tploptions['agnes']['news_page_id'])->where('status', 1)->order('id DESC')->limit($tploptions['agnes']['news_hp_limit']);
					if($_news->count()):$i=1;foreach($_news as $news):
				?>
				
				<div class="item<?php echo($_news->count()==$i)?' last':''?>">
					<div class="date"><?php echo date('d. n. Y', $news['last_change'])?></div>
					<h4><a href="<?php echo url($news)?>"><?php echo $news['name']?></a></h4>
				</div>
				
				<?php $i++;endforeach;else:?>
					<p class="no-news">Žádné novinky a články</p>
				<?php endif;?>
			</div>
		</div>
	<?php endif ?>
	<?php endif?>


	<?php echo $page_action?>
	<?php echo tpl('bottomlinks.php')?>
<?php endif; ?>