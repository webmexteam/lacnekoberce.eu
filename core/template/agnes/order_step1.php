<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$this->override = true;
$tploptions = Registry::get('template_options');
?>

<form action="<?php echo url(PAGE_ORDER)?>" method="post" class="form order clearfix">

	<div class="reset"></div>
	<div class="steps row-fluid">
		<div class="span3 step step-1">
			<div class="num">1.</div>
			<div class="text">Košík</div>
			<div class="reset"></div>
		</div>
		<div class="span3 step step-2 active">
			<div class="num">2.</div>
			<div class="text">Doprava a platba</div>
			<div class="reset"></div>
		</div>
		<div class="span3 step step-3">
			<div class="num">3.</div>
			<div class="text">Dodací údaje</div>
			<div class="reset"></div>
		</div>
		<div class="span3 step step-4">
			<div class="num">4.</div>
			<div class="text">Souhrn</div>
			<div class="reset"></div>
		</div>
		<div class="reset"></div>
	</div>
	<div class="reset"></div>

	<div class="row-fluid">
		<div class="span8">
			<?php if(isSet($order_errors)): ?>
				<div class="errors">
					<ul>
						<?php foreach($order_errors as $error): ?>
							<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

            <h4 class="dp-title">Způsob dodání zboží</h4>

				<script>
					var delivery = {};
				</script>

				<div class="dp-box delivery">
					<table>
						<?php if($deliveries): $i=1;foreach($deliveries as $delivery): ?>
							<tr<?php if($i == count($deliveries)):?> class="last"<?php endif?>>
								<td class="name">
									<input type="radio" name="delivery" id="delivery_<?php echo $delivery['id']?>" value="<?php echo $delivery['id']?>" class="checkbox" />
									<label for="delivery_<?php echo $delivery['id']?>"><strong><?php echo $delivery['name']?></strong></label>
								</td>
								<td class="desc">
									&nbsp;
								</td>
							</tr>

							<script>delivery[<?php echo $delivery['id']?>] = '<?php echo $delivery['price']?>';</script>
						<?php $i++; endforeach; endif; ?>

						
					</table>
				</div>

            <h4 class="dp-title">Způsob platby</h4>

				<script>var payment = {};</script>

				<div class="dp-box payment">
					<div class="help" id="payment_help"><?php echo __('payment_help')?></div>
					<table>
						<?php if($payments): $i=1;foreach($payments as $payment): ?>
							<tr<?php if($i == count($payments)):?> class="last"<?php endif?>>
								<td class="name">
									<input type="radio" name="payment" id="payment_<?php echo $payment['id']?>" value="<?php echo $payment['id']?>" class="checkbox" /> 
									<label for="payment_<?php echo $payment['id']?>"><strong><?php echo $payment['name']?></strong></label> 
								</td>
								<td class="desc"><?php echo $payment['info']?></td>
								<td><span class="price"></span></td>
							</tr>

							<script>payment[<?php echo $payment['id']?>] = '<?php echo $payment['price']?>';</script>
						<?php $i++; endforeach; endif; ?>

						
					</table>
				</div>

			<script>
				var delivery_payments = [];
				<?php foreach(Core::$db->delivery_payments() as $dp):
					$dp_data = $dp->as_array();
				?>
				delivery_payments.push(<?php echo json_encode($dp_data)?>);
				<?php endforeach; ?>
			</script>
		</div>
		<div class="span4 ordersidebar">

			<div class="basket orderbasket">
				<div class="tablewrap last">
					<div class="products w100">
						<table>
							<tbody>
								<?php if($basket_products === null || ! count($basket_products)): ?>
								<tr>
									<td colspan="2" class="basketempty"><?php echo __('basket_is_empty')?></td>
								</tr>
								<?php endif; ?>
								
								<?php foreach($basket_products as $product): ?>
								<tr>
									<td class="name">
										<?php echo $product['product']['name'] ?>
									</td>
									<td class="total"><?php echo price_vat(array($product['product'], $product['price'] * $product['qty']))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>

					<div class="delivery">
							<table>
								<tr class="label">
									<td colspan="2">Doprava:</td>
								</tr>
								<tr class="value">
									<td class="name nazev-dopravy">Vyberte způsob dopravy</td>
									<td class="price cena-dopravy"></td>
								</tr>
							</table>
						</div>

						<div class="payment">
							<table>
								<tr class="label">
									<td colspan="2">Platba:</td>
								</tr>
								<tr class="value">
									<td class="name nazev-platby">Vyberte způsob platby</td>
									<td class="price cena-platby"></td>
								</tr>
							</table>
						</div>

						<hr class="dorovnani">
						
						<div class="total-price">
							<table>
								<?php if($basket_products !== null && count($basket_products)):
									$total = Basket::total();
								?>
								<?php if($voucher = Basket::voucher()): ?>
									<tr class="voucher">
										<td class="label" colspan="2"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
										<td class="value"><span id="discount"><?php echo '-'.price($total->discount)?></span></td>
									</tr>
								<?php endif; ?>
								<tr>
									<td rowspan="2" class="label">
										Celková cena:
									</td>
									<td class="value td-big">
										<span id="total"><?php echo price($total->total)?></span>
									</td>
								</tr>
								<?php if((int) Core::config('vat_payer')): ?>
									<tr>
										<td class="value td-small">
											<span id="subtotal"><?php echo price($total->subtotal)?> bez DPH</span>
										</td>
									</tr>
								<?php endif; ?>
								
								<?php endif; ?>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>

	<div class="newbasket step-1">
		<div class="buttons">
			<button type="submit" name="submit_order" class="button pokracovat gotostep2 step-1" style="padding-left:40px;margin-left:20px;">Pokračovat</button>
			<a href="<?php echo url(PAGE_BASKET)?>" class="button back step-1"><span>Zpět do košíku</span></a>
		</div>
	</div>

	<input type="hidden" name="dp_price">
	<input type="hidden" name="total_price">
	<input type="hidden" name="delivery_name">
</form>

<script>
	var _subtotal_excl_vat = <?php echo $total->subtotal?>;
	var _subtotal = <?php echo $total->_total_before_discount?>;
	
	<?php if($voucher): ?>
	var _voucher_value = '<?php echo $voucher['value']?>';
	<?php endif; ?>

	$(document).ready(function(){
		$('.delivery input[type="radio"]').change(function(){
			$('.delivery .input.active').removeClass('active');
			$(this).closest('.input').addClass('active');
			$('#delivery-value').html($(this).parent().find('strong').text());
			$('input[name="delivery_name"]').val($(this).parent().find('strong').text());
			if($(this).parent().find('select').length) {
				$('input[name="delivery_name"]').val($(this).parent().find('strong').text() + ' - ' + $(this).parent().find('select').val());
				$('#delivery-value').html($(this).parent().find('strong').text() + ' - ' + $(this).parent().find('select').val());
			}
			$('.pinfobox').removeClass('active').hide();
		});

		$('.payment input[type="radio"]').change(function(){
			$('.payment .input.active').removeClass('active');
			$(this).closest('.input').addClass('active');
			$('#payment-value').html($(this).parent().find('strong:first').text());
		});

		$('#mesto').change(function(){
			console.log($('#delivery-value').html()+' - '+$(this).val());
			$('#delivery-value').html($(this).parent().find('strong').text()+' - '+$(this).val());
			$('input[name="delivery_name"]').val($(this).parent().find('strong').text()+' - '+$(this).val());
		});

		$('.pinfobox').hide();
		$('.pinfo').click(function(){
			if($(this).parent().css('opacity') == 0.5)
				return false;

			if($(this).parent().find('.pinfobox').hasClass('active')) {
				$(this).parent().find('.pinfobox').removeClass('active').hide();
			}else{
				$(this).parent().find('.pinfobox').addClass('active').show();
			}

			$('.pinfobox').hide();
			if($(this).parent().find('.pinfobox').hasClass('active')) {
				$('.pinfobox').removeClass('active');
				$(this).parent().find('.pinfobox').addClass('active').show();
			}else{
				$('.pinfobox').removeClass('active');
			}
			return false;
		});

		$('.delivery input[type="radio"]').click(function(){
			$(this).closest('.span8').find('tr').removeClass('active');
			$(this).closest('tr').addClass('active');
		});

		$('.payment input[type="radio"]').click(function(){
			$(this).closest('table').find('tr').removeClass('active');
			$(this).closest('tr').addClass('active');
		});
	});
</script>