<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="news">
	<div class="wrap">
		<?php 
			$i=1;
			foreach($pages as $page):
		?>
		
		<div class="item<?php echo(count($pages)==$i)?' last':''?>">
			<div class="date"><?php echo date('d. n. Y', $page['last_change'])?></div>
			<h4><a href="<?php echo url($page)?>"><?php echo $page['name']?></a></h4>
		</div>
		
		<?php 
			$i++;
			endforeach;
		?>
	</div>
</div>