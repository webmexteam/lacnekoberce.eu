<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

?>

<div class="reset"></div>
<div class="steps row-fluid">
	<div class="span3 step step-1">
		<div class="num">1.</div>
		<div class="text">Košík</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-2 active">
		<div class="num">2.</div>
		<div class="text">Doprava a platba</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-3">
		<div class="num">3.</div>
		<div class="text">Dodací údaje</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-4">
		<div class="num">4.</div>
		<div class="text">Souhrn</div>
		<div class="reset"></div>
	</div>
	<div class="reset"></div>
</div>
<div class="reset"></div>

<form action="<?php echo url(PAGE_ORDER)?>" method="post" class="form order clearfix">

	<?php if(isSet($order_errors)): ?>
	<div class="errors">
		<ul>
		<?php foreach($order_errors as $error): ?>
			<li><?php echo $error[1]?> - <?php echo __($error[0])?></li>
		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<div class="clearfix">
		<div class="row-fluid">
			<div class="span8 step-2" style="margin-left:0;">
				<div class="clearfix">
					<fieldset class="colleft">
						<legend><?php echo __('billing_address')?></legend>
						
						<div class="clearfix">
							<div class="input inline required">
								<label for="first_name"><?php echo __('first_name')?>:</label>
								<input type="text" name="first_name" id="first_name" value="<?php echo $order['first_name']?>" class="text" />
							</div>
							
							<div class="input inline inline-right required">
								<label for="last_name"><?php echo __('last_name')?>:</label>
								<input type="text" name="last_name" id="last_name" value="<?php echo $order['last_name']?>" class="text" />
							</div>
						</div>
						
						<div class="input required">
							<label for="street"><?php echo __('street')?>:</label>
							<input type="text" name="street" id="street" value="<?php echo $order['street']?>" class="text" />
						</div>
						
						<div class="clearfix">
							<div class="input inline required">
								<label for="city"><?php echo __('city')?>:</label>
								<input type="text" name="city" id="city" value="<?php echo $order['city']?>" class="text" />
							</div>
							
							<div class="input inline inline-right required">
								<label for="zip"><?php echo __('zip')?>:</label>
								<input type="text" name="zip" id="zip" value="<?php echo $order['zip']?>" class="text short" />
							</div>
							
							<div class="input inline required">
								<label for="country"><?php echo __('country')?>:</label>
								<select name="country" id="country">
								<?php foreach(Core::$db->country() as $country): ?>
									<option value="<?php echo $country['name']?>"<?php echo (($order && $order['country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div class="clearfix">
							<div class="input inline required">
								<label for="email"><?php echo __('email')?>:</label>
								<input type="text" name="email" id="email" value="<?php echo $order['email']?>" class="text"<?php echo (Customer::$logged_in ? ' disabled="disabled"' : '')?> />
							</div>
							
							<div class="input inline inline-right<?php echo ($phone_required ? ' required' : '')?>">
								<label for="phone"><?php echo __('phone')?>:</label>
								<input type="text" name="phone" id="phone" value="<?php echo $order['phone']?>" class="text" />
							</div>
						</div>

						<div class="input">
							<input type="checkbox" name="nafirmu" id="nafirmu" value="0" class="checkbox"><label for="nafirmu">Zboží nakupuji na firmu (IČ)</label> 
						</div>

						<script>
							jQuery(document).ready(function($) {
								$('.nafirmu').hide();

								$('#nafirmu').change(function(){
									$('.nafirmu').toggle();
								});

								$('#shipping_address').hide();
								
								$('#shipping_as_billing').change(function(){
									console.log($(this).is(':checked'))
									if($(this).is(':checked')) {
										$('#shipping_address').hide();
									}else{
										$('#shipping_address').show();	
									}
								});
							});
						</script>

						<div class="nafirmu">
							<div class="input">
								<label for="company"><?php echo __('company')?>:</label>
								<input type="text" name="company" id="company" value="<?php echo $order['company']?>" class="text" />
							</div>
							
							<div class="clearfix">
								<div class="input inline">
									<label for="company_id"><?php echo __('company_id')?>:</label>
									<input type="text" name="company_id" id="company_id" value="<?php echo $order['company_id']?>" class="text" />
								</div>
								
								<div class="input inline inline-right">
									<label for="company_vat"><?php echo __('company_vat')?>:</label>
									<input type="text" name="company_vat" id="company_vat" value="<?php echo $order['company_vat']?>" class="text" />
								</div>
							</div>
						</div>
					
					</fieldset>
					
					<fieldset class="colright">
						<legend><?php echo __('shipping_address')?></legend>
						
						<div class="input">
							<input type="checkbox" name="shipping_as_billing" id="shipping_as_billing" value="1" class="checkbox"<?php echo ($order['ship_street'] ? '' : 'checked="checked"')?> /> 
							<label for="shipping_as_billing"><?php echo __('shipping_as_billing')?></label> 
						</div>
						
						<div id="shipping_address">
							<div class="clearfix">
								<div class="input inline required">
									<label for="ship_first_name"><?php echo __('first_name')?>:</label>
									<input type="text" name="ship_first_name" id="ship_first_name" value="<?php echo $order['ship_first_name']?>" class="text" />
								</div>
								
								<div class="input inline inline-right required">
									<label for="ship_last_name"><?php echo __('last_name')?>:</label>
									<input type="text" name="ship_last_name" id="ship_last_name" value="<?php echo $order['ship_last_name']?>" class="text" />
								</div>
							</div>
							
							<div class="input">
								<label for="ship_company"><?php echo __('company')?>:</label>
								<input type="text" name="ship_company" id="ship_company" value="<?php echo $order['ship_company']?>" class="text" />
							</div>
							
							<div class="input required">
								<label for="ship_street"><?php echo __('street')?>:</label>
								<input type="text" name="ship_street" id="ship_street" value="<?php echo $order['ship_street']?>" class="text" />
							</div>
							
							<div class="clearfix">
								<div class="input inline required">
									<label for="ship_city"><?php echo __('city')?>:</label>
									<input type="text" name="ship_city" id="ship_city" value="<?php echo $order['ship_city']?>" class="text" />
								</div>
								
								<div class="input inline inline-right required">
									<label for="ship_zip"><?php echo __('zip')?>:</label>
									<input type="text" name="ship_zip" id="ship_zip" value="<?php echo $order['ship_zip']?>" class="text short" />
								</div>
								
								<div class="input inline required">
									<label for="ship_country"><?php echo __('country')?>:</label>
									<select name="ship_country" id="ship_country">
									<?php foreach(Core::$db->country() as $country): ?>
										<option value="<?php echo $country['name']?>"<?php echo (($order && $order['ship_country'] == $country['name']) ? ' selected="selected"' : '')?>><?php echo $country['name']?></option>
									<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
				
				<div class="clearfix">
					
					<fieldset class="colright">
						<legend><?php echo __('order_note')?></legend>
						
						<div class="input">
							<label for="comment"><?php echo __('comment')?>:</label>
							<textarea name="comment" id="comment" cols="90" rows="4"></textarea>
						</div>

						<?php if(! Customer::$logged_in): ?>
							<?php if(Core::$is_premium && (int) Core::config('enable_customer_login')): ?>
							<div class="input inline">
								<input type="checkbox" name="create_account" id="create_account" value="1" class="checkbox" checked="checked" /> 
								<label for="create_account"><?php echo __('create_account')?></label> 
							</div>
							<?php endif; ?>
							
							<div class="input inline inline-right">
								<input type="checkbox" name="newsletter" id="newsletter" value="1" class="checkbox" checked="checked" /> 
								<label for="newsletter""><?php echo __('want_to_receive_newsletter')?></label> 
							</div>
						<?php endif; ?>
					</fieldset>
				</div>
			</div>

			<div class="span8 step-1" style="margin-left:0">

				<h4 class="dp-title">Způsob dodání zboží</h4>
				
				<script>var delivery = {};</script>

				<div class="dp-box delivery">
					<table>
						<?php if($deliveries): $i=1;foreach($deliveries as $delivery): ?>
							<tr<?php if($i == count($deliveries)):?> class="last"<?php endif?>>
								<td class="name">
									<input type="radio" name="delivery" id="delivery_<?php echo $delivery['id']?>" value="<?php echo $delivery['id']?>" class="checkbox" />
									<label for="delivery_<?php echo $delivery['id']?>"><strong><?php echo $delivery['name']?></strong></label>
								</td>
								<td class="desc">
									&nbsp;
								</td>
							</tr>

							<script>delivery[<?php echo $delivery['id']?>] = '<?php echo $delivery['price']?>';</script>
						<?php $i++; endforeach; endif; ?>

						
					</table>
				</div>

				<h4 class="dp-title">Způsob platby</h4>

				<script>var payment = {};</script>

				<div class="dp-box payment">
					<div class="help" id="payment_help"><?php echo __('payment_help')?></div>
					<table>
						<?php if($payments): $i=1;foreach($payments as $payment): ?>
							<tr<?php if($i == count($payments)):?> class="last"<?php endif?>>
								<td class="name">
									<input type="radio" name="payment" id="payment_<?php echo $payment['id']?>" value="<?php echo $payment['id']?>" class="checkbox" /> 
									<label for="payment_<?php echo $payment['id']?>"><strong><?php echo $payment['name']?></strong></label> 
								</td>
								<td class="desc"><?php echo $payment['info']?></td>
								<td><span class="price"></span></td>
							</tr>

							<script>payment[<?php echo $payment['id']?>] = '<?php echo $payment['price']?>';</script>
						<?php $i++; endforeach; endif; ?>

						
					</table>
				</div>
			</div>
			<div class="span4">
				<div class="helpme">
					<div class="text1">Potřebujete<br>poradit?</div>
					<div class="text2">Jsme tu pro Vás</div>
					<div class="phone"><?php echo $tploptions['agnes']['basket_phone']?></div>
					<div class="email"><?php echo $tploptions['agnes']['basket_email']?></div>
				</div>

				<div class="basket orderbasket">
					<div class="tablewrap">
						<div class="products w100">
							<table>
								<tbody>
									<?php if($basket_products === null || ! count($basket_products)): ?>
									<tr>
										<td colspan="2" class="basketempty"><?php echo __('basket_is_empty')?></td>
									</tr>
									<?php endif; ?>
									
									<?php foreach($basket_products as $product): ?>
									<tr>
										<td class="name">
											<?php echo $product['product']['name'] ?>
										</td>
										<td class="total"><?php echo price_vat(array($product['product'], $product['price'] * $product['qty']))?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>

						<div class="delivery">
							<table>
								<tr class="label">
									<td colspan="2">Doprava:</td>
								</tr>
								<tr class="value">
									<td class="name nazev-dopravy">Vyberte způsob dopravy</td>
									<td class="price cena-dopravy"></td>
								</tr>
							</table>
						</div>

						<div class="payment">
							<table>
								<tr class="label">
									<td colspan="2">Platba:</td>
								</tr>
								<tr class="value">
									<td class="name nazev-platby">Vyberte způsob platby</td>
									<td class="price cena-platby"></td>
								</tr>
							</table>
						</div>

						<hr class="dorovnani">
						
						<div class="total-price">
							<table>
								<?php if($basket_products !== null && count($basket_products)):
									$total = Basket::total();
								?>
								<tr>
									<td rowspan="2" class="label">
										Celková cena:
									</td>
									<td class="value td-big">
										<span id="total"><?php echo price($total->total)?></span>
									</td>
								</tr>
								<tr>
									<td class="value td-small">
										<span id="subtotal"><?php echo price($total->subtotal)?> bez DPH</span>
									</td>
								</tr>
								<?php endif; ?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			var delivery_payments = [];
			<?php foreach(Core::$db->delivery_payments() as $dp): 
				$dp_data = $dp->as_array();
			?>
			delivery_payments.push(<?php echo json_encode($dp_data)?>);
			<?php endforeach; ?>
		</script>
	</div>

	
	<div class="newbasket step-1">
		<div class="buttons">
			<button type="submit" name="submit_order" class="button pokracovat gotostep2 step-1" style="padding-left:40px;margin-left:20px;">Pokračovat</button>
			<a href="<?php echo url(PAGE_BASKET)?>" class="button back step-1"><span>Zpět</span></a>
		</div>
	</div>

	<div class="newbasket step-2">
		<div class="buttons orderbuttons">
			<button type="submit" name="submit_order" class="button pokracovat" style="padding-left:40px;">Pokračovat</button>
			<a href="#" class="button back step-2" style="margin-right:-30px;margin-left:30px;"><span>Zpět</span></a>
			<div class="podminky">
				<input type="checkbox" name="terms" id="terms" class="checkbox" /> <label for="terms"><?php echo __('i_accept_terms')?></label>
			</div>
		</div>
	</div>
	
</form>

<script>
	var _subtotal_excl_vat = <?php echo $total->subtotal?>;
	var _subtotal = <?php echo $total->_total_before_discount?>;
	
	<?php if($voucher): ?>
	var _voucher_value = '<?php echo $voucher['value']?>';
	<?php endif; ?>
</script>

<script>
	var _subtotal_excl_vat = <?php echo $total->subtotal?>;
	var _subtotal = <?php echo $total->_total_before_discount?>;
	
	<?php if($voucher): ?>
	var _voucher_value = '<?php echo $voucher['value']?>';
	<?php endif; ?>

	$(function(){
	
		$('.bottom-terms').change(function(){
			$('.top-terms').trigger('click');
		});

		$('.top-terms').change(function(){
			$('.bottom-terms').trigger('click');
		});


		$('.delivery input[type="radio"]').click(function(){
			$(this).closest('.span8').find('tr').removeClass('active');
			$(this).closest('tr').addClass('active');
		});

		$('.payment input[type="radio"]').click(function(){
			$(this).closest('table').find('tr').removeClass('active');
			$(this).closest('tr').addClass('active');
		});

		$('.order .step-2').hide();

		$('.gotostep2').click(function(){

			if(!$('.payment tr.active').length){
				alert('Nejdříve vyberte platbu a dopravu.');
				return false;
			}

			if(!$('.delivery tr.active').length){
				alert('Nejdříve vyberte platbu a dopravu.');
				return false;
			}



			$('.order .step-2').show();
			$('.order .step-1').hide();

			$('.steps .active').removeClass('active');
			$('.steps .step-3').addClass('active');

			return false;
		});

		$('.back.step-2').click(function(){
			$('.order .step-2').hide();
			$('.order .step-1').show();
			$('.steps .step-3').removeClass('active');
			$('.steps .step-2').addClass('active');
			return false;
		});
	});
</script>