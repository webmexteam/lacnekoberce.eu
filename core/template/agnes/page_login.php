<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('toplinks.php', array('page' => $page))?>

<!-- top files -->
<?php echo listFiles($page, 1)?>

<!-- right files -->
<?php echo listFiles($page, 3)?>

<!-- left files -->
<?php echo listFiles($page, 2)?>

<h1><?php echo $page['name']?></h1>

<?php if($page['description_short']): ?>
<div class="shortdesc">
	<?php echo $page['description_short']?>
</div>
<?php endif; ?>

<div class="page-protected">
	<?php echo __('page_protected')?>
</div>

<?php echo tpl('customer_login.php')?>

<?php echo tpl('bottomlinks.php')?>