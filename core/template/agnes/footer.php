<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

?>
<div id="footer" class="clearfix">
	<div class="footerTop">
		<div class="wrap">
			<div class="box">
				<h4><?php echo $tploptions['agnes']['menu_1_title']?></h4>
				<?php echo menu(6)?>
				<?php echo nl2br($tploptions['agnes']['menu_1_text'])?>
			</div>
			<div class="box">
				<h4><?php echo $tploptions['agnes']['menu_2_title']?></h4>
				<?php echo menu(7)?>
				<?php echo nl2br($tploptions['agnes']['menu_2_text'])?>
			</div>
			<div class="box">
				<h4><?php echo $tploptions['agnes']['menu_3_title']?></h4>
				<?php echo menu(8)?>
				<?php echo nl2br($tploptions['agnes']['menu_3_text'])?>
			</div>

			<div class="box lastCol">
				<h4><?php echo $tploptions['agnes']['menu_4_title']?></h4>
				<a href="<?php echo $tploptions['agnes']['menu_4_text']?>"><img src="<?php echo url(null)?>core/template/agnes/images/map.png" alt="" style="margin-top: 10px;" /></a>
			</div>
		</div>
	</div>
	<div class="footerBottom">
			<?php if(! Core::$is_premium || ! (int) Core::config('hide_webmex_link')): ?>
			<span class="powered">Powered by <a href="http://www.webmex.cz/" target="_blank">WEBMEX</a></span>
			<?php endif; ?>
			
			<?php if($footer = Core::config('footer')): ?>
				<?php echo preg_replace('/\{year\}/', date('Y'), $footer)?>
				
			<?php else: ?>
				Copyright &copy; <?php echo date('Y').' '.Core::config('store_name')?>
			<?php endif; ?>
	</div>
</div>
<?php echo (Core::$profiler ? tpl('system/profiler.php') : '')?>