<?php if($page['enable_filter']): ?>

<?php 
$_order_options = array('name', 'price', 'default');
$_producers = isSet($_GET['p']) ? (array) $_GET['p'] : array();
$_features = isSet($_GET['f']) ? (array) $_GET['f'] : array();
?>
<form action="<?php echo url(true)?>" method="get">

	<div class="filter">
		<?php if(isSet($_GET['uri']) && ! Core::$use_htaccess): ?>
		<input type="hidden" name="uri" value="<?php echo $_GET['uri']?>" />
		<?php endif; ?>
		
		<?php if(isSet($_GET['q'])): ?>
		<input type="hidden" name="q" value="<?php echo $_GET['q']?>" />
		<?php endif; ?>
		
		<input type="hidden" name="dir-<?php echo strtolower($_order_dir)?>" value="1" />
		<input type="hidden" name="view-<?php echo strtolower($_view)?>" value="1" />
	
		<div class="viewtype">
			<ul>
				<li><?php echo __('show')?>: </li>
				<li><input type="submit" name="view-list" value="<?php echo __('view_list')?>" class="view view-list<?php echo ($_view == 'list' ? ' disabled' : '')?>" /></li>
				<li><input type="submit" name="view-pictures" value="<?php echo __('view_pictures')?>" class="view view-pictures<?php echo ($_view == 'pictures' ? ' disabled' : '')?>" /></li>
			</ul>
		</div>
	
		<?php echo __('order_by')?>: 
		<select name="order">
			<?php foreach($_order_options as $opt): ?>
			<option value="<?php echo $opt?>"<?php echo ($opt == $_order ? ' selected="selected"' : '')?>><?php echo __($opt)?></option>
			<?php endforeach; ?>
		</select>
		
		<input type="submit" name="dir-asc" value="" class="dir dir-asc<?php echo ($_order_dir == 'ASC' ? ' active' : '')?>" />
		<input type="submit" name="dir-desc" value="" class="dir dir-desc<?php echo ($_order_dir == 'DESC' ? ' active' : '')?>" />
	</div>

	<?php if($producers || $features): ?>
	<div class="producers clearfix">
		<a href="#" class="toggle-producers"><?php echo __($features ? 'filter_by_producers_or_features' : 'filter_by_producers')?></a>
		
		<div class="wrap"<?php echo ((! empty($_producers) || ! empty($_features)) ? ' style="display:block;"' : '')?>>
			<div class="producer">
				<input type="checkbox" id="producer_all" <?php echo (empty($_producers) ? ' checked="checked"' : '')?> />
				<label for="producer_all"><?php echo __('all')?></label>
			</div>
			
			<div class="clearfix">
			<?php foreach($producers as $producer): ?>
			<div class="producer">
				<input type="checkbox" name="p[]" id="producer_<?php echo $producer['id']?>" value="<?php echo $producer['id']?>"<?php echo (in_array($producer['id'], $_producers) ? ' checked="checked"' : '')?> />
				<label for="producer_<?php echo $producer['id']?>"><?php echo $producer['name']?></label>
			</div>
			<?php endforeach; ?>
			</div>
			
			<?php if($features): ?>
			<div class="features clearfix">
				<?php foreach($features as $feature_id => $feature): ?>
					<div class="feature">
						<label for="filter-<?php echo $feature_id?>"><?php echo $feature['name']?>:</label> 
						<select name="f[<?php echo $feature_id?>]" id="filter-<?php echo $feature_id?>">
							<option value=""><?php echo __('all')?></option>
							<?php foreach($feature['options'] as $opt_text => $opt): ?>
							<option value="<?php echo $opt['value']?>"<?php echo ((isSet($_features[$feature_id]) && $_features[$feature_id] == $opt['value']) ? ' selected="selected"' : '')?>><?php echo $opt_text?> (<?php echo $opt['count']?>)</option>
							<?php endforeach; ?>
						</select>
					</div>
				<?php endforeach;?>
				</div>
			<?php endif; ?>
			
			<br />
			<button type="submit" class="button"><?php echo __('filter_products')?></button>
			
			<?php if($features): ?>
			<a href="<?php echo url($page)?>" class="button"><?php echo __('filter_reset')?></a>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>

</form>

<?php endif; ?>