<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul class="subpages subpages-<?php echo $position?>">
	<?php foreach($pages as $page): ?>
	<li>
		<a href="<?php echo url($page)?>" class="title"><?php echo $page['name']?></a>
	</li>
	<?php endforeach; ?>
</ul>