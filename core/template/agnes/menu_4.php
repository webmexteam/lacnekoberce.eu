<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><ul>
	<?php foreach($pages as $page): ?>
	<li><a href="<?php echo url($page)?>"><?php echo $page['name']?></a></li>
	<?php endforeach; ?>
</ul>