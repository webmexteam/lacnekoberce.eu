<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

echo tpl('filter.php', array('page' => $page, 'producers' => $producers, 'features' => $features));
?>
<div class="products clearfix products-list">
<table>
	<thead>
		<tr>
			<th class="name"><?php echo __('name')?></th>
			<th class="availability"><?php echo __('availability')?></th>
			<th class="price"><?php echo __('price')?></th>
			<th class="buttons">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($products as $product): ?>
		<tr>
			<td class="name"><a href="<?php echo url($product)?>"><?php echo trim($product['name'].' '.$product['nameext'])?></a></td>
			<td class="availability"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo ($product->availability ? $product->availability['name'] : '&mdash;')?></td>
			<td class="price"><?php echo ((! SHOW_PRICES || $product['price'] === null) ? '&mdash;' : price_vat(array($product, $product['price'] + $product['recycling_fee'])))?></td>
			<td class="buttons">
			<?php if(SHOW_PRICES && $product['price'] !== null && (! (int) Core::config('suspend_no_stock') || ! (notEmpty($product['stock']) && $product['stock'] == 0)) ): ?>
			<a href="<?php echo url(PAGE_BASKET, array('buy' => $product['id']))?>" class="button"><?php echo __('add_to_basket')?></a>
			<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>