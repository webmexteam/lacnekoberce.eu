<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
if (Core::$current_page['id'] != Core::$config['page_index']):
	echo tpl('filter.php', array('page' => $page, 'producers' => $producers, 'features' => $features));
endif;

?>

<?php if (Core::$current_page['id'] == Core::$config['page_index']): ?>
		<!--div id="product-tabs" class="border-bottom">
			<div class="tabs">
				<ul>
					<li class="active"><a href="<?php echo url(NULL)?>">Nejnovější</a></li>
					<li><a href="/nejprodavanejsi-a10">Nejprodávanější</a></li>
					<li><a href="/akce-a11">Akce</a></li>
				</ul>
			</div>
		</div-->
<?php endif ?>

<?php if(count($products)): ?>
<div class="paginationWrap">
	<div class="countProducts">Zobrazuji <?php echo count($products); ?> produktů z <?php echo count($products); ?></div>
	<?php echo pagination($products_count)?>
</div>
<?php endif; ?>

<div class="products clearfix products-<?php echo (isSet($cols) ? $cols : 3)?>cols">
<?php $i = 0; $x=1; foreach($products as $product): ?>
	<?php if($x>3){$x = 1;}?>
	<div class="product<?php echo ((int) $product['promote'] ? ' product-promote' : '').' i'.($i % (isSet($cols) ? $cols : 3))?> p-<?php echo $x?>">	
		<div class="wrap">
			<h3 class="name"><a href="<?php echo url($product)?>"><?php echo trim($product['name'].' '.$product['nameext'])?></a></h3>
			
			<?php if(! $img && $product['discount']): ?>
				<div class="discount-wrap"><span class="discount"><?php echo __('discount')?> <?php echo ((float) $product['discount'])?> %</span></div>
			<?php endif; ?>
				
			<div class="desc">
				<?php if(($labels = $product->product_labels()) && count($labels)): ?>
				<div class="labels">
					<?php foreach($labels as $label): if($label->label['name']): ?>
					<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
					<?php endif; endforeach; ?>
				</div>
				<?php endif; ?>								
			</div>
		</div>

		<?php if(($img = imgsrc($product, null, 3, $alt)) !== null): ?>
			<div class="picture">
				<?php if($product['discount']): ?>
				<span class="discount"><?php echo __('discount')?> <?php echo ((float) $product['discount'])?> %</span>
				<?php endif; ?>
				
				<a href="<?php echo url($product)?>"><img src="<?php echo $img?>" alt="<?php echo $alt?>" /></a>
			</div>
		<?php endif; ?>		
		
		<div class="pricebasket">
			<div class="price">
				<?php if(SHOW_PRICES && $product['price'] !== null): ?>
				<strong><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></strong>
				<?php else: ?>
				&nbsp;
				<?php endif; ?>
						<div class="reset"></div>
		      	<span>
				<?php if(SHOW_PRICES && $product['price'] !== null): ?>
				<?php echo price_unvat(array($product, $product['price'] + $product['recycling_fee']))." bez DPH" ?>
				<?php else: ?>
				&nbsp;
				<?php endif; ?>
				</span>
			</div>
		
			<div class="buttons clearfix">
				<?php if(SHOW_PRICES && $product['price'] !== null && (! (int) Core::config('suspend_no_stock') || ! (notEmpty($product['stock']) && $product['stock'] == 0)) ): ?>
				<a href="<?php echo url(PAGE_BASKET, array('buy' => $product['id']))?>" class="button buy <?php if (Core::$current_page['id'] == Core::$config['page_index']) { echo 'buyhome'; } ?>"><?php echo __('add_to_basket')?></a>
				<?php endif; ?>

			</div>

			<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
				<div class="availability"><strong class="availability-<?php echo $availability['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $availability['name']?></strong></div>
			<?php endif; ?>

			<div class="shortDesc">
			<?php echo $product['description_short']?>
			</div>
		</div>
	</div>
<?php $i ++; $x++; endforeach; ?>
</div>


<?php if(count($products)): ?>
<div class="paginationWrap">
	<div class="countProducts">Zobrazuji <?php echo count($products); ?> produktů z <?php echo count($products); ?></div>
	<?php echo pagination($products_count)?>
</div>
<?php endif; ?>

