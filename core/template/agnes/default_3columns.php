<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="column" id="col2">
	<div class="contentwrap <?php if(Core::$current_page['id'] == Core::$config['page_index']) echo "noShadow" ?>">
		<div class="main">
			<?php echo $content?>
		</div>
	</div>
</div><!--! end of #col2-->

<div class="column" id="col1">
	<?php echo blocks('left')?>
</div><!--! end of #col1-->

<div class="column" id="col3">
	<?php echo blocks('right')?>
</div><!--! end of #col3-->