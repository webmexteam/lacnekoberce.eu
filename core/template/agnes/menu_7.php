<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<ul class="menu l<?php echo ($level - 1)?>">
	<?php $i = 1; foreach($pages as $page): ?>
	<?php $subpages = Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)->order('position ASC')?>
	<li class="<?php if(count($subpages)):?>has-submenu <?php endif?>item-<?php echo $i?> li<?php echo ($level - 1)?> fixhover<?php echo (in_array($page['id'], Core::$active_pages) ? ' active' : '')?>">
		<span class="liwrap<?php echo ($level - 1)?>"><a href="<?php echo url($page)?>"<?php echo ($page['seo_title'] ? ' title="'.$page['seo_title'].'"' : '')?>><?php echo $page['name']?></a></span>
		<?php if(count($subpages)): ?>
			<div class="ul-wrap">
				<?php echo tpl('menu_7.php', array('pages' => $subpages, 'level' => $level + 1))?>
			</div>
		<?php endif; ?>
	</li>
	<?php $i++;endforeach; ?>
</ul>