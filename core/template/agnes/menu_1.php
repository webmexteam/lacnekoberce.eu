<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<ul class="l<?php echo ($level - 1)?>">
	<?php foreach($pages as $page): ?>
	<li class="li<?php echo ($level - 1)?> fixhover<?php echo (in_array($page['id'], Core::$active_pages) ? ' active' : '')?>">
		<span class="liwrap<?php echo ($level - 1)?>"><a href="<?php echo url($page)?>"<?php echo ($page['seo_title'] ? ' title="'.$page['seo_title'].'"' : '')?>><?php echo $page['name']?></a></span>
		<div class="ul-wrap">
		<?php if(count($subpages = Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)->order('position ASC'))): ?>
			<?php echo tpl('menu_1.php', array('pages' => $subpages, 'level' => $level + 1))?>
		<?php endif; ?>
		</div>
	</li>
	<?php endforeach; ?>
</ul>

<?php if($level == 1): ?>
<div class="bar"></div>
<?php endif; ?>