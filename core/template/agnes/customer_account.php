<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div id="customer">

	<div class="header">
		<a href="<?php echo url('customer/logout')?>" class="logout"><?php echo __('logout')?></a>
		
		<h4><?php echo Customer::get('first_name').' '.Customer::get('last_name')?></h4>
		<span class="email"><?php echo Customer::get('email')?></span>
		
		<ul class="tabs">
			<li<?php echo ($active_tab == 'orders' ? ' class="active"' : '')?>><a href="<?php echo url('customer')?>"><?php echo __('orders')?></a></li>
			<li<?php echo ($active_tab == 'settings' ? ' class="active"' : '')?>><a href="<?php echo url('customer/settings')?>"><?php echo __('settings')?></a></li>
		</ul>
	</div>
	
	<?php echo $content?>
</div>