<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="customer">
	<?php if(Customer::$logged_in): ?>
	<div class="customerinfo">
		<small><?php echo __('logged_in_as')?>:</small><br />
		<big><?php echo Customer::get('first_name').' '.Customer::get('last_name')?></big>
		<?php echo Customer::get('email')?>
		
		<ul class="menu">
			<li>&rsaquo; <a href="<?php echo url('customer')?>"><?php echo __('my_account')?></a></li>
			<li>&rsaquo; <a href="<?php echo url('customer/logout')?>"><?php echo __('logout')?></a></li>
		</ul>
	</div>
	
	<?php else: ?>
	<form action="<?php echo url('customer/login')?>" method="post">		
		<fieldset>

			<input type="hidden" name="block_id" value="<?php echo $block->block['id']?>" />
			
			<label for="customer-email"><?php echo __('email')?>:</label>
			<input type="text" name="customer_email" id="customer-email" value="" />
			
			<label for="customer-pass"><?php echo __('password')?>:</label>
			<input type="password" name="customer_password" id="customer-pass" value="" />
			
			<input type="checkbox" name="customer_remember" value="1" class="checkbox" id="customer-remember" />
			<label for="customer-remember"><?php echo __('login_remember')?></label>
			
			<br />
			
			<button type="submit" name="login" class="button"><?php echo __('login')?></button>
		</fieldset>
		
		<p class="links">
			&raquo; <a href="<?php echo url('customer/register')?>"><?php echo __('register')?></a><br />
			&raquo; <a href="<?php echo url('customer/lost_password')?>"><?php echo __('lost_password')?></a>
		</p>
	</form>
	<?php endif; ?>
</div>