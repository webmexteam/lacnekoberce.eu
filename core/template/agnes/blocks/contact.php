<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<div class="contact">
	<form action="#" method="post">		
		<fieldset>
			<?php if($sent): ?>
			<p class="sent"><?php echo __('message_sent')?></p>
			<?php endif; ?>
			
			<?php if($error): ?>
			<p class="error"><?php echo $error?></p>
			<?php endif; ?>
			
			<input type="hidden" name="block_id" value="<?php echo $block->block['id']?>" />
			<input type="hidden" name="bcc_<?php echo $block->block['id']?>" value="0" />
			
			<label for="bc-contact"><?php echo __('your_contact')?>:</label>
			<input type="text" name="contact-contact" id="bc-contact" value="<?php echo @$values['contact-contact']?>" />
			
			<label for="bc-text"><?php echo __('your_message')?>:</label>
			<textarea name="contact-text" id="bc-text" cols="40" rows="5"><?php echo @$values['contact-text']?></textarea>
			
			<?php if($block->config['captcha'] && $captcha = Captcha::get('contact'.$block->block['id'])): ?>
			<div class="bc-captcha" id="bc-captcha-<?php echo $block->block['id']?>">
				<label for="bc-captcha"><?php echo __('captcha_verification_code')?>:</label>
				
				<div class="bc-captchapic">
					<img src="<?php echo $captcha?>" alt="" id="bc-captcha-img" class="bc-code" />
				</div>
				
				<div class="bc-captchainput">
					<input type="text" name="captcha" id="bc-captcha" value="" />
					<a href="#" onclick="(el=document.getElementById('bc-captcha-img')).src=el.src.replace(/\/[\d\.]+$/, '')+'/'+Math.random();return false;" class="bc-reload"><?php echo __('captcha_reload')?></a>
				</div>
			</div>
			<script>
				(function(){
					var captcha = document.getElementById('bc-captcha-<?php echo $block->block['id']?>');
					
					<?php if(empty($values['contact-contact'])): ?>
					captcha.style.display = 'none';
					<?php endif; ?>
					
					$(function(){
						var wrap = $(captcha).parent();
						
						wrap.find('input, textarea').bind('focus', function(){
							$(captcha).slideDown();
						});
						
						wrap.parents('form:first').bind('submit', function(){
							var input = $(this).find('input[name="captcha"]');
							
							if(! input.val()){
								$(captcha).show();
								input.focus();
								return false;
							}
							
							$(this).attr('action', '<?php echo url(true, array('js' => 1))?>');
						});
					});	
				})();
				
				$(function(){ $('input[name="bcc_<?php echo $block->block['id']?>"]').val('<?php echo $block->block['id']?>'); });
			</script>
			<?php endif; ?>
			
			<button type="submit" name="send" class="button"><?php echo __('send_message')?></button>
			
			<input type="text" name="contact-email" value="" class="bc-email" />
		</fieldset>
	</form>
</div>