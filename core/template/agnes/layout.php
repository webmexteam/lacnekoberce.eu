<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('layout_head.php'); ?>

	<div id="container" class="fullwidth">
		<?php echo tpl('header.php'); ?>
		
		<div id="main" class="clearfix">
		
			<?php echo $content?>
		
		</div><!--! end of #main-->
		
		<?php echo tpl('footer.php'); ?>
	</div> <!--! end of #container -->

<?php echo tpl('layout_foot.php'); ?>