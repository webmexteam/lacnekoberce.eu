<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<div class="reset"></div>
<div class="steps row-fluid">
	<div class="span3 step step-1 active">
		<div class="num">1.</div>
		<div class="text">Košík</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-2">
		<div class="num">2.</div>
		<div class="text">Doprava a platba</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-3">
		<div class="num">3.</div>
		<div class="text">Dodací údaje</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-4">
		<div class="num">4.</div>
		<div class="text">Souhrn</div>
		<div class="reset"></div>
	</div>
	<div class="reset"></div>
</div>
<div class="reset"></div>

<div class="basket newbasket">
	<form action="<?php echo url(PAGE_BASKET)?>" method="post">
	
		<?php if(isSet($basket_error)): ?>
		<div class="error">
			<p><?php echo $basket_error?></p>
		</div>
		<?php endif; ?>
		
		<div class="tablewrap">
			<table>
				<thead>
					<tr>
						<td class="picture" colspan="2">Zboží</td>
						<td class="name">Dostupnost</td>
						<td class="quantity"><?php echo __('quantity')?></td>
						<td class="price">Cena za kus<br>bez DPH</td>
						<td class="price">Cena za kus<br>s DPH</td>
						<?php /*<td class="total"><?php echo __('total')?></td>*/?>
						<td class="remove">&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<?php if($basket_products === null || ! count($basket_products)): ?>
					<tr>
						<td colspan="7" class="basketempty"><?php echo __('basket_is_empty')?></td>
					</tr>
					<?php endif; ?>
					
					<?php foreach($basket_products as $product): ?>
					<tr data-productid="<?php echo $product['id'] ?>">
						<td class="picture">
							<?php if(isset($product['attribute_file_id'])): ?>
								<?php if(($img = imgsrcvariant($product['attribute_file_id'], $product['product'], 2)) !== null): ?>
									<div class="image-wrapper"><img src="<?php echo $img?>" alt="" /></div>
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							<?php else: ?>
								<?php if(($img = imgsrc($product['product'], 2)) !== null): ?>
									<div class="image-wrapper"><img src="<?php echo $img?>" alt="" /></div>
								<?php else: ?>
									&nbsp;
								<?php endif; ?>
							<?php endif; ?>
						</td>
						<td class="name">
							<a href="<?php echo url($product['product'])?>"><?php echo $product['product']['name'].' '.$product['product']['nameext']?></a>
							
							<?php 
								$availabilities = array();
								$db_availability = $db_availability_product = Core::$db->availability[(int) $product['product']['availability_id']];
								$availabilities[(int) $product['product']['availability_id']] = (int) $db_availability['days'];
							?>
							<?php if($product['attributes']): ?>
								<?php 
									foreach (explode(';', $product['attributes_ids']) as $attr_id):
									$attribute = Core::$db->product_attributes[(int) $attr_id];
									if($attribute['availability_id'] != NULL) {
										$db_availability = Core::$db->availability[(int) $attribute['availability_id']];
										$availabilities[(int) $attribute['availability_id']] = (int) $db_availability['days'];
									}else{
										$db_availability = $db_availability_product;
									}
								?>
								<div class="attributes">
									<div class="row-fluid">
										<div class="span4 name"><strong><?php echo $attribute['name']?>:</strong></div>
										<div class="span5 value"><?php echo $attribute['value']?></div>
										<div class="span3 dostupnost days-<?php echo $db_availability['days']?>"<?php echo ($db_availability['hex_color']) ? ' style="color:#'.$db_availability['hex_color'].'"' : ""?>>(<?php echo $db_availability['name']?>)</div>
									</div>
								</div>

								<?php endforeach?>
							<?php endif; ?>
							<?php
								$maxAvailabilityId = 0;
								$maxAvailabilityDays = null;
								foreach ($availabilities as $id => $days) {
									if(max($availabilities) == $days) {
										$maxAvailabilityId = $id;
										$maxAvailabilityDays = $days;
									}
								}
								if($maxAvailabilityId > 0) {
									$dostupnost = Core::$db->availability[(int) $maxAvailabilityId];
									$dostupnost_name = $dostupnost['name'];
									if($maxAvailabilityDays == 0) {
										$dostupnost_cls = 'skladem';
									}else{
										$dostupnost_cls = 'days dostupnost-'.$maxAvailabilityDays;
									}
								}else{
									$dostupnost_name = 'Neznámá';
									$dostupnost_cls = 'neznama';
								}
							?>
						</td>
						<td class="availability">
							<strong class="<?php echo $dostupnost_cls?>"><?php echo $dostupnost_name?></strong>
						</td>
						<td class="quantity">
							<div class="qtywrap">
								<input type="text" name="qty[<?php echo $product['id']?>]" class="ajax-qty" value="<?php echo $product['qty']?>" size="2" />
								<span class="qtychange plus"></span>
								<span class="qtychange minus"></span>
							</div>
						</td>
						<td class="price-bez-dph"><?php echo price_unvat(array($product['product'], $product['price']))?></td>

						<td class="price"><?php echo price_vat(array($product['product'], $product['price']))?></td>
						<?php /*<td class="total"><?php echo price_vat(array($product['product'], ($product['price'] * $product['qty'])))?></td>*/?>
						<td class="remove"><a href="<?php echo url(true, array('delete' => $product['id']))?>"></a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				
				<?php if($basket_products !== null && count($basket_products)):
					$total = Basket::total();
				?>
				<tfoot>
					<tr class="total">
						<td>&nbsp;</td>
						<td class="label" colspan="3"><div><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</div></td>
						<td class="value" colspan="2"><div><?php echo price($total->total)?></div></td>
						<td class="remove">&nbsp;</td>
					</tr>

					<?php if((int) Core::config('vat_payer')): ?>
					<tr class="subtotal">
						<td>&nbsp;</td>
						<td class="label" colspan="3"><div><?php echo __('total_excl_vat')?>:</div></td>
						<td class="value" colspan="2"><div><?php echo price($total->subtotal)?></div></td>
						<td class="remove">&nbsp;</td>
					</tr>
					<?php endif; ?>
					
					<?php if($voucher = Basket::voucher()): ?>
					<tr class="subtotal">
						<td>&nbsp;</td>
						<td class="label" colspan="3"><div><?php echo __('voucher')?> (<?php echo $voucher['code']?>, <a href="<?php echo url(true, array('removevoucher' => 1))?>" title="<?php echo __('remove')?>">&times;</a>):</div></td>
						<td class="value" colspan="2"><div><?php echo '-'.price($total->discount)?></div></td>
						<td class="remove">&nbsp;</td>
					</tr>
					<?php endif; ?>
				</tfoot>
				<?php endif; ?>
			</table>
		</div>

		<?php if($basket_products !== null && count($basket_products)): ?>
		
		<?php if(Core::$is_premium && Core::config('enable_vouchers') && ! Basket::voucher()): ?>
		<div class="voucher">
			<h4><?php echo __('discount_voucher')?></h4>
			<p><?php echo __('voucher_info')?></p>
			<fieldset>
				<input type="text" name="voucher" value="" />
				<button type="submit" name="use_voucher" class="button"><?php echo __('use_voucher')?></button>
			</fieldset>
		</div>
		<?php endif; ?>
		
		<div class="buttons clearfix">
			<button type="submit" name="update_qty" class="button update"><?php echo __('update_qty')?></button>
			<button type="submit" name="checkout" class="button checkout">Objednat zboží</button>
			<div class="reset"></div>
		</div>
		<?php endif; ?>
	</form>
</div>

<script>
	jQuery(document).ready(function($) {
		$('.qtychange').click(function(){
			var inp = $(this).parent().find('input');
			var val = parseInt(inp.val());

			if($(this).hasClass('plus')) {
				inp.val(val+1);
			}else if($(this).hasClass('minus') && val >= 2) {
				inp.val(val-1);
			}else{
				inp.val(1);
			}
			inp.trigger('change');
			return false;
		});
	});
</script>