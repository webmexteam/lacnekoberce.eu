<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
  
$pages = Core::$db->page()->where('status', 1)->where('parent_page', $page['id'])->order('position ASC');
?>

<?php if($pages): ?>
<ul>
	<?php foreach($pages as $page):?>
	<li><a href="<?php echo url($page)?>"><?php echo $page['name']?></a>
	<?php echo tpl('sitemap_list.php', array('page' => $page))?>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>