<?php defined('WEBMEX') or die('No direct access.');



/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

?><header id="header">


	<div class="wrapper">
	<div id="topnav" class="clearfix">

		<div class="customer">

			<div class="wrap">
				<?php if(Customer::$logged_in): ?>
					<a href="<?php echo url('/customer')?>" class="register"><?php echo __('my_account')?></a>
					<a href="<?php echo url('customer/logout')?>" class="login"><?php echo __('logout')?></a>
				<?php else: ?>
					<a href="<?php echo url('/customer/register')?>" class="register">Registrace</a> 
					<a href="<?php echo url('/customer/login')?>" class="login">Přihlášení</a>
				<?php endif?>
			</div> 

		</div>

	</div><!--! end of #topnav-->

	

	<div id="banner">

		<div class="wrap">

			<div id="h-1">

			<div class="logo">

				<a href="<?php echo url(null)?>" class="logo" title="<?php echo ((! isSet($_options['default']['show_text_logo']) || $_options['default']['show_text_logo']) ? Core::config('store_name') : '')?>"><span class="logo-img"></span></a>

			</div>

			</div>


			<div class="call">
			     <?php echo $tploptions['agnes']['header_text']?>
			</div>

			<div class="search">
	
				<form action="<?php echo url(PAGE_SEARCH)?>" method="get">
	
					<?php if(Core::$fix_path && ($search_page = Core::$db->page[PAGE_SEARCH])): ?>
	
					<input type="hidden" name="uri" value="<?php echo $search_page['sef_url'].'-a'.$search_page['id']?>" />
	
					<?php endif; ?>
	
					
	
					<fieldset>
	
						<input type="text" name="q" id="search-q" value="" placeholder="hledaný výraz" />
	
						<button type="submit" class="button">&nbsp;</button>
	
					</fieldset>
	
				</form>
	
			</div>


			<?php if($page = Core::$db->page[PAGE_BASKET]): ?>

				<?php if($count = Basket::count()): ?>

				<!--div class="checkout">

					<a href="<?php echo url(PAGE_BASKET)?>"><?php echo __('checkout')?> &rsaquo;</a>

				</div-->

				<?php endif; ?>

			

				<div class="cart">

					<a href="<?php echo url(PAGE_BASKET)?>">

						<?php if($count = Basket::count()): ?>

							<?php $total = Basket::total(); ?>

							<div class="item first"><span class="label">Počet:</span><span class="number"><?php echo Basket::count(); ?> ks</span></div>

							<div class="item"><span class="label">Cena:</span><span class="number price"><?php echo $total->total?> Kč</span></div>

						<?php else:?>

							<div class="item first"><span class="label">Položek:</span><span class="number">0 ks</span></div>

							<div class="item"><span class="label">Cena:</span><span class="number price">0 Kč</span></div>

						<?php endif;?>

						

					</a>

				</div>

			<?php endif; ?>



			

		</div>



	</div><!--! end of #banner-->





	<div id="mainnav">

		<div class="wrap">

			<?php echo menu(1)?>

		</div>

	</div><!--! end of #mainnav-->

	
	</div>
</header>