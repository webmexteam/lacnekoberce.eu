<?php defined('WEBMEX') or die('No direct access.');



/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

echo tpl('toplinks.php', array('page' => $page));
echo breadcrumb(); ?>

<!-- top files -->
<?php echo listFiles($page, 1)?>

<!-- right files -->
<?php echo listFiles($page, 3)?>

<!-- left files -->
<?php echo listFiles($page, 2)?>

<h1><?php echo (trim($page['seo_title']) ? $page['seo_title'] : $page['name'])?></h1>

<?php if($page['description_short']): ?>
	<div class="shortdesc">
		<?php echo $page['description_short']?>
	</div>
<?php endif; ?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 2): ?>
	<?php echo tpl('subpages_news.php', array(
		'pages' => $subpages,
		'position' => 'top'
	))?>
<?php endif; ?>

<?php if($page['description']): ?>
	<div class="longDesc">
		<?php echo $page['description'].$page_file_content?>
	</div>
<?php endif; ?>

<!-- non-image files -->
<?php echo listFiles($page)?>

<!-- bottom files -->
<?php echo listFiles($page, 0)?>

<?php if($page['subpages'] && count($subpages) && $page['subpages_position'] == 1): ?>
	<?php echo tpl('subpages_news.php', array(
		'pages' => $subpages,
		'position' => 'bottom'
	))?>
<?php endif; ?>

<?php echo $page_action_top?>

<?php echo $page_action?>
<?php echo tpl('bottomlinks.php')?>
