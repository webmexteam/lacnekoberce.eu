<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>

<h3>Boxy na úvodní straně:</h3>
<?php echo forminput('text', 'opt[agnes][box_1_title]', $options['agnes']['box_1_title'], array('label' => 'Titulek boxu 1 na úvodní straně'))
		.forminput('text', 'opt[agnes][box_2_title]', $options['agnes']['box_2_title'], array('label' => 'Titulek boxu 2 na úvodní straně'))
		.forminput('text', 'opt[agnes][box_3_title]', $options['agnes']['box_3_title'], array('label' => 'Titulek boxu 3 na úvodní straně'));?>

<div class="clearfix"></div>

<?php echo forminput('textarea', 'opt[agnes][box_1_text]', $options['agnes']['box_1_text'], array('label' => 'Text boxu 3 na úvodní straně'))
	.forminput('textarea', 'opt[agnes][box_2_text]', $options['agnes']['box_2_text'], array('label' => 'Text boxu 3 na úvodní straně'))
	.forminput('textarea', 'opt[agnes][box_3_text]', $options['agnes']['box_3_text'], array('label' => 'Text boxu 3 na úvodní straně')); ?>

<div class="clearfix"></div>

<h3>Novinky a aktuality:</h3>
<?php echo forminput('text', 'opt[agnes][news_page_id]', $options['agnes']['news_page_id'], array('label' => 'ID stránky s novinkama'))
			.forminput('text', 'opt[agnes][news_hp_limit]', $options['agnes']['news_hp_limit'], array('label' => 'Počet novinek na úvodní straně'));?>

<div class="clearfix"></div>

<h3>Hlavička:</h3>
<?php echo forminput('text', 'opt[agnes][header_text]', $options['agnes']['header_text'], array('label' => 'Text nad boxem vyhledávání'));?>

<div class="clearfix"></div>

<h3>Patička:</h3>
<?php echo forminput('text', 'opt[agnes][menu_1_title]', $options['agnes']['menu_1_title'], array('label' => 'Nadpis 1. menu v patičce'))
			.forminput('text', 'opt[agnes][menu_2_title]', $options['agnes']['menu_2_title'], array('label' => 'Nadpis 2. menu v patičce'))
			.forminput('text', 'opt[agnes][menu_3_title]', $options['agnes']['menu_3_title'], array('label' => 'Nadpis 3. menu v patičce'))
			.forminput('text', 'opt[agnes][menu_4_title]', $options['agnes']['menu_4_title'], array('label' => 'Nadpis 4. menu v patičce')) ?>

<div class="clearfix"></div>

<?php echo forminput('textarea', 'opt[agnes][menu_1_text]', $options['agnes']['menu_1_text'], array('label' => 'Text pod 1. menu v patičce'))
			.forminput('textarea', 'opt[agnes][menu_2_text]', $options['agnes']['menu_2_text'], array('label' => 'Text pod 2. menu v patičce'))
			.forminput('textarea', 'opt[agnes][menu_3_text]', $options['agnes']['menu_3_text'], array('label' => 'Text pod 3. menu v patičce'))
			.forminput('textarea', 'opt[agnes][menu_4_text]', $options['agnes']['menu_4_text'], array('label' => 'Odkaz na mapce')) ?>

<div class="clearfix"></div>

<h3>Košík a objednávka:</h3>
<?php echo forminput('text', 'opt[agnes][basket_phone]', $options['agnes']['basket_phone'], array('label' => 'Tel. číslo v nápovědě v košíku'))
			.forminput('text', 'opt[agnes][basket_email]', $options['agnes']['basket_email'], array('label' => 'Email v nápovědě v košíku'))?>