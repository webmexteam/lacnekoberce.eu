<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php echo vsprintf(Core::config('title'), (string) __('maintenance_mode'))?></title>
  
  <meta name="author" content="Daniel Regeci; daniel@regeci.cz">
  <meta name="robots" content="noindex, nofollow">
  
  <base href="<?php echo $base?>">

  <script src="<?php echo APPDIR?>/js/modernizr-1.5.min.js?v=<?php echo $v?>"></script>
  
  <style>
  	body {
  		color: #333;
  		font: 12px sans-serif;
  	}
  	#container {
  		background: #f0f0f0;
  		padding: 20px;
  		margin: 40px;
  	}
  </style>

</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>	   <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>	   <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>	   <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

  <div id="container">
	
	<p><?php echo __('maintenance_mode')?></p>
	
  </div> <!--! end of #container -->
  
</body>
</html>