<?php defined('WEBMEX') or die('No direct access.');
$view->override = true;
/**
 * Webmex - http://www.webmex.cz.
 */

$tploptions = Registry::get('template_options');

 //echo tpl('toplinks.php')?>
 
<?php echo breadcrumb($product); ?>

<div class="productdetail<?php echo ((int) $product['promote'] ? ' productdetail-promote' : '')?>">
	<h1 class="name"><?php echo trim($product['name'].' '.$product['nameext'])?></h1>
	<div class="wrap">
		<!-- top files -->
		<?php echo listFiles($product, 1)?>
		
		<!-- right files -->
		<?php echo listFiles($product, 3, 3)?>
		
		<!-- left files -->
		<?php echo listFiles($product, 2, 'product')?>
		
		<div class="producthead clearfix">
				
			<?php if(($labels = $product->product_labels()) && count($labels)): ?>
			<div class="labels">
				<?php foreach($labels as $label): if($label->label['name']): ?>
				<span class="label" style="background-color:#<?php echo trim($label->label['color'])?>"><?php echo $label->label['name']?></span>
				<?php endif; endforeach; ?>
			</div>
			<?php endif; ?>
					
			<?php if($product['description_short']): ?>
			<div class="shortdesc">
				<?php echo $product['description_short']?>
			</div>
			<?php endif; ?>
						
			<div class="productinfo">
				<?php if((($availability = $product->availability) && $availability['name']) || ($availability = $product['bestAvailability'])): ?>
				<div class="transportBox">
							<div class="title"><strong>Doručení k Vám</strong></div>
							<div class="wrap">
								<?php if($display_stock && ! $attributes_have_stock && (int) $product['stock'] > 0): ?>
									(<?php //echo $product['stock']?> <?php echo __('pcs')?>)
								<?php endif; ?>
								
								<label>Dostupnost</label> <strong class="availability-<?php echo $availability['days']?>days"><?php echo $availability['name']?></strong><br />
								<label>Nejpozději doručíme do data:</label> <strong>23. 5. 2013 (úterý)</strong>			
							</div>					
				</div>
				<?php endif; ?>				
				
				<div class="priceBox">
					<?php if(SHOW_PRICES && $product['price'] !== null && $product['vat']): ?>
						<span class="priceinclvat"><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></span>

						<?php if(SHOW_PRICES && $product['price'] !== null): ?>
							<span class="price"><?php echo "bez DPH ".price_unvat(array($product, $product['price'] + $product['recycling_fee']))?></span>
						<?php endif; ?>
					<?php else: ?>
						<?php if(SHOW_PRICES && $product['price'] !== null): ?>
							<span class="priceinclvat"><?php echo price_vat(array($product, $product['price'] + $product['recycling_fee']))?></span>
						<?php endif?>
					<?php endif; ?>		
				</div>
				
				<form action="<?php echo url(PAGE_BASKET)?>" method="post" class="basket clearfix">
					<?php if($enable_buy && SHOW_PRICES): ?>
					<fieldset class="buy">
						<input type="hidden" name="product_id" value="<?php echo $product['id']?>" />
						
						<button type="submit" name="buy" class="button buy"><?php echo "Vložit do košíku" ?></button>
						<div class="qtywrap">
							<input type="text" name="qty" value="1" size="2"><span class="qtychange plus"></span>
							<span class="qtychange minus"></span>
						</div>
						<div style="text-align: right;width:83px;"><?php echo (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?></div>
						<script>
							jQuery(document).ready(function($) {
								$('.qtychange').click(function(){
									var inp = $(this).parent().find('input');
									var val = parseInt(inp.val());

									if($(this).hasClass('plus')) {
										inp.val(val+1);
									}else if($(this).hasClass('minus') && val >= 2) {
										inp.val(val-1);
									}else{
										inp.val(1);
									}
									inp.trigger('change');
									return false;
								});
							});
						</script>
					</fieldset>
					<?php endif; ?>
										
					<?php if(Core::$is_premium): $i = 0;
					$attr_total_prices = ((int) Core::config('attributes_prices_change') || count($attributes) > 1) ? 0 : 1;
					?>
					
					<script>
						_product_discount = <?php echo (($product['discount'] && ! $product['price_old']) ? (float) $product['discount'] : 0)?>;
						_product_price_before_discount = <?php echo (float) $product['pricebeforediscount']?>;
						_product_price = <?php echo (float) $product['price']?>;
						_product_vat = <?php echo (float) $product['vat']?>;
					</script>
					
					<?php foreach($attributes as $attr_name => $options): ?>
					
					<fieldset class="attribute">
						<label><?php echo $attr_name?>:</label>
						
						<?php if(in_array($attr_name, $attributes_with_stock)): ?>
						<div class="product-variants">
							<?php foreach($options as $opt): ?>
							<label class="product-variant">
								<span class="variant-availability">
									<?php if($opt['availability']): ?>
									<strong class="availability-<?php echo $opt['availability']['days']?>days"<?php echo ($availability['hex_color']) ? ' style="color:#'.$availability['hex_color'].'"' : ""?>><?php echo $opt['availability']['name']?></strong>
									<?php endif; ?>
									
									<?php echo (($display_stock && $opt['stock'] > 0) ? ' ('.$opt['stock'].' '.__('pcs').')' : '')?></span>
									<input type="radio" name="attributes[<?php echo $i?>]" value="<?php echo $opt['id']?>"<?php echo (($opt['default'] && $opt['enable']) ? ' checked="checked"' : '').($opt['enable'] ? '' : ' disabled="disabled"')?> /> <?php echo $opt['value']?>
								
								
								<?php if($opt['price']): ?>
									<?php if($attr_total_prices): ?>
										<small>(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)</small>
										
									<?php else: ?>
										<small>(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)</small>
									<?php endif; ?>
								<?php endif; ?>
							</label>
							
							<script>
								attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
							</script>
							
							<?php if($opt['file_id']): ?>
							<script>
								attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
							</script>
							<?php endif; ?>
							
							<?php endforeach; ?>
						</div>
						<?php else: ?>

						<select name="attributes[]">
							<?php foreach($options as $opt): ?>
							<option value="<?php echo $opt['id']?>"<?php echo ($opt['default'] ? ' selected="selected"' : '')?>>
								<?php echo $opt['value']?>
								
								<?php if($opt['price']): ?>
									<?php if($attr_total_prices): ?>
										(<?php echo price_vat(array($product, $opt['price'] + $product['price']))?>)
										
									<?php else: ?>
										(<?php echo ($opt['price'] > 0 ? '+' : '').price_vat(array($product, $opt['price']))?>)
									<?php endif; ?>
								<?php endif; ?>
							</option>
							
							<script>
								attrPrices(<?php echo $opt['id']?>, <?php echo (float) $opt['price']?>);
							</script>
							
							<?php if($opt['file_id']): ?>
							<script>
								attrFiles(<?php echo $opt['id']?>, <?php echo $opt['file_id']?>);
							</script>
							<?php endif; ?>
							
							<?php endforeach; ?>
						</select>
						<?php endif; ?>
						
					</fieldset>
					<?php $i ++; endforeach; 
					endif; ?>
				</form>

				<ul class="pages">
					<?php foreach($product->product_pages() as $page): ?>
						<li><?php echo ($page->page['menu'] == 3 ? '<span class="manufacturer">'.__('manufacturer').':</span>' : '').pageFullPath($page->page, true)?></li> 
					<?php endforeach; ?>
					
					<?php if($product['discount']): ?>
						<li class="discount"><span><?php echo __('discount')?>:</span><strong><?php echo (float) $product['discount']?> %</strong></li>
					<?php endif; ?>
					
					<?php if(SHOW_PRICES && ((float) $product['price_old'] || $product['discount'])): ?>
						<li class="oldprice"><span><?php echo __('old_price')?>:</span><del id="product-old-price"><?php echo ((float) $product['price_old'] ? price($product['price_old']) : price_vat(array($product, $product['pricebeforediscount'])))?></del></li>
					<?php endif; ?>
					
					<?php if(SHOW_PRICES && $product['recycling_fee']): ?>
						<li class="recycling_fee"><span><?php echo __('recycling_fee')?>:</span><?php echo price_vat(array($product, $product['recycling_fee']))?></li>
					<?php endif; ?>
					
					<?php if(SHOW_PRICES && $product['copyright_fee']): ?>
						<li class="copyright_fee"><span><?php echo __('copyright_fee')?>:</span><?php echo price_vat(array($product, $product['copyright_fee']))?></li>
					<?php endif; ?>

					<?php if($product['show_sku'] && $product['sku']): ?>
						<li class="sku"><span class="sku"><?php echo __('sku')?>:</span> <?php echo $product['sku']?></li>
					<?php endif; ?>
					
					<?php if($product['show_ean13'] && $product['ean13']): ?>
						<li class="ean"><span class="ean"><?php echo __('ean')?>:</span> <?php echo $product['ean13']?></li>
					<?php endif; ?>
					
					
					<?php if($product['guarantee']): ?>
						<li class="guarantee"><span><?php echo __('guarantee')?>:</span><?php echo $product['guarantee'].(is_numeric($product['guarantee']) ? ' '.__('months_short') : '')?></li>
					<?php endif; ?>
				</ul>

				<?php if(count($features)): ?>
				<div class="features">
					<table>
						<tbody>
						<?php foreach($features as $feature): ?>
							<tr>
								<td class="featurename"><?php echo $feature['name']?>:</td>
								<td><?php echo $feature['value'].' '.$feature['unit']?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

		
	<div class="sharelinks">
	<?php echo Core::config('sharelinks'); ?>
	</div>

	<div id="flags">
		<div class="item">
			<img src="<?php echo url(null)?>core/template/agnes/images/label-1.png" width="30" alt="" />
			<label><span><?php echo $tploptions['agnes']['box_1_title']?></span><?php echo $tploptions['agnes']['box_1_text']?></label>
		</div>
		<div class="item">
			<img src="<?php echo url(null)?>core/template/agnes/images/label-1.png" width="30" alt="" />
			<label><span><?php echo $tploptions['agnes']['box_2_title']?></span><?php echo $tploptions['agnes']['box_2_text']?></label>
		</div>
		<div class="item">
			<img src="<?php echo url(null)?>core/template/agnes/images/label-1.png" width="30" alt="" />
			<label><span><?php echo $tploptions['agnes']['box_3_title']?></span><?php echo $tploptions['agnes']['box_3_text']?></label>
		</div>
	</div>
	<div class="reset"></div>

	<div class="description">
		<h2><?php echo trim($product['name'].' '.$product['nameext'])?></h2>
		
		<?php echo $product['description']?>
	</div>
		
	<?php if(Core::$is_premium && $attributes && $product['list_attributes']): ?>
	<div class="attributes-list">
			<h4 class="pane-title"><?php echo __('available_options')?>:</h4>
			
			<?php foreach($attributes as $attr_name => $options): ?>
			<table class="grid attrs">
				<caption><?php echo $attr_name?></caption>
				<tbody>
					<?php foreach($options as $option): ?>
					<tr>
						<td class="attr-value">
							<?php echo $option['value']?>
							
							<?php if($attributes_show_sku && $option['sku']): ?>
							<span class="sku"><?php echo __('sku').': '.$option['sku']?></span>
							<?php endif; ?>
							
							<?php if($attributes_show_ean13 && $option['ean13']): ?>
							<span class="ean13"><?php echo __('ean').': '.$option['ean13']?></span>
							<?php endif; ?>
						</td>
						<td class="attr-price"><?php echo ($option['price'] ? ($option['price'] > 0 ? '+' : '').price_vat(array($product, $option['price'])) : '')?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if($related_products): ?>
	<div class="related-products">
		<h4 class="pane-title"><span><?php echo __('related_products')?></span></h4>
		
		<div class="clearfix">
			<?php foreach($related_products as $i => $related): ?>
			<div class="related<?php echo (($i + 1) % 3 === 0 ? ' related-last' : '')?>">
				<div class="related-title"><a href=""><?php echo limit_words($related['name'], 5)?></a></div>
			
				<div class="picture">
					<a href="<?php echo url($related)?>"><img src="<?php echo imgsrc($related)?>" alt="<?php echo limit_words($related['name'], 5)?>" /></a>
				</div>
				
				<div class="pricebasket">
					<div class="prices">
						<div class="price">
						<strong><?php echo price_vat($related)?></strong>
						
				      	<span>
						<?php if(SHOW_PRICES && $related['price'] !== null): ?>
						<?php echo price_unvat($related)." bez DPH" ?>
						<?php else: ?>
						&nbsp;
						<?php endif; ?>
						</span>	
						</div>					
						<a href="<?php echo url($related)?>">Detail</a>
					</div>

					<?php if((($availability = $related->availability) && $availability['name']) || ($availability = $related['bestAvailability'])): ?>
						<div class="availability"><strong class="availability-<?php echo $availability['days']?>days"><?php echo $availability['name']?></strong></div>
					<?php endif; ?>
		
					<div class="shortDesc">
					<p><?php echo $related['description_short']?></p>
					</div>				
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>	

	<?php if(Core::$is_premium && $discounts): ?>
	<div class="quantity-discounts">
		<h4 class="pane-title"><?php echo __('quantity_discounts')?>:</h4>
		<table class="grid">
			<thead>
				<tr>
					<td class="range"><?php echo __('quantity')?></td>
					<td class="discount"><?php echo __('discount')?></td>
					<td class="price"><?php echo __('price')?></td>
				</tr>
			</thead>
			<tbody>
			<?php foreach($discounts as $discount): ?>
				<tr>
					<td class="range"><?php echo $discount['range']?></td>
					<td class="discount"><?php echo (float) $discount['discount']?> %</td>
					<td class="price"><?php echo price_vat(array($product, $discount['price']))?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php endif; ?>
	
	<!-- non-image files -->
	<?php echo listFiles($product)?>
	
	<!-- bottom files -->
	<?php echo listFiles($product, 0)?>
</div>

<?php echo tpl('bottomlinks.php')?>