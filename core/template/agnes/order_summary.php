<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><div class="ordersummary clearfix">

<div class="reset"></div>
<div class="steps row-fluid">
	<div class="span3 step step-1">
		<div class="num">1.</div>
		<div class="text">Košík</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-2">
		<div class="num">2.</div>
		<div class="text">Doprava a platba</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-3">
		<div class="num">3.</div>
		<div class="text">Dodací údaje</div>
		<div class="reset"></div>
	</div>
	<div class="span3 step step-4 active">
		<div class="num">4.</div>
		<div class="text">Souhrn</div>
		<div class="reset"></div>
	</div>
	<div class="reset"></div>
</div>
<div class="reset"></div>

	<div class="wrap clearfix">
		<h4><?php echo __('your_order')?></h4>
		<ul>
			<li><span><?php echo __('order_id')?>:</span>#<?php echo $order['id']?></li>
			<li><span><?php echo __('received')?>:</span><?php echo fdate($order['received'], true)?></li>
			
			<?php if($order['status'] > 1): ?>
			<li><span><?php echo __('status')?>:</span><?php echo __('status_'.Core::$def['order_status'][$order['status']])?></li>
			<?php endif; ?>
			
			<?php if($order['track_num']): ?>
			<li><span><?php echo __('track_num')?>:</span><?php echo $order['track_num']?></li>
			<?php endif; ?>
		</ul>
	</div>

	<div class="wrap clearfix">
		<div class="colleft">
			<h4><?php echo __('billing_address')?></h4>
			<ul>
				<li><span><?php echo __('first_name')?>:</span><?php echo $order['first_name']?></li>
				<li><span><?php echo __('last_name')?>:</span><?php echo $order['last_name']?></li>
				<li><span><?php echo __('company')?>:</span><?php echo $order['company']?></li>
				<li><span><?php echo __('company_id')?>:</span><?php echo $order['company_id']?></li>
				<li><span><?php echo __('company_vat')?>:</span><?php echo $order['company_vat']?></li>
				<li><span><?php echo __('street')?>:</span><?php echo $order['street']?></li>
				<li><span><?php echo __('city')?>:</span><?php echo $order['city']?></li>
				<li><span><?php echo __('zip')?>:</span><?php echo $order['zip']?></li>
				<li><span><?php echo __('country')?>:</span><?php echo $order['country']?></li>
			</ul>
		</div>
		
		<div class="colright">
			<h4><?php echo __('shipping_address')?></h4>
			
			<?php if($order['ship_street']): ?>
			<ul>
				<li><span><?php echo __('first_name')?>:</span><?php echo $order['ship_first_name']?></li>
				<li><span><?php echo __('last_name')?>:</span><?php echo $order['ship_last_name']?></li>
				<li><span><?php echo __('company')?>:</span><?php echo $order['ship_company']?></li>
				<li><span><?php echo __('street')?>:</span><?php echo $order['ship_street']?></li>
				<li><span><?php echo __('city')?>:</span><?php echo $order['ship_city']?></li>
				<li><span><?php echo __('zip')?>:</span><?php echo $order['ship_zip']?></li>
				<li><span><?php echo __('country')?>:</span><?php echo $order['ship_country']?></li>
			</ul>
			<?php else: ?>
			<p><?php echo __('shipping_as_billing')?></p>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="wrap clearfix">
		<div class="colleft">
			<h4><?php echo __('contact_info')?></h4>
			<ul>
				<li><span><?php echo __('email')?>:</span><?php echo $order['email']?></li>
				<li><span><?php echo __('phone')?>:</span><?php echo $order['phone']?></li>
				<li><span><?php echo __('comment')?>:</span><?php echo nl2br($order['comment'])?></li>
			</ul>
		</div>
		
		<div class="colright">
			<h4><?php echo __('delivery_payment')?></h4>
			<ul>
				<li><span><?php echo __('delivery')?>:</span><?php echo $order->delivery['name']?></li>
				<li><span><?php echo __('payment')?>:</span><?php echo $order->payment['name']?></li>

                                <?php if(Core::config('account_num') && strlen(Core::config('account_num')) > 0): ?>
				<li><span><?php echo __('account_num')?>:</span><?php echo Core::config('account_num')?></li>
				<?php endif; ?>
                                
				<?php if((int) $order['payment_realized']): ?>
				<li><span><?php echo __('payment_realized')?>:</span><?php echo fdate($order['payment_realized'], true)?></li>
				<?php endif; ?>
				
				<?php if((int) $order['payment_realized'] && $order['payment_session']): ?>
				<li><span><?php echo __('payment_session')?>:</span><?php echo $order['payment_session']?></li>
				<?php endif; ?>
				
				<?php if((int) Core::config('allow_order_payment_change') && ! (int) $order['payment_realized'] && $order['status'] == 1): ?>
				<li><span>&nbsp;</span><a href="<?php echo url(PAGE_ORDER, array('payment' => 1, 'id' => $order['id'], 'hash' => $order->model->hash()), true)?>"><?php echo __('select_different_payment')?></a></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
	
	<div class="wrap basket clearfix">
		<h4><?php echo __('products')?></h4>
		
		<div class="tablewrap">
			<table>
				<tbody>
					<?php 
					$products = $order->order_products();
					
					if(Core::$db_inst->_type == 'sqlite'){
						$products->order('rowid ASC');
					}
					
					foreach($products as $product): ?>
					<tr>
						<td class="name">
							<?php echo $product['name'].' '.$product['nameext']?>
						</td>
						<td class="price price_qty"><?php echo $product['quantity']?> &times; <?php echo price_vat($product['price'], $product['vat'], $order['currency'])?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				
				<tfoot>
					<?php if((int) Core::config('vat_payer')): ?>
					<tr class="subtotal">
						<td class="label"><?php echo __('total_excl_vat')?>:</td>
						<td class="value"><span id="subtotal"><?php echo price($order['total_price'] + price_unvat($order['delivery_payment'], VAT_DELIVERY)->price, $order['currency'])?></span></td>
					</tr>
					<?php endif; ?>
					
					<?php if($order['voucher_id'] && ($voucher = $order->voucher) && $voucher['id']): ?>
					<tr class="voucher">
						<td class="label"><?php echo __('voucher')?> (<?php echo $voucher['code']?>):</td>
						<td class="value"><span id="discount"><?php echo '-'.price(estPrice($voucher['value'], $order->model->getTotal_incl_vat(true)), $order['currency'])?></span></td>
					</tr>
					<?php endif; ?>
					
					<tr class="total">
						<td class="label"><?php echo __((int) Core::config('vat_payer') ? 'total_incl_vat' : 'total')?>:</td>
						<td class="value"><span id="total"><?php echo price($order['total_incl_vat'], $order['currency'])?></span></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

</div>
