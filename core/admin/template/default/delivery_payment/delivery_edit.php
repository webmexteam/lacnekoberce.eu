<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><style>
	.inline .inputwrap {
		width: 100px !important;
	}
</style>

<form class="form" action="<?php echo url('admin/settings/delivery/'.$delivery['id'])?>" method="post">

	<h2><a href="<?php echo url('admin/settings/delivery_payment')?>"><?php echo __('delivery_payment')?></a> &rsaquo; <?php echo $delivery['name']?></h2>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="clearfix">
		<fieldset style="width:350px">
			<?php echo forminput('text', 'name', $delivery['name'])
      .forminput('text', 'ord', $delivery['ord'],  array('cls' => 'short right', 'label' => __('delivery_payment_order')))
			.forminput('select', 'driver', $delivery['driver'], array('options' => $drivers))
			.forminput('text', 'price', $delivery['price'], array('cls' => 'short right'))?>
		</fieldset>

		<fieldset style="width:350px">
			<h3><?php echo __('restrictions')?></h3>
			<?php echo forminput('select', 'customer_group_id', $delivery['customer_group_id'], array('label' => __('only_for_group'), 'options' => $groups))?>

			<div class="inline">
			<?php echo forminput('text', 'weight_min', $delivery['weight_min'], array('cls' => 'short right'))
			.forminput('text', 'weight_max', $delivery['weight_max'], array('cls' => 'short right'))?>
			</div>

			<div class="inline">
			<?php echo forminput('text', 'price_min', $delivery['price_min'], array('cls' => 'short right'))
			.forminput('text', 'price_max', $delivery['price_max'], array('cls' => 'short right'))?>
			</div>
		</fieldset>
	</div>

	<div class="buttons bbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>


</form>