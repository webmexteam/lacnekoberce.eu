<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<style>
	.topbar .button {
		float: right;
	}
	.topbar .inputwrap {
		width: 150px;
		float: right;
		margin: 0 10px 0 0;
	}
	.topbar .inputwrap label {
		display: none !important;
	}
	.form .status {
		line-height: 22px !important;
	}
	.form .status .input-checkbox {
		vertical-align: middle;
		margin: -1px 3px 0 0;
	}
	td.s-1 {
		background-color: #ffebed !important;
	}
	td.s-2 {
		background-color: #e7ffe3 !important;
	}
	td.s-3 {
		background-color: #e9e9e9 !important;
	}
	.printbar {
		background: #f0f0f0;
		padding: 10px 5px;
		margin: 5px 0 0 0;
		color: #aaa;
	}
	.printbar a {
		color: #444;
		margin: 0 5px;
	}
	.printbar .inputwrap {
		margin: 0 5px !important;
	}
	.printbar select {
		width: auto !important;
		display: inline !important;
	}
	.printbar label {
		display: none;
	}
	.inv-type-proforma {
		background-color: #f0f0f0;
	}
	span.proforma {
		color: #777;
		margin-right: 5px;
	}
</style>

<a href="<?php echo url('admin/invoices/edit/0')?>" class="addbtn"><?php echo __('new_invoice')?></a>


<h2><a href="<?php echo url('admin/orders')?>"><?php echo __('orders')?></a> &rsaquo; <?php echo __('invoices')?></h2>

<?php if(! Core::$is_premium): ?>
<p class="premium-info"><?php echo __('premium_info')?></p>
<?php endif; ?>

<form class="form" action="<?php echo url('admin/invoices')?>" method="get">
	<?php if(Core::$fix_path): ?>
	<input type="hidden" name="uri" value="<?php echo Core::$orig_uri?>" />
	<?php endif; ?>
	
	<div class="gridsearch">
		<fieldset class="clearfix label-left">
			<?php echo forminput('text', 'search', ! empty($_GET['search']) ? $_GET['search'] : '')
			.forminput('select', 'where', ! empty($_GET['where']) ? $_GET['where'] : '', array('options' => array(
				'all' => __('all'),
				'invoice_num' => __('invoice_num'),
				'order_id' => __('order_id'),
				'customer_name' => __('customer'),
				'company_id' => __('company_id'),
			)))?>
			
			<button type="submit" class="button"><?php echo __('search')?> &rsaquo;</button>
		</fieldset>
	</div>
</form>

<form class="form gridform" action="<?php echo url('admin/invoices')?>" method="post">

	<div class="buttons topbar clearfix">
		<?php echo gridpagination($total_count)?>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
		<?php echo forminput('select', 'new_status', '', array('options' => $status_options))?>
	</div>
	
	<table class="datagrid">
		<thead>
			<tr>
				<td width="100" class="<?php echo ($order_by == 'invoice_num' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'invoice_num', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('invoice_num')?></a></td>
				<td width="100" class="<?php echo ($order_by == 'order_id' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'order_id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('order')?></a></td>
				<td class="<?php echo ($order_by == 'customer' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'customer', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('customer')?></a></td>
				<td width="100" class="<?php echo ($order_by == 'company_id' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'company_id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('company_id')?></a></td>
				<td width="100" class="<?php echo ($order_by == 'date_issue' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'date_issue', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('invoice_issue_date')?></a></td>
				<td width="100" class="<?php echo ($order_by == 'date_due' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'date_due', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('invoice_due_date')?></a></td>
				<td width="100" class="<?php echo ($order_by == 'status' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'status', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('status')?></a></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>
		
		<tbody>
			<?php if(! count($invoices)): ?>
				<tr class="norecords">
					<td colspan="8"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>
			
			<?php foreach($invoices as $invoice): 
				$customer = $invoice['customer_name'];
				
				if(! $customer && preg_match('/^([^\n]*)\n/', $invoice['customer'], $m)){
					$customer = $m[1];
				}
			?>
				<tr>
					<td class="inv-type-<?php echo $invoice['type']?>">
						
						<?php if($invoice['type'] == 'proforma'): ?>
						<span class="proforma">P</span>
						<?php endif; ?>
					
						<a href="<?php echo url('admin/invoices/view/'.$invoice['id'])?>"><?php echo $invoice['invoice_num']?></a>
					</td>
					
					<?php if($invoice['order_id']): ?>
						<td><a href="<?php echo url('admin/orders/edit/'.$invoice['order_id'])?>"><?php echo $invoice['order_id']?></a></td>
					<?php else: ?>
						<td>&nbsp;</td>
					<?php endif; ?>
					
					<td><?php echo $customer?></td>
					<td><?php echo $invoice['company_id']?></td>
					<td><?php echo fdate((int) $invoice['date_issue'])?></td>
					<td><?php echo fdate((int) $invoice['date_due'])?></td>
					
					<td class="input status s-<?php echo $invoice['status']?>">
						<input type="checkbox" name="invoices[]" value="<?php echo $invoice['id']?>" class="input-checkbox" />
						<?php echo __('invoice_status_'.$invoice['status'])?>
					</td>
					
					<td class="actions">
						<a href="<?php echo url('admin/invoices/download/'.$invoice['id'])?>" class="download" title="<?php echo __('download')?>">&nbsp;</a>
						<a href="<?php echo url('admin/invoices/edit/'.$invoice['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/invoices/delete/'.$invoice['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<div class="buttons buttons-foot clearfix">
		<?php echo gridpagination($total_count)?>
	</div>
	
	<div class="printbar">
		<?php echo forminput('select', 'export', '', array(
			'options' => $export_options
		))?>
	</div>

</form>

<script>
$(function(){
	$('select[name="export"]').bind('change', function(){
		var e = $(this).val();
	
		if(e){
			window.location = '<?php echo url('admin/invoices/export/')?>'+e;
		}
	});
});
</script>