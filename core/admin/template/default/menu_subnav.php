<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php foreach(Menu::$items as $item): if(Menu::countSubitems($item)): ?>
<ul class="subnav clearfix" id="subnav-<?php echo $item['key']?>">
	<?php foreach($item as $key => $sub_item): if($key{0} == '_'): ?>

		<?php if($sub_item['premium'] && ! Core::$is_premium): ?>
			<li><span class="premium"><?php echo $sub_item['text']?></span></li>

		<?php else: ?>
			<li class="<?php echo $sub_item['cls']?>"><a href="<?php echo $sub_item['url']?>"><?php echo $sub_item['text']?></a></li>
		<?php endif; ?>

	<?php endif; endforeach; ?>
</ul>
<?php endif; endforeach; ?>