<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

echo tpl('layout_head.php'); ?>

<link rel="stylesheet" href="<?php echo $base.$tplbase?>iframe.css?v=<?php echo $v?>">

  <div id="iframewrap">
	<?php echo $content?>
  </div> <!--! end of #container -->

  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.css?v=<?php echo $v?>">
  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/datepicker/datepicker.css?v=<?php echo $v?>">
  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/plupload/css/plupload.queue.css">

  <!--[if lt IE 7 ]>
    <script src="<?php echo $base.APPDIR?>/js/dd_belatedpng.js?v=<?php echo $v?>"></script>
  <![endif]-->
</body>
</html>