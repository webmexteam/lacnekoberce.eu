<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<h2><a href="<?php echo url('admin/translations')?>"><?php echo __('translations')?></a></h2>

<form class="form gridform" action="<?php echo url('admin/translations/edit')?>" method="post" id="translation-form" style="width:350px;">

	<?php echo forminput('select', 'language', '', array('label' => __('choose_translation'), 'options' => $languages)) ?>

    <button type="submit" class="button" name="edit"><?php echo __('edit')?></button>

</form>

<script>
	$(function(){
		$('#translation-form').bind('submit', function(){
			$(this).attr('action', '<?php echo url('admin/translations/edit', null, true)?>/'+$('select[name="language"]').val());
		});
	});
</script>