<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>  <!-- Javascript at the bottom for fast page loading -->

  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.css?v=<?php echo $v?>">
  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/datepicker/datepicker.css?v=<?php echo $v?>">
  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/plupload/css/plupload.queue.css">

  <?php if(! empty($_SESSION['flash_msg']) && is_array($_SESSION['flash_msg'])): ?>
    	<script>
    		flashMsg(<?php echo json_encode($_SESSION['flash_msg'])?>);
    	</script>
    <?php endif; ?>

  <div id="sessionalert">
  	<a href="<?php echo url('admin/default/logout')?>" class="logout"><?php echo __('logout')?></a>
  	<?php echo __('admin_session_expire')?> <span class="time">&nbsp;</span> <a href="#" class="extend"><?php echo __('admin_session_extend')?> &raquo;</a>
  </div>

  <!--[if lt IE 7 ]>
    <script src="<?php echo $base.APPDIR?>/js/dd_belatedpng.js?v=<?php echo $v?>"></script>
  <![endif]-->

  <img src="http://www.webmex.cz/license/report?instid=<?php echo Core::config('instid')?>&p=<?php echo (Core::$is_premium ? 1 : 0)?>&t=<?php echo (Core::$is_trial ? $is_trial : 0)?>&un=<?php echo (Core::$unlimited_license ? Core::$unlimited_license['id'] : 0)?>" alt="" width="1" height="1" />
</body>
</html>