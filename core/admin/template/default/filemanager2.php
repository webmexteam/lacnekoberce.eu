<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<!doctype html>
<html lang="<?php echo Core::$language?>" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>WEBMEX | FileManager v2</title>

  <meta name="author" content="www.webmex.cz">

  <base href="<?php echo $base?>">

  <link rel="shortcut icon" href="<?php echo $base.$tplbase?>favicon.ico">
  <link rel="stylesheet" href="<?php echo url('style/admin', array('v' => $v), true)?>">
  <link rel="stylesheet" href="<?php echo $base.$tplbase?>filemanager2.css?v=<?php echo $v?>">

  <script src="<?php echo $base.APPDIR?>/js/modernizr-1.5.min.js?v=<?php echo $v?>"></script>

  <script src="<?php echo url('script/constants/admin', array('v' => $v))?>"></script>
  <script src="<?php echo url('script/lang/admin/'.Core::$language, array('v' => $v, 't' => time()))?>"></script>

  <script>
  	_fm_thumbs = <?php echo json_encode(Filemanager::getThumbSizes())?>;
  	_basepath = '<?php echo preg_replace('/index\.php.*$/', '', url(''))?>';
  	_fm_root = '<?php echo Core::$def['filemenager_root']?>';
  	_upload_driver = '<?php echo Core::config('upload_driver')?>';
  </script>

</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

  <div id="container">

  </div> <!--! end of #container -->

  <!-- Javascript at the bottom for fast page loading -->

  <?php if(! empty($_GET['reset'])): ?>
  <script>
  	if(window.opener && window.opener._fm_callback){
  		window.opener._fm_callback = null;
  	}
  </script>
  <?php endif; ?>

  <script src="<?php echo $base.APPDIR?>/js/jquery-1.4.2.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/plugins.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/util.js?v=<?php echo $v?>"></script>

  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.gears.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.flash.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.silverlight.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.html5.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.html4.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/jquery.plupload.queue.min.js?v=<?php echo $v?>"></script>

  <script src="<?php echo $base.APPDIR?>/js/admin.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/filemanager2.js?v=<?php echo $v?>"></script>

  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/plupload/css/plupload.queue.css?v=<?php echo $v?>">

  <!--[if lt IE 7 ]>
    <script src="<?php echo $base.APPDIR?>/js/dd_belatedpng.js?v=<?php echo $v?>"></script>
  <![endif]-->

  <script>
	$(document).ready(function(){
		var cb, ck = <?php echo (! empty($_GET['ck']) ? 'true' : 'false')?>;

		var options = {
			targetEl: $('#container')
			//types: ['jpg', 'jpeg', 'gif', 'png']
		}

		var urlParam = function(name){
			var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
			return results ? (results[1] || 0) : 0;
		}

		if(window.opener && window.opener._fm_options){
			if(window.opener._fm_options.callback){

				options.select = window.opener._fm_options.select || 'multi';

				options.selectCallback = function(fm, files){
					if(window.opener && typeof window.opener._fm_options.callback != 'undefined'){
						window.opener._fm_options.callback.call(fm, files);
					}
				}
			}

		}else if(ck && window.opener && window.opener.CKEDITOR){
			options.select = 'one';
			options.imageSize = true;

			options.selectCallback = function(fm, file){
				if(window.opener && typeof window.opener.CKEDITOR.tools.callFunction != 'undefined'){
					window.opener.CKEDITOR.tools.callFunction(urlParam('CKEditorFuncNum'), file);
				}

				window.close();
			}
		}

		var FM = new FileManager(options);

	});
  </script>

</body>
</html>