<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><a href="<?php echo url('admin/features/edit/0')?>" class="addbtn"><?php echo __('new_featureset')?></a>

<h2><a href="<?php echo url('admin/products')?>"><?php echo __('products')?></a> &rsaquo; <?php echo __('features')?></h2>

<form class="form gridform" action="<?php echo url('admin/features')?>" method="post">
	
	<table class="datagrid">
		<thead>
			<tr>
				<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('name')?></a></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>
		
		<tbody>
			<?php if(! count($featuresets)): ?>
				<tr class="norecords">
					<td colspan="2"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>
			
			<?php foreach($featuresets as $featureset): ?>
				<tr>
					<td><a href="<?php echo url('admin/features/edit/'.$featureset['id'])?>"><?php echo $featureset['name']?></a></td>
					
					<td class="actions">
						<a href="<?php echo url('admin/features/edit/'.$featureset['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/features/delete/'.$featureset['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</form>