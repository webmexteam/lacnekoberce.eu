<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><!doctype html>
<html lang="<?php echo Core::$language?>" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>WEBMEX | Login</title>

  <meta name="author" content="Daniel Regeci; daniel@regeci.cz">

  <base href="<?php echo $base?>">

  <link rel="shortcut icon" href="<?php echo $base.$tplbase?>favicon.ico">
  <link rel="stylesheet" href="<?php echo url('style/admin', array('v' => $v), true)?>">

  <script src="<?php echo $base.APPDIR?>/js/modernizr-1.5.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/jquery-1.4.2.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo url('script/lang/admin/'.Core::$language, array('v' => $v))?>"></script>

</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>	   <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>	   <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>	   <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

  <div id="container">

	<div id="login">
		<form action="<?php echo url('admin/default/login'); ?>" method="post" class="form">
		
			<div class="title">
				<?php echo Core::config('store_name')?>
			</div>
			
			<?php if(! empty($_SESSION['flash_login_error'])): ?>
				<div class="msg msg-error"><?php echo __($_SESSION['flash_login_error'])?></div>
			<?php endif; ?>
			
			<!--[if lt IE 8 ]>
				<div class="msg msg-error"><?php echo __('unsupported_browser')?></div>
			<![endif]-->
		
			<fieldset>
				<?php echo forminput('text', 'username')
				.forminput('password', 'password', '', array('autocomplete' => 'on'))?>
			</fieldset>
			
			<div class="buttons">
				<button type="submit" class="btn btn-inverse"><?php echo __('login')?> &rsaquo;</button>
        <a href="#" onclick="lostpassword(this);return false;" class="pull-right btn"><?php echo __('lost_password')?></a>
			</div>
		</form>
		
		<p><a href="<?php echo $base?>"><?php echo __('homepage')?></a> | <a href="http://www.webmex.cz/">WEBMEX <?php echo Core::version?></a></p>
	</div>

  </div> <!--! end of #container -->

  <!-- asset prefetch, only FF -->
  <link rel="prefetch" href="<?php echo $base.APPDIR?>/js/util.js?v=<?php echo $v?>">
  <link rel="prefetch" href="<?php echo $base.APPDIR?>/vendor/ckeditor/ckeditor.js?v=<?php echo $v?>">
  <link rel="prefetch" href="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.mousewheel-3.0.4.pack.js?v=<?php echo $v?>">
  <link rel="prefetch" href="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.js?v=<?php echo $v?>">
  <link rel="prefetch" href="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.css?v=<?php echo $v?>">


  <?php if(! empty($_SESSION['flash_msg']) && is_array($_SESSION['flash_msg'])): ?>
	<script>
		flashMsg(<?php echo json_encode($_SESSION['flash_msg'])?>);
	</script>
  <?php endif; ?>

  <script>
  	function lostpassword(el)
  	{
  		var el = $(el);
  		var msg = el.html();
  		var email = prompt('<?php echo __('enter_your_email')?>');

  		if(email){
  			el.html(_lang.loading).addClass('loading');
  			$.post('<?php echo url('admin/default/lost_password')?>', { email: email }, function(response){
  				el.html(msg).removeClass('loading');

  				alert(response.msg);
  			}, 'json');
  		}
  	}
  </script>

  <!-- version: <?php echo Core::version?>, instid: *<?php echo substr(Core::config('instid'), -10)?> -->

</body>
</html>