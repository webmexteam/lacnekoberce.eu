<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<h2><?php echo __('packages')?></h2>

<form class="form gridform" action="<?php echo url('admin/packages/print_labels')?>" method="post">

	<table class="datagrid">
		<thead>
			<tr>
				<td class="center input" width="40">
					<input type="checkbox" name="all" value="1" checked="checked" class="check-all" />
				</td>
				<td class="right<?php echo ($order_by == 'id' ? ' sort-'.$order_dir : '')?>" width="60"><a href="<?php echo url(true, array('sort' => 'id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('id')?></a></td>
				<td class="<?php echo ($order_by == 'last_name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'last_name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('fullname')?></a></td>
				<td><?php echo __('address')?></td>
				<td width="120" class="<?php echo ($order_by == 'received' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'received', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('date')?></a></td>
				<td class="right<?php echo ($order_by == 'total_price' ? ' sort-'.$order_dir : '')?>" width="100"><a href="<?php echo url(true, array('sort' => 'total_price', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('total')?></a></td>
				<td width="120"><?php echo __('payment')?></td>
				<td width="120"><?php echo __('delivery')?></td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($orders)): ?>
				<tr class="norecords">
					<td colspan="8"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($orders as $_order): ?>
				<tr>
					<td class="center input">
						<input type="checkbox" name="order[<?php echo $_order['id']?>]" value="1" checked="checked" />
					</td>
					<td class="right"><?php echo $_order['id']?></td>

					<td><?php echo $_order['first_name'].' '.$_order['last_name'].($_order['company'] ? ', '.$_order['company'] : '')?></td>
					<td><?php echo $_order['street'].', '.$_order['zip'].' '.$_order['city']?></td>
					<td><?php echo fdate($_order['received'], true)?></td>
					<td class="right"><?php echo price($_order['total_incl_vat'])?></td>
					<td><?php echo $_order->payment['name']?></td>
					<td><?php echo $_order->delivery['name']?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php if(count($orders)): ?>
	<div style="margin:15px 0 0 0;">
		<button type="submit" class="button"><?php echo __('print_labels')?></button>
	</div>
	<?php endif; ?>

</form>

<script>
	$(function(){
		$('.check-all').bind('click', function(){
			var inputs = $(this).parents('table:first').find('tbody').find('input[type="checkbox"]');

			if(this.checked){
				inputs.attr('checked', true);

			}else{
				inputs.removeAttr('checked', true);
			}
		});
	});
</script>