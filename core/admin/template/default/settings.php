<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><script src="<?php echo APPDIR?>/vendor/farbtastic/farbtastic.js"></script>
<link rel="stylesheet" href="<?php echo APPDIR?>/vendor/farbtastic/farbtastic.css">
<style>
	.tabwrap {
		width: 400px;
	}
	fieldset.optional {
		border: 1px solid #d6d6d6;
		margin: 15px 0;
		padding: 5px 10px;
	}
	fieldset.optional legend input {
		vertical-align: middle;
		margin-top: -1px;
	}
	.editorwrap .cke_skin_kama {
		margin-left: 135px;
	}
	.editorwrap textarea {
		height: 200px;
	}
	.watermark-holder {
		padding-left: 33%;
	}
</style>

<form class="form" action="<?php echo url('admin/settings')?>" method="post" enctype="multipart/form-data">
	<h2><?php echo __('settings')?></h2>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="clearfix">
		<div class="tabsWrap">
			<ul class="tabs clearfix">
				<li class="active"><a href="#tab-general"><?php echo __('general')?></a></li>
				<li><a href="#tab-stock"><?php echo __('stock')?></a></li>
				<li><a href="#tab-prices"><?php echo __('prices')?></a></li>
				<li><a href="#tab-pages"><?php echo __('pages')?></a></li>
				<li><a href="#tab-seo"><?php echo __('seo')?></a></li>
				<li><a href="#tab-invoices"><?php echo __('invoices')?></a></li>
				<?php /* todo: odstrani uplne?<li><a href="#tab-pohoda"><?php echo __('pohoda')?></a></li>*/ ?>
				<li><a href="#tab-security"><?php echo __('security')?></a></li>
				<li><a href="#tab-services"><?php echo __('services')?></a></li>
				<li><a href="#tab-frontend"><?php echo __('frontend')?></a></li>
				<li><a href="#tab-images"><?php echo __('watermark')?></a></li>
				<li><a href="#tab-admin"><?php echo __('admin')?></a></li>
				<li><a href="#tab-license"><?php echo __('license')?></a></li>
				<li><a href="#conversion-rate"><?php echo __("conversion_rate")?></a></li>
			</ul>

			<div id="tab-general" class="tab active">
				<div class="tabwrap">
					<?php echo forminput('text', 'store_name', $config['store_name'])
					.forminput('text', 'email_sender', $config['email_sender'])
					.forminput('text', 'email_notify', $config['email_notify'])
					.forminput('select', 'language', $config['language'], array(
						'options' => $languages,
						'label' => __('language').' (admin)'))
					.forminput('select', 'language_front', $config['language_front'], array(
						'options' => $languages,
						'label' => __('language').' ('.__('frontend').')'))?>
				</div>
			</div>

			<div id="tab-stock" class="tab label-left">
				<div class="tabwrap">

					<?php echo forminput('checkbox', 'display_stock', $config['display_stock'])
					.forminput('checkbox', 'suspend_no_stock', $config['suspend_no_stock'])
					.forminput('checkbox', 'hide_no_stock_products', $config['hide_no_stock_products'])
					.forminput('checkbox', 'stock_out_set_status', $config['stock_out_set_status'])
					.forminput('checkbox', 'stock_auto', $config['stock_auto'])
					.forminput('text', 'stock_notify', $config['stock_notify'], array('cls' => 'short'))
					.forminput('text', 'default_unit', $config['default_unit'], array('cls' => 'short'))
					.forminput('select', 'stock_out_availability', $config['stock_out_availability'], array('options' => $availabilities))?>
				</div>
			</div>

			<div id="tab-prices" class="tab">
				<div class="tabwrap" style="width:500px;">
					<div class="label-left">
						<?php echo forminput('text', 'currency', $config['currency'], array('cls' => 'short'))
						.forminput('select', 'price_format', $config['price_format'], array('options' => array(
							1 => fprice(12345.99, 1),
							2 => fprice(12345.99, 2),
							3 => fprice(12345.99, 3),
							4 => fprice(12345.99, 4),
							5 => fprice(12345.99, 5),
							6 => fprice(12345.99, 6),
						)))
						.forminput('select', 'invoice_price_format', $config['invoice_price_format'], array('options' => array(
							1 => fprice(12345.99, 1),
							2 => fprice(12345.99, 2),
							3 => fprice(12345.99, 3),
							4 => fprice(12345.99, 4),
							5 => fprice(12345.99, 5),
							6 => fprice(12345.99, 6),
						)))
						.forminput('select', 'order_round_decimals', $config['order_round_decimals'] , array('options' => array(
							0 => __('decimals', 0),
							1 => __('decimals', 1),
							2 => __('decimals', 2)
						)))
						.forminput('select', 'price_vat_round', $config['price_vat_round'] , array('options' => array(
							-1 => __('default'),
							0 => __('decimals', 0),
							1 => __('decimals', 1),
							2 => __('decimals', 2)
						)))?>

						<fieldset class="optional">
							<legend><input type="checkbox" name="vat_payer" value="1"<?php echo ($config['vat_payer'] ? ' checked="checked"' : '')?> /> <?php echo __('vat_payer')?></legend>
							<?php
							$vat_rates = Core::$db->vat();

							$_rates = array();
							foreach($vat_rates as $vat){
								$_rates[$vat['id']] = (float) $vat['rate'].' % ('.$vat['name'].')';
							}

							echo forminput('select', 'vat_mode', $config['vat_mode'], array('options' => array(
								'exclude' => __('vat_mode_exclude'),
								'include' => __('vat_mode_include')
							)))
							.forminput('select', 'vat_delivery', $config['vat_delivery'], array('options' => $_rates))?>

							<h4><?php echo __('vat_rates')?>:</h4>
							<table class="datagrid" style="margin-top:5px;">
								<thead>
									<tr>
										<td width="70" class="center"><?php echo __('default')?></td>
										<td><?php echo __('name')?></td>
										<td width="70"><?php echo __('rate')?> %</td>
									</tr>
								</thead>
								<tbody>
									<?php foreach($vat_rates as $vat): ?>
									<tr>
										<td class="input center">
											<input type="radio" name="vat_rate_default" value="<?php echo $vat['id']?>" class="input-checkbox"<?php echo ($vat['id'] == Core::config('vat_rate_default') ? ' checked="checked"' : '')?> />
										</td>
										<td class="input"><input type="text" name="vat_rate[<?php echo $vat['id']?>][name]" value="<?php echo $vat['name']?>" class="input-text" /></td>
										<td class="input"><input type="text" name="vat_rate[<?php echo $vat['id']?>][rate]" value="<?php echo $vat['rate']?>" class="input-text" /></td>
									</tr>
									<?php endforeach; ?>
									<tr>
										<td class="input center">
											<input type="radio" name="vat_rate_default" value="0" class="input-checkbox" disabled="disabled" />
										</td>
										<td class="input"><input type="text" name="vat_rate[0][name]" value="" class="input-text" /></td>
										<td class="input"><input type="text" name="vat_rate[0][rate]" value="" class="input-text" /></td>
									</tr>
								</tbody>
							</table>
						</fieldset>
					</div>

					<h4><?php echo __('price_groups')?>:</h4>
					<?php if(! Core::$is_premium): ?>
					<p class="premium-info"><?php echo __('premium_info')?></p>
					<?php endif; ?>
					<table class="datagrid" style="margin-top:5px;">
						<thead>
							<tr>
								<td class="center"><?php echo __('default')?></td>
								<td class="center"><?php echo __('for_registered')?></td>
								<td width="200"><?php echo __('name')?></td>
								<td width="100"><?php echo __('coefficient')?></td>
							</tr>
						</thead>
						<tbody>
							<?php foreach(Core::$db->customer_group() as $group): ?>
							<tr>
								<td class="input center">
									<input type="radio" name="customer_group_default" value="<?php echo $group['id']?>" class="input-checkbox"<?php echo ($group['id'] == Core::config('customer_group_default') ? ' checked="checked"' : '')?> />
								</td>
								<td class="input center">
									<input type="radio" name="customer_group_registered" value="<?php echo $group['id']?>" class="input-checkbox"<?php echo ($group['id'] == Core::config('customer_group_registered') ? ' checked="checked"' : '')?> />
								</td>
								<td class="input"><input type="text" name="group[<?php echo $group['id']?>][name]" value="<?php echo $group['name']?>" class="input-text" /></td>
								<td class="input"><input type="text" name="group[<?php echo $group['id']?>][coefficient]" value="<?php echo $group['coefficient']?>" class="input-text" /></td>
							</tr>
							<?php endforeach; ?>
							<tr>
								<td class="input center">
									<input type="radio" name="customer_group_default" value="0" class="input-checkbox" disabled="disabled" />
								</td>
								<td class="input center">
									<input type="radio" name="customer_group_registered" value="0" class="input-checkbox" disabled="disabled" />
								</td>
								<td class="input"><input type="text" name="group[0][name]" value="" class="input-text" /></td>
								<td class="input"><input type="text" name="group[0][coefficient]" value="" class="input-text" /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div id="tab-pages" class="tab">
				<div class="tabwrap">
					<?php echo forminput('select', 'page_index', $config['page_index'], array('options' => $pages))
					.forminput('select', 'page_basket', $config['page_basket'], array('options' => $pages))
					.forminput('select', 'page_order', $config['page_order'], array('options' => $pages))
					.forminput('select', 'page_order_step1', $config['page_order_step1'], array('options' => $pages))
					.forminput('select', 'page_order_step2', $config['page_order_step2'], array('options' => $pages))
					.forminput('select', 'page_order_finish', $config['page_order_finish'], array('options' => $pages))
					.forminput('select', 'page_order_finish', $config['page_order_finish'], array('options' => $pages))
					.forminput('select', 'page_terms', $config['page_terms'], array('options' => $pages))
					.forminput('select', 'page_search', $config['page_search'], array('options' => $pages))
					.forminput('select', 'page_404', $config['page_404'], array('options' => $pages))
					.forminput('select', 'page_sitemap', $config['page_sitemap'], array('options' => $pages))?>
				</div>
			</div>

			<div id="tab-seo" class="tab">
				<div class="tabwrap">
					<?php echo forminput('text', 'title', $config['title'])
					.forminput('text', 'description', $config['description'])
					.forminput('text', 'keywords', $config['keywords'])
					.forminput('text', 'meta_robots', $config['meta_robots'])
					.forminput('text', 'meta_author', $config['meta_author'])
					.forminput('textarea', 'html_header', $config['html_header'])
					.forminput('select', 'url_rewrite', $config['url_rewrite'], array('options' => array(
						0 => __('no'),
						1 => __('yes'),
					)))?>
				</div>
			</div>

			<div id="tab-invoices" class="tab">
				<div class="tabwrap">
					<?php echo forminput('text', 'invoice_num', $config['invoice_num'])
					.forminput('text', 'invoice_due', $config['invoice_due'], array('label' => __('invoice_due_days'), 'cls' => 'short'))
					.forminput('textarea', 'invoice_issuer', $config['invoice_issuer'], array('rows' => 15, 'cls' => 'monospace'))
					.forminput('text', 'account_num', $config['account_num'])
					.forminput('text', 'invoice_footer', $config['invoice_footer'])
					.forminput('select', 'invoice_symbol', $config['invoice_symbol'], array('options' => array(
						'order_id' => __('order_id'),
						'invoice_num' => __('invoice_num'),
					)))
					.forminput('file', 'invoice_logo', '', array(
						'label' => __('invoice_logo') . ' ('  . __('invoice_signature_format') . ')'
						. ($config['invoice_logo'] ? ' (<a href=\'admin/settings/deleteInvoiceLogo\'>' . __('delete') . '</a>) (<a href=\'etc/invoice_logo.jpg\' class=\'lightbox\'>' . __('invoice_preview') . '</a>)' : '')))
					.forminput('file', 'invoice_signature', '', array(
						'label' => __('invoice_signature') . ' ('  . __('invoice_signature_format') . ')'
						. ($config['invoice_signature'] ? ' (<a href=\'admin/settings/deleteInvoiceSignature\'>' . __('delete') . '</a>) (<a href=\'etc/invoice_signature.jpg\' class=\'lightbox\'>' . __('invoice_preview') . '</a>)' : '')))
					?>
				</div>
			</div>

			<?php /* todo: odstranit uplne?
			<div id="tab-pohoda" class="tab label-left">
				<div class="tabwrap" style="width:650px;">
					<?php if(! Core::$is_premium): ?>
					<p class="premium-info"><?php echo __('premium_info')?></p>
					<?php endif; ?>

					<?php
					$pohoda = Pohoda::config();

					echo forminput('text', 'pohoda[ico]', $pohoda['ico'], array('label' => __('pohoda_ico'), 'cls' => 'medium'))
					.forminput('checkbox', 'pohoda[stock_internet_only]', $pohoda['stock_internet_only'], array('label' => __('pohoda_stock_internet_only')))
					.forminput('checkbox', 'pohoda[stock_price_only]', $pohoda['stock_price_only'], array('label' => __('pohoda_stock_price_only')))
					.forminput('text', 'pohoda[stock_price_id]', $pohoda['stock_price_id'], array('label' => __('pohoda_stock_price_id'), 'cls' => 'medium'))
					.forminput('checkbox', 'pohoda[stock_enable_insert]', $pohoda['stock_enable_insert'], array('label' => __('pohoda_stock_enable_insert')))
					.forminput('checkbox', 'pohoda[stock_categories]', $pohoda['stock_categories'], array('label' => __('pohoda_stock_categories')))
					.forminput('checkbox', 'pohoda[stock_pictures]', $pohoda['stock_pictures'], array('label' => __('pohoda_stock_pictures')))
					.forminput('checkbox', 'pohoda[stock_pictures_watermark]', $pohoda['stock_pictures_watermark'], array('label' => __('pohoda_stock_pictures_watermark')))
					.forminput('text', 'pohoda[stock_pictures_root]', $pohoda['stock_pictures_root'], array('label' => __('pohoda_stock_pictures_root')))?>


					<h3 style="margin-top: 20px;"><?php echo __('pohoda_export_links')?></h3>
					<table class="datagrid">
						<thead>
							<tr>
								<td width="90"><?php echo __('type')?></td>
								<td><?php echo __('url')?></td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td><?php echo __('addressbook')?></td>
								<td><?php echo url('admin/pohoda/export/addressbook', array('k' => Core::config('sync_key')), true)?></td>
							</tr>
							<tr>
								<td><?php echo __('orders')?></td>
								<td><?php echo url('admin/pohoda/export/orders', array('k' => Core::config('sync_key')), true)?></td>
							</tr>
						</tbody>
					</table>

					<h3 style="margin-top: 20px;"><?php echo __('pohoda_import_links')?></h3>
					<table class="datagrid">
						<thead>
							<tr>
								<td width="90"><?php echo __('type')?></td>
								<td><?php echo __('url')?></td>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td><?php echo __('categories')?></td>
								<td><?php echo url('admin/pohoda/import/categories', array('k' => Core::config('sync_key')), true)?></td>
							</tr>
							<tr>
								<td><?php echo __('products')?></td>
								<td><?php echo url('admin/pohoda/import/stock', array('k' => Core::config('sync_key')), true)?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			*/ ?>

			<div id="tab-frontend" class="tab label-left">
				<div class="tabwrap">
					<?php echo forminput('text', 'limit_records', $config['limit_records'], array('cls' => 'short'))
					//.forminput('checkbox', 'maintenance', $config['maintenance'])
					.forminput('select', 'default_order', $config['default_order'], array('options' => array(
						'position ASC' => __('order_position_asc'),
						'position DESC' => __('order_position_desc'),
						'price ASC' => __('order_price_asc'),
						'price DESC' => __('order_price_desc'),
						'name ASC' => __('order_name_asc'),
						'name DESC' => __('order_name_desc'),
						'position price ASC' => __('order_position_price_asc'),
						'position price DESC' => __('order_position_price_desc'),
					)))
					.forminput('checkbox', 'ajaxbasket', $config['ajaxbasket'])
					.forminput('checkbox', 'enable_customer_login', $config['enable_customer_login'], array('premium' => true))
					.forminput('checkbox', 'customer_confirmation', $config['customer_confirmation'], array('premium' => true))
					.forminput('checkbox', 'confirm_orders', $config['confirm_orders'])
					.forminput('checkbox', 'send_order_status', $config['send_order_status'])
					.forminput('checkbox', 'hide_prices_to_visitors', $config['hide_prices_to_visitors'])
					.forminput('checkbox', 'enable_vouchers', $config['enable_vouchers'], array('premium' => true))
					?>

					<div class="editorwrap" style="width:600px;">
						<?php echo forminput('textarea', 'footer', $config['footer'])?>
					</div>
				</div>
			</div>

			<div id="tab-images" class="tab label-left">
				<div class="tabwrap" style="width:500px;">
					<?php
						echo forminput('checkbox', 'watermark_apply_on_original', $config['watermark_apply_on_original'])
							.forminput('text', 'watermark_min_size', $config['watermark_min_size'], array('cls' => 'short'))
							.forminput('select', 'watermark_type', $config['watermark_type'], array('options' => array(
								'text'  => __('watermark_type_text'),
								'image' => __('watermark_type_image'),
							)))
							.forminput('text', 'watermark_filepath', $config['watermark_filepath'])
							.forminput('text', 'watermark_text', $config['watermark_text'])
							.forminput('select', 'watermark_alignment', $config['watermark_alignment'], array('options' => array(
								'tr' => __('watermark_alignment_tr'),
								'br' => __('watermark_alignment_br'),
								'bl' => __('watermark_alignment_bl'),
								'tl' => __('watermark_alignment_tl'),
								'c'  => __('watermark_alignment_c'),
							)))
							.forminput('text', 'watermark_font_color', $config['watermark_font_color'], array('cls' => 'medium colorpickerinput'));
						?>
						<div class="watermark-holder"><div class="colorpicker"><div id="colorpicker"></div><div class="cpoverlay"></div></div></div>
						<?php echo forminput('text', 'watermark_font_size', $config['watermark_font_size'], array('cls' => 'short'))
							.forminput('select', 'watermark_opacity', $config['watermark_opacity'], array('options' => array(
								'10' => '90%',
								'20' => '80%',
								'30' => '70%',
								'40' => '60%',
								'50'  => '50%',
								'60'  => '40%',
								'70'  => '30%',
								'80'  => '20%',
								'90'  => '10%',
								'99'  => '0%',
							)))
							.forminput('checkbox', 'watermark_png24', $config['watermark_png24'])
							.forminput('checkbox', 'watermark_text_shadow', $config['watermark_text_shadow'])
					?>
					<script>
						$(function(){
							$('.colorpickerinput').css({'float':'left'}).after('<span class="color" style="background:'+$('.colorpickerinput').val()+';margin-left:5px;margin-top:5px;border:1px solid #ccc;width:18px;height:18px;float:left;display:block"></span>');
							$('#colorpicker').farbtastic(function(color){
								$('.colorpickerinput').val(color);
								$('.colorpickerinput').parent().find('span.color').css('backgroundColor', color);
							});
							$.farbtastic('#colorpicker').setColor($('.colorpickerinput').val());
						});
					</script>
				</div>
			</div>

			<div id="tab-admin" class="tab label-left">
				<div class="tabwrap">
					<?php echo forminput('text', 'limit_records_admin', $config['limit_records_admin'], array('cls' => 'short'))
					.forminput('select', 'upload_driver', $config['upload_driver'], array('options' => array(
						'auto' => __('automatic'),
						'html5' => 'HTML 5',
						'flash' => 'Flash',
						'silverlight' => 'Silverlight',
						'html4' => 'HTML 4',
					)))
					.forminput('text', 'command', '')?>
				</div>
			</div>

			<div id="tab-services" class="tab">
				<div class="tabwrap">
					<?php echo forminput('text', 'analytics', $config['analytics'], array('label' => __('analytics_ua')))
					.forminput('text', 'heureka_overeno', $config['heureka_overeno'])
					.forminput('text', 'heureka_conversions', $config['heureka_conversions'])
					.forminput('text', 'sklik_conversions', $config['sklik_conversions'])
					.forminput('text', 'zbozi_conversions', $config['zbozi_conversions'])
					.forminput('textarea', 'sharelinks', $config['sharelinks'])?>
				</div>
			</div>

			<div id="tab-security" class="tab">
				<div class="tabwrap">
					<?php echo forminput('text', 'access_key', $config['access_key'])?>
					<small><?php echo __('access_key_help')?></small><br /><br />

					<?php echo forminput('text', 'sync_key', $config['sync_key'])?>
					<small><?php echo __('sync_key_help')?></small>
				</div>
			</div>

			<div id="tab-license" class="tab">
				<?php if(Core::$unlimited_license): ?>
					<p style="margin:10px 0;font-size:120%"><?php echo __('unlimited_license_info')?></p>

					<table class="datagrid">
						<tr>
							<td width="80"><?php echo __('license_id')?>:</td>
							<td><?php echo Core::$unlimited_license['id']?></td>
						</tr>
						<tr>
							<td width="80"><?php echo __('date')?>:</td>
							<td><?php echo date('Y-m-d H:i', Core::$unlimited_license['date'])?></td>
						</tr>
					</table>
				<?php else: ?>
					<?php echo forminput('text', 'instid', $config['instid'], array('label' => __('store_id')))
					.forminput('textarea', 'license_key', $config['license_key'])?>

					<?php if(is_array($license = Core::decodelKey())): ?>
						<?php if(! empty($_SESSION['invalid_license'])): ?>
						<p style="margin: 20px 0;padding:10px;background:#f0f0f0;"><?php echo __('invalid_license')?></p>
						<?php unset($_SESSION['invalid_license']); endif; ?>

						<table class="datagrid">
							<tr>
								<td width="80"><?php echo __('license_id')?>:</td>
								<td><?php echo $license[1]?></td>
							</tr>
							<tr>
								<td width="80"><?php echo __('date')?>:</td>
								<td><?php echo date('Y-m-d H:i', $license[2])?></td>
							</tr>
							<tr>
								<td><?php echo __('domain')?>:</td>
								<td><?php echo $license[4]?></td>
							</tr>
							<tr>
								<td><?php echo __('owner')?>:</td>
								<td><?php echo $license[5]?></td>
							</tr>
						</table>
					<?php elseif($license === false): ?>
						<p><strong><?php echo __('invalid_license')?></strong></p>
					<?php endif; ?>

				<?php endif; ?>
			</div>

			<div id="conversion-rate" class="tab">

				<?php echo forminput('textarea', 'conversion_rate_script', $config['conversion_rate_script'])?>

			</div>
		</div>
	</div>

	<div class="buttons bbar">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>


</form>

<script>
	$(function(){
		$('fieldset.optional legend input[type="checkbox"]').bind('change', function(){
			var inputs = $(this).parent().parent().find('.inputwrap');

			if(! this.checked){
				inputs.css('opacity', 0.5).find('input, select, textarea').attr('readonly', true).val('');

			}else{
				inputs.css('opacity', 1).find('input, select, textarea').removeAttr('readonly');
			}

			inputs = $(this).parent().parent().find('td.input');

			if(! this.checked){
				inputs.css('opacity', 0.5).find('input, select, textarea').attr('readonly', true);

			}else{
				inputs.css('opacity', 1).find('input, select, textarea').removeAttr('readonly');
			}
		}).bind('click', function(){
			$(this).trigger('change');
		}).trigger('change');

		$('a[href="#tab-frontend"]').bind('click', function(){
			var id = 'inp-footer';

			var toolbar = [
				['Bold','Italic','NumberedList','-','Link','Unlink','-','Image','SpecialChar'],
				['RemoveFormat'], ['Source']
			];

			if(CKEDITOR.instances[id]){
				return;
			}

			CKEDITOR.replace(id, {
				customConfig: false,
		        toolbar : toolbar,
		        height: 100,
		        resize_enabled: true,
		        resize_dir: 'vertical',
		        resize_maxHeight: 900,
		        resize_minHeight: 100,
		        uiColor : '#cccccc',
		        bodyClass: 'ckeditor',
		        filebrowserBrowseUrl: _filemanager_url,
		        baseHref: _url,
		        contentsCss: _tpl_front+'css/editor.css',
		        stylesSet: 'mystyles:'+_tpl_front+'css/styles.js'
		    });
		});
	});
</script>
