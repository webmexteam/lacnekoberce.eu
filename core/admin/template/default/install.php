<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><!doctype html>
<html lang="<?php echo Core::$language?>" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>WEBMEX | Install</title>

  <meta name="author" content="www.webmex.cz">
  <meta name="robots" content="noindex, nofollow">

  <base href="<?php echo $base?>">

  <link rel="shortcut icon" href="<?php echo $base.$tplbase?>favicon.ico">
  <link rel="stylesheet" href="<?php echo $base.APPDIR?>/vendor/flatstrap/css/bootstrap.min.css">

  <script src="<?php echo $base.APPDIR?>/js/modernizr-1.5.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/jquery-1.4.2.min.js?v=<?php echo $v?>"></script>

  <style>
  	#install {
  		width: 600px;
		margin: 0 auto;
		padding-top: 80px;
		overflow: hidden;
  	}
  	#install form p {
  		margin: 10px 0;
  	}
  	#install form div.admin {
  		background: #fff;
  		padding: 10px;
  		line-height: 160%;
  		margin-bottom: 10px;
  	}
  	#install #mysql {
  		display: none;
  	}
  </style>

</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>	   <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>	   <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>	   <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

  <div id="container">

	<div id="install">
		<form action="<?php echo url('admin/install/step/'.($step + 1))?>" method="post" class="well">
			<div class="page-header">
				<h1>WEBMEX <?php echo Core::version?></h1>
			</div>

			<?php if($step == 0): ?>
			<fieldset>
				<?php if($errors): ?>
					<p><?php echo __('install_missing_drivers')?>:</p>
					<p><?php echo join('<br />', $errors)?></p>
				<?php else: ?>
					<div class="row-fluid">
						<div class="span12">
							<?php echo forminput('select', 'language', '', array('options' => $languages, 'cls' => 'span12'))?>
						</div>
					</div>
				<?php endif; ?>
			</fieldset>

			<?php elseif($step == 1): ?>
			<fieldset>
				<p><?php echo __('install_set_permissions')?>:</p>

				<div class="well">
					<strong>/etc</strong> - <?php echo (is_writable(DOCROOT.'etc') ? __('install_writable') : __('install_not_writable'))?><br />
					<strong>/files</strong> - <?php echo (is_writable(DOCROOT.'files') ? __('install_writable') : __('install_not_writable'))?><br />
				</div>

				<a href="#" onclick="window.location=window.location.href;return false;" class="btn btn-inverse"><?php echo __('install_check_again')?></a>
			</fieldset>

			<?php if($ftp_required): ?>
			<p class="alert alert-danger"><?php echo __('install_set_ftp')?></p>
			<fieldset id="ftp" style="margin-bottom:15px;">
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'server', $ftp['server'], array('cls' => 'span12'))?>
					</div>
					<div class="span6">
						<?php echo forminput('text', 'port', $ftp['port'], array('cls' => 'span3'))?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'dir', $ftp['dir'], array('cls' => 'span12'))?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'username', $ftp['username'], array('cls' => 'span12'))?>
					</div>
					<div class="span6">
						<?php echo forminput('text', 'password', $ftp['password'], array('cls' => 'span12'))?>
					</div>
				</div>
				<a href="#" class="test btn btn-inverse"><?php echo __('install_test_connection')?></a>
			</fieldset>
			<script>
				$(function(){
					$('#ftp .test').bind('click', function(){
						$.post('<?php echo url('admin/install/ajax_testFtpConnection')?>', {
							server: $('#ftp input[name="server"]').val(),
							port: $('#ftp input[name="port"]').val(),
							dir: $('#ftp input[name="dir"]').val(),
							username: $('#ftp input[name="username"]').val(),
							password: $('#ftp input[name="password"]').val()
						}, function(result){
							if(result == 'OK'){
								alert('<?php echo __('install_connection_ok')?>');

							}else{
								alert('<?php echo __('install_connection_not_ok')?>: '+result);
							}
						});
						return false;
					});
				});
			</script>
			<?php endif; ?>

			<?php elseif($step == 2): ?>



			<fieldset style="margin-bottom:15px;">
				<p><?php echo __('install_select_db')?>:</p>
				<label class="radio"><input type="radio" name="db" value="sqlite"<?php echo (isSet($has_sqlite) ? ' checked="checked"' : ' disabled="disabled"')?> onclick="$('#mysql').toggle(! this.checked);" /> SQLite</label>
				<label class="radio"><input type="radio" name="db" value="mysql"<?php echo (! isSet($has_mysql) ? ' disabled="disabled"' : '')?> onclick="$('#mysql').toggle(this.checked);" /> MySQL</label>
			</fieldset>

			<fieldset id="mysql">
				<hr>
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'server', $mysql['server'], array('cls' => 'span12')) ?>
					</div>
					<div class="span6">
						<?php echo forminput('text', 'port', $mysql['port'], array('cls' => 'span3')) ?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'dbname', $mysql['dbname'], array('cls' => 'span12')) ?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'username', $mysql['username'], array('cls' => 'span12')) ?>
					</div>
					<div class="span6">
						<?php echo forminput('text', 'password', $mysql['password'], array('cls' => 'span12')) ?>
					</div>
				</div>

				<a href="#" class="test btn btn-inverse"><?php echo __('install_test_connection')?></a>
			</fieldset>

			<script>
				$(function(){
					$('#mysql .test').bind('click', function(){
						$.post('<?php echo url('admin/install/ajax_testConnection')?>', {
							server: $('#mysql input[name="server"]').val(),
							port: $('#mysql input[name="port"]').val(),
							dbname: $('#mysql input[name="dbname"]').val(),
							username: $('#mysql input[name="username"]').val(),
							password: $('#mysql input[name="password"]').val()
						}, function(result){
							if(result == 'OK'){
								alert('<?php echo __('install_connection_ok')?>');

							}else{
								alert('<?php echo __('install_connection_not_ok')?>: '+result);
							}
						});
						return false;
					});
				});
			</script>

			<?php elseif($step == 3): ?>
			<fieldset>
				<div class="row-fluid">
					<div class="span6">
						<?php echo forminput('text', 'store_name', '', array('required' => true, 'cls' => 'span12'))?>
					</div>
					<div class="span6">
						<?php echo forminput('text', 'email', '', array('required' => true, 'cls' => 'span12'))?>
					</div>
				</div>

				<?php/*<div class="row-fluid">
					<div class="span12">
						<label for="inp-test" class="checkbox">
							<input type="checkbox" name="test" value="1" id="inp-test" class="input-checkbox">
							Instalovat ukázková data
						</label>
					</div>
				</div>*/?>

				<div class="row-fluid">
					<div class="span12">
						<label for="inp-license" class="checkbox">
							<input type="checkbox" name="license" value="1" id="inp-license" class="input-checkbox">
							<?php echo __('already_have_license') ?>
						</label>
					</div>
				</div>
			</fieldset>

			<fieldset id="license" style="display:none;">
				<div class="row-fluid">
					<div class="span12">
						<?php echo forminput('text', 'instid', '', array('label' => __('store_id'), 'cls' => 'span12'))?>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<?php echo forminput('textarea', 'license_key', '', array('cls' => 'span12'))?>
					</div>
				</div>
			</fieldset>

			<?php elseif($step == 4): ?>
				<p class="alert alert-success"><?php echo __('install_success')?></p>
				<p><?php echo __('install_login')?>:</p>

				<div class="admin">
					<?php echo __('username')?>: <strong>admin</strong><br />
					<?php echo __('password')?>: <strong>admin</strong>
				</div>
			<?php endif; ?>

			<?php if((! isSet($errors) || empty($errors)) && $step != 4): ?>
			<hr>
			<div class="form-a">
				<button type="submit" class="btn btn-primary pull-right"><?php echo __('next')?> &rsaquo;</button>
			</div>
			<div class="clearfix"></div>
			<?php elseif ($step == 4):?>
				<hr>
				<div class="form-a">
					<button type="submit" class="btn btn-primary pull-right"><?php echo __('administration')?></button>
				</div>
				<div class="clearfix"></div>
			<?php endif; ?>
		</form>
	</div>

  </div> <!--! end of #container -->

  <script>
  	$(function(){
		$('input[name="license"]').bind('change', function(){
			$('#license').toggle(this.checked);

		}).bind('click', function(){
			$(this).trigger('change');
		});
  	});
  </script>

</body>
</html>