<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>WEBMEX | Admin</title>

  <meta name="author" content="Daniel Regeci; daniel@regeci.cz">
  <meta name="robots" content="noindex, nofollow">

  <base href="<?php echo $base?>">

  <link rel="shortcut icon" href="<?php echo $base.$tplbase?>favicon.ico">
  <link rel="stylesheet" href="<?php echo url('style/admin', array('v' => $v), true)?>">

  <script src="<?php echo APPDIR?>/js/modernizr-1.5.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo APPDIR?>/js/jquery-1.4.2.min.js?v=<?php echo $v?>"></script>

  <style>
  	#container {
  		padding: 15px;
  	}
  	#container form {
  		padding: 10px 0;
  	}
  	#result {
  		background: #fff;
  		border: 1px solid #d6d6d;
  		margin: 0;
  		padding: 10px;
  		font-family: monospace;
  		white-space: pre;
  		min-height: 450px;
  	}
  </style>

</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>	   <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>	   <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>	   <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

  <div id="container">

	<div id="result"><?php echo $content?></div>

	<form action="<?php echo url('admin')?>" method="post">
		<button type="submit" class="button"><?php echo __('back_to_admin')?></button>
	</form>

  </div> <!--! end of #container -->

</body>
</html>