<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<style>
.pagetoggle {
	padding: 0 6px;
	margin-right: 3px;
	text-decoration: none;
	background: url(<?php echo $tplbase?>images/toggle-plus.png) no-repeat left center;
}
.pagetoggle-open {
	background-image: url(<?php echo $tplbase?>images/toggle-minus.png);
}
.pagetoggle-empty {
	opacity: 0.4;
}
.loading {
	background-image: url(<?php echo $tplbase?>images/loader.gif);
}
.expandall {
	float: left;
	margin: 5px 0 0 10px;
}
.expandall input {
	margin: 0 5px 0 0;
}
</style>

<a href="<?php echo url('admin/pages/edit/0')?>" class="addbtn"><?php echo __('new_page')?></a>

<h2><?php echo __('pages')?></h2>

<form class="form gridform" action="<?php echo url('admin/pages')?>" method="post">
	
	<div class="buttons">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<table class="datagrid">
		<thead>
			<tr>
				<td class="right<?php echo ($order_by == 'id' ? ' sort-'.$order_dir : '')?>" width="40"><a href="<?php echo url(true, array('sort' => 'id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('id')?></a></td>
				<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('name')?></a></td>
				<td class="center<?php echo ($order_by == 'position' ? ' sort-'.$order_dir : '')?>" width="60"><a href="<?php echo url(true, array('sort' => 'position', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('position')?></a></td>
				<td class="center" width="60"><?php echo __('status')?></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>
		
		<tbody>
			<?php foreach(Core::$def['page_types'] as $type_id => $type): 
			
				$pages = Core::$db->page()->order($order_by.' '.$order_dir)->where('menu', $type_id);
				
				if(! count($pages)){
					continue;
				}
			
			?>
				<tr class="gridtitle">
					<td colspan="5"><?php echo $type?></td>
				</tr>
				
				<?php 
					$pages_i = 1;
					
					echo $controller->_pageChildren($pages, 0, $pages_i, $expand_all);
				?>
				
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<div class="buttons buttons-foot">
		<div class="expandall">
			<label><input type="checkbox" name="expand_all" value="1"<?php echo ($expand_all ? ' checked="checked"' : '')?> /> <?php echo __('expand_all')?></label>
		</div>
	
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

</form>

<script>
	$(function(){
	
		$('input[name="expand_all"]').bind('change', function(){
			window.location = '<?php echo url(true)?>?expand_all='+($(this).is(':checked') ? 1 : 0);
		});
		
		$('a.pagetoggle').live('click', function(){
			var self = $(this);
			var attrs = self.attr('href').match(/\#(.*)$/)[1].split(';');
			
			var id = parseInt(attrs[0]);
			var opened = self.hasClass('pagetoggle-open');
			
			var removeChildren = function(parent_id){
				$('tr.parent-'+parent_id).each(function(){
					var pid = parseInt($(this).attr('id').match(/\d+$/)[0]);
					
					if(pid){
						removeChildren(pid);
					}
					
					$(this).remove();
				});
			}
			
			if(opened){
				self.removeClass('pagetoggle-open');
				removeChildren(id);
				
			}else{	
				self.addClass('loading');
						
				$.get(_base+'admin/pages/subpages/'+id+'/'+attrs[1], function(response){
					self.addClass('pagetoggle-open');
					
					if(! response){
						self.addClass('pagetoggle-empty');
					}
					
					self.parent().parent().after(response);
					self.removeClass('loading');
				});
			}
			
			return false;
		});
		
	});
</script>