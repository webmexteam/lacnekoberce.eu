<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><tr class="<?php echo ($i % 2 === 0 ? 'even' : '')?> parent-<?php echo $parent_id?>" id="page-id-<?php echo $page['id']?>">
	<td class="right"><?php echo $page['id']?></td>
	<td><?php echo str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level)?>
		<?php if(! $expand_all): ?>
		<a href="#<?php echo $page['id'].';'.($level + 1)?>" class="pagetoggle">&nbsp;</a>
		<?php endif; ?>
		
		<a href="<?php echo url('admin/pages/edit/'.$page['id'])?>"><?php echo $page['name']?></a>
	</td>
	<td class="center input">
		<input type="text" name="position[<?php echo $page['id']?>]" class="input-text center" value="<?php echo $page['position']?>" />
	</td>
	<td class="center input">
		<input type="checkbox" name="status[<?php echo $page['id']?>]" class="input-checkbox"<?php echo ($page['status'] ? ' checked="checked"' : '')?> />
	</td>
	<td class="actions">
		<a href="<?php echo url('admin/pages/edit/'.$page['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
		<a href="<?php echo url('admin/pages/delete/'.$page['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
	</td>
</tr>