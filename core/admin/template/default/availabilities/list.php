<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><a href="<?php echo url('admin/settings/availability/0')?>" class="addbtn"><?php echo __('new_availability')?></a>

<h2><?php echo __('availabilities')?></h2>

<form class="form gridform" action="<?php echo url('admin/settings/availabilities')?>" method="post">

	<div class="buttons">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<table class="datagrid">
		<thead>
			<tr>
				<td width="40" class="right"><?php echo __('id')?></td>
				<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('name')?></a></td>
				<td width="60" class="<?php echo ($order_by == 'days' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'days', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('days')?></a></td>
				<td width="70" class="<?php echo ($order_by == 'hex_color' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'hex_color', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('hex_color')?></a></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($availabilities)): ?>
				<tr class="norecords">
					<td colspan="5"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($availabilities as $availability): ?>
				<tr>
					<td class="right"><?php echo $availability['id']?></td>
					<td><a href="<?php echo url('admin/settings/availability/'.$availability['id'])?>"><?php echo $availability['name']?></a></td>
					<td class="center input">
						<input type="text" name="days[<?php echo $availability['id']?>]" class="input-text" value="<?php echo $availability['days']?>" />
					</td>
					<td class="center input">
						<input type="text" name="hex_color[<?php echo $availability['id']?>]" class="input-text" value="<?php echo $availability['hex_color']?>" />
					</td>

					<td class="actions">
						<a href="<?php echo url('admin/settings/availability/'.$availability['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/settings/availability_delete/'.$availability['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="buttons buttons-foot">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

</form>