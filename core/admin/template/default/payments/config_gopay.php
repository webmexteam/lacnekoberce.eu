<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
?>
<style>
	.gpchannels {
		margin: 10px 0;
	}
	.gpchannels label {
		display: inline-block;
		margin-bottom: 5px;
	}
	.gpchannels label input {
		margin: 0 5px 0 0;
		vertical-align: top;
	}
</style>
<fieldset class="gpchannels">
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_gp_w"<?php echo (in_array('cz_gp_w', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Gopay peněženka</label><br />
	<label><input type="checkbox" name="payment_config[channels][]" value="eu_mb_w"<?php echo (in_array('eu_mb_w', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Moneybookers peněženka</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_gp_c"<?php echo (in_array('cz_gp_c', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Platební karty (GP WebPay)</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_cs_c"<?php echo (in_array('cz_cs_c', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Platební karty (CS 3DSecure)</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="eu_mb_a"<?php echo (in_array('eu_mb_a', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Platební karty (MoneyBookers)</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="eu_mb_b"<?php echo (in_array('eu_mb_b', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Platební karty (American Expres a JCB - MoneyBookers)</label><br />
	<label><input type="checkbox" name="payment_config[channels][]" value="SUPERCASH"<?php echo (in_array('SUPERCASH', $payment->config['channels']) ? ' checked="checked"' : '')?> /> SuperCash</label><br />
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_sms"<?php echo (in_array('cz_sms', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Premium SMS</label><br />
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_kb"<?php echo (in_array('cz_kb', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Moje Platba od Komerční banky</label><br />
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_rb"<?php echo (in_array('cz_rb', $payment->config['channels']) ? ' checked="checked"' : '')?> /> ePlatby od Raiffeisen bank</label><br />
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_mb"<?php echo (in_array('cz_mb', $payment->config['channels']) ? ' checked="checked"' : '')?> /> mPeníze od mBank</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_fb"<?php echo (in_array('cz_fb', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Fio banka</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_ge"<?php echo (in_array('cz_ge', $payment->config['channels']) ? ' checked="checked"' : '')?> /> GE Moneybank</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_vb"<?php echo (in_array('cz_vb', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Volksbank</label><br />
	
	<label><input type="checkbox" name="payment_config[channels][]" value="cz_bank"<?php echo (in_array('cz_bank', $payment->config['channels']) ? ' checked="checked"' : '')?> /> Bankovní převod</label>
</fieldset>

<div style="width:250px;">
<?php
echo forminput('select', 'payment_config[default_channel]', $payment->config['default_channel'], array('label' => 'Výchozí platební kanál', 'options' => array()));
?>
</div>

<script>
	$(function(){
		var default_channel = '<?php echo $payment->config['default_channel']?>';
		var select = $('select[name*="default_channel"]');
		
		var populateDefaultSelect = function(){
			default_channel = select.val() || default_channel;
			select.find('option').remove();
			
			$('.gpchannels input:checked').each(function(){
				var el = $('<option value="'+this.value+'">'+$(this).parent().text()+'</option>');
				select.append(el);
				
				if(default_channel == this.value){
					el.attr('selected', true);
				}
			});
			
		}
		
		$('.gpchannels input[type="checkbox"]').bind('click', function(){
			populateDefaultSelect();
		});
		
		populateDefaultSelect();
	});
</script>