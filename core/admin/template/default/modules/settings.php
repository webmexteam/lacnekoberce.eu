<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<form class="form" action="<?php echo url(true)?>" method="post">

	<h2><a href="<?php echo url('admin/modules')?>"><?php echo __('modules')?></a> &rsaquo; <?php echo $module->getInfo('name')?></h2>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="clearfix">

		<?php echo $module->getSettingForm()?>

	</div>

	<div class="buttons bbar">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

</form>