<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<style>
	.moduleinfo {
		margin: 5px 0 0 0;
		font-size: 90%;
		padding-left: 20px;
	}
	td.status {
		text-align: center;
	}
	td.status span.icon {
		padding: 0 5px;
		background-repeat: no-repeat;
		background-position: center center;
	}
	.collisions {
		font-weight: bold;
		background: #f0f0f0;
		border: 1px solid #ddd;
		margin-top: 5px;
		padding: 10px;
	}
	.collisions .collision {
		display: block;
	}
	a.settings {
		margin-left: 15px;
	}
	a.togglemoreinfo {
		float: left;
		width: 20px;
		background: url(./webmex/admin/template/default/images/toggle-plus.png) no-repeat -2px -2px;
		text-decoration: none;
		margin-left: -20px;
		opacity: 0.5;
	}
	a.togglemoreinfo:hover {
		opacity: 1;
	}
	a.tmi-active {
		background-image: url(./webmex/admin/template/default/images/toggle-minus.png);
		opacity: 1;
	}
	div.moreinfo {
		display: none;
	}
</style>
<h2><?php echo __('modules')?></h2>

<form class="form gridform" action="<?php echo url('admin/modules')?>" method="post">

	<?php if(isset($_SESSION['module_collisions'])): ?>
	<div class="collisions" style="margin-bottom:15px;">
		<?php foreach($_SESSION['module_collisions'] as $collision): ?>
			<span class="collision"><?php echo $collision?></span>
		<?php endforeach; ?>
	</div>
	<?php unset($_SESSION['module_collisions']); endif; ?>

	<table class="datagrid">
		<thead>
			<tr>
				<td class="status" width="40">&nbsp;</td>
				<td><?php echo __('module')?></td>
				<td width="150">&nbsp;</td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($modules)): ?>
				<tr class="norecords">
					<td colspan="3"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($modules as $module_name => $module): ?>
				<tr>
					<td class="status">
						<span class="icon icon-<?php echo (($module->installed && $module->status) ? 'tick' : 'cross')?>">&nbsp;</span>
					</td>
					<td>
						<strong title="<?php echo $module->module_name?>"><?php echo $module->getInfo('name')?></strong>

						<?php if($module->has_settings && $module->status): ?>
						<a href="<?php echo url('admin/modules/settings/'.$module_name)?>" class="settings"><?php echo __('settings')?></a>
						<?php endif; ?>

						<br />

						<div class="moduleinfo">
							<a href="#" class="togglemoreinfo">&nbsp;</a>

							<?php echo $module->getInfo('description')?>

							<div class="moreinfo">
							<?php echo __('version').': '.$module->getInfo('version')?><br />
							<?php echo __('author').': '.$module->getInfo('author')?><br />
							<?php echo __('www').': '.$module->getInfo('www')?><br />
							<?php echo __('license').': '.$module->getInfo('license')?><br />
							</div>

							<?php if($module->installed && ($collisions = $module->checkCollisions())): ?>
							<div class="collisions">
								<?php foreach($collisions as $collision): ?>
									<span class="collision"><?php echo $collision?></span>
								<?php endforeach; ?>
							</div>
							<?php endif; ?>
						</div>
					</td>

					<td style="text-align:right;">
						<?php if(! $module->installed): ?>
							<input type="submit" name="install[<?php echo $module_name?>]" value="<?php echo __('install')?>" />
						<?php endif; ?>

						<?php if($module->installed && ! $module->status): ?>
							<input type="submit" name="activate[<?php echo $module_name?>]" value="<?php echo __('activate')?>" />
						<?php endif; ?>

						<?php if($module->installed && $module->status): ?>
							<input type="submit" name="deactivate[<?php echo $module_name?>]" value="<?php echo __('deactivate')?>" />
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</form>

<script>
	$(function(){
		$('a.togglemoreinfo').bind('click', function(){
			$(this).toggleClass('tmi-active');
			$(this).parent().find('div.moreinfo').toggle();
			return false;
		});
	});
</script>