<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<a href="<?php echo url('admin/emails/edit/0')?>" class="addbtn"><?php echo __('new_email')?></a>

<h2><?php echo __('emails')?></h2>

<form class="form gridform" action="<?php echo url('admin/settings/emails')?>" method="post">
	
	<table class="datagrid">
		<thead>
			<tr>
				<td width="200"><?php echo __('event')?></td>
				<td width="110"><?php echo __('template')?></td>
				<td><?php echo __('subject')?></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>
		
		<tbody>			
			<?php foreach($emails as $email): ?>
				<tr>
					<td><small style="color:#666;font-size:11px"><?php echo __('email_event_'.$email['key'])?></small></td>
					<td><small style="color:#666;font-size:11px"><?php echo $templates[$email['template']]?></small></td>
					<td><a href="<?php echo url('admin/emails/edit/'.$email['id'])?>"><?php echo $email['subject']?></a></td>
					
					<td class="actions">
						<a href="<?php echo url('admin/emails/edit/'.$email['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/emails/delete/'.$email['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</form>