<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<style>
	#dashboard .widget ul li.order {
		float: left;
		width: 100%;
	}

	#dashboard .widget ul li.order .status {

	}
</style>
<?php
$widget_orders = $widget_stats = $widget_changes = null;

if(User::has(User::has_or, 'orders', 'orders-view')){
	$nodes = null;

	if(! count($orders)){
		$nodes = li('@no_records')->class('norecords');

	}else{
		foreach($orders as $order){
			$nodes .= li(
				div('@status_'.Core::$def['order_status'][$order['status']])->class('status s-'.$order['status']),
				a('@order #'.$order['id'])->href(url('admin/orders/edit/'.$order['id'])),
				' - '.$order['first_name'].' '.$order['last_name'],
				br(),
				span('@received '.fdate($order['received'], true).', @total: '.price($order['total_incl_vat']))->class('date')
			)->class('order');
		}
	}

	$widget_orders = div(
		h3('@received_orders'),
		div(
			ul($nodes)
		)->class('widget-content clearfix')

	)->class('widget');
}

if(User::has(User::has_or, 'orders', 'orders-view')){
	$nodes = null;

	for($i = 6; $i >= 0; $i --){
		$tmp = $daystotal * $stats[$i];

		$nodes .= div(
			div(
				span(($i == 0 ? '@today' : fdate(strtotime('-'.$i.' days')))),
				em(price($stats[$i]))

			)->class('bar')->style('width:'.($tmp ? (($daystotal && ($p = 100 / $tmp) >= 100) ? 'auto' : $p.'%' ) : '0'))
		)->class('row');
	}

	$widget_stats = div(
		h3('@statistics'),
		div(
			div(
				div(
					span('@orders'),
					big(fprice($total_orders, 6))
				),
				div(
					span('@customers'),
					big(fprice($total_customers, 6))
				),
				div(
					span('@this_month_revenue'),
					big(price($month_revenue))
				)
			)->class('numstats clearfix'),

			div(
				$nodes

			)->class('orderstats')

		)->class('widget-content')

	)->class('widget');
}

$nodes = null;

if(! count($changes)){
	$nodes .= tr(td('@no_records')->colspan(3)->class('norecords'));

}else{
	foreach($changes as $change){
		$nodes .= tr(
			td(
				span('['.$change['id'].']')->class('info'),
				' ',
				a($change['name'])->href(url($change['object'] == 'page' ? 'admin/pages/edit/'.$change['id'] : 'admin/products/edit/'.$change['id']))
			),
			td(fdate($change['date'], true)),
			td($change['user'])
		);
	}
}

$widget_changes = div(
	h3('@last_changes'),
	div(
		table(
			thead(
				tr(
					td('@object'),
					td('@date')->width(110),
					td('@user')->width(80)
				)
			),
			tbody(
				$nodes
			)
		)->class('datagrid')

	)->class('widget-content')

)->class('widget no-border');

$notifications = null;

if($msgs = Notify::messages()){
	foreach($msgs as $msg){
		$notifications .= div(span(fdate($msg->datetime, true), a('@hide')->href('#'.$msg->uid)->class('notify-close'))->class('dtime'), p($msg->url ? a((string) $msg)->href($msg->url) : (string) $msg))->class('notify-msg notify-msg-'.$msg->type);
	}
}

echo
div()->id('dashboard')->class('clearfix')->append(

	$notifications,

	div()->class('col')->append(
		div(
			User::has(User::has_or, 'orders', 'orders-view') ? a('@manager_orders')->href(url('admin/orders'))->class('orders') : '',
			User::has(User::has_or, 'products') ? a('@new_product')->href(url('admin/products/edit/0'))->class('add') : '',
			User::has(User::has_or, 'pages') ? a('@new_page')->href(url('admin/pages/edit/0'))->class('add') : ''
		)->class('quicklinks clearfix'),

		$widget_orders
	),

	div()->class('col')->append(
		$widget_stats,
		$widget_changes
	)
);