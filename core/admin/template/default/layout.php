<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo tpl('layout_head.php'); ?>
  <div id="container">
    <?php echo tpl('header.php'); ?>

    <div id="main">
		<?php echo $content?>
    </div>

    <?php echo tpl('footer.php'); ?>

  </div> <!--! end of #container -->

<?php echo tpl('layout_foot.php'); ?>