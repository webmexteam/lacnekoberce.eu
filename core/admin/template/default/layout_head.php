<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><!doctype html>
<html lang="<?php echo Core::$language?>" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php echo Core::config('store_name')?> | WEBMEX admin</title>

  <meta name="author" content="www.webmex.cz">

  <base href="<?php echo $base?>">

  <link rel="shortcut icon" href="<?php echo $base.$tplbase?>favicon.ico">
  <link rel="stylesheet" href="<?php echo url('style/admin', array('v' => $v), true)?>">

  <script src="<?php echo $base.APPDIR?>/js/modernizr-1.5.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/jquery-1.7.2.min.js?v=<?php echo $v?>"></script>

  <!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="<?php echo $base.APPDIR?>/js/selectivizr.js"></script>
  <![endif]-->

  <script src="<?php echo $base.APPDIR?>/js/plugins.js?v=<?php echo $v?>"></script>

  <script src="<?php echo url('script/constants/admin', array('v' => $v, 't' => time()))?>"></script>
  <script src="<?php echo url('script/lang/admin/'.Core::$language, array('v' => $v))?>"></script>

  <script>
  	_active_tab = '<?php echo Core::$active_tab?>';
  </script>

  <script src="<?php echo $base.APPDIR?>/js/util.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/ckeditor/ckeditor.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.mousewheel-3.0.4.pack.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/fancybox/jquery.fancybox-1.3.4.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/datepicker/datepicker.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/bootstrap/js/bootstrap.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/js/admin.js?v=<?php echo $v?>"></script>

  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.gears.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.flash.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.silverlight.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.html5.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/plupload.html4.min.js?v=<?php echo $v?>"></script>
  <script src="<?php echo $base.APPDIR?>/vendor/plupload/js/jquery.plupload.queue.min.js?v=<?php echo $v?>"></script>

  <?php foreach(View::$js_files['admin'] as $js): ?>
  <script src="<?php echo $base.$js?>"></script>
  <?php endforeach; ?>

  <?php foreach(View::$css_files['admin'] as $css): ?>
  <link rel="stylesheet" href="<?php echo url('style/admin/'.$css, array('v' => $v), true)?>">
  <?php endforeach; ?>
</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->