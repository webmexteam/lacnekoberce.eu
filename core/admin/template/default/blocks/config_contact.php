<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
echo forminput('text', 'block_config[email]', $block->config['email'], array('label' => __('email')))
.forminput('checkbox', 'block_config[captcha]', $block->config['captcha'], array('label' => __('captcha')));