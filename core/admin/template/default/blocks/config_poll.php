<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><style>
	fieldset.inline .inputwrap {
		width: 300px !important;
	}
</style>

<?php echo forminput('text', 'block_config[question]', $block->config['question'], array('label' => __('question')))?>

<?php for($i = 0; $i < 7; $i++): ?>
<fieldset class="inline">
	<?php echo forminput('text', 'block_config[answers]['.$i.'][text]', $block->config['answers'][$i]['text'], array('label' => __('answer').' '.($i+1)))
	.forminput('text', 'block_config[answers]['.$i.'][votes]', $block->config['answers'][$i]['votes'], array('cls' => 'short right', 'label' => __('votes')))?>
</fieldset>
<?php endfor; ?>