<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<form class="form" action="<?php echo url('admin/import/import_osc')?>" method="post">
	<h2><?php echo __('import')?></h2>
	
	<div class="clearfix">
		
		<fieldset class="inline" style="margin-bottom:10px;">
			<div style="margin-bottom:5px;">
				<h3>OS Commerce <?php echo $data['version']?> (<?php echo $data['config']['STORE_NAME']?>)</h3>
			</div>
			
			<label><input type="checkbox" name="delete_data" /> <?php echo __('delete_existing_data')?></label><br />
			<label><input type="checkbox" name="import_config" checked="checked" /> <?php echo __('import_config')?></label><br />
			<label><input type="checkbox" name="import_pages" checked="checked" /> <?php echo __('import_pages')?></label><br />
			<label><input type="checkbox" name="import_products" checked="checked" /> <?php echo __('import_products')?></label><br />
			<label><input type="checkbox" name="import_files" checked="checked" /> <?php echo __('import_files')?></label><br />
			<label><input type="checkbox" name="import_orders" checked="checked" /> <?php echo __('import_orders')?></label>
			
		</fieldset>
		
		<button type="submit" name="next" class="button"><?php echo __('next')?></button>
		
	</div>

</form>