<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>
<form class="form" action="<?php echo url(true)?>" method="post">
	<h2><?php echo __('import')?></h2>

	<div class="clearfix">

		<fieldset class="inline" style="margin-bottom:10px;">
			<div style="margin-bottom:5px;">
				<h3>Quick.Cart <?php echo $data['version']?> (<?php echo $data['config']['language']?>)</h3>
			</div>

			<label><input type="checkbox" name="delete_data" /> <?php echo __('delete_existing_data')?></label><br />
			<label><input type="checkbox" name="import_config" checked="checked" /> <?php echo __('import_config')?></label><br />
			<label><input type="checkbox" name="import_pages" checked="checked" /> <?php echo __('import_pages')?></label><br />
			<label><input type="checkbox" name="import_products" checked="checked" /> <?php echo __('import_products')?></label><br />
			<label><input type="checkbox" name="import_files" checked="checked" /> <?php echo __('import_files')?></label><br />

			<?php if(! isset($panavis)): ?>
			<label><input type="checkbox" name="import_orders" checked="checked" /> <?php echo __('import_orders')?></label>
			<?php endif; ?>

			<?php if(preg_match('/h2/i', $data['version_spec'])): ?>
			<br /><label><input type="checkbox" name="import_invoices" checked="checked" /> <?php echo __('import_invoices')?></label>
			<?php endif; ?>
		</fieldset>

		<button type="submit" name="next" class="button"><?php echo __('next')?></button>

	</div>

</form>