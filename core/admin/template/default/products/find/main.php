<style>
	.toggle-advsearch {
		float: right;
		margin: 10px 10px 0 0;
	}
	.gridsearch {
		border-width: 0 0 1px 0;
		margin: 0;
	}
	.gridsearch label {
		width: 70px !important;
	}
	.gridsearch .input-text,
	.gridsearch select {
		width: 65% !important;
	}
</style>

<script>
	window._products = {};
</script>

<form class="form" action="<?php echo url('admin/products/find_main')?>" method="get">
	<div class="gridsearch">
		<fieldset class="clearfix label-left">
			<a href="#" class="toggle-advsearch"><?php echo __('advanced_search')?></a>
			<?php echo forminput('text', 'search', ! empty($_GET['search']) ? $_GET['search'] : '')?>

			<button type="submit" class="button"><?php echo __('search')?> &rsaquo;</button>

			<?php if(isSet($_GET['search'])): ?>
			<button type="submit" name="cancel_search" class="button"><?php echo __('cancel_search')?> &rsaquo;</button>
			<?php endif; ?>
		</fieldset>

		<fieldset class="clearfix label-left advsearch" style="display:<?php echo (! empty($_GET['filter_what']) ? 'block' : 'none')?>">
			<?php echo forminput('select', 'filter_what', ! empty($_GET['filter_what']) ? $_GET['filter_what'] : '', array('options' => array(
				'' => __('filter_by'),
				'id' => __('id'),
				'sku' => __('sku'),
				'ean13' => __('ean13'),
				'status' => __('status'),
				'price' => __('price'),
				'stock' => __('stock'),
				'availability' => __('availability'),
				'promote' => __('promote'),
				'_no_image' => __('filter_no_image'),
				'_no_category' => __('filter_no_category'),
			)))
			.forminput('text', 'filter_value', isSet($_GET['filter_value']) ? $_GET['filter_value'] : '', array('cls' => 'long'))?>
		</fieldset>
	</div>
</form>

<form class="form gridform" action="<?php echo url('admin/products/find_main')?>" method="post">
	<div class="buttons clearfix">
		<?php echo gridpagination($total_count)?>
	</div>

	<table class="datagrid">

		<thead>
			<tr>
				<td class="rowselect center input" width="20">
					<input type="checkbox" name="all" value="1" class="check-all" />
				</td>
				<td class="right<?php echo ($order_by == 'id' ? ' sort-'.$order_dir : '')?>" width="40"><a href="<?php echo url(true, array('sort' => 'id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('id')?></a></td>
				<td class="<?php echo ($order_by == 'sku' ? ' sort-'.$order_dir : '')?>" width="90"><a href="<?php echo url(true, array('sort' => 'sku', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('sku')?></a></td>
				<td width="70">&nbsp;</td>
				<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('name')?></a></td>

				<td class="right<?php echo ($order_by == 'price' ? ' sort-'.$order_dir : '')?>" width="90"><a href="<?php echo url(true, array('sort' => 'price', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __(PRICE_NAME)?></a></td>
				<td class="center<?php echo ($order_by == 'stock' ? ' sort-'.$order_dir : '')?>" width="60"><a href="<?php echo url(true, array('sort' => 'stock', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('stock')?></a></td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($products)): ?>
				<tr class="norecords">
					<td colspan="7"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($products as $product): ?>
			<script>
				_products[<?php echo $product['id']?>] = {
					name: '<?php echo addslashes($product['name'].' '.$product['nameext'])?>',
					price: '<?php echo ((float) $product['price'] + (float) $product['recycling_fee'] + (float) $product['copyright_fee'])?>',
					sku: '<?php echo addslashes($product['sku'])?>',
					vat: '<?php echo (float) $product['vat']?>',
					attrs: {}
				};
			</script>
			<tr>
				<td class="rowselect center input">
					<input type="checkbox" name="item[]" value="<?php echo $product['id']?>" />
				</td>
				<td class="right" width="40"><?php echo $product['id']?></td>
				<td><?php echo $product['sku']?></td>
				<td class="center">
					<?php if(($img = imgsrc($product, 1, 2, $alt)) !== null): ?>
					<img src="<?php echo $img?>" alt="<?php echo $alt?>" style="max-width:60px;max-height:40px;" />
					<?php endif; ?>
				</td>
				<td>
					<?php echo $product['name'].' '.$product['nameext']?>


					<?php if(Core::$is_premium): $i = 0;

					$attributes = null;
					$attrs_skus = array();
					$attrs_eans = array();
					$has_default = false;

					foreach($product->product_attributes()->order('name ASC, id ASC') as $attr){
						$price = estPrice($attr['price'], $product['price']);
						$sku = $attr['sku'];
						$ean = $attr['ean13'];
						$stock = ($attr['stock'] !== '' && $attr['stock'] !== null) ? $attr['stock'] : null;

						if(strrpos($sku, '*') !== false){
							$sku = preg_replace('/\*/', $product['sku'], $sku);

							if(! in_array($attr['name'], $attrs_skus)){
								$attrs_skus[] = $attr['name'];
							}
						}

						if(strrpos($ean, '*') !== false){
							$ean = preg_replace('/\*/', $product['ean13'], $ean);

							if(! in_array($attr['name'], $attrs_eans)){
								$attrs_eans[] = $attr['name'];
							}
						}

						if(! $has_default && $attr_enable && $attr['is_default']){
							$has_default = true;
						}

						$attributes[$attr['name']][] = array(
							'id' => $attr['id'],
							'value' => $attr['value'],
							'price' => $price,
							'default' => ($attr_enable && $attr['is_default']),
							'sku' => $sku,
							'ean13' => $ean,
							'stock' => $stock,
							'availability' => $attr->availability,
							'file_id' => $attr['file_id'],
							'enable' => $attr_enable
						);
					}

					if($attributes && ! $has_default){
						$key = key($attributes);
						$attributes[$key][0]['default'] = 1;
					}

					if($attributes): foreach($attributes as $attr_name => $options): if($attr_name): ?>
					<fieldset class="attribute">
						<label><?php echo $attr_name?>:</label>

						<select name="attributes[<?php echo $product['id']?>][]">
							<?php foreach($options as $opt): ?>
							<option value="<?php echo $opt['id']?>"<?php echo ($opt['default'] ? ' selected="selected"' : '')?>>
								<?php echo $opt['value']?>
								<?php echo ($opt['price'] ? ' ('.($opt['price'] > 0 ? '+' : '').price($opt['price']).')' : '')?>
							</option>
							<script>
								_products[<?php echo $product['id']?>]['attrs'][<?php echo $opt['id']?>] = {
									name: '<?php echo addslashes($attr_name.': '.$opt['value'])?>',
									price: '<?php echo (float) $opt['price']?>',
									sku: '<?php echo addslashes($opt['sku'])?>'
								};
							</script>
							<?php endforeach; ?>
						</select>
					</fieldset>
					<?php endif; $i ++; endforeach;  endif;
					endif; ?>

				</td>
				<td class="right" width="110"><?php echo fprice($product['price']).(($fees = (float) $product['recycling_fee'] + (float) $product['copyright_fee']) ? ' (+'.fprice($fees).')' : '')?></td>
				<td class="center" width="40"><?php echo $product['stock']?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>

	</table>

	<div class="buttons buttons-foot clearfix">
		<?php echo gridpagination($total_count)?>
	</div>
</form>

<script>
	$(function(){
		$('.check-all').bind('click', function(){
			var inputs = $(this).parents('table:first').find('tbody').find('input[type="checkbox"]');

			if(this.checked){
				inputs.attr('checked', true);

			}else{
				inputs.removeAttr('checked', true);
			}
		});

		$('.toggle-advsearch').bind('click', function(){
			$(this).parent().parent().find('.advsearch').toggle();
			return false;
		});

		$('select[name="filter_what"]').bind('change', function(){
			var value = this.value;

			if(value.substr(0, 1) == '_'){
				$('input[name="filter_value"]').attr('disabled', true).parent().css('opacity', 0.4);

			}else{
				$('input[name="filter_value"]').removeAttr('disabled').parent().css('opacity', 1);
			}
		}).trigger('change');
	});
</script>