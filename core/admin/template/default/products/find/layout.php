<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>
<style>
html {
	overflow: hidden;
}
</style>

<div id="sidebar">
	<div class="tbar">
		<h3><?php echo __('categories')?></h3>
	</div>

	<iframe src="<?php echo url('admin/products/find_sidebar')?>" frameborder="0" border="0" width="100%" style="margin:0;padding:0;border:0;" class="ifr"></iframe>
</div>

<div id="main">
	<div class="tbar">
		<h3><?php echo __('products')?></h3>
	</div>

	<iframe src="<?php echo url('admin/products/find_main')?>" frameborder="0" border="0" width="100%" class="ifr" id="mainframe" name="mainframe"></iframe>
</div>

<div id="ifrfoot">
	<button class="button" id="insert"><?php echo __('insert_selected')?></button>
</div>

<script>
	$(function(){
		var mainframe = $('#mainframe')[0];

		var resize = function(){
			var wh = $(window).height();

			$('iframe.ifr').height(wh - 26 - 45);
		}

		$(window).resize(resize);

		$('#insert').bind('click', function(){
			var products = [];

			$('input[name^="item["]:checked', mainframe.contentDocument).each(function(){
				var data = {};
				var product_id = this.value;

				var product_data = data = $.extend({}, mainframe.contentWindow._products[product_id]);

				data.product_id = product_id;

				if(product_data.attrs){
					$(this).parent().parent().find('select[name^="attributes['+product_id+']"]').each(function(){
						var attr_id = this.value;

						if(attr = product_data.attrs[attr_id]){
							data.name += ' ['+attr.name+']';
							data.price = parseFloat(data.price) + parseFloat(attr.price);

							if(attr.sku) data.sku = attr.sku;
						}
					});
				}

				data.price = Math.round(data.price * 100) / 100;
				data.vat = Math.round(data.vat * 100) / 100;

				products.push(data);

				this.checked = false;
			});

			if(window.opener && window.opener._pf_callback){
				window.opener._pf_callback.call(window, products);
			}

			return false;
		});

		setTimeout(resize, 1000);
	});
</script>