<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 if(count($pages)): ?>
<ul>
	<?php if(isSet($show_all)): ?>
	<li><a href="<?php echo url(true, array('p' => 0))?>"><?php echo __('all')?></a></li>
	<?php endif; ?>
	
	<?php foreach($pages as $page): ?>
	<li<?php echo (in_array($page['id'], (array) $expanded) ? ' class="active"' : '')?>>
	
	<a href="<?php echo url(true, array('p' => $page['id']))?>"><?php echo $page['name']?></a>
	
	<?php if(in_array($page['id'], (array) $expanded)): ?>
	<?php echo tpl('products/find/pages_ul.php', array('pages' => $controller->find_getPages($page['id']), 'expanded' => $expanded))?>
	<?php endif; ?>
	
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>