<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<style>
body {
	background: #fff;
}
ul {
	margin: 0;
	padding: 0;
	list-style: none;
}
ul li {
	padding: 3px 5px;
	border-bottom: 1px solid #ddd;
}
ul li li {
	border-bottom: 0;
	margin-left: 10px;
}
ul li.active a {
	font-weight: bold;
}
</style>

<?php echo tpl('products/find/pages_ul.php', array('pages' => $pages, 'expanded' => $expanded, 'show_all' => 1))?>

<script>
	$(function(){
		$('li a').bind('click', function(){
			var category_id = this.href.match(/p\=(\d+)/)[1];
			
			window.parent.frames['mainframe'].location = '<?php echo url('admin/products/find_main', array('category' => ''))?>'+category_id;
		});
	});
</script>