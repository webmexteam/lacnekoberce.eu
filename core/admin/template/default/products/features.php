<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<?php foreach($featureset->feature()->order('position ASC, id ASC') as $feature):

	if($feature['type'] == 'text'){
		echo '<div class="inputwrap clearfix"><label for="inp-features['.$feature['id'].']" style="float:none;">'.($feature['name'].($feature['unit'] ? ' ['.$feature['unit'].']' : '')).':</label><div class="editor"><textarea name="features['.$feature['id'].']" id="inp-features['.$feature['id'].']" class="input-textarea" rows="7">'.htmlspecialchars($values ? @$values['f'.$feature['id']] : $feature['value']).'</textarea></div></div>';

	}else if($feature['type'] == 'yesno'){
		echo forminput('select', 'features['.$feature['id'].']', isSet($values['f'.$feature['id']]) ? $values['f'.$feature['id']] : (int) $feature['value'], array(
			'label' => $feature['name'].($feature['unit'] ? ' ['.$feature['unit'].']' : ''),
			'options' => array('0' => __('no'), '1' => __('yes')),
			'cls' => 'medium'
		));

	}else if($feature['type'] == 'number'){
		echo forminput('text', 'features['.$feature['id'].']', $values ? @$values['f'.$feature['id']] : $feature['value'], array(
			'label' => $feature['name'].($feature['unit'] ? ' ['.$feature['unit'].']' : ''),
			'cls' => 'medium right'
		));

	}else if($feature['type'] == 'select'){
		$_values = array('' => '&mdash;');

		foreach(array_map('trim', preg_split('/\r?\n/', $feature['value'])) as $value){
			if($value){
				$_values[$value] = $value;
			}
		}

		echo forminput('select', 'features['.$feature['id'].']', @$values['f'.$feature['id']], array(
			'label' => $feature['name'].($feature['unit'] ? ' ['.$feature['unit'].']' : ''),
			'options' => $_values
		));

	}else{
		echo forminput('text', 'features['.$feature['id'].']', $values ? @$values['f'.$feature['id']] : $feature['value'], array(
			'label' => $feature['name'].($feature['unit'] ? ' ['.$feature['unit'].']' : '')
		));
	}
endforeach; ?>