<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><h2><a href="<?php echo url('admin/products')?>"><?php echo __('products')?></a> &rsaquo; <?php echo __('export_import')?></h2>


<form class="form clearfix" action="<?php echo url('admin/products/export_import')?>" method="post" enctype="multipart/form-data">

	<div style="width:300px;float:left;margin-right:30px;">
		<h3><?php echo __('export')?></h3>

		<fieldset>
		    <?php echo forminput('select', 'type', 'simple', array('options' => array(
			'simple' => __('export_type_simple'),
			'advanced' => __('export_type_advanced'),
			'simple_xls' => __('export_type_simple_xls'),
			'advanced_xls' => __('export_type_advanced_xls'),
		    )))?>
		</fieldset>

		<div class="buttons buttons-foot">
			<button type="submit" class="button" name="download_csv"><?php echo __('download_export')?> &rsaquo;</button>
		</div>
	</div>

	<div style="width:300px;float:left;">
		<h3><?php echo __('import')?></h3>

		<fieldset>
		    <?php echo forminput('file', 'import_file', '', array())?>
		</fieldset>

		<div class="buttons buttons-foot">
			<button type="submit" class="button" name="upload_import"><?php echo __('upload_import')?> &rsaquo;</button>
		</div>
	</div>

</form>