<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><a href="<?php echo url('admin/products/edit/0')?>" class="addbtn"><?php echo __('new_product')?></a>


<h2><?php echo __('products')?></h2>

<style>
	.bulk {
		background: #feffef;
		padding: 10px;
		margin-bottom: 10px;
		border: 1px solid #f5eea5;
	}
	.bulk a {
		margin-left: 20px;
	}
	.product-pages .page {
		font-size: 90%;
		border-right: 1px solid #d6d6d6;
		padding-right: 5px;
		margin-right: 2px;
	}
	.product-pages .page:last-child {
		border-right: none;
	}
	.toggle-advsearch {
		float: right;
		margin: 10px 10px 0 0;
	}
	.gridsearch label {
		width: 70px !important;
	}
	.gridsearch .input-text,
	.gridsearch select {
		width: 65% !important;
	}
</style>

<?php if(isSet($_SESSION['bulk']) && count($_SESSION['bulk'])): ?>
<div class="bulk">
	<?php echo __('bulk_changes')?>: <strong><?php echo count($_SESSION['bulk']).' '.__('items')?></strong> <a href="<?php echo url('admin/products/bulk')?>"><?php echo __('go_to_bulk_changes')?> &raquo;</a>
</div>
<?php endif; ?>

<form class="form" action="<?php echo url('admin/products')?>" method="get">
	<?php if(Core::$fix_path): ?>
	<input type="hidden" name="uri" value="<?php echo Core::$orig_uri?>" />
	<?php endif; ?>

	<div class="gridsearch">
		<fieldset class="clearfix label-left">
			<a href="#" class="toggle-advsearch"><?php echo __('advanced_search')?></a>
			<?php echo forminput('text', 'search', ! empty($_GET['search']) ? $_GET['search'] : '')
			.forminput('select', 'category', ! empty($_GET['category']) ? $_GET['category'] : '', array('options' => $categories))?>

			<button type="submit" class="button"><?php echo __('search')?> &rsaquo;</button>

			<?php if(isSet($_GET['search'])): ?>
			<button type="submit" name="cancel_search" class="button"><?php echo __('cancel_search')?> &rsaquo;</button>
			<?php endif; ?>
		</fieldset>

		<fieldset class="clearfix label-left advsearch" style="display:<?php echo (! empty($_GET['filter_what']) ? 'block' : 'none')?>">
			<?php echo forminput('select', 'filter_what', ! empty($_GET['filter_what']) ? $_GET['filter_what'] : '', array('options' => array(
				'' => __('filter_by'),
				'id' => __('id'),
				'sku' => __('sku'),
				'ean13' => __('ean13'),
				'status' => __('status'),
				'price' => __('price'),
				'stock' => __('stock'),
				'availability' => __('availability'),
				'promote' => __('promote'),
				'_no_image' => __('filter_no_image'),
				'_no_category' => __('filter_no_category'),
			)))
			.forminput('text', 'filter_value', isSet($_GET['filter_value']) ? $_GET['filter_value'] : '', array('cls' => 'long'))?>
		</fieldset>
	</div>
</form>

<form class="form gridform" action="<?php //echo url('admin/products')?>" method="post">

	<div class="buttons">
		<?php echo gridpagination($total_count)?>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<table class="datagrid">
		<thead>
			<tr>
				<td class="rowselect center input" width="20">
					<input type="checkbox" name="all" value="1" class="check-all" />
				</td>
				<td class="right<?php echo ($order_by == 'id' ? ' sort-'.$order_dir : '')?>" width="40"><a href="<?php echo url(true, array('sort' => 'id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('id')?></a></td>
				<td class="<?php echo ($order_by == 'sku' ? ' sort-'.$order_dir : '')?>" width="90"><a href="<?php echo url(true, array('sort' => 'sku', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('sku')?></a></td>
				<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('name')?></a></td>
				<td><?php echo __('pages')?></td>
				<td class="center<?php echo ($order_by == 'price' ? ' sort-'.$order_dir : '')?>" width="90"><a href="<?php echo url(true, array('sort' => 'price', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __(PRICE_NAME)?></a></td>
				<td class="center<?php echo ($order_by == 'stock' ? ' sort-'.$order_dir : '')?>" width="100"><a href="<?php echo url(true, array('sort' => 'stock', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('stock')?></a></td>
				<td class="center<?php echo ($order_by == 'position' ? ' sort-'.$order_dir : '')?>" width="60"><a href="<?php echo url(true, array('sort' => 'position', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('position')?></a></td>
				<td class="center<?php echo ($order_by == 'status' ? ' sort-'.$order_dir : '')?>" width="60"><a href="<?php echo url(true, array('sort' => 'status', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')), false, true)?>"><?php echo __('status')?></a></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($products)): ?>
				<tr class="norecords">
					<td colspan="10"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($products as $product): ?>
				<tr>
					<td class="rowselect center input">
						<input type="checkbox" name="item[<?php echo $product['id']?>]" value="1" />
					</td>
					<td class="right" width="40"><?php echo $product['id']?></td>
					<td><?php echo $product['sku']?></td>
					<td><a href="<?php echo url('admin/products/edit/'.$product['id'], array(),false,true)?>"><?php echo $product['name'].' '.$product['nameext']?></a></td>
					<td class="product-pages">
						<?php foreach($product->product_pages() as $page): ?>
							<span class="page"><?php echo pageFullPath($page->page)?></span>
						<?php endforeach; ?>
					</td>
					<td class="center input">
						<input type="text" name="price[<?php echo $product['id']?>]" class="input-text right" value="<?php echo (notEmpty($product['price']) ? fprice($product['price']) : '')?>" />
					</td>
					<td class="center input" style="vertical-align: middle;line-height: 23px;">
						<input type="text" style="width:60%;float:left;" name="stock[<?php echo $product['id']?>]" class="input-text center" value="<?php echo $product['stock']?>" />&nbsp;<?php echo (!empty($product['unit'])) ? $product['unit'] : Core::config('default_unit')?>
					</td>
					<td class="center input">
						<input type="text" name="position[<?php echo $product['id']?>]" class="input-text center" value="<?php echo $product['position']?>" />
					</td>
					<td class="center input">
						<input type="checkbox" name="status[<?php echo $product['id']?>]" class="input-checkbox"<?php echo ($product['status'] ? ' checked="checked"' : '')?> />
					</td>
					<td class="actions">
						<a href="<?php echo url('admin/products/copy/'.$product['id'])?>" class="copy" title="<?php echo __('copy')?>">&nbsp;</a>
						<a href="<?php echo url('admin/products/edit/'.$product['id'], array(),false,true)?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/products/delete/'.$product['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="buttons buttons-foot">
		<select name="action" class="action" style="width:100px;">
			<option disabled="disabled" selected="selected"><?php echo __('marked_action')?></option>
			<option value="bulk"><?php echo __('add_to_bulk')?></option>
			<option value="delete"><?php echo __('delete')?></option>
		</select>

		<?php echo gridpagination($total_count)?>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

</form>

<script>
	$(function(){
		$('.check-all').bind('click', function(){
			var inputs = $(this).parents('table:first').find('tbody').find('input[type="checkbox"][name^="item"]');

			if(this.checked){
				inputs.attr('checked', true);

			}else{
				inputs.removeAttr('checked', true);
			}
		});

		$('select[name="action"]').bind('change', function(){
			var action = $(this).val();
			var form = $(this).parents('form:first');

			if(action == 'delete'){
				if(confirm(_lang.confirm_delete)){
					form.submit();
				}else{
					$(this).val('');
				}

			}else{
				form.submit();
			}
		});

		$('.toggle-advsearch').bind('click', function(){
			$(this).parent().parent().find('.advsearch').toggle();
			return false;
		});

		$('select[name="filter_what"]').bind('change', function(){
			var value = this.value;

			if(value.substr(0, 1) == '_'){
				$('input[name="filter_value"]').attr('disabled', true).parent().css('opacity', 0.4);

			}else{
				$('input[name="filter_value"]').removeAttr('disabled').parent().css('opacity', 1);
			}
		}).trigger('change');
	});
</script>