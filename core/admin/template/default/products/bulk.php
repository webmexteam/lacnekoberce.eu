<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<style>
	.products {
		background: #f6f6f6;
		border: 1px solid #ddd;
		padding: 10px;
		margin-bottom: 20px;
	}
	.products .total {
		margin-bottom: 10px;
		padding-bottom: 5px;
		border-bottom: 1px solid #ddd;
	}
	.products ul {
		margin: 0;
		padding: 0;
		max-height: 200px;
		overflow: auto;
		list-style: none;
	}
	.products ul li {
		margin: 5px 0;
	}
	.products ul li .product-id {
		display: inline-block;
		width: 50px;
		margin-right: 10px;
		text-align: right;
	}
	.products ul li .remove {
		color: #888;
		margin-left: 10px;
	}
	.col-param {
		width: 150px;
		float: left;
	}
	.col-value {
		margin-left: 190px;
	}
</style>

<form class="form" action="<?php echo url('admin/products/bulk')?>" method="post">
	<h2><a href="<?php echo url('admin/products')?>"><?php echo __('products')?></a> &rsaquo; <?php echo __('bulk_changes')?></h2>

	<div class="products">
		<div class="total"><strong><?php echo count($product_ids).' '.__('items')?></strong> (<a href="<?php echo url('admin/products/bulk/cancel')?>"><?php echo __('cancel_bulk')?></a>)</div>

		<ul>
			<?php foreach($product_ids as $product_id): $product = Core::$db->product[$product_id]; ?>
			<li>
				<a href="<?php echo url('admin/products/edit/'.$product['id'])?>" class="product-id"><?php echo $product['id']?></a>
				<?php echo ($product['sku'] ? '('.$product['sku'].') ' : '').$product['name']?>
				<a href="<?php echo url('admin/products/bulk/remove/'.$product['id'])?>" class="remove"><?php echo __('remove')?></a>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>

	<fieldset class="clearfix param">
		<div class="col-param">
		<?php echo forminput('select', 'param[]', '', array('label' => __('param'), 'options' => $params))?>
		</div>

		<div class="col-value">
		<?php echo forminput('text', 'value[]', '', array('label' => __('value')))?>
		</div>
	</fieldset>

	<a href="#" class="add_param">+ <?php echo __('add_param')?></a>

	<div class="buttons bbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
	</div>
</form>

<script>
	$(function(){
		$('a.add_param').bind('click', function(){
			var fset = $('fieldset.param:first').clone();

			fset.find('input, select').val('');

			$(this).before(fset);

			return false;
		});
	});
</script>