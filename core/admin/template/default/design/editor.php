<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><script src="<?php echo APPDIR?>/vendor/farbtastic/farbtastic.js"></script>
<link rel="stylesheet" href="<?php echo APPDIR?>/vendor/farbtastic/farbtastic.css">

<div id="designeditor" class="clearfix">
	<form action="<?php echo url(null, array('preview' => 1))?>" method="post" target="preview">
	
	<input type="hidden" name="theme_name"  value="" />
	
	<div class="sidebar">
		<ul>
			<?php foreach($variables as $group => $vars): ?>
			<li><a href="#"><?php echo (($str = __('design_group_'.$group)) == '__design_group_'.$group ? $group : $str)?></a>
				
				<div class="vars">
				<?php foreach($vars as $n => $v): ?>
					<div class="var">
						<label><?php echo (($str = __('design_var_'.$n)) == '__design_var_'.$n ? $n : $str)?>:</label>
						
						<?php if(is_string($v) && preg_match('/^\#[0-9a-f]{3,}/i', $v)): ?>
							<input type="text" name="css[<?php echo ($group == 'global' ? $n : $group.'-'.$n)?>]" value="<?php echo $v?>" class="color" />
							<span class="color" style="background-color:<?php echo $v?>">&nbsp;</span>
						
						<?php elseif(is_array($v) && $n == 'gradient'): ?>
							<input type="text" name="css[<?php echo ($group == 'global' ? $n : $group.'-'.$n)?>-top]" value="<?php echo $v['top']?>" class="color short" />
							<input type="text" name="css[<?php echo ($group == 'global' ? $n : $group.'-'.$n)?>-bottom]" value="<?php echo $v['bottom']?>" class="color short" />
							<span class="color grad" style="background-color:<?php echo $v['top']?>">&nbsp;</span>
							<span class="color grad" style="background-color:<?php echo $v['bottom']?>">&nbsp;</span>
						
						<?php elseif($n == 'radius' || $n == 'width' || $n == 'fontsize'): ?>
							<input type="text" name="css[<?php echo ($group == 'global' ? $n : $group.'-'.$n)?>]" value="<?php echo $v?>" class="pixels" />
						
						<?php elseif($n == 'image'): ?>
							<input type="text" name="css[<?php echo ($group == 'global' ? $n : $group.'-'.$n)?>]" value="<?php echo ($v == 'none' ? '' : $v)?>" class="image" />
							<a href="#" class="filemanager">&nbsp;</a>
						<?php endif; ?>
						
					</div>
				<?php endforeach; ?>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
		
		<div class="colorpicker"><div id="colorpicker"></div><div class="cpoverlay"></div></div>
	</div>
	
	<div class="preview">
		<div class="toolbar">
			<button type="submit" name="save" class="button" style="float:right;"><?php echo __('save')?> &raquo;</button>
			<button type="submit" name="preview" class="button"><?php echo __('update_preview')?></button>
		</div>
		<iframe src="<?php echo url(null, array('preview' => 1))?>" frameborder="0" name="preview" id="preview"></iframe>
	</div>
	
	</form>
</div>
<script type="text/javascript">
	$(function(){
		var sidebar = $('#designeditor .sidebar');
		var overlay = sidebar.find('.cpoverlay');
		var active_color;
		
		$('#designeditor .sidebar li:first').addClass('active');

		sidebar.find('li a:not(.filemanager)').bind('click', function(){
			sidebar.find('li.active').removeClass('active');
			$(this).parent().addClass('active');
			return false
		});

		overlay.css('opacity', 0.7);

		var toggleColorPicker = function(show){
			if(! $.browser.msie){
				overlay.toggle(show);
			}
		}

		toggleColorPicker(true);

		sidebar.find('input.color').bind('focus', function(){
			active_color = $(this);
			toggleColorPicker(false);
			$.farbtastic('#colorpicker').setColor($(this).val());
			
		}).bind('blur', function(){
			if(! $.browser.msie){
				active_color = null;
			}

			toggleColorPicker(true);
		});
		
		$('#colorpicker').farbtastic(function(color){
			if(active_color){
				active_color.val(color);
				active_color.parent().find('span.color:eq('+(active_color.index() - 1)+')').css('backgroundColor', color);
			}
		});

		sidebar.find('span.color').each(function(){
			var m = this.className.match(/gradient\((\#[0-9a-f]{3,6});(\#[0-9a-f]{3,6})\)/i);
			
			if(m){
				$(this).css();
			}
		});

		$('button[name="preview"]').bind('click', function(){
			var url = $('#preview')[0].contentWindow.window.location;
			$('#designeditor form').attr('action', url);
		});

		$('button[name="save"]').bind('click', function(){
			$('#designeditor form').attr('action', '<?php echo url(true)?>').removeAttr('target');
		});
		
		$('a.filemanager').bind('click', function(){
			var inp = $(this).parent().find('input');
			
			filemanager({
				select: 'one',
				callback: function(file){
					file = String(file);
					
					if(file){
						file = file.substr(file.indexOf(_fm_root)).replace(/^\//, '');
						
						inp.val(file);
						
						if(window._fm_window){
							window._fm_window.close();
						}
					}
				}
			});
			
			return false;
		});

		<?php if(! $custom_theme): ?>
		$('button[name="save"]').bind('click', function(){
			var name;
			
			if(name = prompt('<?php echo __('enter_theme_name')?>')){
				$('input[name="theme_name"]').val(dirify(name, true));
				return true;
			}
			
			return false;
		});
		<?php endif; ?>
	});
</script>