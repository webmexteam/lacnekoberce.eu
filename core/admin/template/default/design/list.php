<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<style>
	.template {
		position: relative;
		border: 1px solid #d6d6d6;
		overflow: hidden;
		padding: 5px 10px 5px 40px;
		margin-top: 5px;
	}
	.template .checkbox {
		position: absolute;
		top: 0;
		left: 0;
		background: #f0f0f0;
		padding: 5px;
		height: 600px;
	}
	.template h3 {
		margin: 0;
		font-size: 125% !important;
	}
	.template .info {
		float: left;
		width: 190px;
		margin: 0;
		padding: 0;
		list-style: none;
	}
	.template .info li {
		margin: 0;
		padding: 3px 0;
	}
	.template .styles {
		margin-left: 200px;
	}
	.template .styles .style {
		position: relative;
		border: 1px solid #eee;
		overflow: hidden;
		padding: 5px 5px 5px 40px;
		margin-bottom: 5px;
	}
	.template .styles .style .preview {
		border: 2px solid #ddd;
		padding: 1px;
		float: left;
		margin-right: 15px;
	}
	.template .styles .style .preview img {
		width: 120px;
	}
	.template .styles .style h4 {
		font-size: 110%;
	}
	.template .styles .style .themes {
		padding: 10px 0 0 0;
	}
	.template .styles .style .themes .theme {
		margin-right: 15px;
	}
	.template .styles .style .themes .edit,
	.template .styles .style .themes .delete {
		background-color: #dfdfdf;
		padding: 2px 5px;
		font-size: 90%;
	}
	.template .tplsettings {
		background: #f0f0f0;
		padding: 10px 10px 0 10px;
		margin: 10px 0 5px 200px;
	}

</style>

<h2><?php echo __('design')?></h2>

<form class="form" action="<?php echo url('admin/design')?>" method="post">

	<div class="buttons tbar">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>
	
	<h3><?php echo __('select_template')?>:</h3>
	
	<?php foreach($templates as $tpl => $template): ?>
	<div class="template">
		<div class="checkbox">
			<input type="radio" name="template" value="<?php echo $tpl?>"<?php echo (Core::config('template') == $tpl ? ' checked="checked"' : '')?> /> 
		</div>
		
		<h3><?php echo $template['info']['name']?></h3>
		
		<ul class="info">
			<li><?php echo __('author')?>: <?php echo $template['info']['author']?></li>
			<li><?php echo __('version')?>: <?php echo $template['info']['version']?></li>
			<li><?php echo __('www')?>: <?php echo $template['info']['www']?></li>
		</ul>
		
		<div class="styles">
			<?php foreach($template['styles'] as $style_name => $style): ?>
			<div class="style">
				<div class="checkbox">
					<input type="radio" name="style" value="<?php echo $style_name?>"<?php echo (Core::config('template') == $tpl && Core::config('template_style') == $style_name ? ' checked="checked"' : '')?> /> 
				</div>
				
				<?php if(file_exists(DOCROOT.'core/template/'.$tpl.'/preview_'.$style_name.'.jpg')): ?>
				<div class="preview">
					<img src="<?php echo $base.'core/template/'.$tpl.'/preview_'.$style_name.'.jpg'?>" alt="" />
				</div>
				<?php endif; ?>
				
				<h4><?php echo $style['name']?></h4>

				<div class="themes">
					<?php foreach($style['themes'] as $themename => $theme): ?>
						<span class="theme">
							<label>
								<input type="radio" name="theme" value="<?php echo $theme?>"<?php echo (Core::config('template') == $tpl && Core::config('template_style') == $style_name && Core::config('template_theme') == $theme ? ' checked="checked"' : '')?> />
								<?php echo $theme?></label> 

								<?php if(!file_exists(DOCROOT . 'core/template/' . $tpl . '/css/theme/' . $style['name'] . '.disable')):?>
									<a href="<?php echo url('admin/design/editor/'.$tpl.'/'.$style_name.'/'.$theme)?>" class="edit"><?php echo __('edit')?></a>
									<?php if(substr($themename, 0, 2) == 'c_'): ?>
									<a href="<?php echo url('admin/design/delete/'.$tpl.'/'.$style_name.'/'.$theme)?>" class="delete"><?php echo __('delete')?></a>
									<?php endif; ?>
								<?php endif; ?>
						</span>
					<?php endforeach; ?>
					
					<?php if(in_array('default', $style['themes']) && !file_exists(DOCROOT . 'core/template/' . $tpl . '/css/theme/' . $style['name'] . '.disable')): ?>
					<p style="padding:10px 0 0 0;">
						&rsaquo; <a href="<?php echo url('admin/design/editor/'.$tpl.'/'.$style_name.'/default')?>"><?php echo __('create_theme')?></a>
					</p>
					<?php endif; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
		
		<?php if(file_exists(DOCROOT.'core/template/'.$tpl.'/tpl_settings.php')): ?>
		<div class="tplsettings clearfix inline">
			<?php include(DOCROOT.'core/template/'.$tpl.'/tpl_settings.php') ?>
		</div>
		<?php endif; ?>
		
	</div>
	<?php endforeach; ?>
	
	<div class="buttons bbar">
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

</form>

<script>
	$(function(){
		$('a.delete').bind('click', function(){
			return confirm(_lang.confirm_delete);	
		});
		
		var toggleRadios = function()
		{
			$('input[name="template"]').each(function(){
				var styles = $(this).parent().parent().find('.style > .checkbox > input[type="radio"]');
				
				if($(this).is(':checked')){
					styles.removeAttr('disabled');
					
				}else{
					styles.attr('disabled', true).removeAttr('checked');
				}
				
				styles.each(function(){
					var themes = $(this).parent().parent().find('.theme input[type="radio"]');
					
					if($(this).is(':checked')){
						themes.removeAttr('disabled');
						
					}else{
						themes.attr('disabled', true).removeAttr('checked');
					}
				});
			});
		}
		
		$('form.form input[type="radio"]').bind('click', toggleRadios);
		
		toggleRadios();
	});
</script>