<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><style>
	.topbar .button {
		float: right;
	}
	.topbar .inputwrap {
		width: 150px;
		float: right;
		margin: 0 10px 0 0;
	}
	.topbar .inputwrap label {
		display: none !important;
	}
	.form .status {
		line-height: 22px !important;
	}
	.form .status .input-checkbox {
		vertical-align: middle;
		margin: -1px 3px 0 0;
	}
	td.s-1 {
		background-color: #ffebed !important;
	}
	td.s-2 {
		background-color: #def0ff !important;
	}
	td.s-3 {
		background-color: #e7ffe3 !important;
	}
	td.s-4 {
		background-color: #e9e9e9 !important;
	}
	td.s-5 {
		background-color: #ffecde !important;
	}
	td .paid {
		background-image: url(<?php echo $tplbase?>icons/paid.png);
		background-repeat: no-repeat;
		background-position: right center;
		float: right;
		width: 17px;
	}
	td .confirmed {
		background-image: url(<?php echo $tplbase?>images/tick.png);
		background-repeat: no-repeat;
		background-position: left center;
		float: right;
		width: 17px;
	}
	p.customer_orders {
		background: #f0f0f0;
		padding: 10px;
		margin: 0 0 10px 0;
	}
	.printbar {
		background: #f0f0f0;
		padding: 10px 5px;
		margin: 5px 0 0 0;
		color: #aaa;
	}
	.printbar a {
		color: #444;
		margin: 0 5px;
	}
</style>

<h2><?php echo __('orders')?></h2>

<?php if(! empty($_GET['customer']) && $customer = Core::$db->customer[(int) $_GET['customer']]): ?>
	<p class="customer_orders">
		<?php echo __('orders_for_customer', $customer['first_name'].' '.$customer['last_name'])?>
		(<a href="<?php echo url('admin/orders')?>"><?php echo __('cancel_filter')?></a>)
	</p>
<?php endif; ?>

<form class="form" action="<?php echo url('admin/orders')?>" method="get">
	<?php if(Core::$fix_path): ?>
	<input type="hidden" name="uri" value="<?php echo Core::$orig_uri?>" />
	<?php endif; ?>

	<div class="gridsearch">
		<fieldset class="clearfix label-left">
			<?php echo forminput('text', 'search', ! empty($_GET['search']) ? $_GET['search'] : '')
			.forminput('select', 'where', ! empty($_GET['where']) ? $_GET['where'] : '', array('options' => array(
				'all' => __('all'),
				'id' => __('id'),
				'last_name' => __('last_name'),
				'email' => __('email'),
				'street' => __('street'),
			)))
			.forminput('select', 'status', ! empty($_GET['status']) ? $_GET['status'] : '', array('options' => $search_status_options))?>

			<button type="submit" class="button"><?php echo __('search')?> &rsaquo;</button>
		</fieldset>
	</div>
</form>

<form class="form gridform" action="<?php echo url('admin/orders')?>" method="post">


	<div class="buttons topbar clearfix">
		<?php echo gridpagination($total_count)?>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
		<?php echo forminput('select', 'new_status', '', array('options' => $status_options))?>
	</div>

	<table class="datagrid">
		<thead>
			<tr>
				<td class="rowselect center input" width="20">
					<input type="checkbox" name="all" value="1" class="check-all" />
				</td>
				<td class="right<?php echo ($order_by == 'id' ? ' sort-'.$order_dir : '')?>" width="60"><a href="<?php echo url(true, array('sort' => 'id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('id')?></a></td>
				<td width="100"><?php echo __('invoice')?></td>
				<td class="<?php echo ($order_by == 'last_name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'last_name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('fullname')?></a></td>
				<td class="<?php echo ($order_by == 'company' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'company', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('company')?></a></td>
				<td width="120" class="<?php echo ($order_by == 'received' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'received', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('date')?></a></td>
				<td class="right<?php echo ($order_by == 'total_price' ? ' sort-'.$order_dir : '')?>" width="100"><a href="<?php echo url(true, array('sort' => 'total_price', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('total')?></a></td>
				<td width="140" class="<?php echo ($order_by == 'status' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'status', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('status')?></a></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($orders)): ?>
				<tr class="norecords">
					<td colspan="9"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($orders as $_order): ?>
				<tr>
					<td class="rowselect center input">
						<input type="checkbox" name="item[<?php echo $_order['id']?>]" value="1" />
					</td>
					<td class="right"><?php echo $_order['id']?></td>

					<?php if($invoice = Core::$db->invoice()->where('order_id', $_order['id'])->fetch()): ?>
						<td><a href="<?php echo url('admin/invoices/view/'.$invoice['id'])?>"><?php echo $invoice['invoice_num']?></a></td>
					<?php else: ?>
						<td>&nbsp;</td>
					<?php endif; ?>

					<td><a href="<?php echo url('admin/orders/edit/'.$_order['id'])?>"><?php echo $_order['first_name'].' '.$_order['last_name']?></a></td>
					<td><?php echo $_order['company']?></td>
					<td><?php echo fdate($_order['received'], true)?></td>
					<td class="right"><?php echo price($_order['total_incl_vat'], $_order['currency'])?></td>
					<td class="input status s-<?php echo $_order['status']?>" title="<?php echo ($_order['payment_realized'] ? __('payment_realized') : '')?>">

						<?php if($_order['payment_realized']): ?>
						<span class="paid" title="<?php echo __('payment_realized')?>">&nbsp;</span>
						<?php endif; ?>
						<?php if($_order['confirmed'] && (int) Core::config('confirm_orders')): ?>
						<span class="confirmed" title="<?php echo __('confirmed')?>">&nbsp;</span>
						<?php endif; ?>

						<input type="checkbox" name="orders[]" value="<?php echo $_order['id']?>" class="input-checkbox" />
						<?php echo __('status_'.Core::$def['order_status'][$_order['status']])?>
					</td>

					<td class="actions">
						<a href="<?php echo url('admin/orders/edit/'.$_order['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/orders/delete/'.$_order['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
						<a href="#" data-toggle="popover" data-content-from-element="#tooltip-<?php echo $_order?>" data-placement="left" data-original-title="<?php echo __('order_items')?>" class="zoom" title="<?php echo __('order_items')?>">&nbsp;</a>
						<div id="tooltip-<?php echo $_order?>" style="display:none">
							<table style="font-size:10px;">
								<thead>
									<tr>
										<th style="padding:4px">ID</th>
										<th style="padding:4px">Obrázek</th>
										<th style="padding:4px">Název</th>
										<th style="padding:4px">Počet</th>
									</tr>
								</thead>
								<tbody>
							<?php
								$products = $_order->order_products();

								if(Core::$db_inst->_type == 'sqlite'){
									$products->order('rowid ASC');
								}

								if($_order):
									foreach($products as $product):

										if(isset($product['product_id'])){
											$files = Core::$db->product_files()->where('product_id', $product['product_id'])->order('position ASC')->limit(1);

											if(count($files)){
												foreach ($files as $key => $value) {
													$image = $value;
												}
											}else{
												$image = false;
											}
										}else{
											$image = false;
										}
									?>

									<tr>
										<td style="background:none;"><?php echo $product['product_id'] ? '#'.$product['product_id'] : '';?></td>
										<td style="background:none;">
											<?php if(isset($image) && isset($product['product_id']) && !empty($product['product_id'])):?>
												<img src="<?php echo imgsrc($image, null)?>" width="40" />
											<?php endif?>
										</td>
										<td style="background:none;"><?php echo $product['name']?></td>
										<td style="background:none;"><?php echo $product['quantity']?>x</td>
									</tr>

									<?php endforeach;
								endif;
							?>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="buttons buttons-foot">
		<select name="action" class="action">
			<option disabled="disabled" selected="selected"><?php echo __('marked_action')?></option>
			<option value="delete"><?php echo __('delete')?></option>
		</select>

		<?php echo gridpagination($total_count)?>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="printbar">
		<a href="<?php echo url('admin/orders/print_orders', array('id' => ''))?>" id="orders-print-selected"><?php echo __('print_selected')?></a> |
		<a href="<?php echo url('admin/orders/print_orders')?>"><?php echo __('print_today')?></a> |
		<a href="<?php echo url('admin/orders/print_orders/'.date('Y-m-d', strtotime('-'.(date('N') - 1).' days')).'/'.date('Y-m-d'))?>"><?php echo __('print_this_week')?></a> |
		<a href="<?php echo url('admin/orders/print_orders/'.date('Y-m-d', strtotime('-'.(date('j') - 1).' days')).'/'.date('Y-m-d'))?>"><?php echo __('print_this_month')?></a>
	</div>

</form>

<script>
	$(function(){
		$('#orders-print-selected').bind('click', function(){
			var ids = [];

			$('input[name^="item["]:checked').each(function(){
				ids.push(this.name.match(/\d+/));
			});

			this.href += ids.join(';');
		});

		$('.check-all').bind('click', function(){
			var inputs = $(this).parents('table:first').find('tbody').find('input[type="checkbox"][name^="item"]');

			if(this.checked){
				inputs.attr('checked', true);

			}else{
				inputs.removeAttr('checked', true);
			}
		});

		$('select[name="action"]').bind('change', function(){
			var action = $(this).val();
			var form = $(this).parents('form:first');

			if(action == 'delete'){
				if(confirm(_lang.confirm_delete)){
					form.submit();

				}else{
					$(this).val('');
				}

			}else{
				form.submit();
			}
		});
	});
</script>