<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><style>
	.inline .inputwrap {
		width: 150px !important;
	}
	.attr {
		border-top: 1px solid #e5e5e5;
		border-bottom: 1px solid #c0c0c0;
		padding: 5px 0 0 10px;
		margin: 10px 0 0 0;
		background: #f6f6f6;
	}
	.attr .default {
		clear: left;
	}
	.attr .default .inputwrap {
		margin-left: 120px;
	}
	.attr .default .help {
		display: block;
		clear: left;
		font-size: 90%;
		color: #777;
		margin: 0 0 5px 120px;
	}
	.template {
		display: none;
	}
	.yesnobox {
		display: block;
	}
	.checkboxes .inputwrap {
		margin: 20px 0 0 0 !important;
	}
</style>

<form class="form" action="<?php echo url('admin/attribute_templates/edit/'.$template['id'])?>" method="post">

	<h2><a href="<?php echo url('admin/products')?>"><?php echo __('products')?></a> &rsaquo; <a href="<?php echo url('admin/attribute_templates')?>"><?php echo __('attribute_templates')?></a> &rsaquo; <?php echo $template['name']?></h2>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="clearfix">
		<fieldset style="width:350px">
			<?php echo forminput('text', 'name', $template['name'], array('label' => __('attribute_template_name')))?>
		</fieldset>

		<div class="attrs">
			<?php if(is_object($template) && count($template->product_attribute_template_items())): foreach($template->product_attribute_template_items()->order('id ASC') as $attr): ?>
			<fieldset class="inline attr">
				<?php echo forminput('text', 'attr['.$attr['id'].'][name]', $attr['name'], array('label' => __('name')))
				.forminput('text', 'attr['.$attr['id'].'][value]', $attr['value'], array('label' => __('value')))
				.forminput('text', 'attr['.$attr['id'].'][price]', $attr['price'], array('label' => __('price'), 'cls' => 'short right'))?>
			</fieldset>
			<?php endforeach; endif; ?>

			<fieldset class="inline attr">
				<?php echo forminput('text', 'attr[00][name]', '', array('label' => __('name')))
				.forminput('text', 'attr[00][value]', '', array('label' => __('value')))
				.forminput('text', 'attr[00][price]', '', array('label' => __('price'), 'cls' => 'short right'))?>
			</fieldset>

			<button class="button" name="add_attribute" style="margin-top:10px;"><?php echo __('add_attribute')?></button>
		</div>
	</div>

	<div class="buttons bbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>


</form>

<script>
	$(function(){
		var newId = 1;

		$('button[name="add_attribute"]').bind('click', function(){
			var el = $('fieldset.attr:first').clone();
			var nid = newId++;

			el.find('label').each(function(){
				$(this).attr('for', $(this).attr('for').replace(/attr\[\d+\]/, 'attr[0'+nid+']'));
			});

			el.find('input, select, textarea').each(function(){
				this.value = '';
				this.name = this.name.replace(/^attr\[\d+\]/, 'attr[0'+nid+']');
				this.id = this.id.replace(/attr\[\d+\]/, 'attr[0'+nid+']');
				$(this).removeAttr('disabled');
			});

			$(this).before(el);

			return false;
		});

		$('select.disabled').attr('disabled', true);
	});
</script>