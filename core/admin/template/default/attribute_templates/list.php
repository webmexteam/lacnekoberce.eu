<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?>

<a href="<?php echo url('admin/attribute_templates/edit/0')?>" class="addbtn"><?php echo __('new_attribute_template')?></a>

<h2><a href="<?php echo url('admin/products')?>"><?php echo __('products')?></a> &rsaquo; <?php echo __('attribute_templates')?></h2>

<form class="form gridform" action="<?php echo url('admin/attribute_templates')?>" method="post">

	<table class="datagrid">
		<thead>
			<tr>
				<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('name')?></a></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>

		<tbody>
			<?php if(! count($templates)): ?>
				<tr class="norecords">
					<td colspan="2"><?php echo __('no_records')?></td>
				</tr>
			<?php endif; ?>

			<?php foreach($templates as $template): ?>
				<tr>
					<td><a href="<?php echo url('admin/attribute_templates/edit/'.$template['id'])?>"><?php echo $template['name']?></a></td>

					<td class="actions">
						<a href="<?php echo url('admin/attribute_templates/edit/'.$template['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
						<a href="<?php echo url('admin/attribute_templates/delete/'.$template['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</form>