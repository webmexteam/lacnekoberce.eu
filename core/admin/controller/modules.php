<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Modules extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Core::$active_tab = 'modules';
	}

	public function index()
	{
		if (!empty($_POST)) {
			$post = $_POST;

			if (!empty($post['install'])) {
				$module_name = key($post['install']);
			} else if (!empty($post['deactivate'])) {
				$module_name = key($post['deactivate']);
			} else if (!empty($post['activate'])) {
				$module_name = key($post['activate']);
			}

			if ($module = Core::$modules[$module_name]) {
				if (!empty($post['install'])) {
					if ($mod = Module::install_module($module_name)) {
						$module->install($mod);
					}
				} else if (!empty($post['deactivate'])) {
					if ($mod = Core::$db->module()->where('name', $module_name)->fetch()) {
						$module->deactivate();
					}
				} else if (!empty($post['activate'])) {
					if ($mod = Core::$db->module()->where('name', $module_name)->fetch()) {

						if ($collisions = $module->checkCollisions()) {
							$_SESSION['module_collisions'] = $collisions;
							flashMsg(__('msg_error'), 'error');
							redirect('admin/modules');
						} else {
							$module->activate();
						}
					}
				}

				FS::cleanCache();

				flashMsg(__('msg_saved'));
				redirect('admin/modules');
			}
		}

		$this->content = tpl('modules/list.php', array(
			'modules' => Core::$modules
				));
	}

	public function settings($module_name)
	{
		if (!($module = Core::$modules[$module_name])) {
			redirect('admin/modules');
		}

		if (!empty($_POST)) {
			if ($module->saveSettingForm()) {
				redirect(true);
			}
		}

		$this->content = tpl('modules/settings.php', array(
			'module' => $module
				));
	}

}