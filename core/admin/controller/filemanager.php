<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Filemanager extends AdminController
{

	protected $root;
	public $version = 2;

	public function __construct()
	{
		parent::__construct();

		$this->root = DOCROOT . Core::$def['filemenager_root'];

		define('FM_ROOT', $this->root);
	}

	public function index()
	{
		$this->tpl = 'filemanager' . ($this->version > 1 ? $this->version : '') . '.php';
	}

	/* Version 2 */

	public function emptySrc()
	{
		$this->tpl = null;
	}

	public function getFolders()
	{
		$this->tpl = null;

		echo json_encode(Filemanager::getFolders());

		exit;
	}

	public function getFiles()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];
		$search = $_POST['search'];

		if (is_string($folder) && !$search) {
			echo json_encode(Filemanager::getFiles($folder));
		} else if ($search && strlen($search) >= 2) {

			if (!preg_match('/\*/', $search)) {
				$search = '*' . Filemanager::globistr($search) . '*';
			}

			echo json_encode(Filemanager::searchFiles($search));
		}

		exit;
	}

	public function getFileDetail()
	{
		$this->tpl = null;

		$file = $_POST['file'];
		$folder = $_POST['folder'];

		echo json_encode(Filemanager::getInfo($folder . '/' . $file));

		exit;
	}

	public function renameFile()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];
		$file = $_POST['file'];
		$new_name = $_POST['new_name'];

		$result = Filemanager::renameFile($folder . '/' . $file, $new_name);

		echo json_encode(array('status' => $result));

		exit;
	}

	public function createFolder()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];

		$result = Filemanager::createFolder($folder);

		echo json_encode(array('status' => $result));

		exit;
	}

	public function renameFolder()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];
		$new_name = $_POST['new_name'];

		$result = Filemanager::renameFolder($folder, $new_name);

		echo json_encode(array('status' => $result));

		exit;
	}

	public function downloadFile($file_path_b64)
	{
		$this->tpl = null;

		$path = base64_decode($file_path_b64);

		Filemanager::download($path);

		exit;
	}

	public function deleteFile_2()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];
		$file = $_POST['file'];

		$result = Filemanager::delete($folder . '/' . $file);

		echo json_encode(array('status' => $result));

		exit;
	}

	public function deleteFiles()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];
		$files = explode(';', $_POST['files']);

		foreach ($files as $file) {
			Filemanager::delete($folder . '/' . $file);
		}

		echo json_encode(array('status' => true));

		exit;
	}

	public function deleteFolder()
	{
		$this->tpl = null;

		$folder = $_POST['folder'];

		$result = Filemanager::deleteFolder($folder);

		echo json_encode(array('status' => $result));

		exit;
	}

	public function getConfig()
	{
		$this->tpl = null;

		$config = array(
			'thumbs' => Commercio::config('commercio.fileManager.thumbs')
		);

		echo json_encode($config);

		exit;
	}

	/* Version 1 */

	public function createdir()
	{
		$this->tpl = null;

		$path = $this->root . trim($_POST['path'], '/') . '/';
		$dir = preg_replace('/\.|\s|\//', '-', trim($_POST['dir'], '/'));

		if (is_dir($path) && is_writable($path) && !is_dir($path . $dir)) {
			FS::mkdir($path . $dir, 0777);
			die('1');
		}

		die('0');
	}

	public function deletefile()
	{
		$file = $this->root . $_POST['file'];

		if (is_dir($file)) {
			$this->rmdir($file);

			die('1');
		} else if (file_exists($file)) {
			@unlink($file);

			die('1');
		}

		die('0');
	}

	public function listFiles()
	{
		$this->tpl = null;

		$dir = $this->root . trim($_POST['dir'], '/') . '/';

		$files = array();

		if ($f = glob($dir . '*')) {
			foreach ($f as $file) {
				if (substr($file, 0, 1) == '.') {
					continue;
				}

				$files[] = $this->_getFileInfo($file);
			}
		}

		echo json_encode($files);

		exit;
	}

	public function check()
	{
		$this->tpl = null;
		file_put_contents(DOCROOT . 'etc/tmp/fm.txt', time() . print_r($_POST, true) . print_r($_FILES, true));
		$exist = array();

		$path = ltrim(trim($_POST['path'], '/') . '/', '/');

		foreach ($_POST as $key => $value) {
			if (!in_array($key, array('session_id', 'path', 'folder')) && file_exists(DOCROOT . $this->root . $path . $value)) {
				$exist[$key] = $value;
			}
		}

		echo json_encode($exist);

		exit;
	}

	public function upload()
	{
		$this->tpl = null;

		$disallowed = array('php', 'php3', 'php4', 'php5', 'php6', 'phtml', 'py', 'sh', 'cgi', 'asp');

		if (!empty($_FILES['Filedata']) && empty($_FILES['Filedata']['error'])) {
			$ext = strtolower(substr($_FILES['Filedata']['name'], strrpos($_FILES['Filedata']['name'], '.') + 1));

			if (in_array($ext, $disallowed) || $_FILES['Filedata']['type'] == 'text/php') {
				header('HTTP/1.0 405');
				die('0');
			}

			$path = ltrim(trim($_POST['path'], '/') . '/', '/');
			$name = $_FILES['Filedata']['name']; //dirify(substr($_FILES['Filedata']['name'], 0, strrpos($_FILES['Filedata']['name']) + 1), '.').'.'.$ext;

			move_uploaded_file($_FILES['Filedata']['tmp_name'], $this->root . $path . $name);

			FS::writable($this->root . $path . $name);

			if (in_array($ext, array('gif', 'jpg', 'jpeg', 'png'))) {
				Image::thumbs($this->root . $path . $name);
			}
		}

		die('1');
	}

	public function plupload()
	{
		$this->tpl = null;

		$targetDir = $this->root;
		$disallowed = array('php', 'php3', 'php4', 'php5', 'php6', 'phtml', 'py', 'sh', 'cgi', 'asp');
		$cleanupTargetDir = false; // Remove old files
		$maxFileAge = 60 * 60; // Temp file age in seconds

		$path = '';
		if (isSet($_POST['path'])) {
			$path = ltrim(trim($_POST['path'], '/') . '/', '/');
		}

		$targetDir .= $path;

		// HTTP headers for no cache etc
		header('Content-type: text/plain; charset=UTF-8');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// 5 minutes execution time
		@set_time_limit(5 * 60);
		// usleep(5000);
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
		$chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = dirify($fileName, '-', true);

		$fileinfo = pathinfo($fileName);
		$ext = strtolower($fileinfo['extension']);

		if (in_array($ext, $disallowed)) {
			die('{"jsonrpc" : "2.0", "error" : {"code": 200, "message": "Forbidden file type"}, "id" : "id"}');
		}

		if ((int) Core::config('upload_unique_names') && file_exists($targetDir . '/' . $fileName)) {
			$i = 1;
			$max = 99;

			while ($max && file_exists($targetDir . '/' . $fileinfo['filename'] . '_' . $i . '.' . $fileinfo['extension'])) {
				$i++;
				$max--;
			}

			$fileName = $fileinfo['filename'] . '_' . $i . '.' . $fileinfo['extension'];
		}

		// Remove old temp files
		if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$filePath = $targetDir . DIRECTORY_SEPARATOR . $file;

				// Remove temp files if they are older than the max age
				if (preg_match('/\\.tmp$/', $file) && (filemtime($filePath) < time() - $maxFileAge))
					FS::remove($filePath);
			}

			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];

		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					if (!($in = @fopen($_FILES['file']['tmp_name'], "rb"))) {
						$tmp = DOCROOT . 'etc/tmp/tmpfile_' . uniqid();

						move_uploaded_file($_FILES['file']['tmp_name'], $tmp);

						$in = @fopen($tmp, "rb");
					}

					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

					fclose($in);
					fclose($out);

					$result = true;

					if (isSet($tmp)) {
						@unlink($tmp);
					}

					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");

				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

				fclose($in);
				fclose($out);

				$result = true;
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}

		if ($result) {
			$file = $targetDir . DIRECTORY_SEPARATOR . $fileName;

			FS::writable($file);

			if (in_array($ext, array('gif', 'jpg', 'jpeg', 'png'))) {
				$wm = (Core::$is_premium && isSet($_POST['watermark']) && $_POST['watermark'] == 1);

				if ($wm && Core::$def['image_watermark']['apply_on_original']) {
					$img = new Image($file);
					$img->watermark(Core::$def['image_watermark']);
					$img->save();

					unset($img);

					Image::thumbs($file, false);
				} else {

					Image::thumbs($file, $wm);
				}
			}
		}

		// Return JSON-RPC response
		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "filename":"' . $path . $fileName . '"}');
	}

	private function _getFileInfo($file)
	{
		$width = $height = $mime = $ext = null;

		$type = 'document';

		if (preg_match('/\.([a-zA-Z0-9]*)$/', $file, $matches)) {
			$ext = $matches[1];

			if ($matches && preg_match('/jpe?g|gif|png|bmp/i', $matches[1])) {
				$type = 'image';
			}
		}

		if (is_dir($file)) {
			$type = 'folder';
		}

		if ($type == 'image') {
			list($width, $height) = @getimagesize($file);
		}

		return array(
			'name' => substr($file, strrpos($file, '/') + 1),
			'path' => trim(substr($file, strlen($this->root)), '/'),
			'ext' => $ext,
			'size' => $type == 'folder' ? null : filesize($file),
			'cdate' => $type == 'folder' ? null : date('Y-m-d H:i:s', filectime($file)),
			'type' => $type,
			'width' => $width,
			'height' => $height
		);
	}

	private function rmdir($dir, $deleteRootToo = true)
	{
		if (!$dh = @opendir($dir)) {
			return;
		}

		while (false !== ($obj = readdir($dh))) {
			if ($obj == '.' || $obj == '..') {
				continue;
			}

			if (!@unlink($dir . '/' . $obj)) {
				$this->rmdir($dir . '/' . $obj, true);
			}
		}

		closedir($dh);

		if ($deleteRootToo) {
			@rmdir($dir);
		}

		return;
	}

}