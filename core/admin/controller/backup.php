<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Backup extends AdminController
{

	public function __construct()
	{
		if (!defined('CRON')) {
			parent::__construct();
			gatekeeper('backup');
			Core::$active_tab = 'system';
		}
	}

	public function index()
	{
		$backups = glob(DOCROOT . 'etc/backup/*.zip');

		$this->content = tpl('backup/list.php', array(
			'backups' => $backups ? $backups : array()
				));
	}

	public function create()
	{
		$this->tpl = null;

		if (Core::$is_trial) {
			die('This function is disabled in trial mode');
		}

		@set_time_limit(0);

		$fpath = DOCROOT . 'etc/backup/backup_' . date('Y-m-d_H-i') . '.zip';

		FS::cleanCache();

		$zip = new Zip($fpath);

		$zip->addFile(array('etc', 'core', 'files', 'index.php'));

		$zip->removeDir('etc/backup');
		$zip->removeDir('etc/tmp');
		$zip->addEmptyDir('etc/tmp');

		if (file_exists(DOCROOT . 'etc/data.sqlite.php')) {
			$zip->addFile('etc/data.sqlite.php');
		}

		if (file_exists(DOCROOT . '.htaccess')) {
			$zip->addFile('.htaccess');
		}

		if (file_exists(DOCROOT . 'license.txt')) {
			$zip->addFile('license.txt');
		}

		if (Core::$db_inst->_type == 'mysql') {
			$sql_file = 'mysqldump-' . date('y-m-d_H-i') . '.sql';

			$dump = new Db_Dump(Core::$db_conn, DOCROOT . 'etc/tmp/' . $sql_file);

			if ($dump->dump()) {
				$zip->addFile('etc/tmp/' . $sql_file, $sql_file);
			}
		}

		$zip->create();

		FS::writable($fpath);

		if (isSet($sql_file)) {
			FS::remove(DOCROOT . 'etc/tmp/' . $sql_file);
		}

		if (!defined('CRON')) {
			redirect('admin/backup');
		} else {
			return $fpath;
		}
	}

}