<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Xmlimport extends AdminController
{

	protected $progress;
	protected $is_cron = false;
	protected $is_ajax = false;
	protected $ajax_progress = array(
		'total' => 0,
		'offset' => 0
	);
	public $log = '';

	public function __construct()
	{
		if (defined('CRON') || (preg_match('/^xmlimport\/import\//i', Core::$uri) && !empty($_GET['k']) && $_GET['k'] == Core::config('access_key'))) {
			parent::__construct(true);

			$this->is_cron = true;
		} else {
			parent::__construct();
			gatekeeper('products-export_import');
		}

		Core::$active_tab = 'products';
	}

	public function index()
	{
		$imports = array();

		if ($f = glob(DOCROOT . 'etc/xml_import/*.php')) {
			foreach ($f as $file) {
				$info = pathinfo($file);

				$imports[$info['basename']] = substr($info['basename'], 0, -4);
			}
		}

		$this->content = tpl('xmlimport.php', array(
			'imports' => $imports,
			'key' => Core::config('access_key'),
			'result' => !empty($_SESSION['result']) ? $_SESSION['result'] : null
				));

		unset($_SESSION['result']);
	}

	public function import($schema = null)
	{
		if ($schema && !empty($_GET['k'])) {
			redirect(Core::$url . 'core/cron.php?' . http_build_query(array('k' => $_GET['k'], 'job' => 'dataimport', 'schema' => preg_replace('/(.|_)php$/', '', $schema))));
		}

		$cli_time = time();

		if (!empty($_GET['ajax'])) {
			$this->is_ajax = true;

			$schema = $_GET['schema'];

			echo str_pad(' ', 512);
			ob_end_flush();
		}

		if ($schema && preg_match('/_php$/', $schema)) {
			$schema = preg_replace('/_php$/', '.php', $schema);
		}

		$this->tpl = ($this->is_cron || $this->is_ajax) ? null : 'result.php';

		if (!Core::$is_premium) {
			die($this->is_cron ? "You need premium licence to run this\n" : __('premium_info'));
		}

		$_ids = array();
		$_unique = array();

		$this->getProgress($schema, $_ids);

		@set_time_limit(defined('IS_CLI') ? 0 : 90);
		$time_limit = (int) ini_get('max_execution_time');
		$start_time = time();

		if ($this->progress['schema'] || !empty($_POST)) {

			if (!empty($_POST['schema'])) {
				$this->saveProgress($schema, array(
					'time' => time(),
					'schema' => $_POST['schema'],
					'product_offset' => 0
						), $_ids);
			}

			include(DOCROOT . 'etc/xml_import/' . (!empty($this->progress['schema']) ? $this->progress['schema'] : $_POST['schema']));

			$_insert = 0;
			$_update = 0;

			try {
				$this->log("Start import\n");

				if (!empty($xml_import) && !empty($xml_import['source'])) {

					$source = $xml_import['source'];

					if (substr($source, 0, 3) == 'fn_') {
						$source = $source($xml_import);
					}

					if (preg_match('/\:\/\//', $source) || file_exists($source)) {

						$item = $xml_import['item'];

						$xml = new XMLParser($source);
						$total = $xml->countElement($item);
						unset($xml);

						$this->ajax_progress['total'] = $total;

						$xml = new XMLParser($source);

						$next_ai = Core::$db_inst->getNextAI('product');
						$sql_queue = '';

						$i = 0;
						while ($product = $xml->getElement($item)) {

							$this->ajax_progress['offset'] = $i;

							if ((int) $this->progress['product_offset'] > $i) {
								$i++;
								continue;
							}

							$this->ajaxProgress();

							if (($time_limit && (time() - $start_time) >= ($time_limit - 7)) || (strlen($sql_queue) > 1000000)) {

								if ($sql_queue) {
									Core::$db_conn->exec($sql_queue);
									$sql_queue = '';
								}

								$this->saveProgress($schema, array('product_offset' => $i), $_ids);

								$this->writeLog($this->log);

								if (!defined('IS_CLI') && !$this->is_cron && !$this->is_ajax) {
									redirect('admin/xmlimport/import/' . $this->progress['schema'] . '/' . time());
								}

								if (!defined('IS_CLI')) {
									if (!$this->is_ajax) {
										echo $this->log;
									} else {
										$percent = round(100 / $this->ajax_progress['total'] * ($this->ajax_progress['offset'] + 1));

										echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) $percent, 'log' => '', 'nextStep' => 1)) . '\');</script>';
										flush();
									}
								} else {
									echo "\n!! Run this again. Execution was interrupted by max_execution_time directive. !!\n";
								}

								exit;
							}

							$product_data = array();

							foreach ($xml_import['item_data'] as $o => $x) {
								if (substr($x, 0, 3) == 'fn_' && function_exists($x)) {
									$value = $x($product, $product_data, $xml_import);

									if ($value !== null) {
										$product_data[$o] = $value;
									}
								} else if ($x && property_exists($product, $x)) {
									$value = $product->$x;

									if (is_object($value)) {
										$product_data[$o] = (string) $value;
									} else {
										$product_data[$o] = (string) $value;
									}
								} else if ($x !== null) {
									$product_data[$o] = $x;
								}
							}

							if (isSet($product_data['skip_product']) && $product_data['skip_product']) {
								$i++;
								continue;
							}

							$data = prepare_data('product', $product_data, false);
							$skip_files = false;

							if (!empty($data['name'])) {
								$data['name'] = preg_replace('/\s{2,}/', ' ', $data['name']);
							}

							if (isSet($product_data['stock']) && $product_data['stock'] !== null) {
								$data['stock'] = (int) $product_data['stock'];
							}

							if ($xml_import['unique'] && ($db_product = Core::$db->product()->where($xml_import['unique'], $data[$xml_import['unique']])->limit(1)->fetch())) {
								// update
								$product_id = $db_product['id'];

								$_ids[] = $product_id;

								if (isSet($product_data['delete']) && $product_data['delete']) {
									$this->deleteProduct($db_product);
									$i++;
									continue;
								}

								if (isSet($xml_import['insert_only']) && $xml_import['insert_only']) {
									$i++;
									continue;
								}

								if ((int) $db_product['lock_data']) {
									foreach ($data as $n => $v) {
										if (!in_array($n, array('price', 'stock', 'availability_id', 'status'))) {
											unset($data[$n]);
										}
									}
								}

								if ((int) $db_product['lock_price']) {
									unset($data['price']);
								}

								if ((int) $db_product['lock_stock']) {
									unset($data['stock'], $data['availability_id']);
								}

								if (!empty($data) && $db_product->needUpdate($data)) {
									$sql_queue .= "\n" . $db_product->update($data, true) . ";";

									$this->log("Update product " . $data[$xml_import['unique']] . " (ID " . $product_id . ")\n");

									$_update++;
								}

								if (!isSet($xml_import['files_force_update']) || $xml_import['files_force_update'] === false) {
									$skip_files = true;
								}

								if (is_object($db_product)) {
									$db_product->releaseMem();
								}
							} else if (!isSet($xml_import['update_only']) || !$xml_import['update_only']) {
								// insert
								if (!$product_data['name']) {
									$i++;
									continue;
								}

								if (isSet($product_data['delete']) && $product_data['delete']) {
									$i++;
									continue;
								}

								if (isSet($_unique[$data[$xml_import['unique']]])) {
									$product_id = $_unique[$data[$xml_import['unique']]];
								} else {
									if (empty($data['sef_url'])) {
										$data['sef_url'] = dirify((string) $data['name']);
									}

									if (!isSet($data['status'])) {
										$data['status'] = 1;
									}

									if (!isSet($data['vat_id'])) {
										$data['vat_id'] = (int) Core::config('vat_rate_default');
									}

									$sql_queue .= "\n" . Core::$db->product($data, true) . ";";

									$product_id = $next_ai++;

									try {
										Search::indexProduct(array_merge(array(
													'id' => $product_id,
													'name' => '',
													'description_short' => '',
													'description' => ''
														), $data));
									} catch (PDOException $e) {

									}

									$_ids[] = $product_id;
									$_unique[$data[$xml_import['unique']]] = $product_id;

									$this->log("Insert product " . $data[$xml_import['unique']] . " (ID " . $product_id . ")\n");

									$_insert++;
								}
							} else if ($xml_import['unique'] == 'sku' || $xml_import['unique'] == 'ean13') {
								if ($attribute = Core::$db->product_attributes()->where($xml_import['unique'], $data[$xml_import['unique']])->limit(1)->fetch()) {
									$data = prepare_data('product_attributes', $product_data, false);

									if (isSet($product_data['stock']) && $product_data['stock'] !== null) {
										$data['stock'] = (int) $product_data['stock'];
									}

									if (isSet($data['price']) && $data['price']) {
										$product_price = $attribute->product['price'];

										if (!$product_price) {
											$product_price = $data['price'];
											$data['price'] = 0;
											$attribute->product->update(array('price' => $product_price));
										} else {
											$data['price'] = round($data['price'] - $product_price, 2);
										}

										if ($data['price'] === 0) {
											$data['price'] = null;
										}
									}

									if ($data && $attribute->needUpdate($data)) {
										$attribute->update($data);

										$this->log("Update attribute " . $attribute[$xml_import['unique']] . " (ID " . $attribute['id'] . ")\n");
									}

									$attribute->releaseMem();

									$i++;

									continue;
								}
							}

							if (defined('IS_CLI')) {
								echoProgress('Progress: update ' . $_update . ' items, insert ' . $_insert . ' items; total time: ' . fhours(time() - $cli_time));
							}


							if (!empty($product_data['attributes'])) {
								foreach ($product_data['attributes'] as $attr) {
									if ($attr['name'] && $attr['value']) {

										if ($attr['price'] && $data['price']) {
											$attr['price'] = round($attr['price'] - $data['price'], 2);
										}

										$attr_data = prepare_data('product_attributes', $attr, false);

										if ($attribute = Core::$db->product_attributes()->where('product_id', $product_id)->where('name', $attr['name'])->where('value', $attr['value'])->limit(1)->fetch()) {
											// update
											if ($attribute->needUpdate($attr_data)) {
												$attribute->update($attr_data);
											}

											if (is_object($attribute)) {
												$attribute->releaseMem();
											}
										} else {
											// insert
											$attr_data['product_id'] = $product_id;
											Core::$db->product_attributes($attr_data);
										}
									}
								}
							}

							if (!empty($product_data['categories'])) {
								foreach ($product_data['categories'] as $category) {
									if (is_numeric($category)) {
										if (!($pp = Core::$db->product_pages()->where('product_id', $product_id)->where('page_id', $category)->limit(1)->fetch())) {
											Core::$db->product_pages(array(
												'product_id' => $product_id,
												'page_id' => $category
											));
										} else {
											if (is_object($pp)) {
												$pp->releaseMem();
											}
										}
									} else if (is_array($category)) {
										$parent_id = isSet($xml_import['category_parent']) ? (int) $xml_import['category_parent'] : 0;

										foreach ($category as $category_name) {
											$category_data = array();

											if (is_array($category_name)) {
												$category_data = $category_name[1];
												$category_name = $category_name[0];
											}

											$category_name = (string) $category_name;

											if ($xml_import['create_categories'] && !($cat = Core::$db->page()->where('parent_page', $parent_id)->where('name', $category_name)->limit(1)->fetch())) {
												$parent_id = Core::$db->page(array(
													'parent_page' => $parent_id,
													'name' => $category_name,
													'sef_url' => dirify($category_name),
													'status' => (isSet($category_data['status']) ? $category_data['status'] : 1),
													'menu' => $parent_id ? 0 : (isSet($category_data['menu']) ? $category_data['menu'] : 2),
													'products' => 1,
													'rss' => 0,
													'position' => (isSet($category_data['position']) ? $category_data['position'] : 0),
													'product_columns' => (isSet($category_data['product_columns']) ? $category_data['product_columns'] : 2),
													'authorization' => 0
														));

												$this->log("Create category " . $category_name . " (ID " . $parent_id . ")\n");
											} else if ($cat) {
												$parent_id = $cat['id'];
												if (is_object($cat)) {
													$cat->releaseMem();
												}
											}
										}

										if ($parent_id && !Core::$db->product_pages()->where('product_id', $product_id)->where('page_id', $parent_id)->limit(1)->fetch()) {
											Core::$db->product_pages(array(
												'product_id' => $product_id,
												'page_id' => $parent_id
											));
										}
									}
								}
							}


							if (!$skip_files && !empty($product_data['files'])) {
								foreach ($product_data['files'] as $file) {
									$is_http = false;
									$_target = null;
									$_file_type = null;

									if (is_array($file)) {
										$_target = $file[1];

										if (!empty($file[2])) {
											$_file_type = $file[2];
										}

										$file = $file[0];
									}

									if (!preg_match('/^https?\:\/\//', $file)) {
										$filepath = rtrim($xml_import['files_base'], '/') . '/' . ltrim($file, '/');
									} else {
										$filepath = $file;
										$is_http = true;
									}

									if ($is_http || file_exists($filepath)) {
										$fileinfo = pathinfo($filepath);

										if ($_target) {
											$target = 'files/' . ($xml_import['files_target_dir'] ? trim($xml_import['files_target_dir'], '/') . '/' : '') . ltrim($_target, '/');
										} else {
											$target = 'files/' . ($xml_import['files_target_dir'] ? trim($xml_import['files_target_dir'], '/') . '/' : '') . $fileinfo['basename'];
										}

										$targerinfo = pathinfo($target);

										if (!is_dir($targerinfo['dirname'])) {
											FS::mkdir($targerinfo['dirname'], true);
										}

										$is_image = 0;

										if ($_f = Core::$db->product_files()->where('product_id', $product_id)->where('filename', $target)->fetch()) {
											$_f->releaseMem();
											continue; // already assigned
										}

										$imgfile = @file_get_contents($filepath);

										if ($imgfile && @file_put_contents(DOCROOT . $target, $imgfile)) {
											if ($_file_type == 'image' || in_array(strtolower($fileinfo['extension']), array('gif', 'jpg', 'jpeg', 'png'))) {
												try {
													Image::thumbs(DOCROOT . $target);
												} catch (Exception $e) {
													$this->log("Error: " . $e->getMessage() . " (" . $filepath . ")\n");
												}

												$is_image = 1;
											}

											Core::$db->product_files(array(
												'product_id' => $product_id,
												'filename' => $target,
												'is_image' => $is_image,
												'position' => 0,
												'description' => '',
												'size' => $is_image ? -1 : null, // auto
												'align' => $is_image ? 2 : null // left
													));

											$this->log("Add file " . $target . " to product ID " . $product_id . "\n");
										} else {
											$this->log("Warning: copy file " . $target . " failed\n");
										}
									} else {
										$this->log("Warning: file " . $filepath . " not found\n");
									}
								}
							}

							$i++;
						}

						print_r($xml_import['data']);

						if ($sql_queue) {
							Core::$db_conn->exec($sql_queue);
							$sql_queue = '';
						}
					} else {
						throw new Exception('XML file "' . $source . '" does not exist');
					}
				}

				if (!empty($_ids)) {
					if ($delete = (isSet($xml_import['delete_products']) ? (int) $xml_import['delete_products'] : 0)) {

						if ($delete == 1) {
							// status
							foreach (Core::$db->product()->where('id NOT IN (' . implode(',', $_ids) . ')') as $p) {
								if ($p['status'] == 1) {
									$p->update(array('status' => 0));
								}
							}
						} else if ($delete == 2) {
							// delete
							Core::$db->product()->where('id NOT IN (' . implode(',', $_ids) . ')')->delete();
							Core::$db->product_pages()->where('product_id NOT IN (' . implode(',', $_ids) . ')')->delete();
							Core::$db->product_attributes()->where('product_id NOT IN (' . implode(',', $_ids) . ')')->delete();
							Core::$db->product_features()->where('product_id NOT IN (' . implode(',', $_ids) . ')')->delete();
							Core::$db->product_files()->where('product_id NOT IN (' . implode(',', $_ids) . ')')->delete();
						}
					}
				}

				$this->log("Import complete\n");

				$this->writeLog($this->log);

				if (!$this->is_cron) {
					$this->content = file_get_contents(DOCROOT . 'etc/log/import_' . substr($this->progress['schema'], 0, -4) . '_' . date('Y-m-d', $this->progress['time']) . '.txt');
				} else if (!defined('IS_CLI')) {
					echo $this->log;
				}
			} catch (Exception $e) {
				$this->content = 'ERROR: ' . $e->getMessage();
			}

			if (!$this->is_cron) {
				$this->deleteProgress($schema);
			}

			if ($this->is_ajax) {
				echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) $percent, 'log' => trim(htmlspecialchars($str)), 'finished' => 1)) . '\');</script>';
				flush();
				exit;
			}
		} else {
			redirect('admin/xmlimport');
		}
	}

	private function deleteProduct($product)
	{
		if (is_object($product)) {
			$product->product_pages()->delete();
			$product->product_attributes()->delete();
			$product->product_features()->delete();
			$product->product_files()->delete();
			$product->delete();
		}
	}

	private function getProgress($schema = null, & $_ids = array())
	{
		$progress = @file_get_contents(DOCROOT . 'etc/tmp/import_progress_' . md5($schema) . '.txt');

		$_ids = @unserialize(@file_get_contents(DOCROOT . 'etc/tmp/import_ids_' . md5($schema) . '.txt'));

		if ($progress) {
			$progress = unserialize($progress);

			if (is_array($progress) && $progress['time'] >= strtotime('-3 hours') && (!$schema || $schema == $progress['schema'])) {
				$this->progress = $progress;
				return;
			}
		}

		$this->progress = array(
			'time' => time(),
			'schema' => $schema,
			'product_offset' => 0
		);
	}

	private function saveProgress($schema, $data, $_ids)
	{
		$this->progress = array_merge($this->progress, $data);

		file_put_contents(DOCROOT . 'etc/tmp/import_progress_' . md5($schema) . '.txt', serialize($this->progress));
		file_put_contents(DOCROOT . 'etc/tmp/import_ids_' . md5($schema) . '.txt', serialize($_ids));
	}

	private function deleteProgress($schema)
	{
		@unlink(DOCROOT . 'etc/tmp/import_progress_' . md5($schema) . '.txt');
		@unlink(DOCROOT . 'etc/tmp/import_ids_' . md5($schema) . '.txt');
	}

	private function log($str)
	{
		$this->log .= $str;

		if ($this->is_ajax) {
			$percent = @round(100 / $this->ajax_progress['total'] * ($this->ajax_progress['offset'] + 1));

			echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) $percent, 'log' => trim(htmlspecialchars($str)))) . '\');</script>';
			flush();
		}
	}

	private function ajaxProgress()
	{
		if ($this->is_ajax) {
			$percent = round(100 / $this->ajax_progress['total'] * ($this->ajax_progress['offset'] + 1));

			echo '<script>parent._progress(\'' . json_encode(array('progress' => (int) $percent, 'log' => '')) . '\');</script>';
			flush();
		}
	}

	private function writeLog($str)
	{
		if (!is_dir(DOCROOT . 'etc/log')) {
			FS::mkdir(DOCROOT . 'etc/log', true);
		}

		@file_put_contents(DOCROOT . 'etc/log/import_' . substr($this->progress['schema'], 0, -4) . '_' . date('Y-m-d', $this->progress['time']) . '.txt', $str . "\n\n", FILE_APPEND);
	}

}

class XMLParser extends XMLReader
{

	protected $file;

	public function __construct($file)
	{
		$this->file = $file;

		$this->open($this->file);
	}

	public function getElement($name)
	{
		while ($this->read()) {
			if ($this->nodeType == XMLReader::ELEMENT && $this->name == $name) {
				return new SimpleXMLElement($this->readOuterXML(), LIBXML_NOCDATA);
			}
		}

		return false;
	}

	public function countElement($name)
	{
		$count = 0;
		while ($this->read()) {
			if ($this->nodeType == XMLReader::ELEMENT && $this->name == $name) {
				$count++;
			}
		}

		return $count;
	}

}