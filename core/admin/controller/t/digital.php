<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Digital extends Controller
{

	public function digital()
	{
		$this->tpl = null;

		@set_time_limit(0);

		$type = 'xml';
		$category = 0;

		if (!empty($_POST['type'])) {
			$type = trim($_POST['type']);
		}

		if (!empty($_POST['category'])) {
			$category = (int) trim($_POST['category']);
		}

		if ($type) {
			$export = new Export($type);

			$columns = isset($_POST['cols']) ? (array) $_POST['cols'] : null;

			if ($columns) {
				setcookie('dataexport_cols', join(';', $columns), strtotime('+14 days'));
			}

			if ($columns) {
				$export->addHeader(array_values($columns));
			}

			$offset = 0;

			$products = Core::$db->product();

			if ($category > 0) {
				if ($page = Core::$db->page[$category]) {
					$this->getSubpages($page['id'], $ids);

					$products->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
				}
			}

			$where = $products->getWhere();

			while (($_products = $products->order('id ASC')->limit(500, $offset)) && count($_products)) {
				foreach ($_products as $product) {
					$export->addProduct($product, $columns);
				}

				$offset += 500;

				$products = Core::$db->product();

				if ($where) {
					$products->where($where);
				}
			}

			$name = 'export_' . date('Y-m-d_H:i');

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers

			if ($type == 'xml') {
				header("Content-Type: application/xml; charset=UTF-8");
			} else if ($type == 'xlsx') {
				header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
			} else if ($type == 'csv') {
				header("Content-Type: text/x-csv; charset=Windows-1250");
			} else if ($type == 'ods') {
				header("Content-Type: application/vnd.oasis.opendocument.spreadsheet; charset=UTF-8");
			}

			if ($type == 'xlsx') {
				$type = 'xls';
			}

			header("Content-Disposition: attachment; filename=\"" . dirify($name) . "." . $type . "\";");
			header("Content-Transfer-Encoding: binary");

			echo $export->save();
		} else {
			redirect('admin/dataexport');
		}
	}

	

}
