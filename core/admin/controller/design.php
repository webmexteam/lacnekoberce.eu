<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Design extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('design');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		$templates = $this->getTemplates();

		$options = Registry::get('template_options');

		if (!$options) {
			$options = array();
		}

		if (!$options['default']) {
			$options['default'] = array(
				'show_text_logo' => 1,
				'show_font_size' => 1
			);
		}

		if (!empty($_POST)) {
			if (!empty($_POST['template'])) {
				Core::$db->config()->where('name', 'template')->update(array('value' => $_POST['template']));
			}
			if (!empty($_POST['style'])) {
				Core::$db->config()->where('name', 'template_style')->update(array('value' => $_POST['style']));
			}
			if (!empty($_POST['theme'])) {
				Core::$db->config()->where('name', 'template_theme')->update(array('value' => $_POST['theme']));
			}

			if (!empty($_POST['opt'])) {

				$options = $_POST['opt'];

				Registry::set('template_options', $options);
			}

			flashMsg(__('msg_saved'));

			redirect('admin/design');
		}

		$this->content = tpl('design/list.php', array(
			'templates' => $templates,
			'options' => $options
				));
	}

	public function delete($template, $style, $theme)
	{
		$this->tpl = null;
		$f = DOCROOT . 'etc/theme/' . $template . '_' . $style . '_' . $theme . '.less.css';

		if (file_exists($f)) {
			FS::remove($f);
			flashMsg(__('msg_saved'));
		}

		redirect('admin/design');
	}

	public function editor($template, $style, $theme)
	{
		if ($theme == 'fancybox') {
			return; // Stupind IE
		}

		$_SESSION['webmex_design_template'] = $template;
		$_SESSION['webmex_design_style'] = $style;
		$_SESSION['webmex_design_theme'] = $theme;

		$custom_theme = false;
		$tf = APPROOT . 'template/' . $template . '/css/theme/' . $style . '_' . $theme . '.less.css';

		if (!file_exists($tf)) {
			$tf = DOCROOT . 'etc/theme/' . $template . '_' . $style . '_' . $theme . '.less.css';
			$custom_theme = true;
		}

		if (!empty($_POST['css'])) {
			$tmp = file_get_contents($tf);

			$base = rtrim(preg_replace('/index\.php/', '', Core::$url), '/') . '/';

			foreach ($_POST['css'] as $k => $v) {
				if (preg_match('/image$/', $k)) {
					if (!$v) {
						$v = 'none';
					} else {
						$v = 'url("' . (preg_match('/^https?\:/', $v) ? '' : '__base__') . $v . '")';
					}
				}

				$tmp = preg_replace('/^\@' . $k . ':\s+([^;]*)/miu', '@' . $k . ': ' . $v, $tmp);
			}

			if ($custom_theme) {
				file_put_contents($tf, $tmp);
			} else {
				file_put_contents(DOCROOT . 'etc/theme/' . $template . '_' . $style . '_' . $_POST['theme_name'] . '.less.css', $tmp);
				redirect('admin/design/editor/' . $template . '/' . $style . '/' . $_POST['theme_name']);
			}
		}

		$theme_content = file($tf);
		$variables = array();

		foreach ($theme_content as $line) {
			$line = trim($line);
			$name = $value = null;

			if (preg_match('/^\@([a-z0-9\-\_]+)\(([^\)]*)\)/', $line, $m)) {
				// function
				$name = $m[1];

				preg_match_all('/\@([a-z0-9\-\_]+):\s+?([^\;\@]*)/', $m[2], $_m);

				$value = array();
				foreach ($_m[1] as $i => $n) {
					$value[$n] = $_m[2][$i];
				}
			} else if (preg_match('/^\@([a-z0-9\-\_]+):\s+?(.*)$/i', $line, $m)) {
				// variable
				$name = $m[1];
				$value = trim($m[2], ' ;');

				if (preg_match('/image$/', $name)) {
					$value = preg_replace('/^__base__/', '', trim(substr($value, 4, -1), '"')); // strip url(..)
				}
			}

			if ($name) {
				$parts = explode('-', $name);

				if (count($parts) == 3) {
					$variables[$parts[0]][$parts[1]][$parts[2]] = $value;
				} else if (count($parts) == 2) {
					$variables[$parts[0]][$parts[1]] = $value;
				} else {
					$variables['global'][$name] = $value;
				}
			}
		}

		$_SESSION['design_mode']['css'] = array();

		$this->content = tpl('design/editor.php', array(
			'variables' => $variables,
			'custom_theme' => $custom_theme
				));
	}

	private function getTemplates()
	{
		$templates = array();

		if ($tpls = Core::glob('template/*', true)) {
			foreach ($tpls as $tpl) {
				$tpl_name = substr($tpl, strrpos($tpl, '/') + 1);

				if ($tpl_name == 'system' || !file_exists($tpl . '/template_info.php')) {
					continue;
				}

				include($tpl . '/template_info.php');

				$styles = array();

				if ($f = Core::glob('template/' . $tpl_name . '/css/style/*.less.css')) {
					foreach ($f as $style) {
						$n = substr($style, strrpos($style, '/') + 1, -9);

						$styles[$n] = array(
							'name' => $n,
							'themes' => array()
						);
					}
				}

				if ($f = Core::glob('template/' . $tpl_name . '/css/theme/*.less.css')) {
					foreach ($f as $theme) {
						$n = substr($theme, strrpos($theme, '/') + 1, -9);
						$parts = explode('_', $n);

						if (isSet($styles[$parts[0]]['themes'])) {
							$styles[$parts[0]]['themes'][] = $parts[1];
						}
					}
				}

				if ($f = glob(DOCROOT . 'etc/theme/' . $tpl_name . '_*.less.css')) {
					foreach ($f as $theme) {
						$n = substr($theme, strrpos($theme, '/') + 1, -9);
						$parts = explode('_', $n);

						if (isSet($styles[$parts[1]]['themes'])) {
							$styles[$parts[1]]['themes']['c_' . $parts[2]] = $parts[2];
						}
					}
				}

				$templates[$tpl_name] = array('info' => $template, 'styles' => $styles);
			}
		}

		return $templates;
	}

	private function getTemplates_old()
	{
		$templates = array();

		foreach ((array) Core::glob('core/template', true) as $tpl) {
			$tpl_name = substr($tpl, strrpos($tpl, '/') + 1);

			if ($tpl_name == 'system' || !file_exists($tpl . '/template_info.php')) {
				continue;
			}

			include($tpl . '/template_info.php');

			$styles = array();

			if ($f = glob($tpl . '/css/style/*.less.css')) {
				foreach ($f as $style) {
					$n = substr($style, strrpos($style, '/') + 1, -9);

					$styles[$n] = array(
						'name' => $n,
						'themes' => array()
					);
				}
			}

			if ($f = glob($tpl . '/css/theme/*.less.css')) {
				foreach ($f as $theme) {
					$n = substr($theme, strrpos($theme, '/') + 1, -9);
					$parts = explode('_', $n);

					if (isSet($styles[$parts[0]]['themes'])) {
						$styles[$parts[0]]['themes'][] = $parts[1];
					}
				}
			}

			if ($f = glob(DOCROOT . 'etc/theme/' . $tpl_name . '_*.less.css')) {
				foreach ($f as $theme) {
					$n = substr($theme, strrpos($theme, '/') + 1, -9);
					$parts = explode('_', $n);

					if (isSet($styles[$parts[1]]['themes'])) {
						$styles[$parts[1]]['themes']['c_' . $parts[2]] = $parts[2];
					}
				}
			}

			$templates[$tpl_name] = array('info' => $template, 'styles' => $styles);
		}

		return $templates;
	}

}