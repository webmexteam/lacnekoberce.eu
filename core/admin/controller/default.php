<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Default extends AdminController
{

	public function index()
	{
		unset($_SESSION['result']);

		$orders = Core::$db->order()->order('received DESC')->limit(10);

		$changes = array();
		foreach (Core::$db->product()->where('last_change IS NOT NULL')->order('last_change DESC')->limit(10) as $product) {
			$changes[] = array(
				'id' => $product['id'],
				'name' => $product['name'],
				'date' => $product['last_change'],
				'user' => Core::$db->user[$product['last_change_user']]['username'],
				'object' => 'product'
			);
		}

		foreach (Core::$db->page()->where('last_change IS NOT NULL')->order('last_change DESC')->limit(10) as $page) {
			$changes[] = array(
				'id' => $page['id'],
				'name' => $page['name'],
				'date' => $page['last_change'],
				'user' => Core::$db->user[$page['last_change_user']]['username'],
				'object' => 'page'
			);
		}

		usort($changes, array($this, 'sortChanges'));

		$stats = array();
		$date = strtotime(date('Y-m-d'));

		for ($i = 6; $i >= 0; $i--) {
			$d = $date - ($i * 86400);

			$dtotal = Core::$db->order()->select('SUM(total_incl_vat) as total')->where('status != 4')->where('received >= ' . $d . ' AND received <= ' . ($d + 86399))->fetch();

			$stats[$i] = (float) $dtotal['total'];
		}

		$first_login = User::get('id') == 1 && (isSet($_SESSION['first_login']) || Core::config('first_login'));

		if ($first_login) {
			$_SESSION['first_login'] = true;

			Core::$db->config()->where('name', 'first_login')->update(array('value' => 0));
		}

		if (mt_rand(1, 10) == 1) {
			$update = Update::check();
		}

		$total_orders = Core::$db->order()->select('COUNT(id) as total')->fetch();
		$total_customers = Core::$db->customer()->select('COUNT(id) as total')->fetch();

		$month_revenue = Core::$db->order()->where('status != 4')->select('SUM(total_incl_vat) as total')->where('received >= ' . strtotime(date('Y-m-01')))->fetch();

		$this->content = tpl('dashboard.php', array(
			'orders' => $orders,
			'total_orders' => (float) $total_orders['total'],
			'total_customers' => (float) $total_customers['total'],
			'month_revenue' => (float) $month_revenue['total'],
			'changes' => array_slice($changes, 0, 5),
			'stats' => $stats,
			'daystotal' => array_sum($stats),
			'first_login' => $first_login
				));
	}

	private function sortChanges($a, $b)
	{
		return $a['date'] < $b['date'] ? 1 : -1;
	}

	public function login()
	{
		$this->tpl = 'login.php';

		if (User::$logged_in) {
			redirect('admin/default/index');
		}

		if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])) {
			if (($result = User::login($_POST['username'], $_POST['password'])) === true) {
				$r = !empty($_SESSION['login_redirect_to']) ? $_SESSION['login_redirect_to'] : 'admin/default/index';

				unset($_SESSION['login_redirect_to'], $_SESSION['first_login']);

				if (Core::$is_premium) {
					$last_check = (int) Core::config('last_lcheck');

					if ($last_check < strtotime('-14 days')) {
						if (Core::$unlimited_license) {

							if (Core::unlimited(true) === false) {
								Core::$db->config()->where('name', 'activated')->update(array(
									'value' => 0
								));
							}
						} else if (!Core::validatelkey() || !Core::checklkey()) {
							Core::$db->config()->where('name', 'activated')->update(array(
								'value' => 0
							));
						}
					}
				}

				redirect($r);
			} else {
				sleep(3);

				$_SESSION['flash_login_error'] = $result;
			}
		}
	}

	public function logout()
	{
		User::logout();

		redirect('admin/default/login');
	}

	public function lost_password($action = null)
	{
		$this->tpl = null;

		if ($action == 'reset') {

			if (!empty($_GET['u']) && !empty($_GET['t']) && !empty($_GET['h'])) {

				$user = Core::$db->user[(int) $_GET['u']];
				$t = (int) $_GET['t'];

				if ($user && $t >= strtotime('-4 hours') && $_GET['h'] == substr($user['password'], 2, 5) . sha1($user['email'] . $user['last_login'] . $t)) {
					$new_pass = strtoupper(random());

					$user->update(array(
						'password' => sha1(sha1($new_pass))
					));

					$_data = $user->as_array();
					$_data['password'] = $new_pass;

					sendmail($user['email'], array(__('new_user_password'), __('new_user_password_text')), $_data);

					echo __('new_password_sent');
				} else {
					redirect('admin/default/login');
				}
			} else {
				redirect('admin/default/login');
			}
		} else {
			sleep(3);

			$ret = array('success' => false, 'msg' => '');

			$email = $_POST['email'];

			if ($email) {
				$user = Core::$db->user('email', $email)->fetch();

				if ($user) {
					$ret['success'] = true;
					$ret['msg'] = (string) __('reset_mail_sent');

					$t = time();
					$h = substr($user['password'], 2, 5) . sha1($user['email'] . $user['last_login'] . $t);
					$l = url('admin/default/lost_password/reset', array('u' => $user['id'], 't' => $t, 'h' => $h), true);

					$_data = $user->as_array();
					$_data['link'] = $l;

					sendmail($user['email'], array(__('password_reset'), __('reset_mail')), $_data);
				} else {
					$ret['msg'] = (string) __('user_not_found');
				}
			} else {
				$ret['msg'] = (string) __('invalid_email');
			}

			echo json_encode($ret);
		}
	}

	public function quicklaunch()
	{
		$this->tpl = null;

		if (!empty($_POST['order'])) {
			$_POST['order'] = trim($_POST['order']);

			if (is_numeric($_POST['order'])) {
				$order = Core::$db->order[(int) $_POST['order']];
			} else if (valid_email($_POST['order'])) {
				$order = Core::$db->order()->where('email', $_POST['order'])->order('id DESC')->limit(1)->fetch();
			} else if (preg_match('/\s/', $_POST['order'])) {
				$parts = explode(' ', $_POST['order']);
				$order = Core::$db->order()->where('first_name LIKE "' . $parts[0] . '%" AND last_name LIKE "' . $parts[1] . '%"')->order('id DESC')->limit(1)->fetch();
			} else {
				$order = Core::$db->order()->where('last_name LIKE "%' . $_POST['order'] . '%"')->order('id DESC')->limit(1)->fetch();
			}

			if ($order) {
				redirect('admin/orders/edit/' . $order['id']);
			} else {
				flashMsg(__('msg_record_not_found', $_POST['order']), 'error');
			}
		} else if (!empty($_POST['product'])) {
			$_POST['product'] = trim($_POST['product']);

			if (is_numeric($_POST['product']) && strlen($_POST['product']) <= 5) {
				$product = Core::$db->product[(int) $_POST['product']];
			} else if (is_numeric($_POST['product']) && strlen($_POST['product']) >= 10) {
				$product = Core::$db->product()->where('ean13', $_POST['product'])->order('id DESC')->limit(1)->fetch();
			} else {
				$product = Core::$db->product()->where('name LIKE "%' . $_POST['product'] . '%" OR sku LIKE "%' . $_POST['product'] . '%"')->order('id DESC')->limit(1)->fetch();
			}

			if ($product) {
				redirect('admin/products/edit/' . $product['id']);
			} else {
				flashMsg(__('msg_record_not_found', $_POST['product']), 'error');
			}
		} else if (!empty($_POST['page'])) {
			$_POST['page'] = trim($_POST['page']);

			if (is_numeric($_POST['page'])) {
				$page = Core::$db->page[(int) $_POST['page']];
			} else {
				$page = Core::$db->page()->where('name LIKE "%' . $_POST['page'] . '%"')->order('id DESC')->limit(1)->fetch();
			}

			if ($page) {
				redirect('admin/pages/edit/' . $page['id']);
			} else {
				flashMsg(__('msg_record_not_found', $_POST['page']), 'error');
			}
		} else if (!empty($_POST['customer'])) {
			$_POST['customer'] = trim($_POST['customer']);

			if (is_numeric($_POST['customer'])) {
				$customer = Core::$db->customer[(int) $_POST['customer']];
			} else if (valid_email($_POST['customer'])) {
				$customer = Core::$db->customer()->where('email', $_POST['customer'])->order('id DESC')->limit(1)->fetch();
			} else if (preg_match('/\s/', $_POST['customer'])) {
				$parts = explode(' ', $_POST['customer']);
				$customer = Core::$db->customer()->where('first_name LIKE "' . $parts[0] . '%" AND last_name LIKE "' . $parts[1] . '%"')->order('id DESC')->limit(1)->fetch();
			} else {
				$customer = Core::$db->customer()->where('last_name LIKE "%' . $_POST['customer'] . '%"')->order('id DESC')->limit(1)->fetch();
			}

			if ($customer) {
				redirect('admin/customers/edit/' . $customer['id']);
			} else {
				flashMsg(__('msg_record_not_found', $_POST['customer']), 'error');
			}
		} else {
			flashMsg(__('msg_error'), 'error');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function emptysrc()
	{
		$this->tpl = null;
		exit;
	}

	public function ajax_checkSession()
	{
		$this->tpl = null;
		$status = 1;
		$t = strtotime('-30 minutes');
		$t_span = 600; // seconds

		if (!empty($_SESSION['webmex_user']) && is_array($_SESSION['webmex_user'])) {
			if ($_SESSION['webmex_user']['t'] < $t) {
				$status = 2;
			}
		}

		echo json_encode(array('status' => $status, 'time_left' => (((int) $_SESSION['webmex_user']['t']) + $t_span) - $t));
	}

	public function ajax_extendSession()
	{
		$this->tpl = null;
	}

	public function ajax_removeNotification($uid)
	{
		$this->tpl = null;

		Notify::remove($uid);

		echo json_encode(array('status' => true, 'count' => Notify::count()));
	}

}