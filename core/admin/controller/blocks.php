<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Blocks extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('blocks');

		Core::$active_tab = 'blocks';
	}

	public function index()
	{
		if (isSet($_POST['save']) && !empty($_POST['position'])) {
			foreach ($_POST['position'] as $id => $position) {
				$block = Core::$db->block[(int) $id];

				if ($block) {
					$block->update(array('position' => (int) $position));
				}
			}

			flashMsg(__('msg_saved'));
		}


		$this->content = tpl('blocks/list.php', array(
				));
	}

	public function edit($id)
	{
		$block = Core::$db->block[(int) $id];

		if ($id > 0 && !$block) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/blocks');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('title'))) === true) {
				$data = $_POST;

				$data['config'] = json_encode($data['block_config']);

				if (!$block) {
					$block_id = Core::$db->block(prepare_data('block', $data));

					if ($block_id) {
						$block = Core::$db->block[$block_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $block->update(prepare_data('block', $data));
				}

				if ($block && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/blocks');
				} else {
					redirect('admin/blocks/edit/' . $block['id']);
				}
			}
		} else if ($id == 0) {
			$block['id'] = 0;
			$block['title'] = __('new_block');
			$block['position'] = 0;
		}

		$drivers = array(0 => __('none'));
		foreach (Block::$drivers as $driver) {
			$drivers[$driver] = __($driver);
		}

		$this->content = tpl('blocks/edit.php', array(
			'block' => $block,
			'drivers' => $drivers,
			'pages' => $this->_pageTree()
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$block = Core::$db->block[(int) $id];

		if ($block) {
			$block->delete();
		}

		redirect('admin/blocks');
	}

	public function ajax_getConfig($driver, $block_id = null)
	{
		$this->tpl = null;

		$driver_name = 'Block_' . ucfirst($driver);

		if ($block_id) {
			$block = Core::$db->block[(int) $block_id];
		} else {
			$block = null;
		}

		$inst = new $driver_name($block);

		echo $inst->getConfigForm();
	}

	private function _pageTree($parent_id = 0, $level = 0)
	{
		$out = array(0 => __('all'));

		$pages = Core::$db->page()->where('parent_page', $parent_id)->order('position ASC');

		if ($parent_id == 0) {
			$pages->where('menu != 2')->where('menu != 3'); // not categories
		}

		foreach ($pages as $page) {
			$out[$page['id']] = str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'];

			$out += $this->_pageTree($page['id'], $level + 1);
		}

		return $out;
	}

}