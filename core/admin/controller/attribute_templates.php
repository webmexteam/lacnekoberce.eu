<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Attribute_templates extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Core::$active_tab = 'products';
	}

	public function index()
	{
		$order_by = 'name';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		$templates = Core::$db->product_attribute_template()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		$this->content = tpl('attribute_templates/list.php', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'templates' => $templates
				));
	}

	public function edit($id)
	{
		$template = Core::$db->product_attribute_template[(int) $id];

		if ($id > 0 && !$template) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/attribute_templates');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

				if (!$template) {
					$template_id = Core::$db->product_attribute_template(prepare_data('product_attribute_template', $data));

					if ($template_id) {
						$template = Core::$db->product_attribute_template[$template_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $template->update(prepare_data('product_attribute_template', $data));
				}

				if ($template && $saved !== false) {

					foreach ($_POST['attr'] as $aid => $adata) {
						if ($aid{0} != '0' && ($attr = Core::$db->product_attribute_template_items[$aid])) {
							// update
							if (!trim($adata['name'])) {
								$attr->delete();
							} else {
								$attr->update(array(
									'name' => $adata['name'],
									'value' => $adata['value'] ? $adata['value'] : null,
									'price' => $adata['price'] ? $adata['price'] : null,
								));
							}
						} else if ($aid{0} == '0' && trim($adata['name'])) {
							// insert
							Core::$db->product_attribute_template_items(array(
								'product_attribute_template_id' => $template['id'],
								'name' => $adata['name'],
								'value' => $adata['value'] ? $adata['value'] : null,
								'price' => $adata['price'] ? $adata['price'] : null,
							));
						}
					}

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/attribute_templates');
				} else {
					redirect('admin/attribute_templates/edit/' . $template['id']);
				}
			}
		} else if ($id == 0) {
			$template['id'] = 0;
			$template['name'] = __('new_attribute_template');
		}

		$this->content = tpl('attribute_templates/edit.php', array(
			'template' => $template
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$template = Core::$db->product_attribute_template[(int) $id];

		if ($template) {
			$template->product_attribute_template_items()->delete();
			$template->delete();
		}

		redirect('admin/attribute_templates');
	}

}