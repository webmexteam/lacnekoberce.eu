<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Translations extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('settings');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		$languages = array();

		foreach (Core::glob('i18n/*') as $lang) {
			$l = substr($lang, strrpos($lang, '/') + 1);

			$languages[$l] = __('language_' . $l);
		}

		$this->content = tpl('translations/list.php', array(
			'languages' => $languages,
				));
	}

	public function edit($language)
	{
		if (!empty($language)) {

			$lang = Core::$lang;
			// load translation
			Core::$language = $language;
			Core::loadI18N(false);
			$translations = Core::$lang;
			// set language back
			Core::$lang = $lang;

			ksort($translations);

			foreach ($translations as $k => $v) {
				if (is_array($v)) {
					ksort($v);

					foreach ($v as $_k => $_v) {
						$translations[$k . '.' . $_k] = $_v;
					}
					unset($translations[$k]);
				}
			}

			$custom = Registry::get('i18n_' . $language);

			if (!$custom) {
				$custom = array();
			}

			foreach ($custom as $k => $v) {
				if (trim($translations[$k]) == trim($v)) {
					unset($custom[$k]);
				}
			}

			if (!empty($_POST['i18n']) && is_array($_POST['i18n'])) {
				$data = array();

				foreach ($_POST['i18n'] as $k => $v) {
					if (trim((string) $translations[$k]) != trim($v)) {
						$data[$k] = $v;
					}
				}

				Registry::set('i18n_' . $language, $data);

				flashMsg(__('msg_saved'));

				redirect('admin/translations/edit/' . $language);
			}

			$this->content = tpl('translations/edit.php', array(
				'lang' => $language,
				'translations' => $translations,
				'custom_18n' => $custom
					));
		} else {
			redirect('admin/translations');
		}
	}

	public function reset($language)
	{
		if (Registry::get('i18n_' . $language)) {
			Registry::set('i18n_' . $language, null);
			flashMsg(__('msg_deleted'));
		}

		redirect('admin/translations/edit/' . $language);
	}

}