<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Labels extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('settings');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		$order_by = 'name';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		if (!empty($_POST)) {
			foreach ($_POST['name'] as $id => $name) {
				if ($id && ($label = Core::$db->label[(int) $id])) {
					if (trim($name)) {
						$label->update(array(
							'name' => trim($name),
							'color' => $_POST['color'][$id]
						));
					} else {
						$label->delete();
					}
				} else if ($id == 0 && $_POST['name'][0]) {
					Core::$db->label(array(
						'name' => trim($_POST['name'][0]),
						'color' => $_POST['color'][0]
					));
				}
			}

			redirect('admin/labels');
		}

		$labels = Core::$db->label()->order($order_by . ' ' . $order_dir);

		$this->content = tpl('labels/list.php', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'labels' => $labels
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		if ($label = Core::$db->label[(int) $id]) {
			$label->product_labels()->delete();
			$label->delete();

			flashMsg(__('msg_deleted'));
		}

		redirect('admin/labels');
	}

}