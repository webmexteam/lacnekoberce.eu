<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Ckfilemanager extends AdminController
{

	public function index()
	{

	}

	public function filetree()
	{
		$this->tpl = null;

		if (!isSet($_POST['dir'])) {
			die('Missing argument');
		}

		$_POST['dir'] = urldecode($_POST['dir']);

		if (file_exists(DOCROOT . $_POST['dir'])) {
			$files = scandir(DOCROOT . $_POST['dir']);

			natcasesort($files);

			if (count($files) > 2) {
				echo "<ul class=\"jqueryFileTree\" style=\"display: none;\">";
				// All dirs
				foreach ($files as $file) {
					if (file_exists(DOCROOT . $_POST['dir'] . $file) && $file != '.' && $file != '..' && is_dir(DOCROOT . $_POST['dir'] . $file)) {
						echo "<li class=\"directory collapsed\"><a href=\"#\" rel=\"" . htmlentities($_POST['dir'] . $file) . "/\">" . htmlentities($file) . "</a></li>";
					}
				}
				// All files
				foreach ($files as $file) {
					if (file_exists(DOCROOT . $_POST['dir'] . $file) && $file != '.' && $file != '..' && !is_dir(DOCROOT . $_POST['dir'] . $file)) {
						$ext = preg_replace('/^.*\./', '', $file);
						echo "<li class=\"file ext_$ext\"><a href=\"#\" rel=\"" . htmlentities($_POST['dir'] . $file) . "\">" . htmlentities($file) . "</a></li>";
					}
				}
				echo "</ul>";
			}
		}

		exit;
	}

	public function files()
	{
		$this->tpl = null;

		require_once(DOCROOT . 'vendor/ckfilemanager/connectors/php/filemanager.config.php');
		require_once(DOCROOT . 'vendor/ckfilemanager/connectors/php/filemanager.class.php');

		$fm = new Filemanager($config);

		$response = '';

		if (!auth()) {
			$fm->error($fm->lang('AUTHORIZATION_REQUIRED'));
		}

		if (!isset($_GET)) {
			$fm->error($fm->lang('INVALID_ACTION'));
		} else {

			if (isset($_GET['mode']) && $_GET['mode'] != '') {

				switch ($_GET['mode']) {

					default:

						$fm->error($fm->lang('MODE_ERROR'));
						break;

					case 'getinfo':

						if ($fm->getvar('path')) {
							$response = $fm->getinfo();
						}
						break;

					case 'getfolder':

						if ($fm->getvar('path')) {
							$response = $fm->getfolder();
						}
						break;

					case 'rename':

						if ($fm->getvar('old') && $fm->getvar('new')) {
							$response = $fm->rename();
						}
						break;

					case 'delete':

						if ($fm->getvar('path')) {
							$response = $fm->delete();
						}
						break;

					case 'addfolder':

						if ($fm->getvar('path') && $fm->getvar('name')) {
							$response = $fm->addfolder();
						}
						break;

					case 'download':
						if ($fm->getvar('path')) {
							$fm->download();
						}
						break;
				}
			} else if (isset($_POST['mode']) && $_POST['mode'] != '') {

				switch ($_POST['mode']) {

					default:

						$fm->error($fm->lang('MODE_ERROR'));
						break;

					case 'add':

						if ($fm->postvar('currentpath')) {
							$fm->add();
						}
						break;
				}
			}
		}

		echo json_encode($response);
		exit;
	}

}