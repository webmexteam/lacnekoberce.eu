<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Pages extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('pages');

		Core::$active_tab = 'pages';
	}

	public function index()
	{
		$order_by = 'position';
		$order_dir = 'ASC';
		$expand_all = Registry::get('pages_expand_all');

		if (isset($_GET['expand_all'])) {
			$expand_all = (int) $_GET['expand_all'];
			Registry::set('pages_expand_all', $expand_all);
		}

		sortDir($order_by, $order_dir);

		if (isSet($_POST['save']) && !empty($_POST['position'])) {
			foreach ($_POST['position'] as $id => $position) {
				$page = Core::$db->page[$id];

				if ($page) {
					$page->update(array('position' => (int) $position, 'status' => isSet($_POST['status'][$id]) ? 1 : 0));
				}
			}

			flashMsg(__('msg_saved'));
		}

		$this->content = tpl('pages/list.php', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'expand_all' => $expand_all
				));
	}

	public function subpages($parent_id, $level = 0)
	{
		$this->tpl = null;

		echo $this->_pageChildren($parent_id, $level);
	}

	public function edit($page_id)
	{
		$page = Core::$db->page[(int) $page_id];

		if ($page_id > 0 && !$page) {
			flashMsg(__('msg_record_not_found', $page_id), 'error');
			redirect('admin/pages');
		}

		$content_files = array(
			'' => __('none')
		);

		$rootlen = strlen(DOCROOT . 'etc/pages/');
		foreach ((array) Core::glob('pages', false, true) as $file) {
			$fname = substr($file, $rootlen);
			$content_files[$fname] = $fname;
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;
				$data['last_change'] = time();
				$data['last_change_user'] = User::get('id');

				if ($data['parent_page'] != 0) {
					$data['menu'] = 0;
				}

				if (!empty($data['pubdate'])) {
					$data['pubdate'] = strtotime($data['pubdate']);
				}

				if (!empty($data['discount_date'])) {
					$data['discount_date'] = strtotime($data['discount_date']);
				}

				if (!$page) {
					$page_id = Core::$db->page(prepare_data('page', $data));

					if ($page_id) {
						$page = Core::$db->page[$page_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $page->update(prepare_data('page', $data));
				}

				if ($page && $saved !== false) {
					$deleted = array();

					if (!empty($_POST['files_delete'])) {
						$deleted = $_POST['files_delete'];

						Core::$db->page_files()->where('id', $deleted)->delete();
					}

					if (!empty($_POST['files_filename'])) {
						foreach ($_POST['files_filename'] as $file_id => $filename) {

							if (!is_numeric($file_id) && !in_array($file_id, $deleted)) {
								$is_image = (bool) preg_match('/\.(jpe?g|gif|png)$/i', $filename);

								Core::$db->page_files(array(
									'page_id' => $page_id,
									'filename' => $filename,
									'description' => $_POST['files_description'][$file_id],
									'position' => $_POST['files_position'][$file_id],
									'is_image' => $is_image ? 1 : 0,
									'size' => ($is_image && isSet($_POST['files_size'][$file_id])) ? $_POST['files_size'][$file_id] : null,
									'align' => ($is_image && isSet($_POST['files_align'][$file_id])) ? $_POST['files_align'][$file_id] : null
								));

								$info = pathinfo(DOCROOT . $filename);

								if ($is_image && !file_exists($info['dirname'] . '/_' . Core::$def['image_sizes'][2] . '/' . $info['basename'])) {
									Image::thumbs(DOCROOT . $filename);
								}
							} else {
								Core::$db->page_files()->where('id', $file_id)->update(array(
									'description' => $_POST['files_description'][$file_id],
									'position' => $_POST['files_position'][$file_id],
									'size' => isSet($_POST['files_size'][$file_id]) ? $_POST['files_size'][$file_id] : null,
									'align' => isSet($_POST['files_align'][$file_id]) ? $_POST['files_align'][$file_id] : null
								));
							}
						}
					}

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/pages');
				} else {
					redirect('admin/pages/edit/' . $page['id']);
				}
			}
		} else if ($page_id == 0) {
			$page['id'] = 0;
			$page['name'] = __('new_page');
			$page['menu'] = 5; // hidden page
			$page['position'] = 0;
			$page['status'] = 1;
			$page['enable_filter'] = 1;
			$page['products'] = 1;
		}

		$featuresets = array(0 => '&mdash;');
		foreach (Core::$db->featureset() as $featureset) {
			$featuresets[$featureset['id']] = $featureset['name'];
		}

		$this->content = tpl('pages/edit.php', array(
			'page' => $page,
			'pagetree' => $this->_pageTree($page),
			'content_files' => $content_files,
			'featuresets' => $featuresets
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$page = Core::$db->page[(int) $id];

		if ($page) {
			$this->deleteChildren((int) $id);

			$page->product_pages()->delete();
			$page->page_files()->delete();
			$page->delete();
		}

		redirect('admin/pages');
	}

	private function _pageTree($page_obj, $parent_id = 0, $level = 1)
	{
		$out = '';
		$order = 'position ASC';

		if (!empty($_GET['sort'])) {
			$order = $_GET['sort'] . ' ASC';
		}

		if ($level == 1) {
			$out .= '<option value="0">' . __('none') . '</option>';

			foreach (Core::$def['page_types'] as $type_id => $type) {
				$pages = Core::$db->page()->where('parent_page', $parent_id)->where('menu', $type_id)->order($order);

				if (count($pages)) {
					$out .= '<option disabled="disabled">' . $type . '</option>';

					foreach ($pages as $page) {
						$out .= '<option value="' . $page['id'] . '"' . ($page_obj['parent_page'] == $page['id'] ? ' selected="selected"' : '') . ($page_obj['id'] == $page['id'] ? ' disabled="disabled"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

						$out .= $this->_pageTree($page_obj, $page['id'], $level + 1);
					}
				}
			}
		} else {
			foreach (Core::$db->page()->where('parent_page', $parent_id)->order($order) as $page) {
				$out .= '<option value="' . $page['id'] . '"' . ($page_obj['parent_page'] == $page['id'] ? ' selected="selected"' : '') . ($page_obj['id'] == $page['id'] ? ' disabled="disabled"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

				$out .= $this->_pageTree($page_obj, $page['id'], $level + 1);
			}
		}

		return $out;
	}

	public function _pageChildren($page_id, $level, &$pages_i = 0, $expand_all = 0)
	{
		$out = '';
		$order = 'position ASC';

		if (!empty($_GET['sort'])) {
			$order = $_GET['sort'] . ' ASC';
		}

		$pages = is_object($page_id) ? $page_id : Core::$db->page()->where('parent_page', $page_id)->order($order);

		foreach ($pages as $page) {
			$out .= tpl('pages/list_page.php', array(
				'page' => $page,
				'i' => $pages_i,
				'level' => $level,
				'parent_id' => is_object($page_id) ? 0 : $page_id,
				'expand_all' => $expand_all
					));

			$pages_i++;

			if ($expand_all) {
				$out .= $this->_pageChildren($page['id'], $level + 1, $pages_i, $expand_all);
			}
		}

		return $out;
	}

	private function deleteChildren($parent_id)
	{
		if (($pages = Core::$db->page()->where('parent_page', $parent_id)) && count($pages)) {
			foreach ($pages as $page) {
				$this->deleteChildren($page['id']);

				$page->product_pages()->delete();
				$page->page_files()->delete();
				$page->delete();
			}
		}
	}

}