<?php

defined('WEBMEX') or die('No direct access.');

$gopay = array(
	// pridelene goID
	'goId' => '',
	// prideleny tajny klic
	'secret' => '',
	// testovaci provoz (gate.gopay.cz)
	'test' => false
);