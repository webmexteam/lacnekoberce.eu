<?php

defined('WEBMEX') or die('No direct access.');

$paypal = array(
	// API udaje
	'username' => '',
	'password' => '',
	'signature' => '',
	// true pro pouziti tzv. sandboxu (testovaci rozhrani)
	'sandbox' => true,
	// kod meny (CZK, EUR, USD, GBP,…)
	'currency_code' => 'CZK',
	// kod jazyka (cestina neni podporovana)
	'locale_code' => 'US'
);