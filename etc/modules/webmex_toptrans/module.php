<?php defined('WEBMEX') or die('No direct access.');

/**
 * @author Tomas Nikl <tomasnikl.cz@gmail.com>
 */
class Module_Webmex_Toptrans extends Module {
	
	public $has_settings = true;

	protected $options = array(
		'delivery_id' => 1,
	);

	public $module_info = array(
		'name'		=> array(
			'cs' => 'Nastavení dopravce TOPTRANS'
		),
		'description' => array(
			'cs' => 'Přidá možnost konfigurace dopravy pro dopravce TOPTRANS'
		),
		
		'version'	=> '1.0.1',
		'license'	=> 'Webmex.cz',
		'author'	=> 'Webmex.cz; (c) 2013',
		'www'		=> 'http://www.webmex.cz/'
	);

	public function install($module) {
		
		$module->update(array('storage' => serialize($this->options)));

		$cols = array(
			'id' => array(
				'type' => 'integer',
				'primary' => true,
				'autoincrement' => true
			),
			'region' => array(
				'type' => 'varchar'
			),
			'w05' => array(
				'type' => 'float'
			),
			'w5' => array(
				'type' => 'float'
			),
			'w15' => array(
				'type' => 'float'
			),
			'w30' => array(
				'type' => 'float'
			),
			'w50' => array(
				'type' => 'float'
			),
			'w75' => array(
				'type' => 'float'
			),
			'w100' => array(
				'type' => 'float'
			),
			'w150' => array(
				'type' => 'float'
			),
			'w200' => array(
				'type' => 'float'
			),
			'w300' => array(
				'type' => 'float'
			),
			'w400' => array(
				'type' => 'float'
			),
			'w500' => array(
				'type' => 'float'
			),
			'w700' => array(
				'type' => 'float'
			),
			'w1000' => array(
				'type' => 'float'
			),
			'w1500' => array(
				'type' => 'float'
			),
			'w2000' => array(
				'type' => 'float'
			),
			'w2500' => array(
				'type' => 'float'
			),
			'w3000' => array(
				'type' => 'float'
			),
		);
		
		Core::$db_inst->schema->createTable('webmex_module_toptrans', $cols);
	}

	public function saveSettingForm() {
		if(! empty($_POST)){

			// Update storage
			$data = (array) $this->getStorage();
			if(!isset($_POST['delivery_id'])){
				$_POST['delivery_id'] = 1;
			}
			$data = Arr::overwrite($this->options, $data, $_POST);
			$this->saveStorage($data);

			// Update database
			$post = $_POST;
			if(isset($post['id'])){
				foreach ($post['id'] as $key => $value) {
					
					// Update
					$update = array(
						'region' => $post['region'][$key],
						'w05'    => (float) $post['w05'][$key],
						'w5'     => (float) $post['w5'][$key],
						'w15'    => (float) $post['w15'][$key],
						'w30'    => (float) $post['w30'][$key],
						'w50'    => (float) $post['w50'][$key],
						'w75'    => (float) $post['w75'][$key],
						'w100'   => (float) $post['w100'][$key],
						'w150'   => (float) $post['w150'][$key],
						'w200'   => (float) $post['w200'][$key],
						'w300'   => (float) $post['w300'][$key],
						'w400'   => (float) $post['w400'][$key],
						'w500'   => (float) $post['w500'][$key],
						'w700'   => (float) $post['w700'][$key],
						'w1000'  => (float) $post['w1000'][$key],
						'w1500'  => (float) $post['w1500'][$key],
						'w2000'  => (float) $post['w2000'][$key],
						'w2500'  => (float) $post['w2500'][$key],
						'w3000'  => (float) $post['w3000'][$key],
					);

					$trans = Core::$db->webmex_module_toptrans[$key];
					if($trans){
						$trans->update($update);
					}

					// Delete
					if(isset($post['remove'][$key])){
						$trans = Core::$db->webmex_module_toptrans[$key];
						if($trans){
							$trans->delete();
						}
					}

					// Add new
					if(isset($post['add_region']) && trim($post['add_region']) != ""){
						$insert = array(
							'region' => $post['add_region'],
							'w05'    => (float) $post['add_w05'],
							'w5'     => (float) $post['add_w5'],
							'w15'    => (float) $post['add_w15'],
							'w30'    => (float) $post['add_w30'],
							'w50'    => (float) $post['add_w50'],
							'w75'    => (float) $post['add_w75'],
							'w100'   => (float) $post['add_w100'],
							'w150'   => (float) $post['add_w150'],
							'w200'   => (float) $post['add_w200'],
							'w300'   => (float) $post['add_w300'],
							'w400'   => (float) $post['add_w400'],
							'w500'   => (float) $post['add_w500'],
							'w700'   => (float) $post['add_w700'],
							'w1000'  => (float) $post['add_w1000'],
							'w1500'  => (float) $post['add_w1500'],
							'w2000'  => (float) $post['add_w2000'],
							'w2500'  => (float) $post['add_w2500'],
							'w3000'  => (float) $post['add_w3000'],
						);

						Core::$db->webmex_module_toptrans()->insert($insert);
					}
				}
			}else{
				// Add new
				if(isset($post['add_region']) && trim($post['add_region']) != ""){
					$insert = array(
						'region' => $post['add_region'],
						'w05'    => (float) $post['add_w05'],
						'w5'     => (float) $post['add_w5'],
						'w15'    => (float) $post['add_w15'],
						'w30'    => (float) $post['add_w30'],
						'w50'    => (float) $post['add_w50'],
						'w75'    => (float) $post['add_w75'],
						'w100'   => (float) $post['add_w100'],
						'w150'   => (float) $post['add_w150'],
						'w200'   => (float) $post['add_w200'],
						'w300'   => (float) $post['add_w300'],
						'w400'   => (float) $post['add_w400'],
						'w500'   => (float) $post['add_w500'],
						'w700'   => (float) $post['add_w700'],
						'w1000'  => (float) $post['add_w1000'],
						'w1500'  => (float) $post['add_w1500'],
						'w2000'  => (float) $post['add_w2000'],
						'w2500'  => (float) $post['add_w2500'],
						'w3000'  => (float) $post['add_w3000'],
					);

					Core::$db->webmex_module_toptrans()->insert($insert);
				}
			}
			flashMsg(__('msg_saved'));
		}
	}
	
	public function getSettingForm() {
		return tpl('module_webmex_toptrans.php', array('module' => $this, 'storage' => $this->getStorage(), 'transports' => Core::$db->webmex_module_toptrans()->order('id DESC')));
	}
}