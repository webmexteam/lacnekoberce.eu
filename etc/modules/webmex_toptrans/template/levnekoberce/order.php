<?php defined('WEBMEX') or die('No direct access.');

	$view->override = false;
	$view->slot_start(); 
	$regions = Core::$db->webmex_module_toptrans();
	$_wm_weight = 0;
	$storage = Core::module('webmex_toptrans')->getStorage();

	foreach($basket_products as $_wm_product){
		if(isset($_wm_product['weight']) && (float) $_wm_product['weight'] > 0){
			$_wm_weight = $_wm_weight + (float) $_wm_product['weight'];
		}
	}
?>

	<div class="input">
		<div class="row-fluid">
			<div class="span4">
				<label for="delivery_1">
					<input type="radio" name="delivery" id="delivery_1" value="1" class="checkbox"><strong>Rozvoz ČR - Top Trans, </strong>
				</label>
			</div>
			<div class="span8">
				<div class="input inline inline-right required">
			<label for="region"><?php echo __('webmex_module_toptrans_region')?>:</label>
			<select name="region">
				<?php foreach ($regions as $region):?>
					<option value="<?php echo $region['id']?>"><?php echo $region['region']?></option>
				<?php endforeach?>
			</select>
		</div>
		<input type="hidden" name="webmex_module_toptrans_cena" id="webmex_module_toptrans_cena">
			</div>
		</div>
	</div>
	

<?php $html = $view->slot_stop();  ?>


<?php $view->slot_start(); ?>

	<script>
		$(function(){
			var webmex_module_toptrans_regions = [];
			var webmex_module_toptrans_weight = <?php echo $_wm_weight ?>;
			var webmex_module_toptrans_price;
			var webmex_module_toptrans_delivery_id = <?php echo $storage['delivery_id']?>;


			<?php $i=0; foreach ($regions as $region):?>
				webmex_module_toptrans_regions[<?php echo $region['id'] ?>] = {
					'region' : '<?php echo $region['region'] ?>',
					'w05' : '<?php echo $region['w05'] ?>',
					'w5' : '<?php echo $region['w5']?>',
					'w15' : '<?php echo $region['w15']?>',
					'w30' : '<?php echo $region['w30']?>',
					'w50' : '<?php echo $region['w50']?>',
					'w75' : '<?php echo $region['w75']?>',
					'w100' : '<?php echo $region['w100']?>',
					'w150' : '<?php echo $region['w150']?>',
					'w200' : '<?php echo $region['w200']?>',
					'w300' : '<?php echo $region['w300']?>',
					'w400' : '<?php echo $region['w400']?>',
					'w500' : '<?php echo $region['w500']?>',
					'w700' : '<?php echo $region['w700']?>',
					'w1000' : '<?php echo $region['w1000']?>',
					'w1500' : '<?php echo $region['w1500']?>',
					'w2000' : '<?php echo $region['w2000']?>',
					'w2500' : '<?php echo $region['w2500']?>',
					'w3000' : '<?php echo $region['w3000']?>'
				};
			<?php $i++; endforeach; ?>

			recalculateDeliveryTopTrans($('select[name="region"]').val());

			$('select[name="region"]').change(function(){
				if($('.delivery input#delivery_'+webmex_module_toptrans_delivery_id).is(':checked')){
					recalculateDeliveryTopTrans($(this).val());
				}else{
					$('#webmex_module_toptrans_cena').val("");
				}
			});

			$('.payment input, .payment label').click(function(){
				if($('.delivery input#delivery_'+webmex_module_toptrans_delivery_id).is(':checked')){
					recalculateDeliveryTopTrans($('select[name="region"]').val());
				}else{
					$('#webmex_module_toptrans_cena').val("");
				}
			})

			$('.delivery input#delivery_'+webmex_module_toptrans_delivery_id).click(function(){
				recalculateDeliveryTopTrans($('select[name="region"]').val());
			});

			function recalculateDeliveryTopTrans(id) {
				var p = 0,
					r = webmex_module_toptrans_regions,
					w = webmex_module_toptrans_weight;

				if(w <= 0.5){ 			p = r[id]['w05']; }
				if(w>0.5 && w<=5){ 		p = r[id]['w5']; }
				if(w>5 && w<=15){ 		p = r[id]['w15']; }
				if(w>15 && w<=30){ 		p = r[id]['w30']; }
				if(w>30 && w<=50){ 		p = r[id]['w50']; }
				if(w>50 && w<=75){ 		p = r[id]['w75']; }
				if(w>75 && w<=100){ 	p = r[id]['w100']; }
				if(w>100 && w<=150){ 	p = r[id]['w150']; }
				if(w>150 && w<=200){ 	p = r[id]['w200']; }
				if(w>200 && w<=300){ 	p = r[id]['w300']; }
				if(w>300 && w<=400){ 	p = r[id]['w400']; }
				if(w>400 && w<=500){ 	p = r[id]['w500']; }
				if(w>500 && w<=700){ 	p = r[id]['w700']; }
				if(w>700 && w<=1000){ 	p = r[id]['w1000']; }
				if(w>1000 && w<=1500){ 	p = r[id]['w1500']; }
				if(w>1500 && w<=2000){ 	p = r[id]['w2000']; }
				if(w>2000 && w<=2500){ 	p = r[id]['w2500']; }
				if(w>2500 && w<=3000){ 	p = r[id]['w3000']; }

				for (var i = 0; i < delivery_payments.length; i++) {
					if(delivery_payments[i]['delivery_id'] == webmex_module_toptrans_delivery_id){
						delivery_payments[i]['price'] = p;
					}
				};

				if($('#deliverypayment').text() != ""){
					$('#deliverypayment').html(displayPrice(price_vat(p, _vat_delivery)));
				}
				$('.payment .price').html(p ? displayPrice(price_vat(p, _vat_delivery)) : _lang.free_delivery).show();

				// Recalculate
				if(typeof _subtotal != 'undefined'){
					var total = parseFloatNum(_subtotal) + price_vat(parseFloatNum(p), _vat_delivery);

					if(typeof _voucher_value != 'undefined'){
						var discount = estPrice(_voucher_value, total);
						$('#discount').html('-'+displayPrice(discount));
						total -= discount;
						
						if(total < 0){
							total = 0;
						}
					}

					total = Math.round(total * Math.pow(10, _order_round_decimals)) / Math.pow(10, _order_round_decimals);

					$('#total').html(displayPrice(total));
				}

				$('#webmex_module_toptrans_cena').val(p);

				return p;
			}

		});
	</script>

<?php $html2 = $view->slot_stop();  ?>


<?php 
	$_tmp = $view->find('#delivery_1')->parent()->parent();
	$_tmp->after($html);

	$_tmp2 = $view->find('.basket');
	$_tmp2->before($html2);

	$_tmp3 = $view->find('form.form');
	$_tmp3->attr('action', url('webmex_toptrans/'));
?>