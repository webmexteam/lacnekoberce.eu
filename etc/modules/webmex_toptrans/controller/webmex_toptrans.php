<?php defined('WEBMEX') or die('No direct access.');

class Controller_Webmex_toptrans extends Controller_Default {

	public function step1() {
		$storage = Core::module('webmex_toptrans')->getStorage();

		$data = $_POST;
		$error = false;
		$dp_price = null;

		if(isset($_POST['webmex_module_toptrans_cena']) && !empty($_POST['webmex_module_toptrans_cena'])) {
			$_SESSION['order']['region'] = $_POST['region'];
			$region = Core::$db->webmex_module_toptrans[(int) $_POST['region']];
			$region = ' '.$region['region'];
			$dp_price = (float) $_POST['webmex_module_toptrans_cena'];
		}else{
			$region = "";
			$_SESSION['order']['region'] = null;
		}

		$delivery = Core::$db->delivery[(int) $data['delivery']];
		$payment = Core::$db->payment[(int) $data['payment']];

		if ($delivery && $payment) {
			$_SESSION['order']['delivery'] = (int) $delivery['id'];
			$_SESSION['order']['payment'] = (int) $payment['id'];

			$_SESSION['order']['delivery_name'] = $data['delivery_name'].$region;
		} else {
			$error = true;
		}

		if(isset($data['dp_price']))
			$_SESSION['order']['dp_price'] = $data['dp_price'];

		if(isset($data['total_price']))
			$_SESSION['order']['total_price'] = $data['total_price'];

		if ($error) {
			View::$global_data['order_errors'] = array(array('', __('order_cannot_be_send')));
		} else {
			redirect(url(PAGE_ORDER2, array(), true));
		}

	}

	public function step2() {
		$this->_saveOrder();
	}

	public function index()
	{
		if (isSet($_POST['login']) && !empty($_POST['customer_email']) && !empty($_POST['customer_password'])) {
			// customer login
			if (($result = Customer::login($_POST['customer_email'], $_POST['customer_password'])) !== true) {
				View::$global_data['customer_login_error'] = __($result);
			} else {
				redirect(PAGE_ORDER);
			}
		}

		$customer = null;

		if (Customer::$logged_in) {
			View::$global_data['order'] = Customer::get();
			$customer = Core::$db->customer[Customer::get('id')];
		}

		$basket_total = Basket::total();

		$deliveries_payments = $this->getDeliveriesPayments($customer, $basket_total->_total_before_discount, $basket_total->weight);

		$deliveries = $deliveries_payments['deliveries'];
		$payments = $deliveries_payments['payments'];

		View::$global_data['deliveries'] = $deliveries;
		View::$global_data['payments'] = $payments;

		$phone_required = (Core::config('phone_required') === null || (int) Core::config('phone_required'));
		View::$global_data['phone_required'] = $phone_required;

		if (isSet($_POST['submit_order'])) {
			$required = array('first_name', 'last_name', 'street', 'city', 'zip', 'delivery', 'payment');
			$errors = array();

			if ($phone_required) {
				$required[] = 'phone';
			}

			if (!Customer::$logged_in) {
				$required[] = 'email';
			}

			foreach ($required as $name) {
				if (empty($_POST[$name])) {
					$errors[] = array('required', __($name));
				} else if ($name == 'email' && !valid_email($_POST[$name])) {
					$errors[] = array('invalid_email', __($name));
				} else if ($name == 'phone') {
					$_POST[$name] = preg_replace('/[^\d\+]/', '', trim($_POST[$name]));

					if (!preg_match('/^((\+|00)[\d]{2,3})?[\d]{7,14}?$/', $_POST[$name])) {
						$errors[] = array('invalid_phone', __($name));
					}
				}
			}

			// simple spam protection
			if (strlen($_SERVER['HTTP_USER_AGENT']) < 25) {
				// if HTTP_USER_AGENT is too short, it is propably a bot

				$errors[] = array('', __('order_cannot_be_send'));
			}

			if (empty($errors)) {
				$this->_saveOrder();
			} else {
				View::$global_data['order_errors'] = $errors;
			}

			View::$global_data['order'] = array_merge((array) View::$global_data['order'], $_POST);
		}
	}



	public function _saveOrder()
	{
		$data = $_POST;
		$error = false;
		$dp_price = null;

		$data['received'] = time();
		$data['ip'] = $_SERVER['REMOTE_ADDR'];

		if (Customer::$logged_in) {
			$data['customer_id'] = Customer::get('id');
			$data['email'] = Customer::get('email');
		}

		$group_ids = array(0, (int) Core::config('customer_group_default'));

		if (Customer::$logged_in) {
			$group_ids[] = (int) Core::config('customer_group_registered');

			if (($gid = (int) Customer::get('customer_group_id'))) {
				$group_ids[] = $gid;
			}
		}

		$delivery = Core::$db->delivery[(int) $_SESSION['order']['delivery']];
		$payment = Core::$db->payment[(int) $_SESSION['order']['payment']];
		$delivery['name'] = $_SESSION['order']['delivery_name'];

		$subtotal = Area_Units_Basket::total();

		if ($delivery && $payment) {
			$dp_price = estPrice($delivery['price'], $subtotal->subtotal) + estPrice($payment['price'], $subtotal->subtotal);

			if (!in_array($delivery['customer_group_id'], $group_ids) || !in_array($payment['customer_group_id'], $group_ids)) {
				$error = true;
			} else {
				if ($dp = Core::$db->delivery_payments()->where('delivery_id', $delivery['id'])->where('payment_id', $payment['id'])->fetch()) {
					$dp_price += estPrice($dp['price'], $subtotal->subtotal);

					if ($dp['free_over'] && parseFloat($dp['free_over']) <= $subtotal->total) {
						$dp_price = 0;
					}

					$data['delivery_id'] = $delivery['id'];
					$data['payment_id'] = $payment['id'];
					$data['delivery_payment'] = $dp_price;
				} else {
					// disallowed D&P combination
					$error = true;
				}
			}
		} else {
			$error = true;
		}

		$strg = Core::module('webmex_toptrans')->getStorage();
		if($data['delivery_id'] == $strg['delivery_id']) {
			$data['delivery_payment'] = (float) $_SESSION['order']['dp_price'];
		}

		if ($error) {
			View::$global_data['order_errors'] = array(array('', __('order_cannot_be_send')));
		} else {

			$_total = $subtotal->_total_before_discount + price_vat($dp_price, VAT_DELIVERY)->price;

			if ($voucher = Area_Units_Basket::voucher()) {
				$data['voucher_id'] = $voucher['id'];

				$discount = estPrice($voucher['value'], $_total);
				$_total -= $discount;

				if ($_total < 0) {
					$_total = 0;
				}
			}

			if($data['delivery_id'] == $strg['delivery_id']) {
				$_total += (float) $_SESSION['order']['dp_price'];
			}

			$data['total_price'] = $subtotal->subtotal;

			if($data['delivery_id'] == $strg['delivery_id']) {
				$data['total_price'] += (float) $_SESSION['order']['dp_price']-(float) $_SESSION['order']['dp_price']*0.1736;
			}

			$data['total_price'] = round($data['total_price']);

			$data['total_incl_vat'] = round($_total, (int) Core::config('order_round_decimals'));
			$total_excl_vat = round($subtotal->subtotal + $dp_price, (int) Core::config('order_round_decimals'));

			$data = prepare_data('order', $data);

			$order_id = Core::$db->order($data);

			$data['id'] = $order_id;

			if (isset($voucher) && $voucher) {
				$voucher->update(array('used' => $voucher['used'] + 1));

				Core::$db->voucher_orders(array(
					'order_id' => $order_id,
					'voucher_id' => $voucher['id'],
					'date' => time()
				));
			}

			$_SESSION['order_id'] = (int) $order_id;

			foreach (Area_Units_Basket::products() as $product) {
				$name = trim($product['product']['name'] . ' ' . $product['product']['nameext']) . ($product['attributes'] ? ' [' . $product['attributes'] . ']' : '');
				if($product['basket_product']['area_units_unit_1'] && $product['basket_product']['area_units_unit_1']) {
					$name .= ' '.'('.$product['basket_product']['area_units_unit_1'].$product['product']['area_units_unit_1'].' x '.$product['basket_product']['area_units_unit_2'].$product['product']['area_units_unit_2'].')';
				}
				$price = $product['price'];
				if(!isset( $product['product']['id']) || (int) $product['product']['id'] == 0) {
					if($data['delivery_id'] == $strg['delivery_id']) {
						$price = (float) $_SESSION['order']['dp_price'];
					}
				}

                $u2Fee = 0;
				if($product['basket_product']['area_units_unit_1'] > 0 && $product['basket_product']['area_units_unit_2'] > 0) {
					//$name .= ' '.'('.$product['basket_product']['area_units_unit_1'].$product['product']['area_units_unit_1'].' x '.$product['basket_product']['area_units_unit_2'].$product['product']['area_units_unit_2'].')';
                    $u2Fee = $product['basket_product']['area_units_unit_1'] * $product['basket_product']['area_units_unit_2'] * $product['basket_product']['area_units_unit_2_fee'];
				}

                $transportFee = 0;
                if(floatval($product['product']['price_transport']) > 0) {
                    //$name .= ' včetně transportní návinky + '.price_vat($product['product']['price_transport'], $product['product']['vat']);
                    $transportFee = $product['product']['price_transport'];
                }

				$oproduct_id = Core::$db->order_products(array(
					'order_id' => $order_id,
					'product_id' => $product['product']['id'],
					'price' => $price + $u2Fee + $transportFee,
					'vat' => $product['product']['vat'],
					'quantity' => $product['qty'],
					'sku' => $product['sku'],
					'ean13' => $product['ean13'],
					'name' => $name
						));

				$_update = array();

				$_product = Core::$db->order_products[(int) $oproduct_id];

				if ((int) Core::config('stock_auto')) {
					$product['product']->model->setStock((int) $product['qty'] * -1, $product['attributes_ids']);
				}

				$_update['sold_qty'] = $product['product']['sold_qty'] + $product['qty'];

				if (!empty($_update)) {
					$product['product']->update($_update);
				}
			}

			if($data['delivery_id'] == $strg['delivery_id']) {
				$dp_price = (float) $_SESSION['order']['dp_price'];
			}

			Core::$db->order_products(array(
				'order_id' => $order_id,
				'price' => $dp_price,
				'vat' => VAT_DELIVERY,
				'quantity' => 1,
				'type' => 1,
				'name' => __('delivery_payment') . ' [' . $delivery['name'] . ', ' . $payment['name'] . ']'
			));

			Area_Units_Basket::destroy();

			$order = Core::$db->order[(int) $order_id];

			Email::event('new_order', $order['email'], null, array('order' => $order));
			Email::event('new_order', Core::config('email_notify'), $order['email'], array('order' => $order));

			$customer_id = 0;
			$customer_data = $data;

			unset($customer_data['id']);

			if ($customer = Core::$db->customer()->where('email', $customer_data['email'])->fetch()) {
				$customer_id = $customer['id'];
			}

			//create customer account
			if (!$customer && isSet($_POST['create_account'])) {
				$password = strtoupper(random());
				$customer_data['password'] = sha1(sha1($password));

				$customer_data['active'] = (int) Core::config('customer_confirmation') ? 0 : 1;

				$customer_id = Core::$db->customer(prepare_data('customer', $customer_data));

				if ($customer_id) {
					$customer_data['id'] = $customer_id;

					if (!(int) Core::config('customer_confirmation')) {
						$customer_data['password'] = $password;

						Email::event('new_account', $customer_data['email'], null, $customer_data);
					} else {
						sendmail(Core::config('email_notify'), array(__('customer_registered'), __('customer_registered_text')), $customer_data);
					}
				}
			}

			if (isSet($_POST['newsletter'])) {
				if (!Core::$db->newsletter_recipient()->where('email', $customer_data['email'])->fetch()) {
					Core::$db->newsletter_recipient(array(
						'email' => $customer_data['email'],
						'date' => time()
					));
				}
			}

			if ($customer_id) {
				Core::$db->order()->where('id', $order_id)->update(array(
					'customer_id' => $customer_id
				));
			}

			$group_id = (int) Core::config('customer_group_default');

			if (Customer::$logged_in) {
				$group_id = (int) Core::config('customer_group_registered');

				if (($gid = (int) Customer::get('customer_group_id'))) {
					$group_id = $gid;
				}
			}

			if ($customer_id) {
				$_customer = Core::$db->customer[$customer_id];
			} else {
				$_customer = $order['email'];
			}

			Event::run('Controller_Default::saveOrder', $order, $customer, $payment, $delivery);

			Voucher::generate('order_submit', $customer, $order);

			if (!(int) Core::config('confirm_orders') && $payment && $payment['driver']) {
				$driverclass = 'Payment_' . ucfirst($payment['driver']);
				if ($inst = new $driverclass($payment)) {
					$order = Core::$db->order[(int) $order_id];

					$inst->process($order);
				}
			}

			unset($_SESSION['order']);

			redirect(url(PAGE_ORDER_FINISH, array('finish' => $data['received']), true));
		}
	}

	public function _saveOrderDELETE()
	{
		$data = $_POST;
		$error = false;
		$dp_price = null;

		$data['received'] = time();
		$data['ip'] = $_SERVER['REMOTE_ADDR'];

		if (Customer::$logged_in) {
			$data['customer_id'] = Customer::get('id');
			$data['email'] = Customer::get('email');
		}

		$group_ids = array(0, (int) Core::config('customer_group_default'));

		if (Customer::$logged_in) {
			$group_ids[] = (int) Core::config('customer_group_registered');

			if (($gid = (int) Customer::get('customer_group_id'))) {
				$group_ids[] = $gid;
			}
		}

		$delivery = Core::$db->delivery[(int) $_SESSION['order']['delivery']];
		$payment = Core::$db->payment[(int) $_SESSION['order']['payment']];
		$delivery['name'] = $_SESSION['order']['delivery_name'];

		$subtotal = Basket::total();

		if ($delivery && $payment) {
			$dp_price = estPrice($delivery['price'], $subtotal->subtotal) + estPrice($payment['price'], $subtotal->subtotal);

			if (!in_array($delivery['customer_group_id'], $group_ids) || !in_array($payment['customer_group_id'], $group_ids)) {
				$error = true;
			} else {
				if ($dp = Core::$db->delivery_payments()->where('delivery_id', $delivery['id'])->where('payment_id', $payment['id'])->fetch()) {
					$dp_price += estPrice($dp['price'], $subtotal->subtotal);

					if ($dp['free_over'] && parseFloat($dp['free_over']) <= $subtotal->total) {
						$dp_price = 0;
					}

					$data['delivery_id'] = $delivery['id'];
					$data['payment_id'] = $payment['id'];
					$data['delivery_payment'] = $dp_price;
				} else {
					// disallowed D&P combination
					$error = true;
				}
			}
		} else {
			$error = true;
		}

		$storage = Core::module('webmex_toptrans')->getStorage();
		if($data['delivery_id'] == $storage['delivery_id']) {
			$data['delivery_payment'] = (float) $_SESSION['order']['dp_price'];
		}

		if ($error) {
			View::$global_data['order_errors'] = array(array('', __('order_cannot_be_send')));
		} else {

			$_total = $subtotal->_total_before_discount + price_vat($dp_price, VAT_DELIVERY)->price;

			if ($voucher = Basket::voucher()) {
				$data['voucher_id'] = $voucher['id'];

				$discount = estPrice($voucher['value'], $_total);
				$_total -= $discount;

				if ($_total < 0) {
					$_total = 0;
				}
			}

			$data['total_price'] = $subtotal->subtotal;
			$data['total_incl_vat'] = round($_total, (int) Core::config('order_round_decimals'));
			$total_excl_vat = round($subtotal->subtotal + $dp_price, (int) Core::config('order_round_decimals'));

			$data = prepare_data('order', $data);

			$order_id = Core::$db->order($data);

			$data['id'] = $order_id;

			if (isset($voucher) && $voucher) {
				$voucher->update(array('used' => $voucher['used'] + 1));

				Core::$db->voucher_orders(array(
					'order_id' => $order_id,
					'voucher_id' => $voucher['id'],
					'date' => time()
				));
			}

			$_SESSION['order_id'] = (int) $order_id;

			foreach (Basket::products() as $product) {
				$name = trim($product['product']['name'] . ' ' . $product['product']['nameext']) . ($product['attributes'] ? ' [' . $product['attributes'] . ']' : '');

				$oproduct_id = Core::$db->order_products(array(
					'order_id' => $order_id,
					'product_id' => $product['product']['id'],
					'price' => $product['price'],
					'vat' => $product['product']['vat'],
					'quantity' => $product['qty'],
					'sku' => $product['sku'],
					'ean13' => $product['ean13'],
					'name' => $name
						));

				$_update = array();

				$_product = Core::$db->order_products[(int) $oproduct_id];

				if ((int) Core::config('stock_auto')) {
					$product['product']->model->setStock((int) $product['qty'] * -1, $product['attributes_ids']);
				}

				$_update['sold_qty'] = $product['product']['sold_qty'] + $product['qty'];

				if (!empty($_update)) {
					$product['product']->update($_update);
				}
			}

			Core::$db->order_products(array(
				'order_id' => $order_id,
				'price' => $dp_price,
				'vat' => VAT_DELIVERY,
				'quantity' => 1,
				'type' => 1,
				'name' => __('delivery_payment') . ' [' . $delivery['name'] . ', ' . $payment['name'] . ']'
			));

			Basket::destroy();

			$order = Core::$db->order[(int) $order_id];

			Email::event('new_order', $order['email'], null, array('order' => $order));
			Email::event('new_order', Core::config('email_notify'), $order['email'], array('order' => $order));

			$customer_id = 0;
			$customer_data = $data;

			unset($customer_data['id']);

			if ($customer = Core::$db->customer()->where('email', $customer_data['email'])->fetch()) {
				$customer_id = $customer['id'];
			}

			//create customer account
			if (!$customer && isSet($_POST['create_account'])) {
				$password = strtoupper(random());
				$customer_data['password'] = sha1(sha1($password));

				$customer_data['active'] = (int) Core::config('customer_confirmation') ? 0 : 1;

				$customer_id = Core::$db->customer(prepare_data('customer', $customer_data));

				if ($customer_id) {
					$customer_data['id'] = $customer_id;

					if (!(int) Core::config('customer_confirmation')) {
						$customer_data['password'] = $password;

						Email::event('new_account', $customer_data['email'], null, $customer_data);
					} else {
						sendmail(Core::config('email_notify'), array(__('customer_registered'), __('customer_registered_text')), $customer_data);
					}
				}
			}

			if (isSet($_POST['newsletter'])) {
				if (!Core::$db->newsletter_recipient()->where('email', $customer_data['email'])->fetch()) {
					Core::$db->newsletter_recipient(array(
						'email' => $customer_data['email'],
						'date' => time()
					));
				}
			}

			if ($customer_id) {
				Core::$db->order()->where('id', $order_id)->update(array(
					'customer_id' => $customer_id
				));
			}

			$group_id = (int) Core::config('customer_group_default');

			if (Customer::$logged_in) {
				$group_id = (int) Core::config('customer_group_registered');

				if (($gid = (int) Customer::get('customer_group_id'))) {
					$group_id = $gid;
				}
			}

			if ($customer_id) {
				$_customer = Core::$db->customer[$customer_id];
			} else {
				$_customer = $order['email'];
			}

			Event::run('Controller_Default::saveOrder', $order, $customer, $payment, $delivery);

			Voucher::generate('order_submit', $customer, $order);

			if (!(int) Core::config('confirm_orders') && $payment && $payment['driver']) {
				$driverclass = 'Payment_' . ucfirst($payment['driver']);
				if ($inst = new $driverclass($payment)) {
					$order = Core::$db->order[(int) $order_id];

					$inst->process($order);
				}
			}

			unset($_SESSION['order']);

			redirect(url(PAGE_ORDER_FINISH, array('finish' => $data['received']), true));
		}
	}
}