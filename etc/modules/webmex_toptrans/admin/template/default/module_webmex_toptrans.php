<?php defined('WEBMEX') or die('No direct access.');?>

<style>
	table.datagrid tr td .input-text.short {
		width:150px;
	}
	table.datagrid tr td .input-text.shorter {
		width:50px;
	}
	table.datagrid tr td label,
	.hide-label label {
		display: none;
	}
	.hide-label .shorter {
		width:100px;
	}
</style>

<h2><?php echo __('webmex_module_toptrans_delivery_id')?></h2>
<div class="hide-label"><?php echo forminput('text', 'delivery_id', (int) $storage['delivery_id'], array('cls' => 'shorter')) ?></div>

<br>

<h2><?php echo __('webmex_module_toptrans_edit_trans')?></h2>
<table class="datagrid">
	<thead>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="18" style="text-align:center"><?php echo __('webmex_module_toptrans_weight')?></td>
		</tr>
		<tr>
			<td><?php echo __('webmex_module_toptrans_remove')?></td>
			<td><?php echo __('webmex_module_toptrans_region')?></td>
			<td>0.5</td>
			<td>5</td>
			<td>15</td>
			<td>30</td>
			<td>50</td>
			<td>75</td>
			<td>100</td>
			<td>150</td>
			<td>200</td>
			<td>300</td>
			<td>400</td>
			<td>500</td>
			<td>700</td>
			<td>1000</td>
			<td>1500</td>
			<td>2000</td>
			<td>2500</td>
			<td>3000</td>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($transports as $item):
		?>
		<tr>
			<td><?php echo forminput('checkbox', 'remove['. $item['id'] .']') ?></td>
			<td>
				<?php echo forminput('text', 'region['. $item['id'] .']', 	$item['region'], 	array('cls' => 'short')) ?>
				<input type="hidden" name="id[<?php echo $item['id']?>]" value="<?php echo $item['id']?>" />
			</td>
			<td><?php echo forminput('text', 'w05['. $item['id'] .']', 		$item['w05'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w5['. $item['id'] .']', 		$item['w5'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w15['. $item['id'] .']', 		$item['w15'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w30['. $item['id'] .']', 		$item['w30'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w50['. $item['id'] .']', 		$item['w50'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w75['. $item['id'] .']', 		$item['w75'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w100['. $item['id'] .']', 	$item['w100'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w150['. $item['id'] .']', 	$item['w150'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w200['. $item['id'] .']', 	$item['w200'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w300['. $item['id'] .']', 	$item['w300'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w400['. $item['id'] .']', 	$item['w400'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w500['. $item['id'] .']', 	$item['w500'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w700['. $item['id'] .']', 	$item['w700'], 		array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w1000['. $item['id'] .']', 	$item['w1000'], 	array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w1500['. $item['id'] .']', 	$item['w1500'], 	array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w2000['. $item['id'] .']', 	$item['w2000'], 	array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w2500['. $item['id'] .']', 	$item['w2500'], 	array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'w3000['. $item['id'] .']', 	$item['w3000'], 	array('cls' => 'shorter')) ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<br><br>

<h2><?php echo __('webmex_module_toptrans_add')?></h2>
<table class="datagrid">
	<thead>
		<tr>
			<td>&nbsp;</td>
			<td colspan="18" style="text-align:center"><?php echo __('webmex_module_toptrans_weight')?></td>
		</tr>
		<tr>
			<td><?php echo __('webmex_module_toptrans_region')?></td>
			<td>0.5</td>
			<td>5</td>
			<td>15</td>
			<td>30</td>
			<td>50</td>
			<td>75</td>
			<td>100</td>
			<td>150</td>
			<td>200</td>
			<td>300</td>
			<td>400</td>
			<td>500</td>
			<td>700</td>
			<td>1000</td>
			<td>1500</td>
			<td>2000</td>
			<td>2500</td>
			<td>3000</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo forminput('text', 'add_region', "", array('cls' => 'short')) ?></td>
			<td><?php echo forminput('text', 'add_w05', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w5', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w15', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w30', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w50', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w75', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w100', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w150', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w200', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w300', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w400', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w500', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w700', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w1000', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w1500', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w2000', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w2500', "", array('cls' => 'shorter')) ?></td>
			<td><?php echo forminput('text', 'add_w3000', "", array('cls' => 'shorter')) ?></td>
		</tr>
	</tbody>
</table>