<?php defined('WEBMEX') or die('No direct access.');

Core::$lang += array(
	'webmex_module_toptrans_delivery_id' => 'ID dopravy TOPTRANS v eshopu Webmex',
	'webmex_module_toptrans_edit_trans' => 'Existující konfigurace dopravy',
	'webmex_module_toptrans_weight'     => 'Váhav KG',
	'webmex_module_toptrans_remove'     => 'Smazat',
	'webmex_module_toptrans_region'     => 'Kraj',
	'webmex_module_toptrans_add'        => 'Přidat novou konfiguraci dopravy',
);