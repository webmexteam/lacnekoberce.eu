<?php
	$storage = Core::module('area_units')->getStorage();
	$view->override = false;

	// Zalozka
	$view->slot_start();
	echo '<li><a href="#tab-area-units">'.__('area_units_tab_title').'</a></li>';
	$html = $view->slot_stop();
	$view->find('.rightcol ul.tabs>li:first')->after($html);

	// Obsah zalozky
	$view->slot_start(); ?>
	<style>
		#tab-area-units .units label {
			float: none;
			width:100%;
			display: block;
		}
	</style>
	<div id="tab-area-units" class="tab" style="display: none;">
		<?php echo forminput('select', 'area_units_allowed', $product['area_units_allowed'], array('options'=>array(0=>__('no'), 1=>__('yes')))) ?>
		<?php echo forminput('text', 'area_units_unit', $product['area_units_unit']) ?>
		<hr>
		<div class="clearfix units">
			<div style="width:48%;float:left;">
				<div class="normal">
					<?php echo forminput('text', 'area_units_unit_1', $product['area_units_unit_1']) ?>
				</div>
				<?php
					echo forminput('select', 'area_units_lock_unit_1', $product['area_units_lock_unit_1'], array('options'=>array(0=>__('no'), 1=>__('yes'))))
						.forminput('select', 'area_units_show_unit_1', $product['area_units_show_unit_1'], array('options'=>array(0=>__('no'), 1=>__('yes'))))
						.forminput('textarea', 'area_units_default_unit_1', $product['area_units_default_unit_1'])
				?>
			</div>
			<div style="width:48%;float:right;">
				<div class="normal">
					<?php echo forminput('text', 'area_units_unit_2', $product['area_units_unit_2']) ?>
				</div>
				<?php
				echo forminput('select', 'area_units_lock_unit_2', $product['area_units_lock_unit_2'], array('options'=>array(0=>__('no'), 1=>__('yes'))))
					.forminput('select', 'area_units_show_unit_2', $product['area_units_show_unit_2'], array('options'=>array(0=>__('no'), 1=>__('yes'))))
					.forminput('textarea', 'area_units_default_unit_2', $product['area_units_default_unit_2'])
				?>
			</div>
		</div>
	</div>

	<?php $html = $view->slot_stop();
	$view->find('#tab-features')->after($html);
?>