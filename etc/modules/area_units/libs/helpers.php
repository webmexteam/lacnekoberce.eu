<?php

	function webmex_homecredit_get_float_price($product) {
		$productPrice = $product['price'];
		if ($product['recycling_fee']) {
			$productPrice += (float) $product['recycling_fee'];
		}
		if ($product['copyright_fee']) {
			$productPrice += (float) $product['copyright_fee'];
		}
		if($product['vat']) {
			$productPrice = $productPrice + $productPrice / 100 * (float) $product['vat'];
		}
		return round((float) $productPrice * 100) / 100;
	}

	/**
	 * @param string $return
	 * @return string
	 */
	function webmex_homecredit_prescoring_link($return, $storage) {
		$request_time   = date('d.m.Y-H:i:s');
		$plain_text = $storage['eshop_id'] . $request_time . $return . $storage['hash'];

		return (($storage['development']) ? $storage['dev_prescoring_url'] : $storage['pub_prescoring_url']) .
		'?shop='			. $storage['eshop_id'] .
		'&time_request='	. $request_time .
		'&ret_url='			 .$return .
		'&sh='           	. md5($plain_text);
	}

	/**
	 * PHP skript pro demonstraci vytvoreni kontrolniho souctu
	 * a navazani spojeni do aplikace i-Calc spolecnosti Home Credit.
	 *
	 * Skript je napsan v kodovani windows-1250 a mel by proto bez zasadnich
	 * uprav fungovat na systemech Microsoft Windows. Uprava skriptu pro
	 * vetsinu UNIXovych systemu by mela spocivat ve zmene hodnoty promenne
	 * source_encoding na iso-8859-2. Pro systemy zalozene na UTF8 (Red Hat
	 * Linux apod.) staci skript prekodovat (napr. pomoci iconv) a odstranit
	 * radky s prekodovanim.
	 *
	 * Autor: David Olszynski <david.olszynski@homecredit.net>
	 * Datum: 3.7.2006
	 *
	 * @param array $storage
	 * @param Db_Row $product
	 * @return string
	 */
	function webmex_homecredit_redirect_link($storage, Db_Row $product) {
		// Vypocet ceny
		$productPrice = str_replace('.', ',', webmex_homecredit_get_float_price($product));
		// Eof vypocet ceny

		$source_encoding = 'windows-1250';
		$server_encoding = 'utf-8';

		//povinne udaje pro vstupni bod
		$request_time   = date('d.m.Y-H:i:s');
		$secret_code    = $storage['hash'];

		//plain text pro kontrolni hash
		$plain_text = $storage['eshop_id'] . $productPrice . $request_time . $secret_code;

		//hash
		$checksum = md5($plain_text);

		//vstupni bod aplikace Home Credit - i-Shop
		$ishop_entry_point = (($storage['development']) ? $storage['dev_icalc_url'] : $storage['pub_icalc_url']);

		//vstupni bod aplikace i-Shop se vsemi parametry
		return $ishop_entry_point .
			'?shop='         . $storage['eshop_id'] .
			'&o_price='      . $productPrice .
			'&time_request=' . $request_time .
			'&sh='           . $checksum ;
	}


	/**
	 * PHP skript pro demonstraci vytvoreni kontrolniho souctu
	 * a navazani spojeni do aplikace i-Shop spolecnosti Home Credit.

	 * Skript je napsan v kodovani windows-1250 a mel by proto bez zasadnich
	 * uprav fungovat na systemech Microsoft Windows. Uprava skriptu pro
	 * vetsinu UNIXovych systemu by mela spocivat ve zmene hodnoty promenne
	 * source_encoding na iso-8859-2. Pro systemy zalozene na UTF8 (Red Hat
	 * Linux apod.) staci skript prekodovat (napr. pomoci iconv) a odstranit
	 * radky s prekodovanim.
	 *
	 * Autor: David Olszynski <david.olszynski@homecredit.net>
	 * Datum: 3.7.2006
	 *
	 * @param array $storage
	 * @param Db_Row $order
	 */
	function webmex_homecredit_ishop_link($storage, $order, $returnUrl) {
		$source_encoding = 'utf-8';
		$server_encoding = 'utf-8';

		// Nejdrazsi z produktu
		$price = 0;
		$biggest_product = null;
		foreach (Basket::products() as $product) {
			if($product['price']*$product['qty'] > $price) {
				$biggest_product = $product['product'];
				$price = $product['price']*$product['qty'];
			}
		}
		$product_name = $biggest_product['name'];
		$producer = '';
		foreach (Core::$db->product_pages()->where('product_id', $biggest_product['id']) as $_page) {
			$page = Core::$db->page[(int) $_page['page_id']];
			if($page['menu'] == 3) {
				$producer = $page['name'];
				break;
			}
		}

		//povinne udaje pro vstupni bod
		$shop_code      = $storage['eshop_id'];
		$order_code     = substr($order['id'],0,10);
		$goods_price    = str_replace('.', ',', $order['total_incl_vat']);
		$client_name    = substr($order['first_name'],0,30);
		$client_surname = substr($order['last_name'],0,30);
		$goods_name     = substr($product_name,0,60);
		$goods_producer = substr($producer,0,50);
		$request_time   = date('d.m.Y-H:i:s');
		$secret_code    = $storage['hash'];

		//prekodovani
		$client_name    = iconv($source_encoding, $server_encoding, $client_name);
		$client_surname = iconv($source_encoding, $server_encoding, $client_surname);
		$goods_name     = iconv($source_encoding, $server_encoding, $goods_name);
		$goods_producer = iconv($source_encoding, $server_encoding, $goods_producer);

		//plain text pro kontrolni hash
		$plain_text = $shop_code . $order_code . $goods_price . $client_name .
			$client_surname . $goods_name . $goods_producer . $request_time .
			$secret_code;

		//hash
		$checksum = md5($plain_text);

		//vstupni bod aplikace Home Credit - i-Shop
		$ishop_entry_point = (($storage['development']) ? $storage['dev_ishop_url'] : $storage['pub_ishop_url']);

		//url pro navrat
		$our_ishop_home    = $returnUrl;

		//nepovinne udaje
		$client_title   = '';
		$client_phone   = '';
		$client_mobile  = $order['phone'];
		$client_email   = $order['email'];
		$client_p_street= $order['street'];
		$client_p_num   = '';
		$client_p_city  = $order['city'];
		$client_p_zip   = $order['zip'];
		$client_c_street= '';
		$client_c_num   = '';
		$client_c_city  = '';
		$client_c_zip   = '';

		//prekodovani
		$client_p_city  = iconv($source_encoding, $server_encoding, $client_p_city);
		$client_p_street= iconv($source_encoding, $server_encoding, $client_p_street);
		$client_c_city  = iconv($source_encoding, $server_encoding, $client_c_city);
		$client_c_street= iconv($source_encoding, $server_encoding, $client_c_street);

		//vstupni bod aplikace i-Shop se vsemi parametry
		$url = $ishop_entry_point .
			'?shop='         . $shop_code .
			'&o_code='       . $order_code .
			'&o_price='      . $goods_price .
			'&c_name='       . urlencode($client_name) .
			'&c_surname='    . urlencode($client_surname) .
			'&g_name='       . urlencode($goods_name) .
			'&g_producer='   . urlencode($goods_producer) .
			'&ret_url='      . urlencode($our_ishop_home) .
			'&time_request=' . $request_time .
			'&sh='           . $checksum .
			'&c_title='		 . urlencode($client_title) .
			'&c_phone='		 . urlencode($client_phone) .
			'&c_mobile='	 . urlencode($client_mobile) .
			'&c_email='		 . substr($client_email,0,40) .
			'&c_p_street='   . urlencode(substr($client_p_street,0,30)) .
			'&c_p_num='      . urlencode(substr($client_p_num,0,10)) .
			'&c_p_city='     . urlencode(substr($client_p_city,0,30)) .
			'&c_p_zip='      . urlencode(substr($client_p_zip,0,6)) .
			'&c_c_street='   . urlencode(substr($client_c_street,0,30)) .
			'&c_c_num='      . urlencode(substr($client_c_num,0,10)) .
			'&c_c_city='     . urlencode(substr($client_c_city,0,30)) .
			'&c_c_zip='      . urlencode(substr($client_c_zip,0,6));

		//misto redirectu je mozne realizovat pripojeni pres html formular
		return $url;
	}
?>