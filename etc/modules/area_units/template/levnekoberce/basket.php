<?php
	$storage = Core::module('area_units')->getStorage();
	$view->override = false;

	$basket_products = Area_Units_Basket::products();

	if($basket_products !== null && count($basket_products))
		$total = Area_Units_Basket::total();

	$view->find('tfoot .subtotal .value')->html(price($total->subtotal));
	$view->find('tfoot .voucher .value')->html('-'.price($total->discount));
	$view->find('tfoot .total .value')->html(price($total->total));
	$view->find('.ajax-qty')->removeClass('ajax-qty')->addClass('ajax-qty-new');

	$i=0;
	foreach($basket_products as $product){
		$product_name = $product['product']['name'].' '.$product['product']['nameext'];
//		$product_price = price_vat(array($product['product'], $product['price']));
//		$product_total = price_vat(array($product['product'], ($product['price'] * $product['qty'])));

        $u2Fee = 0;
        $price = price_vat($product['price'], $product['product']['vat']);
        $priceTotalProduct = price_vat($product['price'] * $product['basket_product']['qty'], $product['product']['vat']);

        // Transportni navinka
        $transportFee = '';
        if(floatval($product['product']['price_transport']) > 0) {
            $transportFee = '<br /> '.__('transport_fee').' '.price_vat($product['product']['price_transport'], $product['product']['vat']);
            $product_name .= $transportFee;

            $price = price_vat($product['product']['price'] + $product['product']['price_transport'], $product['product']['vat']);
            $priceTotalProduct = price_vat(($product['product']['price'] * $product['basket_product']['qty']) + ($product['basket_product']['qty'] * $product['product']['price_transport']), $product['product']['vat']);
        }


		if($product['basket_product']['area_units_unit_1'] && $product['basket_product']['area_units_unit_2']){
            if((int) $product['basket_product']['area_units_unit_1'] > 0 && $product['basket_product']['area_units_unit_2'] > 0) {
                $u2Fee = $product['basket_product']['area_units_unit_1'] * $product['basket_product']['area_units_unit_2'] * $product['basket_product']['area_units_unit_2_fee'];
            }

			$product_name .= ' ('.$product['basket_product']['area_units_unit_1'].$product['product']['area_units_unit_1'] . ' x '.$product['basket_product']['area_units_unit_2'].$product['product']['area_units_unit_2'].')';
            $price = price_vat($product['product']['price'] * ($product['basket_product']['area_units_unit_1'] * $product['basket_product']['area_units_unit_2']) + $u2Fee + $product['product']['price_transport'], $product['product']['vat']);
            $priceTotalProduct = price_vat($product['product']['price'] * $product['basket_product']['qty'] * ($product['basket_product']['area_units_unit_1'] * $product['basket_product']['area_units_unit_2']) + ($product['basket_product']['qty'] * $u2Fee) + ($product['basket_product']['qty'] * $product['product']['price_transport']), $product['product']['vat']);
//			$product_price = price_vat(array($product['product'], $product['price'] * (float) $product['basket_product']['area_units_unit_2'] * (float) $product['basket_product']['area_units_unit_1']));
//			$product_total = price_vat(array($product['product'], ($product['price'] * $product['qty'] * (float) $product['basket_product']['area_units_unit_2'] * (float) $product['basket_product']['area_units_unit_1'])));
		}

		$view->find('.basket .tablewrap table tbody tr[data-productid="'.$product['id'].'"] .name a')->html($product_name);
        $view->find('.basket .tablewrap table tbody tr[data-productid="'.$product['id'].'"] .price')->html($price);
        $view->find('.basket .tablewrap table tbody tr[data-productid="'.$product['id'].'"] .totaltd .total')->html($priceTotalProduct);
//		$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .price')->html($product_price);
//		$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .total')->html($product_total);
		$i++;
	}
?>

<?php $view->slot_start(); ?>
<script>
	$(document).ready(function(){
		var timer;
		$('.ajax-qty-new').bind('keyup change', function(){

			$(this).closest('tr').find('.totaltd .ajax').show();
			$(this).closest('tr').find('.totaltd .total').hide();

			clearInterval(timer);  //clear any interval on key up
			timer = setTimeout(function() { //then give it a second to see if the user is finished
				$.post(_base+'area_units/recalculate_basket', {
					data: $('div.basket form').serialize()
				}, function(response){
					if($('.webmex-basket-free-shipping-module').length){
						location.reload();
					}

					var data = $.parseJSON(response);

					$.each(data.products, function(){
						var tr = $('.basket .tablewrap table tr[data-productid="'+this.id+'"]');
						tr.find('.total').show().html(this.total);
						tr.find('.ajax').hide();
						tr.find('.ajax-qty').val(this.qty);
					});

					if(data.subtotal){
						$('.basket .tablewrap table tr.subtotal:nth-child(1) td.value').html(data.subtotal);
					}

					if(data.voucher){
						$('.basket .tablewrap table tr.subtotal:nth-child(2) td.value').html(data.voucher);
					}

					$('.basket .tablewrap table tr.total td.value').html(data.total);
				});
			}, 600);
		});
	})
</script>
<?php
	$html = $view->slot_stop();
	$view->find('.basket')->append($html);
?>