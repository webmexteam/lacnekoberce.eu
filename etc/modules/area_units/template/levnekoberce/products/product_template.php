<?php
	$storage = Core::module('area_units')->getStorage();
	$view->override = false;

	foreach ($products as $product) {
		
		// s DPH
		$_price = $view->find('.product-id-'.$product['id'].' .price .with-vat')->html();
		$_price = str_replace(Core::config('currency'), '<span class="curr-wrap"><span class="currency">'.Core::config('currency').'<span></span></span></span>', $_price);
		$view->find('.product-id-'.$product['id'].' .price .with-vat')->html($_price);
		if($product['area_units_allowed'])
			$view->find('.product-id-'.$product['id'].' .price .with-vat .curr-wrap .currency span')->html('/m<sup>2</sup>');
		
		// bez DPH
		$_price = $view->find('.product-id-'.$product['id'].' .price .unvat')->html();
		$_price = str_replace(Core::config('currency'), '<span class="curr-wrap"><span class="currency">'.Core::config('currency').'<span></span></span></span>', $_price);
		$view->find('.product-id-'.$product['id'].' .price .unvat')->html($_price);
		if($product['area_units_allowed'])
			$view->find('.product-id-'.$product['id'].' .price .unvat .curr-wrap .currency span')->html('/m<sup>2</sup>');
		
	}

?>