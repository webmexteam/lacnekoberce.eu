<?php
	$storage = Core::module('area_units')->getStorage();
	$view->override = false;

	$view->find('form.basket')->attr('action', url('area_units/buy'));

	// related
	foreach ($related_products as $related) {

		// s DPH
		$_price = $view->find('.product-id-'.$related['id'].' .price .with-vat')->html();
		$_price = str_replace(Core::config('currency'), '<span class="curr-wrap"><span class="currency">'.Core::config('currency').'<span></span></span></span>', $_price);
		$view->find('.product-id-'.$related['id'].' .price .with-vat')->html($_price);
		if($related['area_units_allowed'])
			$view->find('.product-id-'.$related['id'].' .price .with-vat .curr-wrap .currency span')->html('/'.$related['area_units_unit']);

		// bez DPH
		$_price = $view->find('.product-id-'.$related['id'].' .price .unvat')->html();
		$_price = str_replace(Core::config('currency'), '<span class="curr-wrap"><span class="currency">'.Core::config('currency').'<span></span></span></span>', $_price);
		$view->find('.product-id-'.$related['id'].' .price .unvat')->html($_price);
		if($related['area_units_allowed'])
			$view->find('.product-id-'.$related['id'].' .price .unvat .curr-wrap .currency span')->html('/'.$related['area_units_unit']);

	}


	$view->slot_start();

	// Pokud u produktu neni cena, nezobrazujeme
	if(!SHOW_PRICES || $product['price'] == null || (float) $product['price'] == 0 || !$product['area_units_allowed'])
		return false;

	if($product['area_units_unit']){
		$_price1 = $view->find('.priceBox strong')->html();
		$_price2 = $view->find('.priceBox em')->html();
		$_price2 = str_replace(Core::config('currency'), Core::config('currency').'/'.$product['area_units_unit'], $_price2);

		$view->find('.priceBox strong')->html($_price1.'/'.$product['area_units_unit']);
		$view->find('.priceBox em')->html($_price2);
	}
?>

<?php

	$u1 = array();
	foreach (explode("\n", $product['area_units_default_unit_1']) as $value) {
		$u1[] = str_replace(',', '.', $value);
	}
	$u2 = array();
	foreach (explode("\n", $product['area_units_default_unit_2']) as $value) {
		$u2[] = str_replace(',', '.', $value);
	}
?>



	<?php if($product['area_units_lock_unit_1']):?>
		<input type="hidden" name="area_units_unit_1" value="<?php echo $product['area_units_default_unit_1']?>">
		<input type="text" autocomplete="off" disabled="disabled" name="nothing_1" value="<?php echo $product['area_units_default_unit_1']?>" class="text">
	<?php else:?>
		<?php if(count($u1) > 1):?>
			<select name="area_units_unit_1" class="tochange">
				<?php foreach($u1 as $v):?>
				<option value="<?php echo $v?>"><?php echo $v?></option>
				<?php endforeach?>
			</select>
		<?php else:?>
			<input type="text" autocomplete="off" name="area_units_unit_1" value="<?php echo $product['area_units_default_unit_1']?>" class="text tochange">
		<?php endif?>
	<?php endif?>
	<?php if($product['area_units_show_unit_1']):?>
	<span class="unit" style="margin:0 6px 0 -8px;font-size:11px;"><?php echo $product['area_units_unit_1']?></span>
	<?php endif?>
	<span>x</span>

	<?php if($product['area_units_lock_unit_2']):?>
        <?php $v2 = preg_replace('/(\[.*\])/', '', $u2[0]);?>
		<input type="hidden" name="area_units_unit_2" value="<?php echo $product['area_units_default_unit_2']?>">
		<input type="text" autocomplete="off" disabled="disabled" name="nothing_2" value="<?php echo $v2?>" class="text">
	<?php else:?>
		<?php if(count($u2) > 1):?>
			<select name="area_units_unit_2" class="tochange">
				<?php foreach($u2 as $v):?>
                <?php
                    $v2 = preg_replace('/(\[.*\])/', '', $v);
                ?>
				<option value="<?php echo $v?>"><?php echo $v2 ?></option>
				<?php endforeach?>
			</select>
		<?php else:?>
			<input type="text" autocomplete="off" name="area_units_unit_2" value="<?php echo $product['area_units_default_unit_2']?>" class="text tochange">
		<?php endif?>
	<?php endif?>
	<?php if($product['area_units_show_unit_2']):?>
	<span class="unit" style="margin:0 0 0 -8px;font-size:11px;"><?php echo $product['area_units_unit_2']?></span>
	<?php endif?>
	<span>=&nbsp;&nbsp;<span id="area-units-result">1</span> <?php echo $product['area_units_unit']?></span>

	<script>
		$(document).ready(function(){
			<?php if(SHOW_PRICES && $product['price'] !== null && $product['vat']): ?>
				// Cena s DPH
				var au_price_vat = <?php echo parseFloat(str_replace(',','.', price_vat(array($product, $product['price']+$product['recycling_fee']))))?>;
				var au_price = <?php echo parseFloat(str_replace(',','.', price_unvat(array($product, $product['price']+$product['recycling_fee']))))?>;
                var price_transport_vat = <?php echo parseFloat(str_replace(',','.', price_vat(array($product, $product['price_transport']))))?>;
                var price_transport = <?php echo parseFloat(str_replace(',','.', price_unvat(array($product, $product['price_transport']))))?>;
                var vat_rate = 1.21;
			<?php elseif(SHOW_PRICES && $product['price'] !== null): ?>
				// Cena bez DPH
				var au_price_vat = null;
				var au_price = <?php echo parseFloat(str_replace(',','.', price($product['price'] + $product['recycling_fee'])))?>;
                var price_transport = <?php echo parseFloat(str_replace(',','.', price(array($product, $product['price_transport']))))?>;
			<?php endif?>

			areaUnitsRecalc();

			$('[name="area_units_unit_1"], input[name="qty"], [name="area_units_unit_2"]').keyup(function(){
				areaUnitsRecalc(false);
			});

			$('[name="area_units_unit_1"], input[name="qty"], [name="area_units_unit_2"]').change(function(){
				areaUnitsRecalc();
			});

			function areaUnitsRecalc(fixValue) {
				fixValue = (typeof fixValue !== 'undefined')? fixValue: true;

				var u1 = 1,
					u2 = 1,
					quantity = parseInt($('input[name="qty"]').val()),
					final_price_vat = au_price, // S DPH jen kdyz je nastavene
					final_price = au_price; // Bez DPH nebo konecna

				if(quantity <= 0)
					quantity = 1;

				if(parseFloat($('[name="area_units_unit_1"]').val().replace(',','.')) > 0){
					u1 = parseFloat($('[name="area_units_unit_1"]').val().replace(',','.'))
					$('[name="area_units_unit_1"]').val($('[name="area_units_unit_1"]').val().replace(',','.'))
				} else if(fixValue) {
					$('[name="area_units_unit_1"]').val(u1)
				} else {
					return;
				}

                var fee2 = 0;
                var au_price_fee2 = 0;
				if(parseFloat($('[name="area_units_unit_2"]').val().replace(',','.')) > 0) {
                    var valueFee = $('[name="area_units_unit_2"]').val();

                    if(valueFee.match(/\[(.*)\]/)) {
                        fee2 = parseFloat((valueFee.match(/\[(.*)\]/)[1]).replace(',','.'));
                    }

                    au_price_fee2 = fee2 / vat_rate;

					u2 = parseFloat($('[name="area_units_unit_2"]').val().replace(',','.'))
					$('[name="area_units_unit_2"]').val($('[name="area_units_unit_2"]').val().replace(',','.'))
				} else if(fixValue) {
					$('[name="area_units_unit_2"]').val(u2)
				} else {
					return;
				}

				if($('input[name="qty"]').val() == "" || parseInt($('input[name="qty"]').val()) == 0 || !parseInt($('input[name="qty"]').val())) {
					if (fixValue) {
						$('input[name="qty"]').val(1);
						quantity = 1;
					} else {
						return;
					}
				}

				$('#area-units-result').html(u1*u2);

				if(au_price_vat) {
					final_price_vat = (quantity * u1 * u2 * au_price_vat) + (u1 * u2 * fee2 * quantity) + (price_transport_vat * quantity);
					final_price = (quantity * u1 * u2 * final_price) + (u1 * u2 * au_price_fee2 * quantity) + (price_transport * quantity);
					$('#product-price').html(displayPrice(final_price_vat));
					$('#product-price-excl-vat').html(displayPrice(final_price));
                    if(price_transport_vat > 0) {
                        $('.result .transport_fee').remove();
                        $('.result').append('<span class="transport_fee"><?php echo __('transport_fee') ?>'+displayPrice(price_transport_vat)+'</span>');
                    }
				}else{
					final_price = (quantity * u1 * u2 * final_price) + (u1 * u2 * fee2 * quantity) + (price_transport * quantity);
					$('#product-price').html(displayPrice(final_price));
                    if(price_transport > 0) {
                        $('.result .transport_fee').remove();
                        $('.result').append('<span class="transport_fee"><?php echo __('transport_fee') ?>'+displayPrice(price_transport)+'</span>');
                    }
				}
			}
		})
	</script>

<?php
	$html = $view->slot_stop();
	$view->find('#append-calculator')->append($html);
?>