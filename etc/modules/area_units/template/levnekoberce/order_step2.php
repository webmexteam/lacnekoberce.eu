<?php
$storage = Core::module('area_units')->getStorage();
$view->override = false;

if($basket_products !== null && count($basket_products))
	$total = Area_Units_Basket::total();

$view->find('tfoot .subtotal .value')->html(price(round($total->subtotal+$_SESSION['order']['dp_price']-$_SESSION['order']['dp_price']*0.1736)));
$view->find('tfoot .voucher .value')->html('-'.price($total->discount));
//$view->find('tfoot .total .value')->html(price($total->total));
$view->find('.ajax-qty')->removeClass('ajax-qty')->addClass('ajax-qty-new');

$i=0;
foreach($basket_products as $product){

	$product_name = $product['product']['name'].' '.$product['product']['nameext'];
//		$produc_price = price_vat(array($product['product'], $product['price']));
//		$product_total = price_vat(array($product['product'], ($product['price'] * $product['qty'])));

    $u2Fee = 0;
    $priceTotalProduct = price_vat($product['product']['price'] * $product['basket_product']['qty'], $product['product']['vat']);

    $transportFee = '';
    if(floatval($product['product']['price_transport']) > 0) {
        $transportFee = '<br /> '.__('transport_fee').' '.price_vat($product['product']['price_transport'], $product['product']['vat']);
        $product_name .= $transportFee;
        $priceTotalProduct = price_vat(($product['product']['price'] * $product['basket_product']['qty']) + ($product['basket_product']['qty'] *$product['product']['price_transport']), $product['product']['vat']);
    }

	if($product['basket_product']['area_units_unit_1'] && $product['basket_product']['area_units_unit_2']){
        if($product['basket_product']['area_units_unit_1'] > 0 && $product['basket_product']['area_units_unit_2'] > 0) {
            $u2Fee = $product['basket_product']['area_units_unit_1'] * $product['basket_product']['area_units_unit_2'] * $product['basket_product']['area_units_unit_2_fee'];
        }

        $transportFee = '';
        if(floatval($product['product']['price_transport']) > 0) {
            $transportFee = '<br /> '.__('transport_fee').' '.price_vat($product['product']['price_transport'], $product['product']['vat']);
        }

		$product_name .= ' ('.$product['basket_product']['area_units_unit_1'].$product['product']['area_units_unit_1'] . ' x '.$product['basket_product']['area_units_unit_2'].$product['product']['area_units_unit_2'].')';
        $priceTotalProduct = price_vat((($product['product']['price'] * ($product['basket_product']['area_units_unit_1'] * $product['basket_product']['area_units_unit_2']) + $u2Fee) * $product['basket_product']['qty']) + ($product['basket_product']['qty'] * $product['product']['price_transport']), $product['product']['vat']);

//			$product_price = price_vat(array($product['product'], $product['price'] * (float) $product['basket_product']['area_units_unit_2'] * (float) $product['basket_product']['area_units_unit_1']));
//			$product_total = price_vat(array($product['product'], ($product['price'] * $product['qty'] * (float) $product['basket_product']['area_units_unit_2'] * (float) $product['basket_product']['area_units_unit_1'])));
	}

	$view->find('.basket .tablewrap table tbody tr.basket-product-id-'.$product['id'].' .name .inner-name')->html($product_name);
    $view->find('.basket .tablewrap table tbody tr.basket-product-id-'.$product['id'].' .total')->html($priceTotalProduct);
//		$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .price')->html($product_price);
//		$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .total')->html($product_total);
	$i++;
}
?>