<?php defined('SQC') or die('No direct access.');

/*
 * Copyright (c) 2010-2011 Daniel Regéci. All rights reserved.
 * License: http://www.superqc.com/license
 */
 
class Module_Product_tabs extends Module {

	public $has_settings = true;
	
	public $module_info = array(
		'name'			=> array(
			'en' => 'Product Tabs', 
			'cs' => 'Záložky v detailu zboží',
			'sk' => 'Záložky v detaile tovaru'
		),
		'description' 	=> array(
			'en' => 'Add tabs to the product detail for a description, properties and others.',
			'cs' => 'Přidá do detailu zboží záložky pro popis, vlastnosti a další.',
			'sk' => 'Pridá do detailu tovaru záložky pre popis, vlastnosti a ďalšie.'
		),
		
		'version'		=> '1.2',
		'core_version_required' => '1.2.0-rc2',
		
		'author'		=> 'Daniel Regéci; (c) 2011',
		'license'		=> 'http://www.superqc.com/license',
		'www'			=> 'http://www.superqc.com/'
	);
	
	public $added_tabs = array();
	
	public function install($module)
	{
		$module->update(array('storage' => serialize(array('tabs' => array()))));
	}
	
	/////////
	
	public function setup()
	{
		View::addCSS('front', DOCROOT.'etc/modules/product_tabs/template/'.Core::config('template').'/css/tabs.less.css');
		View::addJS('front', 'etc/modules/product_tabs/template/'.Core::config('template').'/js/tabs.js');
	}

	public function saveSettingForm()
	{
		if(! empty($_POST['tab'])){
			$data = $this->getStorage();
			
			if(empty($data['tabs'])){
				$data['tabs'] = array();
			}
			
			$tabs = array();
				
			foreach($_POST['tab'] as $tab){
				if($tab['title']){
					$tabs[$tab['title']] = $tab['content'];
				}
			}
			
			$data['tabs'] = $tabs;
			
			$this->saveStorage($data);
			
			flashMsg(__('msg_saved'));
		}
	}
	
	public function getSettingForm()
	{
		return tpl('module_product_tabs.php', array('module' => $this));
	}
	
	public function addTab($title, $content, $key = null)
	{
		$this->added_tabs[] = array('title' => $title, 'content' => $content, 'key' => $key);
	}
	
	public function getTabs($all_tabs = true)
	{
		$data = $this->getStorage();
		$tabs = array();
		
		if($all_tabs){
			if(empty($data['tabs'])){
				$data['tabs'] = array();
			}
			
			$data['tabs'] += $this->added_tabs;
		}
		
		if(! empty($data['tabs'])){
			$i = 0;
			foreach($data['tabs'] as $title => $content){
				$key = $i;
				
				if(is_array($content) && $content['title']){
					$title = $content['title'];
					if($content['key']) $key = $content['key'];
					$content = $content['content'];
				}
			
				$tabs[] = array(
					'title' => stripslashes($title),
					'content' => stripslashes($content),
					'key' => $key
				);
				
				$i ++;
			}
		}
		
		return $tabs;
	}
	
}