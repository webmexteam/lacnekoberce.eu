<?php

$view->override = false;

$view->slot_start(); ?>
<div class="tabs">
	<ul>
		<li class="active"><a href="#description"><?php echo __('description')?></a></li>

		<?php if(count($features)): ?>
		<li><a href="#features"><?php echo __('features')?></a></li>
		<?php endif; ?>

		<?php if(Core::$is_premium && $attributes && $product['list_attributes']): ?>
		<li><a href="#attributes-list"><?php echo __('available_options')?></a></li>
		<?php endif; ?>

		<?php if(Core::$is_premium && $discounts): ?>
		<li><a href="#quantity-discounts"><?php echo __('quantity_discounts')?></a></li>
		<?php endif; ?>

		<?php/* if($related_products): ?>
		<li><a href="#related-products"><?php echo __('related_products')?></a></li>
		<?php endif; */?>
		
		<?php
			$tables = Core::module('product_tabs')->getTabs();
			krsort($tables);
			sort($tables);
		?>

		<?php foreach($tables /*Core::module('product_tabs')->getTabs()*/ as $tab): ?>
		<li><a href="#tab-<?php echo $tab['key']?>"><?php echo $tab['title']?></a></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php
$tabs = $view->slot_stop();

$view->slot_start();

?>
<?php foreach(Core::module('product_tabs')->getTabs() as $tab): ?>
	<div class="tab tab-<?php echo $tab['key']?>">
		<?php echo $tab['content']?>
	</div>
<?php endforeach; ?>
<?php
$tabpanes = $view->slot_stop();

$panes = $view->find('div.description, div.features, div.attributes-list, div.quantity-discounts, div.related-products');

$panes->find('h4.pane-title')->remove();

$panes->addClass('tab')
	->wrapAll('<div id="product-tabs" />')
	->prepend($tabs)
	->append($tabpanes);
?>
