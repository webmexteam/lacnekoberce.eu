$(function(){
	var el = $('#product-tabs');
	var tabs = el.find('.nav li');
	var tab_panes = el.find('div.tab');
	
	tab_panes.removeClass('active').hide();
	
	el.find('.nav a').bind('click', function(){
		if(!$(this).hasClass('onhp')){
			var m = this.href.match(/\#(.+)$/);
			if(!m)
				return false;
			var tab = this.href.match(/\#(.+)$/)[1];
			
			if(tab){
				tab_panes.removeClass('active').hide();
				el.find('div.'+tab).addClass('active').show();
			}
			
			tabs.removeClass('active');
			$(this).parent().addClass('active');
			
			return false;
		}
	});
	
	el.find('.nav li.active a').trigger('click');
});