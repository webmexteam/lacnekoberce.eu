<?php defined('SQC') or die('No direct access.');

/*
 * Copyright (c) 2010-2011 Daniel Regéci. All rights reserved.
 * License: http://www.superqc.com/license
 */

Core::$lang += array(
	'add_tab' => 'Pridať záložku',
);