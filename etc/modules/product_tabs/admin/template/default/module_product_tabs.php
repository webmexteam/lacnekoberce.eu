<?php defined('SQC') or die('No direct access.');

/*
 * Copyright (c) 2010-2011 Daniel Regéci. All rights reserved.
 * License: http://www.superqc.com/license
 */

?>

<fieldset>

	<?php $i = 0; foreach($module->getTabs(false) as $tab): ?>
	<?php echo forminput('text', 'tab['.$i.'][title]', $tab['title'], array('label' => __('title')))
//	.forminput('textarea', 'tab['.$i.'][content]', $tab['content'], array('rows' => '6', 'label' => __('content')))
	.forminput('textarea', 'tab['.$i.'][content]', $tab['content'], array('cls' => 'editor full', 'rows' => 6, 'label' => __('content')))?>
	
	<hr />
	<?php $i ++; endforeach; ?>
	
	<h3><?php echo __('add_tab')?></h3>
	
	<?php echo forminput('text', 'tab['.$i.'][title]', '', array('label' => __('title')))
//	.forminput('textarea', 'tab['.$i.'][content]', '', array('rows' => '6', 'label' => __('text')))
	.forminput('textarea', 'tab['.$i.'][content]', '', array('cls' => 'editor full', 'rows' => 6, 'label' => __('content'))) ?>
</fieldset>



