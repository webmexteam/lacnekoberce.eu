<?php defined('WEBMEX') or die('No direct access.');

	$view->override = false;
	
	
	$view->slot_start();

	$body = $view->find('#container');


	preg_match_all("/{carousel [+0-9]}/i", $body, $items);

    $isProcess = array();

	$i = 1;
    foreach ($items[0] as $item) {
        $carousel_id = str_replace('}', '', str_replace('{carousel ', '', $item));

        if(in_array($carousel_id, $isProcess))
            continue;

        $isProcess[] = $carousel_id;

    	$carousel = Core::$db->webmex_carousel[(int) $carousel_id];

    	if(!$carousel)
    		continue;

    	$citems = Core::$db->webmex_carousel_item()->where('carousel_id', $carousel_id);

    	if(!$citems->count())
    		continue;

    	$html = "";

    	$html .= '<div class="reset"></div><div id="webmex-carousel-'.$i.'" class="webmex-carousel"><ul>';

    	foreach($citems as $ci){

            if(isset($ci['product']) && $ci_product = Core::$db->product[(int) $ci['product']]) {
                // Product
                $title = !empty($ci['title']) ? trim($ci['title']) : trim($ci_product['name'].' '.$ci_product['nameext']);

                if(SHOW_PRICES && $ci_product['price'] !== null && (! (int) Core::config('suspend_no_stock') || ! (notEmpty($ci_product['stock']) && $ci_product['stock'] == 0)) ){
                    $btn = '<a href="'.url(PAGE_BASKET, array('buy' => $ci_product['id'])).'" class="button buy">'.__('add_to_basket').'</a>';
                }else{
                    $btn = "";
                }

                $html .= '  <li class="webmex-carousel-item webmex-carousel-product">
                                <div class="webmex-carousel-product-bg-top">
                                    <div class="webmex-carousel-product-bg-bottom">
                                        <div class="webmex-carousel-cols clearfix">
                                            <div class="webmex-carousel-cols-left">
                                                <div class="webmex-carousel-padd">
                                                    <h3><a href="'. url($ci_product) .'">'. $title .'</a></h3>
                                                    <div class="webmex-carousel-desc">'. $ci_product['description_short'] .'</div>
                                                    <strong class="webmex-carousel-price">'. price_vat($ci_product) .'</strong>
                                                    '.$btn.'
                                                </div>
                                            </div>
                                            <div class="webmex-carousel-cols-right">
                                                <a href="'. url($ci_product) .'"><img src="'. imgsrc($ci_product, null, 4, $ci_product['name']) .'"></a>
                                            </div>
                                            <div class="reset"></div>
                                        </div>
                                    </div>
                                </div>
                            </li>';

            }else{
                // Image + Link
                $html .= '<li class="webmex-carousel-item webmex-carousel-image">';
                if(!empty($ci['link'])) $html .= '<a href="'.$ci['link'].'" title="'.$ci['title'].'">';
                $html .= '<img src="'.$ci['image'].'" alt="'.$ci['title'].'" />';
                if(!empty($ci['title']) || !empty($ci['description'])) $html .= '<span class="desc-wrap"><span class="bg"></span>';
                if(!empty($ci['title'])) $html .= '<span class="title">'.$ci['title'].'</span>';
                if(!empty($ci['description'])) $html .= '<span class="description">'.$ci['description'].'</span>';
                if(!empty($ci['title']) || !empty($ci['description'])) $html .= '</span>';
                if(!empty($ci['link'])) $html .= '</a>';
                $html .= '</li>';
            }

    	}

    	$html .= '</ul></div><div class="reset"></div>';

        $carousel_settings = array(
            'loop' => (int) $carousel['loop'] ? 'true' : 'false',
            'pagination' => (int) $carousel['pagination'] ? 'true' : 'false',
            'nextPrevActions' => (int) $carousel['prevnext'] ? 'true' : 'false',
            'autoScroll' => (int) $carousel['autoscroll'] ? 'true' : 'false',
            'pause' => (int) $carousel['pause'] * 1000,
        );

    	$html .= '
    		<script>
    			$(window).load(function(){
                    var height = 0;
                    $("#webmex-carousel-'.$i.' .webmex-carousel-item").each(function(){
                        if(height < $(this).find("img").height()) {
                            height = $(this).find("img").height();
                        }
                        $(this).css({
                            "width":$("#webmex-carousel-'.$i.'").width(),
                            "height":height
                        });
                    });
                    $("#webmex-carousel-'.$i.' .webmex-carousel-item, #webmex-carousel-'.$i.' .webmex-carousel-item .webmex-carousel-product-bg-bottom").css("height", height);
                    $("#webmex-carousel-'.$i.'").carousel({
                        loop: '.$carousel_settings['loop'].',
                        pagination: '.$carousel_settings['pagination'].',
                        nextPrevActions: '.$carousel_settings['nextPrevActions'].',
                        autoScroll: '.$carousel_settings['autoScroll'].',
                        pause: '.$carousel_settings['pause'].'
                    });

                    $("#webmex-carousel-'.$i.'").hover(function(){
                        $(this).find(".rs-carousel-action-next").animate({
                            left: "5px"
                        }, 200);
                        $(this).find(".rs-carousel-action-prev").animate({
                            right: "5px"
                        }, 200);
                    }, function(){
                        $(this).find(".rs-carousel-action-next").animate({
                            left: "-26px"
                        }, 200);
                        $(this).find(".rs-carousel-action-prev").animate({
                            right: "-26px"
                        }, 200);
                    });
                });
    		</script>
    	';

		$body = preg_replace("/{carousel ".$carousel_id."}/i", $html, $body);
        $html = "";
		$i++;
    }

    echo $body;

	?>


<?php $qq = $view->slot_stop();  ?>


<?php 
	$view->find('#container')->replaceWith($qq);
?>