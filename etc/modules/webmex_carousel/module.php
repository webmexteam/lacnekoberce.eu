<?php defined('WEBMEX') or die('No direct access.');

/**
* @author Tomas Nikl, www.webmex.cz
* @copyright webmex.cz 2013
*/

class Module_Webmex_carousel extends Module {

	public $has_settings = false;

	public $module_info = array(
		'name'		=> array(
			'cs' => 'Rotátor obrázků'
		),
		'description' => array(
			'cs' => 'Přidá možnost vytvářet Rotátory obrázků (carousely)'
		),
		
		'version'	=> '2.0.0',
		'author'	=> 'Tomáš Nikl; (c) 2013',
		'www'		=> 'http://www.webmex.cz/'
	);

	public function install($module) {
		
		// Tabulka carouselu
		$carouselCols = array(
			'id' => array(
				'type' => 'integer',
				'primary' => true,
				'autoincrement' => true
			),
			'name' => array(
				'type' => 'varchar'
			),
			'loop' => array(
				'type' => 'integer'
			),
			'pagination' => array(
				'type' => 'integer'
			),
			'prevnext' => array(
				'type' => 'integer'
			),
			'autoscroll' => array(
				'type' => 'integer'
			),
			'pause' => array(
				'type' => 'integer'
			),
		);

		// Tabulka polozek carouselu
		$carouselItemCols = array(
			'id' => array(
				'type' => 'integer',
				'primary' => true,
				'autoincrement' => true
			),
			'carousel_id' => array(
				'type' => 'integer'
			),
			'title' => array(
				'type' => 'varchar'
			),
			'description' => array(
				'type' => 'text'
			),
			'link' => array(
				'type' => 'text'
			),
			'image' => array(
				'type' => 'text'
			),
			'product' => array(
				'type' => 'integer',
				'not_null' => false,
			)
		);
		
		// Create tables
		try {
			Core::$db_inst->schema->createTable('webmex_carousel', $carouselCols);
			Core::$db_inst->schema->createTable('webmex_carousel_item', $carouselItemCols);
		} catch (Exception $e) {}
	}

	public function setup() {
		View::addCSS('admin', 'etc/modules/webmex_carousel/admin/template/'.Core::config('template').'/resources/css/screen.css');

		View::addCSS('front', 'etc/modules/webmex_carousel/template/'.Core::config('template').'/resources/css/screen.css');
		View::addCSS('front', 'etc/modules/webmex_carousel/template/'.Core::config('template').'/resources/vendor/jquery-ui-carousel/dist/css/jquery.rs.carousel.css');
		View::addJS('front', 'etc/modules/webmex_carousel/template/'.Core::config('template').'/resources/vendor/jquery-ui-carousel/vendor/jquery.ui.widget.js');
		View::addJS('front', 'etc/modules/webmex_carousel/template/'.Core::config('template').'/resources/vendor/jquery-ui-carousel/dist/js/jquery.rs.carousel.js');
		View::addJS('front', 'etc/modules/webmex_carousel/template/'.Core::config('template').'/resources/vendor/jquery-ui-carousel/dist/js/jquery.rs.carousel-autoscroll.js');

		Event::add('Controller::construct', array($this, 'init'));
	}

	public function init() {
		$subnav = array();
		$subnav['carousel_list'] = array(
			'position' => 15,
			'text' => __('webmex_carousel_list_carousel'),
			'url' => url('admin/webmex_carousel'),
			'cls' => 'icon-webmex-add-carousel-list'
		);
		$subnav['carousel_item'] = array(
			'position' => 85,
			'text' => __('webmex_carousel_new_carousel_item'),
			'cls' => 'icon-webmex-add-carousel-item',
			'url' => url('admin/webmex_carousel/edit_item/0/0')
		);
		$subnav['carousel'] = array(
			'position' => 85,
			'text' => __('webmex_carousel_new_carousel'),
			'cls' => 'icon-webmex-add-carousel',
			'url' => url('admin/webmex_carousel/edit_carousel/0')
		);

		Menu::add(array(
			'position' => 2000,
			'text' => __('webmex_carousel_menu_title'),
			'iconCls' => 'icon-webmex-carousel',
			'url' => url('admin/webmex_carousel'),
			'menu' => $subnav,
		), 'webmex_carousel');
	}

	public function update() {
		Core::$db_inst->schema->addColumns('webmex_carousel_item', array(
			'product' => array(
				'type' => 'integer',
				'not_null' => false,
			)
		));

		Core::$db_inst->schema->addColumns('webmex_carousel', array(
			'loop' => array(
				'type' => 'integer'
			),
			'pagination' => array(
				'type' => 'integer'
			),
			'prevnext' => array(
				'type' => 'integer'
			),
			'autoscroll' => array(
				'type' => 'integer'
			),
			'pause' => array(
				'type' => 'integer'
			),
		));
	}
	
}