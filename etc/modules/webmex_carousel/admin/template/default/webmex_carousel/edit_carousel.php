<?php defined('WEBMEX') or die('No direct access.');

?>

<style>
.datagrid .image {
	width: 60px;
	height: 60px;
	text-align: center;
	overflow: hidden;
}
</style>

<form class="form" action="<?php echo url('admin/webmex_carousel/edit_carousel/'.$carousel['id'])?>" method="post">
	<h2><a href="<?php echo url('admin/webmex_carousel')?>"><?php echo __('webmex_carousel_list_carousel')?></a> &rsaquo; <?php echo $carousel['name'] ?></h2>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>
	
	<div class="clearfix">
		<div class="rightcol">
			<div class="tabsWrap label-left">
				<ul class="tabs clearfix">
					<li class="active"><a href="#tab-default"><?php echo __('webmex_carousel_default_settings')?></a></li>
					<li><a href="#tab-code"><?php echo __('webmex_carousel_code')?></a></li>
				</ul>
				<div id="tab-default" class="tab active">
					<fieldset>
						<?php 
							echo forminput('text', 'webmex_carousel_carousel_name', $carousel['name'], array('required' => true))
								.forminput('checkbox', 'webmex_carousel_carousel_loop', $carousel['loop'])
								.forminput('checkbox', 'webmex_carousel_carousel_pagination', $carousel['pagination'])
								.forminput('checkbox', 'webmex_carousel_carousel_prevnext', $carousel['prevnext'])
								.forminput('checkbox', 'webmex_carousel_carousel_autoscroll', $carousel['autoscroll'])
								.forminput('text', 'webmex_carousel_carousel_pause', $carousel['pause'], array('cls' => 'short'))

						?>
					</fieldset>
				</div>
				<div id="tab-code" class="tab">
					<strong><?php echo __('webmex_carousel_code')?>:</strong>
					<?php if((int) $carousel['id'] > 0):?>
						<pre class="carousel-pre">{carousel <?php echo $carousel['id']?>}</pre>
						<br>
						<p><i><?php echo __('webmex_carousel_carousel_code_info')?></i></p>
					<?php else:?>
						<pre class="carousel-pre">---</pre>
						<br>
						<p><i><?php echo __('webmex_carousel_have_to_create')?></i></p>
					<?php endif?>
				</div>
			</div>
		</div>

		<div class="leftcol">
			<h3><?php echo __('webmex_carousel_item_list')?></h3>
			<table class="datagrid">
				<thead>
					<tr>
						<td>&nbsp;</td>
						<td><?php echo __('id')?></td>
						<td><?php echo __('webmex_carousel_item_image')?></td>
						<td><?php echo __('webmex_carousel_item_title_product')?></td>
						<td class="actions">&nbsp;</td>
					</tr>
				</thead>

				<tbody>
					<?php if(! count($items)): ?>
						<tr class="norecords">
							<td colspan="5"><?php echo __('no_records')?></td>
						</tr>
					<?php endif; ?>

					<?php foreach($items as $item): ?>
						<tr>
							<td width="20"><i class="icon-webmex-image"></i></td>
							<td><?php echo $item['id']?></td>
							<td>
								<div class="image">
									<?php
										if(!empty($item['product']) && $product = Core::$db->product[(int) $item['product']]) {
											echo '<a href="' . imgsrc($product, 0) .'" class="lightbox"><img src="' . imgsrc($product, 1) . '"></a>';
										}else{
											echo '<a href="' . $item['image'] .'" class="lightbox"><img src="' . $item['image'] . '" width="60"></a>';
										}
									?>
								</div>
							</td>
							<td>
								<?php
									if(!empty($item['product']) && $product = Core::$db->product[(int) $item['product']]) {
										echo '<a href="' . url('admin/products/edit/'.$product['id'], array(),false,true) .'">' . $product['name'] . '</a>';
									}else{
										echo '<a href="'.url('admin/webmex_carousel/edit_item/'.$item['id'].'/'.$carousel['id']).'" class="edit">'.$item['title'].'</a>';
									}
								?>
							</td>
							<td class="actions">
								<a href="<?php echo url('admin/webmex_carousel/edit_item/'.$item['id'].'/'.$carousel['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
								<a href="<?php echo url('admin/webmex_carousel/delete_item/'.$item['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>

	<hr>


		
	<div class="buttons bbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>
</form>





