<?php 
	defined('WEBMEX') or die('No direct access.');
	$storage = Core::module('webmex_carousel')->getStorage();
	$products = unserialize($storage['products']);
?>

<style>
.names .inputwrap {
	width: 47% !important;
}
.related-products .products {
	display: block;
	border: 1px solid #ddd;
	padding: 0;
	margin: 10px 0 5px 0;
	background: #fff;
}
.form .label-left .related-products label {
	padding-left: 5px;
	display: block;
	float: none;
	width: 95%;
	font-weight: bold;
}
.related-products .products ul {
	margin: 0;
	padding: 0;
	list-style: none;
}
.related-products .products ul li {
	padding: 3px;
	border-bottom: 1px solid #eee;
}
.related-products .products ul li:last-child {
	border-bottom: none;
}
.related-products .products ul li .price,
.related-products .products ul li .id {
	font-size: 90%;
	color: #777;
	margin-right: 5px;
}
.related-products .products ul li .pic {
	float: left;
	width: 40px;
	margin-right: 5px;
}
.related-products .products ul li img {
	height: 40px;
	max-height: 40px;
	max-width: 40px;
}
.related-products .products ul li a.delete {
	float: right;
	width: 16px;
	height: 16px;
	background: url(./core/admin/template/default/icons/delete.png) no-repeat top right;
	text-decoration: none;
}
.related-products .products .addproduct {
	background: #eee;
	padding: 4px;
}
</style>

<form class="form" action="<?php echo url('admin/webmex_carousel/edit_item/'.$item['id'])?>" method="post">
	<?php if((int) $item['carousel_id'] > 0):?>
		<h2><a href="<?php echo url('admin/webmex_carousel')?>"><?php echo __('webmex_carousel_list_carousel')?></a> &rsaquo; <?php echo __('webmex_carousel_carousel_item')?> &rsaquo; <?php echo $item['title'] ?></h2>
	<?php else:?>
		<h2><a href="<?php echo url('admin/webmex_carousel')?>"><?php echo __('webmex_carousel_list_carousel')?></a> &rsaquo; <?php echo __('webmex_carousel_carousel_item')?></h2>
	<?php endif?>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="clearfix">
		<div class="leftcol" style="width:40%">
			<div class="tabsWrap label-left">
				<ul class="tabs clearfix">
					<li class="active"><a href="#tab-default"><?php echo __('webmex_carousel_tab_default')?></a></li>
					<li><a href="#tab-image"><?php echo __('webmex_carousel_tab_image')?></a></li>
					<li><a href="#tab-product"><?php echo __('webmex_carousel_tab_product')?></a></li>
				</ul>
				<div id="tab-default" class="tab active">
					<div class="inputwrap clearfix">
						<?php echo forminput('select', 'webmex_carousel_item_carousel_id', $item['carousel_id'], array('options' => $carousels))?>
					</div>
				</div>
				<div id="tab-image" class="tab">
					<p style="font-style:italic;margin-bottom:20px;color:#777777">Pokud chcete, aby se v carouselu zobrazoval vlastní obrázek a libovolný odkaz, zvolte tuto možnost.</p>
					<div class="inputwrap clearfix">
						<?php echo forminput('text', 'webmex_carousel_item_title', $item['title'])?>
					</div>
					<div class="inputwrap clearfix">
						<?php echo forminput('textarea', 'webmex_carousel_item_description', $item['description'])?>
					</div>
					<div class="inputwrap clearfix">
						<?php echo forminput('text', 'webmex_carousel_item_link', $item['link'])?>
					</div>
					<div class="inputwrap clearfix">
						<?php echo forminput('text', 'webmex_carousel_item_image', $item['image'])?>
					</div>
				</div>
				<div id="tab-product" class="tab">
					<p style="font-style:italic;margin-bottom:20px;color:#777777">Pokud chcete, aby se v carouselu zobrazoval produkt a odkaz na něj, zvolte tuto možnost.</p>
					<div class="inputwrap related-products">
						<label>Vybraný produkt:</label>
						<div class="products">
							<ul>
								<?php if(!empty($product)): ?>
								<?php $product = Core::$db->product[(int) $product] ?>
								<li class="clearfix">
									<a href="#" class="delete">&nbsp;</a>
									<input type="hidden" name="webmex_carousel_products[]" value="<?php echo $product['id']?>" />

									<?php echo $product['name']?><br />
									<span class="price"><?php echo price($product['price'])?></span>
									<span class="id">(<?php echo $product['id']?>)</span>
								</li>
								<?php endif; ?>
							</ul>
							<div class="addproduct">
								<a href="#">Přidat produkt</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="buttons bbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>
</form>
<script>
	$(document).ready(function($) {
		$('#inp-webmex_carousel_item_image').parent().find('label').append(' <a href="<?php echo url("admin/filemanager");?>?reset=1&ck=1&CKEditor=inp-webmex_carousel_item_image&CKEditorFuncNum=4&langCode=cs" class="j-webmex-carousel-popup" style="font-size:0.9em;">(Vybrat obrázek)</a>');

	    $('a.j-webmex-carousel-popup').live('click', function(){
	        newwindow=window.open($(this).attr('href'),'','height=600,width=940');
	        if (window.focus) {newwindow.focus()}
	        return false;
	    });
	});

	CKEDITOR.tools.callFunction = function(ref, params) {
		$('#inp-webmex_carousel_item_image').val(params);
		return false;
	}
</script>

<script>

$(function(){
	$('.addproduct').bind('click', function(){
		productfinder({
			callback: function(products){

				var wrap = $('.related-products .products ul');

				wrap.find('li').remove();

				if(products.length){
					$('.related-products .products li.empty').remove();
				}

				$.each(products, function(i, product){
					var li = $('<li class="clearfix" />');
					li.append('<a href="#" class="delete">&nbsp;</a>');
					li.append('<input type="hidden" name="webmex_carousel_products[]" value="'+product['product_id']+'" />');
					li.append(product['name']+'<br />');
					li.append('<span class="price">'+displayPrice(product['price'])+'</span>');
					li.append('<span class="id">(ID: '+product['product_id']+')</span>');

					wrap.append(li);
					return false;
				});
			}
		});
		return false;
	});

	$('.related-products .products a.delete').live('click', function(){
		$(this).parent().remove();
		return false;
	});

	var relatedCount = $('.related-products .products li').length;

	if(! relatedCount){
		$('.related-products .products ul').append('<li class="empty">Žádný produkt nebyl vybrán</li>');
	}

	$('.buttons.tbar button').click(function(){
		if($('.related-products .products ul li').length > 1) {
			alert('Můžete přidat pouze jeden produkt. Odeberte prosím produkty tak, aby zbyl pouze jeden.');
			return false;
		}
	});
});
</script>
