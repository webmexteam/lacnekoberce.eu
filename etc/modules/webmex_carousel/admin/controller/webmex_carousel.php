<?php defined('WEBMEX') or die('No direct access.');

class Controller_Webmex_Carousel extends AdminController {

	public function __construct()
	{
		parent::__construct();
		gatekeeper('webmex_carousel');
		Core::$active_tab = 'webmex_carousel';
	}
	
	public function index()
	{
		$order_by = 'id';
		$order_dir = 'DESC';
		
		sortDir($order_by, $order_dir);
		
		$carousels = Core::$db->webmex_carousel;
		$carousels->order($order_by.' '.$order_dir);
		
		$this->content = tpl('webmex_carousel/list.php', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'carousels' => $carousels
		));
	}
	
	public function edit_carousel($id)
	{
		$carousel = Core::$db->webmex_carousel[(int) $id];
		$items = array();
		
		if($id > 0 && ! $carousel){
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/webmex_carousel');
		}else{
			$items = Core::$db->webmex_carousel_item()->where('carousel_id', $id);
		}
		
		if(! empty($_POST)){
			if(($errors = validate(array('webmex_carousel_carousel_name'))) === true){
				$post = $_POST;
				$data = array(
					'name' => $post['webmex_carousel_carousel_name'],
					'loop' => $post['webmex_carousel_carousel_loop'],
					'pagination' => $post['webmex_carousel_carousel_pagination'],
					'prevnext' => $post['webmex_carousel_carousel_prevnext'],
					'autoscroll' => $post['webmex_carousel_carousel_autoscroll'],
					'pause' => $post['webmex_carousel_carousel_pause'],
				);
			}

			if (!$carousel) {
				$carousel_id = Core::$db->webmex_carousel(prepare_data('webmex_carousel', $data));

				if ($carousel_id) {
					$carousel = Core::$db->webmex_carousel[$carousel_id];
					$saved = true;
				}
			} else {
				$saved = (bool) $carousel->update(prepare_data('webmex_carousel', $data));
			}
			
			if($saved !== false && $carousel){
				flashMsg(__('msg_saved'));
			}else{
				flashMsg(__('msg_error'), 'error');
			}
			
			if(isSet($_POST['save_go'])){
				redirect('admin/webmex_carousel');
			}else{
				redirect('admin/webmex_carousel/edit_carousel/'.$carousel['id']);
			}
		} else if ($id == 0) {
			$carousel['id'] = 0;
			$carousel['name'] = __('webmex_carousel_new_carousel');
		}

		$this->content = tpl('webmex_carousel/edit_carousel.php', array(
			'carousel' => $carousel,
			'items' => $items
		));
	}
	
	public function delete_carousel($id) {
		$this->tpl = null;
		
		if($carousel = Core::$db->webmex_carousel[(int) $id]) {
			$carousel->delete();			
			flashMsg(__('msg_deleted'));
		}
		
		redirect('admin/webmex_carousel');
	}

	public function edit_item($id, $carousel_id = 0)
	{
		$item = Core::$db->webmex_carousel_item[(int) $id];
		
		if($id > 0 && ! $item){
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/webmex_carousel');
		}
		
		if(! empty($_POST)){
			if(($errors = validate(array('webmex_carousel_item_carousel_id'))) === true){
				$post = $_POST;
				$data = array(
					'carousel_id' => $post['webmex_carousel_item_carousel_id'],
					'title'       => $post['webmex_carousel_item_title'],
					'description' => $post['webmex_carousel_item_description'],
					'link'        => $post['webmex_carousel_item_link'],
					'image'       => $post['webmex_carousel_item_image'],
					'product'	  => (int) $post['webmex_carousel_products'][0],
				);
			}

			if (!$item) {
				$item_id = Core::$db->webmex_carousel_item(prepare_data('webmex_carousel_item', $data));

				if ($item_id) {
					$item = Core::$db->webmex_carousel_item[$item_id];
					$saved = true;
				}
			} else {
				$saved = (bool) $item->update(prepare_data('webmex_carousel_item', $data));
			}
			
			if($saved !== false && $item){
				flashMsg(__('msg_saved'));
			}else{
				flashMsg(__('msg_error'), 'error');
			}
			
			if(isSet($_POST['save_go'])){
				redirect('admin/webmex_carousel/edit_carousel/'.$post['webmex_carousel_item_carousel_id']);
			}else{
				redirect('admin/webmex_carousel/edit_item/'.$item['id']);
			}
		} else if ($id == 0) {
			$item['id'] = 0;
			$item['carousel_id'] = $carousel_id;
		}

		$carousels = array();

		foreach (Core::$db->webmex_carousel as $c) {
			$carousels[$c['id']] = $c['name'];
		}

		$this->content = tpl('webmex_carousel/edit_item.php', array(
			'carousel' => $carousel,
			'carousels' => $carousels,
			'product' => $item['product'],
			'item' => $item,
		));
	}

	public function delete_item($id) {
		$this->tpl = null;
		
		if($item = Core::$db->webmex_carousel_item[(int) $id]) {
			$carousel_id = $item['carousel_id'];
			$item->delete();			
			flashMsg(__('msg_deleted'));
		}
		
		redirect('admin/webmex_carousel/edit_carousel/'.$carousel_id);
	}
		
}