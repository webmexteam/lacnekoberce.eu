<?php

/**
 * Webmex - http://www.webmex.cz.
 */
version_compare(PHP_VERSION, '5.2', '<') and exit('Webmex requires PHP 5.2 or newer.');

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);

ini_set('display_errors', true);

$pathinfo = pathinfo(__FILE__);
$dirname = $pathinfo['dirname'];

define('SQC', true);
define('WEBMEX', true);
define('DOCROOT', $dirname . '/');
define('APPDIR', 'core');
define('APPROOT', DOCROOT . APPDIR . '/');

require_once(APPROOT . 'include/core.php');

// display errors and exceptions
Core::$debug = ($_SERVER['HTTP_HOST'] == 'localhost');

// display profiler at the bottom
Core::$profiler = false;

Core::setup();